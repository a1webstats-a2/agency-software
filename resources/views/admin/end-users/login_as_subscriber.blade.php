@extends( 'templates.signup' )

<?php
$configData = getAgentConfigData();
$loginLogo  = $configData['logo'] ?? '';
?>

@section( 'content' )

    <div class="col-md-6 col-md-offset-3">

        <div id="loginForm">

            <div id="a1strap">
                <h1>{{ $configData['name'] }}</h1>
            </div>

            <br/><br/>

            <form method="POST" action="/auth/login">

                <div class="form-group">
                    <label for="username">Email Address</label>
                    <input type="text" name="email" class="form-control" id="email" placeholder="Email Address">
                </div>

                <br/>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>

                <br/>

                <input type="hidden" name="subscriberID" value="{{ $subscriberID }}"/>

                <button type="submit" alt="login" class="btn btn-primary">Login</button>
            </form>
        </div>
    </div>
@endsection
