import React from 'react';

function getCurrentState(props) {
    return props;
}

class Slide extends React.Component {
    constructor(props) {
        super(props);
        this.state = getCurrentState(props);
    }
    componentWillReceiveProps(newProps) {
        this.setState(getCurrentState(newProps));
    }
    render() {
        const slide = this.state.slide;
        let display = 'Loading ...';
        if (slide.youtube_url && slide.youtube_url !== "") {
            let fullYoutubeURL = "https://www.youtube.com/embed/" + slide.youtube_url;
            display = (
                <iframe width="560" height="315" src={fullYoutubeURL}></iframe>
            )
        } else if (typeof slide.image_filename !== 'undefined') {
            let imgURL = "https://api1.websuccess-data.com/images/user_tips/" + slide.image_filename;
            let imgAlt = "User tip image for description, " + slide.instructions;
            display = (
                <img alt={imgAlt} src={imgURL} style={{maxWidth: 560}}/>
            )
        }
        return (
            <div>
                <h2>{slide.title}</h2>
                <div className="clr"></div>
                <div dangerouslySetInnerHTML={{__html: slide.instructions}}/>
                <div className="clr"></div>
                <br/>
                {display}
            </div>
        )
    }
}

export default Slide;
