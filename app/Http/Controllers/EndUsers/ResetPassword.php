<?php

namespace App\Http\Controllers\EndUsers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ResetPassword extends Controller
{
    public function index($userID, $validationString)
    {
        return view('auth.reset-password', [
            'id'               => $userID,
            'validationString' => $validationString,
            'agentData'        => getAgentConfigData(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'newPassword' => 'min:12|same:newPasswordRepeat',
        ]);

        $client     = new Client();
        $formParams = $request->input();

        $response = $client->request(
            'POST',
            env('API_SOURCE') . '/password-reset/change',
             [
                 'form_params' => $formParams,
             ]
        );

        $contents = $response->getBody()->getContents();

        $result = (bool) $contents;

        return view('auth.password-reset-finish', [
            'result'    => $result,
            'agentData' => getAgentConfigData(),
        ]);
    }
}
