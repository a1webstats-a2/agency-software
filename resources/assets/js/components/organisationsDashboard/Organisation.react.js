import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Comment from 'material-ui/svg-icons/communication/comment';
import EditRecordIcon from 'material-ui/svg-icons/content/create';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import HistoryIcon from 'material-ui/svg-icons/action/history';
import IntelligenceIcon from 'material-ui/svg-icons/action/visibility';
import TagIcon from 'material-ui/svg-icons/action/label-outline';
import Paper from 'material-ui/Paper';
import FilterActions from '../../actions/FilterActions';
import normalizeUrl from 'normalize-url';
import moment from 'moment';
import DisplayTag from '../sessionResult/DisplayTag.react';

function getCurrentState(props) {

    return {

        myKey: props.myKey,
        excludeMe: props.excludeMe,
        data: props.data,
        settings: props.settings,
        exportSelection: props.exportSelection,
        relationships: props.relationships,
        selectionType: props.selectionType,
        clientTypesByIndex: props.clientTypesByIndex
    }
}

function parseDate(inputDate) {

    var parts = inputDate.match(/(\d+)/g);

    return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
}

function ordinal_suffix_of(i) {

    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function twoDigits(d) {

    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
}

const styles = {

    popover: {

        padding: 20
    },

    checkbox: {

        marginTop: 10
    },

    headline: {

        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },

    paper: {

        marginBottom: 40,
        width: '100%'
    },

    standardTable: {

        td: {

            width: '50%',
            paddingBottom: 20
        }
    },

    buttons: {

        secondary: {

            margin: 12
        },

        primary: {

            margin: 12
        }
    },

    svg: {

        color: '#170550'
    }
};

var Organisation = React.createClass({

    getInitialState: function () {

        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (newProps) {

        this.setState(getCurrentState(newProps));
    },

    shouldComponentUpdate: function (nextProps, nextState) {

        if (this.props.settings !== nextProps.settings) {

            return true;
        }

        if (this.props.relationships !== nextProps.relationships) {

            return true;
        }

        if (this.props.selectionType !== nextProps.selectionType) {

            return true;
        }

        if (this.props.excludeMe !== nextProps.excludeMe) {

            return true;
        }

        if (this.props.exportSelection !== nextProps.exportSelection) {

            return true;
        }

        if (this.props.data !== nextProps.data) {

            return true;
        }

        return false;
    },

    render: function () {
        let organisation = this.state.data;
        let displayDate;

        if (!this.state.data[5]) {
            displayDate = 'New Visitor';
        } else {
            displayDate = moment(this.state.data[5]).format("Do MMMM YYYY");

            switch (this.state.settings.user.preferred_date_format) {
                case "dd/mm/yyyy" :
                    displayDate = moment(this.state.data[5]).format("DD/MM/YYYY");

                    break;

                case "mm/dd/yyyy" :
                    displayDate = moment(this.state.data[5]).format("MM/DD/YYYY");

                    break;

                case "yyyy-mm-dd" :
                    displayDate = moment(this.state.data[5]).format("YYYY-MM-DD");

                    break;
            }
        }

        var content = (

            <div>
                <p><strong>Number of Visits (during date range)</strong> {organisation[2]}</p>
                <p><strong>Last Visit</strong> {displayDate}</p>
            </div>
        )

        var isTracked = (organisation[6] && organisation[6] > 0) ? true : false;

        var exportMe = (typeof this.state.exportSelection[parseInt(this.state.data[0])] !== "undefined") ? true : false;

        var historyButton = "";

        if (this.state.data[5]) {
            historyButton = (
                <FlatButton onClick={this._showHistory} label="History" icon={<HistoryIcon color={styles.svg.color}/>}/>
            );
        }

        var websiteURL = '';
        var website = '';

        if (this.state.data[4]) {
            websiteURL = normalizeUrl(this.state.data[4]);

            website = (
                <p>
                    <a href={websiteURL} onClick={this._openLink} target="_blank">{websiteURL}</a>
                </p>
            );

        }


        var subtitleText = (

            <div className="subtitleText">
                {website}
                <p>{this.state.data[3]}</p>
            </div>
        )

        var linkedInLink = "";

        const linkedInHref = "https://www.linkedin.com/search/results/companies/?keywords=" + this.state.data[1] + "&origin=SWITCH_SEARCH_VERTICAL";

        linkedInLink = (

            <a target="_blank" href={linkedInHref}>
                <img src="/images/linkedin.png"/>
            </a>
        )

        let displayTags = this.state.relationships.map(function (relationship, i) {
            if (relationship) {
                if (typeof this.state.clientTypesByIndex[relationship] !== "undefined") {

                    return <DisplayTag key={i} relationship={this.state.clientTypesByIndex[relationship]}/>;

                }
            }
        }.bind(this));

        return (
            <div className="sessionResult">
                <Paper style={styles.paper} zDepth={5}>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="customSessionResultContainer">
                                <div className="customSessionResultContainerHeader">
                                    <div className="row">
                                        <div className="col-md-7">
                                            <p className="organisationName">{this.state.data[1]}</p>
                                            {subtitleText}
                                        </div>
                                        <div className="col-md-3 col-md-offset-1">
                                            {displayTags}
                                        </div>
                                        <div className="col-md-1">
                                            {linkedInLink}
                                        </div>
                                    </div>
                                </div>

                                <div className="customSessionResultContainerInner">
                                    <div className="row">
                                        <div className="col-md-7">
                                            {content}
                                        </div>

                                        <div className="col-md-5">
                                            <div className="row exportOptions">
                                                <div className="col-md-4">
                                                    <label>Select</label>
                                                    <input type="checkbox" checked={exportMe}
                                                           onChange={this._addToExportSelection}/>
                                                </div>
                                                <div className="col-md-4">
                                                    <label>Exclude</label>
                                                    <input type="checkbox" checked={this.state.excludeMe}
                                                           onChange={this._addToExcludeSelection}/>
                                                </div>
                                                <div className="col-md-4">
                                                    <label>Track</label>
                                                    <input type="checkbox" checked={isTracked}
                                                           onChange={this._trackCompany}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="customSessionResultContainerActions">
                                    <FlatButton onClick={this._openEditCompanyRecord} label="Edit"
                                                icon={<EditRecordIcon color="#170550"/>}/>
                                    <FlatButton label="Notes" onClick={this._createNote}
                                                icon={<Comment color={styles.svg.color}/>}/>
                                    {historyButton}
                                    <FlatButton onClick={this._showIntelligence} label="Intel"
                                                icon={<IntelligenceIcon color={styles.svg.color}/>}/>
                                    <FlatButton onClick={this._sendViaEmail}
                                                icon={<EmailIcon color={styles.svg.color}/>} label="Email"/>
                                    <FlatButton onClick={this._showTags} label="Tags"
                                                icon={<TagIcon color={styles.svg.color}/>}/>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="clr"></div>
                </Paper>
            </div>
        )
    },

    _openLinkedIn: function () {
        var linkedInHref = "http://www.linkedin.com/vsearch/p?company=" + this.state.data[1];
        var win = window.open(linkedInHref, '_blank');
        win.focus();
    },


    _showHistory: function () {
        FilterActions.setPreviousHistory({
            currentSession: 1,
            organisationId: parseInt(this.state.data[0]),
            organisationName: this.state.data[1]
        });
    },

    _showTags: function () {
        FilterActions.openClientTypesLightbox({
            organisation_id: parseInt(this.state.data[0]),
            relationships_json: JSON.stringify(this.state.relationships)
        });
    },

    _openIntelligence: function () {
        FilterActions.showAdditionalCompanyData(parseInt(this.state.data[0]));
    },

    _blockOrganisation: function () {
        if (!confirm("Are you sure you want to block " + this.state.data[1])) {

            return false;
        }

        FilterActions.blockOrganisation({
            name: this.state.data[1],
            id: parseInt(this.state.data[0])
        });
    },
    _showIntelligence: function () {
        FilterActions.showAdditionalCompanyData(parseInt(this.state.data[0]));
    },
    _sendViaEmail: function () {
        FilterActions.sendOrganisationViaEmail(parseInt(this.state.data[0]));
    },
    _addToExportSelection: function () {
        FilterActions.addOrRemoveQuickExportSelection({

            id: parseInt(this.state.data[0]),
            display: this.state.data[1]
        });
    },
    _openEditCompanyRecord: function () {
        FilterActions.setActiveRecord({
            data: {
                use_organisation_linked_in_address: '',
                use_organisation_tel: this.state.data[3],
                use_organisation_website: this.state.data[4],
                organisationid: parseInt(this.state.data[0]),
                use_organisation_name: this.state.data[1]
            },
            myKey: this.state.myKey
        });

    },
    _addToExcludeSelection: function () {
        FilterActions.addToExcludeSelection({
            id: parseInt(this.state.data[0]),
            name: this.state.data[1]
        });
    },

    _trackCompany: function (event) {
        var sendObj = {
            organisation_id: parseInt(this.state.data[0]),
            use_organisation_tel: this.state.data[3],
            use_organisation_website: this.state.data[4],
            organisationid: parseInt(this.state.data[0]),
            use_organisation_name: this.state.data[1],
            key: this.state.myKey,
            assigned_user: parseInt(this.state.data[6])
        };

        FilterActions.setTrackedOrganisationData(sendObj);
    },

    _createNote: function () {
        FilterActions.openNotes(parseInt(this.state.data[0]));
    }
});

export default Organisation;
