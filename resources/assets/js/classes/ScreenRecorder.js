import React 					from 'react';
import domtoimage 				from 'dom-to-image';
import PlayVideo 				from '../components/PlayVideo.react';
import ScreenRecorderActions 	from '../actions/ScreenRecorderActions';
import WebsocketConn 			from './WebsocketConn';

function getCurrentState() {

	return {

		recordingID 	: 	"",
		websocketConn 	: 	new WebsocketConn(),
		screenshot 		: 	0,
		screenshots 	: 	{},
		recording 		: 	false,
		mouseX 			: 	0,
		mouseY 			: 	0,
		frames 			: 	{},
		interval 		: 	0 	
	}
}

function getCursorPosition( event ) {
    
    var dot, eventDoc, doc, body, pageX, pageY;

    event = event || window.event; 

    if ( event.pageX == null && event.clientX != null ) {
        eventDoc = ( event.target && event.target.ownerDocument ) || document;
        doc 	= eventDoc.documentElement;
        body 	= eventDoc.body;

        event.pageX = event.clientX +
        	( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) -
          	( doc && doc.clientLeft || body && body.clientLeft || 0 );
        event.pageY = event.clientY +
          	( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) -
          	( doc && doc.clientTop  || body && body.clientTop  || 0 );
    }

    const mousePos = {
    
        x 	: 	event.pageX,
        y 	: 	event.pageY
    };

    return mousePos;
}

String.prototype.hashCode = function() {
	var hash = 0, i, chr;
	if (this.length === 0) return hash;
	for (i = 0; i < this.length; i++) {
		chr   = this.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
		hash |= 0; 
	}
	return hash;
};

class ScreenRecorder {

	constructor() {

		this.state = getCurrentState();

	}

	_storeFrame() {

		let frames 					= this.state.frames;
		frames[this.state.interval] = {

			screenshot 	: 	this.state.screenshot,
			scrollTop 	: 	window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0,
			mouseX 		: 	this.state.mouseX,
			mouseY 		: 	this.state.mouseY
		}

		this.state.frames = frames;

	}

	_setInterval() {

		this.state.interval+= 1;
	}

	_takeNewScreenshot() {

		if( !this.state.recording ) {

			return false;
		}

		let node 		= document.getElementById( 'content' );

		domtoimage.toPng( node )

		.then( function( dataUrl ) {

			const interval 						= this.state.interval;
			this.state.screenshots[interval] 	= dataUrl;
			this.state.screenshot 				= interval;

			if( typeof this.state.frames[interval] === undefined ) {

				this._storeFrame();
			}

		}.bind( this ) )
		.catch( function ( error ) {

		    console.error( 'oops, something went wrong!', error );
		});
	}

	_handleMouseMoveEvent( event ) {

		const cursorPosition 	= getCursorPosition( event );
		this.state.mouseX 		= cursorPosition.x;
		this.state.mouseY 		= cursorPosition.y;
		this._storeFrame(); 
	}

	getRecording() {

		return {

			screenshots 	: 	this.state.screenshots,
			frames 			: 	this.state.frames
		}
	}

	stopRecording() {

		this.state.recording = false;
		ScreenRecorderActions.saveRecording( this.getRecording() );
	}

	startRecording() {
				
		return false;
		const toHash 			= Date().toLocaleString() + 1653 + "";
		const newRecordingHash 	= toHash.hashCode();
		this._storeInDB( newRecordingHash );
		this.state.recordingID 	= newRecordingHash;
		this.state.recording 	= true;
		this._takeNewScreenshot();
		setInterval( this._setInterval.bind( this ), 1 );
		window.onmousemove = this._handleMouseMoveEvent.bind( this );
		window.addEventListener( 'scroll', this._storeFrame.bind( this ) );
		let classname = document.getElementsByClassName( "track" );
		classname.addEventListener( 'click', this._takeNewScreenshot.bind( this ) );
	}

	_storeInDB( recordingID ) {

		this.state.websocketConn.getConnection().send( JSON.stringify({

			type 	: 	"startRecording",
			hash 	: 	recordingID
		}));
	}
}

export default ScreenRecorder;