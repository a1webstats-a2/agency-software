import React from 'react';
import FilterActions from '../actions/FilterActions';
import FiltersStore from '../stores/FiltersStore';
import TeamMembersSelect from './TeamMembersSelect.react';
import DownloadTypes from './export/DownloadTypes.react';
import RaisedButton from 'material-ui/RaisedButton';
import ExportSelection from './export/ExportSelection.react';
import ExcludeSelection from './export/ExcludeSelection.react';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import NotificationsStore from '../stores/NotificationsStore';
import NavBar from './navigation/NavBar.react';
import SnackbarA1 from './Snackbar.react';
import SelectIncludeFields from './export/SelectIncludeFields.react';
import UserTips from './UserTips.react';
import UserTipHelpLink from './UserTipHelpLink.react';
import Checkbox from 'material-ui/Checkbox';

const styles = {

    button: {

        margin: 12
    },

    block: {
        maxWidth: 250,
    },
    radioButton: {
        marginBottom: 16,
        marginTop: 0
    },

    checkbox: {
        marginBottom: 16
    }
};


function getCurrentState(props) {

    var exportOptions = {
        exportSelection: FiltersStore.getExportSelection(),
        teamMembers: FiltersStore.getTeamMembers(),
        include: FiltersStore.getExportCriteria(),
        selectionType: FiltersStore.getExportSelectionType(),
        autoReportOpen: FiltersStore.checkLightboxOpen('newAutomatedReport', 1),
        templates: FiltersStore.getExistingTemplates(),
        automationSettings: FiltersStore.getAutomationSettings(),
        existingAutomatedReport: FiltersStore.getExistingReportData(),
        existingAutomatedReportOpen: FiltersStore.checkLightboxOpen('viewEditExistingReport', 1),
        firstLoadComplete: FiltersStore.isFirstLoadComplete('reportWizard'),
        quickOpen: FiltersStore.getQuickOpenObj(),
        createTemplateStatus: FiltersStore.getCreateTemplateStatus(),
        filters: FiltersStore.getFilters()
    }

    return {
        exportOptions: exportOptions,
        excludeSelection: FiltersStore.getExcludeSelection(),
        exportSelection: exportOptions.exportSelection,
        include: exportOptions.include,
        teamMembers: exportOptions.teamMembers,
        selectionType: exportOptions.selectionType,
        settings: FiltersStore.getAllSettings(),

        excludeSelectionByName: FiltersStore.getExcludeSelectionByName(),
        snackbarSettings: FiltersStore.getSnackbarSettings(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        }
    }
}

const ExportOptions = React.createClass({

    getInitialState: function () {

        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (newProps) {

        this.setState(getCurrentState(newProps));
    },

    componentDidMount: function () {

        FiltersStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {

        FiltersStore.removeChangeListener(this._onChange);
    },


    render: function () {

        var defaultSelect = (this.state.exportSelection.length > 0) ? 'selection' : 'all';

        return (

            <div className="reportBuilder">

                <NavBar data={this.state.notificationsData}/>

                <div className="clr"></div>

                <div className="container mainContainer">
                    <div id="results">

                        <div className="row">

                            <div className="col-md-3">

                                <h1>Export Wizard</h1>

                            </div>

                            <div className="col-md-9">

                                <div style={{marginTop: 53}}>
                                    <UserTipHelpLink/>
                                </div>
                            </div>
                        </div>


                        <div className="row">

                            <div className="col-md-2">

                                <div className="exportColumnHeader">

                                    <h3>1. Options</h3>

                                </div>

                                <div className="clr"></div>

                                <RadioButtonGroup onChange={this._setSelectionType} name="selectionType"
                                                  valueSelected={this.state.selectionType}>
                                    <RadioButton
                                        value="selection"
                                        label="Selection Only"
                                        style={styles.radioButton}
                                    />
                                    <RadioButton
                                        value="all"
                                        label="All"
                                        style={styles.radioButton}


                                    />
                                </RadioButtonGroup>

                                <Checkbox
                                    onCheck={(event, isChecked) => this._updateExportOptions(isChecked, "showFullURLOnEntryPage")}
                                    checked={this.state.exportOptions.include.showFullURLOnEntryPage}
                                    label="Show full URL on Entry Page"/>

                                <DownloadTypes
                                    csvChecked={this.state.include.exportTypes.CSV}
                                    pdfChecked={this.state.include.exportTypes.PDF}
                                    />

                                <ExportSelection settings={this.state.settings} selection={this.state.exportSelection}/>

                                <ExcludeSelection selection={this.state.excludeSelection}
                                                  selectionByName={this.state.excludeSelectionByName}/>

                            </div>

                            <div className="col-md-4">

                                <div className="exportColumnHeader">

                                    <h3>2. Select the fields you would like</h3>

                                </div>

                                <div className="clr"></div>

                                <SelectIncludeFields editing={false} data={this.state.include.include}/>

                            </div>

                            <div className="col-md-4">

                                <div className="exportColumnHeader">

                                    <h3>3. Who should receive this export?</h3>
                                </div>

                                <div className="clr"></div>

                                <TeamMembersSelect includeTeamMembers={this.state.include.includeTeamMembers}
                                                   teamMembers={this.state.teamMembers}/>

                            </div>

                            <div className="col-md-2">

                                <div className="exportColumnHeader">

                                    <h3>4. Let's Go!</h3>
                                </div>

                                <div className="clr"></div>

                                <RaisedButton label="Start Export" onClick={this._startExport} secondary={true}
                                              style={styles.button}/>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="clr"></div>
                <br/><br/><br/>

                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>
                <UserTips/>

            </div>
        )
    },

    _updateExportOptions(newVal, field) {

        let exportOptions = this.state.exportOptions.include;
        exportOptions[field] = newVal;

        FilterActions.updateExportOptions(exportOptions);
    },

    _setSelectionType: function (event, value) {

        FilterActions.setExportSelectionType(value);

    },

    _startExport: function () {

        FilterActions.startExport();
    },

    _onChange: function () {

        this.setState(getCurrentState());
    }
});

export default ExportOptions;