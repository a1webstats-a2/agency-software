import React 					from 'react';
import ScreenRecorderActions 	from '../../actions/ScreenRecorderActions';

function getCurrentState( props ) {

	return {

		videoIsLoaded 	: 	props.videoIsLoaded,
		playStatus 		: 	props.playStatus,
		numPlays 		: 	props.numPlays
	}
}

class CentreContent extends React.Component {

	constructor( props ) {

		super( props );
		this.state 		= getCurrentState( props );
	}

	componentWillReceiveProps( nextProps ) {

		this.setState( getCurrentState( nextProps ) );
	}

	render() {

		let centreDivStyle = {

			zIndex 			: 	1,
			position 		: 	'relative',
			margin 			: 	'auto',
			width 			: 	128,
			opacity 		: 	1
		}

		let display = "";

		if( !this.state.videoIsLoaded ) {

			display = ( 

				<img src="/images/pacman.gif" />
			);

		} else if( this.state.playStatus === 3 ) {

			display = (

				<img src="/images/play-button.png" onClick={this._startPlayVideo} />
			)

		} else if( this.state.playStatus === 0 ) {

			if( this.state.numPlays >= 1 ) {

				display = (

					<img src="/images/replay-arrows.png" onClick={this._startPlayVideo} />
	
				)
			
			} else {

				display = (

					<img src="/images/play-button.png" onClick={this._startPlayVideo} />
				)
			}
		
		} else {

			display = (

				<img src="/images/rounded-pause-button.png" onClick={this._pauseVideo} />
			)
		}


		return (

			<div style={centreDivStyle}>
				{display}
				<div className="clr"></div><br /><br />
			</div>
		)
	}

	_pauseVideo() {

		ScreenRecorderActions.pauseVideo();

	}

	_startPlayVideo() {

		ScreenRecorderActions.startPlayVideo();
	}
}

export default CentreContent;