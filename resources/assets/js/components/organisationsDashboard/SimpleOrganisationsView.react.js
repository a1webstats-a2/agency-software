import React from 'react';
import OrganisationsDatatable from './OrganisationsDatatable.react';

function getCurrentState(props) {
    return {
        tags: props.tags,
        flatPageVisits: props.flatPageVisits,
        type: props.selectionType,
        organisations: props.organisations,
        relationships: props.relationships
    }
}

var SimpleOrganisationsView = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },

    render: function () {
        if (!this.state.organisations) {
            return (<div></div>);
        }

        return (
            <div className="col-md-12" id="results">
                <div className="clr"></div>
                <br/>
                <OrganisationsDatatable
                    tags={this.state.tags}
                    flatPageVisits={this.state.flatPageVisits}
                    companies={this.state.organisations}
                    relationships={this.state.relationships}
                />
            </div>
        )
    }
});

export default SimpleOrganisationsView;