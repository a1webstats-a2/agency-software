import React 					from 'react';
import injectTapEventPlugin    	from 'react-tap-event-plugin';
injectTapEventPlugin();
import { Router, Route, browserHistory } from 'react-router'
import ReportBuilderApp 		from './ReportBuilder.react';
import CompanyTracker 			from './companyTracker/App.react';
import CommonPaths 				from './commonPaths/CommonPaths.react';
import Team 					from './team/Dashboard.react';
import Settings 				from './team/Settings.react';
import FilterActions 			from '../actions/FilterActions';
import Referrers 			    from './referrersDashboard/ReferrersDashboard.react';
import Organisations 			from './organisationsDashboard/OrganisationsDashboard.react';
import Keywords 				from './keywordsDashboard/KeywordsDashboard.react';
import AllPages 				from './allPagesDashboard/AllPagesDashboard.react';
import EntryPages 				from './entryPagesDashboard/EntryPagesDashboard.react';
import SavedReports 			from './export/SavedReports.react';
import CreateReport 			from './CreateReportTemplate.react';
import Export 					from './ExportOptions.react';
import Results 					from './Results.react';
import Countries 				from './countriesDashboard/CountriesDashboard.react';
import AdvancedFilters 			from './advancedFilters/AdvancedFilters.react';
import StripePayment 			from './payment/Stripe.react';
import GocardlessPayment 		from './payment/Gocardless.react';
import LoadLink 				from './LoadLink.react';
import Billing 					from './team/Payment.react';
import FilterLog 				from './FilterLog.react';
import a1Theme 					from '../themes/a1Theme';
import getMuiTheme 				from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider 		from 'material-ui/styles/MuiThemeProvider';
import DeveloperDashboard 		from './developer/DeveloperDashboard.react';
import Tags 					from './tags/Tags.react';
import LiveOrganisationView 	from './LiveOrganisationView.react';
import WebSocketTest 			from './WebSocketTest.react';
import Zapier					from './Zapier.react';

var App = React.createClass({
	getInitialState : function() {
		return {
			user 		: 	null,
			router 		: 	false,
			loaded 		: 	false
		};
	},
	componentDidMount : function() {
		const regexp  = new RegExp( 'organisations/[0-9]+', 'gi' );
		const regexp2 = new RegExp( 'results/[0-9]+', 'gi' );

		if( !window.location.pathname.match( regexp ) &&
			!window.location.pathname.match( regexp2 ) ) {

			FilterActions.loadInitialData();
		}
	},
	render : function() {
		const routes = [
			<Route key={1} path="/" component={ReportBuilderApp} />,
			<Route key={2} path="/company-tracker" component={CompanyTracker} />,
			<Route key={3} path="/common-paths" component={CommonPaths} />,
			<Route key={4} path="/settings" component={Settings} />,
			<Route key={5} path="/team" component={Team} />,
			<Route key={6} path="/referrers" component={Referrers} />,
			<Route key={7} path="/organisations" component={Organisations} />,
			<Route key={8} path="/keywords" component={Keywords} />,
			<Route key={9} path="/all-pages" component={AllPages} />,
			<Route key={10} path="/entry-pages" component={EntryPages} />,
			<Route key={11} path="/saved-templates" component={SavedReports} />,
			<Route key={12} path="/create-template" component={CreateReport} />,
			<Route key={13} path="/export" component={Export} />,
			<Route key={14} path="/results" component={Results} />,
			<Route key={16} path="/countries" component={Countries} />,
			<Route key={19} path="/advanced-report" component={AdvancedFilters} />,
			<Route key={20} path="/lt/organisation/:organisationID" component={LoadLink} />,
			<Route key={21} path="/payment/gocardless" component={GocardlessPayment} />,
			<Route key={22} path="/payment/stripe" component={StripePayment} />,
			<Route key={23} path="/billing" component={Billing} />,
			<Route key={24} path="/organisations/:reportID" component={Organisations} />,
			<Route key={25} path="/results/:reportID" component={Results} />,
			<Route key={26} path="/developer" component={DeveloperDashboard} />,
			<Route key={27} path="/tags" component={Tags} />,
			<Route key={28} path="/filter-log" component={FilterLog} />,
			<Route key={29} path="/live-org-view" component={LiveOrganisationView} />,
			<Route key={30} path="/websocket-test" component={WebSocketTest} />,
			<Route key={31} path="/zapier" component={Zapier} />
		];

		return (
			<MuiThemeProvider muiTheme={getMuiTheme(a1Theme)}>
				<Router 
					history={browserHistory} 
					routes={routes} />
			</MuiThemeProvider>
		);
	}
});

export default App;
