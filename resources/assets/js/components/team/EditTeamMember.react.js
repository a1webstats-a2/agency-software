import React from 'react';

import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

function getCurrentState( props ) {

	var teamMember = FiltersStore.getTeamMemberToEdit();

	return {

		teamMember 	: 	teamMember,
		open 		: 	FiltersStore.checkLightboxOpen( 'editTeamMember', 1 ),
		newDetails 	: 	teamMember
	}
}

const style = {

	marginLeft : 20
};

var EditTeamMember = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState() );
	},

	handleClose : function() {

		FilterActions.closeLightbox();
	},

	updateForename : function( event ) {

		var newDetails = this.state.newDetails;

		newDetails.forename = event.target.value;

		this.setState({

			newDetails : newDetails
		})
	},

	updateSurname : function( event ) {

		var newDetails = this.state.newDetails;

		newDetails.surname = event.target.value;

		this.setState({

			newDetails : newDetails
		})
	},

	updateEmail : function( event ) {
		
		var newDetails = this.state.newDetails;

		newDetails.email = event.target.value;

		this.setState({

			newDetails : newDetails
		})
	},

	updatePassword : function( event ) {

		var newDetails = this.state.newDetails;

		newDetails.password = event.target.value;
		
		this.setState({

			newDetails : newDetails
		})
	},

	setRepeatPassword : function( event ) {

		var newDetails = this.state.newDetails;

		newDetails.repeatPassword = event.target.value;
		
		this.setState({

			newDetails : newDetails
		})
	},

	render : function() {

		const actions = [
			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this.handleClose}
			/>,
			<FlatButton
				label="Submit"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this._updateTeamMember}
			/>,
		];

		var forename 	= '';
		var surname 	= '';
		var email 		= '';


		if( this.state.teamMember && typeof this.state.teamMember.forename !== "undefined" ) {

			forename 	= this.state.teamMember.forename;
			surname 	= this.state.teamMember.surname;
			email 		= this.state.teamMember.email;
		}

		return (

			<Dialog
				title="Edit Team Member Details"
				actions={actions}
				modal={false}
				open={this.state.open}
				onRequestClose={this.handleClose}>

					<TextField id="forename" name="forename" hintText="Forename" onChange={this.updateForename} floatingLabelText="Forename"  defaultValue={forename} style={style} underlineShow={false} />
					<Divider />
					<TextField id="surname" name="surname" hintText="Surname" onChange={this.updateSurname} floatingLabelText="Surname" defaultValue={surname} style={style} underlineShow={false} />
					<Divider />
					<TextField id="email" name="email" hintText="Email address" onChange={this.updateEmail} floatingLabelText="Email" defaultValue={email} style={style} underlineShow={false} />
					<Divider />
					<TextField id="password" name="password" hintText="New Password" onChange={this.updatePassword} floatingLabelText="Password (min 12 characters)" style={style} type="password" underlineShow={false} />
					<Divider />
					<TextField id="newPassword" name="newPassword" hintText="Confirm New Password" onChange={this.setRepeatPassword} floatingLabelText="Confirm new password"style={style} type="password" underlineShow={false} />
					<Divider />

			</Dialog>
		);
	},

	_updateTeamMember : function() {

		FilterActions.closeLightbox();

		FilterActions.updateTeamMember( this.state.newDetails );

	}

});

export default  EditTeamMember;