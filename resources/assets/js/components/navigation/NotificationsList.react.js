import React from 'react';
import IndividualMessage from './IndividualMessage.react';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import Badge from 'material-ui/Badge';
import Notifications from 'material-ui/svg-icons/social/notifications';
import NotificationsActive from 'material-ui/svg-icons/social/notifications-active';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import Delete from 'material-ui/svg-icons/action/delete';
import Folder from 'material-ui/svg-icons/file/folder';
import Avatar from 'material-ui/Avatar';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import Dialog from 'material-ui/Dialog';
import NotificationActions from '../../actions/NotificationActions';
import moment from 'moment';

function getCurrentState( props ) {

	return {

		notificationsBoxOpen 	: 	props.notificationsBoxOpen,
		numberOfNew 			: 	props.numberOfNew,
		settings 				: 	props.settings,
		isNewNotifications		: 	props.isNewNotifications,
		notifications 			: 	props.notifications,
		display 				: 	props.display,
		viewNotificationID 		: 	props.viewNotificationID
	}
}

function twoDigits( d ) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}


function ordinal_suffix_of( i ) {
    
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

var NotificationsList = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		var notifications 	 = this.state.notifications;

		var oldNotifications = notifications.old.map( function(notification, n ) {


	        var displayDate     =   moment( notification.notification_date ).format( "Do MMMM YYYY" ); 

	        switch( this.state.settings.user.preferred_date_format ) {

	            case "dd/mm/yyyy" :

	                displayDate = moment( notification.notification_date ).format( "DD/MM/YYYY" );

	                break;

	            case "mm/dd/yyyy" :

	                displayDate = moment( notification.notification_date ).format( "MM/DD/YYYY" );

	                break;

	            case "yyyy-mm-dd" :

	                displayDate = moment( notification.notification_date ).format( "YYYY-MM-DD" );
	                
	                break;

	        }
				
			var selected  = this.state.notifications.selection.indexOf( notification.notification_id ) > -1;

			var avatarSrc = "/api/proxy/api/v1/user/avatar/" + notification.created_by_end_user_id;

			var hyperlink = "/internal/download/contents/" + notification.asset_id;

			return ( 

				<TableRow key={n} selected={selected} className="notification">

					<TableRowColumn><Avatar className="floatingAvatar" src={avatarSrc} />{notification.forename} {notification.surname}</TableRowColumn>
					<TableRowColumn>{displayDate}</TableRowColumn>
					<TableRowColumn>{notification.message}</TableRowColumn>
					<TableRowColumn><a href="#" onClick={(event)=>this._viewNotification( notification.notification_id, event )}>View</a></TableRowColumn>

				</TableRow>

			);

		}.bind( this ) );

		var newNotifications = notifications.new.map( function( notification, n ) {

			var displayDate     =   moment( notification.notification_date ).format( "Do MMMM YYYY" ); 

	        switch( this.state.settings.user.preferred_date_format ) {

	            case "dd/mm/yyyy" :

	                displayDate = moment( notification.notification_date ).format( "DD/MM/YYYY" );

	                break;

	            case "mm/dd/yyyy" :

	                displayDate = moment( notification.notification_date ).format( "MM/DD/YYYY" );

	                break;

	            case "yyyy-mm-dd" :

	                displayDate = moment( notification.notification_date ).format( "YYYY-MM-DD" );
	                
	                break;

	        }
			
			var avatarSrc 	= "/api/proxy/api/v1/user/avatar/" + notification.created_by_end_user_id;

			var hyperlink 	= "/internal/download/contents/" + notification.asset_id;

			var selected 	= this.state.notifications.selection.indexOf( notification.notification_id ) > -1;

			var fontColour 	= ( notification.read ) ? '#999' : '#000';

			return ( 

				<TableRow key={n} selected={selected} className="notification" style={{ color : fontColour }}>

					<TableRowColumn><Avatar className="floatingAvatar" src={avatarSrc} />{notification.forename} {notification.surname}</TableRowColumn>
					<TableRowColumn>{displayDate}</TableRowColumn>
					<TableRowColumn>{notification.message}</TableRowColumn>
					<TableRowColumn><a href="#" onClick={(event)=>this._viewNotification( notification.notification_id, event )}>View</a></TableRowColumn>

				</TableRow>
			);

		}.bind( this ) );
	
		if( this.state.display === "all" ) {

			var actions = [

				<FlatButton 
					label="Delete"
					icon={<Delete />}
					secondary={true}
					onTouchTap={this._deleteSelection}
				/>,

		    	<FlatButton
		        	label="Close"
		        	secondary={true}
		        	onTouchTap={this.closeNotificationsBox}
		      	/>
	      	];


	      	if( typeof this.state.notifications.viewType === "string" && this.state.notifications.viewType.trim() === "current" ) {

	      		actions.unshift(

	      			<FlatButton 
						label="Archive"
						icon={<Folder />}
						secondary={true}
						onTouchTap={this._archiveSelection}
					/>
	      		)
	      	}

	    } else {

	    	var actions = [

	    		<FlatButton 
					label="Delete"
					icon={<Delete />}
					secondary={true}
					onTouchTap={this._deleteIndividual}
				/>,

		    	<FlatButton
		        	label="Back"
		        	secondary={true}
		        	onTouchTap={this._backToAllMessages}
		      	/>,

		      	<FlatButton
		        	label="Close"
		        	secondary={true}
		        	onTouchTap={this.closeNotificationsBox}
		      	/>

	    	];

	    }

		if( this.state.display === "all" ) {

			var content = (

				<Tabs onChange={this._setNotificationViewType}>

	        		<Tab label="Current" value="current">
	        		
	        			<br /><br />
	        			<Table
	  					 	onRowSelection={this._addToNewExportSelection}
							multiSelectable={true}
							selectable={true}
	        			>
			        		<TableHeader
			        			enableSelectAll={true}
								displaySelectAll={true}
			        		>
			        			<TableRow>
			        				<TableHeaderColumn>From</TableHeaderColumn>
			        				<TableHeaderColumn>Date</TableHeaderColumn>
			        				<TableHeaderColumn>Message</TableHeaderColumn>
			        				<TableHeaderColumn></TableHeaderColumn>
			        			</TableRow>

			        		</TableHeader>

			        		<TableBody
								displayRowCheckbox={true}
								deselectOnClickaway={false}
			        		>
			        			{newNotifications}
			        		</TableBody>
			        	</Table>
	        		</Tab>

	        		<Tab label="Archived" value="archived">
	        			<br /><br />
	        			<Table 
	        				className="table"
	        				multiSelectable={true}
							selectable={true}
							onRowSelection={this._addToArchiveExportSelection}

	        			>
	        				<TableHeader
	        					enableSelectAll={true}
								displaySelectAll={true}
	        				>
			        			<TableRow>
			        				<TableHeaderColumn>From</TableHeaderColumn>
			        				<TableHeaderColumn>Date</TableHeaderColumn>
			        				<TableHeaderColumn>Message</TableHeaderColumn>
			        				<TableHeaderColumn>Actions</TableHeaderColumn>
			        			</TableRow>

			        		</TableHeader>
	        				<TableBody
	        					displayRowCheckbox={true}
								deselectOnClickaway={false}
	        				>
	        					{oldNotifications}
	        				</TableBody>
	        			</Table>
	        		</Tab>
	        	</Tabs>
	        );

		} else {

			var content = (

				<div>
					<IndividualMessage settings={this.state.settings} viewNotificationID={this.state.viewNotificationID} notifications={this.state.notifications} />
				</div>
			)
		}

		return (

			<Dialog
	          	title="Notifications"
	          	modal={false}
	          	actions={actions}
	          	autoScrollBodyContent={true}
	          	open={this.state.notificationsBoxOpen}
	          	onRequestClose={this.closeNotificationsBox}
	        >
	        	{content}
	        </Dialog>


		)
	},

	closeNotificationsBox : function() {

		NotificationActions.closeNotifications();
        
    },


	_viewNotification : function( attributeID, event ) {

		event.preventDefault();

		NotificationActions.viewNotification( attributeID );	
	
		event.stopPropagation();

	},

	_deleteIndividual : function() {

		if( ! confirm( "Are you sure you want to delete this message?" ) ) {

			return false;
		}

		NotificationActions.deleteIndividual();
	},

	_backToAllMessages : function() {	

		NotificationActions.backToAllMessages();
	},

	_deleteSelection : function() {

		if( !confirm( "Are you sure you want to continue" ) ) {

			return false;
		}

		NotificationActions.deleteSelection();
	},


	_archiveSelection : function() {

		NotificationActions.archiveSelection();
	},

	_addToNewExportSelection : function( selectedRows ) {

		var selection = [];

		if( selectedRows !== "all" ) {

			if( Array.isArray( selectedRows ) ) {

				selection = selectedRows.map( function( select, i ) {

					if( typeof this.state.notifications.new !== "undefined" &&
						typeof this.state.notifications.new[select] !== "undefined" ) {	

						return this.state.notifications.new[select].notification_id;

					}
				
				}.bind ( this ) );
			}

		} else {

			if( Array.isArray( this.state.notifications.new ) ) {

				selection = this.state.notifications.new.map( function( notification, n ) {

					return notification.notification_id;
				})
			}
		} 


		NotificationActions.setNotificationSelection({

			type 		: 	'new',
			selected  	: 	selection 
		});
	},

	_addToArchiveExportSelection : function( selectedRows ) {

		if( selectedRows === "all" ) {

			var selection = "all";
		
		} else {

			var selection = [];

			var selection = this.state.notifications.old.map( function( notification, n ) {

				return notification.notification_id;
			})
		}
		
		NotificationActions.setNotificationSelection({

			type 		: 	'archive',
			selected 	: 	selection 
		});
	},

	_setNotificationViewType : function( type ) {

		if( typeof type !== "string" ) {

			return false;
		}

		NotificationActions.setNotificationsViewType( type );
	}
});

export default  NotificationsList