import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import Address from './Address.react';
import {Tabs, Tab} from 'material-ui/Tabs';

function checkUndefined( val ) {

	if( typeof val === "undefined" ) {

		return " unknown";
	}

	return val;
}

function getCurrentState( props ) {

	var companiesHouseData 	= false;
	var a1Data 				= false;

	if( typeof props.additionalCompanyData !== "undefined" ) {

		if( typeof props.additionalCompanyData.data[1] !== "undefined" ) {

			a1Data = props.additionalCompanyData.data[1];
		}

		if( typeof props.additionalCompanyData.data[2] !== "undefined" ) {

			companiesHouseData = props.additionalCompanyData.data[2];
		}
	}

	return {

		open 			: 	props.open,
		a1Data 			: 	a1Data,
		companiesHouse 	: 	companiesHouseData,
		isLoading 		: 	props.isLoading
	};
}


const styles = {
	headline 	: {
		fontSize 		: 24,
		paddingTop 		: 16,
		marginBottom 	: 12,
		fontWeight		: 400,
	},
};

var AdditionalCompanyData = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props ); 
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render 	: 	function() {

		 const actions = [
			<FlatButton
				label="Close"
				primary={true}
				onTouchTap={this._handleClose} />
		];

		var companiesHouseDisplay = "";
		var a1Data = "";

		if( this.state.a1Data ) {

			var trs = [];

			if( typeof this.state.a1Data.trading_address1 === "undefined" ) {

				var tradingAddressArray = [ 'Unknown' ];

				var tradingAddress = <Address type="Trading" address={tradingAddressArray} />
			
			} else {

				var tradingAddressArray = [

					this.state.a1Data.trading_address1,
					this.state.a1Data.trading_address2,
					this.state.a1Data.trading_address3,
					this.state.a1Data.trading_address_postcode
				];

				var tradingAddress 		= <Address type="Trading" address={tradingAddressArray} />

			}

			trs.push({

				description : 	'Name',
				value 		: 	checkUndefined( this.state.a1Data.name )

			})

			trs.push({

				description : 	'Reg care of',
				value 		: 	checkUndefined( this.state.a1Data.reg_care_of )

			})

			trs.push({

				description : 	'Company Number',
				value 		: 	checkUndefined( this.state.a1Data.id )

			})

			trs.push({

				description : 	'Company Type',
				value 		: 	checkUndefined( this.state.a1Data.company_type )
			})

			trs.push({

				description 	: 	'Description',
				value 			: 	checkUndefined( this.state.a1Data.description )
			})

			trs.push({

				description 	: 	'SIC Code',
				value 			: 	checkUndefined( this.state.a1Data.sic_code )
			})

			trs.push({

				description 	: 	'SIC Description',
				value 			: 	checkUndefined( this.state.a1Data.sic_description )
			})

			trs.push({

				description : 	'Net Assets',
				value 		: 	'£' + checkUndefined( this.state.a1Data.accounts_assets_net )
			})

			trs.push({

				description : 	'Total Assets',
				value 	 	: 	'£' + checkUndefined( this.state.a1Data.accounts_assets_total_current )

			});

			trs.push({

				description : 	'Cash',
				value 		: 	'£' + checkUndefined( this.state.a1Data.accounts_cash )
			})

			trs.push({

				description : 	'Current Liabilities',
				value 		: 	'£' + checkUndefined( this.state.a1Data.accounts_liabilities_current )
			})

			trs.push({

				description : 	'Total Liabilities',
				value 		: 	'£' + checkUndefined( this.state.a1Data.accounts_liabilities_total )
			})

			trs.push({

				description : 	'Net Worth',
				value 		: 	'£' + checkUndefined( this.state.a1Data.accounts_net_worth )
			})

			trs.push({

				description : 	'Paid Up Equity',
				value 		: 	'£' + checkUndefined( this.state.a1Data.accounts_paid_up_equity )
			})

			trs.push({

				description : 	'Shareholder Funds',
				value 		: 	'£' + checkUndefined( this.state.a1Data.accounts_shareholder_funds )
			})

			var trElements = trs.map( function( tr, i ) {

				return (


					<tr key={i}>

						<td>{tr.description}</td>
						<td>{tr.value}</td>

					</tr>
				)
			});

			a1Data = (

				<div className="AdditionalCompanyData">

					<br />

					{tradingAddress}

					<br />

					<table className="table">

						<thead>
							<tr>
								<th>Description</th>
								<th>Value</th>
							</tr>
						</thead>

						<tbody>
							{trElements}

						</tbody>

					</table>

				</div>
			);

			if( trElements.length === 0 ) {

				a1Data = (

					<p>No data found</p>
				)
			}

		}

		if( this.state.companiesHouse ) {

			var companiesHouseAddressData = [

					this.state.companiesHouse.address.premises,
					this.state.companiesHouse.address.address_line_1,
					this.state.companiesHouse.address.locality,
					this.state.companiesHouse.address.postal_code,
					this.state.companiesHouse.address.country
				];

			var companiesHouseAddress 	= <Address type="Companies House" address={companiesHouseAddressData} />

			companiesHouseDisplay = (

				<div className="companiesHouseInfo" style={{ height : 400, overflow : 'scroll' }}>

					<br />

					{companiesHouseAddress}

					<br />

					<table className="table">

						<thead>
							<tr>
								<th>Description</th>
								<th>Value</th>
							</tr>
						</thead>

						<tbody>

							<tr>
								<td>Title</td>
								<td>{checkUndefined( this.state.companiesHouse.title )}</td>
							</tr>

							<tr>
								<td>Care of</td>
								<td>{checkUndefined( this.state.companiesHouse.care_of_name )}</td>
							</tr>

							<tr>
								<td>Company Number</td>
								<td>{checkUndefined( this.state.companiesHouse.company_number )}</td>
							</tr>

							<tr>
								<td>Company Status</td>
								<td>{checkUndefined( this.state.companiesHouse.company_status )}</td>
							</tr>

							<tr>
								<td>Company Type</td>
								<td>{checkUndefined( this.state.companiesHouse.company_type )}</td>
							</tr>

							<tr>
								<td>Date of Creation</td>
								<td>{checkUndefined( this.state.companiesHouse.date_of_creation )}</td>
							</tr>

							<tr>
								<td>Description</td>
								<td>{checkUndefined( this.state.companiesHouse.description )}</td>
							</tr>
						</tbody>
					</table>
				</div>
			)
		}

		let display = "";

		if( this.state.isLoading ) {

			display = (

				<div>
					<div className="clr"></div><br />
					<p>Loading</p>
					<div className="clr"></div><br /><br />
					<img src="/images/loading_large.gif" />
				</div>
			)
		
		} else {

			display = (

				<Tabs>
					<Tab label="Companies House Data">

						{companiesHouseDisplay}
					</Tab>
					<Tab label="Other Intel">

						{a1Data}

					</Tab>

				</Tabs>
			)
		}

		return (


			 <Dialog
				title="Additional Company Data"
				actions={actions}
				autoScrollBodyContent={true}
				modal={false}
				open={this.state.open}
				onRequestClose={this.handleClose}
				>

				{display}
				

			</Dialog>
		)

		
	},

	_handleClose : function() {

		FilterActions.closeLightbox();
	}

});

export default  AdditionalCompanyData;