import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import APIIcon from 'material-ui/svg-icons/action/http';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

function getCurrentState( props ) {
	return {
		newOrgHook 		: 	props.settings.newOrgHook,
		newVisitHook 	: 	props.settings.newVisitHook,
		secret 			: 	FiltersStore.getClientSecret(),
		clientId 		: 	FiltersStore.getClientId()
	}
}

var API = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {
		if( this.state.secret !== "" ) {
			var secret = this.state.secret;
		} else {
			var secret = (
				<button 
					onClick={this._showSecret}
					className="btn btn-primary">Show Secret</button>
			)
		}

		let apiURL = (
			<a href="https://apidocs.websuccess-data.com" target="_blank">
							https://apidocs.websuccess-data.com</a>
		)

		return (
			<div>
				<div className="row">
					<div className="col-md-12">
						<h3>API</h3><br />
					</div>
				</div>

				<div className="clr"></div><br /><br />

				<div className="row">

					<div className="col-md-12">

						<APIIcon />

						<h4>API</h4>

						<div className="clr"></div><br />

						<p>We have recently launched limited API functionality. To view the 
							endpoints available, please visit: {apiURL}</p>

						<p>We provide a RESTful API authenticated via OAuth2</p>

						<p>Before calling the API endpoints you will need a token. To generate a token please
							send a GET request to https://api1.websuccess-data.com/oauth/get-access-token</p>

						<p>You will need to pass the following parameters:</p>

						<Table>
							<TableHeader adjustForCheckbox={false} displaySelectAll={false}>
								<TableRow>
									<TableHeaderColumn>Parameter</TableHeaderColumn>
									<TableHeaderColumn>Value</TableHeaderColumn>
								</TableRow>
							</TableHeader>
							<TableBody displayRowCheckbox={false}>
								<TableRow>
									<TableRowColumn>client_id</TableRowColumn>
									<TableRowColumn>{this.state.clientId}</TableRowColumn>
								</TableRow>
								<TableRow>
									<TableRowColumn>client_secret</TableRowColumn>
									<TableRowColumn>
										{secret}
									</TableRowColumn>
								</TableRow>
								<TableRow>
									<TableRowColumn>grant_type</TableRowColumn>
									<TableRowColumn>"client_credentials"</TableRowColumn>
								</TableRow>
							</TableBody>
						</Table>

						<div className="clr"></div><br />

						<p>Then with each api call, please pass the parameter 'access_token' with the value returned.</p>
					</div>
				</div>

				<div className="clr"></div><br /><br />
			</div>
		)
	},

	_showSecret : function() {

		FilterActions.showClientSecret();
	}
});

export default  API;