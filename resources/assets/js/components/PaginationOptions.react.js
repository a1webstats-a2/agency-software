import React         	from 'react';
import PaginationNext 	from './pagination/next.react';
import PaginationPrev 	from './pagination/prev.react';
import FiltersStore 	from '../stores/FiltersStore';
import SelectField 		from 'material-ui/SelectField';
import MenuItem 		from 'material-ui/MenuItem';
import FilterActions 	from '../actions/FilterActions';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import UpdateResultsIcon from 'material-ui/svg-icons/action/cached';
import RaisedButton from 'material-ui/RaisedButton';
import SaveIcon from 'material-ui/svg-icons/content/save';
import FileDownloadIcon from 'material-ui/svg-icons/file/file-download';
import BankIcon from 'material-ui/svg-icons/editor/attach-money';
import Slider from 'material-ui/Slider';

function getCurrentState( props ) {

	return {

		orderBy 	 			: 	props.orderBy,
		pagination 				: 	props.paginationData,
		isApplicationResting 	: 	props.isApplicationResting,
		isLoadingResults 		: 	!props.isApplicationResting,
		minNumberOfPages 		: 	props.minNumberOfPages,
		maxNumberOfPages 		: 	props.maxNumberOfPages,
		minSessionDuration 		: 	props.minSessionDuration
	}
}

const style = {
  
  	refresh: {
    	display 	: 	'inline-block',
    	position 	: 	'relative',
  	},

	raisedButton : 	{

		marginBottom 	: 	20,
		fullWidth 		: 	true
	}
};

var PaginationOptions = React.createClass({ 

	getInitialState : function() {
				
		return getCurrentState( this.props );
	},

	componentDidMount: function() {
    	
    	FiltersStore.addChangeListener(this._onChange);
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	componentWillUnmount: function() {
	
		FiltersStore.removeChangeListener(this._onChange);
	},



	render : 	function() {

		var bankResultsDisabled 	= ( this.state.pagination.total > 0 ) ? false : true;

		var pageNum 				= ( Math.ceil( this.state.pagination.offset / this.state.pagination.returnAmount ) ) +1;

		var paginationLoadingStatus = ( !this.state.isApplicationResting ) ? 'loading' : 'ready';

		if( typeof this.state.pagination.total === "undefined" ) {

			return 	( 

				<div className="paginationOptions">


					<h3>Results Display</h3>

					<p><strong>0</strong> result(s) found</p>

				</div> 
			);
		
		} else {

			let numOfPages = this.state.pagination.numPages;

			if( isNaN( numOfPages ) ) {

				numOfPages = 1;
			}

			return (

				<div className="paginationOptions">

					<div className="row">

						<div className="col-md-9">
							<h3>Display Options</h3>
						</div>

						<div className="col-md-3">
							<RefreshIndicator
								size={30}
								left={0}
								className="refreshIndicator"
								top={20}
								loadingColor={"#FF9800"}
								status={paginationLoadingStatus}
								style={style.refresh} />
						</div>
					</div>

					<SelectField 
						style={{ fontSize : 12 }} 
						fullWidth={true} 
						onChange={this._setResultsPerPage} 
						floatingLabelText="Number of Results per Page" 
						floatingLabelStyle={{ fontSize : 14 }}
						value={this.state.pagination.returnAmount}>

						<MenuItem value={10} primaryText="10" />
						<MenuItem value={50} primaryText="50" />
						<MenuItem value={100} primaryText="100" />
					</SelectField>

					<div className="clr"></div>

					<SelectField 
						style={{ fontSize : 12 }} 
						fullWidth={true} 
						onChange={this._setOrderBy} 
						value={this.state.orderBy} 
						floatingLabelStyle={{ fontSize : 14 }}
						floatingLabelText="Sort By">

						<MenuItem value={0} primaryText="Date Ascending" />
						<MenuItem value={1} primaryText="Date Descending" />
					</SelectField>

					<div className="clr"></div><br />

					<div className="row">

						<div className="col-md-12">
							<p>Page <strong>{pageNum}</strong> of <strong>{numOfPages}</strong> | 

							&nbsp;<strong>{this.state.pagination.total}</strong> result(s) found</p>

						</div>
		
					</div>

					<div className="clr"></div>
			
					<br />

					<PaginationPrev isLoadingResults={this.state.isLoadingResults} paginationSettings={this.state.pagination} />
					
					<div className="clr"></div>

					<PaginationNext isLoadingResults={this.state.isLoadingResults} paginationSettings={this.state.pagination}  />	

					<div className="clr"></div><br />

					<p>Minimum number of page visits: {this.state.minNumberOfPages}</p>

				    <Slider
			        	min={0}
			        	max={5}
			          	step={1}
			          	defaultValue={1}
			          	value={this.state.minNumberOfPages}
			          	sliderStyle={{ marginTop : 10, marginBottom : 20 }}
			          	onDragStop={this._setMinNumberOfPages}
			          	onChange={this._setStateMinNumberOfPages}
			        />

			        <p>Maximum number of page visits: {this.state.maxNumberOfPages}</p>

				    <Slider
			        	min={0}
			        	max={5}
			          	step={1}
			          	defaultValue={1}
			          	value={this.state.maxNumberOfPages}
			          	sliderStyle={{ marginTop : 10, marginBottom : 20 }}
			          	onDragStop={this._setMaxNumberOfPages}
			          	onChange={this._setStateMaxNumberOfPages}
			        />

					<div className="clr"></div>

					<p>Minimum visit duration (minutes): {this.state.minSessionDuration}</p>

				    <Slider
			        	min={0}
			        	max={5}
			          	step={1}
			          	defaultValue={1}
			          	sliderStyle={{ marginTop : 10, marginBottom : 20 }}
			          	onChange={this._setStateMinSessionDuration}
			          	value={this.state.minSessionDuration}
			          	onDragStop={this._setMinSessionDuration}
			        />

				</div>
			);
		}
	},

	_setStateMinSessionDuration : function( event, newValue ) {

		this.setState({

			minSessionDuration : newValue
		})
	},

	_setStateMinNumberOfPages : function( event, newValue ) {

		this.setState({

			minNumberOfPages : newValue
		})
	},

	_setMinSessionDuration : function( event ) {

		FilterActions.editFilter({

			id 				: 	0,
			storedValue 	: 	{

				id 			: 	0,
				name 		: 	0,
				value 		: 	this.state.minSessionDuration,
	 			textValue 	: 	this.state.minSessionDuration
			},
			type 			: 	'minSessionDuration',
			round 			: 	0

		});
	},

	_setMaxNumberOfPages : function( event ) {

		let filter = {

			id 				: 	0,
			storedValue 	: 	{

				id 			: 	0,
				name 		: 	0,
				value 		: 	this.state.maxNumberOfPages,
	 			textValue 	: 	this.state.maxNumberOfPages
			},
			type 			: 	'maxNumberOfPages',
			round 			: 	0
		}

		FilterActions.editFilter( filter );
	},

	_setStateMaxNumberOfPages : function( event, newValue ) {

		this.setState({

			maxNumberOfPages : newValue
		})
	},

	_setMinNumberOfPages : function( event ) {

		FilterActions.editFilter({

			id 				: 	0,
			storedValue 	: 	{

				id 			: 	0,
				name 		: 	0,
				value 		: 	this.state.minNumberOfPages,
	 			textValue 	: 	this.state.minNumberOfPages
			},
			type 			: 	'minNumberOfPages',
			round 			: 	0

		});
	},

	_setOrderBy : function( event, key, value ) {

		FilterActions.setOrderBy( value );
	},

	_bankResults : function() {

		if( confirm( "This will reset filters and combine existing results with new filters... proceed?" ) ) {

			FilterActions.bankResults();
		}
	},

	_resetFilters : function() {

		FilterActions.resetFilters();
	},

	_updateResults : function() {

		FilterActions.updateResults();
	},

	_quickOpenSaveTemplate : function() {

		FilterActions.setQuickOpen({

			type 	: 	'reportWizard',
			tab 	: 	2
		});
	},

	_quickOpenExport : function() {

		FilterActions.setQuickOpen({

			type 	: 	'reportWizard',
			tab 	: 	1
		})
	},

	_setResultsPerPage : function( event, key, value ) {

		FilterActions.setPaginationReturnAmount( value );
	},

	_onChange: function() {

		var pagination  = FiltersStore.getPaginationTotals();
		
		this.setState({

			pagination 	: 	pagination
		})
	}
});

export default  PaginationOptions;