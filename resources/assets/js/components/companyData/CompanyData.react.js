import React from 'react';
import FilterActions from '../../actions/FilterActions.js';
import FiltersStore from '../../stores/FiltersStore.js';
import Breakdown from './Breakdown.react';
import ClientTypes from '../prospecting/ClientTypes.react';

import AdditionalCompanyData from './AdditionalCompanyData.react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState( props ) {

    return props.companyData;
}

var CompanyData = React.createClass({

	getInitialState : function() {

        return getCurrentState( this.props );

		
	},

    shouldComponentUpdate : function( nextProps, nextState ) {

        if( !this.state.companyData.set ||

            this.props.companyData === nextProps.companyData  
        
        ) {

            return false;
        }

        return true;
    },

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

    closeCompanyData : function() {

        FilterActions.closeLightbox({

            type    :   'companyData',
            id      :   0
        })
    },

    render : function() {

    	const actions = [

            <FlatButton
                label="Close"
                secondary={true}
                onTouchTap={this.closeCompanyData} />
        ];

        if( !this.state.companyData.set ) {

            return ( <div></div> );
        }


        var orgName     = this.state.companyData.use_organisation_name;
        var orgTel      = this.state.companyData.use_organisation_tel;
        var orgWebsite  = this.state.companyData.use_organisation_website;
        var orgLinkedIn = this.state.companyData.use_organisation_linked_in_address;

    	return(

    		<div className="companyData">

				<Dialog
		            title="Intelligence"
		            modal={false}
		            actions={actions}
		            open={this.state.open}
		            onRequestClose={this.closeCompanyData}
                    autoScrollBodyContent={true}
		        >
                    <AdditionalCompanyData availableData={this.state.dueDilData} />
                      
		        </Dialog>
			</div>
    	);
    }
});

export default  CompanyData;