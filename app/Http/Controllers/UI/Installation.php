<?php

declare(strict_types=1);

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

final class Installation extends Controller
{
    public function install(): Factory|View|Application
    {
        return view('UI/install');
    }

    public function createJSONConfigFile(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required|file|mimes:jpeg,jpg,png',
            'code' => 'required|agentCodeCorrect'
        ]);

        $fileType = $request->file('logo')->getMimeType();

        $extensions = [
            'image/jpeg' => 'jpeg',
            'image/jpg'  => 'jpg',
            'image/png'  => 'png'
        ];

        if (isset($extensions[strtolower($fileType)])) {
            $ext = $extensions[strtolower($fileType)];
        }

        $logoFile = 'agent-logo.' . $ext;

        $request->file('logo')->move('images', $logoFile);
        $agentID = $this->uploadLogoFileToAPIServer($logoFile, $request->input('code'));

        $config = [
            'code'     => $request->input('code'),
            'logoFile' => $logoFile,
            'agentID'  => $agentID
        ];

        $json = json_encode($config);

        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/../a1config.json', 'w');
        fwrite($fp, $json);
        fclose($fp);

        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/../.env', 'wb');
        fwrite($fp, $this->getEnvContents());
        fclose($fp);

        $request->session()->flash('success', 'Thank you, installation completed successfully');

        return redirect('/');
    }

    private function uploadLogoFileToAPIServer($logoFile, $agentCode): int
    {
        $multipart = [
            'multipart' => [
                [
                    'name'     => 'logo',
                    'contents' => fopen($_SERVER['DOCUMENT_ROOT'] . '/images/' . $logoFile, 'r'),
                ],
                [
                    'name'     => 'code',
                    'contents' => $agentCode,
                ],
                [
                    'name'     => 'user_id',
                    'contents' => 1726, // hard coded to Andy Harris for now
                ],
            ]
        ];

        $client   = new Client();
        $response = $client->request('POST', env('API_SOURCE') . '/agents/update-config', $multipart);
        $response = $response->getBody()->getContents();
        $decoded  = json_decode($response, true);

        return $decoded['agentID'] ?? 0;
    }

    private function getEnvContents()
    {
        return "APP_ENV=staging\n" .
            "APP_DEBUG=FALSE\n" .
            "APP_KEY=" . $this->generateRandomString(32) . "\n" .
            "API_SOURCE=https://api1.websuccess-data.com";
    }

    private function generateRandomString($length = 10)
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
