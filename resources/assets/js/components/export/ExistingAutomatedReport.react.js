import React from 'react';
import FiltersStore	from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import DownloadTypes from './DownloadTypes.react';
import TeamMembersSelect from '../TeamMembersSelect.react';
import SelectIncludeFields from './SelectIncludeFields.react';
import Checkbox from 'material-ui/Checkbox';
import {Tabs, Tab} from 'material-ui/Tabs';

const styles = {
  	
  	headline: {
	 
	    fontSize 		: 24,
	    paddingTop 		: 16,
	    marginBottom 	: 20,
	    fontWeight 		: 400
	}
};

function twoDigits(d) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlTime = function() {

	return twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds() ); 
}

Date.prototype.toMysqlFormat = function() {

    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
};

function convertMysqlDateToJSDate( mysqlDate ) {

	var t = mysqlDate.split(/[- :]/);

	return new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
}

function convertMysqlTimeToJSDate( mysqlTime ) {

	var date = new Date();

	var dateString = date.getFullYear() + '-' + twoDigits( 1 + date.getMonth() ) + '-' + twoDigits( date.getSeconds() ); 

	dateString = dateString + ' ' + mysqlTime;

	var returnDate = convertMysqlDateToJSDate( dateString );

	return returnDate;
}



var ExistingAutomatedReport = React.createClass({

	getInitialState : function() {

		var defaultTimeFrom 	= 	new Date();
		defaultTimeFrom.setHours( 0 );
		defaultTimeFrom.setMinutes( 0 );
		defaultTimeFrom.setSeconds( 0 );
			
		var automationSettings 	=	this.props.exportOptions.existingAutomatedReport;
		var allTeamMembers 		=	this.props.exportOptions.teamMembers;
		var exportCriteria 		= 	this.props.exportOptions.include;
		var reportData 			=	false;

		return {

			reportData,
			showFullURLOnEntryPage 	: 	false,
			exportCriteria 			: 	exportCriteria,
			existingReportData 		: 	automationSettings,
			templateId				: 	0,
			title 					: 	'',
			fireDate 				: 	new Date(),
			fireTime 				: 	new Date(),
			open 					: 	this.props.exportOptions.existingAutomatedReportOpen,
			repeat 					: 	0,
            teamMembers 			:   FiltersStore.getTeamMembers(),
            includeFields 			: 	[],
            exportCSV 				: 	false,
            exportPDF 				: 	false,
            reportId 				: 	0,
            settings 				: 	this.props.settings,
            exportOptions 			: 	this.props.exportOptions
		}
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	handleClose : function() {

    	FilterActions.closeLightbox();
    },

    componentWillReceiveProps : function( newProps ) {

		var automationSettings 	=	newProps.exportOptions.existingAutomatedReport;

		var allTeamMembers 		=	newProps.exportOptions.teamMembers;
		var exportCriteria 		= 	newProps.exportOptions.include;


		if( automationSettings.data ) {

			var reportData 			=	automationSettings.data.report;

			this.setState({	

				reportData,
				showFullURLOnEntryPage 	: 	reportData.show_full_urls_on_entry_pages,
				exportCriteria 			: 	exportCriteria,
				settings 				: 	newProps.settings,
				exportOptions 			: 	newProps.exportOptions,
				existingReportData 		: 	automationSettings,
				templateId 				: 	reportData.report_template_id,
				title 					: 	reportData.name,
				fireDate 				: 	convertMysqlDateToJSDate( reportData.first_fire ),
				fireTime 				: 	convertMysqlTimeToJSDate( reportData.fire_time ),
				open 					: 	newProps.exportOptions.existingAutomatedReportOpen,
				repeat 					: 	reportData.repeat,
				includeFields 			: 	reportData.include_fields,
				teamMembers 			: 	automationSettings.data.users,
				allTeamMembers 			: 	allTeamMembers,
				csvChecked 				: 	newProps.exportOptions.include.exportTypes.CSV,
				pdfChecked 				: 	newProps.exportOptions.include.exportTypes.PDF,
				reportId 				: 	automationSettings.data.reportId
			})
		}
    },

	formatDate  :   function( d ) {

        return d.getFullYear() + '-' + ( "0" + ( d.getMonth() + 1 ) ).slice( -2 ) + '-' + ("0" + d.getDate()).slice(-2)
    },

    getInitialDate : function() {

        return new Date();
    },

    selectTeamMembers 	: 	function( teamMembers ) {



    },

    setRepeat : function( event ) {

    	this.setState({

    		repeat 	: 	event.target.value
    	})
    },

    setTitle : function( event ) {

    	this.setState({

    		title 	: 	event.target.value
    	})
    },

    selectTab : function() {


    },

	render : function() {


        const actions = [

            <FlatButton
                label="Cancel"
                secondary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Update Report"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._setAutomationDetails}
            />
        ];

        const showFullURLOnEntryPage = ( typeof this.state.showFullURLOnEntryPage === "undefined" )
        	? false : Boolean( this.state.showFullURLOnEntryPage );

		return (

			<Dialog
	        	title="Edit Automated Report"
	        	modal={false}
	        	actions={actions}
	        	autoScrollBodyContent={true}
	        	open={this.state.open}
	        	onRequestClose={this.handleClose} 
                autoScrollBodyContent={true}
	        >	
	        	<form>

		        	<Tabs>

					    <Tab label="Schedule">

					    	<div>

					    		<h4 style={styles.headline}>Schedule</h4>

				        		<div className="form-group">

				        			<label>Report Title</label>
				        			<input type="text" value={this.state.title} className="form-control" onChange={this.setTitle} />
				        		</div>

				        		<div className="form-group">

				        			<label>Initial Delivery Date</label>

				        			<DatePicker 
				        
				                    	formatDate={this.formatDate} 
				                    	name="deliveryDate"
				                    	id="deliveryDate"
				                    	defaultDate={this.state.fireDate}
				                    	onChange={this._updateDate}  />
				        		</div>


				        		<div className="form-group">

				        			<label>Repeat Delivery</label>

				        			<select onChange={this.setRepeat} value={this.state.repeat} className="form-control">
				        				<option value="0">Never</option>
				        				<option value="1">Every Hour</option>
				        				<option value="2">Every Day</option>
				        				<option value="3">Every Week</option>
				        				<option value="4">Every Month</option>
				        				<option value="5">Every Year</option>
				        			</select>

				        		</div>

				        		<div className="form-group">

				        			<label>Delivery Time</label>

				            		<TimePicker 
					    				ref="picker24hr"
					    				defaultTime={this.state.fireTime}
					          			format="24hr"
					          			id="deliveryTime"
					          			name="deliveryTime"
					          			hintText="Delivery Time"
					          			onChange={this._handleChangeTimePicker} />

					          	</div>
					        </div>
						</Tab>

						<Tab label="Send To">

							<div>

								<div className="form-group">

				                    <TeamMembersSelect existingReport={true} includeTeamMembers={this.state.teamMembers}  teamMembers={this.state.allTeamMembers} />

				        		</div>
				        	</div>
						</Tab>

						<Tab label="Options">
							<div>
					    		<div className="clr"></div><br />

								<Checkbox 
									onCheck={ ( event, isChecked ) => this._updateAutomatedReport( isChecked, "show_full_urls_on_entry_pages" ) }
									checked={showFullURLOnEntryPage}
									label="Show full URL on Entry Page" />

						    	<h2>Export Formats</h2>

								<div className="form-group">
				                    <div className="clr"></div>
				                    <DownloadTypes types={this.getDownloadTypes} csvChecked={this.state.csvChecked} pdfChecked={this.state.pdfChecked} />
				                </div>
				            </div>
						</Tab>

						<Tab label="Include Fields">

							<h4 style={styles.headline}>Include Fields</h4>
							
							<SelectIncludeFields editing={true} data={this.state.includeFields} />

						</Tab>
					</Tabs>
	        	</form>

	        </Dialog>
	    );
	},

	_updateAutomatedReport : function( newVal, field ) {

		let automatedReport 	= this.state.reportData;
		automatedReport[field] 	= newVal;
		FilterActions.updateExistingAutomatedReportInStoreOnly( automatedReport );
	},

	_handleChangeTimePicker : 	function( e, newTime ) {

		this.setState({

			fireTime 	: 	newTime
		})
	},

	_updateDate : function( e, date ) {

		this.setState({

            fireDate  :   date
        })
	},

    _setAutomationDetails : function() {

    	FilterActions.updateAutomatedDelivery({

    		templateId 	: 	this.state.templateId,
    		exportCSV 	: 	this.state.exportCSV,
    		exportPDF 	: 	this.state.exportPDF,
    		fireDate 	: 	this.state.fireDate,
    		fireTime 	: 	this.state.fireTime,
    		repeat 		: 	this.state.repeat,
    		title 		: 	this.state.title,
    		reportId 	: 	this.state.reportId

    	});
    }
});

export default  ExistingAutomatedReport;
