import React from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

function getCurrentState( props ) {

	return {

		updating 	: 	props.updating
	}
}

var Updating = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		if( !this.state.updating.updating ) {

			return <div></div>
		}

		return (

			<div>

				<div className="clr"></div><br /><br />

				<div className="row">

					<div className="col-md-1">
						<div style={style.container}>
							<RefreshIndicator
								size={40}
								left={10}
								top={0}
								status="loading"
								style={style.refresh}
							/>
						</div>
					</div>
					<div className="col-md-10">
						{this.state.updating.message}
					</div>
				</div>
			</div>

		)
	}
});

export default Updating;