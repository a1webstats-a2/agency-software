import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import AddTeamMember from './AddTeamMember.react';
import FilterActions from '../../actions/FilterActions';
import TeamMember from './TeamMember.react';
import EditTeamMember from './EditTeamMember.react';
import NewTeamMember from './NewTeamMember.react';

function getCurrentState(props) {
    return {
        user: props.user,
        open: false,
        newTeamMember: props.newTeamMember,
        teamMembers: props.teamMembers,
        newTeamMemberOpen: props.newTeamMemberOpen
    };
}

const Dashboard = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps, newState) {
        this.setState(getCurrentState(newProps));
    },
    handleOpen: function () {
        this.setState({open: true});
    },
    handleClose: function () {
        this.setState({open: false});
    },
    setNewTeamMember: function (newMember) {
        this.setState({newTeamMember: newMember});
    },
    render: function () {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._addTeamMember}
            />,
        ];
        const allowedSuperUserLevels = [4, 5];
        let isSuperUser = (allowedSuperUserLevels.indexOf(this.state.user.user_type_id) > -1) ? true : false;
        var teamMembers = this.state.teamMembers.map(function (teamMember, i) {
            return <TeamMember user={this.state.user} isSuperUser={isSuperUser} data={teamMember} key={i}/>
        }.bind(this));

        return (
            <div className="row">
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-12">
                            <h3>Team</h3><br/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <br/>
                            <Table>
                                <TableHeader>
                                    <TableRow>
                                        <TableHeaderColumn>Name</TableHeaderColumn>
                                        <TableHeaderColumn>Actions</TableHeaderColumn>
                                    </TableRow>
                                </TableHeader>

                                <TableBody>
                                    {teamMembers}
                                </TableBody>

                            </Table>

                            <div className="clr"></div>
                            <br/>
                            <br/><br/>

                            <RaisedButton disabled={!isSuperUser} label="Add Team Member" primary={true}
                                          onTouchTap={this.handleOpen}/>
                        </div>
                    </div>
                    <Dialog
                        title="Add Team Member"
                        actions={actions}
                        modal={false}
                        open={this.state.open}
                        onRequestClose={this.handleClose}
                    >
                        <br/><br/>
                        <AddTeamMember newTeamMember={this.setNewTeamMember}/>
                    </Dialog>
                    <NewTeamMember
                        open={this.state.newTeamMemberOpen}
                        newTeamMember={this.state.newTeamMember}
                    />
                    <EditTeamMember/>
                    <div className="clr"></div>
                    <br/><br/><br/><br/>
                </div>
            </div>
        );
    },
    _addTeamMember: function () {
        FilterActions.createNewTeamMember(this.state.newTeamMember);
        this.setState({open: false});
    }
});
export default Dashboard;
