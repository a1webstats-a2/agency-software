@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>


@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm" class="signupForm">

				<div id="loginLogo">
                    <img src="/images/{{ $loginLogo }}" />
                </div>

				<div class="clr"></div>

				<br /><br />


				<h1>Migration</h1>

				@if ( count( $errors ) > 0 )
					<div class="alert alert-danger">
					    <ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
				@endif

				<h2>Set New Password</h2>

				<div class="row">

					<div class="col-md-12">

						<form method="POST">

							<div class="form-group">

								<label >New Password<span>*</span></label>

								<input type="password" class="form-control" value="{{ old( 'password' ) }}" name="password" />

							</div>

							<div class="form-group">

								<label >Repeat Password<span>*</span></label>

								<input type="password" class="form-control" value="{{ old( 'repeat_password' ) }}" name="repeat_password" />

							</div>

							<div className="clr"></div><br />

							<button class="btn btn-primary">Set Password</button>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection