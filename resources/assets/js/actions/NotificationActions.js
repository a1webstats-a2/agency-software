import NotificationsDispatcher 	from '../dispatcher/NotificationsDispatcher';
import NotificationConstants 	from '../constants/NotificationConstants';

var NotificationActions = {

	openNotifications : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.OPEN_NOTIFICATIONS
		})

	},

	deleteIndividual : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.DELETE_INDIVIDUAL
		})

	},

	backToAllMessages : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.BACK_TO_ALL
		})
	},

	viewNotification : function( notificationID ) {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.VIEW_NOTIFICATION,
			data 		: notificationID
		})
	},

	deleteSelection : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.DELETE_SELECTION
		})
	},

	archiveSelection : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.ARCHIVE_SELECTION

		})
	},

	setNotificationsViewType : function( type ) {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.SET_NOTIFICATION_VIEW_TYPE,
			data 		: type

		})
	},

	setNotificationSelection : function( selection ) {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.SET_NOTIFICATION_SELECTION,
			data 		: selection

		})
	},

	closeNotifications : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.CLOSE_NOTIFICATIONS
		})

	},


	deleteNotification : function( notificationId ) {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.DELETE_NOTIFICATION,
			data 		: notificationId

		})
	},

	archiveNotification : function( notification ) {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.ARCHIVE_NOTIFICATION,
			data 		: notification

		})
	},

	storeNotifications : function( notifications ) {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.STORE_NOTIFICATIONS,
			data 		: notifications

		})
	},

	create : function( messageObj ) {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.ADD_NOTIFICATION,
			data 		: messageObj

		})
	},

	discardNotifications : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.DISCARD_NOTIFICATIONS

		})
	},

	setInitialNotifications : function() {

		NotificationsDispatcher.dispatch({

			actionType 	: NotificationConstants.LOAD_INITIAL_NOTIFICATIONS

		})
	}

}

export default NotificationActions;