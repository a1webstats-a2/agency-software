import CompanyTrackerDispatcher from '../dispatcher/CompanyTrackerDispatcher';
import CompanyTrackerConstants 	from '../constants/CompanyTrackerConstants';

var TrackedCompaniesActions = {

 	storeTrackedOrganisations 	: 	function( companies ) {

 		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.STORE_TRACKED_ORGANISATIONS,
 			data 		: companies
 		})
 	},

	updateEndUserCompanyRecord 	: function( newState ) {
		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.UPDATE_END_USER_COMPANY_RECORD,
 			data 		: newState

 		});
	},

	trackerUpdateClientRelationship : function( relationship ) {

		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.TRACKER_UPDATE_CLIENT_RELATIONSHIP,
 			data 		: relationship

 		});
	},

	setPopover : function( popoverObj ) {

		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.SET_POPOVER,
 			data 		: popoverObj

 		});
	},

	trackCompaniesSaveAssignedTo : function() {

		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.TRACKED_SAVE_ASSIGNED_TO

 		});
	},

	setAssignedToOption : function( option ) {

		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.SET_ASSIGNED_TO_OPTION,
 			data 		: option

 		});
	},

	setCurrentTrackedObjAssignedToTeamMember : function( memberId ) {

		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.SET_ASSIGNED_TEAM_MEMBER_ID,
 			data 		: memberId

 		});
	},

	setTrackedOptions : function( recordKey ) {

		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.SET_TRACKED_OPTIONS,
 			data 		: recordKey

 		});
	},

 	createNewTrackedOrganisation : function( id ) {

 		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.CREATE_NEW_TRACKED_ORGANISATION,
 			data 		: id

 		});
 	},

 	setTrackedOrganisations : function() {

 		CompanyTrackerDispatcher.dispatch({

 			actionType 	: CompanyTrackerConstants.SET_TRACKED_ORGANISATIONS

 		})
 	}
};

export default TrackedCompaniesActions;