import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import TeamMemberCheckbox from './TeamMemberCheckbox.react';
import FilterActions from '../../actions/FilterActions';
import Subheader from 'material-ui/Subheader';
import Checkbox from 'material-ui/Checkbox';

function getCurrentState(props) {
    return {
        open: props.open,
        teamMembers: props.teamMembers,
        sendEmailToTeamMembers: props.sendEmailToTeamMembers
    }
}

var SendOrganisationViaEmail = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.sendEmailToTeamMembers !== nextProps.sendEmailToTeamMembers) {
            return true;
        }

        return this.props.open !== nextProps.open;
    },
    handleClose: function () {
        FilterActions.closeLightbox();
    },
    render: function () {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Send"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._sendEmail}
            />
        ];

        const teamMembers = this.state.teamMembers.map(function (teamMember, i) {
            const avatarSrc = `/api/proxy/api/v1/user/avatar/${teamMember.id}`;
            const name = `${teamMember.forename} ${teamMember.surname}`;

            return (
                <TeamMemberCheckbox
                    sendEmailToTeamMembers={this.state.sendEmailToTeamMembers} avatarSrc={avatarSrc}
                    name={name}
                    data={teamMember}
                    key={i}/>
            );
        }.bind(this));

        return (
            <div>
                <Dialog
                    title="Send Organisation via Email"
                    actions={actions}
                    modal={false}
                    autoScrollBodyContent={true}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    <p>Note to user:</p>
                    <textarea
                        className="w-full h-48 border-2 border-indigo-800"
                        onChange={this._setOrganisationByEmailText}
                    />
                    <div className="clr"></div>
                    <br/>

                    <Checkbox
                        label="Attach visit data spreadsheet"
                        defaultChecked={false}
                        onCheck={this._setSendOrganisationByEmailAttachSpreadsheet}
                    />

                    <List className="teamMembersSelect">
                        <Subheader>Team Members</Subheader>
                        {teamMembers}
                    </List>
                </Dialog>
            </div>
        )
    },
    _setSendOrganisationByEmailAttachSpreadsheet: function (event, isChecked) {
        FilterActions.setSendOrganisationByEmailAttachSpreadsheet(isChecked);
    },
    _setOrganisationByEmailText: function (event) {
        FilterActions.setOrganisationByEmailText(event.target.value);
    },
    _sendEmail: function () {
        FilterActions.fireOffOrganisationEmail();
    }
});

export default SendOrganisationViaEmail;
