import React 					from 'react';
import Dropzone 				from 'react-dropzone';
import ExistingAvatar 			from './ExistingAvatar.react';
import FilterActions 			from '../../actions/FilterActions';
import FiltersStore 			from '../../stores/FiltersStore';
import Paper 					from 'material-ui/Paper';
import Toggle 					from 'material-ui/Toggle';
import SelectField 				from 'material-ui/SelectField';
import MenuItem 				from 'material-ui/MenuItem';
import moment 					from 'moment';
import Checkbox 				from 'material-ui/Checkbox';
import CommunicationPreferences from './CommunicationPreferences.react';


function getCurrentState( props ) {

	return {

		displayFullURLs 					: 	( typeof props.settings.display_full_urls === "undefined" || 
													!props.settings.display_full_urls || 
													parseInt( props.settings.display_full_urls ) === 0 ) ? false : true,
		communicationPreferences 			: 	FiltersStore.getUserCommunicationPreferences(),
		avatarIsSet 						: 	FiltersStore.isAvatarSet(),
		settings 							: 	props.settings,
		displayPageVisitsByDefault			: 	( typeof props.settings.display_page_visits_by_default === "undefined"  ||
													parseInt( props.settings.display_page_visits_by_default ) === 1 ) ? true : false,
		preferredDateFormat 				: 	( typeof props.settings.preferred_date_format === "undefined" ) ? "yyyy-mm-dd" : props.settings.preferred_date_format,
		uploadingAvatar 					: 	FiltersStore.getUploadAvatarStatus(),
		enableDesktopNotifications 			: 	( typeof props.settings.enable_desktop_notifications === "undefined" ) ? false : !!+parseInt( props.settings.enable_desktop_notifications ),
		enableDesktopNotificationsByDefault : 	( typeof props.settings.enable_desktop_notifications === "undefined"  ||
													parseInt( props.settings.enable_desktop_notifications ) === 1 ) ? true : false
	}
}

const styles = {
 
  	toggle 	: {
    	marginBottom 	: 16,
  	},

  	paper : {

  		width		: '95%',
  		padding 	: 20,
  		display 	: 'inline-block'
  	}
};

var UserSettings = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentDidMount : function() {

		FilterActions.loadCommunicationPreferences();
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},	

	render : function() {

		var errorMsg = "";

		if( this.state.uploadingAvatar.error ) {

			errorMsg = (

				<div className="row">
					<div className="col-md-12">
						<div className="alert alert-danger">
							{this.state.uploadingAvatar.error}
						</div>
					</div>
				</div>
			)
		}

		if( typeof this.state.enableDesktopNotifications === "undefined" ) {

			var enableDesktopNotifications = false;
		
		} else {

			var enableDesktopNotifications = this.state.enableDesktopNotifications;
		}

        const exampleddmmyyyy 	= 	moment().format( 'DD/MM/YYYY' );
	    const examplemmddyyyy 	=	moment().format( 'MM/DD/YYYY' );
        const exampleyyyymmdd 	= 	moment().format( 'YYYY/MM/DD' ); 
        const examplefulltext 	= 	moment().format( 'Do MMMM YYYY' );

		const items = [

			<MenuItem key={1} value="dd/mm/yyyy" primaryText={exampleddmmyyyy} />,
			<MenuItem key={2} value="mm/dd/yyyy" primaryText={examplemmddyyyy} />,
			<MenuItem key={3} value="yyyy-mm-dd" primaryText={exampleyyyymmdd} />,
			<MenuItem key={4} value="fulltext" primaryText={examplefulltext} />
		];


		return (

			<div className="row">
						
				<div className="col-md-12">

					<div className="row">
						
						<div className="col-md-12">
							
							<h3>User Settings</h3><br />
						</div>

					</div>

					{errorMsg}



					<div className="row">

						<div className="col-md-4">

							<label>Avatar (max 2MB)</label><br /><br />

							<div id="dropzoneContainer">
								<Dropzone ref="dropzone" onDrop={this._onDrop}>
					            	<div style={{ padding : 10 }}>Drop your avatar picture here</div>
					            </Dropzone>
					        </div>

							<label>Current Image</label>
							<div className="clr"></div><br />

							<ExistingAvatar status={this.state.uploadingAvatar} isSet={this.state.avatarIsSet} />

							<div className="clr"></div><br /><br />

							<Toggle
								label="Display page visits by default"
								style={styles.toggle}
								toggled={this.state.displayPageVisitsByDefault}
								onToggle={this._setDisplayPageVisitsByDefault}
							/>

							<div className="clr"></div><br />

							<Toggle
								label="Enable Desktop Notifications"
								style={styles.toggle}
								toggled={enableDesktopNotifications}
								onToggle={this._setEnableDesktopNotifications}
							/>

							<div className="clr"></div><br />

							<Toggle
								label="Display complete URL"
								style={styles.toggle}
								toggled={this.state.displayFullURLs}
								onToggle={ ( event, isToggled ) => this._setDisplayFullURL( isToggled ) }
							/><br />

							<div className="clr"></div><br /><br />

							<div id="datePreferenceSettings">

								<SelectField 
									floatingLabelText="Preferred Date Format"
									value={this.state.preferredDateFormat.trim()}
									onChange={this._updateDatePreference}>
									{items}
								</SelectField>
							</div>

							<div className="clr"></div><br /><br /><br /><br />
						</div>
					</div>
				</div>
			</div>
		)
	},

	_setDisplayFullURL : function( isToggled ) {

		var newSettings = this.state.settings;

		if( !newSettings ) {

			newSettings = {};
		}

		newSettings.display_full_urls = isToggled;

		FilterActions.updateUserSettings( newSettings );
	},

	_updateDatePreference : function( event, key, payload ) {

		var newSettings = this.state.settings;

		if( !newSettings ) {

			newSettings = {};
		}

		newSettings.preferred_date_format = payload;

		FilterActions.updateUserSettings( newSettings );

	},

	_setEnableDesktopNotifications : function() {

		var enabled 	= ( this.state.enableDesktopNotifications ) ? 0 : 1;

		var newSettings = this.state.settings;

		if( !newSettings ) {

			newSettings 	=	{};
		}

		newSettings.enable_desktop_notifications = enabled;

		FilterActions.updateUserSettings( newSettings );
	},

	_setDisplayPageVisitsByDefault : function( ) {

		var display 	= ( this.state.displayPageVisitsByDefault ) ? 0 : 1;

		var newSettings = this.state.settings;

		if( !newSettings ) {

			newSettings 	=	{};
		}

		newSettings.display_page_visits_by_default = display;

		FilterActions.updateUserSettings( newSettings );
	},

	_onDrop : function( file ) {

		FilterActions.uploadAvatar( file );
      	
	}
});

export default  UserSettings;