import React 					from 'react';
import ScreenRecorderStore 		from '../../stores/ScreenRecorderStore';
import ScreenRecorderActions 	from '../../actions/ScreenRecorderActions';
import ReactHeatmap  			from 'react-heatmap';
import NavBar 					from '../navigation/NavBar.react';
import FiltersStore  			from '../../stores/FiltersStore';
import NotificationsStore 		from '../../stores/NotificationsStore';
import {RadioButton, 
	RadioButtonGroup} 			from 'material-ui/RadioButton';

function getCurrentState( props ) {

	return {

		heatmapTypeToView 			: 	ScreenRecorderStore.getHeatmapTypeToView(),
		heatmapData 				: 	ScreenRecorderStore.getHeatmapData(),
		notificationsData 			: 	{
	
			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),		
			accountOK 				: 	FiltersStore.accountIsOK(),
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()
		}
	}
}

class HeatmapContainer extends React.Component {

	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );
		this._onChange 	= this._onChange.bind( this );

	}

	componentDidMount() {

		ScreenRecorderStore.addChangeListener( this._onChange );
		const queryParams = window.location.pathname.split( '/' );
		ScreenRecorderActions.loadHeatmap( queryParams[2] );
	}

	componentWillUnmount() {

		ScreenRecorderStore.removeChangeListener( this._onChange );
	}

	render() {

		let img = "";

		if( this.state.heatmapData.screenshot ) {

			const imgSrc = '/api/proxy/api/v1/screenshot/image/' + this.state.heatmapData.screenshot.image;

			img = (

				<img 
					src={imgSrc} 
					style={{ width : 800, top : 0, position : 'absolute', zIndex : 1 }} />

			)
		}

		const data = this.state.heatmapData.heatmapData[this.state.heatmapTypeToView];
			
		let contentDisplay = "";

		if( !this.state.heatmapData.screenshot ) {

			contentDisplay = (

				<div style={{ width : 800 }}>
					<div style={{ width : 800, height : 1000, top : 50, position : 'absolute' }}>

						<h2>Loading</h2>
					</div>
				</div>
			)

		} else {

			const heatmapHeight = this.state.heatmapData.screenshot.height;

			contentDisplay = (

				<div style={{ width : 800 }}>
					<div style={{ width : 800, height : heatmapHeight, top : 50, position : 'absolute' }}>
						<ReactHeatmap data={data} style={{ zIndex : 2 }} unit="pixels" />
						{img}
					</div>
				</div>
			)

		}

		const tdStyle = { borderTop : 0 };

		return (

			<div>

				<NavBar data={this.state.notificationsData} />

				<div className="clr"></div>

				<div className="container mainContainer">

					<div className="clr"></div><br /><br />

					<div className="row">

						<div className="col-md-3">

							<div style={{ marginTop : 50 }}>

								<h3>Heatmap</h3>

								<div className="clr"></div><br /><br />
									
								<table className="table">
									<tbody>
										<tr>
											<td style={tdStyle}><strong>URL</strong></td>
										</tr>
										<tr>
											<td style={tdStyle}>{this.state.heatmapData.visitData.url}</td>
										</tr>
										<tr>
											<td style={tdStyle}><strong>No. Unique Visitors</strong></td>
										</tr>
										<tr>
											<td style={tdStyle}>{this.state.heatmapData.visitData.count}</td>
										</tr>
										<tr>
											<td style={tdStyle}><strong>No. Clicks</strong></td>
										</tr>
										<tr>
											<td style={tdStyle}>{this.state.heatmapData.clickTotals}</td>
										</tr>
									
									</tbody>
								</table>

								<div className="clr"></div>

								<RadioButtonGroup 
									onChange={ ( event, val ) => this._setDisplayType( val ) }
									style={{ marginTop : 50}}
									valueSelected={this.state.heatmapTypeToView}
									name="displayType" >
									<RadioButton
										value="hover"
										label="Hover"
									/>
									<RadioButton
										value="click"
										label="Clicks"
									/>
									<RadioButton
										value="scrollTo"
										label="Scroll To"
									/>
								</RadioButtonGroup>
							</div>
						</div>

						<div className="col-md-9">
							
							{contentDisplay}
							

						</div>
					</div>
					<div className="clr"></div><br /><br /><br /><br /><br />

				</div>
			</div>
		)
	}

	_setDisplayType( val ) {

		ScreenRecorderActions.setDisplayType( val );
	}

	_onChange() {

		this.setState( getCurrentState() );
	}
}

export default HeatmapContainer;