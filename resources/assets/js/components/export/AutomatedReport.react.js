import React from 'react';
import FiltersStore	from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import DownloadTypes from './DownloadTypes.react';
import TeamMembersSelect from '../TeamMembersSelect.react';
import {Tabs, Tab} from 'material-ui/Tabs';
import SelectIncludeFields from './SelectIncludeFields.react';
import Checkbox from 'material-ui/Checkbox';

const styles = {
  	
  	headline: {
	 
	    fontSize 		: 24,
	    paddingTop 		: 16,
	    marginBottom 	: 20,
	    fontWeight 		: 400
	}
};

var AutomatedReport = React.createClass({

	getInitialState : function() {

		var defaultTimeFrom 	= 	new Date();
		defaultTimeFrom.setHours( 0 );
		defaultTimeFrom.setMinutes( 0 );
		defaultTimeFrom.setSeconds( 0 );
		
		return {

			exportCriteria 		: 	this.props.exportOptions.include,
			automationSettings 	: 	this.props.exportOptions.automationSettings,
			templateId			: 	0,
			title 				: 	'',
			fireDate 			: 	new Date(),
			fireTime 			: 	new Date(),
			include 			: 	this.props.exportOptions.include,
			open 				: 	this.props.open,
			repeat 				: 	0,
			sendToAll 			: 	this.props.exportOptions.include.sendToAll,
			exportCSV 			: 	this.props.exportOptions.include.exportTypes.CSV,
			exportPDF 			: 	this.props.exportOptions.include.exportTypes.PDF,
            teamMembers 		:   this.props.exportOptions.teamMembers,
            includeFields 		: 	this.props.exportOptions.include.include,
            preSelected 		: 	[],
            tabSelected 		: 	0
		}
	},

	handleClose : function() {

    	FilterActions.closeLightbox();

    },

    shouldComponentUpdate : function( nextProps, nextState ) {

    	return true;
    },

    componentWillReceiveProps : function( newProps ) {

		var newState = {

			exportCSV 			: 	newProps.exportOptions.include.exportTypes.CSV,
			exportPDF 			: 	newProps.exportOptions.include.exportTypes.PDF,
			automationSettings 	: 	newProps.exportOptions.automationSettings,
			include 			: 	newProps.exportOptions.include,
			open 				: 	newProps.open,
            teamMembers  		:   newProps.exportOptions.teamMembers,
			exportCriteria 		: 	newProps.exportOptions.include,
            includeFields 		: 	newProps.exportOptions.include.include,
            sendToAll 			: 	newProps.exportOptions.include.sendToAll
		}

		if( newState.automationSettings.template ) {

			//state.exportCriteria = newProps.exportOptions.include;
			//state.templateId  	 = state.automationSettings.template.template.id;
		}

		this.setState( newState );
	
    },

	formatDate  :   function( d ) {

        return d.getFullYear() + '-' + ( "0" + ( d.getMonth() + 1 ) ).slice( -2 ) + '-' + ("0" + d.getDate()).slice(-2)
    },

    getInitialDate : function() {

        return new Date();
    },

    selectTeamMembers 	: 	function( teamMembers ) {



    },

    setRepeat : function( event ) {

    	this.setState({

    		repeat 	: 	event.target.value
    	})
    },

    setTitle : function( event ) {

    	this.setState({

    		title 	: 	event.target.value
    	})
    },

    selectTab : function() {


    },

	render : function() {

		var automationSettings 	=	FiltersStore.getAutomationSettings();
		var initialDate 		= 	this.getInitialDate();

        var actions = [

            <FlatButton
                label="Cancel"
                secondary={true}
                onTouchTap={this.handleClose}
            />
        ];

        if( this.state.tabSelected < 3 ) {

        	actions.push(

	            <FlatButton
	                label="Next"
	                primary={true}
	                keyboardFocused={true}
	                onTouchTap={this._setNextTab}
	            />
        	)
        
        } else {

        	actions.push(

	            <FlatButton
	                label="Set Automation"
	                primary={true}
	                keyboardFocused={true}
	                onTouchTap={this._setAutomationDetails}
	            />
        	)
        }

        var open = ( typeof this.state.open !== "undefined" ) ? this.state.open : false;

		return (

			<Dialog
	        	title="Automate delivery of this report"
	        	modal={false}
	        	actions={actions}
	        	autoScrollBodyContent={true}
	        	open={open}
	        	onRequestClose={this.handleClose} 
	        >	


		        	<form >
			        	<Tabs value={this.state.tabSelected}>

						    <Tab label="Schedule" onActive={this._allowDisalloweSetAutomation} value={0}>

						    	<div>

						    		<h4 style={styles.headline}>Schedule</h4>

					        		<div className="form-group">

					        			<label>Report Title</label>

					        			<input type="text" className="form-control" onChange={this.setTitle} />
					        		</div>

					        		<div className="form-group">


					        			<label>Initial Delivery Date</label>

					        			<DatePicker 
					        				name="initialDeliveryDate"
					        				id="initialDeliveryDate"
					                    	formatDate={this.formatDate} 
					                    	defaultDate={initialDate}
					                    	onChange={this._updateDate}  />
					        		</div>


					        		<div className="form-group">

					        			<label>Repeat Delivery</label>

					        			<select onChange={this.setRepeat} value={this.state.repeat} className="form-control">
					        				<option value="0">Never</option>
					        				<option value="1">Every Hour</option>
					        				<option value="2">Every Day</option>
					        				<option value="3">Every Week</option>
					        			</select>

					        		</div>

					        		<div className="form-group">

					        			<label>Delivery Time</label>

					            		<TimePicker 
						    				ref="picker24hr"
						    				id="deliveryTime"
						    				defaultTime={this.state.fireTime}
						          			format="24hr"
						          			name="deliveryTime"
						          			hintText="Delivery Time"
						          			onChange={this._handleChangeTimePicker} />

						          	</div>

						        </div>
							</Tab>

							<Tab label="Send To" onActive={this._allowDisalloweSetAutomation} value={1}>

								<div style={{ height : 500, overflowY : 'scroll' }}>

						    		<h4 style={styles.headline}>Send To</h4>

									<div className="form-group">
	
					                    <TeamMembersSelect sendToAll={this.state.exportCriteria.sendToAll} existingReport={false} includeTeamMembers={this.state.include.includeTeamMembers} teamMembers={this.state.teamMembers} />

					        		</div>
					        	</div>
							</Tab>

							<Tab label="Options" onActive={this._allowDisalloweSetAutomation} value={2}>
								<div>
									
									<div className="clr"></div><br />

									<Checkbox 
										onCheck={ ( event, isChecked ) => this._updateExportOptions( isChecked, "showFullURLOnEntryPage" ) }
										checked={this.state.exportCriteria.showFullURLOnEntryPage}
										label="Show full URL on Entry Page" />

						    		<h2>Export Formats</h2>

									<div className="form-group">
					                    <div className="clr"></div>
										<DownloadTypes csvChecked={this.state.include.exportTypes.CSV} pdfChecked={this.state.include.exportTypes.PDF} />
					                </div>
					            </div>
							</Tab>

							<Tab label="Include Fields" onActive={this._allowDisalloweSetAutomation} value={3}>

								<h4 style={styles.headline}>Include Fields</h4>
								<SelectIncludeFields editing={false} data={this.state.includeFields} />
							</Tab>
						</Tabs>
		        	</form>

	        </Dialog>
	    );
	},

	_allowDisalloweSetAutomation : function( tab ) {

		this.setState({

			tabSelected : tab.props.index
		})
	},

	_updateExportOptions : function( newVal, field ) {

		let exportOptions 		= this.state.exportCriteria;
		exportOptions[field] 	= newVal;

		FilterActions.updateExportOptions( exportOptions );
	},

	_setNextTab : function() {

		var nextTab = this.state.tabSelected + 1;

		this.setState({

			tabSelected : nextTab
		})
	},

	_handleChangeTimePicker : 	function( e, newTime ) {

		this.setState({

			fireTime 	: 	newTime
		})
	},

	_updateDate : function( e, date ) {

		this.setState({

            fireDate  :   date
        })
	},

    _setAutomationDetails : function() {

    	FilterActions.createAutomatedDelivery({

    		exportCSV 	: 	this.state.exportCSV,
    		exportPDF 	: 	this.state.exportPDF,
    		fireDate 	: 	this.state.fireDate,
    		fireTime 	: 	this.state.fireTime,
    		repeat 		: 	this.state.repeat,
    		title 		: 	this.state.title,
    		sendToAll 	: 	this.state.sendToAll

    	});
    }
});

export default AutomatedReport;