import React from 'react';
import FilterActions from '../../actions/FilterActions';
import Checkbox from 'material-ui/Checkbox';

function getCurrentState(props) {
    return {
        allItems: props.allItems,
        displayName: props.displayName,
        include: props.include,
        itemID: props.itemID,
        editing: props.editing
    }
}

var IncludeField = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.displayName !== nextProps.displayName) {
            return true;
        }

        if (this.props.allItems !== nextProps.allItems) {
            return true;
        }

        if (this.props.include !== nextProps.include) {
            return true;
        }

        return false;
    },
    render: function () {
        return (
            <Checkbox
                onCheck={this._setSelected}
                checked={this.state.include}
                label={this.state.displayName}
            />
        )
    },
    _setSelected: function (obj, isChecked) {
        FilterActions.setIndividualFieldExportIncludeType({
            id: parseInt(this.state.itemID),
            checked: isChecked,
            editing: this.state.editing,
            allItems: this.state.allItems
        })
    }
});

export default IncludeField;