<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class Login extends Controller
{
    public function show($id)
    {
        return view('admin.end-users.login_as_subscriber', [
                'subscriberID' => $id,
            ]
        );
    }

    public function loginWithPresuppliedEmail($userID)
    {
        return view('auth.login-via-user-id', [
            'userID' => $userID,
        ]);
    }
}
