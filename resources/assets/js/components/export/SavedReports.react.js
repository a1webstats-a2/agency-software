import React from 'react';
import ExistingTemplates from '../ExistingTemplates.react';
import FiltersStore from '../../stores/FiltersStore';
import NotificationsStore from '../../stores/NotificationsStore';
import NavBar from '../navigation/NavBar.react';
import SnackbarA1 from '../Snackbar.react';
import Footer from '../Footer.react';
import UserTipHelpLink from '../UserTipHelpLink.react';
import EditTriggerActions from './EditTriggerActions.react';

function getCurrentState() {
    const teamMembers = FiltersStore.getTeamMembers();

    return {
        teamMembers,
        scenariosFilterObj: FiltersStore.getScenarioFiltersObj(),
        latestAddedFilters: FiltersStore.getLatestAddedFilters(),
        displayFancyFilters: FiltersStore.showFancyFilters(),
        filters: FiltersStore.getFilters(),
        isApplicationResting: FiltersStore.isApplicationResting(),
        settings: FiltersStore.getAllSettings(),
        existingReportsData: FiltersStore.getExistingReportsData(),
        exportOptions: {
            exportSelection: FiltersStore.getExportSelection(),
            teamMembers,
            include: FiltersStore.getExportCriteria(),
            selectionType: FiltersStore.getExportSelectionType(),
            existingReportOpen: FiltersStore.checkLightboxOpen('viewReportsForTemplate', 1),
            autoReportOpen: FiltersStore.checkLightboxOpen('newAutomatedReport', 1),
            templates: FiltersStore.getExistingTemplates(),
            automationSettings: FiltersStore.getAutomationSettings(),
            existingAutomatedReport: FiltersStore.getExistingReportData(),
            existingAutomatedReportOpen: FiltersStore.checkLightboxOpen('viewEditExistingReport', 1),
            firstLoadComplete: FiltersStore.isFirstLoadComplete('reportWizard'),
            quickOpen: FiltersStore.getQuickOpenObj(),
            createTemplateStatus: FiltersStore.getCreateTemplateStatus(),
            filters: FiltersStore.getFilters()
        },

        triggerActionSendToList: FiltersStore.getTriggerAlertEmailSendToList(),
        editTriggerActionsTemplateID: FiltersStore.getEditTriggerActionsTemplateID(),
        editTriggerActionsOpen: FiltersStore.checkLightboxOpen('editTriggerActions', -1),
        snackbarSettings: FiltersStore.getSnackbarSettings(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        },
        looseFilterSetDisplay: FiltersStore.getLooseFilterSetDisplay(),
        triggerActionsIsLoadingEmailSendTo: FiltersStore.isLoadingTriggerActionsEmailSendTo(),
        zapierIsSetup: FiltersStore.isZapierSetup(),
        autoExports: FiltersStore.getAutoExports(),
    }
}

const SavedReports = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentDidMount: function () {
        FiltersStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        FiltersStore.removeChangeListener(this._onChange);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.state.looseFilterSetDisplay !== nextState.looseFilterSetDisplay) {
            return true;
        }

        if (this.state.settings !== nextState.settings) {
            return true;
        }

        if (this.state.notificationsData !== nextState.notificationsData) {
            return true;
        }

        if (this.state.exportOptions.autoReportOpen !== nextState.exportOptions.autoReportOpen) {
            return true;
        }

        if (this.state.existingReportsData !== nextState.existingReportsData) {
            return true;
        }

        if (this.state.exportOptions !== nextState.exportOptions) {
            return true;
        }

        if (this.state.autoExports !== nextState.autoExports) {
            return true;
        }

        return false;
    },
    render: function () {
        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-12" id="results">
                            <div className="row">
                                <div className="col-md-2">
                                    <h3>Saved Templates</h3>
                                </div>
                                <div className="col-md-10">
                                    <div style={{marginTop: 27}}>
                                        <UserTipHelpLink/>
                                    </div>
                                </div>
                            </div>
                            <div className="clr"></div>
                            <br/>
                            <ExistingTemplates
                                existingReportsData={this.state.existingReportsData}
                                existingReportOpen={this.state.exportOptions.existingReportOpen}
                                autoReportOpen={this.state.exportOptions.autoReportOpen}
                                templates={this.state.exportOptions.templates}
                                settings={this.state.settings}
                                exportOptions={this.state.exportOptions}/>
                        </div>
                    </div>
                </div>

                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>

                <EditTriggerActions
                    open={this.state.editTriggerActionsOpen}
                    templateID={this.state.editTriggerActionsTemplateID}
                    teamMembers={this.state.teamMembers}
                    triggerActionsIsLoadingEmailSendTo={this.state.triggerActionsIsLoadingEmailSendTo}
                    triggerActionSendToList={this.state.triggerActionSendToList}
                    zapierIsSetup={this.state.zapierIsSetup}
                    autoExports={this.state.autoExports}
                />

                <Footer
                    looseFilterSetDisplay={this.state.looseFilterSetDisplay}
                    scenariosFilters={this.state.scenariosFilterObj}
                    isApplicationResting={this.state.isApplicationResting}
                    settings={this.state.settings}
                    filters={this.state.filters}
                    display={this.state.displayFancyFilters}
                    latestAddedFilters={this.state.latestAddedFilters}
                />
            </div>

        )
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }
});

export default SavedReports;
