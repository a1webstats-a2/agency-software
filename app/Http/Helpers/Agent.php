<?php

declare(strict_types=1);

use App\Http\Controllers\Agent\Main;

function getAgentConfigData(): array
{
    return (new Main())->getConfigData();
}
