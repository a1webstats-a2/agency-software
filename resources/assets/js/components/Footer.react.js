import React from 'react';
import FiltersFancyDisplay from './FiltersFancyDisplay.react';
import DisplayAdvancedFilters from './DisplayAdvancedFilters.react';
import DisplayLooseFilterSet from './DisplayLooseFilterSet.react';
import FiltersStore from '../stores/FiltersStore';
import UserTips from './UserTips.react';
import AttemptNewToken from './AttemptNewToken.react';

function getCurrentState(props) {
    return {
        user: FiltersStore.getUser(),
        noResultsStatus: FiltersStore.noResultsFound(),
        looseFilterSetDisplay: props.looseFilterSetDisplay,
        scenariosFilters: props.scenariosFilters,
        isApplicationResting: props.isApplicationResting,
        settings: props.settings,
        filters: props.filters,
        display: props.display,
        latestAddedFilters: props.latestAddedFilters,
        attemptNewTokenOpen: FiltersStore.checkLightboxOpen('attemptNewToken', -1),
        authAttemptMsg: FiltersStore.getAuthenticationMsg()
    }
}

var Footer = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        return (
            <div>
                <div className="mt-64"></div>
                <div id="footer">
                    <FiltersFancyDisplay
                        user={this.state.user}
                        noResultsStatus={this.state.noResultsStatus}
                        isApplicationResting={this.state.isApplicationResting}
                        filters={this.state.filters}
                        settings={this.state.settings}
                        display={this.state.display}
                        latestAddedFilters={this.state.latestAddedFilters}
                    />
                </div>
                <DisplayAdvancedFilters
                    noResultsStatus={this.state.noResultsStatus}
                    filters={this.state.scenariosFilters.filters}
                    open={this.state.scenariosFilters.open}
                />
                <DisplayLooseFilterSet
                    type={this.state.looseFilterSetDisplay.type}
                    latestAddedFilters={this.state.latestAddedFilters}
                    filters={this.state.looseFilterSetDisplay.filters}
                    open={this.state.looseFilterSetDisplay.lightboxOpen}
                />
                <UserTips/>
                <AttemptNewToken
                    msg={this.state.authAttemptMsg}
                    open={this.state.attemptNewTokenOpen}/>
            </div>
        )
    }
});

export default Footer;
