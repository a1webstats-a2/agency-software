import React from 'react';
import FilterActions from '../actions/FilterActions';


function getCurrentState( props ) {

	return {

		
	}
}

var UpdateResultsLink = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return false;
	},

	render : function() {

		return (

			<div>
				Filters changed... <a href="#" onClick={this._updateResults}>Update Results Now?</a>

			</div>

		)
	},

	_updateResults : function() {

		FilterActions.updateResults();
	}
});

export default  UpdateResultsLink;