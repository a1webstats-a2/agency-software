@extends( 'templates.signup' )
@section( 'content' )
  	<div class="row">
        <div  class="col-md-6 col-md-offset-3">
			<div id="signupForm2" class="signupForm">

				<h1>Get Access Token</h1>

				<form method="POST">
				
				  	<div class="form-group">
				    	<label>Secret</label>
				    	<input type="password" class="form-control" name="secret" placeholder="Secret">
				  	</div>

				  	<button class="btn btn-primary">Get Token</button>
				</form>
			</div>
		</div>
	</div>
@endsection
				