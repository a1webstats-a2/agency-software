import React from 'react';
import FilterChoice from './FilterChoice.react';
import TextField from 'material-ui/TextField';
import AdvancedFilterActions from '../../actions/AdvancedFilterActions';
import Checkbox from 'material-ui/Checkbox';
import RefreshIndicator from 'material-ui/RefreshIndicator';

function getCurrentState(props) {
    return {
        updatingAnalytics: props.updatingAnalytics,
        paginationCurrentPage: props.paginationCurrentPage,
        isApplicationResting: props.isApplicationResting,
        selectAnyThatMatch: props.selectAnyThatMatch,
        choiceType: props.choiceType,
        searchText: props.searchText,
        selection: props.selection,
        choices: props.choices
    }
}

function sortByAtoZ(a, b) {
    if (a[1] < b[1]) return -1;
    if (a[1] > b[1]) return 1;
    return 0;
}

const styles = {
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    }
};


function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}


function getPaginatedChoices(choices, currentPage) {
    let choicesClone = clone(choices);

    return choicesClone.splice((currentPage * 100), 100)
}

const FilterChoices = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.updatingAnalytics !== nextProps.updatingAnalytics) {
            return true;
        }

        if (this.props.paginationCurrentPage !== nextProps.paginationCurrentPage) {
            return true;
        }

        if (this.props.selectAnyThatMatch !== nextProps.selectAnyThatMatch) {
            return true;
        }

        if (this.props.isApplicationResting !== nextProps.isApplicationResting) {
            return true;
        }

        if (this.props.selection !== nextProps.selection) {
            return true;
        }

        if (this.props.choices !== nextProps.choices) {
            return true;
        }

        if (this.props.searchText !== nextProps.searchText) {
            return true;
        }

        return false;
    },
    render: function () {
        var displayChoices = [];

        if (this.state.searchText === '') {
            displayChoices = this.state.choices;
        } else {
            let searchText = this.state.searchText;

            if (typeof this.state.searchText === 'undefined') {
                searchText = '';
            }

            searchText = searchText.replace("?", "");
            const regexp = new RegExp(searchText, 'gi');

            this.state.choices.forEach(function (choice, i) {
                if (typeof choice[1] !== "undefined") {
                    if (choice[1].match(regexp)) {
                        displayChoices.push(choice);
                    }
                }
            });
        }

        displayChoices.sort(sortByAtoZ);

        let choices;

        if (!this.state.isApplicationResting || this.state.updatingAnalytics) {
            choices = (
                <div>
                    <p>Loading new filters</p>

                    <div style={styles.container}>
                        <RefreshIndicator
                            size={40}
                            left={10}
                            top={0}
                            status="loading"
                            style={styles.refresh}
                        />
                    </div>
                </div>
            )

        } else {
            const paginatedChoices = getPaginatedChoices(clone(displayChoices), this.state.paginationCurrentPage);

            choices = paginatedChoices.map(function (choice, i) {
                var checked = (typeof this.state.selection[parseInt(choice[0])] !== "undefined") ? true : false;

                return <FilterChoice
                    checked={checked}
                    choice={choice}
                    type={this.state.choiceType}
                    key={i}
                />

            }.bind(this))
        }

        var anyThatMatch = (this.state.selectAnyThatMatch && this.state.selectAnyThatMatch.apply) ? true : false;

        let displayAnyThatMatch = "";
        let hintText = "Search";

        if (this.state.choiceType !== "ip") {
            displayAnyThatMatch = (
                <Checkbox
                    label="Any that match"
                    checked={anyThatMatch}
                    onCheck={this._selectAnyThatMatch}
                    style={{marginTop: 15}}

                />
            )

        } else {
            hintText = "IP Address";
        }

        return (
            <div className="row">
                <div className="col-md-12">
                    <div style={{height: 500}}>
                        <div className="row">
                            <div className="col-md-7">
                                <TextField
                                    onChange={this._setSearchText}
                                    fullWidth={true}
                                    hintText={hintText}
                                />
                            </div>

                            <div className="col-md-5">
                                {displayAnyThatMatch}
                            </div>
                        </div>

                        <div className="clr"></div>

                        <div style={{height: 400, width: 600, marginTop: 50, position: 'fixed', overflow: 'scroll'}}>
                            {choices}
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    _selectAnyThatMatch: function (event, isChecked) {
        AdvancedFilterActions.setSelectAnyThatMatch(isChecked);
    },
    _setSearchText: function (event, searchText) {
        AdvancedFilterActions.setSearchText(searchText);
    }
});

export default FilterChoices;
