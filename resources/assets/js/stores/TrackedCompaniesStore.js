import CompanyTrackerDispatcher from '../dispatcher/CompanyTrackerDispatcher';
import CompanyTrackerConstants from '../constants/CompanyTrackerConstants';
import EventEmitterObj from 'events';

var EventEmitter = EventEmitterObj.EventEmitter;
import merge from 'merge';
import assign from 'object-assign';
import TrackedCompaniesActions from '../actions/TrackedCompaniesActions';

var _trackedOrganisations = [];
var _assignedToTeamMemberID = 0;
var _trackedOptions = null;
var _trackedRelationships = [];
import FilterActions from '../actions/FilterActions';
import FiltersStore from './FiltersStore';

var _assignedTo = 1;
var _allTracked = [];
var _myTracked = [];
var _popoverObj = {open: false};
import {Router, Route, browserHistory} from 'react-router'

function loadTrackedOrganisations() {


}

browserHistory.listen(location => {

    FilterActions.setCurrentRoute(location.pathname.replace("/", ""));

});

function companyTrackerSaveAssignedTo() {

    FilterActions.closeLightbox();

    var user = FiltersStore.getUser();

    if (_trackedOptions.assigned_user === "Don't track") {

        if (typeof _myTracked[_trackedOptions.myKey] !== "undefined") {

            delete _myTracked[_trackedOptions.myKey];
        }

        delete _allTracked[_trackedOptions.myKey];


    } else {

        if (parseInt(user.id) === parseInt(_trackedOptions.assigned_user)) {

            _myTracked[_trackedOptions.myKey] = _trackedOptions;

            _trackedOrganisations = _myTracked;

        } else if (typeof _myTracked[_trackedOptions.myKey] !== "undefined") {

            delete _myTracked[_trackedOptions.myKey];

            _trackedOrganisations = _myTracked;

        }
    }

    switch (_assignedTo) {

        case 1 :

            _trackedOrganisations = _allTracked;

            break;

        case 2 :

            _trackedOrganisations = _myTracked;

            break;
    }

    var request = new XMLHttpRequest();

    request.open("POST", "api/proxy/api/v1/assign-team-member");
    request.setRequestHeader('Content-Type', 'application/json');

    request.onreadystatechange = function () {

        if (request.readyState === 4 && request.status === 200) {

        }
    };

    request.send(JSON.stringify(_trackedOptions));

    FilterActions.setSnackbar({

        message: 'Team member successfully reallocated'
    })

}

function setTrackedOrganisations() {

    var request = new XMLHttpRequest();

    request.open("GET", "api/proxy/api/v1/track-organisation");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    request.onreadystatechange = function () {

        if (request.readyState === 4 && request.status === 200) {

            TrackedCompaniesActions.storeTrackedOrganisations(JSON.parse(request.responseText));
        }
    };

    request.send();
}

function storeTrackedOrganisations(companies) {

    _myTracked = companies.data.my_tracked;
    _allTracked = companies.data.results;
    _trackedOrganisations = _allTracked;
    _trackedRelationships = companies.data.relationships;
}

function setAssignedTeamMemberID(teamMemberID) {

    _assignedToTeamMemberID = teamMemberID;
    _trackedOptions.assigned_user = teamMemberID;
    _trackedOrganisations[_trackedOptions.myKey].assigned_user = teamMemberID;
}

function trackerUpdateClientRelationship(relationship) {

    var tracked = JSON.parse(JSON.stringify(_trackedRelationships));

    _trackedRelationships = [];

    var indexOf = tracked[relationship.organisationId].indexOf(relationship.clientType);

    if (indexOf >= 0) {

        delete tracked[relationship.organisationId][indexOf];

    } else {

        tracked[relationship.organisationId].push(relationship.clientType);
    }

    _trackedRelationships = tracked;

    var request = new XMLHttpRequest();

    request.open("POST", "api/proxy/api/v1/euco-relationships");
    request.setRequestHeader('Content-Type', 'application/json');

    request.onreadystatechange = function () {

    };

    request.send(JSON.stringify({

            type: relationship.clientType,
            organisationId: relationship.organisationId,
            action: relationship.action
        })
    );
}

function setAssignedToOption(option) {

    _assignedTo = option;

    switch (option) {

        case 1 :

            _trackedOrganisations = _allTracked;

            break;

        case 2 :

            _trackedOrganisations = _myTracked;

            break;
    }
}

function setPopover(popover) {

    _popoverObj = popover;
}

function updateEndUserCompanyRecord() {
    var activeRecord = FiltersStore.getActiveRecord();

    _trackedOrganisations[activeRecord.myKey] = activeRecord.data;

    var request = new XMLHttpRequest();

    request.open("POST", "api/proxy/api/v1/end-user-company/organisation");
    request.setRequestHeader('Content-Type', 'application/json');

    request.send(JSON.stringify({
            //sessionID : _updatedActiveRecord.data.main_session_id,
            companyId: activeRecord.data.organisationid,
            name: activeRecord.data.use_organisation_name,
            tel: activeRecord.data.use_organisation_tel,
            website: activeRecord.data.use_organisation_website,
            linkedIn: ""
        })
    );
}

function setTrackedOptions(recordKey) {

    var trackedOptions = _trackedOrganisations[recordKey];
    trackedOptions.myKey = recordKey;
    _trackedOptions = trackedOptions;
}

var TrackedCompaniesStore = merge(EventEmitter.prototype, {
    getAssignedTo: function () {
        return _assignedTo;
    },
    getTrackedOptions: function () {
        return _trackedOptions;
    },
    getRelationships: function () {
        return JSON.parse(JSON.stringify(_trackedRelationships));
    },
    fetchTrackedOrganisations: function () {
        return JSON.parse(JSON.stringify(_trackedOrganisations));
    },
    emitChange: function () {
        this.emit('change');
    },
    getPopoverObj: function () {
        return _popoverObj;
    },
    addChangeListener: function (callback) {
        this.on('change', callback);
    },
    removeChangeListener: function (callback) {
        this.removeListener('change', callback);
    }
});


CompanyTrackerDispatcher.register(function (payload) {
    let returnRes = true;

    switch (payload.actionType) {
        case CompanyTrackerConstants.LOAD_TRACKED_COMPANIES :
            loadTrackedOrganisations();

            break;
        case CompanyTrackerConstants.SET_TRACKED_ORGANISATIONS :
            setTrackedOrganisations();

            break;
        case CompanyTrackerConstants.SET_TRACKED_OPTIONS :
            setTrackedOptions(payload.data);

            break;
        case CompanyTrackerConstants.SET_ASSIGNED_TEAM_MEMBER_ID :
            setAssignedTeamMemberID(payload.data);

            break;
        case CompanyTrackerConstants.STORE_TRACKED_ORGANISATIONS :
            storeTrackedOrganisations(payload);

            break;
        case CompanyTrackerConstants.TRACKED_SAVE_ASSIGNED_TO :
            companyTrackerSaveAssignedTo();

            break;
        case CompanyTrackerConstants.SET_ASSIGNED_TO_OPTION :
            setAssignedToOption(payload.data);

            break;
        case CompanyTrackerConstants.TRACKER_UPDATE_CLIENT_RELATIONSHIP :
            trackerUpdateClientRelationship(payload.data);

            break;
        case CompanyTrackerConstants.UPDATE_END_USER_COMPANY_RECORD :
            updateEndUserCompanyRecord(payload.data);

            break;

        case CompanyTrackerConstants.SET_POPOVER :
            setPopover(payload.data);

            break;
    }

    TrackedCompaniesStore.emitChange();

    return returnRes;

});

export default TrackedCompaniesStore;


