import React from 'react';
import FilterActions from '../actions/FilterActions';
import KeywordOption from './keywordOption.react';
import FilterStore   from '../stores/FiltersStore';

var AddKeywordFilter = React.createClass({

	getInitialState : 	function() {

		return {

			text 	: false,
			id 		: false
		}
	},

	setKeyword 	: 	function( event ) {

		var index 		= event.target.selectedIndex;
		var keywordText = event.target[index].text

		this.setState(
			{

				id 		: 	event.target.value,
				text 	: 	keywordText
			}
		);
	},	

	render: function() {

		var keywords = FilterStore.getKeywords();

		var keywordOptions 	= [];

		keywords.map( function( keyword ) {
			
			keywordOptions.push( <KeywordOption key={keyword.id} myKey={keyword.id} keywordData={keyword} /> );

		});

		return (

			<div className="addDateFilter">
		   		<form className="criteriaBox">
			   		<div className="form-group">
				   		<h4>Keyword Filters</h4>
			   			<select className="form-control" onChange={this.setKeyword}>
			   				{keywordOptions}
			   			</select>


			   		</div>

			   		<button className="btn btn-success" onClick={this._save}>Add Filter</button>

		   		</form>
		   	</div>
		);
	},

	_save     :   function( e ) {

        e.preventDefault();

		if( !this.state.id ) {

			var keywords 	= FilterStore.getKeywords();

			if( keywords.length > 0 ) {

				var keyword 	= keywords[0];

				var keywordText = keyword.keyword;
				var keywordId 	= keyword.id;

			} else {

				return false;
			}
		
		} else {

			var keywordText = this.state.text;
			var keywordId 	= this.state.id;
		}

   		var newState  = {

   			type          :   'keyword', 
   			storedValue   :   {

   				keywordId 		: keywordId,
   				keywordText 	: keywordText,
	   			keywordValue  	:   ''

   			},
   			id            :   null,

   		}

   		FilterActions.createFilter( newState );
    }
});

export default  AddKeywordFilter;