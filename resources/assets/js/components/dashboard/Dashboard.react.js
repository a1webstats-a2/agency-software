import React from 'react';
import ImpressionsWithinLastWeek from './ImpressionsWithinLastWeek.react';
import VisitorStats from '../VisitorStats.react';
import SessionCountries from './SessionCountries.react';
import Top5Referrers from './Top5Referrers.react';
import Top5Keywords from './Top5Keywords.react';
import RecentCompanies from './RecentCompanies.react';
import QuickReportBuilder from './QuickReportBuilder.react';
import NavBar from '../navigation/NavBar.react';
import DashboardTemplates from './DashboardTemplates.react';
import UserTipHelpLink from '../UserTipHelpLink.react';

function getCurrentState(props) {
    return {
        recentCompaniesDivWidth: props.organisationsDatatableWidth,
        accountIsOK: props.accountIsOK,
        templates: props.templates,
        isApplicationResting: props.isApplicationResting,
        analyticsData: props.analyticsData.analyticsData,
        otherData: props.analyticsData,
        settings: props.settings,
        filters: props.filters,
        displayFancyFilters: props.displayFancyFilters,
        latestAddedFilters: props.latestAddedFilters,
        snackbarSettings: props.snackbarSettings,
        quickReportDateRange: props.quickReportDateRange,
        quickReportTrafficType: props.quickReportTrafficType,
        quickReportDataType: props.quickReportDataType,
        notificationsData: props.notificationsData,
    };
}

const Dashboard = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentDidMount: function () {
        window.addEventListener("resize", this._updateDimensions);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-12" id="results">
                            <UserTipHelpLink/>
                            <div className="row">
                                <div className="col-md-3 dashboardDiv">
                                    <QuickReportBuilder
                                        dateRange={this.state.quickReportDateRange}
                                        trafficType={this.state.quickReportTrafficType}
                                        dataType={this.state.quickReportDataType}
                                    />
                                </div>
                                <div className="col-md-3 dashboardDiv">
                                    <VisitorStats
                                        isApplicationResting={this.state.isApplicationResting}
                                        stats={this.state.analyticsData.visitorStats}/>
                                </div>
                                <div className="col-md-6 dashboardDiv" id="recentCompaniesDiv">
                                    <RecentCompanies
                                        width={this.state.recentCompaniesDivWidth}
                                        isApplicationResting={this.state.isApplicationResting}
                                        companies={this.state.analyticsData.organisations}/>
                                </div>
                            </div>
                            <div className="clr"></div>
                            <br/><br/><br/>
                            <div className="row">
                                <div className="col-md-3 dashboardDiv">
                                    <Top5Keywords
                                        isApplicationResting={this.state.isApplicationResting}
                                        keywords={this.state.analyticsData.keywords}/>
                                </div>

                                <div className="col-md-3 dashboardDiv">
                                    <Top5Referrers
                                        isApplicationResting={this.state.isApplicationResting}
                                       referrers={this.state.analyticsData.referrers}/>
                                </div>

                                <div className="col-md-6 dashboardDiv">
                                    <DashboardTemplates templates={this.state.templates}/>
                                </div>
                            </div>
                            <div className="clr"></div>
                            <br/><br/>
                            <div className="row">
                                <div className="col-md-6 dashboardDiv">
                                    <ImpressionsWithinLastWeek
                                        accountIsOK={this.state.accountIsOK}
                                        isApplicationResting={this.state.isApplicationResting}
                                        impressions={this.state.analyticsData.uniqueSessions}/>
                                </div>
                                <div className="col-md-6 dashboardDiv">
                                    <SessionCountries isApplicationResting={this.state.isApplicationResting}
                                                      otherData={this.state.otherData}
                                                      analyticsData={this.state.analyticsData}
                                                      locations={this.state.analyticsData.visitorLocations}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="clr"></div>
                </div>
                <div className="clr"></div>
            </div>
        )
    },
    _updateDimensions: function () {
        if (document.getElementById("recentCompaniesDiv")) {
            this.setState({
                recentCompaniesDivWidth: (document.getElementById("recentCompaniesDiv").offsetWidth - 70)
            })
        }
    }
});

export default Dashboard;
