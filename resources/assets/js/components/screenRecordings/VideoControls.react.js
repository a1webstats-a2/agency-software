import React 					from 'react';
import { ProgressBar } 			from 'react-player-controls';
import ScreenRecorderActions  	from '../../actions/ScreenRecorderActions';

function getCurrentState( props ) {

	return {

		videoLength 	: 	props.videoLength,
		videoI 			: 	props.videoI
	}
}

class VideoControls extends React.Component {

	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( props ) {

		this.setState( getCurrentState( props ) );
	}
	
	render() {

		let style = {

			width 			: 	'100%',
			height 			: 	30,
			marginLeft 		: 	'auto',
			marginRight 	: 	'auto',
			left 			: 	0,
			right 			: 	0,
			zIndex 			: 	3,
			position 		: 	'relative',
			opacity 		: 	1
		}

		return (

			<div style={style}>

				<ProgressBar
					totalTime={this.state.videoLength}
					currentTime={this.state.videoI}
					isSeekable={true}
					onSeek={ seekTime => this._seekTime( seekTime ) }
					onIntent={ seekTime => this._setIntent( seekTime ) }
				/>
			</div>
		)
	}

	_setIntent( seekTime ) {


	}

	_seekTime( seekTime ) {

		ScreenRecorderActions.setVideoI( seekTime );
	}
}

export default VideoControls;