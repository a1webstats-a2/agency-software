import React 			from 'react';
import FilterActions 	from '../../actions/FilterActions';
import FiltersStore 	from '../../stores/FiltersStore';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

function getCurrentState( props ) {

	return {

		invoices 	: 	FiltersStore.getInvoices(),
		settings 	: 	FiltersStore.getAllSettings() 
	}
}

function parseDate( inputDate ) {

    var parts            =  inputDate.match( /(\d+)/g );

    var formattedDate    =  new Date( parts[0], parts[1]-1, parts[2], parts[3], parts[4], parts[5] );

    return new Date( formattedDate );
}

function ordinal_suffix_of( i ) {
    
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function twoDigits( d ) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

var PreviousInvoices = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentDidMount : function() {

		FilterActions.loadInvoices();
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		var monthNames      =   [  "January", "February", "March", "April", "May", "June",
        
          "July", "August", "September", "October", "November", "December"
        ];

		var trs = this.state.invoices.map( function( tr, i ) {

			var rawDate         =   parseDate( tr.created_at );

	        var displayDate     =   ordinal_suffix_of( rawDate.getDate() ) + ' ' + monthNames[rawDate.getMonth()] + ' ' + rawDate.getFullYear() + ' ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );

	        switch( this.state.settings.user.preferred_date_format ) {

	            case "dd/mm/yyyy" :

	                displayDate = twoDigits( rawDate.getDate() ) + '/' + twoDigits( rawDate.getMonth() ) + '/' + rawDate.getFullYear() + ' ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );

	                break;

	            case "mm/dd/yyyy" :

	                displayDate = twoDigits( rawDate.getMonth() ) + '/' + twoDigits( rawDate.getDate() ) + '/' + rawDate.getFullYear() + ' ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );

	                break;

	            case "yyyy-mm-dd" :

	                displayDate = rawDate.getFullYear() + '-' + twoDigits( rawDate.getMonth() ) + '-' + twoDigits( rawDate.getDate() ) + ' ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );
	    
	                break;

	            case "fulltext" :

	                displayDate = ordinal_suffix_of( rawDate.getDate() ) + ' ' + monthNames[rawDate.getMonth()] + ' ' + rawDate.getFullYear() + ' ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );

	                break;

	        }

	        var downloadLink = "/internal/download/invoice/" + tr.id;

			return (

				<TableRow key={i}>
					<TableRowColumn>{displayDate}</TableRowColumn>
					<TableRowColumn><a href={downloadLink}>{tr.filename}</a></TableRowColumn>
				</TableRow>
			)

		}.bind( this ) );

		if( trs.length > 0 ) {

			var display = (

				<Table selectable={false}>
					<TableHeader 
						displaySelectAll={false}
						enableSelectAll={false}>
						<TableRow>
							<TableHeaderColumn>Invoice Date</TableHeaderColumn>
							<TableHeaderColumn>Action</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody displayRowCheckbox={false}>
						{trs}
					</TableBody>
				</Table>
			)
		
		} else {

			var display = (

				<p>Loading Invoices</p>
			)
		}

		return (

			<div>
				
				<h3>Previous Invoices</h3><br />

				{display}
			</div>
		)
	}

});

export default  PreviousInvoices;