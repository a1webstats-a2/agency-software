import React from 'react';
import Snackbar from 'material-ui/Snackbar';
import DownloadLink from './DownloadLink.react';
import FilterActions from '../actions/FilterActions';
import UpdateResultsLink from './UpdateResultsLink.react';
import NewReportTemplatesLink from './NewReportTemplatesLink.react';

function setState(props) {
    const snackbarSettings = props.snackbarSettings;

    return {
        settings: snackbarSettings,
        component: (typeof snackbarSettings.component !== "undefined") ? snackbarSettings.component : false,
        message: snackbarSettings.msg,
        open: snackbarSettings.open,
        data: snackbarSettings.data
    }
}

const SnackbarA1 = React.createClass({
    getInitialState: function () {
        return setState(this.props);
    },
    onActionTouchTap: function () {
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.snackbarSettings.msg !== nextProps.snackbarSettings.msg) {
            return true;
        }

        if (this.props.snackbarSettings.open !== nextProps.snackbarSettings.open) {
            return true;
        }

        if (this.props.snackbarSettings.component !== nextProps.snackbarSettings.component) {
            return true;
        }
        return false;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(setState(newProps));
    },
    handleRequestClose: function () {
        FilterActions.setSnackbarClosed();
    },
    render: function () {
        let msg = '';
        if (typeof this.state.data.type !== 'undefined') {
            switch (this.state.data.type) {
                case 'csv':
                    const link = "/internal/download/contents/" + this.state.data.assetId;
                    msg = <DownloadLink message="Your report is ready" link={link}/>;

                    break;
            }
        } else if (!this.state.component) {
            msg = (
                <p>{this.state.message}</p>
            )
        } else {
            switch (this.state.component) {
                case 'updateResultsLink':
                    msg = <UpdateResultsLink/>

                    break;
                case 'viewSavedReportsLink':
                    msg = <NewReportTemplatesLink/>

                    break;
            }
        }

        return (
            <div className="snackbar">
                <Snackbar
                    autoHideDuration={50000}
                    open={this.state.open}
                    message={msg}
                    onRequestClose={this.handleRequestClose}/>
            </div>
        )
    }
});
export default SnackbarA1;
