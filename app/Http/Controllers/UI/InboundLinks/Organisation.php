<?php

namespace App\Http\Controllers\UI\InboundLinks;

use Illuminate\Http\Request;

class Organisation extends \App\Http\Controllers\Controller {

	public function show( Request $request, $organisationID ) {

		if( !$request->session()->has( 'user' ) ) {

			return view( 'auth/login-with-redirect', 

				[ 
					'agentData'		=>	getAgentConfigData(),
					'filterType' 	=> 	'organisation', 
					'filterID' 		=> 	$organisationID 
				] 
			);

		} else {

			return view( 'UI.dashboard', [

				'agentConfig' 	=>	getAgentConfigData()

			]);

		}
	}
}

