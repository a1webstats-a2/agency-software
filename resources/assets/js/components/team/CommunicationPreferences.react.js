import React 			from 'react';
import Paper 			from 'material-ui/Paper';
import Checkbox 		from 'material-ui/Checkbox';
import FilterActions 	from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {
	
		preferences : props.preferences
	}
}


class CommunicationPreferences extends React.Component {

	constructor( props ) {

		super( props );
	
		this.state = getCurrentState( props );
	}


	componentWillReceiveProps( props ) {

		this.setState( getCurrentState( props ) );
	}

	render() {

		const allowBlog 					= ( this.state.preferences.allow_blog === 1 ) ? true : false;
		const allowOffers 					= ( this.state.preferences.allow_offers === 1 ) ? true : false;
		const allowPhone 					= ( this.state.preferences.allow_phone === 1 ) ? true : false;
		const allowServices 				= ( this.state.preferences.allow_services === 1 ) ? true : false;
		const allowTransactional 			= ( this.state.preferences.allow_transactional === 1 ) ? true : false;		

		const callCheckboxDisabled 			= ( this.state.preferences.allow_phone === -1 ) ? true : false;
		const blogCheckboxDisabled 			= ( this.state.preferences.allow_blog === -1 ) ? true : false;
		const offersCheckboxDisabled 		= ( this.state.preferences.allow_offers === - 1 ) ? true : false;
		const servicesCheckboxDisabled 		= ( this.state.preferences.allow_services === - 1 ) ? true : false;
		const transactionsCheckboxDisabled 	= ( this.state.preferences.allow_transactional === - 1 ) ? true : false;



		return (

			<div>
				<Paper style={{ padding: 20 }}>

					<h2>Communication</h2>

					<div className="clr"></div><br /><br />

					<Checkbox
						onCheck={ ( obj, isChecked ) => this._setAllowOrDisallow( isChecked, 'allow_phone' ) }
						disabled={callCheckboxDisabled}
						checked={allowPhone}
						label="Yes, it's ok to call me regarding my subscription"
					/>
					<div className="clr"></div><br />

					<Checkbox
						onCheck={ ( obj, isChecked ) => this._setAllowOrDisallow( isChecked, 'allow_transactional' ) }
						disabled={transactionsCheckboxDisabled}
						checked={allowTransactional}
						label="Yes, I'd like to receive transactional emails"
					/>

					<div className="clr"></div><br />

					<Checkbox
						onCheck={ ( obj, isChecked ) => this._setAllowOrDisallow( isChecked, 'allow_offers' ) }
						checked={allowOffers}
						disabled={offersCheckboxDisabled}
						label="Yes, I'd like to receive emails regarding new offers"
					/>

					<div className="clr"></div><br />

					<Checkbox
						onCheck={ ( obj, isChecked ) => this._setAllowOrDisallow( isChecked, 'allow_services' ) }					
						checked={allowServices}
						disabled={servicesCheckboxDisabled}
						label="Yes, I'd like to receive emails regarding new services"
					/>

					<div className="clr"></div><br />

					<Checkbox
						onCheck={ ( obj, isChecked ) => this._setAllowOrDisallow( isChecked, 'allow_blog' ) }
						checked={allowBlog}
						disabled={blogCheckboxDisabled}
						label="Yes, I'd like to receive emails regarding new blog posts"
					/>

				</Paper>

			</div>
		)
	}

	_setAllowOrDisallow( allow, permissionType ) {

		let permissions 			= this.state.preferences;
		permissions[permissionType] = ( allow ) ? 1 : 0;
		FilterActions.updateUserPermissions( permissions );
	}
}

export default CommunicationPreferences;