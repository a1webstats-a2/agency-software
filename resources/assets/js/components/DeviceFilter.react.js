import React from 'react';
import DeviceIcon from 'material-ui/svg-icons/action/important-devices';

function getCurrentState( props ) {

	return {

		data : 	props.data
	}
}

var DeviceFilter = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return false;
	},

	render : function() {

		return (

			<div className="dateFromBox form-group">
				<div className="row">

					<div className="col-md-2">
	                    <DeviceIcon />
	                </div>
	                <div className="col-md-8 overflowHidden">
                        <label className="control-label">Device Type:</label>
                        <p>{this.state.data.storedValue.name}</p>
                    </div>
                </div>
            </div>

		)
	}
});

export default  DeviceFilter;