import React 			from 'react';
import FilterActions 	from '../actions/FilterActions';
import TimeFrom 		from './TimeFrom.react';
import TimeTo 			from './TimeTo.react';
import Chip from 'material-ui/Chip';
import Label from 'material-ui/svg-icons/action/label-outline';
import Location from 'material-ui/svg-icons/communication/location-on';
import Search from 'material-ui/svg-icons/action/search';
import Avatar from 'material-ui/Avatar';
import Schedule from 'material-ui/svg-icons/action/schedule';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/image/edit';

const styles = {
  	
  	chip 	: {
    	margin 		: 8
  	},

  	chipWrapper : {

  		display  	: 	'flex',
  		flexWrap 	: 	'wrap',
  		float 		: 	'left'

  	},

  	toggle : {
		
		thumbSwitched: {
			
			backgroundColor: '#53b68b',
		},

		trackSwitched: {
			
			backgroundColor: '#ccc',
		},

		thumbOff: {
		   
		    backgroundColor: '#ccc',
		},
		
		trackOff: {
		
		    backgroundColor: '#ccc',
		}
	}
}

function getHumanFriendlyIncludeTypeString( includeType ) {

	var returnString = "";

	switch( includeType.toLowerCase() ) {

		case "nonexplicitinclude" :

			returnString = "visit may contain";

			break;

		case "include" :

			returnString = "visit must include";

			break;

		case "exclude" :

			returnString = "visit must exclude";

			break;

		case "firstpagevisited" :

			returnString = "first page visited";

			break;

		case "notfirstpagevisited" :

			returnString = "not first page visited";

			break;

		case "lastpagevisited" :

			returnString = "last page visited";

			break;

		case "notlastpagevisited" :

			returnString = "not last page visited";

			break;

	}

	return returnString;
}

function getCurrentState( props ) {

	var latestAddedFilters = props.latestAddedFilters;

	var isNew = false;
	
	if( typeof latestAddedFilters[props.filter.type] !== "undefined" ) {

		if( latestAddedFilters[props.filter.type].indexOf( props.filter.storedValue.id ) > -1 ) {

			isNew = true;
		}
	}

	return {

		latestAddedFilters 	: 	props.latestAddedFilters,
		avatar 				: 	props.avatar, 
		type 				: 	props.type,
		filter  			: 	props.filter,
		text 				: 	props.text,
		isNew 				: 	isNew
	}
}

var A1TimeChip = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( JSON.stringify( this.props.filter ) !== JSON.stringify( nextProps.filter ) ) {

			return true;
		}

		return false;
	},

	render : function() {

		var includeType = "include";

		if( typeof this.state.filter.storedValue.criteria !== "undefined" ) {

			includeType = getHumanFriendlyIncludeTypeString( this.state.filter.storedValue.criteria );
		}

		var avatars = {

	
			schedule 		: 	<Schedule />
	

		};

		var avatar 					= avatars[this.state.avatar];

		var avatarBackgroundColor 	= "#170550";

		var backgroundColor 		= "#170550";

		if( this.state.isNew ) {

			backgroundColor 		= "#53b68b";

			avatarBackgroundColor 	= "#53b68b";
		}

		var showCriteriaTypeFor = [

			'customfilterurl',
			'customfilterreferrer',
			'customfilterreferrercountry',
			'customfilterkeyword',
			'organisationfilter'
		]

		if( showCriteriaTypeFor.indexOf( this.state.filter.type.toLowerCase() ) === -1 ) {

			var includeTypeDisplay = " ";
		
		} else {

			var includeTypeDisplay = "-> " + includeType + " ->";
		}

		var timepicker = "";

		if( this.state.filter.type.toLowerCase() === "timefrom" ) {

			timepicker = <TimeFrom time={this.state.text} />
		
		} else {

			timepicker = <TimeTo time={this.state.text} />
		}

		return (

			<div style={styles.chipWrapper}>
				<Chip  className="chip" style={styles.chip} >

					<Avatar backgroundColor={avatarBackgroundColor} icon={avatar} />

					<strong>{this.state.type}</strong>

					
					{timepicker}

				
				</Chip>
			</div>
		)
	},

	_editTimeFilter : function() {


	}
});

export default  A1TimeChip;
