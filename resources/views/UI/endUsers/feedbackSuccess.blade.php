@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>


@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

				<div id="signupLogo">

                    <img src="/images/{{ $loginLogo }}" />

				</div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<div class="col-md-12">

						<h1>Thank you!</h1>

						<p>All feedback is reviewed by real humans who care about how we're perceived, and we appreciate you taking the time to complete the survey.</p>

					</div>

				</div>
			</div>
		</div>
	</div>
@endsection