<?php 
$configData = getAgentConfigData();
?>

<!doctype html>
    <!--[if IE 7 ]>    <html class="ie ie7" lang="en"> <![endif]-->
    <!--[if IE 8 ]>    <html class="ie ie8" lang="en"> <![endif]-->
    <!--[if IE 9 ]>    <html class="ie ie9" lang="en"> <![endif]-->
    <!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]--> 
    <head>
        <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
   
        <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon'/ >
        <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <meta name="robots" content="noindex,nofollow" />
        <title>{{ $configData['name'] }}</title>
        <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>
    <body id="signupContent">

        <div>

            @if (isset( $errors ) && count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @yield( 'content' );
           
        <div class="clr"></div>
        <footer>
        </footer>   

        <?php if( isset( $scripts ) && !empty( $scripts ) ) : ?>
            <?php foreach( $scripts as $scr ) : ?>
                <script src="<?= $scr ?>"></script>
            <?php endforeach ?>
        <?php endif ?>

        <script>

            var cid = 1653;

            (function() {
                window.a1WebStatsObj = 'a1w';
                window.a1w = window.a1w || function(){
                    (window.a1w.q = window.ga.q || []).push(arguments)
                },
                window.a1w.l = 1 * new Date();
                var a = document.createElement('script');
                var m = document.getElementsByTagName('script')[0];
                a.async = 1;
                a.src = "https://api1.websuccess-data.com/js/a1TrackerCode.js";
                m.parentNode.insertBefore(a,m)
            })();

            var _pt = ["D04D3F09-5C41-435A-8898-763D9B31458E"];
            (function(d,t){
            var a=d.createElement(t),s=d.getElementsByTagName(t)[0];a.src=location.protocol+'//a1webstrategy.com/track.js';s.parentNode.insertBefore(a,s);
            }(document,'script'));

        </script>
    </body>
</html>
