import React from 'react';

import FilterActions from '../../../actions/FilterActions';

function getCurrentState( props ) {

 	return {

 		orgName 	: 	props.orgName,
 		orgId 		: 	props.orgId
  	}
}




var OrganisationFilterLink = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.orgId !== nextProps.orgId ) {

			return true;
		}

		if( this.props.orgName !== nextProps.orgName ) {

			return true;
		}

		return false;
	},

	render : function() {

		if( !this.state.orgName ) {

			return ( <div></div> );
		}
		
        var organisationName = this.state.orgName;

		return (

			<a href="#" onClick={this._createFilter}>{organisationName}</a>

		);		
	},

	_createFilter : function( event ) {

		event.preventDefault();

		var newState  = {

   			type          	:   'organisationFilter', 
   			storedValue 	:   {

				id 			: 	this.state.orgId,
				include 	: 	'include',
				name 		: 	this.state.orgName,
				value 		: 	this.state.orgId
			},
			id 				: 	this.state.orgId
   		}

   		FilterActions.quickDashboardLink( newState );
	}
});

export default  OrganisationFilterLink;