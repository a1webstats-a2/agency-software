import React from 'react';
import FilterActions from '../../actions/FilterActions';
import Checkbox from 'material-ui/Checkbox';

function getCurrentState(props) {
    return {
        currency: props.currency,
        vatApplicable: props.vatApplicable,
        paymentDisabled: props.paymentType === 0,
        paymentType: props.paymentType,
        vendor: props.vendor,
        monthlyFee: props.monthlyFee,
        yearlyFee: props.yearlyFee,
        crossoutFee: props.crossoutFee,
        sixMonthFee: props.sixMonthlyFee,
        sixMonthCrossoutDisplay: props.sixMonthCrossoutDisplay
    }
}

const styles = {
    block: {
        maxWidth: 250,
    },
    radioButton: {
        marginBottom: 16,
    },
    checkbox: {
        marginBottom: 16,
    }
};

const NewSubscription = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        var plusVAT = (this.state.vatApplicable) ? '+ VAT' : '';
        var annualChecked = this.state.paymentType === 1;
        var monthlyChecked = this.state.paymentType === 2;
        var sixMonthsChecked = this.state.paymentType === 3;

        let sixMonthDiv, monthlyDiv, currencySymbol = ''

        if ('EUR' === this.state.currency) {
            currencySymbol = (<span>&euro;</span>)
        }

        if (this.state.monthlyFee && parseInt(this.state.monthlyFee) !== 0) {
            monthlyDiv = (
                <div className="col-md-3">
                    <div className="subscriptionPackageType">
                        <div className="subscriptionPackageTypeHeader">
                            <h4>Monthly Subscription</h4>
                        </div>
                        <div className="clr"></div>
                        <br/><br/>
                        <div className="flex justify-center">
                            <img src="/images/calendar.png"/>
                        </div>
                        <div className="subscriptionPackageTypeMidTier">
                            <p className="price">{currencySymbol}{this.state.monthlyFee} {plusVAT}</p>
                        </div>
                        <div className="subscriptionPackageTypeBottomTier">
                            <p className="price">Each Month</p>
                        </div>
                        <div className="subscriptionPackageTypeCheckbox">
                            <Checkbox
                                checked={monthlyChecked}
                                onCheck={this._setMonthlyPackage}
                                style={styles.checkbox}
                            />
                        </div>
                    </div>

                </div>
            )
        }

        if (this.state.sixMonthFee && parseInt(this.state.sixMonthFee) !== 0) {
            sixMonthDiv = (
                <div className="col-md-3">
                    <div className="subscriptionPackageType">
                        <div className="subscriptionPackageTypeHeader">
                            <h4>6 Month Subscription</h4>
                        </div>

                        <div className="clr"></div>
                        <br/><br/>

                        <div className="flex justify-center">
                            <img src="/images/calendar.png"/>
                        </div>

                        <div className="subscriptionPackageTypeMidTierRed">
                            <p className="strikeoutPrice"><s>{currencySymbol}{this.state.sixMonthCrossoutDisplay} {plusVAT}</s></p>
                        </div>

                        <div className="subscriptionPackageTypeBottomTier">
                            <p className="price">{currencySymbol}{this.state.sixMonthFee} {plusVAT}</p>
                        </div>

                        <div className="subscriptionPackageTypeCheckbox">
                            <Checkbox
                                checked={sixMonthsChecked}
                                onCheck={this._setSixMonthsPackage}
                                style={styles.checkbox}
                            />
                        </div>
                    </div>
                </div>
            )
        }

        let yearlyStrikeoutFee = (
            <div style={{height: 80}}>
                &nbsp;
            </div>
        );

        if (this.state.crossoutFee !== '0.00') {
            yearlyStrikeoutFee = (
                <div className="subscriptionPackageTypeMidTierRed">
                    <p className="strikeoutPrice"><s>{currencySymbol}{this.state.crossoutFee} {plusVAT}</s></p>
                </div>
            )
        }

        return (
            <div>
                <div className="row">
                    {monthlyDiv}
                    {sixMonthDiv}

                    <div className="col-md-3">
                        <div className="subscriptionPackageType">
                            <div className="subscriptionPackageTypeHeader">
                                <h4>Annual Subscription</h4>
                            </div>

                            <div className="clr"></div>
                            <br/><br/>

                            <div className="flex justify-center">
                                <img src="/images/calendar.png"/>
                            </div>

                            {yearlyStrikeoutFee}

                            <div className="subscriptionPackageTypeBottomTier">
                                <p className="price">{currencySymbol}{this.state.yearlyFee} {plusVAT}</p>
                            </div>
                            <div className="subscriptionPackageTypeCheckbox">
                                <Checkbox
                                    onCheck={this._setAnnualPackage}
                                    checked={annualChecked}
                                    style={styles.checkbox}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    _setMonthlyPackage: function (event, bool) {
        if (bool) {
            FilterActions.setPaymentType(2);
        } else {
            FilterActions.setPaymentType(0);
        }
    },
    _setSixMonthsPackage: function (event, bool) {
        if (bool) {
            FilterActions.setPaymentType(3);
        } else {
            FilterActions.setPaymentType(0);
        }
    },
    _setAnnualPackage: function (event, bool) {
        if (bool) {
            FilterActions.setPaymentType(1);
        } else {
            FilterActions.setPaymentType(0);
        }
    }
});

export default NewSubscription;
