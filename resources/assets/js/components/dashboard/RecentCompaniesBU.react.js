import React from 'react';
import DataList from '../DataTables/DataList.react';
import Paper from 'material-ui/Paper';
import NewOrganisationFilterLink from './links/NewOrganisationFilterLink.react';
import {browserHistory} from 'react-router';

/**
 * This file provided by Facebook is for non-commercial testing and evaluation
 * purposes only. Facebook reserves all rights not expressly granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

"use strict";

import FixedDataTable from 'fixed-data-table';

const {Table, Column, Cell} = FixedDataTable;

class DataListWrapper {
    constructor(indexMap, data) {
        this._indexMap = indexMap;
        this._data = data;
        this.data = data.data;
    }
    getSize() {
        return this._indexMap.length;
    }
    getCollection() {
        return this._data;
    }
    getObjectAt(index) {
        return this._data.getObjectAt(
            this._indexMap[index],
        );
    }
}

const SortTypes = {
    ASC: 'ASC',
    DESC: 'DESC',
};

class SortHeaderCell extends React.Component {
    constructor(props) {
        super(props);

        this._onSortChange = this._onSortChange.bind(this);
    }
    render() {
        var {onSortChange, sortDir, children, ...props} = this.props;
        return (
            <Cell {...props}>
                <a onClick={this._onSortChange}>
                    {children} {sortDir ? (sortDir === SortTypes.DESC ? '↓' : '↑') : ''}
                </a>
            </Cell>
        );
    }
    _onSortChange(e) {
        e.preventDefault();

        if (this.props.onSortChange) {
            let newSortDir = (this.props.sortDir === "DESC") ? "ASC" : "DESC";

            this.props.onSortChange(
                this.props.columnKey,
                newSortDir
            );
        }
    }
}

const styles = {
    paper: {
        padding: 20,
        height: 400
    }
}

class RecentCompanies extends React.Component {
    constructor(props) {
        super(props);

        this._dataList = new DataList(props.companies);

        this._defaultSortIndexes = [];
        let size = this._dataList.getSize();

        for (var index = 0; index < size; index++) {

            this._defaultSortIndexes.push(index);
        }

        let width = props.width;

        this.state = {
            colSortDirs: {},
            width: width,
            filteredDataList: this._dataList,
            sortedDataList: this._dataList
        };

        this._onSortChange = this._onSortChange.bind(this);
        this._onFilterChange = this._onFilterChange.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this._dataList = new DataList(newProps.companies);
        this._defaultSortIndexes = [];

        let size = this._dataList.getSize();

        for (var index = 0; index < size; index++) {

            this._defaultSortIndexes.push(index);
        }

        let sortIndexes = this._defaultSortIndexes.slice();

        sortIndexes.sort((indexB, indexA) => {
            var valueA = this._dataList.getObjectAt(indexA)[1];
            var valueB = this._dataList.getObjectAt(indexB)[1];
            var sortVal = 0;

            if (valueA > valueB) {
                sortVal = 1;
            }
            if (valueA < valueB) {
                sortVal = -1;
            }
            if (sortVal !== 0) {
                sortVal = sortVal * -1;
            }

            return sortVal;
        });

        let sortedDataList = new DataListWrapper(sortIndexes, this._dataList);

        this.setState({
            width: newProps.width,
            filteredDataList: this._dataList,
            sortedDataList: sortedDataList
        });
    }

    componentDidMount() {
        if (document.getElementById("recentCompaniesDiv")) {
            this.setState({
                width: (document.getElementById("recentCompaniesDiv").offsetWidth - 70)
            });
        }
    }

    _onSortChange(columnKey, sortDir) {
        var sortIndexes = this._defaultSortIndexes.slice();

        sortIndexes.sort((indexA, indexB) => {
            var valueA = this._dataList.getObjectAt(indexA)[columnKey];
            var valueB = this._dataList.getObjectAt(indexB)[columnKey];
            var sortVal = 0;

            if (valueA > valueB) {
                sortVal = 1;
            }
            if (valueA < valueB) {
                sortVal = -1;
            }
            if (sortVal !== 0 && sortDir === SortTypes.ASC) {
                sortVal = sortVal * -1;
            }

            return sortVal;
        });


        let sortedDataList = new DataListWrapper(sortIndexes, this._dataList);

        this.setState({

            sortedDataList: sortedDataList,
            colSortDirs: {

                [columnKey]: sortDir,
            },
        });
    }
    _onFilterChange(e) {
        if (!e.target.value) {

            this.setState({
                sortedDataList: this._dataList
            });
        }

        var filterBy = e.target.value.toLowerCase();
        var size = this._dataList.getSize();
        var filteredIndexes = [];

        for (var index = 0; index < size; index++) {

            let company = this._dataList.getObjectAt(index);

            if (
                typeof company[1] !== "undefined" &&
                company[1].toLowerCase().includes(filterBy.toLowerCase())

            ) {

                filteredIndexes.push(index);
            }
        }

        this.setState({

            sortedDataList: new DataListWrapper(filteredIndexes, this._dataList),
        });
    }
    render() {
        var {sortedDataList, colSortDirs} = this.state;

        let table = '';

        let width = 0;

        try {
            width = (document.getElementById("recentCompaniesDiv").offsetWidth - 70);
        } catch (e) {
        }

        if (this.state.width && this.state.width > 0) {
            width = this.state.width;
        }

        table = (
            <Table
                rowHeight={30}
                rowsCount={sortedDataList.getSize()}
                headerHeight={30}
                width={width}
                height={200}
                {...this.props}>
                <Column
                    columnKey="1"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs[1]}>
                            Organisation
                        </SortHeaderCell>
                    }
                    cell={<NewOrganisationFilterLink data={sortedDataList} col="1"/>}
                    width={this.state.width}
                />
            </Table>
        );

        return (
            <Paper style={styles.paper} zDepth={4}>
                <div className="blueHeader">
                    <h4>Recent Companies</h4> <a style={{color: '#fff'}} href="#" onClick={this._viewAll}>view all</a>
                </div>
                <input
                    onChange={this._onFilterChange}
                    placeholder="Search"
                />
                <br/>
                {table}
                <div className="clr"></div>
                <br/><br/><br/>
            </Paper>
        );
    }
    _viewAll(event) {
        event.preventDefault();

        browserHistory.push('/organisations');
    }
}

export default RecentCompanies;
