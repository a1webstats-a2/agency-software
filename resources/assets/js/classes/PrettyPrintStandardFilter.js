import React from 'react';

class PrettyPrintScenarioFilter {

	constructor( props ) {

		this.state = {

			filter : {}
		}
	}

	setFilter( filter ) {

		this.state.filter = filter;
	}

	printPrettyString() {

		let textValue = "";

		if( typeof this.state.filter.storedValue.textValue !== "undefined" ) {

			textValue = this.state.filter.storedValue.textValue;
		
		} else {

			textValue = this.state.filter.storedValue;
		}

		return "standard filter " + this.state.filter.type + " - " + textValue;
	}
}

export default PrettyPrintScenarioFilter;