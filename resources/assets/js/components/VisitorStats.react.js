import React from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Paper from 'material-ui/Paper';

const styles = {
    paper: {
        padding: 20,
        height: 400
    },
    refreshIndicator: {
        display: 'inline-block',
        position: 'relative',
    }
}

function getCurrentState(props) {
    return {
        stats: props.stats,
        isApplicationResting: props.isApplicationResting
    }
}

const VisitorStats = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        var update = false;
        if (this.props.isApplicationResting !== nextProps.isApplicationResting) {
            update = true;
        }

        if (typeof this.props.stats.total_visitors === "undefined" &&
            typeof nextProps.stats.total_visitors !== "undefined") {

            update = true;

        } else if (typeof nextProps.stats.total_visitors !== "undefined" &&
            typeof this.props.stats.total_visitors !== "undefined") {

            if (parseInt(this.props.stats.total_visitors[0][0]) !==
                parseInt(nextProps.stats.total_visitors[0][0])) {

                update = true;
            }
        }

        return update;
    },
    render: function () {
        if (!this.state.isApplicationResting) {
            return (
                <div className="visitorDetails">
                    <Paper style={styles.paper} zDepth={4}>
                        <div className="blueHeader">
                            <h4>Visitor Details</h4>
                        </div>
                        <RefreshIndicator
                            size={70}
                            left={0}
                            top={20}
                            status="loading"
                            style={styles.refreshIndicator}
                        />
                        <br/><br/>
                    </Paper>
                </div>
            )
        }

        let totalVisits = null;

        if (typeof this.state.stats.total_visits !== "undefined") {
            totalVisits = (
                <tr>
                    <td>Total Visits</td>
                    <td>{this.state.stats.total_visits}</td>
                </tr>
            )
        }

        if (typeof this.state.stats !== "undefined" && typeof this.state.stats.unique_visitors !== "undefined") {
            return (
                <div className="visitorDetails">
                    <Paper style={styles.paper} zDepth={4}>
                        <div className="blueHeader">
                            <h4>Visitor Details</h4>
                        </div>
                        <br/>
                        <table className="table">
                            <tbody>
                            <tr>
                                <td>New Visitors</td>
                                <td>{this.state.stats.unique_visitors}</td>
                            </tr>
                            <tr>
                                <td>Return Visitors</td>
                                <td>{this.state.stats.return_visitors}</td>
                            </tr>
                            <tr>
                                <td>Total Unique Visitors</td>
                                <td>{this.state.stats.total_visitors}</td>
                            </tr>
                            <tr>
                                <td>Total Page Views</td>
                                <td>{this.state.stats.total_page_count}</td>
                            </tr>
                            {totalVisits}
                            <tr>
                                <td>Average Pages / Per Visit</td>
                                <td>{this.state.stats.average_pages_pervisits}</td>
                            </tr>
                            <tr>
                                <td>% New Visitors</td>
                                <td>{parseFloat(this.state.stats.percentage_new_visits).toFixed(2)}%</td>
                            </tr>
                            </tbody>
                        </table>
                    </Paper>
                </div>
            );
        } else {
            return (
                <div className="visitorStats">
                    <Paper style={styles.paper} zDepth={4}>
                        <div className="blueHeader">
                            <h4>Visitor Details</h4>
                        </div>
                    </Paper>
                </div>
            );
        }
    }
});

export default VisitorStats;
