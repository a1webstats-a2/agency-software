import React from 'react';

function getCurrentState() {

	return {};
}

class EventTracking extends React.Component {

	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( newProps ) {

		this.setState( getCurrentState( newProps ) );
	}

	render() {

		const preText = (

<pre>{`
document.getElementById( "myBtn" ).addEventListener( "click", function( event ){

    event.preventDefault();
    a2CustomEvent( "description for your event" );
})
`}
			</pre>
		)

		return (

			<div>

				<div className="row">
						
					<div className="col-md-12">
						
						<h3>Event Tracking</h3><br />
					</div>

				</div>

				<div className="row">

					<div className="col-md-12">

						<p>You can easily track custom events by using the following function call
							in your javascript code, like so:</p>
						<br />
						<pre>

							a2CustomEvent( "Your custom message... for example... user clicked on download link" );

						</pre>

						<div className="clr"></div><br />
						<p>In the following example, a request is made to A1WebStats whenever the anchor tag 
							with id 'myBtn' is clicked:</p>
						<div className="clr"></div><br />

						{preText}

						<div className="clr"></div><br />

						<p>When clicked, the event should look like this in your results.</p>
						<div className="clr"></div><br />

						<img src="/images/eventExample.png" />

						<div className="clr"></div><br /><br /><br /><br />

					</div>
				</div>
			</div>
		)
	}
}

export default EventTracking;