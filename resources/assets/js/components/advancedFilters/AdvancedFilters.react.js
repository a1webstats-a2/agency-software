import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import NavBar from '../navigation/NavBar.react';
import NotificationsStore from '../../stores/NotificationsStore';
import AdvancedFiltersStore from '../../stores/AdvancedFiltersStore';
import UserTips from '../UserTips.react';
import UserTipActions from '../../actions/UserTipActions';
import SnackbarA1 from '../Snackbar.react';
import Scenario from './Scenario.react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import AdvancedFilterActions from '../../actions/AdvancedFilterActions';
import FilterChoices from './FilterChoices.react';
import GhostScenario from './GhostScenario.react';
import Slider from 'material-ui/Slider';
import ManageSavedScenarios from './ManageSavedScenarios.react';
import Checkbox from 'material-ui/Checkbox';
import FilterActions from '../../actions/FilterActions';
import TextField from 'material-ui/TextField';

function getCurrentState() {
    let allFilters = FiltersStore.getFilters();

    return {
        snackbarSettings: FiltersStore.getSnackbarSettings(),
        managedSavedScenariosOpen: FiltersStore.checkLightboxOpen("manageScenarios", -1),
        savedScenarios: AdvancedFiltersStore.getSavedScenarios(),
        updatingAnalytics: FiltersStore.getUpdatingAnalytics(),
        paginationCurrentPage: AdvancedFiltersStore.getPaginationCurrentPage(),
        looseFilters: AdvancedFiltersStore.getLooseFilters(),
        selectAnyThatMatch: AdvancedFiltersStore.getSelectAnyThatMatch(),
        timeSettings: AdvancedFiltersStore.getTimeSettings(),
        searchText: AdvancedFiltersStore.getSearchText(),
        setCurrentFilters: AdvancedFiltersStore.getSetChoices(),
        currentFilterChoices: AdvancedFiltersStore.getCurrentFilterChoices(),
        filterChoiceType: AdvancedFiltersStore.getFilterChoiceType(),
        filterChoicesOpen: AdvancedFiltersStore.avCheckLightboxOpen('filterChoices', -1),
        settings: FiltersStore.getAllSettings(),
        isApplicationResting: FiltersStore.isApplicationResting(),
        displayFancyFilters: FiltersStore.showFancyFilters(),
        latestAddedFilters: FiltersStore.getLatestAddedFilters(),
        filters: FiltersStore.getFilters(),
        scenarios: AdvancedFiltersStore.getScenarios(),
        analytics: FiltersStore.getAnalyticsData(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            allFilters: allFilters,
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        },
        saveAsTemplateOpen: FiltersStore.checkLightboxOpen('createAdvancedFiltersTemplate'),
        newTemplateName: AdvancedFiltersStore.getNewTemplateName(),
    }
}

function ObjectLength(object) {
    var length = 0;
    for (var key in object) {
        if (object.hasOwnProperty(key)) {
            ++length;
        }
    }
    return length;
}

const AdvancedFilters = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentDidMount: function () {
        AdvancedFiltersStore.addChangeListener(this._onChange);
        FiltersStore.addChangeListener(this._onChange);
        AdvancedFilterActions.firstLoad();
    },
    componentWillUnmount: function () {
        AdvancedFiltersStore.removeChangeListener(this._onChange);
        FiltersStore.removeChangeListener(this._onChange);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        var scenarios = [];
        var scenarioIndex = 0;

        let returnVisitorsOnlyValue, newVisitorsOnlyValue = false;

        if ('returnVisitorsOnly' in this.state.looseFilters) {
            returnVisitorsOnlyValue = this.state.looseFilters.returnVisitorsOnly.storedValue.value;
            newVisitorsOnlyValue = this.state.looseFilters.newVisitorsOnly.storedValue.value
        }

        for (var key in this.state.scenarios) {
            var scenario = this.state.scenarios[key];
            var setFilters = (typeof this.state.setCurrentFilters[scenario.id] !== "undefined") ? this.state.setCurrentFilters[scenario.id] : {};

            scenarioIndex++;

            scenarios.push(
                <Scenario
                    count={ObjectLength(this.state.scenarios)}
                    setFilters={setFilters}
                    scenario={scenario}
                    savedScenarios={this.state.savedScenarios}
                    ghost={false}
                    index={scenarioIndex}
                    filterChoiceType={this.state.filterChoiceType}
                    key={key}
                />
            );
        }

        var timerSettingActions = [
            <RaisedButton label="Cancel" secondary={true} onClick={this._closeTimerSettings}
                          style={{marginRight: 20}}/>,
            <RaisedButton label="Set" primary={true} onClick={this._setTimerSettings}/>
        ]

        var choices = [];
        var choiceType = '';

        if (this.state.filterChoiceType) {
            choiceType = this.state.filterChoiceType.toLowerCase();
            switch (this.state.filterChoiceType.toLowerCase()) {
                case "visitortypefilters" :
                    choices = [
                        [1, "Business"],
                        [2, "Education"],
                        [6, "Unknown"]
                    ];

                    break;
                case "counties" :
                    choices = this.state.analytics.counties;

                    break;
                case "firstpagevisited" :
                    choices = this.state.analytics.entryPages;

                    break;
                case "pagevisited" :
                    choices = this.state.analytics.pages;

                    break;
                case "lastpagevisited" :
                    choices = this.state.analytics.lastPages;

                    break;
                case "campaignsource" :
                    choices = this.state.analytics.campaignSources;

                    break;
                case "campaignmedium" :
                    choices = this.state.analytics.campaignMediums;

                    break;
                case "campaignname" :
                    choices = this.state.analytics.mailCampaigns;

                    break;
                case "geolocation":
                    choices = [
                        [1, "Allowed geolocation"]
                    ];

                    break;
                case "organisation" :
                    choices = this.state.analytics.organisations;

                    break;
                case "referrer" :
                    choices = this.state.analytics.referrers;

                    break;
                case "keyword" :
                    choices = this.state.analytics.keywords;

                    break;
                case "town" :
                    choices = this.state.analytics.cities;

                    break;
                case "countries" :
                    choices = this.state.analytics.visitorLocations;

                    break;
                case "source" :
                    choices = [
                        [1, "PPC"],
                        [2, "Organic"],
                        [3, "Direct"]
                    ];

                    break;
                case "devicetype":
                    choices = [
                        [1, 'Desktop'],
                        [2, 'Tablet'],
                        [3, 'Mobile'],
                    ];

                    break;
                case "tags" :
                    choices = this.state.analytics.tags;

                    break;
                case "events":
                    choices = this.state.analytics.events;

                    break;
            }
        }

        if (typeof choices === "undefined") {
            return (<div></div>);
        }

        var allowNext = (choices.length > ((this.state.paginationCurrentPage + 1) * 100));
        var allowPrev = (this.state.paginationCurrentPage > 0);

        let selectAllDisabled = false;
        let deselectAllDisabled = false;

        if (choiceType === "ip") {
            selectAllDisabled = true;
            deselectAllDisabled = true;
        }

        const createTemplateOptions = [
            <FlatButton
                onTouchTap={this._createNewTemplate}
                label="Save"
                primary={true}
            />,
            <FlatButton
                onTouchTap={this._closeLightbox}
                label="Close"
                secondary={true}
            />
        ]

        const actions = [
            <FlatButton
                onTouchTap={this._choicesPaginationPrev}
                label="Prev"
                primary={true}
                disabled={!allowPrev}
            />,
            <FlatButton
                onTouchTap={this._choicesPaginationNext}
                label="Next"
                primary={true}
                disabled={!allowNext}
            />,
            <FlatButton
                onTouchTap={this._selectAll}
                label="Select All"
                disabled={selectAllDisabled}
                secondary={true}
            />,
            <FlatButton
                label="Deselect All"
                disabled={deselectAllDisabled}
                secondary={true}
                onTouchTap={this._deselectAll}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this._cancel}
            />,
            <FlatButton
                label="Set"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._setChoices}
            />
        ];

        var timeSettings = this.state.timeSettings;

        if (typeof timeSettings.choices[timeSettings.id] !== "undefined") {
            timeSettings = timeSettings.choices[timeSettings.id];
        }

        var minNumberOfPages = 1;

        if (typeof this.state.looseFilters.minNumberOfPages !== "undefined") {
            minNumberOfPages = this.state.looseFilters.minNumberOfPages.storedValue.value
        }

        var minSessionDuration = 0;

        if (typeof this.state.looseFilters.minSessionDuration !== "undefined") {
            minSessionDuration = this.state.looseFilters.minSessionDuration.storedValue.value
        }

        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-12" id="results">
                            <div className="row">
                                <div id="advancedFiltersTopBar">
                                    <div className="col-md-2">
                                        <h3>Advanced Filters</h3><br/>
                                        <a href="#" onClick={(event) => this._showHelp(event)}>
                                            <img src="/images/help.png" alt="Help symbol" style={{marginRight: 15}}/>
                                            Help
                                        </a>
                                    </div>
                                    <div className="col-md-2">
                                        <Checkbox
                                            style={{marginTop: 25}}
                                            onCheck={this._setNewVisitorsOnlyFilter}
                                            checked={newVisitorsOnlyValue}
                                            label="New visitors only"
                                        />
                                        <div className="clr"></div>
                                        <Checkbox
                                            style={{marginTop: 25}}
                                            checked={returnVisitorsOnlyValue}
                                            onCheck={this._setReturnVisitorsOnlyFilter}
                                            label="Return visitors only"
                                        />
                                    </div>
                                    <div className="col-md-2">
                                        <div style={{marginTop: 25}}>
                                            <p>Minimum number of page visits: {minNumberOfPages}</p>
                                            <Slider
                                                min={0}
                                                max={5}
                                                step={1}
                                                defaultValue={1}
                                                value={minNumberOfPages}
                                                onDragStop={this._setMinNumberOfPages}
                                                onChange={this._setStateMinNumberOfPages}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-2">
                                        <div style={{marginTop: 25}}>
                                            <p>Minimum visit duration (minutes): {minSessionDuration}</p>
                                            <Slider
                                                min={0}
                                                max={5}
                                                step={1}
                                                defaultValue={1}
                                                onChange={this._setStateMinSessionDuration}
                                                value={minSessionDuration}
                                                onDragStop={this._setMinSessionDuration}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-2">
                                        <RaisedButton buttonStyle={{marginBottom: 10}}
                                                      primary={true}
                                                      fullWidth={true}
                                                      label="Show Data"
                                                      onClick={this._showData}/>
                                        <RaisedButton buttonStyle={{marginBottom: 10}} primary={true} fullWidth={true}
                                                      label="Save as Template" onClick={this._openSaveAsTemplate}/>
                                        <RaisedButton buttonStyle={{background: '#41DA49'}} labelStyle={{color: '#fff'}}
                                                      style={{marginBottom: 10}} fullWidth={true} label="Export"
                                                      primary={true} onClick={this._export}/>
                                        <RaisedButton buttonStyle={{background: '#B3160E'}} labelStyle={{color: '#fff'}}
                                                      fullWidth={true} label="Reset Filters" onClick={this._reset}/>
                                    </div>
                                </div>
                                <div id="advancedFiltersSpacerDiv"></div>
                                <div className="clr"></div>
                            </div>

                            <div className="clr"></div>
                            <br/>

                            {scenarios}

                            <GhostScenario savedScenarios={this.state.savedScenarios}/>
                            <div className="clr"></div>
                        </div>
                    </div>
                </div>
                <Dialog
                    title="Time Options"
                    modal={true}
                    actions={timerSettingActions}
                    autoScrollBodyContent={true}
                    open={this.state.timeSettings.dialogOpen}
                >
                    <br/><p>Set criteria for time spent on page</p>
                    <select value={timeSettings.type} className="form-control" onChange={this._setTimerMoreLess}>
                        <option value="more">More than</option>
                        <option value="less">Less than</option>
                    </select>
                    <select onChange={this._setTimerSeconds} value={timeSettings.seconds} className="form-control">
                        <option value="10">10 seconds</option>
                        <option value="30">30 seconds</option>
                        <option value="60">1 minute</option>
                        <option value="120">2 minutes</option>
                        <option value="300">5 minutes</option>
                        <option value="600">10 minutes</option>
                    </select>
                </Dialog>
                <Dialog
                    title="Select all desired values"
                    actions={actions}
                    modal={true}
                    autoScrollBodyContent={true}
                    open={this.state.filterChoicesOpen}
                    onRequestClose={this._handleClose}
                >
                    <FilterChoices
                        choiceType={choiceType}
                        updatingAnalytics={this.state.updatingAnalytics}
                        paginationCurrentPage={this.state.paginationCurrentPage}
                        isApplicationResting={this.state.isApplicationResting}
                        selectAnyThatMatch={this.state.selectAnyThatMatch}
                        searchText={this.state.searchText}
                        selection={this.state.currentFilterChoices}
                        choices={choices}/>
                </Dialog>

                <Dialog
                    title="Save as Template"
                    modal={true}
                    autoScrollBodyContent={true}
                    open={this.state.saveAsTemplateOpen}
                    onRequestClose={this._handleClose}
                    actions={createTemplateOptions}
                >
                    <TextField
                        id="name"
                        name="name"
                        hintText="Template Name"
                        onChange={this._updateNewTemplateName}
                        floatingLabelText="Template Name"
                        underlineShow={false}
                        value={this.state.newTemplateName}
                    />
                </Dialog>
                <ManageSavedScenarios
                    open={this.state.managedSavedScenariosOpen}
                    savedScenarios={this.state.savedScenarios}
                />
                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>
                <UserTips/>
            </div>
        )
    },
    _showHelp: function (event) {
        event.preventDefault();
        UserTipActions.showSpecificHelpItem(6);
    },
    _reset: function () {
        AdvancedFilterActions.resetFilters();
    },
    _setNewVisitorsOnlyFilter: function (event, isChecked) {
        AdvancedFilterActions.editAdvancedFilter({
            id: 0,
            storedValue: {
                id: 0,
                name: 0,
                value: isChecked,
                textValue: isChecked
            },
            type: 'newVisitorsOnly',
            round: 0
        })

        if (isChecked) {
            AdvancedFilterActions.editAdvancedFilter({
                id: 0,
                storedValue: {
                    id: 0,
                    name: 0,
                    value: false,
                    textValue: false
                },
                type: 'returnVisitorsOnly',
                round: 0
            })
        }
    },
    _setReturnVisitorsOnlyFilter: function (event, isChecked) {
        AdvancedFilterActions.editAdvancedFilter({
            id: 0,
            storedValue: {
                id: 0,
                name: 0,
                value: isChecked,
                textValue: isChecked
            },
            type: 'returnVisitorsOnly',
            round: 0
        })

        if (isChecked) {
            AdvancedFilterActions.editAdvancedFilter({
                id: 0,
                storedValue: {
                    id: 0,
                    name: 0,
                    value: false,
                    textValue: false
                },
                type: 'newVisitorsOnly',
                round: 0
            })
        }
    },
    _closeTimerSettings: function () {
        AdvancedFilterActions.closeTimerSettings();
    },
    _choicesPaginationPrev: function () {
        AdvancedFilterActions.prevChoicesPage();
    },
    _choicesPaginationNext: function () {
        AdvancedFilterActions.nextChoicesPage();
    },
    _export: function () {
        AdvancedFilterActions.straightToExport();
    },
    _setStateMinSessionDuration: function (event, newValue) {
        this.setState({
            minSessionDuration: newValue
        })
    },
    _setStateMinNumberOfPages: function (event, newValue) {
        this.setState({
            minNumberOfPages: newValue
        })
    },
    _setMinSessionDuration: function (event) {
        AdvancedFilterActions.editAdvancedFilter({
            id: this.state.minSessionDuration,
            storedValue: {
                id: this.state.minSessionDuration,
                name: 0,
                value: this.state.minSessionDuration,
                textValue: this.state.minSessionDuration
            },
            type: 'minSessionDuration',
            round: 0
        });
    },
    _setMinNumberOfPages: function (event) {
        AdvancedFilterActions.editAdvancedFilter({
            id: this.state.minNumberOfPages,
            storedValue: {
                id: this.state.minNumberOfPages,
                name: 0,
                value: this.state.minNumberOfPages,
                textValue: this.state.minNumberOfPages
            },
            type: 'minNumberOfPages',
            round: 0
        });
    },
    _setTimerSeconds: function (event) {
        var timeSettings = this.state.timeSettings;
        timeSettings.seconds = event.target.value;

        AdvancedFilterActions.updateTimeSettings(timeSettings);
    },
    _setTimerSettings: function () {
        AdvancedFilterActions.setTimerSettings();
    },
    _setTimerMoreLess: function (event) {
        var timeSettings = this.state.timeSettings;
        timeSettings.type = event.target.value;
        AdvancedFilterActions.updateTimeSettings(timeSettings);
    },
    _selectAll: function () {
        AdvancedFilterActions.selectAll();
    },
    _deselectAll: function () {
        AdvancedFilterActions.deselectAll();
    },
    _showData: function () {
        AdvancedFilterActions.showData();
    },
    _setChoices: function () {
        AdvancedFilterActions.setChoices();
    },
    _cancel: function () {
        AdvancedFilterActions.cancelCreateFilter();
    },
    _handleClose: function () {
        AdvancedFilterActions.saveFilter();
    },
    _createScenario: function () {
        AdvancedFilterActions.createNewScenario();
    },
    _openSaveAsTemplate: function () {
        FilterActions.setLightbox({
            type: 'createAdvancedFiltersTemplate',
            id: -1
        })
    },
    _updateNewTemplateName: function (event) {
        AdvancedFilterActions.setNewTemplateName(event.target.value);
    },
    _createNewTemplate: function () {
        AdvancedFilterActions.createNewTemplate();
    },
    _closeLightbox: function () {
        FilterActions.closeLightbox();
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }
});

export default AdvancedFilters;
