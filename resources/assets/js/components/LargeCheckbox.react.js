import React from "react";
import Checkbox from 'material-ui/Checkbox';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import a1Theme from "../themes/a1Theme";

const styles = {
    block: {
        maxWidth: 250,
    },
    checkbox: {
        marginBottom: 16,
    },
};

const LargeCheckbox = React.createClass({
    render: function () {
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(a1Theme)}>
                <Checkbox
                    style={styles.checkbox}
                    name={this.props.name}
                    label={this.props.label}
                />
            </MuiThemeProvider>
        )
    }
});

export default LargeCheckbox;