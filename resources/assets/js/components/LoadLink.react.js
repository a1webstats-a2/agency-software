import React from 'react';
import FiltersStore from '../stores/FiltersStore';
import NotificationsStore from '../stores/NotificationsStore';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import SessionResult from './sessionResult/SessionResult.react';
import PreviousVisits from './sessionResult/PreviousVisits.react';
import CompanyData from './companyData/CompanyData.react';
import CreateNote from './sessionResult/CreateNote.react';
import AssignToTeamMember from './sessionResult/AssignToTeamMember.react';
import EditCompanyRecord from './companyData/EditCompanyRecord.react';
import CreateClientTag from './prospecting/CreateClientTag.react';
import NavBar from './navigation/NavBar.react';
import SnackbarA1 from './Snackbar.react';
import Footer from './Footer.react';
import PaginationOptions from './PaginationOptions.react';
import AdditionalCompanyData from './companyData/AdditionalCompanyData.react';
import SendOrganisationViaEmail from './organisationsDashboard/SendOrganisationViaEmail.react';
import FilterActions from '../actions/FilterActions';
import A2GoogleMap from './A2GoogleMap.react';

const refreshIndicatorStyle = {
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
        float: 'left'
    },
};

function getCurrentState(props) {

    var resultsMaster = FiltersStore.getResults();

    return {
        isSearchingForPossibleOptions: FiltersStore.isSearchingForActiveRecordOptions(),
        chosenPossibleOption: FiltersStore.getActiveRecordChosenOption(),
        checkedForPossibleOptions: FiltersStore.checkedForActiveRecordPossibleOptions(),
        possibleOptions: FiltersStore.getActiveRecordSuggestions(),

        clientTypesByIndex: FiltersStore.getClientTypesByIndex(),
        scenariosFilterObj: FiltersStore.getScenarioFiltersObj(),
        currentMapDetails: FiltersStore.getCurrentMapDetails(),
        popoverObj: FiltersStore.getPopoverSettingsNew(),
        blockedOrganisations: FiltersStore.getBlockedList(),
        additionalCompanyData: FiltersStore.getAdditionalCompanyData(),
        additionalCompanyDataOpen: FiltersStore.checkLightboxOpen('additionalCompanyData', -1),
        sendOrganisationViaEmailOpen: FiltersStore.checkLightboxOpen('sendOrganisationViaEmailOpen', 1),
        sendEmailToTeamMembers: FiltersStore.getSendEmailToTeamMembers(),
        exportSelection: FiltersStore.getExportSelection(),
        latestAddedFilters: FiltersStore.getLatestAddedFilters(),
        displayFancyFilters: FiltersStore.showFancyFilters(),
        filters: FiltersStore.getFilters(),
        snackbarSettings: FiltersStore.getSnackbarSettings(),
        orderBy: FiltersStore.getOrderBy(),
        previousVisitsObj: FiltersStore.getPreviousVisits(),
        isLoadingPreviousVisits: FiltersStore.isLoadingPreviousHistory(),
        isApplicationResting: FiltersStore.isApplicationResting(),
        paginationData: FiltersStore.getPaginationTotals(),
        resultsFound: FiltersStore.getResultsFound(),
        displayedColumns: FiltersStore.getDisplayedColumns(),
        clientTags: FiltersStore.getClientTypes(),
        settings: FiltersStore.getAllSettings(),
        displayBy: FiltersStore.getDisplayBy(),
        results: resultsMaster.results,
        relationships: resultsMaster.relationships,
        fancyFiltersData: {},
        currentMapOpen: FiltersStore.checkLightboxOpen('currentMapDetails', 1),
        createNoteOpen: FiltersStore.checkLightboxOpen('createNote', 1),
        previousHistory: {
            open: FiltersStore.checkLightboxOpen('previousVisits', 1),
            prevResults: FiltersStore.getPreviousHistory(),
            orgName: FiltersStore.getPreviousHistoryOrganisationName()
        },
        companyData: {
            companyData: FiltersStore.getOrganisationData(),
            dueDilData: false,
            open: FiltersStore.checkLightboxOpen('companyData', 0)
        },
        editCompanyData: {
            activeRecord: FiltersStore.getActiveRecord(),
            open: FiltersStore.checkLightboxOpen('editCompanyRecord', 1),
        },
        notes: FiltersStore.getNotes(),
        assignedData: {
            currentTrackingObj: FiltersStore.getCurrentTrackingObj(),
            open: FiltersStore.checkLightboxOpen('assignToTeamMember', 1),
            teamMembers: FiltersStore.getTeamMembers()
        },
        createNewClientTagOpen: FiltersStore.checkLightboxOpen('createNewClientTag', 1),
        popoverOpen: FiltersStore.getPopoverID(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        },
        looseFilterSetDisplay: FiltersStore.getLooseFilterSetDisplay(),
        excludeSelection: FiltersStore.getExcludeSelection()
    }
}

var LoadLink = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props, false);
    },
    componentDidMount: function () {
        var queryParams = window.location.pathname.split('/');
        FilterActions.loadLink({
            type: queryParams[2],
            id: queryParams[3]
        })

        FiltersStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        FiltersStore.removeChangeListener(this._onChange);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },

    shouldComponentUpdate: function (nextProps, nextState) {

        if (JSON.stringify(this.state.filters) !== JSON.stringify(nextState.filters)) {

            return true;
        }

        if (this.state.popoverObj !== nextState.popoverObj) {

            return true;
        }

        if (this.state.relationships !== nextState.relationships) {

            return true;
        }

        if (this.state.orderBy !== nextState.orderBy) {

            return true;
        }

        if (JSON.stringify(this.state.exportSelection) !== JSON.stringify(nextState.exportSelection)) {

            return true;
        }

        if (this.state.popoverOpen !== nextState.popoverOpen) {
            return true;
        }

        if (this.state.notificationsData.numberOfNew !== nextState.notificationsData.numberOfNew) {
            return true;
        }

        if (this.state.snackbarSettings !== nextState.snackbarSettings) {
            return true;
        }

        if (this.state.additionalCompanyData !== nextState.additionalCompanyData) {
            return true;
        }

        if (this.state.additionalCompanyDataOpen !== nextState.additionalCompanyDataOpen) {
            return true;
        }

        if (this.state.previousVisitsObj !== nextState.previousVisitsObj) {
            return true;
        }

        if (this.state.companyData.open !== nextState.companyData.open) {
            return true;
        }

        if (this.state.editCompanyData.activeRecord.data.use_organisation_name !== nextState.editCompanyData.activeRecord.data.use_organisation_name) {
            return true;
        }

        if (this.state.editCompanyData.open !== nextState.editCompanyData.open) {
            return true;
        }

        if (JSON.stringify(this.state.relationships) !== JSON.stringify(nextState.relationships)) {
            return true;
        }

        if (JSON.stringify(this.state.results) !== JSON.stringify(nextState.results)) {
            return true;
        }

        if (this.state.isApplicationResting !== nextState.isApplicationResting) {

            return true;
        }

        if (this.state.createNewClientTagOpen !== nextState.createNewClientTagOpen) {

            return true;
        }

        if (this.state.assignedData.open !== nextState.assignedData.open) {

            return true;
        }

        if (this.state.looseFilterSetDisplay !== nextState.looseFilterSetDisplay) {

            return true;
        }

        if (this.state.notes !== nextState.notes) {

            return true;
        }

        if (this.state.blockedOrganisations !== nextState.blockedOrganisations) {

            return true;
        }

        if (this.state.createNoteOpen !== nextState.createNoteOpen) {

            return true;
        }

        if (this.state.previousHistory !== nextState.previousHistory) {

            return true;
        }

        return false;
    },

    render: function () {

        var resultItems = this.state.results.map(function (result, key) {

            var excludeMe = (this.state.excludeSelection.indexOf(result.organisationid) === -1) ? false : true;

            return <SessionResult
                key={key}
                myKey={key}
                clientTypesByIndex={this.state.clientTypesByIndex}
                assignedTo={result.assigned_user}
                popoverOpenID={this.state.popoverOpen}
                blockedOrganisations={this.state.blockedOrganisations}
                settings={this.state.settings}
                relationships={this.state.relationships}
                data={result}
                exportMe={FiltersStore.checkSelectedForExport(result.id)}
                tags={this.state.clientTags}
                excludeMe={excludeMe}
                displayedColumns={this.state.displayedColumns}
                popoverObj={this.state.popoverObj}/>

        }.bind(this));

        var noResultsText = "";

        if (this.state.paginationData.total === 0 && this.state.isApplicationResting) {

            noResultsText = (

                <div className="row">
                    <div className="alert alert-danger" id="resultsUpdatingMessage">
                        Sorry, there are <strong>no results</strong> matching your search criteria
                    </div>
                </div>
            )

        } else if (!this.state.isApplicationResting) {

            noResultsText = (

                <div className="row">
                    <div style={refreshIndicatorStyle.container} id="resultsUpdatingMessage"
                         className="alert alert-success">
                        <RefreshIndicator
                            size={40}
                            left={0}
                            style={refreshIndicatorStyle.refresh}
                            top={0}
                            loadingColor={"#3c763d"}
                            status="loading"
                        />
                        <p style={{float: 'left', marginLeft: 30, marginTop: 12}}>Results are updating, please wait</p>

                    </div>
                </div>
            )

        } else {

            var pageNum = (Math.ceil(this.state.paginationData.offset / this.state.paginationData.returnAmount)) + 1;

            noResultsText = (

                <div className="row">
                    <div className="alert alert-success" id="resultsUpdatingMessage">
                        Page <strong>{pageNum}</strong> of <strong>{this.state.paginationData.numPages}</strong> - {this.state.paginationData.total} results
                    </div>
                </div>
            )
        }

        var hiddenOrganisations = this.state.blockedOrganisations.map(function (blocked, i) {

            return (

                <div key={i}><p className="hiddenOrganisation">{blocked} hidden</p><br/></div>

            );
        })

        var currentMapActions = [

            <FlatButton
                label="Close"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._closeCurrentMap}
            />
        ];

        return (

            <div className="reportBuilder">

                <NavBar data={this.state.notificationsData}/>

                <div className="clr"></div>

                <div className="container mainContainer">

                    <div className="row">

                        <div className="col-md-3">

                            <div className="displayOptions">

                                <PaginationOptions orderBy={this.state.orderBy}
                                                   paginationData={this.state.paginationData}
                                                   isApplicationResting={this.state.isApplicationResting}/>

                            </div>

                            <div className="clr"></div>
                        </div>


                        <div className="col-md-8 col-md-offset-1" id="results">

                            <h3>Results</h3><br/>

                            {hiddenOrganisations}

                            <div className="clr"></div>


                            {noResultsText}

                            <div className="row">

                                {resultItems}

                            </div>
                        </div>
                    </div>
                </div>

                <Footer
                    looseFilterSetDisplay={this.state.looseFilterSetDisplay}
                    scenariosFilters={this.state.scenariosFilterObj}
                    isApplicationResting={this.state.isApplicationResting}
                    settings={this.state.settings}
                    filters={this.state.filters}
                    display={this.state.displayFancyFilters}
                    latestAddedFilters={this.state.latestAddedFilters}
                />

                <Dialog

                    title={this.state.currentMapDetails.locationString}
                    actions={currentMapActions}
                    modal={false}
                    open={this.state.currentMapOpen}
                    onRequestClose={this._closeCurrentMap}
                >
                    <A2GoogleMap
                        open={this.state.currentMapOpen}
                        organisationName={this.state.currentMapDetails.organisationName}
                        isGeoLocated={this.state.currentMapDetails.geolocated}
                        locationString={this.state.currentMapDetails.locationString}
                        organisation={this.state.currentMapDetails.organisationName}
                        longitude={this.state.currentMapDetails.longitude}
                        latitude={this.state.currentMapDetails.latitude}/>
                </Dialog>

                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>

                <PreviousVisits previousVisitsObj={this.state.previousVisitsObj}
                                isLoading={this.state.isLoadingPreviousVisits} settings={this.state.settings}
                                previousVisits={this.state.previousHistory}/>

                <CompanyData companyData={this.state.companyData}/>

                <EditCompanyRecord
                    isSearchingForPossibleOptions={this.state.isSearchingForPossibleOptions}
                    chosenPossibleOption={this.state.chosenPossibleOption}
                    checkedForPossibleOptions={this.state.checkedForPossibleOptions}
                    possibleOptions={this.state.possibleOptions}
                    editCompanyRecordData={this.state.editCompanyData}
                    storeType="reportBuilder"
                />

                <CreateNote notes={this.state.notes} open={this.state.createNoteOpen}/>

                <AssignToTeamMember storeType="reportBuilder" assignedData={this.state.assignedData}/>

                <CreateClientTag open={this.state.createNewClientTagOpen}/>

                <AdditionalCompanyData open={this.state.additionalCompanyDataOpen}
                                       additionalCompanyData={this.state.additionalCompanyData}/>

                <SendOrganisationViaEmail sendEmailToTeamMembers={this.state.sendEmailToTeamMembers}
                                          teamMembers={this.state.assignedData.teamMembers}
                                          open={this.state.sendOrganisationViaEmailOpen}
                                          settings={this.state.settings}/>

            </div>
        );
    },

    _closeCurrentMap: function () {

        FilterActions.closeLightbox();
    },

    _startExport: function () {

        FilterActions.openExport();
    },

    _onChange: function () {

        this.setState(getCurrentState());
    }
});

export default LoadLink;