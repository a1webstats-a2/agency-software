import React from 'react';
import Organisation from './Organisation.react';

function getCurrentState( props ) {

	return {

		clientTypesByIndex 		: props.clientTypesByIndex,
		quickExportResults 		: props.quickExportResults,
		quickExportSearchString : props.quickExportSearchString,
		landingPageNo 			: props.landingPageNo,
		searchString 			: props.searchString, 
		organisations 			: props.organisations,
		excludeSelection 		: props.excludeSelection,
		settings 				: props.settings,
		selection 				: props.selection,
		selectionType 			: props.selectionType,
		exportSelection 		: props.exportSelection,
		displayPerPage 			: props.displayPerPage,
		relationships 	 		: props.relationships
	}
}

function getPaginatedResults( organisations, landingPageNo, perPage = 10 ) {

	var startPage 	= landingPageNo * perPage;
	return organisations.splice( startPage, perPage );
}

var OrganisationsList = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.organisations !== nextProps.organisations ) {

			return true;
		}

		if( this.props.landingPageNo !== nextProps.landingPageNo ) {

			return true;
		}

		if( this.props.selectionType !== nextProps.selectionType ) {

			return true;
		}

		if( this.props.quickExportSearchString !== nextProps.quickExportSearchString ) {

			return true;
		}

		if( this.props.relationships !== nextProps.relationships ) {

			return true;
		}

		if( this.props.displayPerPage !== nextProps.displayPerPage ) {

			return true;
		}

		if( this.props.quickExportResults !== nextProps.quickExportResults ) {

			return true;
		}


		if( this.props.excludeSelection !== nextProps.excludeSelection ) {

			return true;
		}

		if( this.props.settings !== nextProps.settings ) {

			return true;
		}

		if( this.props.selection !== nextProps.selection ) {

			return true;
		}

		return false;
	},

	render : function() {

		var orgs = getPaginatedResults( this.state.quickExportResults, this.state.landingPageNo, this.state.displayPerPage );
		
		var orgs = orgs.map( function( organisation, i ) {

			var organisation 	= organisation;

			var orgID 			= parseInt( organisation[0] );

			var excludeMe 		= ( this.state.excludeSelection.indexOf( orgID ) === -1 ) ? false : true;

			var relationships 	= ( typeof this.state.relationships[orgID] !== "undefined" ) ? this.state.relationships[orgID] : [];

			return 	<Organisation 
						myKey={i} 
						excludeMe={excludeMe} 
						relationships={relationships}
						clientTypesByIndex={this.state.clientTypesByIndex}
						selectionType={this.state.selectionType}
						exportSelection={this.state.exportSelection} 
						settings={this.state.settings} 
						key={i} 
						data={organisation} />
		
		}.bind( this ) );

		var noneFound = "";

		if( orgs.length === 0 ) {

			noneFound = (
				<div className="alert alert-danger">
					No results found
				</div>
			);
		}

		return (

			<div>
				{noneFound}
				{orgs}
				

			</div>
		)
	}
});

export default  OrganisationsList;