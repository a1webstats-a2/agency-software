<?php

declare(strict_types=1);

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class Main extends Controller
{
    public function index()
    {
        return $this->getConfigData();
    }

    public function getConfigData(): array
    {
        try {
            $configContents = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/../a1config.json');
        } catch (\Exception) {
            return [
                'logo'    => '',
                'name'    => '',
                'website' => '',
                'email'   => '',
                'agentID' => 0
            ];
        }

        if ('' === $configContents) {
            return [
                'logo'    => '',
                'name'    => '',
                'website' => '',
                'email'   => '',
                'agentID' => 0
            ];
        }

        $config = json_decode($configContents, true, 512, JSON_THROW_ON_ERROR);

        if (!session('agentData')) {
            $client   = new Client();
            $response = $client->request('GET', env('API_SOURCE') . '/agents/data/' . $config['agentID']);
            $contents = $response->getBody()->getContents();
            $decoded  = json_decode($contents);

            session(['agentData' => $decoded]);
        }

        return [
            'logo'    => $config['logoFile'],
            'name'    => session('agentData')->company,
            'website' => session('agentData')->website,
            'email'   => session('agentData')->contact_email,
            'agentID' => session('agentData')->id,
            'css'     => session('agentData')->css
        ];
    }
}
