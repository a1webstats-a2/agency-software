import React from 'react';



var CountryOption = React.createClass({

	getInitialState 	: 	function() {

		return {

			id 		: 	this.props.myKey,
			name 	: 	this.props.countryName
		};
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.countryName !== nextProps.countryName ) {

			return true;
		}

		return false;
	},

	render 	: 	function() {

		return(

			<option value={this.state.id}>{this.state.name}</option>
		);
	}
});

export default  CountryOption;


