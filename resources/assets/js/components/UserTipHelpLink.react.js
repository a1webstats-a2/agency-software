import React from 'react';
import UserTipsStore from '../stores/UserTipsStore';
import UserTipActions from '../actions/UserTipActions';

function getCurrentState() {

	return {

		tip 	: 	UserTipsStore.getTip()
	}
}

class UserTipHelpLink extends React.Component {

	constructor( props ) {

		super( props );

		this._onChange = this._onChange.bind( this );

		this.state = getCurrentState();
	}

	componentDidMount() {

		UserTipsStore.addChangeListener( this._onChange );

	}

	componentWillUnmount() {

		UserTipsStore.removeChangeListener( this._onChange );
	}

	componentWillReceiveProps( newProps ) {

		this.setState( getCurrentState() );
	}

	render() {

		let display = ( <div><div className="clr"></div><br /><br /><br /></div> );

		if( this.state.tip.id !== -1 && !this.state.tip.displayOnPageLoad ) {

			display = (

				<div>
					<a href="#" onClick={( event )=>this._showHelp( event )}>
						<img src="/images/help.png" alt="Help symbol" style={{ marginRight : 15 }} />
						Need help? Show me some instructions.
					</a>
					<div className="clr"></div><br /><br />
				</div>
			)
		}

		return (

			<span>
				{display}

			</span>
		)
	}

	_showHelp( event ) {

		event.preventDefault();

		UserTipActions.showHelp();
	}

	_onChange() {

		this.setState( getCurrentState() );
	}
}

export default UserTipHelpLink;