import React from 'react';
import moment from 'moment';
import PageLink from './PageLink.react';

const PageVisitTr = React.createClass({
    getInitialState: function () {
        let showFullString = false;

        if (typeof this.props.userSettings !== "undefined" &&
            typeof this.props.userSettings.display_full_urls !== "undefined" &&
            parseInt(this.props.userSettings.display_full_urls) === 1) {
            showFullString = true;
        }

        return {
            lastPageVisited: this.props.lastPageVisited,
            heatmapData: this.props.heatmapData,
            pageID: this.props.pageID,
            clientID: this.props.clientID,
            browser: this.props.browser,
            showFullString: showFullString,
            eventID: this.props.eventID,
            eventDescription: this.props.eventDescription,
            queryString: this.props.queryString,
            url: this.props.url,
            pageVisitDate: this.props.pageVisitDate,
            duration: this.props.duration,
            referrer: this.props.referrer,
            totalNumImpressions: this.props.numPages,
            parentID: this.props.parentID,
            pageTitle: this.props.pageTitle,
            https: this.props.https,
            teamSettings: this.props.teamSettings,
            jobId: this.props.jobId,
        }
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.state.queryString !== nextState.queryString) {
            return true;
        }

        if (this.state.clientID !== nextState.clientID) {
            return true;
        }

        if (this.state.showFullString !== nextState.showFullString) {
            return true;
        }

        if (this.props.parentID !== nextProps.parentID) {
            return true;
        }

        if (this.props.url !== nextProps.url) {
            return true;
        }

        return this.props.duration !== nextProps.duration;


    },
    componentWillReceiveProps: function (newProps) {
        let showFullString = false;

        if (typeof newProps.userSettings !== "undefined" &&
            typeof newProps.userSettings.display_full_urls !== "undefined" &&
            parseInt(newProps.userSettings.display_full_urls) === 1) {

            showFullString = true;
        }

        this.setState({
            lastPageVisited: newProps.lastPageVisited,
            showFullString: showFullString,
            heatmapData: newProps.heatmapData,
            pageID: newProps.pageID,
            clientID: newProps.clientID,
            browser: newProps.browser,
            queryString: newProps.queryString,
            url: newProps.url,
            totalNumImpressions: newProps.numPages,
            eventID: newProps.eventID,
            eventDescription: newProps.eventDescription,
            pageVisitDate: newProps.pageVisitDate,
            duration: newProps.duration,
            referrer: newProps.referrer,
            parentID: newProps.parentID,
            pageTitle: newProps.pageTitle,
            https: newProps.https,
            teamSettings: newProps.teamSettings,
            jobId: newProps.jobId,
        });
    },

    render: function () {
        let duration     = parseInt(this.state.duration);
        const momentDate = moment(this.state.pageVisitDate);

        if (duration === 0 && this.state.totalNumImpressions === 1) {
            duration = 'only page impression';
        } else if (this.state.lastPageVisited) {
            duration = 'last page visited'
        } else if (duration > 60) {
            const newSessionDuration = duration / 60;
            const sessionMinutes     = Math.floor(newSessionDuration);
            const subtract           = sessionMinutes * 60;
            const sessionSeconds     = duration - subtract;

            duration = (
                <div>
                    <bold>{sessionMinutes}</bold> minutes <bold>{sessionSeconds}</bold> seconds
                </div>
            )
        } else {
            duration = (
                <div>
                    <bold>{duration}</bold> seconds
                </div>
            )
        }

        let pageDisplay;

        if (!this.state.url || this.state.url.trim() === '') {
            pageDisplay = this.state.pageTitle;
        } else {
            let URLProtocol = 'http';

            if (this.state.https && this.state.https === 1) {
                URLProtocol = 'https';
            }

            let linkURL     = URLProtocol + '://' + this.state.url.replace("http://", "");
            let queryString = (typeof this.state.queryString === "undefined") ? "" : this.state.queryString;
            let fullLinkURL = linkURL + queryString;
            let displayURL  = fullLinkURL;
            let showFullURL = "";

            if (!this.state.showFullString) {
                if (displayURL.length > 75) {
                    displayURL = displayURL.substring(0, 75);

                    showFullURL = (
                        <a href="#" onClick={this._showFullQueryString}> ...</a>
                    )
                }
            } else if (displayURL.length > 75) {
                showFullURL = (
                    <a href="#" onClick={this._showFullQueryString}> ...</a>
                )
            }

            let pageTitle = this.state.pageTitle;

            if (this.state.eventID) {
                pageTitle = (
                    <div>
                        <span className="eventLabel">
                            Event
                        </span>
                        {this.state.eventDescription}
                    </div>
                )
            }

            let finalURLDiv = (
                <div>
                    <PageLink url={fullLinkURL} display={displayURL}/>
                    {showFullURL}<br/>
                    <div className="clr"></div>
                    {pageTitle}
                </div>
            )

            pageDisplay = (
                <div>
                    {finalURLDiv}
                </div>
            )
        }

        let tds = [];
        tds.push(<td key={1} style={{ width: '65%', paddingRight: 30 }}>{pageDisplay}</td>);
        tds.push(<td key={2}>{duration}</td>);
        tds.push(<td key={3} style={{ width: '10%' }}>{momentDate.format("HH:mm")}</td>);

        if (this.state.teamSettings && this.state.teamSettings.clarity && momentDate < moment().subtract(2, 'hours')) {
            const url = `https://clarity.microsoft.com/projects/view/${this.state.teamSettings.clarity}/impressions?Variables=visitId%3A${this.state.jobId}&date=Last%203%20days`

            tds.push(
                <td key={4}>
                    <a target="_blank" href={url}>
                        <img src="/images/play-button-small.png" />
                    </a>
                </td>
            )
        }

        return (
            <tr>
                {tds}
            </tr>
        );
    },

    _showFullQueryString: function (event) {
        event.preventDefault();

        let showFullString = (!this.state.showFullString);

        this.setState({
            showFullString: showFullString
        })
    }
});

export default PageVisitTr;
