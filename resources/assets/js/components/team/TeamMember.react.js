import React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import FilterActions 	from '../../actions/FilterActions';
import Avatar from 'material-ui/Avatar';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';

function getCurrentState( props ) {

	return {

		user 		: props.user,
		isSuperUser : props.isSuperUser,
		teamMember 	: props.data
	}
}

const styles = {

	avatar 	: 	{

		marginTop		: 	10,
		marginBottom 	: 	10,
		marginRight 	: 	20
	}
}

var TeamMember = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		var avatarURL 		= "/api/proxy/api/v1/user/avatar/" + this.state.teamMember.id;

		let allowEditUser 	= ( this.state.isSuperUser ) ? true : false;

		if( parseInt( this.state.user.id ) === parseInt( this.state.teamMember.id ) ) {

			allowEditUser = true;
		} 

		let editIcon 	= "";
		let deleteIcon 	= "";

		if( allowEditUser ) {

			editIcon = (

				<EditIcon onClick={this._editMember} /> 

			)
		}

		if( this.state.isSuperUser ) {

			deleteIcon = (

				<DeleteIcon onClick={this._deleteMember} />
			)
		}

		return (

			<TableRow>
				<TableRowColumn>
					<Avatar src={avatarURL} style={styles.avatar} />
					{this.state.teamMember.forename} {this.state.teamMember.surname}
				</TableRowColumn>
				<TableRowColumn>
					{editIcon}
					{deleteIcon}
				</TableRowColumn>
			</TableRow>
		);
	},

	_editMember : function( event ) {

		event.preventDefault();

		FilterActions.setTeamMemberToEdit( this.state.teamMember );
	},

	_deleteMember : function( event ) {

		event.preventDefault();

		if( confirm( "Are you sure you want to delete this team member" ) ) {

			FilterActions.deleteTeamMember( this.state.teamMember.id );
		}
	}
});

export default  TeamMember;