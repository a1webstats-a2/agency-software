import React from 'react';
import {List, ListItem} from "material-ui/List";
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox';
import FilterActions from '../../actions/FilterActions';

class AutoExportToCMSNew extends React.Component {
    render() {
        let zapierCheckboxValue = 'Zapier';
        const zapierEnabled = 'undefined' !== typeof this.props.zapierIsSetup && this.props.zapierIsSetup
        let listItems = []
        if (!zapierEnabled) {
            zapierCheckboxValue = (
                <div style={{lineHeight: 2, marginTop: '-10px'}}>Please <a
                    href="https://zapier.com/apps/a1webstats/integrations">setup a
                    Zapier process using the "Export Organisation in A1WebStats" trigger</a> to use this feature</div>
            )
        }

        let checked = false;

        if (this.props.autoExports) {
            checked = undefined !== this.props.autoExports
                .find(autoExport => autoExport.report_template_id === this.props.templateId)
        }

        listItems.push(
            <ListItem
                key={0}
                rightAvatar={<Avatar src="/images/crms/zapier.png"/>}
                primaryText={zapierCheckboxValue}
                insetChildren={true}
                leftCheckbox={
                    <Checkbox
                        checked={checked}
                        disabled={!zapierEnabled}
                        onCheck={(event) => this._setAutoExportToCRM(event.target.checked, 'zapier', templateId)}
                        value="zapier"/>
                }
            />
        )
        const {templateId} = this.props
        return (
            <div className="w-1/2">
                <List>
                    {listItems}
                </List>
            </div>
        )
    }

    _setAutoExportToCRM(checked, crm, templateId) {
        if (checked) {
            return FilterActions.turnAutoExportToCRMOnForReportTemplate({
                templateId,
                crm
            });
        }
        return FilterActions.turnAutoExportToCRMOffForReportTemplate({
            templateId,
            crm
        });
    }
}

export default AutoExportToCMSNew
