import React from 'react';
import FilterActions from '../../actions/FilterActions';
import IconButton from 'material-ui/IconButton';
import {List, ListItem, MakeSelectable} from 'material-ui/List';
import InsertChart from 'material-ui/svg-icons/editor/insert-chart';
import Add from 'material-ui/svg-icons/content/add';
import ResultsIcon from 'material-ui/svg-icons/action/list';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import UpdateResultsIcon from 'material-ui/svg-icons/action/cached';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
let SelectableList = MakeSelectable( List );
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import BankIcon from 'material-ui/svg-icons/editor/attach-money';
import SaveIcon from 'material-ui/svg-icons/content/save';
import FileDownloadIcon from 'material-ui/svg-icons/file/file-download';
import Next from 'material-ui/svg-icons/image/navigate-next';
import Previous from 'material-ui/svg-icons/image/navigate-before';

import BusinessIcon from 'material-ui/svg-icons/communication/business';
import KeyIcon from 'material-ui/svg-icons/communication/vpn-key';
import ReferrerIcon from 'material-ui/svg-icons/navigation/chevron-left';
import PageIcon from 'material-ui/svg-icons/action/find-in-page';

import PaginationNext 	from '../pagination/next.react';
import PaginationPrev 	from '../pagination/prev.react';
import QuickLinks 		from '../QuickLinks.react';

function ordinal_suffix_of( i ) {
    
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function twoDigits( d ) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

function getCurrentState( props ) {

	return {

		value 			: 	0,
		open 			: 	props.open,
		selectedIndex 	: 	0,
		settings 		: 	props.settings,
		newFilters 		: 	props.newFilters,
		paginationData 	: 	props.paginationData,
		dateFrom 		: 	new Date( props.allFilters[0].storedValue ),
		dateTo 			: 	new Date( props.allFilters[1].storedValue ),
		ppcChecked 		: 	props.ppcChecked,
		organicChecked 	: 	props.organicChecked
	} 
}

const style = {

	drawer : 	{

		paddingLeft		: 20,
		paddingRight 	: 20,
		paddingTop 		: 20,
		paddingBottom 	: 100,
		marginTop 		: 100
	},

	selectableList : {

	},

	raisedButton : 	{

		marginBottom 	: 	10,
		width 			: 	180
	}
}

var MenubarWindowbox = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {


		if( this.props.settings !== nextProps.settings ) {

			return true;
		}

		if( this.props.ppcChecked !== nextProps.ppcChecked ) {

			return true;
		}

		if( this.props.organicChecked !== nextProps.organicChecked ) {

			return true;
		}

		if( this.props.open !== nextProps.open ) {

			return true;
		}

		if( this.props.newFilters !== nextProps.newFilters ) {

			return true;
		}

		if( this.props.paginationData.total !== nextProps.paginationData.total ) {

			return true;
		}

		if( this.props.allFilters[0] !== nextProps.allFilters[0] ) {

			return true;
		}

		if( this.props.allFilters[1] !== nextProps.allFilters[1] ) {

			return true;
		}

		return false;
	},

	formatDateMYSQL  :   function( d ) {

        return d.getFullYear() + '-' + ( "0" + ( d.getMonth() + 1 ) ).slice( -2 ) + '-' + ("0" + d.getDate()).slice(-2)
    },

	formatDate : function( dateObj ) {

		var displayDate = '';

		var rawDate = dateObj;

		var monthNames      =   [  "January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];

		switch( this.state.settings.user.preferred_date_format ) {

            case "dd/mm/yyyy" :

                displayDate = twoDigits( rawDate.getDate() ) + '/' + twoDigits( rawDate.getMonth() ) + '/' + rawDate.getFullYear();

                break;

            case "mm/dd/yyyy" :

                displayDate = twoDigits( rawDate.getMonth() ) + '/' + twoDigits( rawDate.getDate() ) + '/' + rawDate.getFullYear();

                break;

            case "yyyy-mm-dd" :

                displayDate = rawDate.getFullYear() + '-' + twoDigits( rawDate.getMonth() ) + '-' + twoDigits( rawDate.getDate() );
    
                break;

            case "fulltext" :

                displayDate = ordinal_suffix_of( rawDate.getDate() ) + ' ' + monthNames[rawDate.getMonth()] + ' ' + rawDate.getFullYear();

                break;

        }

        return displayDate;
	},

	handleRequestChange : function( event, index, value ) {
		
		FilterActions.openDrawer( index );
	},

	render : function() {

		if( !this.state.open ) {

			return ( <div></div> );
		}

	
		var disableUpdateResults 	= ( this.state.newFilters ) ? false : true;
		var backgroundColour 		= '#53b68b';

		var bankResultsDisabled 	= ( this.state.paginationData.total > 0 ) ? false : true;

		return (

			<div className="menubarWindow">

				<div className="row">

					<div className="col-md-2">

						<SelectableList style={style.selectableList} onChange={this.handleRequestChange}>
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={4} value={4} primaryText="Dashboard" rightIcon={<InsertChart color="#fff" />} />      						
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={1} value={1} primaryText="Filters" rightIcon={<Add color="#fff" />} />
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={3} value={3} primaryText="Export" rightIcon={<FileDownload color="#fff" />} />
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={5} value={5} disabled={false} primaryText="View Results" rightIcon={<ResultsIcon color="#fff" />} />
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={5} value={5} disabled={false} primaryText="Save as Template" rightIcon={<SaveIcon color="#fff" />} />
						</SelectableList>

					</div>

					<div className="col-md-2">

						<SelectableList style={style.selectableList} onChange={this.handleRequestChange}>
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={6} value={6} primaryText="Organisations" rightIcon={<BusinessIcon color="#fff" />} />      						
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={7} value={7} primaryText="Keywords" rightIcon={<KeyIcon color="#fff" />} />
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={8} value={8} primaryText="Referrers" rightIcon={<ReferrerIcon color="#fff" />} />
  							<ListItem style={{ color : '#fff', fontSize : 12 }} key={9} value={9} primaryText="Pages" rightIcon={<PageIcon color="#fff" />} />

						</SelectableList>

					</div>

					<div className="col-md-2 overflowHidden">
						<div className="menuContainer overflowHidden">
							From:
							<DatePicker   
							 	autoOk={true}
							 	onChange={this._setDateFrom}   
								className="menuDatepicker"
								textFieldStyle={{ color : '#fff', fontSize : '12px' }}
	    						style={{ color : '#fff' }}       
								inputStyle={{ color : '#fff' }} 
								hintText="Date From" container="inline" 
								value={this.state.dateFrom} 
								formatDate={this.formatDate} />

							To:
	    					<DatePicker 
	    					 	autoOk={true}
							 	onChange={this._setDateTo}   
								className="menuDatepicker"
								textFieldStyle={{ color : '#fff', fontSize : '12px' }}
	    						style={{ color : '#fff' }}
	    						inputStyle={{ color : '#fff' }} 
	    						hintText="Date To" 
	    						container="inline" 
	    						value={this.state.dateTo} 
	    						formatDate={this.formatDate} />

	    					<br />

	    					<p>Page <strong>1</strong> of <strong>{this.state.paginationData.numPages}</strong></p>
							<br />
							<p><strong>{this.state.paginationData.total}</strong> result(s) found</p>
							
						</div>
					</div>

					<div className="col-md-3 menuContainer noDisplayMobile quickLinks">
						<div>
							<QuickLinks ppcChecked={this.state.ppcChecked} organicChecked={this.state.organicChecked} />
	    				</div>
					</div>

					<div className="col-md-3  overflowHidden">

						<div className="menuContainer">

							<RaisedButton labelPosition="before" labelColor="#fff" backgroundColor={backgroundColour} disabled={disableUpdateResults} style={style.raisedButton}  onClick={this._updateResults} label="Update Results"  />
	    					<RaisedButton labelPosition="before" labelColor="#fff" backgroundColor="#FF6961" style={style.raisedButton}  onClick={this._resetFilters} label="Clear All Filters"  />

							<PaginationPrev paginationSettings={this.state.paginationData} isLoadingResults={this.state.isApplicationResting} />
							<PaginationNext paginationSettings={this.state.paginationData} isLoadingResults={this.state.isApplicationResting} />

							<RaisedButton
								label="Bank Results"
								labelPosition="before"
								disabled={bankResultsDisabled}
								onClick={this._bankResults}
								primary={true}
								icon={<BankIcon />}
								style={style.raisedButton}
							/>
						</div>
					</div>


					
				</div>
			</div>
		)
	},

	_setDateFrom : function( e, date ) {

		var newDate = new Date( date );

		FilterActions.editFilter({

            id              :   0,
            storedValue     :   this.formatDateMYSQL( newDate ),
            type            :   'dateFrom'
        });
	},

	_setDateTo : function( e, date ) {

		var newDate = new Date( date );

		FilterActions.editFilter({

            id              :   1,
            storedValue     :   this.formatDateMYSQL( newDate ),
            type            :   'dateTo'
        });
	},

	_bankResults : function() {

		if( confirm( "This will reset filters and combine existing results with new filters... proceed?" ) ) {

			FilterActions.bankResults();
		}
	},

	_getNextResults : function() {

		FilterActions.paginationNext();
	},

	_getPrevResults : function() {

		FilterActions.paginationPrev();
	},

	_resetFilters : function() {

		FilterActions.resetFilters();
	},

	_updateResults : function() {

		FilterActions.updateResults();
	},
});

export default  MenubarWindowbox;
