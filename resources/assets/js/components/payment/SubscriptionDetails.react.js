import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import PreviousInvoices from './PreviousInvoices.react';
import moment from 'moment';

function getCurrentState(props) {
  return {
    user: props.user,
    subscribedUntil: props.subscribedUntil,
    vendor: props.vendor
  }
}

const SubscriptionDetails = React.createClass({
  getInitialState: function () {
    return getCurrentState(this.props);
  },
  componentWillReceiveProps: function (newProps) {
    this.setState(getCurrentState(newProps));
  },
  render: function () {
    let displayDate = moment(this.state.subscribedUntil).format("Do MMMM YYYY");

    switch (this.state.user.preferred_date_format) {
      case "dd/mm/yyyy" :
        displayDate = moment(this.state.subscribedUntil).format("DD/MM/YYYY");

        break;
      case "mm/dd/yyyy" :
        displayDate = moment(this.state.subscribedUntil).format("MM/DD/YYYY");

        break;
      case "yyyy-mm-dd" :
        displayDate = moment(this.state.subscribedUntil).format("YYYY-MM-DD");

        break;
    }

    let updateCardDetails = '';

    if (this.state.vendor.toLowerCase() === "stripe") {
      updateCardDetails = (
          <div>
            <button onClick={this._updateStripeCardDetails} className="btn btn-primary">Update Card Details</button>
          </div>
      )
    }

    return (
        <div>
          <div className="col-md-12">
            <Tabs>
              <Tab label="Payment">
                <h3>Payment</h3><br/>
                <p>You are currently subscribed up till date: <strong>{displayDate}</strong></p>

                <div className="clr"></div>
                <br/>

                {updateCardDetails}

                <div className="clr"></div>
                <br/><br/><br/>
                <p>
                  <a href="#" className="warningText" onClick={this._cancelSubscription}>
                    Cancel subscription
                  </a>
                </p>
              </Tab>
              <Tab label="Previous Invoices">
                <PreviousInvoices/>
              </Tab>
            </Tabs>
          </div>
        </div>
    )
  },
  _updateStripeCardDetails: function() {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', '/api/proxy/api/v1/stripe/update-card-details');
    xhr.send();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          let stripe = Stripe('pk_live_dT41jwq0wBBQvk24YIMdHHUW');

          if (window.location.hostname === 'localhost') {
            stripe = Stripe('pk_test_ZNK2cPSbc954G7d6LIZLmkFO')
          }

          stripe.redirectToCheckout({
            sessionId: JSON.parse(xhr.response).id
          }).then(function (result) {
            alert('Sorry, we can\'t update your subscription at this time, please try again later');
          });
        }
      }
    }
  },
  _cancelSubscription: function (event) {

    event.preventDefault();

    if (confirm("Are you sure you want to cancel your subscription?")) {

      window.location = "/cancel-subscription";
    }
  }
});

export default SubscriptionDetails;
