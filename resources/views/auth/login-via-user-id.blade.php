@extends( 'templates.signup' )
@section( 'content' )
    
    <div class="row">
        <div  class="col-md-6 col-md-offset-3">

            <div id="loginForm">

                <div id="a1strap">
                    <h1>{{ $agentData['name'] }}</h1>
                </div>

                <br /><br />

                <div>

                    <form method="POST" action="/auth/login">

                        <input type="hidden" name="userID" value="{{ $userID }}" />
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        </div>

                        <br />
                        
                        <button type="submit" alt="login" class="btn btn-primary">Login</button>
                    </form>

                    <br />

                </div>

            </div>
        </div>
    </div>
@endsection