import React from 'react';
import Dialog from 'material-ui/Dialog';
import FilterActions from '../../actions/FilterActions';
import FlatButton from 'material-ui/FlatButton';
import FiltersStore from '../../stores/FiltersStore';

function getCurrentState( props ) {

	return {

		otherData 	: 	props.otherData
	}
}



var CountryVisitorStats = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( FiltersStore.clone( nextProps.otherData.countryVisitorStats ) !== 
			FiltersStore.clone( this.props.otherData.countryVisitorStats ) ) {

			return true;
		} 
 
		if( !nextProps.otherData.countryVisitorStats.stats ) {

			return false;
		}

		if( this.props.otherData !== nextProps.otherData ) {

			return true;
		}

		return false;
	},

	handleClose : function() {

		FilterActions.closeLightbox();
	},

	render : function() {

		if( !this.state.otherData.countryVisitorStats.stats ) {

			return ( <div></div> );
		}

		var title = "Visits for " + this.state.otherData.countryVisitorStats.stats[1];

		const actions = [

			<FlatButton
				label="Close"
				primary={true}
				onTouchTap={this.handleClose}
			/>
		];

		return (

			<div>
				<Dialog title={title} actions={actions} open={this.state.otherData.countryVisitorStats.open}>
					{this.state.otherData.countryVisitorStats.stats[2]} visits
				</Dialog>

			</div>

		)
	}
});

export default  CountryVisitorStats;