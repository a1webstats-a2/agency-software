@extends( 'templates.signup' )

@section( 'content' )
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div id="signupForm2" class="signupForm">
                <div id="signupLogo">
                    <img src="/images/A1Webstats-Logo.png"/>
                </div>

                <div class="clr"></div>
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Agent Software Installation</h1>

                        <p>To complete installation, simply enter in your registration code here:</p>

                        <form method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Code</label>
                                <input type="text" class="form-control" name="code"/>
                            </div>

                            <div class="form-group">
                                <label>Logo Image</label>
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                            <div class="clr"></div>
                            <br/><br/>

                            <div class="form-group">
                                <button class="btn-primary btn">Complete Installation</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection( 'content' )
