import React 			from 'react';
import FiltersStore 	from '../stores/FiltersStore';
import TextField 		from 'material-ui/TextField';
import RaisedButton 	from 'material-ui/RaisedButton';

function getCurrentState() {

	return {

		websocketConn 	: 	FiltersStore.getWebsocketConn(),
		msg 			: 	'',
		messages 		: 	FiltersStore.getWebsocketMessages()
	}
}

class WebSocketTest extends React.Component {
	constructor( props ) {
		super( props );

		this.state 		= getCurrentState();

		this._onChange 	= this._onChange.bind( this );
	}
	componentDidMount() {
		FiltersStore.addChangeListener( this._onChange );
	}

	componentWillUnmount() {
		FiltersStore.removeChangeListener( this._onChange );
	}

	componentWillReceiveProps( newProps ) {
		this.setState( getCurrentState() );
	}

	render() {
		const messages = this.state.messages.map( function( msg, i ) {

			return (

				<p key={i}>{msg}</p>
			)
		})

		return (

			<div>
				<div style={{ border : '1px solid #000', padding : 20, marginBottom : 30 }}>

					<TextField value={this.state.msg} id="newMessage" onChange={ ( event, newText ) => this._setMessageText( newText ) } />

				    <RaisedButton label="Send Message" style={{ marginLeft : 30 }} primary={true} onTouchTap={ () => this._sendMessage()} />
				</div>

				<div style={{ padding : 20 }}>
					<h2>Messages</h2>

					<div className="clr"></div><br />

					{messages}
				</div>

			</div>
		)
	}

	_setMessageText( text ) {

		this.setState({

			msg 	: 	text
		})
	}

	_sendMessage() {

		this.setState({

			msg 	: 	''
		})

		this.state.websocketConn.send( this.state.msg );
	}

	_onChange() {

		this.setState( getCurrentState() );
	}
}

export default WebSocketTest;
