import React from 'react';
import FilterActions from '../actions/FilterActions';
import Clear from 'material-ui/svg-icons/content/clear';

const style = {

	marginTop: 	15
}

var HideDrawers = React.createClass({

	getInitialState : function() {

		return {};
	},

	render : function() {

		return (

			<Clear style={style} onClick={this._hideDrawers} />
		);
	},

	_hideDrawers : function() {

		FilterActions.hideDrawers();
	}

});

export default  HideDrawers;