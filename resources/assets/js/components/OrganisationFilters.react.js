import React 					from 'react';
import FiltersStore  			from '../stores/FiltersStore';
import FilterActions 			from '../actions/FilterActions';
import OrganisationCheckbox 	from './OrganisationCheckbox.react';
import NavBar 					from './navigation/NavBar.react';
import Footer  					from './Footer.react';
import NotificationsStore 		from '../stores/NotificationsStore';
import SearchBy 				from './customFilters/SearchBy.react';

import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';

const styles = {

  	block: {
    	maxWidth: 250,
  	},
  	toggle: {
    	marginBottom: 16,
  	},

  	raisedButton : {

  		width 			: 	180,
  		marginRight 	: 	12,
  		marginTop 		: 	0,
  		marginBottom 	: 	20
  	},

  	radioButton : {

  		marginTop 		: 10,
  		marginBottom	: 10
  	}
};

function sortInclude( b, a ) {

	if ( a.include_count < b.include_count )

		return -1;

	if ( a.include_count > b.include_count )

		return 1;

	return 0;
}

function sortExclude( b, a ) {

	if ( a.exclude_count < b.exclude_count )

		return -1;

	if ( a.exclude_count > b.exclude_count )

		return 1;

	return 0;
}

function setInitialState( props ) {

	var organisationsData = {

		allOrgFilters 		: 	FiltersStore.getFiltersByTypeWithParam( 'organisationFilter' ),
		exclude 			: 	FiltersStore.getInclusionType(),
		searchCriteria 		: 	FiltersStore.getCurrentSearchCriteria(),
		filtersBeingEdited 	: 	FiltersStore.areFiltersCurrentlyBeingEdited(),
		type 				: 	'organisationFilter',
		allOrganisations	: 	FiltersStore.getAllOrganisations(),
		filterChange 		: 	FiltersStore.getFilterChange(),
		exclude 			: 	FiltersStore.getInclusionType(),
		selectAll 			: 	FiltersStore.checkAllSelected({

			type 	: 	'organisationFilter'
		}) 
	};

	if( organisationsData.exclude === "include" ) {

		organisationsData.allOrganisations.sort( sortInclude );
	
	} else {

		organisationsData.allOrganisations.sort( sortExclude );
	}

	return {
		
		looseFilterSetDisplay 	: 	FiltersStore.getLooseFilterSetDisplay(),

		scenariosFilterObj 		: 	FiltersStore.getScenarioFiltersObj(),
		latestAddedFilters 		: 	FiltersStore.getLatestAddedFilters(),
		displayFancyFilters 	: 	FiltersStore.showFancyFilters(),
		filters 				: 	FiltersStore.getFilters(),
 		settings 				: 	FiltersStore.getAllSettings(),

		notificationsData 		: 	{

			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 )
		},
			
		searchByData : {

			searchCriteria 				: FiltersStore.getCurrentSearchCriteria(),
			searchString 				: FiltersStore.getCurrentSearchCriteria().searchString,
			inclusionType 				: FiltersStore.shouldCustomFilterIncludeOrExclude(),
			allFilters 					: FiltersStore.getFilters(),
			searchByLoading 			: FiltersStore.getSearchByLoadingStatus(),
			selectAll 					: FiltersStore.getSelectAllSearchByType(),
			filtersBeingEdited 			: FiltersStore.areFiltersCurrentlyBeingEdited(),
			filterChange 				: FiltersStore.getFilterChange(),
			selectedCustomFilterType 	: FiltersStore.getCustomFilterType(),
			countryIncludeType 			: FiltersStore.getCountryInclusionType(),
			organisationIncludeType 	: FiltersStore.getInclusionType(),
			narrowedOptions 			: FiltersStore.getNarrowedOptions(),
			allowedSearchTypes 			: [

				'customfilterreferrer',
				'customfilterreferrercountry',
				'organisationfilter',
				'customfilterurl',
				'customfilterkeyword'
			]
		},


		filtersBeingEdited 		: 	FiltersStore.areFiltersCurrentlyBeingEdited(),
		otherOrganisationData	: 	organisationsData,
		searchCriteria 			: 	FiltersStore.getCurrentSearchCriteria(),
		allOrgFilters 			: 	FiltersStore.getFiltersByTypeWithParam( 'organisationFilter' ),
		type 					: 	'organisationFilter',
		allOrganisations		: 	FiltersStore.getAllOrganisations(),
		exclude 				: 	FiltersStore.getInclusionType(),
		selectAll 				: 	FiltersStore.checkAllSelected({

			type 	: 	'organisationFilter'
		}) 
	}
}

var OrganisationFilters 	= 	React.createClass({

	getInitialState 	: 	function() {

		return setInitialState( this.props );
	},

	componentDidMount : function() {

		FiltersStore.addChangeListener( this._onChange );
	},

	componentWillUnmount: function() {

		FiltersStore.removeChangeListener( this._onChange );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( setInitialState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {
		/*
		if( this.props.organisationsData !== nextProps.organisationsData ) {

			return true;
		}

		if( this.props.searchByData.organisationIncludeType !== nextProps.searchByData.organisationIncludeType ) {

			return true;
		}

		return false;*/

		return true;
	},


	render 	: 	function() {
			
		var organisationsLeft 	=	[];
		var organisationsRight 	=	[];
		var halfOrgsLength 		= 	this.state.allOrganisations.length / 2;

		this.state.allOrganisations.map( function( organisation, i ) {

			if( i > halfOrgsLength ) {

				organisationsRight.push( <OrganisationCheckbox allOrgFilters={this.state.allOrgFilters} searchByData={this.state.searchByData} otherOrganisationData={this.state.otherOrganisationData} searchByCheckbox={false} myKey={i}  key={i} organisationData={organisation} />);

			} else {

				organisationsLeft.push( <OrganisationCheckbox allOrgFilters={this.state.allOrgFilters} searchByData={this.state.searchByData} otherOrganisationData={this.state.otherOrganisationData} searchByCheckbox={false} myKey={i}  key={i} organisationData={organisation} />);

			}

		}.bind( this ))

		var radioIncludeValue = "include";

		return (

			<div className="organisationFilters">

				<NavBar data={this.state.notificationsData} />
			
				<div className="clr"></div>

				<div className="container mainContainer">
					
					<div className="row">

						<div className="col-md-3">

							<div className="displayOptions">

								<h3>Options</h3><br />

								<div className="clr"></div><br />

								<RadioButtonGroup onChange={this._saveIncludeExcludeOption} name="includeExclude" defaultSelected={radioIncludeValue}>
									<RadioButton
										value="include"
										label="Include"
										style={styles.radioButton}
									/>
									<RadioButton
										value="exclude"
										label="Exclude"
										style={styles.radioButton}
									/>
										
								</RadioButtonGroup>

								<div className="clr"></div><br />

								<RaisedButton labelPosition="before" labelColor="#fff" backgroundColor='#53b68b' style={styles.raisedButton} onClick={this._startExport} label="Export"  />
								<div className="clr"></div>


								<RaisedButton primary={true} label="Show Data" style={styles.raisedButton} onClick={this._showData} />





							</div>

						</div>

						<div className="col-md-9" id="results">

							<h3>Advanced Filters - Organisations</h3><br />

							<div className="clr"></div>

							<SearchBy searchByData={this.state.searchByData} type="organisationFilter" />

							<div className="clr"></div>

							<hr />

							<div className="clr"></div><br />

							<h4>All Organisations within Date Range</h4>

							<div className="clr"></div><br />

							<RaisedButton label="Select All" onClick={this._selectAll} style={styles.raisedButton} primary={true} />

							<RaisedButton label="Deselect All" onClick={this._deselectAll} style={styles.raisedButton} secondary={true} />

							<div className="clr"></div>

							<div className="row">

								<div className="col-md-6">
									
									{organisationsLeft}

								</div>

								<div className="col-md-6">
									
									{organisationsRight}

								</div>
							</div>
						</div>
					</div>
				</div>

				<Footer 
					looseFilterSetDisplay={this.state.looseFilterSetDisplay}
					scenariosFilters={this.state.scenariosFilterObj}
					settings={this.state.settings}
					filters={this.state.filters}
					display={this.state.displayFancyFilters}
					latestAddedFilters={this.state.latestAddedFilters}
				/>
			</div>
		);
	},

	_showData : function() {

		FilterActions.updateResults();
	},

	_selectAll : function() {

		FilterActions.setAllSelected({

			type 			: 	this.state.type,
			allSelected 	: 	true
		});

		var organisationFilters = this.state.allOrganisations.map( function( organisation, i ) {

				return  {

		   			type          	:   this.state.type, 
		   			storedValue 	:   {

						id 		 	: 	organisation.id,
						include 	: 	this.state.exclude,
						name 	 	: 	organisation.use_organisation_name,
						searchBox 	: 	false,
						value 		: 	organisation.id
					},
					id 				: 	organisation.id

   				}

   		}.bind( this ));

   		FilterActions.createFilters( organisationFilters );
	},

	_deselectAll : function() {

		FilterActions.setAllSelected({

			type 			: 	this.state.type,
			allSelected 	: 	false
		});

		var removeFilters = this.state.allOrganisations.map( function( organisation, i ) {

			return {

   				type 	: 	this.state.type,
   				id 		: 	organisation.id
   			};
   		}.bind( this ));

   		FilterActions.removeFiltersByType( removeFilters );
	},

	_startExport : function() {

		FilterActions.openExport();
	},

	_saveIncludeExcludeOption : 	function( event, value ) {

		FilterActions.setOrganisationInclusionType( value );
	},

	_onChange: function() {

	    this.setState( setInitialState() );
	}

	
});

export default  OrganisationFilters;
