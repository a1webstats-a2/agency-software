import React    from 'react';
import Checkbox from 'material-ui/Checkbox';

import FilterActions   from '../actions/FilterActions';


var SelectDisplayedItems = React.createClass({

	getInitialState 	: 	function() {

		return {

			displayedColumns 		: 	this.props.showDisplayed
		}
	},


	render: function() {

		var displayedColumns = this.state.displayedColumns;

		return (

			<div className="displayedItems">

				<h3>Displayed Items</h3>
			  	<div className="checkbox">
			    	<Checkbox
						name="url"
					  	value="checkboxValue2"
					  	label="URL"
					  	onCheck={this._updateDisplayedStatus}
					  	defaultChecked={Boolean( displayedColumns.url )} />
				</div>

				<div className="checkbox">
			    	<Checkbox
						name="hostname"
					  	value="checkboxValue2"
					  	label="Hostname"
					  	onCheck={this._updateDisplayedStatus}
					  	defaultChecked={Boolean(displayedColumns.hostname)} />
				</div>

				<div className="checkbox">
			    	<Checkbox
						name="pageVisitDate"
					  	value="checkboxValue2"
					  	label="Page Visit Date"
					  	onCheck={this._updateDisplayedStatus}
					  	defaultChecked={Boolean(displayedColumns.pageVisitDate)} />
				</div>

				<div className="checkbox">
			    	<Checkbox
						name="duration"
					  	value="checkboxValue2"
					  	label="Duration"
					  	onCheck={this._updateDisplayedStatus}
					  	defaultChecked={Boolean(displayedColumns.duration)} />
				</div>

				<div className="checkbox">
			    	<Checkbox
						name="queryString"
					  	value="checkboxValue2"
					  	label="Query String"
					  	onCheck={this._updateDisplayedStatus}
					  	defaultChecked={Boolean(displayedColumns.queryString)} />
				</div>

				<div className="checkbox">
			    	<Checkbox
						name="ipAddress"
					  	value="checkboxValue2"
					  	label="IP Address"
					  	onCheck={this._updateDisplayedStatus}						  	
					  	defaultChecked={Boolean(displayedColumns.ipAddress)} />
				</div>

				<div className="checkbox">
			    	<Checkbox
						name="location"
					  	value="checkboxValue2"
					  	label="Location"
					  	onCheck={this._updateDisplayedStatus}						  	
					  	defaultChecked={Boolean(displayedColumns.location)} />
				</div>

				<div className="checkbox">
			    	<Checkbox
						name="organisation_name"
					  	value="checkboxValue2"
					  	label="Organisation Name"
					  	onCheck={this._updateDisplayedStatus}						  	
					  	defaultChecked={Boolean(displayedColumns.organisation_name)} />
				</div>

			</div>
		);


	},

	_updateDisplayedStatus 	: 	function( status ) {

		FilterActions.updateDisplayedStatus({

            checkboxName     :   status.target.name,
            displayed        :   status.target.checked
        });

	}
});

export default  SelectDisplayedItems;