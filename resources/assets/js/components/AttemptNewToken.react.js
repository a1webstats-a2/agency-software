import React 			from 'react';
import Dialog 			from 'material-ui/Dialog';
import FlatButton 		from 'material-ui/FlatButton';
import RaisedButton 	from 'material-ui/RaisedButton';
import FilterActions 	from '../actions/FilterActions';
import TextField 		from 'material-ui/TextField';

function getCurrentState( props ) {

	return {

		msg 		: 	props.msg,
		open 		: 	props.open,
		email 		: 	"",
		password 	: 	""
	}
}

class AttemptNewToken extends React.Component {

	constructor( props ) {

		super( props );
   		this._setValueForField 	= this._setValueForField.bind( this );
		this.state 				= getCurrentState( props );
	}

	componentWillReceiveProps( props ) {

		this.setState( getCurrentState( props ) );
	}

	render() {

		let actions 	= [];

		let displayMsg 	= "";

		if( this.state.msg.type === 1 ) {

			displayMsg = (

				<div className="alert alert-success">

					{this.state.msg.msg}
				</div>
			)
		
		} else if( this.state.msg.type === 0 ) {

			displayMsg = (

				<div className="alert alert-danger">

					{this.state.msg.msg}
				</div>
			)
		
		}

		let loginForm = "";

		if( this.state.msg.type === 0 || this.state.msg.type === -1 ) {

			actions.push(

				<FlatButton
					label="Login"
					primary={true}
					onClick={ () => this._login() }
				/>
			);

			loginForm = (

				<div>
					<p>Your session has expired, please login:</p>

		        	<TextField
		        		value={this.state.email}
						floatingLabelText="Email"
						fullWidth={true}
						onChange={ ( event, newValue ) => this._setValueForField( 'email', newValue ) }
					/><br />
					<TextField
						value={this.state.password}
						floatingLabelText="Password"
						type="password"
						fullWidth={true}
						onChange={ ( event, newValue ) => this._setValueForField( 'password', newValue ) }

					/><br />
				</div>
			)
		
		} else {

			actions.push( 

				<FlatButton
					label="Continue"
					primary={true}
					onClick={ () => this._handleClose() }
				/>

			);
		}

		return (

			<Dialog
	        	title="Renew Login"
	          	open={this.state.open}
	          	actions={actions}
	          	onRequestClose={this._handleClose}
	        >	
	        	{displayMsg}

	        	<div className="clr"></div><br />

	        	{loginForm}

        	</Dialog>

		)
	}

	_setValueForField( field, val ) {

		let state 		= this.state;
		state[field] 	= val;
		this.setState( state );
	}

	_login() {

		let loginCredentials = this.state;

		this.setState({

			email 	 	: 	"",
			password 	: 	""
		});

		FilterActions.attemptLogin( loginCredentials );
	}

	_handleClose() {

		FilterActions.closeLightbox();
	}
}

export default AttemptNewToken;