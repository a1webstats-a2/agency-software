import React from 'react';
import EditRecordIcon from 'material-ui/svg-icons/content/create';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import TagIcon from 'material-ui/svg-icons/action/label-outline';
import InfoIcon from 'material-ui/svg-icons/action/info-outline';
import IconButton from 'material-ui/IconButton';
import Comment from 'material-ui/svg-icons/communication/comment';
import FilterActions from '../../actions/FilterActions';

function getCurrentState(props) {
    return {
        tags: props.tags,
        col: props.col,
        row: props.rowIndex,
        data: props.data,
        relationships: props.relationships
    }
}

const iconStyle = {
    height: 15, width: 15, color: '#170550'
}

const ActionsCell = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        let tagsDisabled = true;

        if (typeof this.state.tags !== "undefined" && this.state.tags.length > 0) {
            tagsDisabled = false;
        }

        return (
            <div>
                <IconButton
                    onTouchTap={this._openEditCompanyRecord}
                    style={iconStyle}
                    tooltip="Edit"
                    iconStyle={iconStyle}>
                    <EditRecordIcon/>
                </IconButton>

                <IconButton
                    style={iconStyle}
                    tooltip="Email"
                    onTouchTap={this._openEmail}
                    iconStyle={iconStyle}>
                    <EmailIcon/>
                </IconButton>

                <IconButton
                    disabled={tagsDisabled}
                    style={iconStyle}
                    tooltip="Tags"
                    onTouchTap={this._editTags}
                    iconStyle={iconStyle}>
                    <TagIcon/>
                </IconButton>

                <IconButton
                    style={iconStyle}
                    onTouchTap={this._showNotes}
                    tooltip="Notes"
                    iconStyle={iconStyle}>
                    <Comment/>
                </IconButton>

                <IconButton
                    style={iconStyle}
                    onTouchTap={this._showIntel}
                    tooltip="Intel"
                    iconStyle={iconStyle}>
                    <InfoIcon/>
                </IconButton>
            </div>
        );
    },
    _showIntel: function () {
        const orgData = this.state.data.getObjectAt(this.state.row);
        FilterActions.showAdditionalCompanyData(parseInt(orgData.organisation_id));
    },
    _editTags: function () {
        const orgData = this.state.data.getObjectAt(this.state.row);

        let relationships = [];

        if (
            typeof this.state.relationships !== 'undefined' &&
            typeof this.state.relationships[orgData.organisation_id] !== 'undefined') {

            relationships = JSON.stringify(this.state.relationships[orgData.organisation_id]);
        }

        FilterActions.openClientTypesLightbox({
            organisation_id: parseInt(orgData.organisation_id),
            relationships_json: relationships
        });
    },
    _showNotes: function () {
        const orgData = this.state.data.getObjectAt(this.state.row);

        FilterActions.openNotes(parseInt(orgData.organisation_id));
    },
    _openEmail: function () {
        const orgData = this.state.data.getObjectAt(this.state.row);

        FilterActions.sendOrganisationViaEmail(parseInt(orgData.organisation_id));
    },
    _openEditCompanyRecord: function () {
        const orgData = this.state.data.getObjectAt(this.state.row);

        FilterActions.setActiveRecord({
            data: {
                use_organisation_linked_in_address: '',
                use_organisation_tel: orgData.tel,
                use_organisation_website: orgData.website,
                organisationid: orgData.organisation_id,
                use_organisation_name: orgData.name
            },
            myKey: orgData.organisation_id,
            newFormat: true
        });
    }
});

export default ActionsCell;
