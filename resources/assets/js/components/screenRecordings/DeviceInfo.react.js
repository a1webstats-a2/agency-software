import React from 'react';

function getCurrentState( props ) {

	return {

		os 		: 	props.os,
		device 	: 	props.device,
		browser :  	props.browser
	}
}



import ChromePNG     from '../png/ChromePNG.react';
import FirefoxPNG    from '../png/FirefoxPNG.react';
import OperaPNG      from '../png/OperaPNG.react';
import SafariPNG     from '../png/SafariPNG.react';

import DesktopPNG    from '../png/DesktopPNG.react';
import TabletPNG     from '../png/TabletPNG.react';
import MobilePNG     from '../png/MobilePNG.react';
import MacPNG        from '../png/MacPNG.react';

import WindowsPNG    from '../png/WindowsPNG.react';
import LinuxPNG      from '../png/LinuxPNG.react';
import AndroidPNG    from '../png/AndroidPNG.react';
import IOSPNG        from '../png/IOSPNG.react';
import MacosPNG      from '../png/MacOSPNG.react';

function findReadableOperatingSystem( fullOperatingSystemJargon ) {

    var returnName = "Unknown";

    if( !fullOperatingSystemJargon ) {

        return returnName;
    }

    var knownVals = {

        'Win'                   :  'windows',
        'Win16'                 :  'windows',
        'Windows 95'            :  'windows',
        'Windows 98'            :  'windows',
        'Windows NT 5.0'        :  'windows',
        'Windows NT 5.1'        :  'windows',
        'Windows NT 5.2'        :  'windows',
        'Windows NT 6.0'        :  'windows',
        'Windows NT 6.1'        :  'windows',
        'Windows NT 6.2'        :  'windows',
        'Windows NT 4.0'        :  'windows',
        'Windows ME'            :  'windows',
        'OpenBSD'               :  'open bsd',
        'SunOS'                 :  'sun os',
        'Linux'                 :  'linux',
        'Mac_PowerPC'           :  'windows',
        'QNX'                   :  'qnx',
        'BeOS'                  :  'be os',
        'OS/2'                  :  'os 2', 
        '5.0 (Windows NT 10.0'  :  'windows',
        'iPhone OS'             :  'ios',
        'Mac OS'                :  'mac os',
        'Android'               :  'android',
        'Macintosh'				:  'mac os'
    }

    var knownIndexes = [

        'Android',
        'Win',
        'Win16',
        'Windows 95',
        'Windows 98',
        'Windows NT 5.0',
        'Windows NT 5.1',
        'Windows NT 5.2',
        'Windows NT 6.0',
        'Windows NT 6.1',
        'Windows NT 6.2',
        'Windows NT 4.0',
        'Windows ME',
        'OpenBSD',
        'SunOS',
        'Linux',
        'Mac_PowerPC',
        'QNX',
        'BeOS',
        'OS/2',
        '5.0 (Windows NT 10.0',
        'iPhone OS',
        'Mac OS',
        'Macintosh'
    ]

    knownIndexes.forEach( function( name, key ) {

        if( fullOperatingSystemJargon.includes( name ) ) {

            if( typeof knownVals[name] !== "undefined" ) {
            
                returnName = knownVals[name];
            }
        }
    });


    return returnName;
}

class DeviceInfo extends React.Component {

	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( newProps ) {

		this.setState( getCurrentState( newProps ) );
	}

	render() {

		let browserIcon = "";
		let osIcon 		= "";
		let deviceIcon 	= "";
		const imgStyle 	= {


		}

        if( this.state.browser ) {

            switch( this.state.browser.toLowerCase() ) {

                case "firefox" :

					browserIcon = (

						<img src="/images/med/mozilla.png" />
					)

					break;

				case "chrome" :

					browserIcon = (

						<img src="/images/med/chrome.png" />
					)

					break;

				case "internet explorer" :

					browserIcon = (

						<img src="/images/med/internet-explorer.png" />
					)

					break;

				case "safari" :

					browserIcon = (

						<img src="/images/med/safari.png" />
					)

					break;
            }
        }

        if( this.state.device ) {

            switch( this.state.device.toLowerCase() ) {

                case "mac" :

					deviceIcon = (

						<img src="/images/med/imac.png" />
					)


					break;

				case "desktop" :

					deviceIcon = (

						<img src="/images/med/desktop.png" />
					)

					break;

				case "mobile" :

					deviceIcon = (

						<img src="/images/med/smartphonesmall.png" />
					)

					break;

				case "tablet" :

					deviceIcon = (

						<img src="/images/med/tablet.png" />
					)

					break;
            }
        }


		const operatingSystem = findReadableOperatingSystem( this.state.os );

        if( this.state.os ) {

            switch( operatingSystem.toLowerCase() ) {

                case "win" :

					osIcon = (

						<img src="/images/med/windows.png" />
					)

					break;

				case "wow" :

					osIcon = (

						<img src="/images/med/windows.png" />
					)

					break;

				case "android" :

					osIcon = (

						<img src="/images/med/android.png" />
					)

					break;

				case "linux" :

					osIcon = (

						<img src="/images/med/android.png" />
					)

					break;

				case "macintosh" :

					osIcon = (

						<img src="/images/med/macos.png" />

					)

					break;

				case "mac os" :

					osIcon = (

						<img src="/images/med/macos.png" />

					)

					break;
            }

        } 

        const tdStyle = { paddingRight : 20 };

		return (

			<table>	
				<tbody>
					<tr>
						<td style={tdStyle}>{deviceIcon}</td>
						<td style={tdStyle}>{osIcon}</td>
						<td style={tdStyle}>{browserIcon}</td>
					</tr>
				</tbody>
			</table>
				
		)
	}
}

export default DeviceInfo;