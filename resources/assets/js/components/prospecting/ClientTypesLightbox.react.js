import React from 'react';
import ClientTypes from './ClientTypes.react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FilterActions from '../../actions/FilterActions';

function getCurrentState(props) {
    return {
        storeType: props.storeType,
        open: props.open,
        tags: props.tags,
        relationships: props.relationships,
        organisationID: props.organisationID
    }
}

const ClientTypesLightbox = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.open !== nextProps.open) {
            return true;
        }

        if (this.props.tags !== nextProps.tags) {
            return true;
        }

        if (this.props.relationships !== nextProps.relationships) {
            return true;
        }

        if (this.props.storeType !== nextProps.storeType) {
            return true;
        }

        if (this.props.organisationID !== nextProps.organisationID) {
            return true;
        }

        return false;
    },
    render: function () {
        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onTouchTap={this._handleClose}
            />
        ];

        return (
            <div>
                <Dialog
                    title="Tags"
                    actions={actions}
                    modal={false}
                    autoScrollBodyContent={true}
                    open={this.state.open}
                    onRequestClose={this._handleClose}
                >
                    <ClientTypes
                        storeType={this.state.storeType}
                        tags={this.state.tags}
                        key={0}
                        myKey={0}
                        organisationId={this.state.organisationID}
                        organisationid={this.state.organisationID}
                        relationships={this.state.relationships}
                    />
                </Dialog>
            </div>
        )
    },
    _handleClose: function () {
        FilterActions.closeLightbox();
    }
});

export default ClientTypesLightbox;
