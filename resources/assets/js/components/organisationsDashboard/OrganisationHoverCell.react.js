import React 			from 'react';
import moment 			from 'moment';
import FilterActions 	from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {	

		col 		: 	props.col,
		row 		: 	props.rowIndex,
		data 		: 	props.data
	}
}

var OrganisationHoverCell = React.createClass({

	getInitialState : function() {

		let currentState 				= getCurrentState( this.props );
		return currentState;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		let displayText = this.state.data.getObjectAt( this.state.row )[this.state.col];

		return (
            
            <div style={{ marginTop : 14 }}>
	            
            	<a href="#" onClick={this._showVisitData}>
            		{displayText}
            	</a>

			</div>
		);
	},

	_showVisitData : function( event ) {

		event.preventDefault();

		FilterActions.showOrganisationInLightboxAndSetIndex({

			organisation 	: 	this.state.data.getObjectAt( this.state.row ),
			row 			: 	this.state.row
		});
	}
});

export default OrganisationHoverCell;