jest.mock( '../dispatcher/ReportBuilderDispatcher' );
jest.dontMock( './FiltersStore' );
jest.dontMock( 'merge' );
jest.dontMock( 'object-assign' );

describe( 'FiltersStore', function() {

	var FilterConstants = require( '../constants/FilterConstants' );

	var createRawFilter = {

		source 	: 	'VIEW_ACTION',
		action 	: 	{

			actionType 	: 	FilterConstants.CREATE_RAW_FILTER,
			filter 		: 	{

			}
		}
	}

	var AppDispatcher;
	const FiltersStore;
	var callback;

	beforeEach( function() {

		AppDispatcher 	= require( '../dispatcher/ReportBuilderDispatcher' );
		FiltersStore 	= require( './FiltersStore' ).default;
		callback 		= AppDispatcher.register.mock.calls[0][0];
	})

	it( 'registers a callback with the dispatcher', function() {

		expect( AppDispatcher.register.mock.calls.length ).toBe( 1 );
	});

	it( 'test create raw filter works', function() {

		FiltersStore 	= require( './FiltersStore' ).default;

		callback( createRawFilter );
		console.log( FiltersStore.getFilters() );
	});
});