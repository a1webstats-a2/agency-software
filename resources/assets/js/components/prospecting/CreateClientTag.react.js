import React from 'react';
import FilterActions from '../../actions/FilterActions';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';


function getCurrentState( props ) {

	return {

		open 	: 	props.open
	}
}

var CreateClientTag = React.createClass({

	getInitialState : function() {

		var currentState  		= getCurrentState( this.props );
		currentState.newTagName = "";

		return currentState;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.open !== nextProps.open ) {

			return true;
		}

		return false;
	},

	handleClose : function() {

		FilterActions.closeLightbox();

	},

	setNewTagName : function( event ) {

		this.setState({

			newTagName : event.target.value
		})

	},

	render : function() {

		const actions = [
	    	<FlatButton
	        	label="Cancel"
	        	secondary={true}
	        	onTouchTap={this.handleClose}
	      	/>,
	      	<FlatButton
	        	label="Submit"
	        	primary={true}
	        	keyboardFocused={true}
	        	onTouchTap={this._createTagType}
	      	/>
      	];

		return (

			<div>

				<Dialog
		          	title="Create new client tag"
		          	modal={false}
		          	actions={actions}
		          	open={this.state.open}
		          	onRequestClose={this.handleClose}
		        >
		        	<form className="form-horizontal">

	                	<input className="left" style={{ width : 300 }} onChange={this.setNewTagName} type="text" />
	                </form>
		        </Dialog>


			</div>

		)
	},


	_createTagType 	: 	function( event ) {

		event.preventDefault();

		this.setState({ open : false });

		var newTag = this.state.newTagName;

		if( newTag.trim() != "" ) {

			FilterActions.createNewTag({

				tag 	: 	newTag
			})
		}
	}
});

export default  CreateClientTag;