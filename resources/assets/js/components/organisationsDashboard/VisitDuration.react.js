import React 					from  	'react';
import moment 					from 	'moment';

function getCurrentState( props ) {

	return {	

		col 		: 	props.col,
		row 		: 	props.rowIndex,
		data 		: 	props.data
	}
}

var VisitDuration = React.createClass({

	getInitialState : function() {

		let currentState 		= getCurrentState( this.props );

		return currentState;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		let duration = "One Page Visit";

		if( this.state.data.getObjectAt( this.state.row ).complete_session_duration > 0 ) {

			duration = moment( "2015-01-01").startOf('day')
				.seconds( this.state.data.getObjectAt( this.state.row ).complete_session_duration )
				.format( 'H:mm:ss' );

		}

		return (

			<div>
				<p style={{ marginTop : 14 }}>{duration}</p>
			</div>
		);
	}
});

export default VisitDuration;