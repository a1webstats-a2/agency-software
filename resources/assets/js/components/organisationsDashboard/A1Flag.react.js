import React 			from 'react';
import CountriesA1 		from './CountriesA1';
import Flag 			from 'react-world-flags'

function getCurrentState( props ) {

	const dataWrapper 	= props.data;

	const country 		= dataWrapper.getObjectAt( props.rowIndex );

	return {

		code 	: 	CountriesA1.getCountryCode( country.country_flat )
	}
}

class A1Flag extends React.Component {

	constructor( props ) {

		super( props );


		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( props ) {

		this.setState( getCurrentState( props ) );
	}

	render() {


		return (

			<div style={{ marginTop : 12, marginLeft : 5 }}>
				<Flag code={this.state.code} height={12} />

			</div>
		)
	}
}

export default A1Flag;