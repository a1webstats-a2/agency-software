import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import SearchByURLCheckbox from './SearchByURLCheckbox.react';
import SearchByKeyword from './SearchByKeyword.react';
import SearchByReferrerCheckbox from './SearchByReferrerCheckbox.react';
import CountryCheckbox 	from './SearchByCountryCheckbox.react';
import OrganisationCheckbox from './SearchByOrganisationCheckbox.react';
import SearchByRefreshIndicator from './SearchByRefreshIndicator.react';
import Checkbox from 'material-ui/Checkbox';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import RaisedButton from 'material-ui/RaisedButton';

const refreshIndicatorStyle = {
  	container: {
    	position: 'relative',
  	},
  	refresh: {
    	display: 'inline-block',
    	position: 'relative',
  	}
};

const styles = {
 
  	checkbox: {
   	 	
   	 	marginBottom: 16,
  	},

  	raisedButton : {

  		marginRight 	: 	12,
  		marginTop 		: 	0,
  		marginBottom 	: 	40,
  		width 			: 	180
  	}
};

function getTypesHumanFriendlyTitle( type ) {

	var type = type.toLowerCase();

	var friendlyTitles = {

		'customfilterreferrer' 			: 	'Referrer',
		'customfilterreferrercountry' 	: 	'Country',
		'organisationfilter' 			: 	'Organisation',
		'customfilterurl' 				: 	'URL',
		'customfilterkeyword' 			: 	'Keyword'

	};

	if( typeof friendlyTitles[type] !== "undefined" ) {

		return friendlyTitles[type];
	}

	return '';
}

function compareInclude( b,a ) {

	if ( a.include_count < b.include_count )

		return -1;

	if ( a.include_count > b.include_count )

		return 1;

	return 0;
}

function compareNonExplicitInclude( b,a ) {

	if ( a.include_count < b.include_count )

		return -1;

	if ( a.include_count > b.include_count )

		return 1;

	return 0;
}

function compareExclude( b,a ) {

	if ( a.exclude_count < b.exclude_count )

		return -1;

	if ( a.exclude_count > b.exclude_count )

		return 1;

	return 0;
}

function compareFirstPageVisited( b,a) {

	if ( a.first_page_visited_count < b.first_page_visited_count )

		return -1;

	if ( a.first_page_visited_count > b.first_page_visited_count )

		return 1;

	return 0;
}

function compareNotFirstPageVisited( b,a ) {

	if ( a.not_first_page_visited_count < b.not_first_page_visited_count )

		return -1;

	if ( a.not_first_page_visited_count > b.not_first_page_visited_count )

		return 1;

	return 0;
}

function compareLastPageVisited( b,a ) {

	if ( a.last_page_visited_count < b.last_page_visited_count )

		return -1;

	if ( a.last_page_visited_count > b.last_page_visited_count )

		return 1;

	return 0;
}

function compareNotLastPageVisited( b,a ) {

	if ( a.not_last_page_visited_count < b.not_last_page_visited_count )

		return -1;

	if ( a.not_last_page_visited_count > b.not_last_page_visited_count )

		return 1;

	return 0;
}


function getCurrentState( props ) {

	var searchData = props.searchByData;

	return {

		searchByData 				: searchData,
		searchCriteria 				: searchData.searchCriteria,
		noSearchResults				: searchData.searchCriteria.noSearchResults,
		inclusionType 				: searchData.inclusionType,
		allFilters 					: searchData.allFilters,
		searchByLoading 			: searchData.searchByLoading,
		type 						: props.type,
		selectAll 					: searchData.selectAll,
		filtersBeingEdited 			: searchData.filtersBeingEdited,
		filterChange 				: searchData.filterChange,
		narrowedOptions 			: searchData.narrowedOptions,
		selectedCustomFilterType 	: searchData.selectedCustomFilterType,

		allowedSearchTypes 			: [

			'customfilterreferrer',
			'customfilterreferrercountry',
			'organisationfilter',
			'customfilterurl',
			'customfilterkeyword'
		]
	}
}

var SearchBy = React.createClass({

	getInitialState : function() {

		var currentState 				= getCurrentState( this.props );
		currentState.keyboardPressLapse = null;
		currentState.searchString 		= "";

		return currentState;
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.state.searchString !== nextState.searchString ) {

			return true;
		}

		if( this.props.searchByData.countryIncludeType !== nextProps.searchByData.countryIncludeType ) {

			return true;
		}

		if( this.props.searchByData.organisationIncludeType !== nextProps.searchByData.organisationIncludeType ) {

			return true;
		}

		if( this.props.searchByData.filtersBeingEdited !== nextProps.searchByData.filtersBeingEdited ) {

			return true;
		}

		if( this.props.searchByData.searchString !== nextProps.searchByData.searchString ) {

			return true;
		}
			
		if( this.props.searchByData.narrowedOptions !== nextProps.searchByData.narrowedOptions ) {

			return true;
		}
			
		if( this.props.searchByData.searchByLoading !== nextProps.searchByData.searchByLoading ) {

			return true;
		}
			
		if( this.props.searchByData.selectAll !== nextProps.searchByData.selectAll ) {

			return true;
		}
			
		if( this.props.searchByData.inclusionType !== nextProps.searchByData.inclusionType ) {

			return true;
		}
			
		if( this.props.searchByData.allFilters !== nextProps.searchByData.allFilters ) {

			return true;
		} 
			
		if( this.props.searchByData.filterChange !== nextProps.searchByData.filterChange ) {

			return true;
		}

		if( this.props.type !== nextProps.type ) {

			return true;
		}

		return false;
	},

	componentDidMount : function() {

		FilterActions.resetNarrowedOptions();
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},


	render : function() {

		var isSelectAllChecked 			= 	false;

		if( ( this.state.selectAll.toLowerCase() === this.state.type.toLowerCase() ) 
			&& this.state.type != "" ) {

			isSelectAllChecked = true;
		} 

		var nameField 					= 	"";

		var selectAll 					=	false;

		if( this.state.selectAll.toLowerCase() === this.state.type.toLowerCase() ) {

			selectAll = true;
		}

		var narrowedOptions 			=	this.state.narrowedOptions;

		var narrowedOptionsCheckboxes 	= 	[];

		var selectAllCheckbox 			= 	"";

		if( narrowedOptions.length > 0  ) {

			var selectAllCheckbox = (

				<div>
					<div className="row">
						<div className="col-md-12">

						    <RaisedButton label="Select All" onClick={this._selectAll} style={styles.raisedButton} primary={true} />

							<RaisedButton label="Deselect All" onClick={this._deselectAll} style={styles.raisedButton} secondary={true} />

				   		</div>
				   	</div>
				   	<div className="clr"></div>
				</div>
		   	)
		}

		var include = this.state.inclusionType;

		var noResultsMsg = "";

		switch( include.toLowerCase() ) {

			case "include" :

				narrowedOptions.sort( compareInclude );

				break;

			case "nonexplicitinclude" :

				narrowedOptions.sort( compareNonExplicitInclude );

				break;

			case "exclude" :

				narrowedOptions.sort( compareExclude );

				break;

			case "firstpagevisited" :

				narrowedOptions.sort( compareFirstPageVisited );

				break;

			case "notfirstpagevisited" :

				narrowedOptions.sort( compareNotFirstPageVisited );

				break;

			case "lastpagevisited" :

				narrowedOptions.sort( compareLastPageVisited );

				break;

			case "notlastpagevisited" :

				narrowedOptions.sort( compareNotLastPageVisited );

				break;
		}


		var countField 		=	"include_count";

		switch( this.state.inclusionType.toLowerCase() ) {

			case "include" :

				countField = "include_count";

				break;

			case "nonexplicitinclude" :

				countField = "include_count";

				break;

			case "exclude" :

				countField = "exclude_count";

				break;

			case "firstpagevisited" :

				countField = "first_page_visited_count";

				break;

			case "notfirstpagevisited" :

				countField = "not_first_page_visited_count";

				break;

			case "lastpagevisited" :

				countField = "last_page_visited_count";

				break;

			case "notlastpagevisited" :

				countField = "not_last_page_visited_count";

				break;

		}

		
		var returnOptions = [];

		if( narrowedOptions.length > 0 ) {

			narrowedOptions.map( function( obj, i ) {

				if( obj[countField] > 0 ) {

					returnOptions.push( obj );
				}
			});

			switch( this.state.type.toLowerCase() ) {

				case "customfilterkeyword" :

					nameField = "value";

					narrowedOptionsCheckboxes = returnOptions.map( function( keyword, i ) {

						return (

							<SearchByKeyword filtersBeingEdited={this.state.filtersBeingEdited} searchByData={this.state.searchByData} include={include} selectAll={selectAll} key={i} myKey={i} nameField={nameField} type={this.state.type} data={keyword} />
						);

					}.bind( this ));

					break;

				case "customfilterurl" :

					nameField  = "url";

					narrowedOptionsCheckboxes = returnOptions.map( function( option, i ) {

						return (	

							<SearchByURLCheckbox filtersBeingEdited={this.state.filtersBeingEdited} searchByData={this.state.searchByData} include={include} selectAll={selectAll} key={i} myKey={i} nameField={nameField} type={this.state.type} data={option} />
						);

					}.bind( this ));

					break;

				case "customfilterreferrer" :

					nameField = "name";

					narrowedOptionsCheckboxes = returnOptions.map( function( option, i ) {

						return (	

							<SearchByReferrerCheckbox filtersBeingEdited={this.state.filtersBeingEdited} searchByData={this.state.searchByData} include={include} selectAll={selectAll} key={i} myKey={i} nameField={nameField} type={this.state.type} data={option} />
						);

					}.bind( this ));

					break;

				case "customfilterreferrercountry" :

					narrowedOptionsCheckboxes = returnOptions.map( function( option, i ) {

						return (	

							<CountryCheckbox key={i} filtersBeingEdited={this.state.filtersBeingEdited} searchByData={this.state.searchByData} include={include} searchByCheckbox={true} myKey={i} country={option} />
							
						);

					}.bind( this ));

					break;

				case "organisationfilter" :

					narrowedOptionsCheckboxes = returnOptions.map( function( option, i ) {

						return (	

							<OrganisationCheckbox filtersBeingEdited={this.state.filtersBeingEdited} searchByData={this.state.searchByData} include={include} searchByCheckbox={true} key={i} myKey={i} organisationData={option} />
							
						);

					}.bind( this ));

					break;
			}
		} 


		if( returnOptions.length === 0 && this.state.searchCriteria.searchString !== "" ) {
			
			noResultsMsg = (

				<div className="row">
					<div className="col-md-12">

						<div className="alert alert-warning">
							Sorry, there are no matching results 
						</div>
					</div>
				</div>
			)
		}


		var friendlyType = getTypesHumanFriendlyTitle( this.state.type.toLowerCase() );

		if( this.state.allowedSearchTypes.indexOf( this.state.selectedCustomFilterType ) > -1 ) {
		
			return(

				<div className="row">

					<div className="col-md-12">

						<div className="searchBy">

							<div className="row">

								<div className="col-md-12">
									<br />
						   			<h4>Search {friendlyType}</h4>
						   			<br />
						   		</div>


						   	</div>

						   	<div className="row">
						   		<div className="col-md-11">
							   		<div className="form-group">
									    <input type="text" onChange={this._findResults} value={this.state.searchString} className="form-control" placeholder="Search" />
									</div>
								</div>

								<div className="col-md-1">

						   			<SearchByRefreshIndicator isLoading={this.state.searchByLoading} />

								</div>
						   	</div>

						   	<div className="clr"></div>

						   	{noResultsMsg}

						   	<br />

						   	{selectAllCheckbox}

						   	{narrowedOptionsCheckboxes}
						</div>
					</div>
				</div>
			);

		} else {

			return (

				<div></div>
			)
		}
	},

	_findResults : function( event ) {

		this.setState({

			searchString : 	event.target.value
		})

		
		if( this.state.searchByLoading === "ready" ) {

			FilterActions.setSearchByLoadingStatus();
		}

		clearTimeout( this.state.keyboardPressLapse );

		var searchString 	= event.target.value;

		var type 			= '';

		var keyboardPressLapse = setTimeout( function() {

			switch( this.state.type.toLowerCase() ) {

				case "customfilterkeyword" :

					type 	= 'keyword';

					break;

				case "customfilterurl" :

					type 	= 'url';

					break;

				case "customfilterreferrer" :

					type 	= 'referrer';

					break;

				case "customfilterreferrercountry" :

					type 	= 'country';

					break;

				case "organisationfilter" :

					type 	= 'organisation';

					break;
			}

			FilterActions.narrowSearchOptions({

				searchString 	: 	searchString,
				type 			: 	type,
				fullType 		: 	this.state.type.toLowerCase()
			})


		}.bind( this ), 1000 );

		this.setState({

			keyboardPressLapse : keyboardPressLapse
		})
	},

	_selectAll : function() {

		var narrowedOptions 			= 	FiltersStore.getNarrowedOptions();

		FilterActions.setSearchBySelectAll( this.state.type );


		var countField 		=	"include_count";

		switch( this.state.inclusionType.toLowerCase() ) {

			case "include" :

				countField = "include_count";

				break;

			case "nonexplicitinclude" :

				countField = "include_count";

				break;

			case "exclude" :

				countField = "exclude_count";

				break;

			case "firstpagevisited" :

				countField = "first_page_visited_count";

				break;

			case "notfirstpagevisited" :

				countField = "not_first_page_visited_count";

				break;

			case "lastpagevisited" :

				countField = "last_page_visited_count";

				break;

			case "notlastpagevisited" :

				countField = "not_last_page_visited_count";

				break;

		}

		var returnOptions = [];

		if( narrowedOptions.length > 0 ) {

			narrowedOptions.map( function( obj, i ) {

				if( obj[countField] > 0 ) {

					returnOptions.push( obj );
				}
			});
		}


		var newFilters = returnOptions.map( function( filter, i ) {

			if( typeof filter.url !== "undefined" ) {

				filter.name = filter.url;
			}

			if( typeof filter.value !== "undefined" ) {

				filter.name = filter.value;
			}


			return {

	   			type          :   this.state.type, 
	   			storedValue   :   {
	   				id 			: 	filter.id,
	   				criteria 	: 	this.state.inclusionType,
	   				value 		: 	filter.id, 
	   				textValue 	: 	filter.name,
	   				name 		: 	filter.name,
	   				searchBox 	: 	true
	   			},
	   			id 				: 	filter.id

	   		}

	   		
		}.bind( this ) );

		FilterActions.createFilters( newFilters );
	},

	_deselectAll : function() {

		FilterActions.setSearchBySelectAll( '' );
		
		var narrowedOptions = 	FiltersStore.getNarrowedOptions();

		var removeFilters 	= 	narrowedOptions.map( function( filter, i ) {

   			return {

   				type 	: 	this.state.type,
   				id 		: 	filter.id

   			};
   			
   		}.bind( this ));

   		FilterActions.removeFiltersByType( removeFilters );

	}
})

export default  SearchBy;