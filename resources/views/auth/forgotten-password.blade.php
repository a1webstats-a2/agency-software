@extends( 'templates.signup' )
@section( 'content' )
    
    <div class="row">
        <div  class="col-md-6 col-md-offset-3">

            <div id="loginForm">

                <div id="a1strap">
                    <h1>Forgotten Password</h1>
                </div>

                <br /><br />

                <div>

                    <form method="POST">

                        <div class="form-group">
                            <label for="username">Email Address</label>
                            <input type="text" name="email" class="form-control" id="email" placeholder="Email Address" value="{{ old('email') }}">
                        </div>

                        <br />
                        
                        <button type="submit" alt="login" id="submitButton"></button>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection