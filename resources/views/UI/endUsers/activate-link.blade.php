@extends( 'templates.signup' )

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>

<style>
label { display : inline!important; }
</style>

@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm" class="signupForm">

				<div id="signupLogo">

                    <img src="/images/{{ $loginLogo }}" />

				</div>

				<div class="clr"></div>

				<br /><br />

				<h1>Activate Account</h1>

				@if( $success ) 

					<p>Thank you, your account has been activated!</p>

					<p>You can now <a href="/">log in</a></p>

				@else 
				
					<p>Sorry, there was a problem activating your account.
						If you clicked the link straight from your email client, please
						try copying and pasting the link direct in your browser.</p>

				@endif
			</div>
		</div>
	</div>

@endsection