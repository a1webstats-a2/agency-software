import React from 'react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import ReferrerLink from './links/ReferrerLink.react';
import Paper from 'material-ui/Paper';
import RefreshIndicator from 'material-ui/RefreshIndicator';


const styles = {

	paper 	: 	{

		padding 	: 	20,
		height 		: 	600
	},

	refreshIndicator : {

		display 	: 	'inline-block',
		position 	: 	'relative'

	}
}

function getCurrentState( props ) {

	return {

		referrers 				: 	props.referrers,
		isApplicationResting 	: 	props.isApplicationResting

	};
}

var Top5Referrers = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentDidMount : function() {

	},	

	componentWillUnmount: function() {

	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		var update = false;

		if( this.state.isApplicationResting !== nextState.isApplicationResting ) {

			update = true;
		}

		if( nextProps.referrers.length > 0 ) {

			if( this.props.referrers.length === 0 ) {

				update = true;
	
			} else if( this.props.referrers[0][1] !== nextProps.referrers[0][1] ) {

				update = true;
			}
		}

		return update;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		if( !this.state.isApplicationResting ) {

			return ( 

				<div className="visitorDetails">

					<Paper style={styles.paper} zDepth={4}>
						<div className="blueHeader">
							<h4>Recent Referrers</h4>
						</div>
						<RefreshIndicator
							size={70}
							left={0}
							top={20}
							status="loading"
							style={styles.refreshIndicator}
						/>

					</Paper>
				</div>
			)
		}

		var referrersMain = this.state.referrers.slice( 0, 10 );

		var referrers = referrersMain.map( function( referrer, i ) {

			var referrer = referrer;

			return (

				<tr key={i}>
					<td style={{ width : '70%', overflow : 'hidden' }}><ReferrerLink refName={referrer[1]} refId={parseInt( referrer[0] )} /></td>
					<td style={{ textAlign : 'right' }}>{referrer[2]}</td>
				</tr>
			)
		});

		return (

			<div className="visitorDetails">

				<Paper style={styles.paper} zDepth={4}>
					<div className="blueHeader">
						<h4>Recent Referrers</h4>
					</div>
					<br />
					<table style={{ overflow : 'hidden' }} className="table">

						<tbody>
							{referrers}
						</tbody>

					</table>

				</Paper>
			</div>
		)
	}

});

export default  Top5Referrers;