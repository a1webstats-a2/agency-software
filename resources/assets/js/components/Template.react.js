import React from 'react';
import FilterActions from '../actions/FilterActions';
import LoadFiltersIcon from 'material-ui/svg-icons/action/restore-page';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import AddIcon from 'material-ui/svg-icons/content/add';
import ViewExistingIcon from 'material-ui/svg-icons/action/build';
import Toggle from 'material-ui/Toggle';
import IconButton from 'material-ui/IconButton';
import Checkbox from 'material-ui/Checkbox';

function getCurrentState(props) {
    return {
        myKey: props.myKey,
        template: props.data,
        settings: props.settings,
        hasDefaultTemplate: props.hasDefaultTemplate,
        exportOptions: props.exportOptions
    }
}

const styles = {
    svgIcon: {
        marginRight: 20
    }
}

const Template = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.myKey !== nextProps.myKey) {
            return true;
        }

        if (this.props.hasDefaultTemplate !== nextProps.hasDefaultTemplate) {
            return true;
        }

        if (this.props.template !== nextProps.template) {
            return true;
        }

        if (this.props.settings !== nextProps.settings) {
            return true;
        }

        if (this.props.exportOptions !== nextProps.exportOptions) {
            return true;
        }

        return false;
    },

    render: function () {
        var toggleDisabled = false;

        if (this.state.template.end_user_company_id === -1) {
            toggleDisabled = true;
        }

        var displayInDashboard = false;

        if (this.state.template.display_in_dashboard) {
            displayInDashboard = true;
        }

        const defaultTemplate = (this.state.template.load_at_login) ? true : false;

        let disableApplyTemplateAtLogin = false;

        if (this.state.hasDefaultTemplate && !defaultTemplate) {
            disableApplyTemplateAtLogin = true;
        }

        return (
            <tr>
                <td>{this.state.template.template_name}</td>
                <td>
                    <IconButton onClick={this._loadFilters} tooltip="Load Filters">
                        <LoadFiltersIcon style={styles.svgIcon} color="#170550"/>
                    </IconButton>

                    <IconButton disabled={toggleDisabled} onClick={this._deleteReport} tooltip="Delete Template">

                        <DeleteIcon style={styles.svgIcon} color="#170550"/>
                    </IconButton>
                </td>
                <td>
                    <IconButton disabled={toggleDisabled} onClick={this._setAutomation}
                                tooltip="Create Automated Report">
                        <AddIcon style={styles.svgIcon} color="#170550"/>
                    </IconButton>

                    <IconButton disabled={toggleDisabled} onClick={this._viewAutomatedReports}
                                tooltip="Edit Automated Report">
                        <ViewExistingIcon style={styles.svgIcon} color="#170550"/>
                    </IconButton>
                </td>
                <td>
                    <Toggle
                        style={{marginTop: 10}}
                        onToggle={this._displayInDashboard}
                        disabled={toggleDisabled}
                        toggled={Boolean(displayInDashboard)}
                    />
                </td>
                <td>
                    <Checkbox
                        style={{marginTop: 12}}
                        onCheck={this._setLoadOnLogin}
                        disabled={disableApplyTemplateAtLogin}
                        defaultChecked={Boolean(defaultTemplate)}
                    />
                </td>
                <td>
                    <IconButton onClick={this._openTriggerActions} tooltip="Edit Trigger Actions">
                        <ViewExistingIcon style={styles.svgIcon} color="#170550"/>
                    </IconButton>
                </td>
            </tr>
        )
    },

    _openTriggerActions: function () {
        FilterActions.openTriggerActions(this.state.template.id);
    },
    _setAlerts: function (event, isChecked) {
        FilterActions.setAlertsForTemplate({
            apply: isChecked,
            templateID: this.state.template.id
        })
    },
    _setLoadOnLogin: function (event, isChecked) {
        FilterActions.updateLoadOnLoginForTemplate({
            apply: isChecked,
            templateID: this.state.template.id
        })
    },

    _displayInDashboard: function () {
        FilterActions.setTemplateDisplayedInDashboard(this.state.myKey);
    },

    _setAutomation: function () {
        FilterActions.openSetAutomation({
            template: this.state.template
        });
    },
    _deleteReport: function (event) {
        if (!confirm("Are you sure you want to delete this template?")) {
            return false;
        }

        FilterActions.deleteReportTemplate(this.state.template.id);
    },
    _viewAutomatedReports: function (event) {
        event.preventDefault();

        FilterActions.viewExistingAutomatedReports({
            template: this.state.template
        });
    },
    _loadFilters: function () {
        FilterActions.setTemplateFilters({templateId: this.state.template.id});
    }

});

export default Template;
