import AdvancedFiltersDispatcher from '../dispatcher/AdvancedFiltersDispatcher';
import AdvancedFilterConstants from '../constants/AdvancedFilterConstants';

const AdvancedFilterActions = {
    deleteSavedScenario: function (id) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.DELETE_SAVED_SCENARIO,
            data: id
        })
    },
    updateSavedScenario: function (scenarioID) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.UPDATE_SAVED_SCENARIO,
            data: scenarioID
        })
    },
    resetFiltersNoAction: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.RESET_ADVANCED_FILTERS_NO_ACTION
        })
    },
    resetFilters: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.RESET_ADVANCED_FILTERS
        })
    },
    changeIncludeExcludeForFilterSet: function (includeExcObj) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.CHANGE_INCLUDE_EXCLUDE_FOR_FILTER_SET,
            data: includeExcObj
        })
    },
    nextChoicesPage: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.NEXT_CHOICE_PAGE
        })
    },
    loadScenario: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.LOAD_SCENARIO
        })
    },
    setScenarioToLoad: function (scenarioID) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.SET_SCENARIO_TO_LOAD,
            data: scenarioID
        })
    },
    prevChoicesPage: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.PREV_CHOICE_PAGE
        })
    },

    saveScenarioData: function (scenarioID) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SAVE_SCENARIO_DATA,
            data: scenarioID
        })
    },

    setScenarioName: function (dataObj) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_SCENARIO_NAME,
            data: dataObj
        })
    },

    setSelectAnyThatMatch: function (isChecked) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_SELECT_ANY_THAT_MATCH,
            data: isChecked
        })
    },

    editAdvancedFilter: function (filter) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.EDIT_ADVANCED_FILTER,
            data: filter
        })
    },

    setScenarioCurrentAndOr: function (andOrObj) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_SCENARIO_CURRENT_AND_OR,
            data: andOrObj
        })
    },

    setTimerSettings: function () {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_TIMER_SETTINGS
        })
    },

    updateTimeSettings: function (timeSettings) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.UPDATE_TIME_SETTINGS,
            data: timeSettings
        })
    },

    openTimeSettings: function (choiceID) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.OPEN_TIME_SETTINGS,
            data: choiceID
        })
    },
    showData: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.SHOW_DATA
        })
    },
    setAndOrForScenario: function (andOrObj) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.SET_AND_OR_FOR_SCENARIO,
            data: andOrObj
        })
    },
    selectAll: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.SELECT_ALL
        })
    },
    deselectAll: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.DESELECT_ALL
        })
    },

    deleteFilterSet: function (filterSetObj) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.DELETE_FILTER_SET,
            data: filterSetObj
        })
    },

    closeTimerSettings: function () {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.CLOSE_TIMER_SETTINGS
        })
    },

    setSearchText: function (searchText) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_SEARCH_TEXT,
            data: searchText
        })
    },

    setChoices: function () {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_CHOICES
        })
    },

    setAndOr: function (andOr) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_AND_OR,
            data: andOr
        })
    },

    deleteScenario: function (scenarioID) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.DELETE_SCENARIO,
            data: scenarioID
        })
    },

    setIncludeExclude: function (includeExcObj) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_INCLUDE_EXCLUDE_FOR_SCENARIO,
            data: includeExcObj
        })
    },

    setFilterChoice: function (choiceObj) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_FILTER_CHOICE,
            data: choiceObj
        })
    },

    straightToExport: function () {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.STRAIGHT_TO_EXPORT
        })
    },

    setSavedScenarios: function (scenarios) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.SET_SAVED_SCENARIOS,
            data: scenarios
        })
    },

    addFilter: function (scenarioID) {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.ADD_FILTER,
            data: scenarioID
        })
    },

    firstLoad: function () {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.FIRST_LOAD,
        })
    },

    createNewScenario: function () {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.CREATE_NEW_SCENARIO
        })
    },

    cancelCreateFilter: function () {

        AdvancedFiltersDispatcher.dispatch({

            actionType: AdvancedFilterConstants.CANCEL_CREATE_FILTER
        })
    },
    saveFilter: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.SAVE_FILTER
        })
    },
    openFilterChoices: function (filterType) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.OPEN_FILTER_CHOICES,
            data: filterType
        })
    },
    setNewTemplateName: function (name) {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.SET_NEW_TEMPLATE_NAME,
            data: name
        })
    },
    createNewTemplate: function () {
        AdvancedFiltersDispatcher.dispatch({
            actionType: AdvancedFilterConstants.CREATE_NEW_TEMPLATE,
        })
    },
}

export default AdvancedFilterActions;
