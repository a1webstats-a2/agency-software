import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import Checkbox from 'material-ui/Checkbox';
import FilterActions from '../../actions/FilterActions';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState() {
    return {
        tagTypes: FiltersStore.getClientTypesByIndex(),
        autoTags: FiltersStore.getAutoTags(),
    }
}

function autotagIsChecked(autoTags, tagID) {
    if (typeof autoTags[tagID] !== "undefined" &&
        autoTags[tagID]) {
        return true;
    }
    return false;
}

class AutoTagging extends React.Component {
    constructor(props) {
        super(props);
        this.state = getCurrentState(props);
    }

    componentWillReceiveProps(newProps) {
        this.setState(getCurrentState(newProps));
    }

    render() {
        const displayTags = [];
        for (let i in this.state.tagTypes) {
            const tag = this.state.tagTypes[i];
            const isChecked = autotagIsChecked(this.state.autoTags, tag.id);
            displayTags.push(
                <div className="col-md-4" key={i}>
                    <Checkbox
                        checked={isChecked}
                        onCheck={(event, isChecked) => this._autoTag(isChecked, tag.id)}
                        style={{marginBottom: 12}}
                        label={tag.type}/>
                </div>
            )
        }
        return (
            <div>
                <div className="clr"></div>
                <br/>
                <h2 style={{marginLeft: 25}}>Select what tags should automatically be applied</h2>
                <div className="clr"></div>
                <br/>
                <div className="row">
                    {displayTags}
                </div>
                <div className="clr"></div>
                <br/>

                <RaisedButton
                    style={{marginLeft: 25, marginTop: 20}}
                    secondary={true}
                    onTouchTap={() => this._saveChanges()}
                    label="Save Changes"/>
            </div>
        )
    }
    _saveChanges() {
        FilterActions.saveAutoTags();
    }
    _autoTag(isChecked, tagID) {
        let autoTags = this.state.autoTags;
        autoTags[tagID] = isChecked;
        FilterActions.storeAutoTags(autoTags);
    }
}

export default AutoTagging;
