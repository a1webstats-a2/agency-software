import React from 'react';
import {Gmaps, Marker, InfoWindow} from 'react-gmaps';

function getCurrentState(props) {
    return {
        longitude: props.longitude,
        latitude: props.latitude,
        organisation: props.organisation,
        locationString: props.locationString,
        isGeolocated: props.isGeolocated,
        organisationName: props.organisationName
    }
}

const A2GoogleMap = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.open !== nextProps.open) {
            return true;
        }
        if (this.props.longitude !== nextProps.longitude) {
            return true;
        }
        if (this.props.locationString !== nextProps.locationString) {
            return true;
        }
        if (this.props.latitude !== nextProps.latitude) {
            return true;
        }
        return false;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    onMapCreated(map) {
        map.setOptions({
            disableDefaultUI: true,
            scrollwheel: false,
            scaleControl: true,
            zoomControl: true
        });
    },
    onDragEnd(e) {
    },
    onCloseClick() {
    },
    onClick(e) {
    },
    render: function () {
        if (!this.state.longitude) {
            return (<div></div>);
        }

        let locationText;

        if (this.state.isGeolocated) {
            locationText = (
                <p>The user has allowed us to confirm their location:</p>
            )
        } else {
            locationText = (
                <div>
                    <p>Please be aware that the location information below corresponds
                        to the IP address acquired upon the users visit. If the user / organisation
                        does not have a fixed IP address, the location on display could belong to the ISP
                        and therefore may not be accurate.</p>
                </div>
            )
        }
        return (
            <div className="a2GoogleMap">
                {locationText}

                <div className="clr"></div>
                <br/>

                <Gmaps
                    scrollwheel={false}
                    zoomControl={false}
                    width={'100%'}
                    height={'250px'}
                    lat={this.state.latitude}
                    lng={this.state.longitude}
                    zoom={12}
                    styles={
                        [{
                            "featureType": "all",
                            "elementType": "labels",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "administrative",
                            "elementType": "all",
                            "stylers": [{"visibility": "off"}, {"color": "#efebe2"}]
                        }, {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [{"color": "#efebe2"}]
                        }, {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [{"color": "#efebe2"}]
                        }, {
                            "featureType": "poi.attraction",
                            "elementType": "all",
                            "stylers": [{"color": "#efebe2"}]
                        }, {
                            "featureType": "poi.business",
                            "elementType": "all",
                            "stylers": [{"color": "#efebe2"}]
                        }, {
                            "featureType": "poi.government",
                            "elementType": "all",
                            "stylers": [{"color": "#dfdcd5"}]
                        }, {
                            "featureType": "poi.medical",
                            "elementType": "all",
                            "stylers": [{"color": "#dfdcd5"}]
                        }, {
                            "featureType": "poi.park",
                            "elementType": "all",
                            "stylers": [{"color": "#bad294"}]
                        }, {
                            "featureType": "poi.place_of_worship",
                            "elementType": "all",
                            "stylers": [{"color": "#efebe2"}]
                        }, {
                            "featureType": "poi.school",
                            "elementType": "all",
                            "stylers": [{"color": "#efebe2"}]
                        }, {
                            "featureType": "poi.sports_complex",
                            "elementType": "all",
                            "stylers": [{"color": "#efebe2"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "road.arterial",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#ffffff"}]
                        }, {
                            "featureType": "road.arterial",
                            "elementType": "geometry.stroke",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "road.local",
                            "elementType": "geometry.fill",
                            "stylers": [{"color": "#fbfbfb"}]
                        }, {
                            "featureType": "road.local",
                            "elementType": "geometry.stroke",
                            "stylers": [{"visibility": "off"}]
                        }, {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [{"visibility": "off"}]
                        }, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#a5d7e0"}]}]}
                    loadingMessage={'Loading map'}
                    params={{v: '3.exp'}}
                    onMapCreated={this.onMapCreated}>
                    <Marker
                        lat={this.state.latitude}
                        lng={this.state.longitude}
                        draggable={true}
                        onDragEnd={this.onDragEnd}/>
                    <InfoWindow
                        lat={this.state.latitude}
                        lng={this.state.longitude}
                        content={this.state.organisationName}
                        onCloseClick={this.onCloseClick}/>
                </Gmaps>
            </div>
        );
    }
});
export default A2GoogleMap;
