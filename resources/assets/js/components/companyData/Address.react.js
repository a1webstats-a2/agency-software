import React from 'react';
import FilterActions from '../../actions/FilterActions.js';
import FiltersStore from '../../stores/FiltersStore.js';


var Address = React.createClass({

	getInitialState : function() {

		return {

            type    : this.props.type,
			address : this.props.address
		}
	},

    shouldComponentUpdate : function( nextProps, nextState ) {

        return true;
    },

	componentWillReceiveProps : function( newProps ) {

        this.setState({

            type    : newProps.type,
            address : newProps.address
        })
	},


    render : function() {

        var address = this.state.address.map( function( add, i ) {

            return (

                <p key={i}>{add}</p>

            )

        })

        return (

            <div className="address">

                <h4>{this.state.type} Address</h4>

                <br />
                {address}

            </div>
        )

    }
});

export default  Address;