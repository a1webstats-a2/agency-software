import React 			from 'react';
import Divider 			from 'material-ui/Divider';
import Paper 			from 'material-ui/Paper';
import TextField 		from 'material-ui/TextField';
import RaisedButton 	from 'material-ui/RaisedButton';
import Toggle 			from 'material-ui/Toggle';
import FilterActions 	from '../../actions/FilterActions';
import Checkbox 		from 'material-ui/Checkbox';


const style = {

	textfield : {

		marginLeft 	: 20

	},

	button 	: 	{

		margin 	: 12
	},


	toggle 	: {
    	
    	marginTop 		: 16,
    	marginLeft 		: 20,
    	width 			: 200
  	}
};

function getCurrentState() {

	return {

		forename 						: 	'',
		surname 						: 	'',
		email 							: 	'',
		username 						: 	'',
		superUser 						: 	false,
		consentedToTransactionalEmails 	: 	false
 
	};
}

var AddTeamMember = React.createClass({

	getInitialState : function() {

		return getCurrentState();
	},

	passStateBackToParent : function( state ) {

		this.props.newTeamMember( state );
	},

	setSuperUser : function() {

		var superUser = false;

		if( !this.state.superUser ) {

			superUser = true;
		}

		var newState = this.state;

		newState.superUser = superUser;

		this.setState( newState );

		this.passStateBackToParent( newState );

	},

	setUsername : function( event ) {

		var newState = this.state;

		newState.username = event.target.value;

		this.setState( newState );

		this.passStateBackToParent( newState );

	},

	setForename : function( event ) {

		var newState = this.state;

		newState.forename = event.target.value;

		this.setState( newState );

		this.passStateBackToParent( newState );

	},

	setSurname : function( event ) {

		var newState = this.state;

		newState.surname = event.target.value;

		this.setState( newState );

		this.passStateBackToParent( newState );
	},

	setEmail : function( event ) {

		var newState = this.state;

		newState.email = event.target.value;

		this.setState( newState );

		this.passStateBackToParent( newState );
	},

	setConsent : function( isConsented ) {

		var newState 							= this.state;
		newState.consentedToTransactionalEmails = isConsented;
		this.setState( newState );
		this.passStateBackToParent( newState );

	},

	render : function() {

		return (

			<div className="addTeamMember">
	
				<TextField id="username" name="username" onChange={this.setUsername} hintText="Username" style={style.textfield} underlineShow={false} />
				<Divider />
				<TextField id="forename" name="forename" onChange={this.setForename} hintText="Forename" style={style.textfield} underlineShow={false} />
				<Divider />
				<TextField id="surname" name="surname" onChange={this.setSurname} hintText="Surname" style={style.textfield} underlineShow={false} />
				<Divider />
				<TextField id="email" name="email" onChange={this.setEmail} hintText="Email address" style={style.textfield} underlineShow={false} />
				<Divider />
				<Toggle style={style.toggle} label="Super User" onToggle={this.setSuperUser} />

				<div className="clr"></div><br />
				
			</div>
		);
	}
});


export default  AddTeamMember;
