
<html>
    <!--[if IE 7 ]>    <html class="ie ie7" lang="en"> <![endif]-->
    <!--[if IE 8 ]>    <html class="ie ie8" lang="en"> <![endif]-->
    <!--[if IE 9 ]>    <html class="ie ie9" lang="en"> <![endif]-->
    <!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]--> 

    <head>
        <link rel="stylesheet" href="/css/app.css" />    
    </head>

    <body>

        <div class="container">

            <div class="row">

                <header>

                    <div class="col-md-6">


                    </div>
                
                </header>

                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="/">
                                A2 Web Stats
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>
                </nav>
            </div>

            




            @yield( 'content' )

            <div class="clr"></div>

        </div>

        <div class="clr"></div>

        <footer>

           

        </footer>
        


        <?php if( isset( $scripts ) && !empty( $scripts ) ) : ?>

            <?php foreach( $scripts as $scr ) : ?>

                <script src="<?= $scr ?>"></script>

            <?php endforeach ?>

        <?php endif ?>

    </body>

</html>
