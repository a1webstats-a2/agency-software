import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FilterActions from '../../actions/FilterActions';

function getCurrentState(props) {
    return {
        settings: props.settings
    }
}

const Step1 = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        var addr1 = (this.state.settings.addr1) ? this.state.settings.addr1 : '';
        var addr2 = (this.state.settings.addr2) ? this.state.settings.addr2 : '';
        var townOrCity = (this.state.settings.town_or_city) ? this.state.settings.town_or_city : '';
        var postcode = (this.state.settings.postcode) ? this.state.settings.postcode : '';
        var country = (this.state.settings.country) ? this.state.settings.country : '';
        var county = (this.state.settings.county) ? this.state.settings.county : '';

        var country_list = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];

        var countrySelectOptions = country_list.map(function (country, i) {
            return <MenuItem key={i} value={country} primaryText={country}/>
        })

        return (
            <div>
                <div className="stepText flex space-x-6">
                    <span>Step</span><img src="/images/one.png"/><span>of 3</span>
                </div>

                <div className="clr"></div>
                <br/>

                <h2>Registered Billing Address</h2>
                <div className="col-md-6">
                    <TextField
                        value={addr1}
                        onChange={this._setAddr1}
                        floatingLabelText="Address Line 1"
                    />

                    <div className="clr"></div>

                    <TextField
                        value={addr2}
                        onChange={this._setAddr2}
                        floatingLabelText="Address Line 2"
                    />
                    <div className="clr"></div>

                    <TextField
                        onChange={this._setTownCity}
                        value={townOrCity}
                        floatingLabelText="Town/City"
                    />

                </div>

                <div className="col-md-6">
                    <TextField
                        onChange={this._setCounty}
                        value={county}
                        floatingLabelText="County"
                    />
                    <div className="clr"></div>
                    <TextField
                        onChange={this._setPostcode}
                        value={postcode}
                        floatingLabelText="Postcode"
                    />
                    <div className="clr"></div>
                    <SelectField
                        value={country}
                        onChange={this._setCountry}
                        floatingLabelText="Country"
                    >
                        {countrySelectOptions}
                    </SelectField>
                </div>

                <div className="clr"></div>
                <br/><br/>
                <div className="clr"></div>
                <br/><br/>
                <RaisedButton
                    onClick={this._saveBillingDetails}
                    primary={true} label="Proceed to Payment Type Options"/>
                <div className="clr"></div>
                <br/><br/><br/><br/><br/>
            </div>
        )
    },
    _saveBillingDetails: function () {
        var required = [
            'addr1', 'town_or_city', 'postcode', 'country'
        ];

        var stop = false;

        if (!this.state.settings.addr1) {
            return false;
        }

        required.forEach(function (requiredField, i) {
            if (!this.state.settings[requiredField] ||
                this.state.settings[requiredField].trim() === "") {

                stop = true;

                alert("The field \"" + requiredField + "\" must be completed");
            }
        }.bind(this));

        if (stop) {
            return false;
        }

        FilterActions.saveBillingDetails(this.state.settings);
    },
    _setAddr1: function (event, newValue) {
        var currentDetails = this.state.settings;
        currentDetails.addr1 = newValue;

        this.setState({
            settings: currentDetails
        })
    },
    _setAddr2: function (event, newValue) {
        var currentDetails = this.state.settings;
        currentDetails.addr2 = newValue;

        this.setState({
            settings: currentDetails
        })
    },
    _setTownCity: function (event, newValue) {
        var currentDetails = this.state.settings;
        currentDetails.town_or_city = newValue;

        this.setState({
            settings: currentDetails
        })
    },
    _setPostcode: function (event, newValue) {
        var currentDetails = this.state.settings;
        currentDetails.postcode = newValue;

        this.setState({
            settings: currentDetails
        })
    },
    _setCounty: function (event, newValue) {
        var currentDetails = this.state.settings;
        currentDetails.county = newValue;

        this.setState({
            settings: currentDetails
        })
    },

    _setCountry: function (event, key, newValue) {
        var currentDetails = this.state.settings;
        currentDetails.country = newValue;

        this.setState({
            settings: currentDetails
        })
    }
});

export default Step1;
