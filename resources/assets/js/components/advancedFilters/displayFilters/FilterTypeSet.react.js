import React from 'react';
import AdvancedFilterActions from '../../../actions/AdvancedFilterActions';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Lang from '../../../classes/Lang';

function getCurrentState(props) {
    var include = props.include;

    return {
        setID: props.setID,
        filterSetType: props.filterSetType,
        displayTitle: props.displayTypeName,
        types: props.types,
        scenarioID: props.scenarioID,
        include: include,
        andOr: props.andOr,
        anyThatMatch: props.anyThatMatch
    }
}

var FilterTypeSet = React.createClass({
    getInitialState: function () {
        var firstState = getCurrentState(this.props);
        firstState.typesStringDivHeight = 30;
        return firstState;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.state.typesStringDivHeight !== nextState.typesStringDivHeight) {
            return true;
        }

        if (this.props.include !== nextProps.include) {
            return true;
        }

        if (JSON.stringify(this.props.types) !== JSON.stringify(nextProps.types)) {
            return true;
        }

        return false;
    },
    render: function () {
        var typesString = "";

        if (this.state.anyThatMatch.apply && this.state.anyThatMatch.apply !== "0") {
            typesString = "Any values including \"" + this.state.anyThatMatch.pattern + "\"";
        } else {
            this.state.types.forEach(function (type, i) {
                typesString += type;

                if (i !== (this.state.types.length - 1)) {
                    typesString += ", ";
                }
            }.bind(this));
        }

        var fullTypesString = typesString;
        var seeAllText = (this.state.typesStringDivHeight === 30) ? "view all" : "hide";

        if (typesString.length > 50 && this.state.typesStringDivHeight === 30) {
            typesString = (
                <div>
                    {typesString.substring(0, 50)} &nbsp;...&nbsp;&nbsp;
                    <a href="#" onClick={(event) => this._seeFullTypeString(fullTypesString, event)}>
                        {seeAllText}
                    </a>
                </div>
            )
        } else {
            typesString = (
                <div>
                    {fullTypesString} &nbsp;...&nbsp;&nbsp;
                    <a href="#" onClick={(event) => this._seeFullTypeString(fullTypesString, event)}>
                        {seeAllText}
                    </a>
                </div>
            )
        }

        var includeText = "";

        if (this.state.include) {
            includeText = (
                <p className="includeText">{this.state.andOr} where one or more of the options below are found</p>
            )
        } else {
            includeText = (
                <p className="includeText">{this.state.andOr} where none of the options below are found</p>
            )
        }

        var includeExclude = (this.state.include) ? "include" : "exclude";

        return (
            <div>
                <div className="displayFilterDiv">
                    <h4>{Lang.getWordUCFirst(this.state.displayTitle)}</h4>
                    {includeText}
                    <div className="filterSetChoices" style={{height: this.state.typesStringDivHeight}}>
                        {typesString}
                    </div>
                    <div className="clr"></div>
                    <div className="row">
                        <div className="col-md-8">
                            <RadioButtonGroup
                                onChange={this._changeIncludeExclude}
                                name="includeExclude"
                                valueSelected={includeExclude}>
                                <RadioButton
                                    value="include"
                                    label="Include"
                                />
                                <RadioButton
                                    value="exclude"
                                    label="Exclude"
                                />
                            </RadioButtonGroup>
                        </div>
                        <div className="col-md-4">
                            <div className="filterSetActions">
                                <span onClick={this._editTypes} className="editIcon glyphicon glyphicon-pencil"
                                      aria-hidden="true"></span>
                                <span onClick={this._deleteFilterSet} className="deleteIcon glyphicon glyphicon-trash"
                                      aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    _seeFullTypeString: function (fullTypesString, event) {
        event.preventDefault();

        var newHeight = (this.state.typesStringDivHeight === 30) ? 'auto' : 30;

        this.setState({
            typesStringDivHeight: newHeight
        })
    },
    _deleteFilterSet: function () {
        AdvancedFilterActions.deleteFilterSet({
            setID: this.state.setID,
            scenarioID: this.state.scenarioID
        })
    },
    _changeIncludeExclude: function (obj, val) {
        var includeExclude = (val === "include") ? true : false;

        AdvancedFilterActions.changeIncludeExcludeForFilterSet({
            scenarioID: this.state.scenarioID,
            setID: this.state.setID,
            includeExclude: includeExclude
        })
    },
    _editTypes: function () {
        AdvancedFilterActions.openFilterChoices({
            filterType: this.state.filterSetType,
            scenarioID: this.state.scenarioID,
            setID: this.state.setID,
            editing: true
        })
    }
});

export default FilterTypeSet;
