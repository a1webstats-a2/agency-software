import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FilterActions from '../../actions/FilterActions';
import Updating from '../Updating.react';
import UserTipActions from '../../actions/UserTipActions';
import {Link} from "react-router";

function getCurrentState(props) {
    return {
        agentId: props.agentId,
        settings: props.settings,
        updating: props.updatingProp,
        salesforceID: props.salesforceID
    }
}

const ThirdPartyApps = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        let zapier = ''

        if (12 === this.state.agentId) {
            zapier = (
                <div>
                    <div className="row">
                        <div className="col-md-2 flex justify-center">
                            <img style={{marginTop: 25}} height={75} src="/images/crms/zapier.png"/>
                        </div>
                        <div className="col-md-6">
                            <p><Link to='/zapier'>See ready made integrations</Link></p>
                        </div>
                    </div>
                    <div className="clr"></div>
                    <br/>
                </div>
            )
        }

        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <h3>Third Party Apps</h3><br/>
                    </div>
                </div>
                <div className="row" id="developerSettings">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-2 flex justify-center">
                                <img style={{marginTop: 25}} src="/images/salesforceLogo.png"/>
                            </div>
                            <div className="col-md-6">
                                <TextField
                                    value={this.state.settings.salesforce_organisation_id}
                                    onChange={this._setSalesForceKey}
                                    type="password"
                                    fullWidth={true}
                                    floatingLabelText="Salesforce Organisation ID"
                                />
                            </div>
                        </div>
                        <div className="clr"></div>
                        <br/>
                        {zapier}
                        <div className="row">
                            <div className="col-md-2 flex justify-center">
                                <img style={{marginTop: 25}} src="/images/clarity.jpeg"/>
                            </div>
                            <div className="col-md-6">
                                <TextField
                                    value={ this.state.settings.clarity}
                                    onChange={this._setMicrosoftClarityProjectId}
                                    fullWidth={true}
                                    floatingLabelText="Microsoft Clarity Project ID"
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="clr"></div>
                <br/><br/>

                <RaisedButton
                    style={{width: 300, marginTop: 30}}
                    onClick={this._saveThirdPartyAppSettings}
                    primary={true}
                    label="Update"/>

                <div className="clr"></div>
                <br/><br/>

                <Updating updating={this.state.updating}/>
            </div>
        )
    },
    _showDynamicsHelp: function (event) {
        event.preventDefault();

        UserTipActions.showSpecificHelpItem(21);
    },
    _setMicrosoftClarityProjectId: (event, newValue) => {
        FilterActions.setMicrosoftClarityProjectId(newValue)
    },
    _setDynamicsCallbackURL: function (event, newValue) {
        FilterActions.setDynamicsCallbackURL(newValue);
    },
    _setSalesForceKey: function (event, newValue) {
        FilterActions.setSalesforceOrganisationID(newValue);
    },
    _saveThirdPartyAppSettings: function () {
        FilterActions.saveThirdPartyAppSettings();
    },
});

export default ThirdPartyApps;
