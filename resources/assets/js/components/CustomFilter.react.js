import React           from 'react';
import FilterActions   from '../actions/FilterActions' ;
import PageView from 'material-ui/svg-icons/action/pageview';
import Numbered from 'material-ui/svg-icons/editor/format-list-numbered';
import ReferrerIcon from 'material-ui/svg-icons/navigation/chevron-left';
import Timeline from 'material-ui/svg-icons/action/timeline';
import Location from 'material-ui/svg-icons/communication/location-on';

function getFriendlyCriteriaTitles() {

    return {

        include             :   'explicit Include',
        nonexplicitInclude  :   'include',
        notFirstPageVisited :   'not first page visited',
        notLastPageVisited  :   'not last page visited',
        lastPageVisited     :   'last page visited',
        firstPageVisited    :   'first page visited',
        any                 :   ''

    }  
}

var CustomFilter    =  React.createClass({

    getInitialState :   function() {
        
        var displayValue = this.props.value;

        if( typeof this.props.textValue != "undefined" ) {

            displayValue = this.props.textValue;
        }

        var friendlyCriteriaTitles  = getFriendlyCriteriaTitles();

        var friendlyCriteriaTitle   = this.props.criteria;

        if( typeof friendlyCriteriaTitles[this.props.criteria] != "undefined" ) {

            friendlyCriteriaTitle = friendlyCriteriaTitles[this.props.criteria];
        }


        return {

            myKey           :   this.props.myKey,
            criteria        :   friendlyCriteriaTitle,
            value           :   this.props.value,
            type            :   this.props.type,
            displayValue    :   displayValue
        };

    },

    shouldComponentUpdate : function( nextProps, nextState ) {

        return false;
    },

    render  :   function() {

        var svgs = {

            URL                 :   <PageView />,
            'Total Visits'      :   <Numbered />,
            Referrer            :   <ReferrerIcon />,
            'Time on Site'      :   <Timeline />,
            'Referrer Country'  :   <Location />

        }

        var svg = "";

        if( typeof svgs[this.state.type] !== "undefined" ) {

            svg = svgs[this.state.type];
        }

        return (

            <div className="dateFromBox form-group">
                <div className="row">
                    <div className="col-md-2">{svg}</div>
                    <div className="col-md-8 overflowHidden">
                        
                        <label className="control-label">Custom Filter:</label>

                        <p>{this.state.type} {this.state.criteria} {this.state.displayValue}</p>

                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>

            </div>

        )
    },

    getRawType : function( type ) {

        var rawTypes = {

            'Referrer Country' :    'customFilterReferrerCountry'

        };

        if( typeof rawTypes[type] !== "undefined" ) {

            return rawTypes[type];
        }

        return type;
    },

    _removeFilter   :   function( e ) {

        /*

        FilterActions.removeFilterByType({

            type    :   this.getRawType( this.state.type ),
            id      :   this.props.value
        });*/

        FilterActions.removeFilter( this.state.myKey );
    }

     
})

export default  CustomFilter;