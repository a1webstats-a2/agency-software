import React from 'react';
import AdvancedFilterActions from '../../actions/AdvancedFilterActions';
import FilterActions from '../../actions/FilterActions';

function getCurrentState(props) {
    return {
        savedScenarios: props.savedScenarios
    }
}

var GhostScenario = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.savedScenarios !== nextProps.savedScenarios) {
            return true;
        }

        return false;
    },
    render: function () {
        var spacerDiv = "";

        if (this.state.index % 3 === 0) {
            spacerDiv = (
                <div className="clr"></div>
            )
        }

        var savedScenarios = [];

        for (var key in this.state.savedScenarios) {

            let scenario = this.state.savedScenarios[key];

            savedScenarios.push(
                <option key={key} value={scenario.id}>{scenario.name}</option>
            )
        }

        return (
            <div>
                <div className="ghostScenarioBox">
                    <div className="ghostScenarioHeader">
                        <div id="createNewScenario" onClick={this._createNewScenario}>
                            <h4>Add Scenario</h4>
                        </div>
                    </div>

                    <div className="clr"></div>
                    <br/>

                    <div className="displayFiltersContainer">
                    </div>
                </div>
                {spacerDiv}
            </div>
        )
    },
    _manageScenarios: function (event) {
        event.preventDefault();

        FilterActions.setLightbox({
            type: 'manageScenarios',
            id: -1
        })
    },
    _createNewScenario: function () {
        AdvancedFilterActions.createNewScenario();
    },
    _loadScenario: function () {
        AdvancedFilterActions.loadScenario();
    },
    _setScenarioToLoad: function (event) {
        AdvancedFilterActions.setScenarioToLoad({
            scenarioToLoad: event.target.value,
            scenarioID: -1
        });
    }
});

export default GhostScenario;
