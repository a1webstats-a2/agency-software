import React  from 'react';
import TimePicker from 'material-ui/TimePicker';
import Schedule from 'material-ui/svg-icons/action/schedule';

import FilterActions   from '../actions/FilterActions';

function getCurrentState( props ) {

	return {

		time 	: 	props.time
	}
}


var TimeFrom  =  React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( nextProps, nextState ) {

		this.setState( getCurrentState( nextProps ) );
	},	

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.time !== nextProps.time ) {

			return true;
		}

		return false;
	},

	render 	: 	function() {	

		var dateString = "2016-01-01 " + this.state.time;

		var defaultTimeFrom = new Date( dateString );
	
		return(

			<div className="timepicker">
				<TimePicker 
					ref="picker24hr"
					id="timeFrom"
					value={defaultTimeFrom}
		  			format="24hr"
		  			inputStyle={{ color: '#000' }}
		  			textFieldStyle={{ width : 40 }}
		  			hintText=""
		  			onChange={this._handleChangeTimePicker} />
			</div> 	
		);

	},

	_handleChangeTimePicker : function( err, newTime ) {

        FilterActions.editFilter({

            storedValue     	:   newTime,
            type            	:   'timeFrom'
        });
	}

});

export default  TimeFrom;