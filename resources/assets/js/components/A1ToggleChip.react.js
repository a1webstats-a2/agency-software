import React from 'react';
import FilterActions from '../actions/FilterActions';
import Chip from 'material-ui/Chip';
import Business from 'material-ui/svg-icons/communication/business';
import Label from 'material-ui/svg-icons/action/label-outline';
import Location from 'material-ui/svg-icons/communication/location-on';
import Search from 'material-ui/svg-icons/action/search';
import DeviceIcon from 'material-ui/svg-icons/action/important-devices';
import SocialIcon from 'material-ui/svg-icons/social/group';
import BankIcon from 'material-ui/svg-icons/editor/attach-money';
import Avatar from 'material-ui/Avatar';
import TagIcon from 'material-ui/svg-icons/action/label-outline';
import Key from 'material-ui/svg-icons/communication/vpn-key';
import Today from 'material-ui/svg-icons/action/today';
import PageView from 'material-ui/svg-icons/action/pageview';
import Numbered from 'material-ui/svg-icons/editor/format-list-numbered';
import ReferrerIcon from 'material-ui/svg-icons/navigation/chevron-left';
import Timeline from 'material-ui/svg-icons/action/timeline';
import Schedule from 'material-ui/svg-icons/action/schedule';
import PreviousHistoryIcon from 'material-ui/svg-icons/action/restore';
import TrafficIcon from 'material-ui/svg-icons/maps/traffic';
import Toggle from 'material-ui/Toggle';

const styles = {
  	
  	chip 	: {
    	margin 		: 8
  	},

  	chipWrapper : {

  		display  	: 	'flex',
  		flexWrap 	: 	'wrap',
  		float 		: 	'left'

  	},

  	toggle : {
		
		thumbSwitched: {
			
			backgroundColor: '#53b68b',
		},

		trackSwitched: {
			
			backgroundColor: '#ccc',
		},

		thumbOff: {
		   
		    backgroundColor: '#ccc',
		},
		
		trackOff: {
		
		    backgroundColor: '#ccc',
		}
	}
}

function getHumanFriendlyIncludeTypeString( includeType ) {

	var returnString = "";

	switch( includeType.toLowerCase() ) {

		case "nonexplicitinclude" :

			returnString = "visit may contain";

			break;

		case "include" :

			returnString = "visit must include";

			break;

		case "exclude" :

			returnString = "visit must exclude";

			break;

		case "firstpagevisited" :

			returnString = "first page visited";

			break;

		case "notfirstpagevisited" :

			returnString = "not first page visited";

			break;

		case "lastpagevisited" :

			returnString = "last page visited";

			break;

		case "notlastpagevisited" :

			returnString = "not last page visited";

			break;

	}

	return returnString;
}

function getCurrentState( props ) {

	var latestAddedFilters = props.latestAddedFilters;

	var isNew = false;
	
	if( typeof latestAddedFilters[props.filter.type] !== "undefined" ) {

		if( latestAddedFilters[props.filter.type].indexOf( props.filter.storedValue.id ) > -1 ) {

			isNew = true;
		}
	}

	return {

		latestAddedFilters 	: 	props.latestAddedFilters,
		avatar 				: 	props.avatar, 
		type 				: 	props.type,
		filter  			: 	props.filter,
		text 				: 	props.text,
		isNew 				: 	isNew,
		filterVal 			: 	props.filterVal,
		toggled 			: 	props.toggled
	}
}

var A1ToggleChip = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.filter !== nextProps.filter ) {

			return true;
		}

		if( this.props.toggled !== nextProps.toggled ) {

			return true;
		}

		return false;
	},

	render : function() {

		var includeType = "include";

		if( typeof this.state.filter.storedValue.criteria !== "undefined" ) {

			includeType = getHumanFriendlyIncludeTypeString( this.state.filter.storedValue.criteria );
		}

		
		var thisIcon = null;

		switch ( this.state.avatar ) {

			case "bank" :

				thisIcon = <BankIcon />;

				break;

			case "tag" :

				thisIcon = <TagIcon />;

				break;

			case "keyword" :

				thisIcon = <Key />;

				break;

			case "today" 	: 

				thisIcon = <Today />;

				break;

			case "pageView" 	:

				thisIcon = <PageView />;

				break;

			case "numbered" :

				thisIcon = <Numbered />;

				break;

			case "location" :

				thisIcon = <Location />;

				break;

			case "ppc" :

				thisIcon = <PPCIcon />;

				break;

			case "organic" :

				thisIcon = <OrganicIcon />;

				break;

			case "referrerIcon" : 

				thisIcon = <ReferrerIcon />

				break;

			case "timeline" :

				thisIcon = <Timeline />

				break;

			case "business" :

				thisIcon = <Business />

				break;

			case "schedule" :

				thisIcon = <Schedule /> 

				break;

			case "socialIcon" :

				thisIcon = <SocialIcon />;

				break;

			case "deviceIcon" :

				thisIcon = <DeviceIcon />;

				break;

			case "previousHistory" :

				thisIcon = <PreviousHistoryIcon />

				break;

			case "traffic" :

				thisIcon = <TrafficIcon />

				break;
		}

		var avatar 					= thisIcon;

		var avatarBackgroundColor 	= "#170550";

		var backgroundColor 		= "#170550";

		var showCriteriaTypeFor 	= [

			'customfilterurl',
			'customfilterreferrer',
			'customfilterreferrercountry',
			'customfilterkeyword',
			'organisationfilter'
		]

		if( showCriteriaTypeFor.indexOf( this.state.filter.type.toLowerCase() ) === -1 ) {

			var includeTypeDisplay = " ";
		
		} else {

			var includeTypeDisplay = "-> " + includeType + " ->";
		}

		return (

			<div style={styles.chipWrapper}>
				<Chip className="chip" style={styles.chip} >

					<Toggle 
						style={{ float : 'right', width : '20%', marginTop : '5px' }}
						thumbSwitchedStyle={styles.toggle.thumbSwitched} 
						trackSwitchedStyle={styles.toggle.trackSwitched} 
						thumbStyle={styles.toggle.thumbOff}
						onToggle={this._addRemoveFilter}
	  					trackStyle={styles.toggle.trackOff}
	  					toggled={this.state.toggled} />
					<strong>{this.state.type}</strong> {includeTypeDisplay} {this.state.text}
	  				
				</Chip>
			</div>
		)
	},

	_addRemoveFilter : function( event, isChecked ) {

		FilterActions.editFilter({

   			type          	:   "visitorTypeFilter", 
   			storedValue 	:   {

				id 			: 	this.state.filterVal,
				name 		: 	this.state.filterVal,
				value 		: 	isChecked,
				textValue 	: 	isChecked
			},

			id 	: 	this.state.filterVal
		})
	}
});

export default  A1ToggleChip;
