import React 					from 'react';
import ScreenRecorderActions 	from '../../actions/ScreenRecorderActions';

function getCurrentState( props ) {

	return {

		imgSrc 	: 	props.imgSrc,
		width 	: 	props.width
	}
}

class FirstScreenshot extends React.Component {
	
	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( props ) {

		this.setState( getCurrentState( props ) );
	}
	
	render() {

		return (

			<div id="firstScreenshot" ref={ ( divElement ) => this.divElement = divElement }>
				<img 
					src={this.state.imgSrc} 
					width={this.state.width}
					onLoad={ () => this._firstScreenshotLoaded() } />

				<div className="clr"></div>
			</div>
		)
	}

	_firstScreenshotLoaded() {

		ScreenRecorderActions.setRenderedImageHeight( this.divElement.clientHeight );
	}
}

export default FirstScreenshot;