@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>


@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

				 <div id="loginLogo">
                    <img src="/images/{{ $loginLogo }}" />
                </div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<h1>Cancellation Complete</h1>

                	<p>Thank you for your cancellation – your account will continue 
                		working until the end of your current subscription 
                		period and then no further payments will be taken.</p>
                		<br />
                	<p><a href="/">Return to dashboard</a></p>
				</div>
			</div>
		</div>
	</div>
@endsection