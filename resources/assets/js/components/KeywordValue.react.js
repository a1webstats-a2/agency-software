import React from 'react';
import Key from 'material-ui/svg-icons/communication/vpn-key';

import FilterActions from '../actions/FilterActions';
var KeywordValue = React.createClass({

	getInitialState : function() {

		return {

			filter 		: this.props.storedValue,
			textValue 	: this.props.textValue,
			myKey 		: this.props.myKey

		}
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

        return false;
    },

	render 	: function() {

		return (

			<div className="form-group keywordValue">

                <div className="row">

                	<div className="col-md-2">
                		<Key />
                	</div>
                    <div className="col-md-8 overflowHidden">
   			            <label className="control-label">Keyword Value:</label>
                        <p>{this.state.filter.criteria} {this.state.textValue}</p>
                        
                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>
			</div>

		)
	},

	_removeFilter : function() {


		FilterActions.removeFilter( this.state.myKey );

	}
})

export default  KeywordValue;