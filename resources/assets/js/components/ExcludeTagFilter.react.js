import React               from 'react';

import FilterActions       from '../actions/FilterActions';

var ExcludeTagFilter    =  React.createClass({

    getInitialState :   function() {
        
        var displayValue = this.props.value;

        if( typeof this.props.textValue != "undefined" ) {

            displayValue = this.props.textValue;
        }


        return {

            storedValue     :   this.props.storedValue,
            myKey           :   this.props.myKey,
            value           :   this.props.value,
            displayValue    :   displayValue
        };

    },

    shouldComponentUpdate : function( nextProps, nextState ) {

        return false;
    },
    
    render  :   function() {

        return (

            <div className="dateFromBox form-group">
                <label className="control-label">Exclude Tag:</label>
                <div className="row">
                    <div className="col-md-10 overflowHidden">
                        
                        <p>{this.state.displayValue}</p>

                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>

            </div>

        )
    },


    _removeFilter   :   function( e ) {

        FilterActions.removeFilterByType({

            type    :   "excludeTagFilter",
            id      :   this.state.storedValue.id
        });

    }

     
})

export default  ExcludeTagFilter;