import React from 'react';
import FilterActions from '../../../actions/FilterActions';

function getCurrentState( props ) {

 	return {

 		keywordId 	: 	props.keywordId,
 		keyword 	: 	props.keyword
  	}
}

var KeywordLink = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.keywordId !== nextProps.keywordId ) {

			return true;
		}

		if( this.props.keyword !== nextProps.keyword ) {

			return true;
		}

		return false;
	},

	render : function() {

		return (

			<a href="#" onClick={this._createFilter}>{this.state.keyword}</a>
		);		
	},

	_createFilter : function( event ) {

		event.preventDefault();

		var newState  = {

   			type          :   'CustomFilterKeyword', 
   			storedValue   :   {

   				criteria 	: 	'include',
   				value 		: 	this.state.keywordId, 
   				textValue 	: 	this.state.keyword
   			},
            id              :   this.state.keywordId

   		}

   		FilterActions.quickDashboardLink( newState );
	}
});

export default  KeywordLink;