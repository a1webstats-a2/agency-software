import React         from 'react';
import FilterStore   from '../stores/FiltersStore';
import FilterActions from '../actions/FilterActions';


var DisplayByFilter   =   React.createClass({

	getInitialState : function() {

		return {

			value 	: 	2
		}
	},

	render 	: 	function() {

		

		return (

			<div className="displayByFilter">
		   		<form className="criteriaBox">
			   		<div className="form-group">
				   		<h4>Display By</h4>
			   		</div>

			   		<select className="form-control" value={this.state.value} onChange={this._handleChange}>
			        	
			        	<option value={1}>Page Views</option>
			        	<option value={2}>Sessions</option>
			        </select>

		   		</form>
		   	</div>
		);
	},

	_handleChange : function( event ) {

		this.setState({ value : event.target.value });

		FilterActions.setDisplayByValue({

			val : 	event.target.value
		})
	},

});

export default  DisplayByFilter;