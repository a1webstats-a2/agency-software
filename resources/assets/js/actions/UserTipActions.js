import UserTipsDispatcher from '../dispatcher/UserTipsDispatcher';
import UserTipsConstants from '../constants/UserTipsConstants';

const UserTipActions = {
    closeUserTip: function (userTipID) {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.CLOSE_USER_TIP,
            data: userTipID
        })
    },
    storeAndShowUserTip: function (userTipObj) {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.STORE_AND_SHOW_USER_TIP,
            data: userTipObj
        })
    },
    showSpecificHelpItem: function (internalPageID) {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.SHOW_SPECIFIC_HELP_ITEM,
            data: internalPageID
        })
    },
    resetUserTip: function () {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.RESET_USER_TIP
        })
    },
    showHelp: function () {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.SHOW_HELP
        })
    },
    neverShowTipsAgain: function () {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.NEVER_SHOW_TIPS_AGAIN,
        })
    },
    storeUserTip: function (userTip) {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.STORE_USER_TIP,
            data: userTip
        })
    },
    updateTipObj: function (tipObj) {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.UPDATE_TIP_OBJ,
            data: tipObj
        })
    },
    loadOneUserTip: function () {
        UserTipsDispatcher.dispatch({
            actionType: UserTipsConstants.LOAD_ONE_USER_TIP
        })
    }
};
export default UserTipActions;
