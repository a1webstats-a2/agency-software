import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import Checkbox from 'material-ui/Checkbox';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import Person from 'material-ui/svg-icons/social/person';


const styles = {
 
  	checkbox: {
   	 	
   	 	marginBottom: 16,
  	}
};

function getCurrentState( props ) {

	var searchByData = props.searchByData;

	return {

		isChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( props.type, props.data.id ), 
		type 				: 	props.type,
		option 				: 	props.data,
		nameField 			: 	props.nameField,
		textValue 			: 	props.data[props.nameField],
		value 				: 	props.data.id,
		rowCount 			: 	props.data.row_count,
		includeType 		: 	props.include,
		filtersBeingEdited 	: 	props.filtersBeingEdited,
		inclusionType 		: 	searchByData.inclusionType

	}
}

var SearchByKeyword = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.searchByData.inclusionType !== nextProps.searchByData.inclusionType ) {

			return true;
		}

		if( this.props.filtersBeingEdited !== nextProps.filtersBeingEdited ) {

			return true;
		}

		if( nextProps.data.row_count !== this.state.rowCount) {

			return true;
		}

		if( this.state.includeType !== nextProps.include ) {

			return true;
		}

		return false;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );

	},

	render : function() {

		var countField 		=	"include_count";

		switch( this.state.inclusionType.toLowerCase() ) {

			case "include" :

				countField = "include_count";

				break;

			case "nonexplicitinclude" :

				countField = "include_count";

				break;

			case "exclude" :

				countField = "exclude_count";

				break;

		}

		if( this.state.option[countField] === 0 ) {

			return ( <div></div> );
		}

		var	isChecked 		= 	FiltersStore.checkIfFilterTypeAndIndexExists( this.state.type, this.state.option.id );
		
		var resCountClass 	= 	( this.state.filtersBeingEdited ) ? 'greyedOutResultCount' : 'resultCount';

		var label 			= 	(

			<div>

		    	{this.state.option[this.state.nameField]}

		    	<span className={resCountClass}>{this.state.option[countField]} visitors</span>
			</div>
		)

		return (

			<div className="row">
				<div className="col-md-6">
					<Checkbox
				    	label={label}
				    	checked={isChecked}
				    	style={styles.checkbox}
				    	onCheck={this._createFilter}
				    />
				</div>
			</div>


		);
	},

	_createFilter : function() {

		var newState  = {

   			type          :   this.state.type, 
   			storedValue   :   {

   				criteria 	: 	this.state.includeType,
   				value 		: 	this.state.value, 
   				textValue 	: 	this.state.textValue,
   				searchBox 	: 	true
   			},
   			id 				: 	this.state.option.id,
			
   		}

   		var checked = this.state.isChecked;

   		if( this.state.isChecked ) {

   			checked = false;
   		
   		} else {

   			checked = true;
   		}


   		if( checked ) {

   			this.setState({

   				isChecked : true
   			})

   			FilterActions.createFilter( newState );
   		
   		} else {

   			this.setState({

   				isChecked : false
   			})

   			FilterActions.removeFilterByType({

   				type 	: 	'CustomFilterKeyword',
   				id 		: 	this.state.option.id
   			});
   		}

	}

});

export default  SearchByKeyword;