import React from 'react';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import Delete from 'material-ui/svg-icons/action/delete';
import Folder from 'material-ui/svg-icons/file/folder';
import Avatar from 'material-ui/Avatar';
import Badge from 'material-ui/Badge';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import NotificationActions from '../../actions/NotificationActions';
import moment from 'moment';

function getCurrentState( props ) {

	var notification 		= props.data;
	notification.key 		= props.myKey;
	notification.type 		= props.type;
	notification.settings 	= props.settings;

	return notification;
}


var Notification = React.createClass({

	getInitialState : function() {
		
		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {


		return false;
	},

	render : function() {

		var hyperlink = "/internal/download/contents/" + this.state.asset_id;

		var downloadLink = (

			<a href={hyperlink}>
				<FileDownload style={{ marginRight : 20}} color="#170550" />
			</a>
		);

		var archiveLink = '';

		if( this.state.type === "archive" ) {

			archiveLink = (

				<a href="#" onClick={this._deleteNotification}>
					<Delete color="#170550" />
				</a>
			);

		} else {

			archiveLink = (

				<a href="#" onClick={this._archiveNotification}>
					<Folder color="#170550" />
				</a>
			);
		}
		

        var displayDate     =   moment( this.state.notification_date ).format( "Do MMMM YYYY" ); 

        switch( this.state.settings.user.preferred_date_format ) {

            case "dd/mm/yyyy" :

                displayDate = moment( this.state.notification_date ).format( "DD/MM/YYYY" );

                break;

            case "mm/dd/yyyy" :

                displayDate = moment( this.state.notification_date ).format( "MM/DD/YYYY" );

                break;

            case "yyyy-mm-dd" :

                displayDate = moment( this.state.notification_date ).format( "YYYY-MM-DD" );
                
                break;

        }

		var avatarSrc = "/api/proxy/api/v1/user/avatar/" + this.state.created_by_end_user_id;

		return (

			<TableRow key={this.state.key} className="notification">

				<TableRowColumn><Avatar className="floatingAvatar" src={avatarSrc} /> {this.state.forename} {this.state.surname}</TableRowColumn>
				<TableRowColumn>{displayDate}</TableRowColumn>
				<TableRowColumn>{this.state.message}</TableRowColumn>
				<TableRowColumn>
					{downloadLink}
					{archiveLink}
				</TableRowColumn>

			</TableRow>
		)

	}, 

	_deleteNotification : function( event ) {

		event.preventDefault();

		if( confirm( "Are you sure you want to delete this notification" ) ) {

			NotificationActions.deleteNotification( this.state );

		}
	},

	_archiveNotification : function( event ) {

		event.preventDefault();

		NotificationActions.archiveNotification( this.state );
	}
})

export default  Notification;
