import AdvancedFiltersDispatcher from '../dispatcher/AdvancedFiltersDispatcher';
import AdvancedFilterConstants from '../constants/AdvancedFilterConstants';
import EventEmitterObj from 'events';

var EventEmitter = EventEmitterObj.EventEmitter;
import merge from 'merge';
import FiltersStore from './FiltersStore';
import AdvancedFilterActions from '../actions/AdvancedFilterActions';
import FilterActions from '../actions/FilterActions';
import Raven from 'raven-js';
import {browserHistory} from 'react-router'

let _ravenInstalled = false;
let ravenDSN = false;

if (typeof window.location.hostname !== "undefined" &&
    window.location.hostname !== "localhost") {

    ravenDSN = 'https://ba63685621304ec6881296fff1f91196@sentry.io/103582';
    _ravenInstalled = true;
}

Raven.config(ravenDSN)
    .install();

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

var _selectAnyThatMatch = {apply: false, pattern: ''};
var _lightboxSettings = {type: null, id: null};
var _originalLooseFilters = null;
var _lastLightboxSettings = {type: null, id: null};
var _filterChoiceType = "";
var _blankScenario = {
    name: '',
    id: 0,
    filters: [],
    choiceType: "",
    currentChoices: {},
    currentIncludeType: -1,
    currentAnyThatMatch: false,
    currentAndOr: 'AND',
    scenarioCurrentAndOr: 'AND',
    loadScenarioID: -1,
    scenarioNameEdited: false
}

function getBlankScenario() {
    var blankScenario  = JSON.parse(JSON.stringify(_blankScenario));
    blankScenario.name = 'Scenario ' + _scenarioID;
    blankScenario.id   = _scenarioID;

    return blankScenario;
}

var mainScenario = getBlankScenario();
mainScenario.name = "Main Scenario";
var _scenarioID = 1;
var _activeScenario = _scenarioID;
var _scenarioCount = 1;
var _scenarios = {};
var _firstLoadOccurred = false;
var firstBlankScenario = getBlankScenario();
firstBlankScenario.scenarioCurrentAndOr = 'AND';
_scenarios[_scenarioID] = firstBlankScenario;
var _latestFilterSetID = 0;
var _currentFilterChoices = {};
var _currentFilterChoicesType = "";
var _setChoices = {};
var _searchText = "";
var _filterChoiceMode = "create";
var _editingSetID = 0;
var _paginationCurrentPage = 0;
var _savedScenarios = {};
var _scenarioToLoad = {};
var _timeSettings = {
    dialogOpen: false,
    choices: {},
    id: -1,
    type: 'min',
    seconds: 10
}

var _looseScenarioFilters = {};
let _newTemplateName = '';

function getAllChoices(choiceType) {
    let choices = [];
    let analytics = FiltersStore.getAnalyticsData();

    switch (choiceType.toLowerCase()) {
        case "visitortypefilters" :
            choices = [
                [1, "Business"],
                [2, "Education"],
                [3, "ISP"],
                [4, "Crawlbot"],
                [5, "Public"],
                [6, "Unknown"],
                [7, "Complicated"]
            ];

            break;
        case "firstpagevisited" :
            choices = analytics.entryPages;

            break;
        case "campaignsource" :
            choices = analytics.campaignSources;

            break;
        case "campaignmedium" :
            choices = analytics.campaignMediums;

            break;
        case "campaignname" :
            choices = analytics.mailCampaigns;

            break;
        case "geolocation" :
            choices = {
                1: 'User allowed geolocation',
                0: 'User refused geolocation'
            }

            break;
        case "pagevisited" :
            choices = analytics.pages;

            break;
        case "lastpagevisited" :
            choices = analytics.lastPages;

            break;
        case "organisation" :
            choices = analytics.organisations;

            break;
        case "referrer" :
            choices = analytics.referrers;

            break;
        case "countries" :
            choices = analytics.visitorLocations;

            break;
        case "counties" :
            choices = analytics.counties;

            break;
        case "keyword" :
            choices = analytics.keywords;

            break;
        case "deviceType":
            choices = [
                [1, "Desktop"],
                [2, "Tablet"],
                [3, "Mobile"],
            ];

            break;
        case "town" :
            choices = analytics.cities;

            break;
        case "source" :
            choices = [
                [1, "PPC"],
                [2, "Organic"],
                [3, "Direct"]
            ];

            break;
        case "tags" :
            choices = analytics.tags;

            break;
        case "events":
            choices = analytics.events;

            break;
    }

    var regexp = new RegExp(_searchText, 'gi');
    var selection = {};

    choices.forEach(function (choice, i) {
        if (typeof choice[1] !== "undefined" &&
            choice[1]) {
            if (choice[1].match(regexp)) {
                selection[parseInt(choice[0])] = choice[1];
            }
        }
    });

    return selection;
}

browserHistory.listen(location => {
    FilterActions.setCurrentRoute(location.pathname.replace("/", ""));
});

function selectAllSearchChoices() {
    _currentFilterChoices = getAllChoices(_scenarios[_activeScenario].choiceType.toLowerCase());
}

function addFilterToScenario(scenarioID) {
    let newScenarioFilterSets = clone(_scenarios[scenarioID].filters);

    _latestFilterSetID++;

    let newFilters = {};

    for (var key in _currentFilterChoices) {
        newFilters[key] = createRawFilter(key, _currentFilterChoices[key], _scenarios[scenarioID].choiceType);
    }

    let newFilterSet = {
        timeSettings: clone(_timeSettings.choices),
        filters: newFilters,
        type: _scenarios[scenarioID].choiceType,
        setID: _latestFilterSetID,
        include: _scenarios[scenarioID].currentIncludeType,
        andOr: _scenarios[scenarioID].currentAndOr,
        anyThatMatch: _selectAnyThatMatch
    }

    while (typeof newScenarioFilterSets[_latestFilterSetID] !== "undefined") {
        _latestFilterSetID++;
    }

    newScenarioFilterSets[_latestFilterSetID] = newFilterSet;
    _scenarios[scenarioID].filters = newScenarioFilterSets;
    _scenarios[scenarioID].choiceType = '';
    _scenarios[scenarioID].currentChoices = {};
    _scenarios[scenarioID].currentAnyThatMatch = false;
    _scenarios[scenarioID].currentIncludeType = -1;
    _searchText = '';
    _selectAnyThatMatch = false;
}

function openFilterChoices(filterType) {
    advancedFiltersSetLightbox({
        type: 'filterChoices',
        id: -1
    })

    _currentFilterChoices = {};

    let choiceType = filterType.filterType;

    if (filterType.editing) {
        if (_scenarios[filterType.scenarioID] === "undefined") {
            Raven.captureMessage('Error editing filter choices 1', {tags: _scenarios});
        }

        if (_scenarios[filterType.scenarioID].filters[filterType.setID] === "undefined") {
            Raven.captureMessage('Error editing filter choices 2', {tags: _scenarios});
        }

        let filters       = _scenarios[filterType.scenarioID].filters[filterType.setID].filters;
        let thisFilterSet = _scenarios[filterType.scenarioID].filters[filterType.setID];

        if (thisFilterSet.anyThatMatch) {
            if (thisFilterSet.anyThatMatch.apply) {
                _selectAnyThatMatch = true;
                _searchText = thisFilterSet.anyThatMatch.pattern;
            }
        }

        for (var key in filters) {
            var filter = filters[key];
            _currentFilterChoices[filter.storedValue.id] = filter.storedValue.textValue;
        }

        let timeSettingChoices = typeof _scenarios[filterType.scenarioID].filters[filterType.setID].timeSettings !== "undefined"
            ? _scenarios[filterType.scenarioID].filters[filterType.setID].timeSettings
            : {}

        _timeSettings.choices = timeSettingChoices;
    }

    _editingSetID = filterType.setID,
        _filterChoiceMode = filterType.editing,
        _filterChoiceType = choiceType;

    _activeScenario = filterType.scenarioID;
    _currentFilterChoicesType = choiceType;
    _scenarios[filterType.scenarioID].choiceType = choiceType;
}

function closeLightbox() {
    _lastLightboxSettings = _lightboxSettings;
    _lightboxSettings = {type: null, id: null};
}

function createNewScenario() {
    _scenarioID++;
    _scenarios[_scenarioID] = getBlankScenario();
    _scenarioCount++;
}

function advancedFiltersSetLightbox(lightboxObj) {
    let lightboxSettings = typeof lightboxObj.data !== "undefined"
        ? lightboxObj.data
        : lightboxObj

    _lightboxSettings.type = lightboxSettings.type;
    _lightboxSettings.id = lightboxSettings.id;
}

function cancelCreateFilter() {
    _scenarios[_activeScenario].choiceType = '';

    closeLightbox();
}

function editChoices() {
    let activeScenario   = _activeScenario;
    let setID            = _editingSetID;
    let selection        = clone(_currentFilterChoices);
    let currentFilters   = clone(_scenarios[activeScenario].filters[setID].filters);
    let newFilters       = {};
    let newFiltersLength = 0;

    let allChoices = getAllChoices(_scenarios[activeScenario].choiceType);

    for (let key in allChoices) {
        if (typeof selection[key] !== "undefined") {
            newFiltersLength++;
            newFilters[key] = currentFilters[key];
        }
    }

    let anyThatMatch = _scenarios[activeScenario].filters[setID].anyThatMatch;

    if (!_selectAnyThatMatch) {
        anyThatMatch = {
            pattern: '',
            apply: false
        };
    }

    _scenarios[activeScenario].filters[setID].anyThatMatch = anyThatMatch;

    for (var key2 in _currentFilterChoices) {
        if (typeof newFilters[key2] === "undefined") {
            newFilters[key2] = createRawFilter(key2,
                _currentFilterChoices[key2],
                _currentFilterChoicesType);
        }
    }

    _selectAnyThatMatch = false;

    _scenarios[activeScenario].filters[setID].filters = newFilters;

    let filtersLength = 0;

    for (let objKey in _scenarios[activeScenario].filters[setID].filters) {
        filtersLength++;
    }

    if (filtersLength === 0) {
        delete _scenarios[activeScenario].filters[setID];
    }
}

function createRawFilter(id, display, filterType) {
    var timeCriteria = false;
    var timeSettingsCopy = clone(_timeSettings);

    if (typeof _timeSettings.choices[id] !== "undefined") {
        timeCriteria = timeSettingsCopy.choices[id];
    }

    return {
        type: filterType,
        id: id,
        storedValue: {
            id: id,
            value: id,
            textValue: display,
            timeCriteria: timeCriteria,
            name: display
        }
    }
}

function setChoices() {
    if (_filterChoiceMode) {
        editChoices();
    } else {
        var anyThatMatch = false;

        if (_selectAnyThatMatch && _selectAnyThatMatch.apply) {
            anyThatMatch = {
                apply: true,
                pattern: _searchText
            }
        }

        if (_scenarios[_activeScenario].choiceType === "ip") {
            _currentFilterChoices = {
                1: _searchText
            }
        }

        _scenarios[_activeScenario].currentChoices = _currentFilterChoices;
        _scenarios[_activeScenario].currentAnyThatMatch = anyThatMatch;
    }

    closeLightbox();
}

function saveFilter() {
    closeLightbox();
}

function deleteSavedScenario(scenarioID) {
    delete _savedScenarios[scenarioID];

    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", "/api/proxy/api/v1/advanced-filters/saved-scenarios/" + scenarioID, true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            FilterActions.setSnackbar({
                message: 'Scenario deleted'
            })
        }
    };
}

function updateScenarioName(scenarioData) {
    _scenarios[scenarioData.id].scenarioNameEdited = true;
    _scenarios[scenarioData.id].name = scenarioData.name;
}

function getFilterSets(looseFilters) {
    var visitorTypeFilters = {};
    var firstPageVisited = {};
    var pageVisited = {};
    var countries = {};
    var keyword = {};
    var organisation = {};
    var referrer = {};

    if (looseFilters) {
        looseFilters.forEach(function (filter, i) {
            switch (filter.type.toLowerCase()) {
                case "visitortypefilter" :
                    visitorTypeFilters[filter.storedValue.id] = filter;

                    break;
                case "customfilterentrypage" :
                    firstPageVisited[filter.storedValue.id] = filter;

                    break;
                case "customfilterurl" :
                    pageVisited[filter.storedValue.id] = filter;

                    break;
                case "customfilterreferrercountry" :
                    countries[filter.storedValue.id] = filter;

                    break;
                case "customfilterkeyword" :

                    keyword[filter.storedValue.id] = filter;

                    break;
                case "organisationfilter" :
                    filter.storedValue.textValue = filter.storedValue.name;

                    organisation[filter.storedValue.id] = filter;

                    break;
                case "customfilterreferrer" :
                    referrer[filter.storedValue.id] = filter;

                    break;
            }
        })
    }


    const filterSets = {};

    const allFirstFilterSets = {
        visitorTypeFilters,
        firstPageVisited,
        pageVisited,
        keyword,
        organisation,
        referrer
    };

    var filterSetI = 0;

    for (var key in allFirstFilterSets) {
        const filterSet = allFirstFilterSets[key];
        let andOr = false;

        if (filterSetI > 0) {
            andOr = "AND";
        }

        filterSetI++;

        if (getObjectCount(filterSet) > 0) {
            filterSets[_latestFilterSetID] = {
                setID: _latestFilterSetID,
                filters: filterSet,
                type: key,
                include: true,
                andOr: andOr
            }

            _latestFilterSetID++;
        }
    }

    return filterSets;
}

function setSavedScenarios(scenarios) {
    _savedScenarios = scenarios;
}

function firstLoad() {
    let scenarioCount = 0;

    for (let i in _scenarios) {
        scenarioCount++;
    }

    if (scenarioCount === 0) {
        _scenarioID = 1;
        let firstBlankScenario = getBlankScenario();
        firstBlankScenario.scenarioCurrentAndOr = 'AND';
        _scenarios[_scenarioID] = firstBlankScenario;
    }

    if (_firstLoadOccurred) {
        return;
    }

    var xhr = new XMLHttpRequest();

    xhr.open("GET", "/api/proxy/api/v1/advanced-filters/scenarios", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            let parsed;

            try {
                parsed = JSON.parse(xhr.responseText);
            } catch (e) {
                parsed = false;
            }

            if (parsed) {
                AdvancedFilterActions.setSavedScenarios(parsed);
            }
        }
    };

    _firstLoadOccurred = true;

    var looseFilters = FiltersStore.getFilters();
    _originalLooseFilters = looseFilters;

    _looseScenarioFilters = {
        timeFrom: looseFilters[2],
        timeTo: looseFilters[3],
        minNumberOfPages: {
            id: 1,
            storedValue: {
                id: 1,
                name: 0,
                value: 1,
                textValue: '1'
            },
            type: 'minNumberOfPages',
            round: 0
        },
        maxNumberOfPages: {
            id: 1,
            storedValue: {
                id: 1,
                name: 0,
                value: 0,
                textValue: '0'
            },
            type: 'maxNumberOfPages',
            round: 0
        },
        minSessionDuration: {
            id: 0,
            storedValue: {
                id: 0,
                name: 0,
                value: 0,
                textValue: '0'
            },
            type: 'minSessionDuration',
            round: 0
        },
        newVisitorsOnly: {
            id: 0,
            storedValue: {
                id: 0,
                name: 0,
                value: false,
                textValue: "No"

            },
            type: 'newVisitorsOnly',
            round: 0
        },
        returnVisitorsOnly: {
            id: 0,
            storedValue: {
                id: 0,
                name: 0,
                value: false,
                textValue: "No"
            },
            type: 'returnVisitorsOnly',
            round: 0
        },
        hasVideoOnly: {
            id: 0,
            storedValue: {
                id: 0,
                name: 0,
                value: false,
                textValue: "No"

            },
            type: 'hasVideoOnly',
            round: 0
        }
    };
    FilterActions.setLightbox({
        type: 'dateRange',
        id: -1
    })
}

function getObjectCount(obj) {
    var count = 0;
    for (var k in obj) {
        if (obj.hasOwnProperty(k)) {
            ++count;
        }
    }

    return count;
}

function deselectAll() {
    _currentFilterChoices = {};
}

function deleteScenario(scenarioID) {
    delete _scenarios[scenarioID];

    _scenarioCount--;

    if (_scenarioCount === 0) {
        FilterActions.setFiltersBeforeAdvancedFiltering(_originalLooseFilters);
    }
}

function setSearchText(searchText) {
    if (searchText === null || searchText === "undefined" || !searchText) {
        return false;
    }

    if (searchText.length < _searchText.length) {
        _selectAnyThatMatch = false;
    }

    _searchText = searchText;

    _paginationCurrentPage = 0;
}

function deleteFilterSet(filterSetObj) {
    if (typeof _scenarios[filterSetObj.scenarioID] !== "undefined") {
        delete _scenarios[filterSetObj.scenarioID].filters[filterSetObj.setID];
    }
}

function openTimeSettings(choiceID) {
    closeLightbox();

    _timeSettings.dialogOpen = true;
    _timeSettings.id = choiceID;
}

function setIncludeExcludeForScenario(incExcObj) {
    _scenarios[incExcObj.scenarioID].currentIncludeType = incExcObj.include;
}

function changeIncludeExcludeForFilterSet(incExcObj) {
    if (typeof _scenarios[incExcObj.scenarioID].filters[incExcObj.setID] === "undefined") {
    } else {
        _scenarios[incExcObj.scenarioID].filters[incExcObj.setID].include = incExcObj.includeExclude;
    }
}

function setFilterChoice(choiceObj) {
    if (choiceObj.addFilter) {
        _currentFilterChoices[choiceObj.id] = choiceObj.display;
    } else {
        _selectAnyThatMatch = false;
        _scenarios[_activeScenario].currentAnyThatMatch = false;

        delete _currentFilterChoices[choiceObj.id];
    }
}

function setAndOrForScenario(andOrObj) {
    setAndOr(andOrObj);
}

function setAndOr(andOr) {
    _scenarios[andOr.scenarioID].currentAndOr = andOr.andOr;
}

function updateScenarios() {
    let newScenarios = {};

    for (let i in _scenarios) {
        let scenario = _scenarios[i];
        let filterCount = 0;

        for (let filterKey in scenario.filters) {
            filterCount++;
        }

        if (filterCount > 0) {
            newScenarios[i] = scenario;
        }
    }

    _scenarios = newScenarios;
}

function showData() {
    updateScenarios();

    FilterActions.createScenarioFilters({
        scenarios: _scenarios,
        looseFilters: _looseScenarioFilters
    });

    FilterActions.updateResults();
}

function updateTimeSettings(timeSettings) {

    _timeSettings = timeSettings;
}

function closeTimerSettings() {

    _timeSettings.dialogOpen = false;
    _lightboxSettings = _lastLightboxSettings;
}

function setScenarioCurrentAndOr(andOrObj) {

    _scenarios[andOrObj.scenarioID].scenarioCurrentAndOr = andOrObj.andOr;
}

function straightToExport() {
    FilterActions.advancedFiltersStraightToExport({
        scenarios: _scenarios,
        looseFilters: _looseScenarioFilters
    });
}

function selectAnyThatMatch(isSelected) {
    _selectAnyThatMatch = {
        apply: isSelected,
        pattern: _searchText
    }

    if (isSelected) {
        selectAllSearchChoices();
    } else {
        deselectAll();
    }
}

function prevChoicePage() {
    _paginationCurrentPage--;
}

function resetAdvancedFiltersNoAction() {
    const blankScenario = getBlankScenario();

    _scenarios = {}
    _scenarioID = 1;
    blankScenario.id = 1;
    _scenarios[_scenarioID] = blankScenario;
    _scenarioID++;

    if (_originalLooseFilters) {

        _looseScenarioFilters = {

            timeFrom: _originalLooseFilters[2],
            timeTo: _originalLooseFilters[3],
            minNumberOfPages: {
                id: 1,
                storedValue: {

                    id: 1,
                    name: 0,
                    value: 1,
                    textValue: '1'
                },
                type: 'minNumberOfPages',
                round: 0
            },

            maxNumberOfPages: {

                id: 1,
                storedValue: {

                    id: 1,
                    name: 0,
                    value: 0,
                    textValue: '0'
                },
                type: 'maxNumberOfPages',
                round: 0
            },


            minSessionDuration: {

                id: 0,
                storedValue: {

                    id: 0,
                    name: 0,
                    value: 0,
                    textValue: '0'
                },
                type: 'minSessionDuration',
                round: 0
            }
        };
    }

    FilterActions.resetFiltersOnlyAndNoAction();

    FilterActions.showFiltersInConsole();
}

function resetAdvancedFilters() {
    resetAdvancedFiltersNoAction();

    FilterActions.resetFiltersOnlyAndNoAction();

    FilterActions.setLightbox({
        type: 'dateRange',
        id: -1
    })

    FilterActions.setSnackbar({
        message: 'Filters reset'
    })
}

function nextChoicePage() {

    _paginationCurrentPage++;
}

function editAdvancedFilter(filter) {
    if (typeof _looseScenarioFilters[filter.type] !== "undefined") {
        _looseScenarioFilters[filter.type] = filter;
    }
}

function updateSavedScenario(scenarioID) {
    if (confirm("Do you want to update this saved scenario?")) {
        var xhr = new XMLHttpRequest();

        xhr.open("PUT", "/api/proxy/api/v1/advanced-filters/scenarios/" + scenarioID, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xhr.send(JSON.stringify({
                name: _scenarios[scenarioID].name,
                filters: FiltersStore.getFilters(),
                sortedFilters: FiltersStore.getSortedFilters()
            })
        );

        var thisScenario = {
            id: -1,
            name: _scenarios[scenarioID].name,
            scenario: JSON.stringify(_scenarios[scenarioID])
        }

        _savedScenarios[-1] = thisScenario;

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    delete _savedScenarios[-1];

                    var newScenario = JSON.parse(xhr.responseText);

                    thisScenario.id = newScenario.id;

                    _savedScenarios[newScenario.id] = thisScenario;

                    FilterActions.updateReportTemplates();

                    FilterActions.setSnackbar({
                        message: 'Scenario updated successfully'
                    })
                }
            }
        };
    }
}

function saveScenarioData(scenarioID) {
    FilterActions.createScenarioFilters({
        scenarios: _scenarios,
        looseFilters: _looseScenarioFilters
    });

    var xhr = new XMLHttpRequest();

    xhr.open("POST", "/api/proxy/api/v1/advanced-filters/scenarios");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xhr.send(JSON.stringify({
            name: _scenarios[scenarioID].name,
            filters: FiltersStore.getFilters(),
            sortedFilters: FiltersStore.getSortedFilters()
        })
    );

    var thisScenario = {
        id: -1,
        name: _scenarios[scenarioID].name,
        scenario: JSON.stringify(_scenarios[scenarioID])
    }

    _savedScenarios[-1] = thisScenario;

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                delete _savedScenarios[-1];

                var newScenario = JSON.parse(xhr.responseText);

                thisScenario.id = newScenario.id;

                _savedScenarios[newScenario.id] = thisScenario;

                FilterActions.updateReportTemplates();
                FilterActions.setSnackbar({
                    message: 'Scenario saved successfully'
                })

            } else if (xhr.status === 409) {
                AdvancedFilterActions.updateSavedScenario(scenarioID);
            }
        }
    };
}

function loadScenario() {
    let newScenarioSet = {};
    let newScenarioI = 0;

    if (_scenarios[_scenarioToLoad.loadInScenario] !== null &&
        typeof _scenarios[_scenarioToLoad.loadInScenario] !== "object") {

        FilterActions.setSnackbar({
            message: "Failed loading template"
        });

        return false;
    }

    _scenarios[_scenarioToLoad.loadInScenario].loadScenarioID = -1;

    var filters = {};

    try {
        filters = JSON.parse(_scenarioToLoad.filterBundle.filters);
    } catch (e) {
        console.log("Error loading filters", e);
    }

    var looseFilters = [];

    var containsScenario = false;

    for (var key in filters) {
        if (filters[key].type.toLowerCase() === "scenariofilter") {
            containsScenario   = true;
            let scenarioFilter = filters[key].storedValue.scenario;

            _scenarioID++;
            newScenarioI++;
            newScenarioSet[newScenarioI] = scenarioFilter;
        } else {
            looseFilters.push(filters[key]);
        }
    }

    var filterSets    = getFilterSets(looseFilters);
    var newFilterSets = filterSets;

    if (!containsScenario) {
        let ignoreFilterSets = [
            'dateFrom', 'dateTo', 'timeFrom', 'timeTo'
        ];

        var newScenario = getBlankScenario();

        let newAddFilterSets = [];

        for (let looseFilterKey in filterSets) {
            let looseFilterSet = filterSets[looseFilterKey];

            if (ignoreFilterSets.indexOf(looseFilterSet.type) === -1) {
                newAddFilterSets.push(looseFilterSet);
            }
        }

        if (newAddFilterSets.length > 0) {
            newScenario.filters = newFilterSets;
            newScenarioI++;
            newScenarioSet[newScenarioI] = newScenario;
        }
    }

    for (let scenarioI in _scenarios) {
        newScenarioI++;
        newScenarioSet[newScenarioI] = clone(_scenarios[scenarioI]);
    }

    let takenNames = [];

    for (let newScenarioI in newScenarioSet) {
        let scenario     = newScenarioSet[newScenarioI];
        let scenarioName = scenario.name;
        let displayName  = scenarioName;

        if (takenNames.indexOf(scenarioName) > -1) {
            displayName = "Scenario " + parseInt(newScenarioI);
        }

        takenNames.push(scenarioName);

        scenario.id   = newScenarioI;
        scenario.name = displayName;

        _scenarioID = (parseInt(newScenarioI) + 1);
    }

    _scenarios = newScenarioSet;
}

function setScenarioToLoad(scenarioObj) {
    _scenarios[scenarioObj.scenarioID].loadScenarioID = scenarioObj.scenarioToLoad;

    _scenarioToLoad = {
        filterBundle: _savedScenarios[scenarioObj.scenarioToLoad],
        loadInScenario: scenarioObj.scenarioID
    }
}

function setTimerSettings() {
    _timeSettings.choices[_timeSettings.id] = {

        seconds: _timeSettings.seconds,
        type: _timeSettings.type
    }

    _timeSettings.dialogOpen = false;
    _timeSettings.seconds = 10;
    _timeSettings.type = "more";
    _timeSettings.id = -1;
    _lightboxSettings = _lastLightboxSettings;
}

function updateNewTemplateName(name) {
    _newTemplateName = name;
}

function createNewTemplate () {
    FilterActions.closeLightbox();

    updateScenarios();

    FilterActions.createScenarioFilters({
        scenarios: _scenarios,
        looseFilters: _looseScenarioFilters
    });

    FilterActions.saveReportTemplate({
        templateName: _newTemplateName
    })

    _newTemplateName = '';
}

var AdvancedFiltersStore = merge(EventEmitter.prototype, {
    getScenarios: function () {
        return clone(_scenarios);
    },
    getLooseFilters: function () {
        return clone(_looseScenarioFilters);
    },
    getFilterChoiceType: function () {
        return _filterChoiceType;
    },
    getSearchText: function () {
        return _searchText;
    },
    getCurrentFilterChoices: function () {
        return clone(_currentFilterChoices);
    },
    getTimeSettings: function () {
        return clone(_timeSettings);
    },
    getSelectAnyThatMatch: function () {
        return _selectAnyThatMatch;
    },
    getSetChoices: function () {
        return clone(_setChoices);
    },
    getSavedScenarios: function () {
        return clone(_savedScenarios);
    },
    getPaginationCurrentPage: function () {
        return _paginationCurrentPage;
    },
    getNewTemplateName: function () {
        return _newTemplateName;
    },
    avCheckLightboxOpen: function (type, id) {
        if (_lightboxSettings.type === type && parseInt(_lightboxSettings.id) === id) {
            return true;
        }
        return false;
    },
    emitChange: function () {
        this.emit('change');
    },
    addChangeListener: function (callback) {
        this.on('change', callback);
    },
    removeChangeListener: function (callback) {
        this.removeListener('change', callback);
    }
});

AdvancedFiltersDispatcher.register(function (payload) {

    var returnRes = true;

    switch (payload.actionType) {
        case AdvancedFilterConstants.ADD_FILTER :
            addFilterToScenario(payload.data);

            break;
        case AdvancedFilterConstants.OPEN_FILTER_CHOICES :
            openFilterChoices(payload.data);

            break;
        case AdvancedFilterConstants.SAVE_FILTER :
            saveFilter();

            break;
        case AdvancedFilterConstants.UPDATE_TIME_SETTINGS :
            updateTimeSettings(payload.data);

            break;
        case AdvancedFilterConstants.SET_FILTER_CHOICE :
            setFilterChoice(payload.data);

            break;
        case AdvancedFilterConstants.RESET_ADVANCED_FILTERS_NO_ACTION :
            resetAdvancedFiltersNoAction(payload.data);

            break;
        case AdvancedFilterConstants.CANCEL_CREATE_FILTER :
            cancelCreateFilter();

            break;
        case AdvancedFilterConstants.CLOSE_TIMER_SETTINGS :
            closeTimerSettings();

            break;
        case AdvancedFilterConstants.NEXT_CHOICE_PAGE :
            nextChoicePage();

            break;
        case AdvancedFilterConstants.LOAD_SCENARIO :
            loadScenario();

            break;
        case AdvancedFilterConstants.PREV_CHOICE_PAGE :

            prevChoicePage();

            break;

        case AdvancedFilterConstants.FIRST_LOAD :

            firstLoad();

            break;

        case AdvancedFilterConstants.CREATE_NEW_SCENARIO :

            createNewScenario();

            break;


        case AdvancedFilterConstants.SET_CHOICES :

            setChoices();

            break;

        case AdvancedFilterConstants.DELETE_SAVED_SCENARIO :

            deleteSavedScenario(payload.data);

            break;

        case AdvancedFilterConstants.SET_TIMER_SETTINGS :

            setTimerSettings(payload.data);

            break;

        case AdvancedFilterConstants.SET_SAVED_SCENARIOS :

            setSavedScenarios(payload.data);

            break;

        case AdvancedFilterConstants.SET_AND_OR :

            setAndOr(payload.data);

            break;

        case AdvancedFilterConstants.DESELECT_ALL :

            deselectAll();

            break;

        case AdvancedFilterConstants.SET_SCENARIO_CURRENT_AND_OR :

            setScenarioCurrentAndOr(payload.data);

            break;

        case AdvancedFilterConstants.OPEN_TIME_SETTINGS :
            openTimeSettings(payload.data);

            break;

        case AdvancedFilterConstants.EDIT_ADVANCED_FILTER :

            editAdvancedFilter(payload.data);

            break;

        case AdvancedFilterConstants.SET_SCENARIO_NAME :

            updateScenarioName(payload.data);

            break;

        case AdvancedFilterConstants.SET_SEARCH_TEXT :

            setSearchText(payload.data);

            break;

        case AdvancedFilterConstants.SET_AND_OR_FOR_SCENARIO :

            setAndOrForScenario(payload.data);

            break;

        case AdvancedFilterConstants.DELETE_FILTER_SET :

            deleteFilterSet(payload.data);

            break;

        case AdvancedFilterConstants.STRAIGHT_TO_EXPORT :

            straightToExport();

            break;

        case AdvancedFilterConstants.SAVE_SCENARIO_DATA :

            saveScenarioData(payload.data);

            break;

        case AdvancedFilterConstants.DELETE_SCENARIO :

            deleteScenario(payload.data);

            break;

        case AdvancedFilterConstants.SET_INCLUDE_EXCLUDE_FOR_SCENARIO :

            setIncludeExcludeForScenario(payload.data);

            break;

        case AdvancedFilterConstants.SELECT_ALL :

            selectAllSearchChoices();

            break;

        case AdvancedFilterConstants.SET_SELECT_ANY_THAT_MATCH :
            selectAnyThatMatch(payload.data);

            break;
        case AdvancedFilterConstants.SHOW_DATA :
            showData();

            break;
        case AdvancedFilterConstants.SET_SCENARIO_TO_LOAD :
            setScenarioToLoad(payload.data);

            break;
        case AdvancedFilterConstants.RESET_ADVANCED_FILTERS :
            resetAdvancedFilters();

            break;
        case AdvancedFilterConstants.CHANGE_INCLUDE_EXCLUDE_FOR_FILTER_SET :
            changeIncludeExcludeForFilterSet(payload.data);

            break;
        case AdvancedFilterConstants.UPDATE_SAVED_SCENARIO :
            updateSavedScenario(payload.data);

            break;
        case AdvancedFilterConstants.SET_NEW_TEMPLATE_NAME:
            updateNewTemplateName(payload.data);

            break;
        case AdvancedFilterConstants.CREATE_NEW_TEMPLATE:
            createNewTemplate();

            break;
        default :
            return false;
    }

    AdvancedFiltersStore.emitChange();

    return returnRes;
});

export default AdvancedFiltersStore;


