import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import UserTipActions from '../actions/UserTipActions';
import UserTipsStore from '../stores/UserTipsStore';
import Slide from './Slide.react';
import FiltersStore from '../stores/FiltersStore';

function getCurrentState() {
    return {
        tip: UserTipsStore.getTip(),
        settings: FiltersStore.getAllSettings()
    }
}

const styles = {
    dialogRoot: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: 0
    },
    dialogContent: {
        position: "relative",
        width: 600,
        transform: "",
    },
    dialogBody: {
        paddingBottom: 0
    }
};

class UserTips extends React.Component {
    constructor(props) {
        super(props);
        this._onChange = this._onChange.bind(this);
        this.state = getCurrentState();
    }

    componentDidMount() {
        UserTipsStore.addChangeListener(this._onChange);
    }
    componentWillUnmount() {
        UserTipsStore.removeChangeListener(this._onChange);
    }
    render() {
        let actions = [];
        let tipTitle = this.state.tip.title;
        let slide = '';
        if (this.state.tip.slides.length > 0) {
            slide = this.state.tip.slides[this.state.tip.currentSlide];
        }
        if (this.state.tip.slides.length > 1) {
            if ((this.state.tip.currentSlide + 1) !== this.state.tip.slides.length) {
                actions.push(
                    <FlatButton
                        label="Next"
                        primary={true}
                        onTouchTap={() => this._updateTip(this.state.tip.currentSlide + 1, 'currentSlide')}
                    />
                );
            }
            if (this.state.tip.currentSlide > 0) {
                actions.push(
                    <FlatButton
                        label="Previous"
                        primary={true}
                        onTouchTap={() => this._updateTip(this.state.tip.currentSlide - 1, 'currentSlide')}
                    />
                );
            }
        }
        actions.push(
            <FlatButton
                label="Close"
                primary={true}
                keyboardFocused={true}
                onTouchTap={() => this._handleClose()}
            />
        )
        if (this.state.tip.displayOnPageLoad) {
            actions.push(
                <FlatButton
                    label="Don't show tips"
                    labelStyle={{color: 'red'}}
                    primary={true}
                    onTouchTap={() => this._neverShowTipsAgain()}
                />
            )
        }
        return (
            <div>
                <Dialog
                    title={tipTitle}
                    actions={actions}
                    modal={false}
                    bodyStyle={styles.dialogBody}
                    style={styles.dialogRoot}
                    contentStyle={styles.dialogContent}
                    autoScrollBodyContent={true}
                    open={this.state.tip.open}
                    onRequestClose={this.handleClose}
                >
                    <Slide slide={slide}/>
                </Dialog>

            </div>
        )
    }
    _neverShowTipsAgain() {
        if (confirm("Are you sure you want to permanently disable tips?")) {
            UserTipActions.neverShowTipsAgain();
        }
    }
    _handleClose() {
        UserTipActions.closeUserTip(this.state.tip.id);
    }
    _updateTip(newVal, field) {
        let tipObj = this.state.tip;
        tipObj[field] = newVal;
        UserTipActions.updateTipObj(tipObj);
    }
    _onChange() {
        this.setState(getCurrentState());
    }
}

export default UserTips;
