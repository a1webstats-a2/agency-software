
function getState() {

	const conn = new WebSocket( 'ws://localhost:8080' );
	conn.onopen = function( e ) {
    	console.log( "Connection established!" );
	};


	return {

		conn
	}
}


class WebsocketConn {

	constructor() {

		this.state = getState();
	}

	getConnection() {

		return this.state.conn;
	}

}

export default WebsocketConn;