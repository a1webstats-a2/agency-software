var keyMirror = require( 'keyMirror' );

module.exports = keyMirror({

	SET_RENDERED_IMAGE_HEIGHT_2 										: 	null,
	SAVE_RECORDING 														: 	null,
	LOAD_VIDEO 															: 	null,
	STORE_VIDEO 														: 	null,
	START_PLAY_VIDEO													: 	null,
	UPDATE_STATE 														: 	null,
	SET_VIDEO_I 														: 	null,
	SET_NATURAL_HEIGHT	 												: 	null,
	LOAD_HEATMAP 														: 	null,
	STORE_HEATMAP 	 													: 	null,
	SET_DISPLAY_OFFSETS 												: 	null,
	SET_DISPLAY_TYPE 													: 	null,
	PAUSE_VIDEO 														: 	null
});