import React           from 'react';
import FilterActions   from '../actions/FilterActions';
import FilterStore     from '../stores/FiltersStore';
import TextField from 'material-ui/TextField';

var KeywordFilter  =  React.createClass({

    getInitialState :   function() {

        return {

            myKey           :   this.props.myKey,
            storedValue     :   this.props.storedValue.keywordId,
            keywordValue    :   this.props.storedValue.keywordValue,
            textValue       :   this.props.textValue
        };

    },

    shouldComponentUpdate : function( nextProps, nextState ) {

        return false;
    },

    render  :   function() {


        return (

            <div className="dateFromBox form-group">
                <label className="control-label">Keyword Filter:</label>
                <div className="row">
                    <div className="col-md-10 overflowHidden">
                        <p>{this.state.textValue}</p>
                        <TextField
                            name="keyword"
                            id="keyword"
                            hintText="Value (optional)"
                            underlineFocusStyle={{borderColor: '#4F7C8C' }} 
                            onChange={this._setKeywordValue} />
                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>
            </div>
        )
    },

    _setKeywordValue : function( event ) {

        this.setState({ 

            keywordValue : event.target.value

        });

        FilterActions.editFilter({

            id              :   this.state.myKey,
            storedValue     :   {

                keywordId   : this.state.storedValue.keywordId,
                keywordText : this.state.storedValue.keywordText
            },
            type            :   'keyword',
            keywordValue    :   event.target.value 
        });
    },

    _removeFilter   :   function( e ) {

        FilterActions.removeFilter( this.state.myKey );
    }

    
})

export default  KeywordFilter;