import React from 'react';

function getCurrentState( props ) {

	return {

		relationship : 	props.relationship
	}
}

var DisplayTag = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.relationship !== nextProps.relationship ) {

			return true;
		}

		return false;
	},

	render : function() {

		return (

			<div className='tags'>
				<a href="#">{this.state.relationship.type}</a>
			</div>
		)
	}
});

export default DisplayTag;