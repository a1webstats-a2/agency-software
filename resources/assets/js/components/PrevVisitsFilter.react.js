import React from 'react';
import PreviousHistoryIcon from 'material-ui/svg-icons/action/restore';
import FilterActions from '../actions/FilterActions';

function getCurrentState(props) {
    return {
        data: props.data
    }
}

const PrevVisitsFilter = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },

    shouldComponentUpdate: function () {
        return false;
    },
    render: function () {
        return (
            <div className="dateFromBox form-group">
                <div className="row">

                    <div className="col-md-2">
                        <PreviousHistoryIcon/>
                    </div>
                    <div className="col-md-8 overflowHidden">
                        <label className="control-label">Visit History:</label>
                        <p>{this.state.data.storedValue.textValue}</p>
                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>
            </div>
        )
    },
    _removeFilter: function () {
        FilterActions.removeFilterByType({
            type: 'setSessionIDs',
            id: 1
        });
    }
});

export default PrevVisitsFilter;
