import React from 'react';
import FilterAction from '../../actions/FilterActions';
import TrackedCompaniesActions from '../../actions/TrackedCompaniesActions';
import Checkbox from 'material-ui/Checkbox';

const styles = {
    checkbox: {
        marginBottom: 16,
        fontSize: 12,
        fontWeight: 'normal',
    }
};

function getCurrentState(props) {
    return {
        storeType: props.storeType,
        id: props.tagId,
        type: props.tagType,
        myTagTypes: props.relationships,
        checked: props.checked,
        organisationId: props.organisationId
    }
}

const TagType = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        return (
            <div>
                <Checkbox
                    label={this.state.type}
                    checked={this.state.checked}
                    style={styles.checkbox}
                    onCheck={this._saveClientType}
                />
            </div>
        )
    },
    _saveClientType: function (event, isChecked) {
        this.setState({
            checked: isChecked
        })

        let relationship = {
            clientType: this.state.id,
            tagName: this.state.type,
            organisationId: this.state.organisationId,
            action: (isChecked) ? 'create' : 'delete',
            resultKey: null
        }

        switch (this.state.storeType) {
            case 'reportBuilder':
                FilterAction.updateClientRelationship(relationship);

                break;
            case 'tracker':
                TrackedCompaniesActions.trackerUpdateClientRelationship(relationship);

                break;
        }
    }
});

export default TagType;
