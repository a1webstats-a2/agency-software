import React from 'react';
import PageVisit from './PageVisit.react';

function getState(props) {
    return {
        teamSettings: props.teamSettings,
        userSettings: props.userSettings,
        heatmapData: props.heatmapData,
        clientID: props.clientID,
        browser: props.browser,
        resultKey: props.resultKey,
        pages: props.pages,
        parentID: props.parentID,
    }
}

const PageVisits = React.createClass({
    getInitialState: function () {
        return getState(this.props);
    },

    shouldComponentUpdate: function (newProps) {
        if (this.props.clientID !== newProps.clientID) {
            return true;
        }

        if (this.props.heatmapData !== newProps.heatmapData) {
            return true;
        }

        if (this.props.parentID !== newProps.parentID) {
            return true;
        }

        return newProps.pages !== this.props.pages;
    },

    componentWillReceiveProps: function (newProps) {
        this.setState(getState(newProps));
    },

    render: function () {
        let pageVisitsArray = [];
        const numPages = this.state.pages.length;

        Object.keys(this.state.pages).map(function (key) {
            const pageVisit = this.state.pages[key];

            let lastPageVisited = false;

            if ((numPages - 1) === parseInt(key)) {
                lastPageVisited = true;
            }

            pageVisitsArray.push(
                <PageVisit
                    lastPageVisited={lastPageVisited}
                    teamSettings={this.state.teamSettings}
                    userSettings={this.state.userSettings}
                    browser={this.state.browser}
                    parentID={this.state.parentID}
                    key={key}
                    heatmapData={this.state.heatmapData}
                    clientID={this.state.clientID}
                    numPages={numPages}
                    url={pageVisit[0]}
                    pageTitle={pageVisit[5]}
                    queryString={pageVisit[6]}
                    duration={pageVisit[1]}
                    eventID={pageVisit[7]}
                    pageID={pageVisit[11]}
                    https={pageVisit[12]}
                    jobId={pageVisit[13]}
                    eventDescription={pageVisit[8]}
                    pageVisitDate={pageVisit[2]}/>
            );
        }.bind(this));

        return (
            <div className="pageVisits">
                <table className="table">
                    <thead>
                    <tr>
                        <th style={{width: '65%', paddingRight: 30}}>Page</th>
                        <th>Duration</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    {pageVisitsArray}
                    </tbody>
                </table>
            </div>
        )
    }
});

export default PageVisits;
