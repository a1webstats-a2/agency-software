import React from 'react';
import FilterActions from '../../actions/FilterActions';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

const styles = {

    block: {
        maxWidth: 250,
    },
    toggle: {
        marginBottom: 16,
    },
};


var IPFilterItem  =  React.createClass({

    getInitialState :   function() {

        return {

            myKey           :   this.props.myKey,
            storedValue     :   this.props.storedValue,
            rangeStart      :   this.props.storedValue.start,
            rangeEnd        :   this.props.storedValue.end,
            exclude         :   false
        };
    },

    setRangeStart : function( e1 ) {

        this.setState({

            rangeStart :    e1.target.value
        });

        var newRange    =   {

            start   :   e1.target.value,
            end     :   this.state.rangeEnd,
            exclude :   this.state.exclude
        }

        this._updateRange( newRange );
    },

    setRangeEnd : function( e2 ) {

        this.setState({

            rangeEnd    :   e2.target.value
        });

        var newRange    =   {

            start   :   this.state.rangeStart,
            end     :   e2.target.value,
            exclude :   this.state.exclude
        }

        this._updateRange( newRange );
    },

    setType : function() {

        var exclude = false;

        if( !this.state.exclude ) {

            exclude = true;
        }

        this.setState({

            exclude : exclude
        })

        this._updateRange( {

            start   :   this.state.rangeStart,
            end     :   this.state.rangeEnd,
            exclude :   exclude
        } );
    },

    render  :   function() {

        return (

            <div className="dateFromBox form-group">
                <label className="control-label">IP Range:</label>
                <div className="row">
                    <div className="col-md-10 overflowHidden">
                        <TextField
                            hintText="Range Start"
                            name="rangeStart"
                            id="rangeStart"
                            value={this.state.rangeStart}
                            underlineFocusStyle={{borderColor: '#4F7C8C' }} 
                            onChange={this.setRangeStart} />
                        <TextField
                            name="rangeEnd"
                            hintText="Range End"
                            id="rangeEnd"
                            value={this.state.rangeEnd}
                            underlineFocusStyle={{borderColor: '#4F7C8C' }} 
                            onChange={this.setRangeEnd} />
                        <Toggle
                            label="Set to exclude"
                            style={styles.toggle} 
                            toggled={this.state.exclude}
                            onToggle={this.setType} />
                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>
            </div>
        )
    },

    _updateRange    :   function( newRange ) {

        FilterActions.editFilter({

            id              :   this.state.myKey,
            storedValue     :   newRange, 
            type            :   'IPRange'
        });

    },

    _removeFilter   :   function( e ) {

        FilterActions.removeFilter( this.state.myKey );
    }

    
})

export default  IPFilterItem;