import React from 'react';
import NoSubscriptionSetup from './NoSubscriptionSetup.react';
import SubscriptionDetails from '../payment/SubscriptionDetails.react';
import FilterActions from '../../actions/FilterActions';
import NotificationsStore from '../../stores/NotificationsStore';
import NavBar from '../navigation/NavBar.react';
import FiltersStore from '../../stores/FiltersStore';
import UserTipsStore from '../../stores/UserTipsStore';
import UserTips from '../UserTips.react';
import UserTipsHelpLink from '../UserTipHelpLink.react';

function getCurrentState() {
    const settings = FiltersStore.getAllSettings();

    return {
        userType: FiltersStore.getUser(),
        paymentType: FiltersStore.getPaymentType(),
        settings: settings.team,
        newTeamMember: FiltersStore.getNewTeamMember(),
        paymentDisabled: true,
        crypt: FiltersStore.getCrypt(),
        user: FiltersStore.getUser(),
        teamMembers: FiltersStore.getTeamMembers(),
        newTeamMemberOpen: FiltersStore.checkLightboxOpen('newTeamMember', 1),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        }
    }
}

const Payment = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    componentDidMount: function () {
        FilterActions.loadPrices();
        UserTipsStore.addChangeListener(this._onChange);
        FiltersStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        UserTipsStore.removeChangeListener(this._onChange);
        FiltersStore.removeChangeListener(this._onChange);
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        let display;

        if (this.state.settings.using_gocardless || this.state.settings.using_stripe) {
            const usingGocardless = this.state.settings.using_gocardless ? 'GoCardless' : 'Stripe';

            display = (
                <div>
                    <div className="row">
                        <div className="col-md-12">
                            <SubscriptionDetails
                                user={this.state.user}
                                subscribedUntil={this.state.settings.subscribed_until}
                                vendor={usingGocardless}
                            />
                        </div>
                    </div>
                </div>
            )
        } else {
            display = (
                <NoSubscriptionSetup
                    settings={this.state.settings}/>
            )
        }

        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>

                <div className="clr"></div>

                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-12" id="results">
                            <div className="row">
                                <div className="col-md-2">
                                    <h1>Billing</h1>
                                </div>

                                <div className="col-md-10">
                                    <div style={{marginTop: 45}}>
                                        <UserTipsHelpLink/>
                                    </div>
                                </div>
                            </div>

                            {display}
                        </div>
                    </div>
                </div>
                <UserTips/>
            </div>
        )
    },
    _setPaymentType: function (event, value) {
        FilterActions.setPaymentType(value);
    },
    _startGoCardless: function (event) {
        event.preventDefault();

        FilterActions.startGoCardlessProcess();
    },
    _makePayment: function () {
        if (confirm('Are you sure you want to proceed with payment?')) {
            FilterActions.makePayment();
        }
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }
});

export default Payment;
