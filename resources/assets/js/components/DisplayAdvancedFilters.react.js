import React from 'react';
import Dialog from 'material-ui/Dialog';
import DisplayFilterSet from './DisplayFilterSet.react';
import FilterActions from '../actions/FilterActions';
import FlatButton from 'material-ui/FlatButton';
import {browserHistory} from 'react-router';

function getCurrentState(props) {
    return {
        filters: props.filters,
        open: props.open
    }
}

const DisplayAdvancedFilters = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (newProps) {

        this.setState(getCurrentState(newProps));
    },

    shouldComponentUpdate: function (nextProps) {
        if (this.props.filters !== nextProps.filters) {
            return true;
        }

        if (this.props.open !== nextProps.open) {
            return true;
        }

        return false;
    },
    render: function () {
        if (!this.state.filters) {
            return (<div></div>);
        }

        var displayFilterSets = [];

        for (var key in this.state.filters.storedValue.scenario.filters) {
            var filterSet = this.state.filters.storedValue.scenario.filters[key];

            displayFilterSets.push(<DisplayFilterSet key={key} filters={filterSet}/>);
        }

        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onTouchTap={this._closeAdvancedFiltersDisplay}
            />,
            <FlatButton
                label="Edit Advanced Filters"
                primary={true}
                onClick={this._goToAdvancedReports}
            />,
        ];

        return (
            <div>
                <Dialog
                    title={this.state.filters.storedValue.name}
                    modal={false}
                    autoScrollBodyContent={true}
                    actions={actions}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    {displayFilterSets}
                </Dialog>
            </div>
        )
    },
    _goToAdvancedReports: function () {
        browserHistory.push('/advanced-report');
    },
    _closeAdvancedFiltersDisplay: function () {
        FilterActions.closeLightbox();
    }
});

export default DisplayAdvancedFilters
