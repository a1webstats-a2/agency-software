import React         			from 'react';
import FiltersStore  			from '../stores/FiltersStore';
import NotificationsStore 		from '../stores/NotificationsStore';
import FilterActions 			from '../actions/FilterActions';
import Dashboard 				from './dashboard/Dashboard.react';
import Footer 					from './Footer.react';





function getCurrentState( props ) {

	var accountOK = FiltersStore.accountIsOK();

	return {
			
		organisationsDatatableWidth 	: 	FiltersStore.getOrganisationsDatatableWidth(),
		looseFilterSetDisplay 			: 	FiltersStore.getLooseFilterSetDisplay(),
		accountOK 						: 	accountOK,
		scenariosFilterObj 				: 	FiltersStore.getScenarioFiltersObj(),
		isApplicationResting 			: 	FiltersStore.isApplicationResting(),
		settings 						: 	FiltersStore.getAllSettings(),
		filters 						: 	FiltersStore.getFilters(),
		displayFancyFilters				: 	FiltersStore.showFancyFilters(),
		latestAddedFilters 				: 	FiltersStore.getLatestAddedFilters(),
		user 							: 	FiltersStore.getUser(),
		templates 						: 	FiltersStore.getExistingTemplates(),
		analyticsData 					: 	{

			analyticsData 			: 	FiltersStore.getAnalyticsData(),
			firstLoadComplete 		: 	FiltersStore.isFirstLoadComplete( 'analytics' ),
			countryVisitorStats 	: 	{

				open 	: 	FiltersStore.checkLightboxOpen( 'countryVisitorStats', 1 ),
				stats 	: 	FiltersStore.getCountryVisitorStats()
			}
		},
		snackbarSettings  		:	FiltersStore.getSnackbarSettings(),
		quickReportDateRange 	: 	FiltersStore.getQuickReportDateRange(),
		quickReportTrafficType 	: 	FiltersStore.getQuickReportTrafficType(),
		quickReportDataType 	: 	FiltersStore.getQuickReportDataType(),
		notificationsData 		: 	{

			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),
			accountOK 				: 	accountOK,
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()
		}
	};
}

var ReportBuilderApp = React.createClass({

	getInitialState: function() {
		
		return getCurrentState();
	},

	componentDidMount: function() {

		NotificationsStore.addChangeListener( this._onChange );
		FiltersStore.addChangeListener( this._onChange );
	},

	componentWillUnmount: function() {
	
		NotificationsStore.removeChangeListener( this._onChange );
		FiltersStore.removeChangeListener( this._onChange );
	},

	render 	: function() {

		return (

			<div className="reportBuilder">

				<Dashboard 
					isApplicationResting={this.state.isApplicationResting} 
					analyticsData={this.state.analyticsData} 
					user={this.state.user} 
					templates={this.state.templates}
					otherData={this.state.analyticsData}
					accountIsOK={this.state.accountOK}
					settings={this.state.settings}
					filters={this.state.filters}
					displayFancyFilters={this.state.displayFancyFilters}
					latestAddedFilters={this.state.latestAddedFilters}
					snackbarSettings={this.state.snackbarSettings}
					quickReportDateRange={this.state.quickReportDateRange}
					quickReportTrafficType={this.state.quickReportTrafficType}
					quickReportDataType={this.state.quickReportDataType}
					notificationsData={this.state.notificationsData}
					organisationsDatatableWidth={this.state.organisationsDatatableWidth}
				/>

				<Footer 
					looseFilterSetDisplay={this.state.looseFilterSetDisplay}				
					scenariosFilters={this.state.scenariosFilterObj}
 					isApplicationResting={this.state.isApplicationResting}
					settings={this.state.settings}
					filters={this.state.filters}
					display={this.state.displayFancyFilters}
					latestAddedFilters={this.state.latestAddedFilters}
				/>
			</div>
		);
	},
	
	_onChange : function() {

		var newState = getCurrentState();

		this.setState( newState );
	}
});


export default ReportBuilderApp;