<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = null )
    {       

        if( !$request->session()->get( 'userId' ) || $request->session()->get( 'userId' ) === 0 ) {

        //if (Auth::guard($guard)->guest()) {
          
            if ( $request->ajax() ) {

                return response('Unauthorized.', 401);

            } else {


                return redirect()->guest( '/login' );
            }
        }

        return $next( $request );
    }
}
