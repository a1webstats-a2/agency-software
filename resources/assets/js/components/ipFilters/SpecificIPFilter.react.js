import React from 'react';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

const styles = {

    block: {
        maxWidth: 250,
    },
    toggle: {
        marginBottom: 16,
    },
};

import FilterActions from '../../actions/FilterActions';


var SpecificIPFilter    =  	React.createClass({


    getInitialState :   function() {

        return {

            myKey           :   this.props.myKey,
            storedValue     :   this.props.storedValue,
            criteria        :   'include',
            mySetIp         :   '1.0.0.0',
            exclude         :   false
        };

    },
   
    setType :   function() {

        var exclude = false;

        if( !this.state.exclude ) {

            exclude = true;
        }

        var newStoredValue = {

            exclude  :  true,
            value    :  this.state.mySetIp,
            criteria :  exclude
        }

        this.setState({

            criteria :  exclude,
            exclude  :  true
        })

        this._updateFilter( newStoredValue );
    },

	render 	: 	function() {

		return(

			<div className="dateFromBox form-group">
                <label className="control-label">Specific IP:</label>
                <div className="row">
                    <div className="col-md-10 overflowHidden">
                        <TextField
                            name="specificIP"
                            id="specificIP"
                            hintText="Specific IP Address"
                            underlineFocusStyle={{borderColor: '#4F7C8C' }} onChange={this._setIPValue} />
                        <Toggle
                            label="Set to exclude"
                            style={styles.toggle} 
                            toggled={this.state.exclude}
                            onToggle={this.setType} />
                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>
            </div>

		);	
	},

    _setIPValue  :   function( event ) {

        var newStoredValue = {

            value       :   event.target.value,
            criteria    :   this.state.exclude
        }

        this.setState( {

            mySetIp     :   event.target.value
        })

        this._updateFilter( newStoredValue );
    },

    _updateFilter   :   function( newStoredValue ) {

        FilterActions.editFilter({

            id              :   this.state.myKey,
            storedValue     :   newStoredValue,
            type            :   'specificIP'
        });
    },

	_removeFilter   :   function( e ) {

        FilterActions.removeFilter( this.state.myKey );
    }
});

export default  SpecificIPFilter;