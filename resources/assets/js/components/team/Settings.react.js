import React from 'react';
import NavBar from '../navigation/NavBar.react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import UserSettings from './UserSettings.react';
import GeneralSettings from './GeneralSettings.react';
import NotificationsStore from '../../stores/NotificationsStore';
import Dashboard from './Dashboard.react';
import UserSettingsIcon from 'material-ui/svg-icons/social/person';
import PaymentIcon from 'material-ui/svg-icons/action/credit-card';
import {Tabs, Tab} from 'material-ui/Tabs';
import DeveloperIcon from 'material-ui/svg-icons/action/code';
import GeneralSettingsIcon from 'material-ui/svg-icons/action/settings';
import PeopleIcon from 'material-ui/svg-icons/social/people';
import BillingSettings from './BillingSettings.react';
import Snackbar from '../Snackbar.react';
import UserTips from '../UserTips.react';
import UserTipHelpLink from '../UserTipHelpLink.react';
import UserTipsStore from '../../stores/UserTipsStore';
import TrackerCode from './TrackerCode.react';

function getCurrentSettings() {
  const settings = FiltersStore.getAllSettings();
  const snackbarSettings = FiltersStore.getSnackbarSettings();

  return {
    clientId: FiltersStore.getClientId(),
    trackerCode: FiltersStore.getTrackerCode(),
    newTeamMember: FiltersStore.getNewTeamMember(),
    settings: settings,
    paymentDisabled: true,
    crypt: FiltersStore.getCrypt(),
    user: FiltersStore.getUser(),
    snackbarSettings: snackbarSettings,
    paymentType: FiltersStore.getPaymentType(),
    teamMembers: FiltersStore.getTeamMembers(),
    newTeamMemberOpen: FiltersStore.checkLightboxOpen('newTeamMember', 1),
    notificationsData: {
      dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
      accountOK: FiltersStore.accountIsOK(),
      snackbarSettings: snackbarSettings,
      isNewNotifications: NotificationsStore.isNewNotifications(),
      notificationsOpen: NotificationsStore.isNotificationsOpen(),
      numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
      user: FiltersStore.getUser(),
      settings: FiltersStore.getAllSettings(),
      notifications: NotificationsStore.returnNotifications(),
      notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
      isOpen: NotificationsStore.isNotificationsOpen(),
      newFilters: FiltersStore.haveNewFiltersBeenApplied(),
      paginationData: FiltersStore.getPaginationTotals(),
      allFilters: FiltersStore.getFilters(),
      isApplicationResting: FiltersStore.isApplicationResting(),
      ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
      organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
      display: NotificationsStore.getDisplay(),
      viewNotificationID: NotificationsStore.viewNotificationID()
    }
  };
}

var Settings = React.createClass({
  getInitialState: function () {
    return getCurrentSettings();
  },
  componentWillReceieveProps: function (newProps) {
    this.setState(getCurrentSettings());
  },
  componentDidMount: function () {
    UserTipsStore.addChangeListener(this._onChange);
    FiltersStore.addChangeListener(this._onChange);
    FilterActions.loadTrackerCode();
  },
  componentWillUnmount: function () {
    UserTipsStore.removeChangeListener(this._onChange);
    FiltersStore.removeChangeListener(this._onChange);
  },
  render: function () {
    let tabs = [
      <Tab key={1} label="General" icon={<GeneralSettingsIcon/>}>

        <br/><br/>

        <GeneralSettings settings={this.state.settings.team}/>

      </Tab>,

      <Tab key={2} label="User" icon={<UserSettingsIcon/>}>

        <br/><br/>

        <UserSettings settings={this.state.settings.user}/>

      </Tab>,

      <Tab key={3} label="Team" icon={<PeopleIcon/>}>

        <br/><br/>

        <Dashboard
            user={this.state.user}
            newTeamMemberOpen={this.state.newTeamMemberOpen}
            teamMembers={this.state.teamMembers}
            newTeamMember={this.state.newTeamMember}
        />
      </Tab>

    ];

    let showBillingTabs = [-1, 12];

    if (showBillingTabs.indexOf(this.state.settings.agentConfig.id) > -1) {

      tabs.push(
          <Tab key={4} label="Billing" icon={<PaymentIcon/>}>

            <br/><br/>
            <BillingSettings settings={this.state.settings.team}/>

          </Tab>
      );
    }

    tabs.push(
        <Tab key={5} label="Tracker Code" icon={<DeveloperIcon/>}>

          <br/><br/>

          <TrackerCode
              clientId={this.state.clientId}
              trackerCode={this.state.trackerCode}
          />
        </Tab>
    )

    return (

        <div className="reportBuilder">

          <NavBar data={this.state.notificationsData}/>

          <div className="clr"></div>

          <div className="container mainContainer">

            <div className="row">

              <div className="col-md-12" id="results">

                <div className="row">

                  <div className="col-md-2">

                    <h3>Settings</h3>
                  </div>

                  <div className="col-md-10">

                    <div style={{marginTop: 27}}>
                      <UserTipHelpLink/>
                    </div>
                  </div>
                </div>

                <div className="clr"></div>

                <Tabs>


                  {tabs}
                </Tabs>
              </div>
            </div>
          </div>

          <Snackbar snackbarSettings={this.state.snackbarSettings}/>
          <UserTips/>
        </div>
    );
  },

  _getCrypt: function (event) {

    FilterActions.setCrypt(event.target.value);
  },

  _onChange: function () {

    this.setState(getCurrentSettings);
  }


});

export default Settings;