import React 				from  'react';
import { Link, IndexLink } 	from  'react-router'
import FilterActions 		from  '../../../actions/FilterActions';
 
function getCurrentState( props ) {

	return {

		row 		: 	props.rowIndex,
		data 		: 	props.data.data,
		indexMap 	: 	props.data._indexMap
	}
}

var NewOrganisationFilterLink = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		let rowNumber 	=	this.state.row;

		if( typeof this.state.indexMap !== "undefined" ) {

			rowNumber = this.state.indexMap[ this.state.row ];
		}

		const organisationName 	= 	this.state.data[rowNumber][1];

		return (

			<div style={{ marginTop : 10 }}>
				<a href="#" onClick={this._createFilter}>{organisationName}</a>
			</div>
		)
	},

	_createFilter : function( event ) {

		event.preventDefault();

		let rowNumber 	=	this.state.row;

		if( typeof this.state.indexMap !== "undefined" ) {

			rowNumber 	= 	this.state.indexMap[ this.state.row ];
		}

		const org 		= 	this.state.data[rowNumber];

		var newState  	= 	{

   			type          	:   'organisationFilter', 
   			storedValue 	:   {

				id 			: 	parseInt( org[0] ),
				include 	: 	'include',
				name 		: 	org[1],
				value 		: 	parseInt( org[0] )
			},
			id 				: 	parseInt( org[0] )
   		}

   		FilterActions.quickDashboardLink( newState );
	}
});

export default NewOrganisationFilterLink;