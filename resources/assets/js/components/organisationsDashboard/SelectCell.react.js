import React 				from 'react';
import moment 				from 'moment';
import FilterActions 		from '../../actions/FilterActions';
import FiltersStore 		from '../../stores/FiltersStore';
import Checkbox 			from 'material-ui/Checkbox';

function getCurrentState( props ) {

	return {	

		type 		: 	props.type,
		col 		: 	props.col,
		row 		: 	props.rowIndex,
		data 		: 	props.data,
		selection 	: 	props.selection
	}
}

const iconStyle = {

	height : 15, width : 15, color : '#170550'
}

var SelectCell = React.createClass({

	getInitialState : function() {

		let currentState 		= getCurrentState( this.props );

		return currentState;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		const orgData 	= this.state.data.getObjectAt( this.state.row );

		let exportMe 	= false;

		if( typeof this.state.selection[parseInt( orgData.organisation_id )] !== "undefined" && 
			this.state.selection[parseInt( orgData.organisation_id )] !== -1 ) {

			exportMe = true;

		} else if( Array.isArray( this.state.selection ) ) {

			if( this.state.selection.indexOf( parseInt( orgData.organisation_id ) ) > -1 ) {

				exportMe = true;
			}
		}

		return (

			<div style={{ marginTop : 7 }}>
				<Checkbox 
					onCheck={this._addToExport}
					checked={exportMe} />
			</div>
		);
	},

	_addToExport : function() {

		const orgData 	= this.state.data.getObjectAt( this.state.row );

		switch( this.state.type ) {

			case "select" :

				FilterActions.addOrRemoveQuickExportSelection({

		            id      :   parseInt( orgData.organisation_id ),
		            display :   orgData.name
		        });

				break;

			case "exclude" :

				FilterActions.addToExcludeSelection({

		            id      :   parseInt( orgData.organisation_id ),
		            name    :   orgData.name
		        });

				break;

			case "track" 	:

				var sendObj = {

		            organisation_id           :   parseInt( orgData.organisation_id ),
		            use_organisation_tel      :   orgData.tel,
		            use_organisation_website  :   orgData.website,
		            organisationid            :   parseInt( orgData.organisation_id ),
		            use_organisation_name     :   orgData.name,
		            key                       :   -1,
		            assigned_user             :   parseInt( orgData.assigned_user )
		        };

		        FilterActions.setTrackedOrganisationData( sendObj );

				break;
		}
	}
});

export default SelectCell;
