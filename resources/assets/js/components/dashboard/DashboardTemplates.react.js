import React from 'react';
import FilterActions from '../../actions/FilterActions';
import Paper from 'material-ui/Paper';
import Toggle from 'material-ui/Toggle';
import LoadFiltersIcon from 'material-ui/svg-icons/action/restore-page';

const styles = {
    paper: {
        padding: 20,
        height: 600
    }
}

function getCurrentState(props) {
    return {
        templates: props.templates
    }
}

const DashboardTemplates = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        let update = false;

        if (nextProps.templates.length > 0) {
            if (this.props.templates.length === 0) {
                update = true;
            } else if (this.props.templates[0][0] !== nextProps.templates[0][0]) {
                if (this.props.templates.length !== nextProps.templates.length) {
                    update = true;
                } else {
                    this.props.templates.forEach(function (template, i) {
                        if (template.id !== nextProps.templates[i].id) {
                            update = true;
                        }

                        if (template.display_in_dashboard !== nextProps.templates[i].display_in_dashboard) {
                            update = true;
                        }
                    })
                }
            }
        }

        return update;
    },
    render: function () {
        const templates = this.state.templates.map(function (template, i) {
            if (template.end_user_company_id != -1 &&
                !template.display_in_dashboard) {

                return false;
            }

            var automatedToUser = (template.blocked) ? false : true;
            var automatedText = 'None';
            var automated = false;

            switch (template.repeat) {
                case 1:
                    automated = true;
                    automatedText = 'Hourly';

                    break;
                case 2:
                    automated = true;
                    automatedText = 'Daily';

                    break;
                case 3:
                    automated = true;
                    automatedText = 'Weekly';

                    break;
            }

            if (automated) {
                automatedText = (
                    <div>
                        <div className="left">
                            {automatedText}
                        </div>
                        <div className="left">
                            <Toggle
                                defaultToggled={automatedToUser}
                                onToggle={(event) => this._setBlocked(i, event)}
                            />
                        </div>
                    </div>
                )
            }

            return (
                <tr key={i}>
                    <td className="py-2">{template.template_name}</td>
                    <td className="py-2">{automatedText}</td>
                    <td className="py-2 cursor-pointer">
                        <LoadFiltersIcon
                            onClick={(event) => this._loadFilters(template.id, event)}
                            style={{
                                height: 20,
                                width: 20,
                                color: '#9370DB',
                            }}
                        />
                    </td>
                </tr>
            )
        }.bind(this));

        return (
            <div>
                <Paper style={styles.paper} zDepth={4}>
                    <div className="blueHeader">
                        <h4>Your Templates</h4>
                    </div>
                    <br/>
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Automation</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {templates}
                        </tbody>
                    </table>
                </Paper>
            </div>
        )
    },
    _setBlocked: function (id) {
        FilterActions.setAutomatedReportBlocked(id);
    },
    _loadFilters: function (id) {
        FilterActions.setTemplateFilters({templateId: id});
    }
});

export default DashboardTemplates;
