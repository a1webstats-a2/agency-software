import React from 'react';
import NavBar from '../navigation/NavBar.react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import NotificationsStore from '../../stores/NotificationsStore';
import NewSubscription from './NewSubscription.react';
import CurrentSubscriptionDetails from './SubscriptionDetails.react';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState() {
    return {
        pricing: FiltersStore.getPricing(),
        paymentType: FiltersStore.getPaymentType(),
        settings: FiltersStore.getAllSettings(),
        user: FiltersStore.getUser(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        }
    }
}

const Gocardless = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    componentDidMount: function () {
        FilterActions.loadPrices();
        NotificationsStore.addChangeListener(this._onChange);
        FiltersStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        NotificationsStore.removeChangeListener(this._onChange);
        FiltersStore.removeChangeListener(this._onChange);
    },
    render: function () {
        let displayDiv;

        if (this.state.settings.team.using_gocardless) {
            displayDiv = (
                <CurrentSubscriptionDetails
                    subscribedUntil={this.state.settings.team.subscribed_until}
                    vendor="GoCardless"
                    user={this.state.settings.user}
                />
            );
        } else {
            displayDiv = (
                <NewSubscription
                    currency={this.state.pricing.currency}
                    vatApplicable={this.state.pricing.vatApplicable}
                    monthlyFee={this.state.pricing.monthlyDisplay}
                    yearlyFee={this.state.pricing.yearlyDisplay}
                    crossoutFee={this.state.pricing.crossoutDisplay}
                    user={this.state.settings.user}
                    paymentType={this.state.paymentType}
                    vendor="GoCardless"
                    sixMonthlyFee={this.state.pricing.sixMonthsDisplay}
                    sixMonthCrossoutDisplay={this.state.pricing.sixMonthCrossoutDisplay}
                />
            );
        }

        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>

                <div className="clr"></div>

                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-12" id="results">
                            <h3>Create Subscription with GoCardless</h3>

                            <div className="clr"></div>
                            <br/>

                            <div className="stepText flex space-x-6">
                                <span>Step</span><img src="/images/three.png"/><span>of 3</span>
                            </div>

                            <div className="clr"></div>
                            <br/><br/>

                            <div>
                                <p>Based on visitor levels during your trial to date your pricing will be
                                    £{this.state.pricing.monthlyDisplay} per month (paid month to month and <span
                                        className="standOutText">you can cancel at any time</span>).</p>
                                <p>If you expect to use the system ongoing then you can gain a 20% discount by prepaying
                                    for 12 months.</p>
                                <p>If you have any questions at all about your pricing level please do reach out to.</p>

                            </div>
                            <div className="clr"></div>
                            <br/>
                            {displayDiv}
                            <div className="clr"></div>
                            <br/><br/>
                            <RaisedButton label="Create Subscription" onClick={this._makePayment} secondary={true}
                                          disabled={this.state.paymentDisabled}/>
                            <div className="clr"></div>
                            <br/><br/><br/><br/><br/>
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    _makePayment: function () {
        FilterActions.startGoCardlessProcess();
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }
});

export default Gocardless;
