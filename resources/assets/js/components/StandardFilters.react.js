import React 				from 'react';
import AddDateFilter 		from './AddDateFilter.react';
import AddIPFilter   		from './ipFilters/AddIPFilter.react';
import AddKeywordFilter		from './AddKeywordFilter.react';
import DisplayByFilter 		from './DisplayByFilter.react';
import FilterStore 			from '../stores/FiltersStore';

var StandardFilters = React.createClass({

	getInitialState : function() {

		return {

		}

	},	

	render 	: 	function() {


		return(

			<div className="standardFilters">
				<div className="row">
					<div className="col-md-12">
						<h3>Apply Standard Filters</h3><br />

					</div>
				</div>

				<div className="row">
				
					

					<div className="col-md-4">

						<AddIPFilter />
					</div>

					<div className="col-md-4">

						<AddKeywordFilter />
					</div>

					<div className="col-md-4">

						<DisplayByFilter />
					</div>


				</div>
			</div>
		)
	}
});

export default  StandardFilters;