import React 			from 'react';
import Dialog 			from 'material-ui/Dialog';
import FlatButton 		from 'material-ui/FlatButton';
import A1Chip  			from './A1Chip.react';
import A1ToggleChip  	from './A1ToggleChip.react';
import A1ScenarioChip 	from './A1ScenarioChip.react';
import FiltersStore 	from '../stores/FiltersStore';
import FilterActions 	from '../actions/FilterActions';

function getCurrentState( props ) {

	return {

		type 				: 	props.type,
		latestAddedFilters 	: 	props.latestAddedFilters,
		open 				: 	props.open,
		filters 			: 	props.filters
	}
}

var DisplayLooseFilterSet = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		var filters 				= this.state.filters;
		var containsScenarioFilters = false;
		var displayFilters 			= [];
		var visitorTypeFilters 		= {

			1 	: 	{

				storedValue 	: 	{

					id 			: 	1,
					name 		: 	1,
					value 		: 	1,
					textValue 	: 	'Organisations'
				},
				type 			: 	'visitorTypeFilter',
				round 			: 	0

			},

		 	2 	: 	{
		
				storedValue 	: 	{

					id 			: 	2,
					name 		: 	2,
					value 		: 	2,
					textValue 	: 	'Education'
				},
				type 			: 	'visitorTypeFilter',
				round 			: 	0

			},

			6 	: 	{

				storedValue 	: 	{

					id 			: 	6,
					name 		: 	6,
					value 		: 	6,
					textValue 	: 	'Unknown'
				},
				type 			: 	'visitorTypeFilter',
				round 			: 	0
			}
		}

		if( this.state.type.toLowerCase() === "visitortypefilter" ) {

			for( var visitorTypeKey in visitorTypeFilters ) {

				var filter = visitorTypeFilters[visitorTypeKey];

				displayFilters.push( 

					<A1ToggleChip 
						latestAddedFilters={this.state.latestAddedFilters} 
						key={visitorTypeKey} 
						toggled={FiltersStore.checkIfFilterTypeAndIndexExists( 'visitorTypeFilter', filter.storedValue.id )}
						avatar='socialIcon' 
						filterVal={filter.storedValue.id}
						type='Visitor Type' 
						text={filter.storedValue.textValue} 
						filter={filter} />
				);
			}
		}

		for ( var key in filters ) {

			if( !filters[key] ) {

				continue;
			}

			var filter = filters[key]

			switch( filters[key].type ) {

				case "ppc" :
					
					var text = filters[key].storedValue.textValue;

					displayFilters.push(

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='ppc' type='PPC' text={text} filter={filters[key]} />

					);

					break;

				case "organic" :

					var text = filters[key].storedValue.textValue;

					displayFilters.push(

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='organic' type='Organic' text={text} filter={filters[key]} />

					);

					break;

				case "trafficType" :

					displayFilters.push(

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='traffic' type='Traffic Type' text={filters[key].storedValue.textValue} filter={filters[key]} />
					);

					break;

				case "tagFilter" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='tag' type='Include Tag' text={filters[key].storedValue.textValue} filter={filters[key]} />

					);

					break;

				case "CustomFilterKeyword" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='keyword' type='Keyword' text={filters[key].storedValue.textValue} filter={filters[key]} />

					);


					break;		

				case "CustomFilterURL" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='pageView' type='URL' text={filters[key].storedValue.textValue} filter={filters[key]} />
					);

					break;

				case "customFilterTotalVisits" :

					displayFilters.push( 
						
						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='numbered' type='Total Visits' text={filters[key].storedValue.value} filter={filters[key]} />

					);

					break;

				case "CustomFilterEntryPage" :

					displayFilters.push(

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='pageView' type='Entry Page' text={filters[key].storedValue.textValue} filter={filters[key]} />

					);

					break;

				case "CustomFilterLastPage" :

					displayFilters.push(

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='pageView' type='Final Page' text={filters[key].storedValue.textValue} filter={filters[key]} />
					);

					break;

				case "customFilterReferrerCountry" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='location' type='Country' text={filters[key].storedValue.textValue} filter={filters[key]} />

					);

					break;

				case "customFilterReferrer" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='referrerIcon' type='Referrer' text={filters[key].storedValue.textValue} filter={filters[key]} />
					);

					break;

				case "customFilterTimeOnSite" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='timeline' type='Time On URL' text={filters[key].storedValue.value} filter={filters[key]} />
					);

					break;

				case "organisationFilter" :

			        var stringArray  =  filters[key].storedValue.name.split( " " );

					var returnString = 	stringArray.map( function( word, i ) {

			            if( word.indexOf( "Ftip") === -1 ) {

			                return word;
			            
			            } else {

			                return "";
			            }
			        })

			        var organisationName    =   returnString.join( " " ).trim();

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='business' type='Organisation' text={organisationName} filter={filters[key]} />
					)

					break;

				case "scenarioFilter" :

					displayFilters.push( 

						<A1ScenarioChip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='business' type='Scenario' text={filters[key].storedValue.name} filter={filters[key]} />

					)

					break;

				case "deviceFilter" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='deviceIcon' type='Device' text={filters[key].storedValue.name} filter={filters[key]} />

					)

					break;

				case "browserFilter" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='deviceIcon' type='Browser' text={filters[key].storedValue.name} filter={filters[key]} />

					)

					break;	

				case "osFilter" :

					displayFilters.push( 

						<A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='deviceIcon' type='Operating System' text={filters[key].storedValue.name} filter={filters[key]} />

					)

					break;
			}
		}

		const actions = [
		
			<FlatButton
				label="Close"
				primary={true}
				onTouchTap={this._handleClose}
			/>,
			<FlatButton
				label="Update"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this._updateResults}
			/>
		];

		let open = this.state.open;

		if( displayFilters.length === 0 ) {

			open = false;
		}


		return (

			<div>
				<Dialog
		        	title="Filters"
		        	modal={false}
		        	autoScrollBodyContent={true}
		        	actions={actions}
		        	open={open}
		        	onRequestClose={this.handleClose}
		        >
		        	{displayFilters}
		        </Dialog>

			</div>
		)
	},

	_updateResults : function() {

		FilterActions.updateResults();
	},

	_handleClose : function() {

		FilterActions.closeLightbox();
	}
});

export default DisplayLooseFilterSet;