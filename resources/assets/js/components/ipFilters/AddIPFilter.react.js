import React from 'react';
var ReactPropTypes = React.PropTypes;

import FilterActions from '../../actions/FilterActions';

import classNames from 'classnames';

var AddIPFilter = React.createClass({

	getInitialState   :   function() {

		return  {

			value   :   'IPRange'
		};
	},

   shouldComponentUpdate : function( nextProps, nextState ) {

      return false;
   },


   /**
    * @return {object}
    */
   	render: function() {


   		return (

   			<div className="addDateFilter">
		   		<form className="criteriaBox"  onSubmit={this.handleSubmit}>
			   		<div className="form-group">
				   		<h4>IP Filters</h4>
				   		<select className="form-control" onChange={this._onChange} defaultValue="IPRange" value={this.state.value} >
				   			<option value="IPRange">IP Range</option>
				     		   <option value="specificIP">Specific IP</option>
				   		</select>
			   		</div>

			   		<button className="btn btn-success" onClick={this._save}>Add Filter</button>

		   		</form>
		   	</div>
   		);
   	},

   	_onChange  :   function( event ) {

   		this.setState( {

   			value   :   event.target.value
   		})
   	},

   	_save   :   function( event ) {

   		event.preventDefault();

   		var newState  = {

   			type          :   this.state.value, 
   			storedValue   :   event.target.value,
   			id            :   null

   		}

   		FilterActions.createFilter( newState );
   	}
});

export default  AddIPFilter;