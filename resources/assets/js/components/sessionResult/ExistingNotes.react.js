import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import ExistingNote from './ExistingNote.react';

function getCurrentState( props ) {

	return {

		notes : props.notes
	}
}


var ExistingNotes = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( newProps, newState ) {

		if( this.props.notes === newProps.notes ) {

			return false;
		}

		return true;
	},

	render : function() {

		var notes = this.state.notes.map( function( note, i ) {

			return <ExistingNote note={note} myKey={i} key={i} />;
		})

		if( notes.length === 0 ) {

			notes = "There are no notes for this organisation";
		
		} else {

			notes = (

				<table className="table">
    				<thead>
	        			<tr>
	        				<th></th>
	        				<th>Author</th>
	        				<th>Date</th>
	        				<th>Subject</th>
	        				<th>Actions</th>
	        			</tr>

	        		</thead>
    				<tbody>
    					{notes}
    				</tbody>
    			</table>
			)
		}
	
		return (

			<div className="existingNotes">
				<br />
				{notes}

			</div>
		)
	}

});

export default  ExistingNotes;