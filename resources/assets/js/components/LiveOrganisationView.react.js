import React 	from 'react';
import Pusher   from 'pusher-js';

class LiveOrganisationView extends React.Component {

	constructor( props ) {

		super( props );

		this.state = {

			orgs 	: 	[]
		}
	}

    componentDidMount() {

        var pusher = new Pusher( 'd8ef5a0ae7a920f0b203', {
            
            cluster         : 'eu',
            encrypted       : false
        });

        var channel = pusher.subscribe( 'a1' );


        let thisObj = this;

        channel.bind( 'live-organisation-view' , function( data ) {
        
        	let orgs 	= thisObj.state.orgs;
            orgs.unshift( data );
        	thisObj.setState({ orgs : orgs });

        });

    }

	render() {

		
		let displayLogs = this.state.orgs.map( function( org, l ) {

			return (

				<p>{org.name} | {org.hostname} | {org.organisation_type_id}</p>
			);
			
		}.bind( this ));
		

		return (

			<div className="reportBuilder" style={{ background : '#000', color : '#fff', fontSize : 16, minHeight : 1000 }}>

				<div className="clr"></div>

				<div className="container mainContainer" style={{ marginTop : 0 }}>
					
					<div className="row">

						<div className="col-md-12" id="results">
							
							<h2>Organisations Live View</h2>

							<div className="clr"></div><br /><br />

							{displayLogs}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default LiveOrganisationView;