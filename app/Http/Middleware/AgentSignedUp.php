<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class AgentSignedUp
{
    public function handle(Request $request, Closure $next)
    {
        if ($_SERVER['REQUEST_URI'] === '/installation') {
            return $next($request);
        }

        try {
            $configContents = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/../a1config.json');

            if ('' === $configContents) {
                return redirect('installation');
            }

            return $next($request);
        } catch (\Exception) {
            return redirect('installation');
        }
    }
}
