import React 				from 'react';
import VisitorTypeFilter 	from './VisitorTypeFilter.react';
import FilterActions 		from '../../actions/FilterActions';
import FiltersStore 		from '../../stores/FiltersStore';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {

  	checkbox: {
    	marginBottom: 16
  	},

  	raisedButton : {

  		marginRight 	: 	12,
  		marginTop 		: 	0,
  		marginBottom 	: 	40
  	}
};

function getCurrentState() {

	return {

		isChecked : FiltersStore.getAllVisitorTypesSelected()
	}
}

var VisitorTypeFilters = React.createClass({

	getInitialState : function() {

		return getCurrentState();
		
	},

	componentWillReceiveProps : function() {

		this.setState( getCurrentState() );
	},

	render : function() {

		var types = [

			<VisitorTypeFilter myKey="1" key="1" visitorType="Organisations" />,
			<VisitorTypeFilter myKey="3" key="3" visitorType="ISPs" />,
			<VisitorTypeFilter myKey="2" key="2" visitorType="Education" />,
			<VisitorTypeFilter myKey="4" key="4" visitorType="Crawl Bot" />,
			<VisitorTypeFilter myKey="5" key="5" visitorType="Public" />,
			<VisitorTypeFilter myKey="6" key="6" visitorType="Uncategorised" />

		];

		return(

			<div className="visitorTypeFilters">
				<div className="row">
					<div className="col-md-12">
						<br /><h3>Visitor Type Filters</h3><br />

					</div>
				</div>


				<div className="row">

					<div className="col-md-12">

						<RaisedButton label="Select All" onClick={this._selectAll} style={styles.raisedButton} primary={true} />

						<RaisedButton label="Deselect All" onClick={this._deselectAll} style={styles.raisedButton} secondary={true} />

						
					</div>
				</div>

				<div className="row">
					{types}
				</div>
			</div>
		);
	},

	_selectAll : function() {

		var filterNames = [

			'Organisations',
			'Education',
			'ISPs',
			'Crawl Bots',
			'Public',
			'Unknown'

		]

		var createFilters = [];

		for( var i = 0; i <= 6; i++ ) {

			createFilters.push({

				type          	:   "visitorTypeFilter", 
	   			storedValue 	:   {

					id 			: 	i,
					name 		: 	i,
					value 		: 	i,
					textValue 	: 	filterNames[i]
				},

				id 	: 	i
			});
		}

		FilterActions.createFilters( createFilters );

	},

	_deselectAll : function() {

		var removeFilters = [];

		for( var i = 0; i <= 6; i++ ) {

			removeFilters.push({

   				type 	: 	"visitorTypeFilter",
   				id 		: 	i
   			});
		}

		FilterActions.removeFiltersByType( removeFilters );
	}
})

export default  VisitorTypeFilters;