import React from 'react';
import NavBar from "./navigation/NavBar.react";
import UserTipHelpLink from "./UserTipHelpLink.react";
import SnackbarA1 from "./Snackbar.react";
import UserTips from "./UserTips.react";
import FiltersStore from "../stores/FiltersStore";
import NotificationsStore from "../stores/NotificationsStore";

function getCurrentSettings() {
    var settings = FiltersStore.getAllSettings();

    return {
        zapierTemplates: [],
        thirdPartyKeysUpdating: FiltersStore.getUpdateBox('thirdPartyKeys'),
        salesforceID: FiltersStore.getSalesforceID(),
        settings: settings,
        snackbarSettings: FiltersStore.getSnackbarSettings(),
        user: FiltersStore.getUser(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        }
    };
}

const Zapier = React.createClass({
    getInitialState: function () {
        return getCurrentSettings();
    },
    componentWillReceieveProps: function () {
        this.setState(getCurrentSettings());
    },
    componentDidMount: function () {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.zapier.com/v1/zap-templates?limit=10&client_id=VinsiUMS9FUgsFJ6RPtNp4H9i93Lbl5XtoxVLckA');
        xhr.send();

        const self = this;

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    self.setState({
                        zapierTemplates: JSON.parse(xhr.response)
                    });
                }
            }
        }
    },
    componentWillUnmount: function () {
        FiltersStore.removeChangeListener(this._onChange);
    },
    render() {
        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-12" id="results">
                            <div className="row">
                                <div className="col-md-2">
                                    <h3>Zapier Templates</h3>
                                </div>
                                <div className="col-md-10">
                                    <div style={{marginTop: 30}}>
                                        <UserTipHelpLink/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>
                <UserTips/>
                <div className="container">
                    <UserTipHelpLink/>
                    <div className="row">
                        <div className="col-md-12">
                            <table className="table">
                                <thead>
                                <tr>
                                    <th style={{paddingRight: 30}}>App</th>
                                    <th>Description</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    {this.state.zapierTemplates.map(( template, index ) => {
                                        return (
                                            <tr key={index}>
                                                <td>
                                                    <a target="_blank" href={ template.url }>
                                                        <img src={ template.steps[1].images.url_32x32 } style={{ marginRight: 30 }} />
                                                    </a>
                                                    <a target="_blank" href={ template.url }>{ template.steps[1].title }</a>
                                                </td>
                                                <td>{ template.title }</td>
                                                <td><a target="_blank" href={ template.url }>Use Integration</a></td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    _onChange: function () {
        this.setState(getCurrentSettings);
    }
});

export default Zapier;
