import React from 'react';
import ClientTypes from '../prospecting/ClientTypes.react';
import Device from './Device.react';
import PageVisits from './PageVisits.react';
import RawData from './RawData.react';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

function getCurrentState(props) {
    return {
        settings: props.settings,
        heatmapData: props.heatmapData,
        clientID: props.clientID,
        organisationName: props.organisationName,
        pageVisits: props.pageVisits,
        resultKey: props.resultKey,
        data: props.data,
        locationString: props.locationString,
        lastVisit: props.lastVisit,
        longitude: props.longitude,
        latitude: props.latitude,
        tags: props.tags,
        relationships: props.relationships,
        myKey: props.myKey
    }
}

const ResultTabs = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.data.id !== nextProps.data.id) {
            return true;
        }

        if (this.props.heatmapData !== nextProps.heatmapData) {
            return true;
        }

        if (this.props.clientID !== nextProps.clientID) {
            return true;
        }

        if (this.props.longitude !== nextProps.longitude) {
            return true;
        }

        if (this.props.locationString !== nextProps.locationString) {
            return true;
        }

        if (this.props.tags !== nextProps.tags) {
            return true;
        }

        if (this.props.relationships !== nextProps.relationships) {
            return true;
        }

        return false;
    },
    render: function () {
        const numPages = this.state.pageVisits.length;

        let tabs = (
            <Tabs>
                <TabList>
                    <Tab>Pages ({numPages})</Tab>
                    <Tab>Tags</Tab>
                    <Tab>Device</Tab>
                    <Tab>IP</Tab>
                </TabList>
                <TabPanel>
                    <PageVisits
                        teamSettings={this.state.settings.team}
                        userSettings={this.state.settings.user}
                        heatmapData={this.props.heatmapData}
                        parentID={this.state.data.id}
                        clientID={this.state.clientID}
                        resultKey={this.state.resultKey}
                        browser={this.state.data.browser_parent_name}
                        pages={this.state.pageVisits}/>
                </TabPanel>
                <TabPanel>
                    <ClientTypes storeType="reportBuilder" tags={this.state.tags} key={this.state.myKey}
                                 myKey={this.state.myKey} organisationId={this.state.data.organisationid}
                                 organisationid={this.state.data.organisationid}
                                 relationships={this.state.relationships}/>
                </TabPanel>
                <TabPanel>
                    <Device data={this.state.data}/>
                </TabPanel>
                <TabPanel>
                    <RawData data={this.state.data}/>
                </TabPanel>
            </Tabs>
        )

        if (this.state.data.utm_campaign) {
            let campaign = (
                <table className="table">
                    <tbody>
                    <tr>
                        <td>utm_campaign</td>
                        <td>{this.state.data.utm_campaign}</td>
                    </tr>
                    <tr>
                        <td>utm_content</td>
                        <td>{this.state.data.utm_content}</td>
                    </tr>
                    <tr>
                        <td>utm_medium</td>
                        <td>{this.state.data.utm_medium}</td>
                    </tr>
                    <tr>
                        <td>utm_source</td>
                        <td>{this.state.data.utm_source}</td>
                    </tr>
                    <tr>
                        <td>utm_term</td>
                        <td>{this.state.data.utm_term}</td>
                    </tr>
                    </tbody>
                </table>
            )

            tabs = (
                <Tabs>
                    <TabList>
                        <Tab>Pages ({numPages})</Tab>
                        <Tab>Tags</Tab>
                        <Tab>Device</Tab>
                        <Tab>Campaign</Tab>
                        <Tab>IP</Tab>
                    </TabList>

                    <TabPanel>
                        <PageVisits
                            parentID={this.state.data.id}
                            resultKey={this.state.resultKey}
                            pages={this.state.pageVisits}
                        />
                    </TabPanel>

                    <TabPanel>
                        <ClientTypes
                            storeType="reportBuilder"
                            tags={this.state.tags}
                            key={this.state.myKey}
                            myKey={this.state.myKey}
                            organisationId={this.state.data.organisationid}
                            organisationid={this.state.data.organisationid}
                            relationships={this.state.relationships}
                        />
                    </TabPanel>
                    <TabPanel>
                        <Device data={this.state.data}/>
                    </TabPanel>

                    <TabPanel>
                        {campaign}
                    </TabPanel>
                    <TabPanel>
                        <RawData data={this.state.data}/>
                    </TabPanel>
                </Tabs>
            )
        }

        return (
            <div>
                {tabs}
            </div>
        )
    }
});

export default ResultTabs;
