import React 					from  	'react';
import moment 					from 	'moment';

function getCurrentState( props ) {

	return {	

		col 		: 	props.col,
		row 		: 	props.rowIndex,
		data 		: 	props.data
	}
}

var Device = React.createClass({

	getInitialState : function() {

		let currentState 		= getCurrentState( this.props );

		return currentState;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		let data 		= this.state.data.getObjectAt( this.state.row );

		let browserIcon = "";
		let osIcon 		= "";
		let deviceIcon 	= "";


		if( data.browser_flat !== "undefined" &&
			data.browser_flat ) {

			switch( data.browser_flat.toLowerCase() ) {

				case "firefox" :

					browserIcon = (

						<img src="/images/mozilla.png" />
					)

					break;

				case "chrome" :

					browserIcon = (

						<img src="/images/chrome.png" />
					)

					break;

				case "internet explorer" :

					browserIcon = (

						<img src="/images/internet-explorer.png" />
					)

					break;

				case "safari" :

					browserIcon = (

						<img src="/images/safari.png" />
					)

					break;
			}
		}

		if( data.os_flat !== "undefined" &&
			data.os_flat ) {

			switch( data.os_flat.toLowerCase() ) {

				case "win" :

					osIcon = (

						<img src="/images/windows.png" />
					)

					break;

				case "wow" :

					osIcon = (

						<img src="/images/windows.png" />
					)

					break;

				case "linux" :

					osIcon = (

						<img src="/images/android.png" />
					)

					break;

				case "macintosh" :

					osIcon = (

						<img src="/images/macos.png" />

					)

					break;

				
			}
		}


		if( data.device_flat !== "undefined" &&
			data.device_flat ) {

			switch( data.device_flat.toLowerCase() ) {

				case "mac" :

					deviceIcon = (

						<img src="/images/imac.png" />
					)


					break;

				case "desktop" :

					deviceIcon = (

						<img src="/images/desktop.png" />
					)

					break;

				case "mobile" :

					deviceIcon = (

						<img src="/images/smartphonesmall.png" />
					)

					break;

				case "tablet" :

					deviceIcon = (

						<img src="/images/tablet.png" />
					)

					break;
			}
		}


		return (

			<div style={{ marginTop : 14 }}>

				<div style={{ marginRight : 5, float : 'left' }}>{deviceIcon}</div>
				<div style={{ marginRight : 5, float : 'left' }}>{osIcon}</div>
				<div style={{ marginRight : 5, float : 'left' }}>{browserIcon}</div>


			</div>
		);
	}
});

export default Device;