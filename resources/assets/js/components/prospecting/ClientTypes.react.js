import React from 'react';
import TagType from './TagType.react';
import FilterAction from '../../actions/FilterActions';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState(props) {
    return {
        storeType: props.storeType,
        myKey: props.myKey,
        relationships: props.relationships,
        organisationId: props.organisationId,
        newTagName: '',
        tags: props.tags
    }
}

const ClientTypes = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.relationships !== nextProps.relationships) {
            return true;
        }

        if (this.props.organisationId !== nextProps.organisationId) {
            return true;
        }

        if (this.props.storeType !== nextProps.storeType) {
            return true;
        }

        return this.props.tags !== nextProps.tags;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    handleOpen: function () {
        FilterAction.setLightbox({
            type: 'createNewClientTag',
            id: 1
        })
    },
    render: function () {
        if (typeof this.state.tags === 'undefined' || 0 === this.state.tags.length) {
            return (<div>loading tags</div>);
        }

        let relationships = [];

        if (typeof this.state.relationships !== 'undefined') {
            if (typeof this.state.relationships[this.state.organisationId] !== "undefined") {
                if (this.state.relationships[this.state.organisationId]) {
                    relationships = this.state.relationships[this.state.organisationId];
                }
            }
        }

        this.state.tags.sort(function (a, b) {
            if (a.type.toLowerCase() < b.type.toLowerCase()) {
                return -1;
            } else if (a.type.toLowerCase() > b.type.toLowerCase()) {
                return 1;
            }

            return 0;
        })

        const tagTypes = this.state.tags.map((tag, i) => {
            const checked = relationships.indexOf(tag.id) > -1;

            return (
                <TagType
                    checked={checked}
                    storeType={this.state.storeType}
                    organisationId={this.state.organisationId}
                    relationships={this.state.relationships}
                    key={i}
                    tagId={tag.id}
                    tagType={tag.type}
                />
            );
        });

        return (
            <div className="clientTypes">
                <div className="grid grid-cols-2 gap-4">
                    {tagTypes}
                </div>

                <div className="clr"></div>
                <br/><br/>

                <div className="row">
                    <div className="col-md-12">
                        <RaisedButton
                            id="createNewClientButton"
                            secondary={true}
                            label="Create new tag"
                            onTouchTap={this.handleOpen}
                        />

                        <div className="clr"></div>
                        <br/>
                    </div>
                </div>
            </div>
        );
    }
});

export default ClientTypes;
