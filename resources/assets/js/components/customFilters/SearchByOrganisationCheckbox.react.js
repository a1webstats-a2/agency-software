import React from 'react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import Checkbox from 'material-ui/Checkbox';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import Visibility from 'material-ui/svg-icons/action/visibility';


const styles = {
 
  	checkbox: {
   	 	
   	 	marginBottom: 16,
  	}
};

function getCurrentState( props ) {

	return {	
			
		type 				: 	'organisationFilter',
		searchBox 			: 	props.searchByCheckbox,
		myKey 				: 	props.myKey,
		filtersBeingEdited 	: 	props.filtersBeingEdited,
		organisationData 	: 	props.organisationData,
		organisationId 		: 	props.organisationData.id,
		organisationName	: 	props.organisationData.use_organisation_name,
		includeType 		: 	props.include,
		isChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'organisationFilter',
			props.organisationData.id ),
		searchByData 		: 	props.searchByData,
		inclusionType 		: 	props.searchByData.organisationIncludeType
	}
}


var SearchByOrganisationCheckbox 	= 	React.createClass({

	getInitialState 	: 	function() {

		return getCurrentState( this.props );
	},

	
	componentWillReceiveProps 	: 	function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.searchByData.organisationIncludeType !== nextProps.searchByData.organisationIncludeType ) {

			return true;
		}

		if( this.props.filtersBeingEdited !== nextProps.filtersBeingEdited ) {

			return true;
		}

		if(	this.state.isChecked !== FiltersStore.checkIfFilterTypeAndIndexExists( this.state.type,
				this.props.organisationData.id ) ) {

			return true;
		}

		if( this.props.organisationData.use_organisation_name !== nextProps.organisationData.use_organisation_name ) {

			return true;
		}

		if( this.props.include !== nextProps.include ) {

			return true;
		}

		return false;

	},

	render : 	function() {

		var countField 		=	"include_count";

		switch( this.state.inclusionType.toLowerCase() ) {

			case "include" :

				countField = "include_count";

				break;

			case "exclude" :

				countField = "exclude_count";

				break;
		}


		if( this.state.organisationData[countField] === 0 ) {

			return ( <div></div> );
		}

		var resCountClass 	= 	( this.state.filtersBeingEdited ) ? 'greyedOutResultCount' : 'resultCount';

		var stringArray         =   this.state.organisationData.use_organisation_name.split( " " );

        var returnString = stringArray.map( function( word, i ) {

            if( word.indexOf( "ftip") === -1 ) {

                return word;
            
            } else {

                return "";
            }
        })

        var organisationName    =   returnString.join( " " ).trim();


		var label 		= 	(

			<div>

		    	{organisationName}

		    	<span className={resCountClass}>{this.state.organisationData[countField]} visitors</span>
			</div>
		)

		return(

				
	    	<Checkbox
		    	label={label}
		    	checked={this.state.isChecked}
		    	style={styles.checkbox}
		    	onCheck={this._saveCheckedState}
		    />
			    

		);
	},

	_saveCheckedState 	: 	function( event, checked ) {

		var newState  = {

   			type          	:   this.state.type, 
   			storedValue 	:   {

				include 	: 	this.state.includeType,
				id 			: 	this.state.organisationId,
				name 		: 	this.state.organisationName,
				searchBox 	: 	this.state.searchBox,
				value 		: 	this.state.organisationId
			},
			id 				: 	this.state.organisationId
   		}

   		var checked = event.target.checked;

   		this.setState({

   			isChecked : checked
   		})

   		if( checked ) {

   			FilterActions.createFilter( newState );

   		} else {

   			FilterActions.removeFilterByType({

   				type 	: 	this.state.type,
   				id 		: 	this.state.organisationId
   			});
   		}
	},
});

export default  SearchByOrganisationCheckbox;