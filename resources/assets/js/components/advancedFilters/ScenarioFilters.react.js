import React from 'react';
import FilterTypeSet from './displayFilters/FilterTypeSet.react';

function getCurrentState(props) {
    return {
        filters: props.filters,
        scenarioID: props.scenarioID
    }
}

function getJustStringValues(filtersObj) {
    var returnArray = [];

    for (var key in filtersObj) {
        var filter = filtersObj[key];
        returnArray.push(filter.storedValue.textValue);
    }

    return returnArray;
}

const ScenarioFilters = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        var filterTypeSets = [];

        var displayFilterSetTypeNames = {
            'counties': 'Counties',
            'devicetype': 'Device Type',
            'visitortypefilters': 'Visitor Types',
            'referrer': 'Referrers',
            'firstpagevisited': 'First Page Visited',
            'pagevisited': 'Page Visited',
            'lastpagevisited': 'Last Page Visited',
            'organisation': 'Organisations',
            'campaignname': 'Campaign Title',
            'campaignsource': 'Campaign Sources',
            'campaignmedium': 'Campaign Mediums',
            'keyword': 'Keywords',
            'ip': 'IP Search',
            'tags': 'Tags',
            'town': 'Town / City',
            'countries': 'Countries',
            'source': 'Source',
            'geolocation': 'Geolocation',
            'events': 'Events'
        }

        var filterI = 0;

        for (var filterSetKey in this.state.filters) {
            var filterSet = this.state.filters[filterSetKey];
            var andOr     = filterSet.andOr;

            if (filterI === 0) {
                andOr = false;
            }

            var dataSet = getJustStringValues(filterSet.filters);

            var anyThatMatch = false;

            if (typeof filterSet.anyThatMatch !== "undefined") {
                anyThatMatch = filterSet.anyThatMatch;
            }

            if (typeof filterSet.type === "undefined") {
                console.log("filter type not defined", filterSet);
            }

            var displayFilterSet = <FilterTypeSet
                setID={filterSet.setID}
                key={filterSetKey}
                andOr={andOr}
                include={filterSet.include}
                filterSetType={filterSet.type.toLowerCase()}
                displayTypeName={displayFilterSetTypeNames[filterSet.type.toLowerCase()]}
                types={dataSet}
                anyThatMatch={anyThatMatch}
                scenarioID={this.state.scenarioID}/>

            filterTypeSets.push(displayFilterSet);

            filterI++;
        }

        filterTypeSets.reverse();

        return (
            <div>
                {filterTypeSets}
            </div>
        )
    }
});

export default ScenarioFilters;
