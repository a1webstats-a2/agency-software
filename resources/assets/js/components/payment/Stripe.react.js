import React from 'react';
import NavBar from '../navigation/NavBar.react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import NotificationsStore from '../../stores/NotificationsStore';
import NewSubscription from './NewSubscription.react';
import RefreshIndicator from 'material-ui/RefreshIndicator';

function getCurrentState() {
    return {
        isApplicationResting: FiltersStore.isApplicationResting(),
        pricing: FiltersStore.getPricing(),
        paymentType: FiltersStore.getPaymentType(),
        settings: FiltersStore.getAllSettings(),
        user: FiltersStore.getUser(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        }
    }
}

const style = {
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    },
};


var Stripe = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },

    componentDidMount: function () {
        FilterActions.loadPrices();
        NotificationsStore.addChangeListener(this._onChange);
        FiltersStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        NotificationsStore.removeChangeListener(this._onChange);
        FiltersStore.removeChangeListener(this._onChange);
    },

    shouldComponentUpdate: function () {
        return true;
    },

    render: function () {
        let loadingDiv = (
            <div></div>
        )

        let displayDiv = "";

        if (!this.state.notificationsData.isApplicationResting) {
            displayDiv = (
                <div style={style.container}>
                    <p>Please wait, the system is processing.</p>

                    <RefreshIndicator
                        size={40}
                        left={10}
                        top={0}
                        status="loading"
                        style={style.refresh}
                    />

                    <div className="clr"></div>
                    <br/><br/>
                </div>
            )

        } else {
            displayDiv = (
                <div>
                    <div className="stepText">
                        <p>Step <img src="/images/three.png"/> of 3</p>
                    </div>

                    <div className="clr"></div>
                    <br/>
                    <div>
                        <p>Based on visitor levels during your trial to date your pricing will be
                            £{this.state.pricing.monthlyDisplay} per month (paid month to month and <span
                                className="standOutText">you can cancel at any time</span>).</p>
                        <p>If you expect to use the system ongoing then you can gain a 20% discount by prepaying for 12
                            months.</p>
                        <p>If you have any questions at all about your pricing level please do reach out to.</p>
                    </div>

                    <NewSubscription
                        vatApplicable={this.state.pricing.vatApplicable}
                        user={this.state.settings.user}
                        paymentType={this.state.paymentType}
                        vendor="GoCardless"
                        monthlyFee={this.state.pricing.monthlyDisplay}
                        yearlyFee={this.state.pricing.yearlyDisplay}
                        crossoutFee={this.state.pricing.crossoutDisplay}
                        sixMonthlyFee={this.state.pricing.sixMonthsDisplay}
                        sixMonthCrossoutDisplay={this.state.pricing.sixMonthCrossoutDisplay}
                    />

                    <div className="clr"></div>
                    <br/>
                    <button className="btn btn-primary" onClick={() => this._signup()}>Sign Up</button>
                    <div className="clr"></div><br /><br /><br /><br />
                </div>
            )
        }

        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-12" id="results">
                            <h3>Create Subscription with Stripe Payment</h3>
                            <div className="clr"></div>
                            <br/>
                            {loadingDiv}
                            {displayDiv}
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    _signup: function () {
        FilterActions.startStripeSessionAndRedirect(this.state.paymentType);
    },
    _onChange: function () {
        this.setState(getCurrentState());
    },
    _onToken: function (token) {
        FilterActions.processStripePayment(token);
    }
});

export default Stripe;
