<?php 

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;

class Stripe extends \App\Http\Controllers\Controller {

	public function index( Request $request ) {

		if( !isset( $_SERVER['HTTP_REFERER'] ) ) {

			//die();
		}

		return view(

			'UI.payment.stripe.new-mandate', []
		);
	}
}