import React from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';


function getCurrentState( props ) {


	return {

		isSet 		: props.isSet,
		uploading 	: props.status.uploading
	}
}	

const style = {

	refresh 	: {

    	display 	: 'inline-block',
    	position 	: 'relative',
  	}
}

var ExistingAvatar = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		var existingAvatar = '';

		if( this.state.uploading) {

			existingAvatar = (

				<RefreshIndicator
					size={40}
					left={10}
					top={0}
					status="loading"
					style={style.refresh}
				/>
			);
		
		} else {

			existingAvatar = (

				<img src="/api/proxy/api/v1/user/avatar" />
			);
		}

		return (

			<div className="existingAvatar">

				{existingAvatar}
			</div>
		);
	}

});

export default  ExistingAvatar;