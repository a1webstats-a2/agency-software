import React from 'react';
import EditCompanyRecord from '../companyData/EditCompanyRecord.react';
import AssignToTeamMember from '../sessionResult/AssignToTeamMember.react';
import SendOrganisationViaEmail from './SendOrganisationViaEmail.react';
import CreateNote from '../sessionResult/CreateNote.react';
import FiltersStore from '../../stores/FiltersStore';
import NavBar from '../navigation/NavBar.react';
import NotificationsStore from '../../stores/NotificationsStore';
import SnackbarA1 from '../Snackbar.react';
import Footer from '../Footer.react';
import AdditionalCompanyData from '../companyData/AdditionalCompanyData.react';
import PreviousVisits from '../sessionResult/PreviousVisits.react';
import ClientTypesLightbox from '../prospecting/ClientTypesLightbox.react';
import FilterActions from '../../actions/FilterActions';
import ChooseExportType from '../export/ChooseExportType.react';
import SimpleOrganisationsView from './SimpleOrganisationsView.react';
import CreateClientTag from '../prospecting/CreateClientTag.react';
import UserTipActions from '../../actions/UserTipActions';

function getCurrentState() {
    let analyticsData = FiltersStore.getAnalyticsData();

    return {
        flatPageVisits: FiltersStore.getFlatPageVisits(),
        createNewClientTagOpen: FiltersStore.checkLightboxOpen('createNewClientTag', 1),
        trackedOrganisations: FiltersStore.getTrackedOrganisations(),
        newFormatOrganisations: FiltersStore.getNewFormattedOrganisations(),
        chooseExportTypeOpen: FiltersStore.checkLightboxOpen('chooseExportTypeCSVOrCRM', -1),
        looseFilterSetDisplay: FiltersStore.getLooseFilterSetDisplay(),
        scenariosFilterObj: FiltersStore.getScenarioFiltersObj(),
        quickExportResults: FiltersStore.getQuickExportSearchResults(),
        CRMExportSelection: FiltersStore.getCRMExportSelection(),
        displayPerPage: FiltersStore.getDisplayPerPageOnLandingPages(),
        landingPageNo: FiltersStore.getCurrentLandingPageNo(),
        clientTypesByIndex: FiltersStore.getClientTypesByIndex(),
        excludeSelection: FiltersStore.getExcludeSelection(),
        tags: FiltersStore.getClientTypes(),
        quickExportSearchString: FiltersStore.getQuickExportSearchString(),
        additionalCompanyData: FiltersStore.getAdditionalCompanyData(),
        additionalCompanyDataOpen: FiltersStore.checkLightboxOpen('additionalCompanyData', -1),
        isLoadingAdditionalCompanyData: FiltersStore.isLoadingAdditionalCompanyData(),
        latestAddedFilters: FiltersStore.getLatestAddedFilters(),
        displayFancyFilters: FiltersStore.showFancyFilters(),
        filters: FiltersStore.getFilters(),
        clientTypesObj: FiltersStore.getClientTypesLightboxObj(),
        previousHistory: {
            open: FiltersStore.checkLightboxOpen('previousVisits', 1),
            prevResults: FiltersStore.getPreviousHistory(),
            orgName: FiltersStore.getPreviousHistoryOrganisationName()
        },
        bulkOrganisationExportType: FiltersStore.getBulkOrganisationExportType(),
        sendOrganisationViaEmailOpen: FiltersStore.checkLightboxOpen('sendOrganisationViaEmailOpen', 1),
        organisations: analyticsData.organisations,
        relationships: analyticsData.organisationRelationships,
        settings: FiltersStore.getAllSettings(),
        isApplicationResting: FiltersStore.isApplicationResting(),
        editCompanyData: {
            activeRecord: FiltersStore.getActiveRecord(),
            open: FiltersStore.checkLightboxOpen('editCompanyRecord', 1),
        },
        previousVisitsObj: FiltersStore.getPreviousVisits(),
        isLoadingPreviousVisits: FiltersStore.isLoadingPreviousHistory(),
        assignedData: {
            currentTrackingObj: FiltersStore.getCurrentTrackingObj(),
            open: FiltersStore.checkLightboxOpen('assignToTeamMember', 1),
            teamMembers: FiltersStore.getTeamMembers()
        },
        notes: FiltersStore.getNotes(),
        createNoteOpen: FiltersStore.checkLightboxOpen('createNote', 1),
        sendEmailToTeamMembers: FiltersStore.getSendEmailToTeamMembers(),
        quickExportSettings: FiltersStore.getQuickExportSettings(),
        snackbarSettings: FiltersStore.getSnackbarSettings(),
        clientTypesLightboxOpen: FiltersStore.checkLightboxOpen('clientTypes', 1),

        isSearchingForPossibleOptions: FiltersStore.isSearchingForActiveRecordOptions(),
        checkedForPossibleOptions: FiltersStore.checkedForActiveRecordPossibleOptions(),
        possibleOptions: FiltersStore.getActiveRecordSuggestions(),
        chosenPossibleOption: FiltersStore.getActiveRecordChosenOption(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        }
    }
}

const OrganisationsDashboard = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    componentDidMount: function () {
        const queryParams = window.location.pathname.split('/');

        if (typeof queryParams[2] !== "undefined") {
            FilterActions.loadFiltersAndInitialData(parseInt(queryParams[2]));
        } else {
            FilterActions.loadNewFormatOrganisations();
            UserTipActions.loadOneUserTip();
        }

        FilterActions.selectAllOptionsIfSelectionEmpty();
        FiltersStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        FiltersStore.removeChangeListener(this._onChange);
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        const simpleView = (
            <SimpleOrganisationsView
                tags={this.state.tags}
                flatPageVisits={this.state.flatPageVisits}
                selectionType={this.state.quickExportSettings.selectionType}
                organisations={this.state.newFormatOrganisations}
                relationships={this.state.relationships}/>
        )

        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <div className="row">
                        {simpleView}
                    </div>
                </div>

                <Footer
                    looseFilterSetDisplay={this.state.looseFilterSetDisplay}
                    scenariosFilters={this.state.scenariosFilterObj}
                    isApplicationResting={this.state.isApplicationResting}
                    settings={this.state.settings}
                    filters={this.state.filters}
                    display={this.state.displayFancyFilters}
                    latestAddedFilters={this.state.latestAddedFilters}
                />

                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>
                <PreviousVisits previousVisitsObj={this.state.previousVisitsObj}
                                isLoading={this.state.isLoadingPreviousVisits} settings={this.state.settings}
                                previousVisits={this.state.previousHistory}/>
                <EditCompanyRecord
                    isSearchingForPossibleOptions={this.state.isSearchingForPossibleOptions}
                    chosenPossibleOption={this.state.chosenPossibleOption}
                    checkedForPossibleOptions={this.state.checkedForPossibleOptions}
                    possibleOptions={this.state.possibleOptions}
                    editCompanyRecordData={this.state.editCompanyData}
                    storeType="organisationDashboard"/>
                <AssignToTeamMember
                    storeType="organisationsDashboard"
                    tracked={this.state.trackedOrganisations}
                    assignedData={this.state.assignedData}/>
                <SendOrganisationViaEmail
                    sendEmailToTeamMembers={this.state.sendEmailToTeamMembers}
                    teamMembers={this.state.assignedData.teamMembers}
                    open={this.state.sendOrganisationViaEmailOpen}
                    settings={this.state.settings}/>
                <CreateNote notes={this.state.notes} open={this.state.createNoteOpen}/>
                <AdditionalCompanyData
                    open={this.state.additionalCompanyDataOpen}
                    isLoading={this.state.isLoadingAdditionalCompanyData}
                    additionalCompanyData={this.state.additionalCompanyData}/>
                <ClientTypesLightbox
                    open={this.state.clientTypesLightboxOpen}
                    storeType="reportBuilder"
                    relationships={this.state.clientTypesObj.relationships}
                    tags={this.state.tags}
                    organisationID={this.state.clientTypesObj.organisationID}/>
                <CreateClientTag open={this.state.createNewClientTagOpen}/>
                <ChooseExportType
                    selection={this.state.CRMExportSelection}
                    bulkOrganisationExportType={this.state.bulkOrganisationExportType}
                    open={this.state.chooseExportTypeOpen}
                    settings={this.state.settings}/>
            </div>
        )
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }
});

export default OrganisationsDashboard;
