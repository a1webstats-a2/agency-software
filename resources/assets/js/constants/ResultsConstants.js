var keyMirror = require( 'keyMirror' );

module.exports = keyMirror({

	SET_NEXT_RESULTS 					: 	null,
	SET_AND_FETCH_RESULTS				: 	null,
	SET_RESULTS 						: 	null,
	FETCH_RESULTS 						: 	null
});