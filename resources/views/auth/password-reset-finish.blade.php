@extends( 'templates.signup' )
@section( 'content' )
    
 	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

                <div id="a1strap">
                    <h1>{{ $agentData['name'] }}</h1>
                </div>

                <br /><br />

                <div>

                    @if( $result )

                    	<p>Thank you, your password has been successfully reset,
                    		you can now <a href="/auth/login">login</a></p>

                    @else

                    	<p>Sorry, there was a problem resetting your password, please 
                    		<a href="/forgotten-password">try again</a>
                    	</p>

                    @endif

                </div>

            </div>
        </div>
    </div>
@endsection