import React from 'react';
import TimePicker from 'material-ui/TimePicker';
import Schedule from 'material-ui/svg-icons/action/schedule';
import FilterActions from '../actions/FilterActions';

var TimeFrom  =  React.createClass({

	getInitialState : function() {

		return {

		};
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.time !== nextProps.time ) {

			return true;
		}

		return false;
	},

	render 	: 	function() {

		var defaultTimeFrom = new Date();
		defaultTimeFrom.setHours( 23 );
		defaultTimeFrom.setMinutes( 59 );
		defaultTimeFrom.setSeconds( 59 );

		return(

			<div className="timepicker">
				<TimePicker 
					ref="picker24hr"
					id="timeTo"
					defaultTime={defaultTimeFrom}
		  			format="24hr"
		  			inputStyle={{ color: '#000' }}
		  			textFieldStyle={{ width : 40 }}
		  			hintText=""
		  			onChange={this._handleChangeTimePicker} />
			</div> 	
		);

	},

	_handleChangeTimePicker : function( err, newTime ) {

        FilterActions.editFilter({

            storedValue      	:   newTime,
            type             	:   'timeTo'

        });
	}

});

export default  TimeFrom;