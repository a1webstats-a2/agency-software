<?php 

namespace App\Http\Controllers\EndUsers;

use Illuminate\Http\Request;
use Auth;

class Login extends \App\Http\Controllers\Controller {



	public function authenticate( Request $request )
  	{
        $this->validate( $request, [

          'email' =>  'email'
        ]);

      	$loginRes = $this->login( $request );

        if( $loginRes ) {
          
       		$request->session()->flash( 'success', 'Thank you, you have successfully logged in.' );

        	if( $request->input( 'filterType' ) && $request->input( 'id' ) ) {

            return redirect( '/lt/organisation/' . $request->input( 'id' ) );
          }

          if( $request->input( 'redirectArea' ) && 
              $request->input( 'redirectArea' ) !== "" ) {

            $reportID = "";

            if( $request->input( 'reportID' ) ) {

              $reportID = '/' . $request->input( 'reportID' );
            }

            switch( $request->input( 'redirectArea' ) ) {

              case "organisations" :

                return redirect( '/organisations' . $reportID );

                break;

              case "results" :

                return redirect( '/results' . $reportID );

                break;
            }
          }

          return redirect( '/' );

       	}

        return redirect( '/auth/login' )->with( 'status', 'Sorry, your login attempt failed, please try again. If you still can\'t login, please try resetting your password' );
    }

    public function login( Request $request ) {
    
        $formParams               = $request->input();
        $formParams['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $formParams['password']   = htmlentities( $request->input( 'password' ) );
    

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, env( 'API_SOURCE' ) . "/auth/login" );
        curl_setopt( $ch,CURLOPT_POST, 2);
        curl_setopt( $ch,CURLOPT_POSTFIELDS, http_build_query( $formParams )  );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        $result   = curl_exec( $ch );   

        curl_close( $ch );

        $valid    = true;

        $decoded  = json_decode( $result, true );



       	if( $decoded && isset( $decoded['valid'] ) && $decoded['valid'] ) {

          session([

            	//'secret'            =>   $decoded['secret'],
            	'clientId'          =>   $decoded['clientId'],
       			  'userId' 			      =>   $decoded['userId'],
              'full_access_token' =>   $decoded['full_access_token'],
           		'token'             =>   $decoded['token'],
       			  'regenerateCount' 	=>   0,
           		'user'              =>   $decoded['user']
       		]);

       		return true;
       	}


       	return false;
    }
}
