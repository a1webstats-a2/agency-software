import React from 'react';
import FiltersStore from '../stores/FiltersStore';
import NotificationsStore from '../stores/NotificationsStore';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import NoResultsText from './sessionResult/NoResultsText.react';
import SessionResult from './sessionResult/SessionResult.react';
import PreviousVisits from './sessionResult/PreviousVisits.react';
import CompanyData from './companyData/CompanyData.react';
import CreateNote from './sessionResult/CreateNote.react';
import AssignToTeamMember from './sessionResult/AssignToTeamMember.react';
import EditCompanyRecord from './companyData/EditCompanyRecord.react';
import CreateClientTag from './prospecting/CreateClientTag.react';
import NavBar from './navigation/NavBar.react';
import SnackbarA1 from './Snackbar.react';
import Footer from './Footer.react';
import PaginationOptions from './PaginationOptions.react';
import AdditionalCompanyData from './companyData/AdditionalCompanyData.react';
import SendOrganisationViaEmail from './organisationsDashboard/SendOrganisationViaEmail.react';
import FilterActions from '../actions/FilterActions';
import A2GoogleMap from './A2GoogleMap.react';
import ExportToCRM from './ExportToCRM.react';
import ReportIssue from './sessionResult/ReportIssue.react';
import UserTipsStore from '../stores/UserTipsStore';
import UserTipHelpLink from './UserTipHelpLink.react';

const refreshIndicatorStyle = {
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
        float: 'left'
    },
};

function getCurrentState() {
    let resultsMaster = FiltersStore.getResults();
    let minNumberOfPagesFilter = FiltersStore.getFilterByTypeWithParam('minNumberOfPages');
    let maxNumberOfPagesFilter = FiltersStore.getFilterByTypeWithParam('maxNumberOfPages');
    let minSessionDurationFilter = FiltersStore.getFilterByTypeWithParam('minSessionDuration');

    return {
        isLoadingAdditionalCompanyData: FiltersStore.isLoadingAdditionalCompanyData(),
        heatmapData: FiltersStore.filtersGetHeatmapData(),
        resultsReportHasBeenSent: FiltersStore.resultsReportHasBeenSent(),
        reportNoResultsBoxIsOpen: FiltersStore.reportNoResultsBoxIsOpen(),
        reportRecordData: FiltersStore.getReportRecordData(),

        clientTypesByIndex: FiltersStore.getClientTypesByIndex(),
        scenariosFilterObj: FiltersStore.getScenarioFiltersObj(),

        isSearchingForPossibleOptions: FiltersStore.isSearchingForActiveRecordOptions(),
        checkedForPossibleOptions: FiltersStore.checkedForActiveRecordPossibleOptions(),
        possibleOptions: FiltersStore.getActiveRecordSuggestions(),
        chosenPossibleOption: FiltersStore.getActiveRecordChosenOption(),

        updatingExportToCRM: FiltersStore.getUpdateBox('exportToCRM'),
        CRMExportSelection: FiltersStore.getCRMExportSelection(),
        minNumberOfPages: minNumberOfPagesFilter,
        maxNumberOfPages: maxNumberOfPagesFilter,
        minSessionDuration: minSessionDurationFilter,

        numberOfResultsLoad: FiltersStore.getNumberOfResultLoads(),
        currentMapDetails: FiltersStore.getCurrentMapDetails(),
        popoverObj: FiltersStore.getPopoverSettingsNew(),
        blockedOrganisations: FiltersStore.getBlockedList(),

        additionalCompanyData: FiltersStore.getAdditionalCompanyData(),
        reportRecordOpen: FiltersStore.checkLightboxOpen('reportRecord', -1),
        additionalCompanyDataOpen: FiltersStore.checkLightboxOpen('additionalCompanyData', -1),
        sendOrganisationViaEmailOpen: FiltersStore.checkLightboxOpen('sendOrganisationViaEmailOpen', 1),
        exportToCRMOpen: FiltersStore.checkLightboxOpen('exportToCRM', -1),
        sendEmailToTeamMembers: FiltersStore.getSendEmailToTeamMembers(),

        exportSelection: FiltersStore.getExportSelection(),
        latestAddedFilters: FiltersStore.getLatestAddedFilters(),
        displayFancyFilters: FiltersStore.showFancyFilters(),
        filters: FiltersStore.getFilters(),
        snackbarSettings: FiltersStore.getSnackbarSettings(),

        orderBy: FiltersStore.getOrderBy(),
        previousVisitsObj: FiltersStore.getPreviousVisits(),
        isLoadingPreviousVisits: FiltersStore.isLoadingPreviousHistory(),
        isApplicationResting: FiltersStore.isApplicationResting(),
        paginationData: FiltersStore.getPaginationTotals(),
        resultsFound: FiltersStore.getResultsFound(),
        displayedColumns: FiltersStore.getDisplayedColumns(),
        clientTags: FiltersStore.getClientTypes(),
        settings: FiltersStore.getAllSettings(),
        displayBy: FiltersStore.getDisplayBy(),
        results: resultsMaster.results,
        relationships: resultsMaster.relationships,
        fancyFiltersData: {},
        currentMapOpen: FiltersStore.checkLightboxOpen('currentMapDetails', 1),
        createNoteOpen: FiltersStore.checkLightboxOpen('createNote', 1),
        previousHistory: {
            open: FiltersStore.checkLightboxOpen('previousVisits', 1),
            prevResults: FiltersStore.getPreviousHistory(),
            orgName: FiltersStore.getPreviousHistoryOrganisationName()
        },
        companyData: {
            companyData: FiltersStore.getOrganisationData(),
            dueDilData: false,
            open: FiltersStore.checkLightboxOpen('companyData', 0)
        },
        editCompanyData: {
            activeRecord: FiltersStore.getActiveRecord(),
            open: FiltersStore.checkLightboxOpen('editCompanyRecord', 1),
        },
        notes: FiltersStore.getNotes(),
        assignedData: {
            currentTrackingObj: FiltersStore.getCurrentTrackingObj(),
            open: FiltersStore.checkLightboxOpen('assignToTeamMember', 1),
            teamMembers: FiltersStore.getTeamMembers()
        },
        createNewClientTagOpen: FiltersStore.checkLightboxOpen('createNewClientTag', 1),
        popoverOpen: FiltersStore.getPopoverID(),
        user: FiltersStore.getUser(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        },
        excludeSelection: FiltersStore.getExcludeSelection(),
        looseFilterSetDisplay: FiltersStore.getLooseFilterSetDisplay()
    }
}

const Results = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props, false);
    },
    componentDidMount: function () {
        var queryParams = window.location.pathname.split('/');

        if (typeof queryParams[2] !== "undefined") {

            var reportID = parseInt(queryParams[2]);

            FilterActions.loadFilters(reportID);
        } else {
            FilterActions.loadResultsIfNoPreviousLoads();
        }

        FiltersStore.addChangeListener(this._onChange);
        UserTipsStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        FiltersStore.removeChangeListener(this._onChange);
        UserTipsStore.removeChangeListener(this._onChange);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        var resultItems = this.state.results.map(function (result, key) {
            var excludeMe = (this.state.excludeSelection.indexOf(result.organisationid) === -1) ? false : true;

            return <SessionResult
                key={key}
                myKey={key}
                heatmapData={this.state.heatmapData}
                assignedTo={result.assigned_user}
                popoverOpenID={this.state.popoverOpen}
                clientTypesByIndex={this.state.clientTypesByIndex}
                blockedOrganisations={this.state.blockedOrganisations}
                settings={this.state.settings}
                relationships={this.state.relationships}
                data={result}
                exportMe={FiltersStore.checkSelectedForExport(result.id)}
                tags={this.state.clientTags}
                excludeMe={excludeMe}
                displayedColumns={this.state.displayedColumns}
                popoverObj={this.state.popoverObj}/>

        }.bind(this));

        let noResultsText = "";

        if (
            (this.state.paginationData.total === 0 && this.state.isApplicationResting) ||
            (this.state.isApplicationResting && this.state.results.length === 0)
        ) {
            noResultsText = (
                <NoResultsText
                    resultsReportHasBeenSent={this.state.resultsReportHasBeenSent}
                    reportNoResultsBoxIsOpen={this.state.reportNoResultsBoxIsOpen}/>

            )
        } else if (!this.state.isApplicationResting) {
            noResultsText = (
                <div className="row">
                    <div style={refreshIndicatorStyle.container} id="resultsUpdatingMessage"
                         className="alert alert-success">
                        <RefreshIndicator
                            size={40}
                            left={0}
                            style={refreshIndicatorStyle.refresh}
                            top={0}
                            loadingColor={"#3c763d"}
                            status="loading"
                        />
                        <p style={{float: 'left', marginLeft: 30, marginTop: 12}}>Results are updating, please wait</p>

                    </div>
                </div>
            )
        } else {
            var pageNum = (Math.ceil(this.state.paginationData.offset / this.state.paginationData.returnAmount)) + 1;

            noResultsText = (

                <div className="row">
                    <div className="col-md-12">
                        <div className="alert alert-success" id="resultsUpdatingMessage">
                            <p>Page <strong>{pageNum}</strong> of <strong>{this.state.paginationData.numPages}</strong> - {this.state.paginationData.total} results
                            </p>
                            <p>If you were expecting different results, please check your filters at the bottom of the
                                page.</p>
                        </div>
                    </div>
                </div>
            )
        }

        var hiddenOrganisations = this.state.blockedOrganisations.map(function (blocked, i) {
            return (
                <div key={i}><p className="hiddenOrganisation">{blocked} hidden</p><br/></div>

            );
        })

        var currentMapActions = [
            <FlatButton
                label="Close"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._closeCurrentMap}
            />
        ];

        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <UserTipHelpLink/>
                    <div className="row">
                        <div className="col-md-3">
                            <div className="displayOptions">
                                <PaginationOptions
                                    orderBy={this.state.orderBy}
                                    paginationData={this.state.paginationData}
                                    minNumberOfPages={this.state.minNumberOfPages}
                                    maxNumberOfPages={this.state.maxNumberOfPages}
                                    minSessionDuration={this.state.minSessionDuration}
                                    isApplicationResting={this.state.isApplicationResting}/>
                            </div>
                            <div className="clr"></div>
                        </div>

                        <div className="col-md-9" id="results">
                            <div className="row">
                                <div className="col-md-2">
                                    <h3>Results</h3>
                                </div>
                                <div className="col-md-10">
                                    <div style={{marginTop: 25}}>
                                        <UserTipHelpLink/>
                                    </div>
                                </div>
                            </div>

                            <div className="clr"></div>

                            {hiddenOrganisations}

                            <div className="clr"></div>

                            {noResultsText}

                            <div className="row">

                                <div className="col-md-12">

                                    {resultItems}

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <Footer
                    looseFilterSetDisplay={this.state.looseFilterSetDisplay}
                    scenariosFilters={this.state.scenariosFilterObj}
                    isApplicationResting={this.state.isApplicationResting}
                    settings={this.state.settings}
                    filters={this.state.filters}
                    display={this.state.displayFancyFilters}
                    latestAddedFilters={this.state.latestAddedFilters}
                />
                <Dialog
                    title={this.state.currentMapDetails.locationString}
                    actions={currentMapActions}
                    modal={false}
                    open={this.state.currentMapOpen}
                    onRequestClose={this._closeCurrentMap}
                >
                    <A2GoogleMap
                        open={this.state.currentMapOpen}
                        organisationName={this.state.currentMapDetails.organisationName}
                        isGeoLocated={this.state.currentMapDetails.geolocated}
                        locationString={this.state.currentMapDetails.locationString}
                        organisation={this.state.currentMapDetails.organisationName}
                        longitude={this.state.currentMapDetails.longitude}
                        latitude={this.state.currentMapDetails.latitude}/>
                </Dialog>
                <ExportToCRM
                    settings={this.state.settings}
                    updatingBox={this.state.updatingExportToCRM}
                    selection={this.state.CRMExportSelection}
                    open={this.state.exportToCRMOpen}/>
                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>
                <PreviousVisits previousVisitsObj={this.state.previousVisitsObj}
                                isLoading={this.state.isLoadingPreviousVisits} settings={this.state.settings}
                                previousVisits={this.state.previousHistory}/>
                <CompanyData companyData={this.state.companyData}/>
                <EditCompanyRecord
                    isSearchingForPossibleOptions={this.state.isSearchingForPossibleOptions}
                    chosenPossibleOption={this.state.chosenPossibleOption}
                    checkedForPossibleOptions={this.state.checkedForPossibleOptions}
                    possibleOptions={this.state.possibleOptions}
                    editCompanyRecordData={this.state.editCompanyData}
                    storeType="reportBuilder"/>
                <CreateNote notes={this.state.notes} open={this.state.createNoteOpen}/>
                <AssignToTeamMember storeType="reportBuilder" assignedData={this.state.assignedData}/>
                <CreateClientTag open={this.state.createNewClientTagOpen}/>
                <AdditionalCompanyData
                    open={this.state.additionalCompanyDataOpen}
                    isLoading={this.state.isLoadingAdditionalCompanyData}
                    additionalCompanyData={this.state.additionalCompanyData}/>
                <SendOrganisationViaEmail sendEmailToTeamMembers={this.state.sendEmailToTeamMembers}
                                          teamMembers={this.state.assignedData.teamMembers}
                                          open={this.state.sendOrganisationViaEmailOpen}
                                          settings={this.state.settings}/>
                <ReportIssue
                    user={this.state.user}
                    data={this.state.reportRecordData}
                    open={this.state.reportRecordOpen}/>


            </div>
        );
    },
    _closeCurrentMap: function () {
        FilterActions.closeLightbox();
    },
    _startExport: function () {
        FilterActions.openExport();
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }
});

export default Results;
