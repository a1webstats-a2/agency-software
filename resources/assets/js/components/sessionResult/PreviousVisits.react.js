import React from 'react';
import FiltersStore 	from '../../stores/FiltersStore';
import FilterActions 	from '../../actions/FilterActions';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import moment from 'moment';

const style = {
  
  	margin 	: 12,

	container: {
		position: 'relative',
	},
	refresh: {
		display: 'inline-block',
		position: 'relative',
	},
};

function ordinal_suffix_of( i ) {
    
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function twoDigits( d ) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

function getCurrentState( props ) {


	return {

		previousVisitsObj 	: props.previousVisitsObj,
		data 				: props.previousVisits,
		previousVisits 		: props.previousVisits.prevResults,
		open 				: props.previousVisits.open,
		orgName 			: props.previousVisits.orgName,
		settings 			: props.settings,
		isLoading 			: props.isLoading
	}
}

function isIterable( obj ) {
 
  	if ( obj == null ) {
    
    	return false;
  	}
  
  	return typeof obj[Symbol.iterator] === 'function';
}

var PreviousVisits = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );

	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.previousVisitsObj !== nextProps.previousVisitsObj ) {

			return true;
		}

		if( this.props.isLoading !== nextProps.isLoading ) {

			return true;
		}

		if( this.props.previousVisits.open !== nextProps.previousVisits.open ) {

			return true;
		}

		if( this.props.previousVisits.prevResults !== nextProps.previousVisits.prevResults ) {

			return true;
		}

		return false;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	closePreviousVisits : function() {

		FilterActions.closeLightbox();
	},


	render 	: 	function() {

		var prevResults = 	"";

        var monthNames  =   [  "January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];

		if( this.state.previousVisitsObj.allFound.length > 0 ) {

			prevResults = this.state.previousVisitsObj.allFound.map( function( result, i ) {

				if( result.created_at ) {

			        var displayDate     =  	moment( result.created_at ).format( "Do MMMM YYYY" );

			        switch( this.state.settings.user.preferred_date_format ) {

			            case "dd/mm/yyyy" :

			                displayDate = 	moment( result.created_at ).format( "DD/MM/YYYY" );

			                break;

			            case "mm/dd/yyyy" :

			                displayDate = 	moment( result.created_at ).format( "MM/DD/YYYY" );

			                break;

			            case "yyyy-mm-dd" :

			                displayDate = 	moment( result.created_at ).format( "YYYY-MM-DD" );
			                
			                break;
			        }

			        var selected = this.state.previousVisitsObj.selection.indexOf( result.session_id ) > -1;

					return ( 

						<TableRow selected={selected} key={i}>
							<TableRowColumn>{displayDate}</TableRowColumn>
	                		<TableRowColumn>{result.referrer_name}</TableRowColumn>
	                		<TableRowColumn>{result.keyword}</TableRowColumn>
	                		<TableRowColumn>{result.page_visit_count}</TableRowColumn>
						</TableRow> 
					);
				}

			}.bind( this ) );

		}

		var loadingStatus = ( this.state.isLoading ) ? "loading" : "hide";

		const actions = [
        
			<FlatButton
            	label="Show Visits"
            	primary={true}
            	onTouchTap={this._loadAllSessions}
            />,

            <FlatButton
            	label="Close"
            	secondary={true}
            	onTouchTap={this.closePreviousVisits}
            />
        ];

        var table = '';

        var loadMoreLink = '';

		if( this.state.previousVisitsObj.allFound.length > 99 ) {

			loadMoreLink = (

				<a href="#" onClick={this._showMore}>Load More</a>

			)
		}

		if( this.state.previousVisitsObj.allFound.length > 0 ) {

			table = (
				<div>
					<Table selectable={true} multiSelectable={true} onRowSelection={this._setSelection}>

						<TableHeader 
							adjustForCheckbox={true}
							enableSelectAll={true} 
							displaySelectAll={true}
						>
							<TableRow>
								<TableHeaderColumn>Visit Date/Time</TableHeaderColumn>
								<TableHeaderColumn>Referrer</TableHeaderColumn>
								<TableHeaderColumn>Keywords</TableHeaderColumn>
								<TableHeaderColumn>Pages Visited</TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody
				            displayRowCheckbox={true}

						>

			        		{prevResults}
						</TableBody>
					</Table>
					
					<RefreshIndicator
						size={40}
						left={10}
						top={0}
						status={loadingStatus}
						style={style.refresh}
					/>

					{loadMoreLink}


				</div>
			)
		}

		return(

			<div className="previousVisits">


				<Dialog
		          title="Previous Visits Summary"
		          modal={false}
		          autoScrollBodyContent={true}
		          actions={actions}
		          open={this.state.open}
		          onRequestClose={this.closePreviousVisits}
		        >

		        	<div className="row">
		        	</div>

		        	<div style={style.container}>
						<RefreshIndicator
							size={40}
							left={10}
							top={0}
							status={loadingStatus}
						style={style.refresh}
						/>
					</div>

		        	<div className="row">

		        		{table}
		        	</div>

		        </Dialog>

			</div>
		)
	},

	_setSelection : function( selection ) {

		var sendSelection = [];

		if( selection === "all" ) {


			sendSelection = this.state.previousVisitsObj.allFound.map( function( session, i ) {

				return session.session_id;
			});
		
		} else {

			try {

				if( isIterable( selection ) && selection && selection.length > 0  ) {

					sendSelection = selection.map( function( selected, i ) {

						return this.state.previousVisitsObj.allFound[selected].session_id;

					}.bind( this ) );
				}
		
			} catch( e ) {

				
			}
		}

		FilterActions.setPreviousVisitsSelection( sendSelection );
	},

	_showMore : function( event ) {

		event.preventDefault();

		FilterActions.loadMorePreviousVisits();
	},

	_loadAllSessions : function() {



		if( !confirm( 'This will replace currently displayed results... continue?' ) ) {

			return false;
		}


		FilterActions.displayPreviousVisits({

			orgName 	: this.state.orgName 
		});
	}
})

export default  PreviousVisits;