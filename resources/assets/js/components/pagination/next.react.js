import React         	from 'react';
import FilterActions  	from '../../actions/FilterActions';
import Next from 'material-ui/svg-icons/image/navigate-next';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState( props ) {

	return {

		paginationSettings 	: 	props.paginationSettings,
		isLoadingResults 	: 	props.isLoadingResults
	}
}

var PaginationNext = React.createClass({ 

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( nextProps !== this.props ) {

			return true;
		}

		return false;
	},

	render 	: 	function() {

		var lastPage = false;

		var pageNum = ( ( this.state.paginationSettings.returnAmount * this.state.paginationSettings.offset ) / 100 ) + 1;

 		if( pageNum === this.state.paginationSettings.numPages ) {

 			lastPage = true;
 		}

 		var isDisabled = ( this.state.isLoadingResults ) ? true : false;

 		if( !lastPage ) {
			
			return (

				<RaisedButton 
					disabled={isDisabled} 
					fullWidth={true}
					style={{ marginBottom : 20 }} 
					label="Next" primary={true} 
					icon={<Next />} onClick={this._nextAction} />
			);

		} else {

			return (
				<div className="paginationNext">

				</div>
		
			);
		}
	},

	_nextAction 	: 	function() {

		FilterActions.paginationNext();
	}

});

export default  PaginationNext;