import React 					from 'react';
import FilterActions 			from '../actions/FilterActions';
import FiltersStore 			from '../stores/FiltersStore';

import Checkbox from 'material-ui/Checkbox';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import Visibility from 'material-ui/svg-icons/action/visibility';



const styles = {
 
  	checkbox: {
   	 	marginBottom: 16

  	},

  	person 	: 	{

  		marginLeft 	: 	15
  	}
};

function getCurrentState( props ) {

	var type 	= 	'organisationFilter';

	return {	
				
		searchByData 		: 	props.searchByData,
		inclusionType 		: 	props.otherOrganisationData.exclude,
		rowCount 			: 	props.organisationData.row_count,
		type 				: 	type,
		filtersBeingEdited 	: 	props.otherOrganisationData.filtersBeingEdited,
		searchBox 			: 	props.searchByCheckbox,
		myKey 				: 	props.myKey,
		organisationData 	: 	props.organisationData,
		organisationId 		: 	props.organisationData.id,
		allOrgFilters 		: 	props.allOrgFilters,
		organisationName	: 	props.organisationData.use_organisation_name,
		isChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( type,
			props.organisationData.id )
	}
}

var OrganisationCheckbox 	= 	React.createClass({

	getInitialState 	: 	function() {


		return getCurrentState( this.props ); 
	},

	componentWillReceiveProps 	: 	function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {
		
		if( this.props.organisationData.use_organisation_name !== nextProps.organisationData.use_organisation_name ) {

			return true;
		}

		if( this.props.organisationData !== nextProps.organisationData ) {

			return true;
		}

		if( this.state.isChecked !== nextState.isChecked ) {

			return true;
		}

		if( this.props.searchByData.organisationIncludeType !== nextProps.searchByData.organisationIncludeType ) {

			return true;
		}

		if( this.props.otherOrganisationData.filtersBeingEdited !== nextProps.otherOrganisationData.filtersBeingEdited ) {

			return true;
		}

		if( this.props.allOrgFilters !== nextProps.allOrgFilters ) {

			return true;
		}
			
		if( this.props.otherOrganisationData.exclude !== nextProps.otherOrganisationData.exclude ) {

			return true;
		}


		return true;

	},

	render : 	function() {

		var countField 		=	"include_count";

		switch( this.state.searchByData.organisationIncludeType.toLowerCase() ) {

			case "include" :

				countField = "include_count";

				break;

			case "nonexplicitinclude" :

				countField = "include_count";

				break;

			case "exclude" :

				countField = "exclude_count";

				break;

		}

		var resCountClass 	= 	( this.state.filtersBeingEdited ) ? 'greyedOutResultCount' : 'resultCount';

		var organisationName = "";

        var stringArray         =   this.state.organisationName.split( " " );

        var returnString = stringArray.map( function( word, i ) {

            if( word.toLowerCase().indexOf( "ftip") === -1 ) {

                return word;
            
            } else {

                return "";
            }
        })

        organisationName    =   returnString.join( " " ).trim();


		var label 			= 	(

			<div>

		    	{organisationName}

		    	<span className={resCountClass}>{this.state.organisationData[countField]} visitor(s)</span>
			</div>
		)

		return(
	
	    	<Checkbox
		    	label={label}
		    	checked={this.state.isChecked}
		    	style={styles.checkbox}
		    	onCheck={this._saveCheckedState}
		    />

		);
	},

	_saveCheckedState 	: 	function( event, checked ) {

		var newState  = {

   			type          	:   this.state.type, 
   			storedValue 	:   {

				id 			: 	this.state.organisationId,
				include 	: 	this.state.inclusionType,
				name 		: 	this.state.organisationName,
				searchBox 	: 	this.state.searchBox,
				value 		: 	this.state.organisationId
			},
			id 				: 	this.state.organisationId
   		}

   		var checked = event.target.checked;

   		this.setState({

   			isChecked : checked
   		})

   		if( checked ) {

   			FilterActions.createFilter( newState );

   		} else {

   			FilterActions.removeFilterByType({

   				type 	: 	this.state.type,
   				id 		: 	this.state.organisationId
   			});
   		}
	},
});

export default  OrganisationCheckbox;