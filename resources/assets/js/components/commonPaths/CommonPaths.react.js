import React from 'react';
import NavBar from '../navigation/NavBar.react';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import CommonPathsTable from './CommonPathsTable.react';
import Footer from '../Footer.react';
import UserTipHelpLink from '../UserTipHelpLink.react';
import NotificationsStore from '../../stores/NotificationsStore';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState() {

    var settings = FiltersStore.getAllSettings();
    var windowBoxOpen = FiltersStore.getWindowBoxOpen();
    var newFilters = FiltersStore.haveNewFiltersBeenApplied();
    var paginationData = FiltersStore.getPaginationTotals();
    var filters = FiltersStore.getFilters();
    var isApplicationResting = FiltersStore.isApplicationResting();

    return {
        looseFilterSetDisplay: FiltersStore.getLooseFilterSetDisplay(),
        scenariosFilterObj: FiltersStore.getScenarioFiltersObj(),
        sessionIDsBeenSet: FiltersStore.commonPathsSessionIDsHaveBeenSet(),
        getCommonPathRowSelected: FiltersStore.getCommonPathRowSelected(),
        latestAddedFilters: FiltersStore.getLatestAddedFilters(),
        displayFancyFilters: FiltersStore.showFancyFilters(),
        filters: FiltersStore.getFilters(),
        commonPaths: FiltersStore.getCommonPaths(),
        settings: settings,
        isApplicationResting: isApplicationResting,
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: settings,
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            windowBoxOpen: windowBoxOpen,
            newFilters: newFilters,
            paginationData: paginationData,
            allFilters: filters,
            isApplicationResting: isApplicationResting,
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        }
    }
}

var CommonPaths = React.createClass({
    getInitialState: function () {
        return getCurrentState();
    },
    componentDidMount: function () {
        FilterActions.loadCommonPaths();
        FiltersStore.addChangeListener(this._onChange);
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.state.looseFilterSetDisplay !== nextState.looseFilterSetDisplay) {
            return true;
        }

        if (this.state.commonPathsSessionIDsHaveBeenSet !== nextState.commonPathsSessionIDsHaveBeenSet) {
            return true;
        }

        if (this.state.getCommonPathRowSelected !== nextState.getCommonPathRowSelected) {
            return true;
        }

        if (this.state.filters !== nextState.filters) {
            return true;
        }

        if (this.state.commonPaths === nextState.commonPaths) {
            return true;
        }

        return false;
    },
    componentWillUnmount: function () {
        FiltersStore.removeChangeListener(this._onChange);
    },
    render: function () {
        return (
            <div className="reportBuilder">
                <NavBar data={this.state.notificationsData}/>
                <div className="clr"></div>
                <div className="container mainContainer">
                    <div className="row">
                        <div className="col-md-3">
                            <div className="displayOptions">
                                <h3>Actions</h3>
                                <div className="clr"></div>
                                <br/><br/><br/>
                                <RaisedButton disabled={!this.state.sessionIDsBeenSet}
                                              onClick={this._showSelectedCommonPath} secondary={true} fullWidth={true}
                                              label="Show Results"/>
                            </div>

                            <div className="clr"></div>
                            <br/>
                        </div>
                        <div className="col-md-9" id="results">

                            <div className="col-md-12">
                                <div className="row">
                                    <div className="col-md-3">

                                        <h3>Common Paths</h3>
                                    </div>
                                    <div className="col-md-9">
                                        <div style={{marginTop: 25}}>
                                            <UserTipHelpLink/>
                                        </div>
                                    </div>
                                </div>
                                <CommonPathsTable
                                    rowSelected={this.state.getCommonPathRowSelected}
                                    paths={this.state.commonPaths}/>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer
                    looseFilterSetDisplay={this.state.looseFilterSetDisplay}
                    scenariosFilters={this.state.scenariosFilterObj}
                    isApplicationResting={this.state.isApplicationResting}
                    settings={this.state.settings}
                    filters={this.state.filters}
                    display={this.state.displayFancyFilters}
                    latestAddedFilters={this.state.latestAddedFilters}
                />
            </div>
        )
    },
    _getCommonPaths: function () {
    },
    _showSelectedCommonPath: function () {
        FilterActions.showSelectedCommonPath();
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }
});

export default CommonPaths;
