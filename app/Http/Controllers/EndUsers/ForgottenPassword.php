<?php

namespace App\Http\Controllers\EndUsers;

use Illuminate\Http\Request;

class ForgottenPassword extends \App\Http\Controllers\Controller {

	public function index( Request $request ) {

		return view( 'auth.forgotten-password', [

			'agentData'	=>	getAgentConfigData()
		]);
	}

	public function store( Request $request ) {

		$client 	= 	new \GuzzleHttp\Client();

		$formParams =	$request->input();

		$response 	= 	$client->request( 'POST', env( 'API_SOURCE' ) . '/password-reset', [

				'form_params' => $formParams
			]
		);

		$contents 	= $response->getBody()->getContents();

		return view( 'auth.forgotten-password-sent', [

			'email'		=>	$request->input( 'email' ),
			'agentData'	=>	getAgentConfigData()
		]);
	}
}