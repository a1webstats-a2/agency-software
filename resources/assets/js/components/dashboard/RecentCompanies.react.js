import React from 'react';
import Paper from 'material-ui/Paper';
import {browserHistory} from 'react-router';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import TagIcon from 'material-ui/svg-icons/action/label-outline';
import IconButton from 'material-ui/IconButton';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import SendOrganisationViaEmail from '../organisationsDashboard/SendOrganisationViaEmail.react';
import ClientTypesLightbox from '../prospecting/ClientTypesLightbox.react';

const iconStyle = {
    height: 15,
    width: 15,
    color: '#9370DB',
    margin: 0,
    marginRight: 10,
}

const styles = {
    paper: {
        height: 400,
        padding: 20,
    },
    svg: {
        color: '#170550',
    }
}

function getCurrentState() {
    const analyticsData = FiltersStore.getAnalyticsData();

    return {
        settings: FiltersStore.getAllSettings(),
        sendOrganisationViaEmailOpen: FiltersStore.checkLightboxOpen('sendOrganisationViaEmailOpen', 1),
        assignedData: {
            currentTrackingObj: FiltersStore.getCurrentTrackingObj(),
            open: FiltersStore.checkLightboxOpen('assignToTeamMember', 1),
            teamMembers: FiltersStore.getTeamMembers()
        },
        sendEmailToTeamMembers: FiltersStore.getSendEmailToTeamMembers(),
        clientTypesObj: FiltersStore.getClientTypesLightboxObj(),
        clientTypesLightboxOpen: FiltersStore.checkLightboxOpen('clientTypes', 1),
        relationships: analyticsData.organisationRelationships,
        tags: FiltersStore.getClientTypes(),
    }
}

class RecentCompanies extends React.Component {
    constructor(props) {
        super(props);

        this.state = getCurrentState()

        this._showTags = this._showTags.bind(this);

        FiltersStore.addChangeListener(this._onChange);
    }
    componentWillReceiveProps(newProps) {
        this.setState(getCurrentState())
    }
    render() {
        const orgs = this.props.companies.slice(0, 8).map(organisation => {
            return (
                <tr key={organisation[1]}>
                    <td className="w-1/2 p-2">{organisation[1]}</td>
                    <td className="flex justify-end p-1">
                        <IconButton
                            style={iconStyle}
                            tooltip="Email"
                            onTouchTap={this._openEmail}
                            iconStyle={iconStyle}>
                            <EmailIcon data-org-id={organisation[0]}/>
                        </IconButton>
                        <IconButton
                            style={iconStyle}
                            tooltip="Tags"
                            onTouchTap={this._showTags}
                            iconStyle={iconStyle}>
                            <TagIcon data-org-id={organisation[0]}/>
                        </IconButton>
                    </td>
                </tr>
            )
        })

        return (
            <div>
                <Paper style={styles.paper} zDepth={4}>
                    <div className="blueHeader">
                        <h4>Recent Companies</h4> <a style={{color: '#fff'}} href="#" onClick={this._viewAll}>view
                        all</a>
                    </div>
                    <table className="table">
                        <tbody>
                        {orgs}
                        </tbody>
                    </table>
                </Paper>

                <SendOrganisationViaEmail
                    sendEmailToTeamMembers={this.state.sendEmailToTeamMembers}
                    teamMembers={this.state.assignedData.teamMembers}
                    open={this.state.sendOrganisationViaEmailOpen}
                    settings={this.state.settings} />
                <ClientTypesLightbox
                    open={this.state.clientTypesLightboxOpen}
                    storeType="reportBuilder"
                    relationships={this.state.clientTypesObj.relationships}
                    tags={this.state.tags}
                    organisationID={this.state.clientTypesObj.organisationID} />
            </div>
        );
    }
    _viewAll(event) {
        event.preventDefault();

        browserHistory.push('/organisations');
    }
    _openEmail(event) {
        FilterActions.sendOrganisationViaEmail(event.target.parentElement.dataset.orgId);
    }
    _showTags(event) {
        let relationships = [];

        const orgID = event.target.dataset.orgId

        if (
            typeof this.state.relationships !== 'undefined' &&
            typeof this.state.relationships[orgID] !== 'undefined') {

            relationships = JSON.stringify(this.state.relationships[orgID]);
        }

        FilterActions.openClientTypesLightbox({
            organisation_id: orgID,
            relationships_json: relationships
        });
    }
    _onChange() {
        this.state = getCurrentState();
    }
}

export default RecentCompanies;
