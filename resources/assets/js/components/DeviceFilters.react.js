import React 					from 'react';
import DeviceFilter 			from './deviceFilters/DeviceFilter.react';
import FiltersStore				from '../stores/FiltersStore';
import FilterActions 			from '../actions/FilterActions';
import BrowserFilter 			from './deviceFilters/BrowserFilter.react';
import OperatingSystemFilter 	from './deviceFilters/OperatingSystemFilter.react';


function getCurrentState( props ) {

	var deviceFiltersData = props.deviceFiltersData;

	return {

		operatingSystems 	: 	deviceFiltersData.operatingSystems,
		browsers 			: 	deviceFiltersData.browsers,
		filterChanges 		: 	deviceFiltersData.filterChanges
	}
}

var DeviceFilters = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.deviceFiltersData === nextProps.deviceFiltersData ) {

			return false;
		}

		return true;
	},

	componentWillReceiveProps : function( newProps ) {
		
		this.setState( getCurrentState( newProps ) );
	},


	render : function() {
		
		var deviceTypes = [

			<DeviceFilter key="desktop" myId={1} checked={FiltersStore.checkIfFilterTypeAndIndexExists( 'devicefilter', 1 )} device="desktop" />, 
			<DeviceFilter key="tablet" myId={2} checked={FiltersStore.checkIfFilterTypeAndIndexExists( 'devicefilter', 2 )} device="tablet" />,
			<DeviceFilter key="mobile" myId={3} checked={FiltersStore.checkIfFilterTypeAndIndexExists( 'devicefilter', 3 )} device="mobile" />
		];

		var browserComponents = this.state.browsers.map( function( browser, i ) {
	      
	      	return (

	        	<BrowserFilter key={i}  browserData={browser} checked={FiltersStore.checkIfFilterTypeAndIndexExists( 'browserfilter', browser.id) } />
	      	);
	    });

	    var operatingSystemComponents = this.state.operatingSystems.map( function( os, i ) {

	    	return (

	    		<OperatingSystemFilter key={i} checked={FiltersStore.checkIfFilterTypeAndIndexExists( 'osfilter', os.id )} osData={os} />
	    	);
	    });

		return(

			<div className="deviceFilters">

				<div className="row">
					<div className="col-md-12">
						<br /><h3>Device Type</h3><br />

					</div>
				</div>

				<div className="row">

					{deviceTypes}
				</div>

				<div className="row">
					<div className="col-md-12">
						<h3>Browser</h3><br />

					</div>
				</div>

				<div className="row">
					{browserComponents}

				</div>

				<div className="row">
					<div className="col-md-12">
						<h3>Operating System</h3><br />

					</div>
				</div>

				<div className="row">

					{operatingSystemComponents}
				</div>

			</div>
		)
	}
});

export default  DeviceFilters;