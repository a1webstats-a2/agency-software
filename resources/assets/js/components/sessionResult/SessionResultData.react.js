import React from 'react';
import moment from 'moment';

function getCurrentState(props) {
    return {
        data: props.data,
        settings: props.settings,
        numberOfPages: props.numberOfPages,
        referrer: props.referrer,
        parentID: props.parentID,
        sessionDuration: props.sessionDuration
    }
}

const styles = {
    standardTable: {
        td: {
            width: '40%'
        }
    }
};

const SessionResultData = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.data.id !== nextProps.data.id) {
            return true;
        }

        return this.props.settings !== nextProps.settings;
    },
    render: function () {
        let displayDate = moment(this.state.data.created_at).format("Do MMMM YYYY");

        switch (this.state.settings.user.preferred_date_format) {
            case "dd/mm/yyyy" :
                displayDate = moment(this.state.data.created_at).format("DD/MM/YYYY");

                break;
            case "mm/dd/yyyy" :
                displayDate = moment(this.state.data.created_at).format("MM/DD/YYYY");

                break;
            case "yyyy-mm-dd" :
                displayDate = moment(this.state.data.created_at).format("YYYY-MM-DD");

                break;
        }

        var referrerLink = (
            <a href={this.state.referrer} target="_blank">{this.state.referrer}</a>
        )

        let lastVisit = 1 === this.state.data.is_a_new_visitor
            ? 'New Visitor'
            : 'Unknown'

        if (this.state.data.last_visit) {
            var rawLastVisit = this.state.data.last_visit;
            lastVisit = moment(rawLastVisit).format("Do MMMM YYYY");

            switch (this.state.settings.user.preferred_date_format) {
                case "dd/mm/yyyy" :
                    lastVisit = moment(rawLastVisit).format("DD/MM/YYYY");

                    break;
                case "mm/dd/yyyy" :
                    lastVisit = moment(rawLastVisit).format("MM/DD/YYYY");

                    break;
                case "yyyy-mm-dd" :
                    lastVisit = moment(rawLastVisit).format("YYYY-MM-DD");

                    break;
            }
        }

        var gclidData = null;

        if (this.state.settings.team.display_gclid) {
            gclidData = (
                <tr>
                    <td style={styles.standardTable.td}>
                        <strong>GCLID:</strong>
                    </td>
                    <td>
                        {this.state.data.gclid}
                    </td>
                </tr>
            )
        }

        return (

            <div>
                <div className="row">
                    <div className="col-md-12">
                        <table className="table">
                            <tbody>
                            <tr>
                                <td style={styles.standardTable.td}>
                                    <strong>Date:</strong>
                                </td>

                                <td>{displayDate}</td>
                            </tr>

                            <tr>
                                <td style={styles.standardTable.td}>
                                    <strong>Last Visit:</strong>
                                </td>

                                <td>{lastVisit}</td>
                            </tr>

                            <tr>

                                <td style={styles.standardTable.td}>
                                    <strong>Duration:</strong>
                                </td>

                                <td>{this.state.sessionDuration}</td>

                            </tr>

                            <tr>
                                <td style={styles.standardTable.td}>
                                    <strong>Keywords:</strong>
                                </td>
                                <td>{this.state.data.keyword}</td>
                            </tr>

                            <tr>
                                <td style={styles.standardTable.td}>
                                    <strong>Referrer:</strong>
                                </td>
                                <td>
                                    {referrerLink}

                                </td>
                            </tr>
                            {gclidData}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
});

export default SessionResultData;
