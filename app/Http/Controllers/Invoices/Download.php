<?php

namespace App\Http\Controllers\Invoices;

use App\Http\Controllers\API\Proxy;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Download extends Controller
{
    private function startPDF($contents)
    {
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=invoice_download.pdf");
        header('Pragma: no-cache');
        echo $contents;
    }

    private function startCSV($contents, $filename)
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Pragma: no-cache');
        exit($contents);
    }

    public function show(Request $request)
    {
        $proxy = new Proxy();

        $contents = $proxy->run($request);

        if ($contents) {
            $contents = $contents->getContent();
            $this->startPDF($contents);
        }
    }
}
