'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

require('./themeFiles/colors');

require('./themeFiles/colorManipulator');

var _spacing = require('./themeFiles/spacing');

_interopRequireDefault(_spacing);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
}

exports.default = {
    palette: {
        primary1Color: '#2d1467',
        accent1Color: '#2d1467'
    },
    icon: {
        color: '#2d1467'
    },
    chip: {
        textColor: '#fff',
        backgroundColor: '#2d1467',
        color: '#fff'
    },
    radioButton: {}
};
