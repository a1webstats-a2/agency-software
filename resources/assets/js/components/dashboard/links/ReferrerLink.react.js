import React from 'react';
import FilterActions from '../../../actions/FilterActions';

function getCurrentState( props ) {

 	return {

 		refId 		: 	props.refId,
 		refName 	: 	props.refName
  	}
}

var ReferrerLink = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.refId !== nextProps.refId ) {

			return true;
		}

		if( this.props.refName !== nextProps.refName ) {

			return true;
		}

		return false;
	},

	render : function() {

		return (

			<a href="#" onClick={this._createFilter}>{this.state.refName}</a>
		);		
	},

	_createFilter : function( event ) {

		event.preventDefault();

		var newState  = {

   			type          :   'customFilterReferrer', 
   			storedValue   :   {

   				criteria 	: 	'include',
   				value 		: 	this.state.refId, 
   				textValue 	: 	this.state.refName
   			},
            id              :   this.state.refId

   		}


   		FilterActions.quickDashboardLink( newState );
	}
});

export default  ReferrerLink;