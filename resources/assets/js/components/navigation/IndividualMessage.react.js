import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import Avatar from 'material-ui/Avatar';

function getCurrentState( props ) {

	return {

		viewNotificationID 	: props.viewNotificationID,
		notifications 		: props.notifications,
		settings 			: props.settings
	}
}

function twoDigits( d ) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}


function ordinal_suffix_of( i ) {
    
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

var IndividualMessage = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.settings.user.preferred_date_format !== nextProps.settings.user.preferred_date_format ) {

			return true;
		}

		if( this.props.viewNotificationID !== nextProps.viewNotificationID ) {

			return true;
		}

		return false;
	},

	render : function() {

		var messages = this.state.notifications.old.concat( this.state.notifications.new );

		var thisNotification = null;

		messages.map( function( notification, i ) {

			if( notification.notification_id === this.state.viewNotificationID ) {

				thisNotification = notification;
			}

		}.bind( this ) )

		var message = "";

		if( thisNotification ) {

			var monthNames      =   [  "January", "February", "March", "April", "May", "June",
	          "July", "August", "September", "October", "November", "December"
	        ];

	        var rawDate 	= new Date( thisNotification.notification_date );

	        var displayDate = ordinal_suffix_of( rawDate.getDate() ) + ' ' + monthNames[rawDate.getMonth()] + ' ' + rawDate.getFullYear() + '  ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits(rawDate.getMinutes() );


	        switch( this.state.settings.user.preferred_date_format ) {

	            case "dd/mm/yyyy" :

	                displayDate = twoDigits( rawDate.getDate() ) + '/' + twoDigits( rawDate.getMonth() ) + '/' + rawDate.getFullYear() + '  ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );

	                break;

	            case "mm/dd/yyyy" :

	                displayDate = twoDigits( rawDate.getMonth() ) + '/' + twoDigits( rawDate.getDate() ) + '/' + rawDate.getFullYear() + '  ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );

	                break;

	            case "yyyy-mm-dd" :

	                displayDate = rawDate.getFullYear() + '-' + twoDigits( rawDate.getMonth() ) + '-' + twoDigits( rawDate.getDate() ) + '  ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );
	    
	                break;

	            case "fulltext" :

	                displayDate = ordinal_suffix_of( rawDate.getDate() ) + ' ' + monthNames[rawDate.getMonth()] + ' ' + rawDate.getFullYear() + '  ' + twoDigits( rawDate.getHours() ) + ':' + twoDigits( rawDate.getMinutes() );

	                break;

	        }

			var avatarSrc 	= 	"/api/proxy/api/v1/user/avatar/" + thisNotification.created_by_end_user_id;

			var currentDate = 	new Date();

			var timeDiff 	= Math.abs( currentDate.getTime() - rawDate.getTime());
			var diffDays 	= Math.ceil( timeDiff / ( 1000 * 3600 * 24 ) );

			var downloadLink = "";

			if( diffDays < 14 && thisNotification.asset_id ) {

				var href = "/internal/download/contents/" + thisNotification.asset_id;

				downloadLink = (

					<div>
						<a href={href}>Download</a>
					</div>
				)
			
			} else if( thisNotification.asset_id ) {

				downloadLink = (

					<div>

						<p>Sorry, this download link has expired</p>
					</div>
				)
			}

			message = (

				<div>

					<div className="individualMessageFrom">

						<Avatar src={avatarSrc} style={{ marginRight : 30 }} />{thisNotification.forename} {thisNotification.surname}
					</div>

					<div className="individualMessageDate">

						{displayDate}
					</div>

					<div className="individualMessageContent">

						{thisNotification.message}

						<div className="clr"></div>

						<br />

						<p style={{ color : '#170550' }}>Downloads are available for 2 weeks after their creation</p>

						<div className="clr"></div>

						<br />

						{downloadLink}

					</div>

				</div>
			)
		}

		return (

			<div className="individualMessage">

				<div className="row">

					<div className="col-md-12">

						<div className="row">

							<h3>Message</h3>

						</div>

						<div className="row">

							{message}

						</div>
					</div>
				</div>
			</div>

		)
	}
});

export default  IndividualMessage;
