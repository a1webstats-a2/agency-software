var keyMirror = require( 'keyMirror' );

module.exports = keyMirror({
	UPDATE_END_USER_COMPANY_RECORD 		: 	null,
	TRACKER_UPDATE_CLIENT_RELATIONSHIP 	: 	null,
	SET_POPOVER 						: 	null,
	SET_ASSIGNED_TO_OPTION 				: 	null,
	TRACKED_SAVE_ASSIGNED_TO 			: 	null,
	SET_ASSIGNED_TEAM_MEMBER_ID 		: 	null,
	SET_TRACKED_OPTIONS 				: 	null,
	LOAD_TRACKED_COMPANIES				: 	null,
	SET_TRACKED_ORGANISATIONS 			: 	null,
	STORE_TRACKED_ORGANISATIONS 		: 	null
});