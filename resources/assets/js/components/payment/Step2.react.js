import React from 'react';
import {Link} from 'react-router'

function getCurrentState(props) {
    return {
        pricing: props.pricing
    }
}

const Step2 = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.pricing !== nextProps.pricing) {
            return true;
        }

        return false;
    },
    render: function () {
        return (
            <div>
                <div className="stepText flex space-x-6">
                    <span>Step</span><img src="/images/two.png"/><span>of 3</span>
                </div>

                <div className="clr"></div>
                <br/>

                <h4>We offer two solutions for payment, please choose your preferred option:</h4>
                <div className="clr"></div>
                <br/><br/>

                <div className="row">

                    <div className="col-md-6">
                        <Link to="/payment/gocardless">
                            <div className="paymentOption">
                                <div className="row">
                                    <div className="col-md-3">
                                        <img id="paymentDDImage" src="/images/direct-debit.png"/>
                                    </div>
                                    <div className="col-md-9">
                                        <img src="/images/gc.png"/>
                                    </div>
                                </div>

                                <p>GoCardless allows you to create a subscription using your sort code and
                                    account number.</p>
                            </div>
                        </Link>
                    </div>
                    <div className="col-md-6">
                        <Link to="/payment/stripe">
                            <div className="paymentOption">
                                <div className="row">
                                    <div className="col-md-3">
                                        <img id="paymentCCImage" src="/images/credit-card.png"/>
                                    </div>
                                    <div className="col-md-9">
                                        <img src="/images/stripe.png"/>
                                    </div>
                                </div>

                                <p>Stripe allows you to make a payment using your debit / credit card</p>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
});

export default Step2;
