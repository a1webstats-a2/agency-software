import React from 'react';
import FilterActions from '../../actions/FilterActions.js';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const styles = {
    refreshContainer: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    }
};

function getCurrentState(props) {
    return {
        isSearchingForPossibleOptions: props.isSearchingForPossibleOptions,
        chosenPossibleOption: props.chosenPossibleOption,
        possibleOptions: props.possibleOptions,
        storeType: props.storeType,
        activeRecord: props.activeRecord,
        tel: props.tel,
        name: props.name,
        website: props.website,
        readOnly: props.readOnly,
        companyId: props.companyId,
        linkedIn: props.linkedIn
    }
}

const Breakdown = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    shouldComponentUpdate: function (nextProps) {
    	if (this.props.tel !== nextProps.tel) {
            return true;
        }

        if (this.props.isSearchingForPossibleOptions !== nextProps.isSearchingForPossibleOptions) {
            return true;
        }

        if (this.props.website !== nextProps.website) {
            return true;
        }

        if (this.props.chosenPossibleOption !== nextProps.chosenPossibleOption) {
            return true;
        }

        if (this.props.possibleOptions !== nextProps.possibleOptions) {
            return true;
        }

        if (this.props.name !== nextProps.name) {
            return true;
        }

        if (JSON.stringify(this.props.activeRecord) !== JSON.stringify(nextProps.activeRecord)) {
            return true;
        }

        return false;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    changeCompanyName: function (event) {
        if (this.state.readOnly) {
            return false;
        }

        let activeRecord = JSON.parse(JSON.stringify(this.state.activeRecord));
        activeRecord.data.use_organisation_name = event.target.value;

        FilterActions.updateActiveRecord(activeRecord);
    },
    changeCompanyTel: function (event) {
        if (this.state.readOnly) {
            return false;
        }

        let activeRecord = JSON.parse(JSON.stringify(this.state.activeRecord));
        activeRecord.data.use_organisation_tel = event.target.value;

        FilterActions.updateActiveRecord(activeRecord);
    },

    changeCompanyWebsite: function (event) {
        if (this.state.readOnly) {
            return false;
        }

        var activeRecord = JSON.parse(JSON.stringify(this.state.activeRecord));
        activeRecord.data.use_organisation_website = event.target.value;

        FilterActions.updateActiveRecord(activeRecord);
    },
    render: function () {
        let possibleOptions = [];
        let listItems = [];

        if (typeof this.state.possibleOptions !== 'undefined' && this.state.possibleOptions.length > 0) {
            listItems = this.state.possibleOptions.map(function (option, po) {
                return <RadioButton
                    label={option.name}
                    value={option.id}
                    key={po}
                />
            }.bind(this));

            possibleOptions = (
                <div style={{marginLeft: 15}}>
                    <div className="clr"></div>
                    <br/>
                    <h4>We've found these possible matches, is it one of these?</h4>
                    <br/>
                    <RadioButtonGroup
                        name="chosenPossibleOption"
                        onChange={this._setChosenPossibleOption}
                        valueSelected={this.state.chosenPossibleOption}>
                        <RadioButton
                            value={-1}
                            label="No, create a new record"
                        />
                        {listItems}
                    </RadioButtonGroup>
                </div>
            )
        }

        let searchingForOptions = '';

        if (this.state.isSearchingForPossibleOptions) {
            searchingForOptions = (
                <div className="row" style={{marginTop: 15, float: 'left'}}>
                    <div className="col-md-2">
                        <div style={styles.refreshContainer}>
                            <RefreshIndicator
                                size={40}
                                left={10}
                                top={0}
                                status="loading"
                                style={styles.refresh}
                            />
                        </div>
                    </div>
                    <div className="col-md-10">
                        <p style={{marginTop: 8, marginLeft: 15}}>Searching for existing organisations</p>
                    </div>
                </div>
            )
        }

        return (
            <div className="companyBreakdown">
                <form>
                    <div className="row">
                        <div className="col-md-12">
                            <TextField
                                floatingLabelText="Organisation Name"
                                hintText="Organisation Name"
                                value={this.state.name}
                                floatingLabelFixed={true}
                                name="organisationName"
                                fullWidth={true}
                                onChange={this.changeCompanyName}
                            />
                        </div>
                        {searchingForOptions}
                        {possibleOptions}
                    </div>

                    <div className="clr"></div>
                    <br/>

                    <div className="row">
                        <div className="col-md-12">
                            <TextField
                                floatingLabelText="Tel"
                                hintText="Tel"
                                floatingLabelFixed={true}
                                name="organisationTel"
                                fullWidth={true}
                                value={this.state.tel}
                                onChange={this.changeCompanyTel}
                            />
                        </div>
                    </div>

                    <div className="clr"></div>
                    <br/>

                    <div className="row">
                        <div className="col-md-12">
                            <TextField
                                floatingLabelText="Website"
                                hintText="Website"
                                floatingLabelFixed={true}
                                fullWidth={true}
                                name="organisationWebsite"
                                value={this.state.website}
                                onChange={this.changeCompanyWebsite}
                            />
                        </div>
                    </div>
                </form>
            </div>
        );
    },
    _setChosenPossibleOption: function (event, value) {
        FilterActions.setPossibleOption(value);
    }
});

export default Breakdown;
