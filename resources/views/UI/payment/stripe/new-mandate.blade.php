@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>

@section( 'content' )


	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm" class="signupForm">
				<h1>Success!</h1>

				<p>Thank you for creating a subscription with {{ $configData['name'] }}.</p><br />
				<p>Would you like to return to the <a href="/">dashboard?</a></p>
			</div>

		</div>
	</div>

@endsection