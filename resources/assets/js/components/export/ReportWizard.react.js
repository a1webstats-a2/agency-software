import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import ExportOptions from '../ExportOptions.react';
import CreateReportTemplate from  '../CreateReportTemplate.react';
import DrawerToolbar from '../DrawerToolbar.react';
import SavedReports from './SavedReports.react';
import ChartIcon from 'material-ui/svg-icons/editor/show-chart';
import FileDownload from 'material-ui/svg-icons/file/file-download';
import SavedIcon from 'material-ui/svg-icons/content/save';
import FiltersFancyDisplay from '../FiltersFancyDisplay.react';

function getCurrentProps( props ) {

	var newState = {

		drawerOpen		: props.drawerOpen,
		filters 		: props.exportOptions.filters,
		settings 		: props.settings,
		exportOptions 	: props.exportOptions
	};

	if( props.exportOptions.quickOpen.type === 'reportWizard' ) {

		newState.activeTab = props.exportOptions.quickOpen.tab;
	}

	return newState;
}

var ReportWizard = React.createClass({

	getInitialState : function() {


		var initialState 		= getCurrentProps( this.props );
		initialState.activeTab 	= 0;

		return initialState;
	},

	shouldComponentUpdate : function( nextProps, nextState ) {
		
		if( !nextProps.drawerOpen ) {

			return false;
		}

		if( !nextProps.drawerOpen && nextProps.exportOptions.quickOpen.type !== "reportWizard" ) {

			return false;
		}

		return true;
	},

	setActiveTab : function( activeTab ) {

		this.setState({

			activeTab 	: 	activeTab.props.value 
		});
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentProps( newProps ) );
	},

	render : function() {

		var initialSelectedIndex = 0;

		

		return (

			<div>


				<Tabs value={this.state.activeTab}>
					<Tab onActive={this.setActiveTab} value={0} label="Saved Templates" icon={<SavedIcon />}>

						<SavedReports settings={this.state.settings} exportOptions={this.state.exportOptions} />
					</Tab>
					<Tab onActive={this.setActiveTab} value={1} label="Export" icon={<FileDownload />} >
						<ExportOptions settings={this.state.settings} exportOptions={this.state.exportOptions} />
					</Tab>
					<Tab onActive={this.setActiveTab} value={2} label="Create Report" icon={<ChartIcon />} >
						<CreateReportTemplate settings={this.state.settings} exportOptions={this.state.exportOptions}  />
					</Tab>

					
				</Tabs>


			</div>

		)
	}
})

export default  ReportWizard;