import React from 'react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import Person from 'material-ui/svg-icons/social/person';
import Checkbox from 'material-ui/Checkbox';


String.prototype.capitalize = function() {

    return this.charAt(0).toUpperCase() + this.slice(1);
}

const styles = {
 
  	checkbox: {
   	 	
   	 	marginBottom: 16,
  	}
};


function getCurrentState( props ) {

	var searchByData = props.searchByData;

	return {

		isChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'customFilterReferrerCountry', props.country.id ),
		country 			: 	props.country.name,
		rowCount 			: 	props.country.row_count,
		textValue 			: 	props.country.name,
		countryId 			: 	props.country.id,
		filtersBeingEdited 	: 	props.filtersBeingEdited,
		type 				: 	'customFilterReferrerCountry',
		searchCheckbox 		: 	props.searchByCheckbox,
		includeType 		: 	props.include,
		countryRaw 			: 	props.country,
		inclusionType 		: 	searchByData.countryIncludeType

	}
}

var SearchByCountryCheckbox = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.searchByData.countryIncludeType !== nextProps.searchByData.countryIncludeType ) {

			return true;
		}

		if( this.props.searchByData.inclusionType !== nextProps.searchByData.inclusionType ) {

			return true;
		}

		if( this.props.filtersBeingEdited !== nextProps.filtersBeingEdited ) {

			return true;
		}

		if( this.props.rowCount !== nextProps.country.row_count ) {

			return true;
		}

		if( this.state.isChecked !== FiltersStore.checkIfFilterTypeAndIndexExists( 'customFilterReferrerCountry', this.props.country.id ) ) {

			return true;
		} 
			
		if( this.props.includeType !== nextProps.includeType ) {

			return true;
		}

		if( this.props.country.name !== nextProps.country.name ) {

			return true;
		}

		return false;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render 	: function() {

		var countField 		=	"include_count";

		if( this.state.includeType === "exclude" ) {

			countField = "exclude_count";

		}

		if( this.state.countryRaw[countField] === 0 ) {

			return ( <div></div> );
		}

		var resCountClass 	= 	( this.state.filtersBeingEdited ) ? 'greyedOutResultCount' : 'resultCount';

		var label 		= 	(

			<div>

		    	{this.state.country}

		    	<span className={resCountClass}>{this.state.countryRaw[countField]} visitor(s)</span>
			</div>
		)

		return(

			<Checkbox
				label={label}
				checked={this.state.isChecked}
				style={styles.checkbox}
				onCheck={this._saveCheckedState}
		    />
		)
	},

	_saveCheckedState : function() {

		var isChecked = ( this.state.isChecked ) ? false : true;

		this.setState({

			isChecked : isChecked
		})

		if( isChecked ) {

			var newState  = {

	   			type          :   this.state.type, 
	   			storedValue   :   {

	   				criteria 	: 	this.state.includeType,
	   				value 		: 	this.state.countryId, 
	   				textValue 	: 	this.state.textValue,
	   				searchBox 	: 	this.state.searchCheckbox,
	   				id 			: 	this.state.countryId 
	   			},
	            id              :   this.state.countryId

	   		}

	   		FilterActions.createFilter( newState );

	   	} else {

	   		FilterActions.removeFilterByType({

	   			type 	: 	this.state.type,
	   			id 		: 	this.state.countryId
	   		});

	   	}
	}
})

export default  SearchByCountryCheckbox;