import React from 'react';
import FiltersStore 	from '../../stores/FiltersStore';
import FilterActions 	from '../../actions/FilterActions';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';


function getCurrentState( props ) {

	return {
			
		tracked 			: 	props.tracked,
		currentTrackingObj 	: 	props.assignedData.currentTrackingObj,
		open 				: 	props.assignedData.open,
		teamMembers 		: 	props.assignedData.teamMembers,
		storeType 			: 	props.storeType
	}
} 

const styles = {

  
  	radioButton : {

  		marginTop 		: 10,
  		marginBottom	: 10
  	}
};

var AssignToTeamMember = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.assignedData.open !== nextProps.assignedData.open ) {

			return true;
		}

		if( this.props.assignedData.currentTrackingObj !== nextProps.assignedData.currentTrackingObj ) {

			return true;
		}

		if( this.props.assignedData.teamMembers !== nextProps.assignedData.teamMembers ) {

			return true;
		}

		return false;
	},

	handleClose : function() {

		this._handleRequestClose();
	},

	render : function() {

		var actions = [

			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this.handleClose}
			/>,
		
			<FlatButton
				label="Confirm"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this._assignToTeamMember}
			/>

		];

		var assignedTo 	=	"teamMember-1";


		if( this.state.currentTrackingObj ) {

			if( this.state.currentTrackingObj.assigned_user  ) {

				assignedTo = "teamMember" + this.state.currentTrackingObj.assigned_user;
			
			} else {

				if( 
					typeof this.state.currentTrackingObj !== "undefined" &&
					typeof this.state.tracked !== "undefined" &&
					typeof this.state.tracked[this.state.currentTrackingObj.organisationid] !== "undefined" ) {

					assignedTo = "teamMember" + this.state.tracked[this.state.currentTrackingObj.organisationid];
				}
			}
		}

		var teamMemberRadioButtons = this.state.teamMembers.map( function( teamMember, i ) {

			var val 	= 'teamMember' + teamMember.id;

			var label 	= (

				<div>
					{teamMember.forename} {teamMember.surname}
				</div>
			)

			return (

				<RadioButton 
					style={styles.radioButton}
					key={i}
					value={val}
					label={label}
				/>

			)
		});

		return (

			<div>

				<Dialog
					title="Assign to Team Member"
					actions={actions}
					modal={false}
					open={this.state.open}
					onRequestClose={this.handleClose}
				>
					<RadioButtonGroup name="assignedTo" onChange={this._setTeamMember} valueSelected={assignedTo}>

						<RadioButton style={styles.radioButton} key={-1} value="teamMember-1" label="Don't Track" />

						<RadioButton style={styles.radioButton} key={-2} value="teamMember-2" label="Track - Unassigned" />

						{teamMemberRadioButtons}

					</RadioButtonGroup>

				</Dialog>

			</div>

		)
	},

	_setTeamMember : function( event, value ) {

		FilterActions.setCurrentTrackedObjAssignedToTeamMember({

			teamMemberID 	: value.replace( 'teamMember', '' ), 
			storeType 		: this.state.storeType

		});
	},

	_assignToTeamMember : function() {

		switch( this.state.storeType ) {

			case "reportBuilder" :

				FilterActions.saveAssignedTo();

				break;

			case "organisationsDashboard" :

				FilterActions.saveAssignedToForOrganisationsDashboard();

				break;
		}

	},

	_handleRequestClose : function() {

		FilterActions.setCurrentTrackedObjAssignedToTeamMember({

			teamMemberID 	: -1, 
			storeType 		: this.state.storeType

		});

		FilterActions.closeLightbox();
	}
});

export default  AssignToTeamMember;