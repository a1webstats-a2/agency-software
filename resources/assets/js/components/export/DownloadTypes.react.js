import React from 'react';
import FilterActions from '../../actions/FilterActions';
import Checkbox from 'material-ui/Checkbox';

const styles = {

    checkbox: {
        marginBottom: 16
    }
};

const DownloadTypes = React.createClass({

    getInitialState: function () {
        return {
            csvChecked: this.props.csvChecked,
            pdfChecked: this.props.pdfChecked,
            excelChecked: this.props.excelChecked
        }
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.csvChecked !== nextProps.csvChecked) {
            return true;
        }

        if (this.props.pdfChecked !== nextProps.pdfChecked) {
            return true;
        }

        if (this.props.excelChecked !== nextProps.excelChecked) {
            return true;
        }

        return false;
    },

    componentWillReceiveProps: function (newProps) {
        this.setState({
            csvChecked: newProps.csvChecked,
            pdfChecked: newProps.pdfChecked,
            excelChecked: newProps.excelChecked

        })
    },


    render: function () {

        var pdfImg = (<img src="/ui-images/pdf.png"/>);
        var csvImg = (<img src="/ui-images/csv.png"/>);
        var excelImg = (<img src="/ui-images/excel.png" />);


        return (

            <div className="downloadImages">


                <div className="row">

                    <div className="col-md-3">

                        <div className="downloadImg downloadCSV">
                            <Checkbox
                                label={csvImg}
                                style={styles.checkbox}
                                checked={this.state.csvChecked}
                                value="CSV"
                                onCheck={this._addRemoveExportType}
                            />
                        </div>

                        {/*<div className="downloadImg downloadExcel">*/}
                            {/*<Checkbox*/}
                                {/*label={excelImg}*/}
                                {/*style={styles.checkbox}*/}
                                {/*checked={this.state.excelChecked}*/}
                                {/*value="Excel"*/}
                                {/*onCheck={this._addRemoveExportType}*/}
                            {/*/>*/}
                        {/*</div>*/}
                    </div>
                </div>
            </div>
        );
    },

    _addRemoveExportType: function (event, inclusionType) {
        let type = event.target.value;

        FilterActions.addRemoveExportType({
            type: type,
            value: inclusionType
        })
    }
});

export default DownloadTypes;