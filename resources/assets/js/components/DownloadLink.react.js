import React from 'react';

const DownloadLink = React.createClass({
	getInitialState : 	function() {
		return {
			link 	: this.props.link,
			message : this.props.message
		}
	},
	render 	: function() {
		return (
			<div>
				<p>{this.state.message} : <a className="text-white" href={this.state.link}>Download</a></p>
			</div>
		);
	}
});

export default  DownloadLink;
