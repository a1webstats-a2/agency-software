@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>


@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm" class="signupForm">

				<div id="loginLogo">
                    <img src="/images/{{ $loginLogo }}" />
                </div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<div class="col-md-6">

						<ul class="progress-indicator">
				            <li class="completed">
				                <span class="bubble"></span>
				                Step 1
				            </li>
				            <li>
				                <span class="bubble"></span>
				                Step 2
				            </li>
				            <li>
				            	<span class="bubble"></span>
				            	Finished!
				            </li>
				        	
				        </ul>

				    </div>
				</div>

				<h1>Sign Up</h1>

				@if ( count( $errors ) > 0 )
					<div class="alert alert-danger">
					    <ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
				@endif

				<h2>Here's how it works...</h2>

				<div class="row">

					<div class="row">

						<div class="col-md-11">
							<div class="signupBullet" id="signupBullet1">Please complete the signup details below. Your details are used purely to sign up for your free 30 day trial and no payment details are needed.</div>
							<div class="clr"></div>
							<div class="signupBullet" id="signupBullet2">After signup, you will be provided with a javascript tracker code that you can add to your website pages.  This is typically a 5 minute job by your web developer.  If you need help with this, please contact us on <a href="mailto:{{ $configData['email'] }}">{{ $configData['email'] }}</a> and we’ll be happy to help.</div>
							<div class="clr"></div>
							<div class="signupBullet" id="signupBullet3">As soon as your tracking code is installed, you will be on day 1 of your 30 day trial.  If it takes a few days to get the tracking code installed, you won’t lose any of the 30 days.</div>
							<div class="clr"></div>
							<div class="signupBullet" id="signupBullet4">During your trial you will receive emails of data related to your website visitors, plus useful information that will help you to gain more success from your website.  Please whitelist our <a href="mailto:{{ $configData['email'] }}">{{ $configData['email'] }}</a> email address to ensure that you receive emails from us.
 </div>
							<div class="clr"></div>
							<div class="signupBullet" id="signupBullet5">We would also encourage you to have a phone conversation with us during your trial – 9 times out of 10 our trial subscribers gain highly useful information from those phone calls.
 </div>
							<div class="clr"></div>
							<div class="signupBullet" id="signupBullet6">At no point do we sales pitch our service – we treat your trial as purely the opportunity to help you gain more from your website visitors data.</div>
							<div class="clr"></div>
							<div class="signupBullet" id="signupBullet7">If you find that {{ $configData['name'] }} matches your business requirements then you have the option to continue your subscription after the free 30 days. There is no contract period and you can cancel at any time.</div>
							

							<br />
							<p>If you have any questions at all then please don’t hesitate to <a href="mailto:{{ $configData['email'] }}" target="_blank">contact us</a> – we’re here to help.</p>
						</div>

					</div>

				</div>

				<form method="POST">

					<div class="row">

						<div class="col-md-6">

							<h2>Your Details</h2>

							<div class="form-group">

								<label >First Name <span>*</span></label>

								<input type="text" class="form-control" value="{{ old( 'forename' ) }}" name="forename" />

							</div>

							<div class="form-group">

								<label >Last Name <span>*</span></label>

								<input type="text" name="surname" value="{{ old( 'surname' ) }}" class="form-control" />

							</div>

							<div class="form-group">

								<label >Best Contact Number <span>*</span></label>

								<input type="text" name="tel" value="{{ old( 'tel' ) }}" class="form-control" />

							</div>


							<div class="form-group">

								<label >Email Address <span>*</span></label>

								<input type="text" name="email" value="{{ old( 'email' ) }}" class="form-control" />

							</div>

							<div class="form-group">

								<label>Username<span>*</span></label>

								<input type="text" name="username" value="{{ old( 'username' ) }}" class="form-control" />

							</div>

							<div class="form-group">

								<label>Password (min 12 characters)<span>*</span></label>

								<input type="password" name="password" class="form-control" />

							</div>

							<div class="form-group">

								<label>Repeat Password (min 12 characters)<span>*</span></label>

								<input type="password" name="repeatPassword" class="form-control" />

							</div>
						</div>

						<div class="col-md-6">

							<h2>Your Company's Details</h2>

							<div class="form-group">

								<label >Company Name <span>*</span></label>

								<input type="text" name="companyName" value="{{ old( 'companyName' ) }}" class="form-control" />

							</div>

							<div class="form-group">

								<label >Website Address <span>*</span></label>

								<input type="text" name="website" value="{{ old( 'website' ) }}" class="form-control" />

							</div>

							<div class="form-group">

								<label>Timezone <span>*</span></label>

								<select class="form-control" name="timezone">

									@foreach( $timezones as $tz ) 

										<?php 

										$selected = "";

										if( $tz === "Europe/London" ) {

											$selected = " selected=\"selected\" ";
										}

										?>

										<option {{ $selected }} value="{{ $tz }}">{{ $tz }}</option>

									@endforeach 


								</select>

							</div>

							<div class="form-group">

								<label>How did you hear about {{ $configData['name'] }}?</label>

								<select class="form-control" name="hearAboutA1">
									<option selected="selected" value="">Please Select</option>
									<option value="Bartercard">Bartercard</option>
									<option value="Bing">Bing</option>
									<option value="Blog">Blog</option>
									<option value="Email From {{ $configData['name'] }}">Email From {{ $configData['name'] }}</option>
									<option value="Facebook">Facebook</option>
									<option value="Forum">Forum</option>
									<option value="Google Search">Google Search</option>
									<option value="LinkedIn">LinkedIn</option>
									<option value="Online Directory">Online Directory</option>
									<option value="Recommendation">Recommendation</option>
									<option value="Trialling a Competitor">Trialling a Competitor</option>
									<option value="Twitter">Twitter</option>
									<option value="Yahoo">Yahoo</option>
									<option value="Other">Other</option>

								</select>

							</div>

							<div class="form-group">

								<label>

									<input type="checkbox" name="isABusiness" />
										Are you a business?
								</label>
							</div>

							<div class="form-group"></div><br />

							<div class="form-group">

								<label>VAT number (if applicable)</label>

								<input type="text" name="vatNumber" value="{{ old( 'website' ) }}" class="form-control" />

							</div>

							<div class="form-group">

								<div class="form-group">
									<label>
										<input name="termsAndConditions" type="checkbox">I agree with the <a href="/terms-and-conditions" target="_blank">
											terms and conditions</a>
									</label>
								</div>
							</div>
						</div>
					</div>

					<hr />

				


					<div class="row">

						<div class="col-md-6">
							<div id="captchaContainer">
								<div class="g-recaptcha" data-sitekey="6Lf_Yw4UAAAAAOGe4csyGYJxqgRh5Y4EH5e1cXR-"></div> 
							</div>
						</div>

						<div class="col-md-6">

							<div class="form-group">

								<button type="submit" class="signupButton">Sign Up</button>

							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

@endsection