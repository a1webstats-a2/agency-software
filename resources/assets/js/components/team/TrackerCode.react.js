import React from 'react';

function getCurrentState(props) {
  return props;
}

class TrackerCode extends React.Component {
  constructor(props) {
    super(props);

    this.state = getCurrentState(props);
  }

  componentWillReceiveProps(props) {
    this.setState(getCurrentState(props));
  }

  render() {
    const wordpressDownloadUrl = '/download-wordpress-plugin/' + this.props.clientId;

    return (
        <div className="row">
          <div className="col-md-12">
            <h3>Tracker Code</h3><br/>
            <div className="clr"></div>
            <br/>
            <pre>
              {this.state.trackerCode.sampleInScriptTags}
            </pre>
            <div className="clr"></div>
            <br/>

            <div style={{ marginTop: 30, marginBottom: 100 }}>
              <h4>Wordpress Plugin</h4>
              <div className="clr"></div><br />
              <a href={wordpressDownloadUrl} target="_blank">
                <img height={50} src="/images/wordpress.png"/><br /><br />
                Download
              </a>
            </div>
          </div>
        </div>
    )
  }
}

export default TrackerCode;