<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\API\Proxy;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Download extends Controller
{
    private function startPDF($contents, $filename)
    {
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=" . $filename);
        header('Pragma: no-cache');
        echo $contents;
    }

    private function startCSV($contents, $filename)
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Pragma: no-cache');
        exit($contents);
    }

    public function show(Request $request, $assetId)
    {
        $proxy = new Proxy();

        $contents = $proxy->run($request);

        if ($contents) {
            if ($contents->getStatusCode() === 500) {
                return response('Sorry, the resource could not be found', 404);
            }

            $contents = json_decode($contents->getContent());

            $this->startCSV($contents->contents, $contents->filename);
        }

        return false;
    }
}
