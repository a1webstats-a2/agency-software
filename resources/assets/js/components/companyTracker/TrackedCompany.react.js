import React from 'react';
import TrackedCompaniesStore from '../../stores/TrackedCompaniesStore';
import TrackedCompaniesActions from '../../actions/TrackedCompaniesActions';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import {Tabs, Tab} from 'material-ui/Tabs';
import Paper from 'material-ui/Paper';
import SvgIcon from 'material-ui/SvgIcon';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import AdditionalCompanyData   from '../companyData/AdditionalCompanyData.react';
import {List, ListItem} from 'material-ui/List';
import Business from 'material-ui/svg-icons/communication/business';
import Comment from 'material-ui/svg-icons/communication/comment';
import Checkbox from 'material-ui/Checkbox';
import FlagIcon from 'material-ui/svg-icons/content/flag';
import WebIcon from 'material-ui/svg-icons/social/public';
import PhoneIcon from 'material-ui/svg-icons/communication/phone';
import Popover from 'material-ui/Popover';
import EditRecordIcon from 'material-ui/svg-icons/content/create';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import Assessment from 'material-ui/svg-icons/action/assessment';
import A2GoogleMap from '../A2GoogleMap.react';
import ClientTypes from '../prospecting/ClientTypes.react';
import PindropIcon from 'material-ui/svg-icons/maps/pin-drop';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import BlockIcon from 'material-ui/svg-icons/content/block';
import HistoryIcon from 'material-ui/svg-icons/action/history';
import IntelligenceIcon from 'material-ui/svg-icons/action/visibility';
import TagIcon from 'material-ui/svg-icons/action/label-outline';


const styles = {
    
    checkbox    :   {

    },

    headline    :   {
        
        fontSize        : 24,
        paddingTop      : 16,
        marginBottom    : 12,
        fontWeight      : 400,
    },

    paper   :   {

        marginBottom    : 50,
        width           : '100%'
    },

    standardTable   :   {

        td  :   {

            width   :   100
        }
    },

    popover     :   {

        padding     :   20
    },

    buttons     :   {

        secondary : {

            margin  :   12
        },

        primary : {

            margin : 12
        }
    },

    svg     :   {

        color :     '#170550'
    }
};


function ordinal_suffix_of( i ) {
    
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function twoDigits( d ) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

function getCurrentState( props ) {

    var dueDilData = false;

    if( typeof props.data.a1 !== "undefined" ) {

        if( typeof props.data.a1.additionalData[0] !== "undefined" ) {

            dueDilData = props.data.a1.additionalData[0];
        }   
    }

	return {

        id              :   props.data.id,
        popover         :   TrackedCompaniesStore.getPopoverObj(),
        myKey           :   props.myKey,
        relationships   :   props.relationships,
        tags            :   props.tags,
		data 	        : 	props.data,
        main            :   props.data.a1,
        dueDilData      :   dueDilData,
        settings        :   props.settings
	}
}

var TrackedCompany = React.createClass({

	getInitialState : function() {

        var currentState = getCurrentState( this.props );

        currentState.map = null;

		return currentState;
	},

    shouldComponentUpdate : function( nextProps, nextState ) {

        if( JSON.stringify( this.props.relationships ) !== JSON.stringify( nextProps.relationships ) ) {

            return true;
        }

        if( this.props.tags !== nextProps.tags ) {

            return true;
        }

        if( JSON.stringify( this.props.data ) !== JSON.stringify( nextProps.data ) ) {

            return true;
        }

        if( this.props.dueDilData !== nextProps.dueDilData ) {

            return true;
        }

        return false;
    },

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

    openPopover : function( event ) {

        TrackedCompaniesActions.setPopover({

            open        :   true,
            anchorEl    :   event.currentTarget,
            id          :   this.state.id
        })
    },

    showHistory     :   function() {

        FilterActions.setPreviousHistory({

            currentSession  :   this.state.data.id,
            organisationId  :   this.state.data.organisationid
        });

        FilterActions.setLightbox({

            type    :   'previousVisits',
            id      :   1
        })
    },

    closePopover : function() {

        TrackedCompaniesActions.setPopover({

            open        :   false,
            anchorEl    :   null,
            id          :   null
        })
    },

    openEditCompanyRecord : function() {

        FilterActions.setActiveRecord({

            data    :   this.state.data,
            myKey   :   this.state.myKey
        });

        FilterActions.setLightbox({

            type    :   'editCompanyRecord',
            id      :   1
        })
    },

    openEditCompany : function() {

        FilterActions.setLightbox({

            type    :   'companyData',
            id      :   0
        });        
    },
   
	render : function() {

        var isTracked       =   ( this.state.data.is_tracked ) ? true : false;

        var subtitleText = (

            <div>
                <p>{this.state.data.use_organisation_website}</p>
                <p>{this.state.data.use_organisation_tel}</p>
            </div>
        )
        
        var linkedInHref = "http://www.linkedin.com/vsearch/p?company=" + this.state.data.use_organisation_name;

        var linkedInLink = (

            <a target="_blank" href={linkedInHref}>
                <img src="/images/linkedin.png" />
            </a>
        )

        var displayDate     =   "";

        var rawDate         =   new Date( this.state.data.last_visit );

        var monthNames      =   [  "January", "February", "March", "April", "May", "June",
                                    "July", "August", "September", "October", "November", "December"
                                ];

        
        switch( this.state.settings.preferred_date_format ) {

            case "dd/mm/yyyy" :

                displayDate = twoDigits( rawDate.getDate() ) + '/' + twoDigits( rawDate.getMonth() ) + '/' + rawDate.getFullYear();

                break;

            case "mm/dd/yyyy" :

                displayDate = twoDigits( rawDate.getMonth() ) + '/' + twoDigits( rawDate.getDate() ) + '/' + rawDate.getFullYear();

                break;

            case "yyyy-mm-dd" :

                displayDate = rawDate.getFullYear() + '-' + twoDigits( rawDate.getMonth() ) + '-' + twoDigits( rawDate.getDate() );
    
                break;

            case "fulltext" :

                displayDate = ordinal_suffix_of( rawDate.getDate() ) + ' ' + monthNames[rawDate.getMonth()] + ' ' + rawDate.getFullYear();

                break;
        }

        var content = (

            <div>
                <p><strong>Number of Visits</strong> {this.state.data.total_number_of_visits}</p>
                <p><strong>Last Visit</strong> {displayDate}</p>
            </div>
        )

		return (

		    <div className="sessionResult">
    
                <Paper style={styles.paper} zDepth={5}>

                    <div className="row" >

                        <div className="col-md-12">

                             <div className="customSessionResultContainer">

                                <div className="customSessionResultContainerHeader">

                                    <div className="row">
                                        <div className="col-md-11">


                                            <p className="organisationName">{this.state.data.use_organisation_name}</p>
                                            {subtitleText} 
                                    
                                        </div>

                                        <div className="col-md-1">
                                            {linkedInLink}
                                        </div>
                                        
                                    </div>

                                </div>

                                 <div className="customSessionResultContainerInner">
                                    <div className="row">
                                        <div className="col-md-7">
                                            {content}
                                        </div>

                                        <div className="col-md-5">

                                            <div className="row exportOptions">
                                                
                                                <div className="col-md-4">
                                                    <label>Track</label>
                                                    <input type="checkbox" checked={isTracked} onChange={this._trackCompany}  />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="customSessionResultContainerActions">
                                    <FlatButton onClick={this.openEditCompanyRecord} label="Edit" icon={<EditRecordIcon color="#170550" />} />
                                    <FlatButton onClick={this._createNote} label="Notes" icon={<Comment color={styles.svg.color}  />} />
                                    <FlatButton onClick={this._showHistory} label="History" icon={<HistoryIcon color={styles.svg.color} />} />
                                    <FlatButton onClick={this._openIntelligence} label="Intel" icon={<Business color={styles.svg.color} />}  />
                                    <FlatButton onClick={this._sendViaEmail} icon={<EmailIcon color={styles.svg.color} />} label="Email" />
                                    <FlatButton onClick={this._showTags} label="Tags" icon={<TagIcon color={styles.svg.color} />} />
                                </div> 
                            </div>
                        </div>
                    </div>
                </Paper>
			</div>

		)
	},

    _sendViaEmail : function() {

        FilterActions.sendOrganisationViaEmail( this.state.data.organisationid );
    },


    _showHistory : function() {

        FilterActions.setPreviousHistory({

            currentSession      :   1,
            organisationId      :   this.state.data.organisationid,
            organisationName    :   this.state.data.use_organisation_name
        });
    },  

    _trackCompany : function( event ) {

        TrackedCompaniesActions.setTrackedOptions( this.state.myKey );

        FilterActions.setLightbox({

            type    :   'assignToTeamMemberInCompanyTracker',
            id      :   1
        })
    },

    _showTags   : function() {

        var relationships = this.state.relationships[this.state.data.organisationid];

        FilterActions.openClientTypesLightbox({

            organisation_id     :   parseInt( this.state.data.organisationid ),
            relationships_json  :   JSON.stringify( relationships )
        });
    },

    _createNote : function() {

        FilterActions.openNotes( this.state.data.organisationid );

    },

    _openIntelligence : function() {
        
        FilterActions.showAdditionalCompanyData( this.state.data.organisationid );

    },

    _addToExportSelection : function() {

        if( this.state.selectedForExport ) {

            FilterActions.removeExportSelection( this.state.data.id );
        
        } else {

            FilterActions.addToExportSelection({

                sessionId           :   this.state.data.id,
                organisationName    :   this.state.data.use_organisation_name,
                sessionDateTime     :   this.state.data.session_start_date_time
            });
        }
    },
});

export default  TrackedCompany;
