import React from 'react';
import ChartistGraph from 'react-chartist';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Paper from 'material-ui/Paper';

const style = {
    paper: {
        padding: 20,
        height: 550
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    }
};

function getCurrentState(props) {
    return {
        accountIsOK: props.accountIsOK,
        data: props.impressions,
        isApplicationResting: props.isApplicationResting
    };
}

var ImpressionsWithinLastWeek = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    shouldComponentUpdate: function (nextProps) {
        var update = false;

        if (this.props.isApplicationResting !== nextProps.isApplicationResting) {
            update = true;
        } else if (typeof this.props.impressions === "undefined" &&
            typeof nextProps.impressions !== "undefined") {
            update = true;
        } else if (typeof this.props.impressions !== "undefined") {
            if (this.props.impressions.labels[0] !== nextProps.impressions.labels[0]) {
                update = true;
            } else if (this.props.impressions.labels[(this.props.impressions.labels.length - 1)] !==
                nextProps.impressions.labels[(this.props.impressions.labels.length - 1)]) {
                update = true;
            }
        }

        return update;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        var type = "Line";

        var options = {
            chartPadding: 15
        };

        if (typeof this.state.data !== "undefined") {
            if (this.state.isApplicationResting) {
                return (
                    <div className="visitorDetails">
                        <Paper style={style.paper} zDepth={4}>
                            <div className="blueHeader">
                                <h4>Unique Visitors</h4>
                                <p>{this.state.data.description}</p>
                            </div>
                            <div className="clr"></div>
                            <br/><br/><br/>
                            <ChartistGraph
                                className={'ct-octave'}
                                data={this.state.data}
                                options={options}
                                type={type} />

                            <div className="clr"></div>
                        </Paper>
                    </div>
                )
            }
        } else if (!this.state.accountIsOK.accountOK) {
            return (
                <div className="visitorDetails">
                    <Paper style={style.paper} zDepth={4}>
                        <div className="blueHeader">
                            <h4>Unique Visitors</h4>
                        </div>
                    </Paper>
                </div>
            )
        }

        return (
            <div className="visitorDetails">
                <Paper style={style.paper} zDepth={4}>
                    <div className="blueHeader">
                        <h4>Unique Visitors</h4>
                    </div>
                    <RefreshIndicator
                        size={70}
                        left={0}
                        top={20}
                        status="loading"
                        style={style.refresh}
                    />
                </Paper>
            </div>
        )
    }
});

export default ImpressionsWithinLastWeek;
