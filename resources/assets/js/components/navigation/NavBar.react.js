import React from 'react';
import {browserHistory, Link} from 'react-router'
import NotificationsStore from '../../stores/NotificationsStore';
import NotificationsManager from './NotificationsManager.react';
import FilterActions from '../../actions/FilterActions';
import NotificationActions from '../../actions/NotificationActions';
import moment from 'moment';
import Pusher from 'pusher-js';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {DateRange, defaultRanges} from 'react-date-range';
import Dialog from 'material-ui/Dialog';
import FiltersStore from '../../stores/FiltersStore';
import TimeFrom from '../TimeFrom.react';
import TimeTo from '../TimeTo.react';
import Lang from '../../classes/Lang';

function getCurrentState(props) {
    let dateFrom = new Date();

    if (typeof props.data.allFilters[0] !== "undefined") {
        dateFrom = moment(props.data.allFilters[0].storedValue);
    }

    let dateTo = new Date();

    if (typeof props.data.allFilters[1] !== "undefined") {
        dateTo = moment(props.data.allFilters[1].storedValue);
    }

    return {
        dateFrom: dateFrom,
        dateTo: dateTo,
        value: 0,
        isNewNotifications: props.data.isNewNotifications,
        notificationsOpen: props.data.notificationsOpen,
        numberOfNew: props.data.numberOfNew,
        user: props.data.user,
        settings: props.data.settings,
        notifications: props.data.notifications,
        notificationsBoxOpen: props.data.notificationsBoxOpen,
        isOpen: props.data.isOpen,
        windowboxOpen: props.data.windowBoxOpen,
        paginationData: props.data.paginationData,
        newFilters: props.data.newFilters,
        allFilters: props.data.allFilters,
        ppcChecked: props.data.ppcChecked,
        organicChecked: props.data.organicChecked,
        isApplicationResting: props.data.isApplicationResting,
        display: props.data.display,
        viewNotificationID: props.data.viewNotificationID,
        accountOK: props.data.accountOK,
        dateRangeOpen: props.data.dateRangeOpen
    }
}

const NavBar = React.createClass({
    getInitialState: function () {
        let currentState = getCurrentState(this.props);
        currentState.displayTimeOptions = 'none';

        return currentState;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },
    componentDidMount: function () {
        NotificationsStore.addChangeListener(this._onChange);

        var pusher = new Pusher('d8ef5a0ae7a920f0b203', {
            cluster: 'eu',
            encrypted: false
        });

        let channel = pusher.subscribe('a1');

        channel.bind('download', function (data) {
            if (typeof data.alertUsers[FiltersStore.getUser().id] !== "undefined") {
                NotificationActions.setInitialNotifications();

                FilterActions.setSnackbar(data);
            }
        });

        channel.bind('misc', function (data) {
            if (typeof data.alertUsers[FiltersStore.getUser().id] !== "undefined") {
                FilterActions.setSnackbar(data);
            }
        });

        var lastOrgVisit = "";

        channel.bind('newOrganisationVisit', function (data) {
            if (typeof Notification !== "undefined") {

                Notification.requestPermission(function (permission) {
                    var userSettings = FiltersStore.getAllSettings();

                    if (parseInt(userSettings.team.end_user_company_id) ===
                        parseInt(data.applicable_to_subscriber_id)) {

                        if (lastOrgVisit !== data.name) {
                            lastOrgVisit = data.name;
                        }
                    }
                });
            }
        });
    },
    componentWillUnmount: function () {
        NotificationsStore.removeChangeListener(this._onChange);
    },
    clickLink: function (event) {
        event.preventDefault();
    },
    formatDateFilter: function (d) {
        const momentDate = moment(d);
        const returnDate = momentDate.format('YYYY-MM-DD');

        return returnDate;

    },
    formatDate: function (dateObj) {
        let displayDate = moment(dateObj).format("Do MMMM YYYY");

        switch (this.state.settings.user.preferred_date_format) {
            case "dd/mm/yyyy" :
                displayDate = moment(dateObj).format("DD/MM/YYYY");
                break;
            case "mm/dd/yyyy" :
                displayDate = moment(dateObj).format("MM/DD/YYYY");
                break;
            case "yyyy-mm-dd" :
                displayDate = moment(dateObj).format("YYYY-MM-DD");

                break;
        }
        return displayDate;
    },
    handleLeftIconClick: function () {
        var isWindowBoxOpen = (this.state.windowboxOpen) ? false : true;
        FilterActions.setMenuWindowboxOpen(isWindowBoxOpen);
    },
    openNotifications: function (event) {
        event.preventDefault();
        NotificationActions.openNotifications();
    },
    render: function () {
        let timeFrom = moment().hours(0).minutes(0);
        let timeTo = moment().hours(23).minutes(59);

        if (typeof this.state.allFilters[2] !== "undefined" &&
            typeof this.state.allFilters[3] !== "undefined") {

            timeFrom = moment(this.state.allFilters[2].storedValue);
            timeTo = moment(this.state.allFilters[3].storedValue);
        }

        let subscriptionOutOfDate = (<div></div>);

        if (window.location.pathname !== "/billing" &&
            window.location.pathname !== "/payment/stripe" &&
            window.location.pathname !== "/payment/gocardless") {

            if (!this.state.accountOK.accountOK) {
                switch (this.state.accountOK.accountStatus) {
                    case 401 :
                        const contactUsLink = (
                            <a href={`mailto:${this.state.settings.agentConfig.contact}`}>
                                {this.state.settings.agentConfig.contact}
                            </a>
                        )

                        subscriptionOutOfDate = (
                            <div>
                                <div id="subscriptionOutOfDate" className="alert alert-danger">

                                    <p>Oops – you don’t currently have a paid subscription. Please go to <a
                                        href="/billing">Billing</a>, or contact us at {contactUsLink} for further support.</p>
                                    <p></p>
                                </div>
                            </div>
                        )

                        break;
                    case 402 :
                        subscriptionOutOfDate = (
                            <div>
                                <div id="subscriptionOutOfDate" className="alert alert-danger">

                                    <p>Oops – you don’t currently have a paid subscription. Please go to <a
                                        href="/billing">Billing</a>, or contact us for further support.</p>

                                    <p>If you haven’t had the opportunity to look at the
                                        data collected during your 30 day trial, please
                                        contact us and
                                        we’ll happily make that available to you.</p>

                                </div>
                            </div>
                        )

                        break;
                    case 403 :
                        subscriptionOutOfDate = (
                            <div>
                                <div id="subscriptionOutOfDate" className="alert alert-danger">

                                    Your account has not been validated. Please ensure you have clicked the activation
                                    link in the email sent to you following sign up.

                                </div>
                            </div>
                        )

                        break;
                }
            }
        }

        let dateRangeText = (
            <div onClick={this._openDateRangeSelector}>
                {this.formatDate(this.state.dateFrom)} &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; {this.formatDate(this.state.dateTo)}
            </div>
        )

        let billing = '';

        if (this.state.settings.agentConfig.id === 12) {
            billing = (
                <li><Link to="/billing">Billing</Link></li>
            )
        }

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this._handleClose}
            />
        ];

        var logoSrc = '/api/proxy/api/v1/images/agent-logo';

        var logoLink = '';

        if (typeof this.state.settings.team.logo_extension !== "undefined" &&
            this.state.settings.team.logo_extension &&
            this.state.settings.team.logo_extension !== "") {

            const agentCustomerLogoSrc = "https://api1.websuccess-data.com/images/agents/customers/" +
                this.state.settings.team.end_user_company_id + "." + this.state.settings.team.logo_extension;

            logoLink = (
                <img style={{marginRight: 15, maxHeight: 50}} src={agentCustomerLogoSrc}/>
            )
        } else if (this.state.settings.agentConfig.logo) {
            logoLink = (
                <img style={{marginRight: 15, maxHeight: 50}} src={logoSrc}/>
            )
        }

        let showOrHideText = (this.state.displayTimeOptions === "none") ? "Show" : "Hide";

        let companyName = (typeof this.state.user.company_name !== "undefined") ? this.state.user.company_name : "";

        let filterLogLink = "";

        if (typeof this.state.user.super_user_is_admin !== "undefined" &&
            parseInt(this.state.user.super_user_is_admin) === 1) {

            filterLogLink = (
                <li><a href="/filter-log" target="_blank">Filter Log</a></li>
            )
        }

        return (
            <div className="navContainer">
                <header>
                    <div className="inner">
                        <nav>
                            <Link to="/" className="logo w-2/12 float-left">{logoLink}</Link>
                            <input type="checkbox" id="nav"/><label htmlFor="nav"></label>
                            <ul id="topLevelNav">
                                <li>
                                    {dateRangeText}
                                </li>
                                <li>
                                    <Link to="/">Dashboard</Link>
                                </li>
                                <li>
                                    <a href="#">Filters</a>
                                    <ul>
                                        <li><Link to="/advanced-report">Advanced Filters</Link></li>
                                        <li><Link to="/all-pages">All Pages</Link></li>
                                        <li><a href="#" onClick={this._viewAllVisitors}>All Visitors</a></li>
                                        <li><Link to="/common-paths">Common Paths</Link></li>
                                        <li><Link to="/countries">Countries</Link></li>
                                        <li><Link to="/entry-pages">Entry Pages</Link></li>
                                        <li><Link to="/keywords">Keywords</Link></li>
                                        <li><Link to="/organisations">{Lang.getWordUCFirst("organisations")}</Link></li>
                                        <li><Link to="/referrers">Referrers</Link></li>
                                        <li><Link to="/company-tracker">Tracker</Link></li>

                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Options</a>
                                    <ul>
                                        {billing}
                                        <li><Link to="/create-template">Create Template</Link></li>
                                        <li><Link to="/developer">Developer</Link></li>
                                        <li><Link to="/export">Export</Link></li>
                                        <li><Link to="/saved-templates">My Templates</Link></li>
                                        <li><Link to="/settings">Settings</Link></li>
                                        <li><Link to="/tags">Tags</Link></li>
                                        {filterLogLink}
                                        <li><a href="/logout">Log Out</a></li>

                                    </ul>
                                </li>
                                <li>
                                    Welcome, {companyName}
                                </li>
                                <li onClick={this.openNotifications}>
                                    <a href="#">
                                        <NotificationsManager
                                            isOpen={this.state.isOpen}
                                            display={this.state.display}
                                            notifications={this.state.notifications}
                                            viewNotificationID={this.state.viewNotificationID}
                                            isNew={this.state.isNewNotifications}
                                            numberOfNew={this.state.numberOfNew}
                                            settings={this.state.settings}/>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    {subscriptionOutOfDate}
                </header>

                <Dialog
                    title="Select Date From and Date To"
                    modal={false}
                    contentStyle={{width: "75%", maxWidth: "none"}}
                    actions={actions}
                    open={this.state.dateRangeOpen}
                >
                    <div style={{textAlign: 'center'}}>
                        <DateRange
                            onInit={this.handleSelect}
                            twoStepChange={true}
                            ranges={defaultRanges}
                            onChange={this._handleDateRangeChange}
                        />

                        <div className="clr"></div>
                        <br/>
                    </div>
                    <div>
                        <div className="row">
                            <div className="col-md-3 col-md-offset-3">
                                <p><a href="#" onClick={this._showTimeOptions}>{showOrHideText} time options</a></p>
                            </div>
                        </div>
                        <div style={{display: this.state.displayTimeOptions}}>
                            <div className="row">
                                <div className="row">
                                    <div className="col-md-3 col-md-offset-3">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <p style={{marginLeft: 8, float: 'left'}}><strong>Time From:</strong>
                                                </p>

                                                <div>
                                                    <TimeFrom time={timeFrom.format("HH:mm:ss")}/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <p style={{marginLeft: 8, float: 'left'}}><strong>Time To:</strong></p>
                                                <div>
                                                    <TimeTo time={timeTo.format("HH:mm:ss")}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-3">
                                        <RaisedButton onTouchTap={this._applyTimeFilters} primary={true} label="Apply"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Dialog>
            </div>
        )
    },
    _applyTimeFilters: function () {
        FilterActions.updateResults();
    },
    _showTimeOptions: function (event) {
        const option = (this.state.displayTimeOptions === "none") ? "inline" : "none";

        event.preventDefault();

        this.setState({
            displayTimeOptions: option
        })
    },
    _viewAllVisitors: function () {
        FilterActions.resetFilters(true);

        browserHistory.push('/results');
    },
    _handleClose: function () {
        FilterActions.closeLightbox();
    },
    _handleDateRangeChange: function (newDates) {
        FilterActions.setDateRange([
            {
                id: 1,
                storedValue: newDates.endDate.format('YYYY-MM-DD'),
                type: 'dateTo'
            },
            {
                id: 0,
                storedValue: newDates.startDate.format('YYYY-MM-DD'),
                type: 'dateFrom'
            }
        ]);
    },
    _openDateRangeSelector: function () {
        FilterActions.setLightbox({
            type: 'dateRange',
            id: -1
        })
    },
    _onChange : function() {
        // do not delete
    }
});

export default NavBar;
