import React from 'react';

var ReferrerOption = React.createClass({

	getInitialState 	: 	function() {

		return {

			id 		: 	this.props.details.id,
			name 	: 	this.props.details.name_sanitised
		};
	},

	shouldComponentUpdate : function( newProps, newState ) {

		if( this.props.details.id !== newProps.details.id ) {

			return true;
		}

		return false;
	},

	render 	: 	function() {
		return(

			<option value={this.state.id}>{this.state.name}</option>

		);
	}
});

export default  ReferrerOption;