import React from 'react';
import FiltersStore	from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import Checkbox from 'material-ui/Checkbox';

const styles = {

  	checkbox: {
    	marginBottom: 16
  	}
};

function getCurrentState( props ) {

	return {

			browserData : props.browserData,
			isChecked 	: props.checked
	} 
}

var BrowserFilter = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.isChecked !== nextProps.checked ) {

			return true;
		}

		return false;
	},

	render : function() {

		var browserName = ( typeof this.state.browserData !== "undefined" ) ? this.state.browserData.name : '';

		return (

			<div className="col-md-4">

				<Checkbox
					label={browserName}
					style={styles.checkbox}
					checked={this.state.isChecked}
					onCheck={this._saveCheckedState}
				/>
				
			</div>
		)

	},

	_saveCheckedState : function( event ) {

		var newState  = {

   			type          	:   "browserFilter", 
   			id 				: 	this.state.browserData.id,
   			storedValue 	:   {

				id 		: 	this.state.browserData.id,
				name 	: 	this.state.browserData.name,
				value 	: 	this.state.browserData.id
			},
			overrideKey 	: "browser" + this.state.browserData.id, 

   		}

   		var checked = event.target.checked;

   		if( checked ) {

   			FilterActions.createFilter( newState );

   		} else {

   			FilterActions.removeFilterByType({

   				type 	: 	'browserFilter',
   				id 		: 	this.state.browserData.id
   			});
   		}
	}
});

export default  BrowserFilter;