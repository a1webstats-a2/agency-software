import React from 'react';
import FilterActions from '../../actions/FilterActions';
import Checkbox from 'material-ui/Checkbox';

function getCurrentState( props ) {

	return {

        quickExportResults 	: 	props.quickExportResults,
		isChecked 			: 	props.isChecked,
		data 				: 	props.data
	}
}

var QuickExportCheckbox = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.isChecked !== nextProps.isChecked ) {

			return true;
		}

		if( this.props.quickExportResults !== nextProps.quickExportResults ) {

			return true;
		}

		if( this.props.data !== nextProps.data ) {

			return true;
		}

		return false;
	},

	render : function() {

		return (

			<div>
				<Checkbox 
					onCheck={this._addToExportSelection}
					checked={this.state.isChecked}
				/>
			</div>
		)
	},

	_addToExportSelection : function() {

		var selection = this.state.quickExportResults;

		if( typeof selection[parseInt( this.state.data[0])] !== "undefined" && 
			selection[parseInt( this.state.data[0])] ) {

			delete selection[parseInt( this.state.data[0] )];
		
		} else {

			selection[parseInt( this.state.data[0] )] = this.state.data[1];
		}

		FilterActions.setAddOrRemoveQuickExportSelection( selection );
	}
});

export default QuickExportCheckbox;