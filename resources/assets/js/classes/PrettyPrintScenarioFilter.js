import React from 'react';

class PrettyPrintScenarioFilter {

	constructor( props ) {

		this.state = {

			filter : {}
		}
	}

	setFilter( filter ) {

		this.state.filter = filter;
	}

	printPrettyFilterSet( filterSetType, filters  ) {

		let returnString = "";
		returnString+= "filter set type " + filterSetType + "<br />";
		returnString+= "filters: <br />";

		for( let i in filters ) {

			let filter = filters[i];

			returnString+= i + " - " + filter;
		}

		return returnString;
	}

	printPrettyString() {
                 

		const includeOrExclude = ( Boolean( this.state.filter.include ) ) ? "include" : "exclude";

		let returnString = "";

		returnString+= "---\n";
		returnString+= "filter set " + parseInt( this.state.filter.setID ) + "<br />";
		returnString+= "filter set and/or " + this.state.filter.andOr + "<br />";
		returnString+= "filter set include/exclude " + includeOrExclude + "<br />";

		let filterSetTypeObj 	= {};
		let filterSetType 		= "";

		for( let i in this.state.filter.filters ) {

			let individualFilterSetFilter 					= this.state.filter.filters[i];

			filterSetType = this.state.filter.type;
			filterSetTypeObj[individualFilterSetFilter.storedValue.id] 	= individualFilterSetFilter.storedValue.textValue;
		}

		returnString+= this.printPrettyFilterSet( filterSetType, filterSetTypeObj );

		return returnString;
	}
}

export default PrettyPrintScenarioFilter;