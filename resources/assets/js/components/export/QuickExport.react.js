import React from 'react';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import FilterActions from '../../actions/FilterActions';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RefreshIndicator from 'material-ui/RefreshIndicator';

function getCurrentState( props ) {

	return {

		timer 				: 	null,
		applicationResting 	: 	props.applicationResting,
		searchString 		: 	props.searchString,
		filterType 			: 	props.filterType,
		type 				: 	props.type,
		exportSelection		: 	props.selection,
		fileTypes 			: 	props.fileTypes,
		disableNext 		: 	props.disableNext,
		selectionType 		: 	props.selectionType,
		landingPageNo 		: 	props.landingPageNo,
		displayPerPage 		: 	props.displayPerPage
	}
}

const styles = {

	button : {

		fontSize 		: '10px'
	},

  	block: {
    	maxWidth: 250,
  	},
  	radioButton: {
    	marginBottom: 16,
  	},

  	checkbox: {
    	marginBottom: 16,
  	},

  	raisedButton : 	{

  		fontSize 		: 	'12px',
		marginBottom 	: 	25
	},

	refreshContainer 	: {

    	position 	: 'relative',
  	},

  	refresh 	: {

    	display 	: 'inline-block',
    	position 	: 'relative',
  	}
};

var QuickExport = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		var prevDisabled 		= ( this.state.landingPageNo === 0 ) ? true : false;

		var	paginationOptions 	= (

			<div>
				<div className="row">
					<div className="col-md-12">
						<SelectField
							floatingLabelText="Display per page"
							onChange={this._setDisplayPerPageForLandingPages}
							value={this.state.displayPerPage}
							fullWidth={true}
						>
							<MenuItem value={10} primaryText="10" />
							<MenuItem value={50} primaryText="50" />
							<MenuItem value={100} primaryText="100" />
						</SelectField>

					</div>
				</div>

				<div className="clr"></div><br />

				<div className="row">

					<div className="col-md-6">

						<RaisedButton disabled={prevDisabled} labelStyle={styles.button} labelPosition="before" labelColor="#fff" backgroundColor='#FFB347' style={styles.raisedButton} onClick={this._landingPagePrevious} label="Previous"  />
					</div>

					<div className="col-md-6">

						<RaisedButton disabled={this.state.disableNext} labelStyle={styles.button} labelPosition="before" labelColor="#fff" backgroundColor='#FFB347' style={styles.raisedButton} onClick={this._landingPageNext} label="Next"  />
					</div>
				</div>
			</div>
		)

		var loadingSearchResults = "ready";

		if( !this.state.applicationResting ) {

			loadingSearchResults = "loading";
		}

		let exportButton = "";

		if( this.state.filterType.toLowerCase() !== "organisationfilter" ) {

			exportButton = 	(

				<RaisedButton fullWidth={true} labelStyle={styles.button} labelPosition="before" labelColor="#fff" backgroundColor='#53b68b' style={styles.raisedButton} onClick={this._startExport} label="Export"  />
			);
		
		} else {

			exportButton = (

				<RaisedButton fullWidth={true} labelStyle={styles.button} labelPosition="before" labelColor="#fff" backgroundColor='#53b68b' style={styles.raisedButton} onClick={this._openExportType} label="Export"  />
			);
		}

		return (

			<div>
				<div className="row">
				
					<div className="col-md-12">

						<h3>Export / View More</h3>
					
						<div className="clr"></div>

						<br />

						<div className="row">

							<div className="col-md-9">

								<RadioButtonGroup className="exportOptionsSelection" name="selectionType" onChange={this._setSelectionType} valueSelected={this.state.selectionType}>
									
									<RadioButton
										value="all"
										label="All"
										style={styles.radioButton}
									/>

									<RadioButton
										value="selection"
										label="Selection"
										style={styles.radioButton}
									/>

									<RadioButton
										value="patternMatch"
										label="All that match pattern"
										style={styles.radioButton}
									/>
									
								</RadioButtonGroup>

							</div>

							<div className="col-md-2">
								<RefreshIndicator
							    	size={30}
							    	left={0}
							    	top={0}
							    	status={loadingSearchResults}
							    	style={styles.refresh}
							    />
							</div>
						</div>

						<div className="clr"></div>


						<TextField
					    	id="quickExportSearch"
					    	hintText="Search"
					    	fullWidth={true}
					    	onChange={this._quickExportSearch}
					    />
						<div className="clr"></div><br />

						<div className="row">

							<div className="col-md-6">

								{exportButton}
							</div>
						
							<div className="col-md-6">

								<RaisedButton fullWidth={true} labelStyle={styles.button} primary={true} label="Show" style={styles.raisedButton} onClick={this._showData} />
							</div>
						</div>


						<div className="row">

							<div className="col-md-6">

								<RaisedButton fullWidth={true} labelStyle={styles.button} labelPosition="before" labelColor="#fff" backgroundColor='#FFB347' style={styles.raisedButton} onClick={this._selectAll} label="Select All"  />

							</div>

							<div className="col-md-6">

								<RaisedButton fullWidth={true} labelStyle={styles.button} labelPosition="before" labelColor="#fff" backgroundColor='#FFB347' style={styles.raisedButton} onClick={this._deselectAll} label="Deselect All"  />

							</div>

						</div>

						{paginationOptions}
					</div>
				</div>
			</div>

		)
	},

	_selectAll : function() {

		FilterActions.quickExportSelectAll( this.state.type );
	},

	_deselectAll : function() {

		FilterActions.quickExportDeselectAll( this.state.type );
	},

	_openExportType : function() {

		FilterActions.openChooseExportType();
	},

	_quickExportSearch : function( event, textValue ) {

		var timer = this.state.timer;

		if( this.state.applicationResting ) {

			this.setState({

				applicationResting : false
			})
		}

		clearTimeout( timer );
		
		var newTimer = setTimeout( function() {
			
			FilterActions.setQuickExportSearchResults( textValue );

		}, 1000 );

		this.setState({

			timer : newTimer
		})

	},

	_showData : function() {

		FilterActions.quickExportShowData( this.state.filterType );
	},

	_landingPagePrevious : function() {

		FilterActions.setLandingPage( 'previous' )
	},

	_landingPageNext : function() {

		FilterActions.setLandingPage( 'next' );
	},

	_setDisplayPerPageForLandingPages : function( event, key, value ) {

		FilterActions.setDisplayPerPageForLandingPages( value );
	},

	_setExportPDFValue : function( event, isChecked ) {

		FilterActions.setQuickExportFileType({

			type 	: 	'PDF',
			apply 	: 	isChecked
		})
	},

	_setExportCSVValue : function( event, isChecked ) {

		FilterActions.setQuickExportFileType({

			type 	: 	'CSV',
			apply 	: 	isChecked
		})
	},

	_startExport : function() {

		FilterActions.startQuickExport( this.state.type );
	},

	_setSelectionType : function( event, value ) {

		FilterActions.setQuickExportSelectionType( value );
	}
});

export default  QuickExport;
