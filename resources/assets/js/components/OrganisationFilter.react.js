import React 				from 'react';

import FilterActions   	from '../actions/FilterActions';
import Business from 'material-ui/svg-icons/communication/business';

var OrganisationFilter  = React.createClass({

    getInitialState :   function() {

        return {

            data            :   this.props.data,
            myKey           :   this.props.myKey,
            value           :   this.props.value,
            type            :   this.props.type,
            criteria        :   this.props.criteria
        };

    },

    shouldComponentUpdate : function( nextProps, nextState ) {

        return false;
    },

    render  :   function() {


        var organisationName = "";

        var stringArray         =   this.state.value.split( " " );

        var returnString = stringArray.map( function( word, i ) {

            if( word.toLowerCase().indexOf( "ftip") === -1 ) {

                return word;
            
            } else {

                return "";
            }
        })

        organisationName    =   returnString.join( " " ).trim();
    
        return (

            <div className="organisationFilter form-group">
                <div className="row">
                    <div className="col-md-2">
                        <Business />
                    </div>
                    <div className="col-md-8 overflowHidden">
                        <label className="control-label">Organisation Filter:</label>
                        <p>{this.state.criteria} {organisationName}</p>

                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>

            </div>

        )
    },

    _removeFilter   :   function( e ) {

        FilterActions.removeFilterByType({

            type    :   'organisationFilter',
            id      :   this.state.data.value
        });
    }
})

export default  OrganisationFilter;