import React from 'react';
import Toggle from 'material-ui/Toggle';
import FilterActions from '../../actions/FilterActions';
import TextField from 'material-ui/TextField';


function getCurrentState( props ) {

	return {

		settings 				: 	props.settings,
		use_geolocation 		: 	Boolean( props.settings.use_geolocation ),
		display_gclid 			: 	Boolean( props.settings.display_gclid ),
		concatenate_page_visits : 	Boolean( props.settings.display_page_visits_concatenated )
	}
}

const styles = {
 
  	toggle 	: {
    	marginBottom 	: 16,
  	}
};


var GeneralSettings = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		return (

			<div>
				<div className="row">
						
					<div className="col-md-12">
						
						<h3>General Settings</h3><br />
					</div>

				</div>

				<div className="row">

					<div className="col-md-8">

						<Toggle
							label="Use geolocation on site? (Requests permission from user on page load)"
							style={styles.toggle}
							toggled={this.state.use_geolocation}
							onToggle={this._setUseGeolocation}
						/><br />

						
					</div>
				</div>

				<div className="row">

					<div className="col-md-8">

						<Toggle
							label="Display GCLID text? (ID used within Google AdWords)"
							style={styles.toggle}
							toggled={this.state.display_gclid}
							onToggle={this._setDisplayGCLID}
						/><br />

						
					</div>
				</div>

				<div className="row">

					<div className="col-md-8">

						<Toggle
							label="Export page visits concatenated"
							style={styles.toggle}
							toggled={this.state.concatenate_page_visits}
							onToggle={this._setConcatenatePageVisits}
						/><br />

						
					</div>
				</div>

				

			
			</div>
		)
	},

	_setConcatenatePageVisits : function() {

		let concatenatePageVisits = ( this.state.concatenate_page_visits ) ? false : true;

		FilterActions.setConcatenatePageVisits( concatenatePageVisits );
	},

	_setDisplayGCLID : function() {

		var displayGCLID = ( this.state.display_gclid ) ? false : true;

		FilterActions.setDisplayGCLID( displayGCLID );
	},

	_setUseGeolocation : function() {

		var useGeolocation = ( this.state.use_geolocation ) ? false : true;

		FilterActions.setGeolocation( useGeolocation );
	}
});

export default  GeneralSettings;