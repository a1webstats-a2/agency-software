import UserTipsDispatcher from '../dispatcher/UserTipsDispatcher';
import UserTipsConstants from '../constants/UserTipsConstants';
import EventEmitterObj from 'events';

let EventEmitter = EventEmitterObj.EventEmitter;
import merge from 'merge';
import assign from 'object-assign';
import FiltersStore from './FiltersStore';
import UserTipActions from '../actions/UserTipActions';

let _tip = {
    id: -1,
    title: '',
    slides: [],
    slideCount: 0,
    currentSlide: 0,
    open: false,
    displayOnPageLoad: false,
    openedThroughHelpLink: false
}

function resetUserTip() {
    _tip = {
        id: -1,
        title: '',
        slides: [],
        slideCount: 0,
        currentSlide: 0,
        open: false,
        displayOnPageLoad: false,
        openedThroughHelpLink: false
    }
}

function neverShowTipsAgain() {
    _tip = {
        id: -1,
        title: '',
        slides: [],
        slideCount: 0,
        currentSlide: 0,
        open: false,
        displayOnPageLoad: false,
        openedThroughHelpLink: false
    }
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/user-tip", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({neverShowAgain: true, id: -1}));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    break;
            }
        }
    };
}

function getInternalPageID(currentRoute) {
    let internalPageID = 9;
    let routes = {
        "advanced-report": 6,
        "countries": 18,
        "all-pages": 15,
        "results": 8,
        "common-paths": 19,
        "entry-pages": 13,
        "keywords": 16,
        "organisations": 10,
        "referrers": 17,
        "company-tracker": 11,
        "billing": 7,
        "create-template": 14,
        "developer": 5,
        "export": 4,
        "saved-templates": 12,
        "settings": 20,
        "tags": 3,
    }

    if (typeof routes[currentRoute] !== "undefined") {
        internalPageID = routes[currentRoute];
    }

    return internalPageID;
}

function storeAndShowUserTip(userTipObj) {
    userTipObj.open = true;
    userTipObj.currentSlide = 0;
    userTipObj.slideCount = userTipObj.slides.length;
    userTipObj.openedThroughHelpLink = true;
    _tip = userTipObj;
}

function storeUserTip(userTip) {
    userTip.open = false;
    if (userTip.displayOnPageLoad === 1) {
        userTip.open = true;
    }
    userTip.currentSlide = 0;
    userTip.slideCount = userTip.slides.length;
    _tip = userTip;
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function closeUserTip(userTipID) {
    let openedThroughHelpLink = _tip.openedThroughHelpLink;
    _tip.open = false;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/user-tip", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({id: userTipID, openedThroughHelpLink}));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    break;
            }
        }
    };
}

function updateTipObj(tipObj) {
    _tip = tipObj;
}

function openHelp() {
    _tip.open = true;
    _tip.openedThroughHelpLink = true;
}

function showSpecificHelpItem(internalPageID) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/api/proxy/api/v1/user-tip/force/${internalPageID}`, true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send();

    _tip.open = true;
    _tip.openedThroughHelpLink = true;
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    let response = false;
                    try {
                        response = JSON.parse(xhr.responseText);
                    } catch (e) {
                        UserTipActions.resetUserTip();
                    }
                    if (response) {
                        UserTipActions.storeAndShowUserTip(response);
                    }
                    break;
            }
        }
    };
}

function loadOneUserTip() {
    let internalPageID = getInternalPageID(location.pathname.replace("/", ""));
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/proxy/api/v1/user-tip/" + internalPageID, true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    let response = false;
                    try {
                        response = JSON.parse(xhr.responseText);
                    } catch (e) {
                        UserTipActions.resetUserTip();
                    }
                    if (response) {
                        UserTipActions.storeUserTip(response);
                    }
                    break;
            }
        }
    };
}

let UserTipsStore = merge(EventEmitter.prototype, {
    getTip: function () {
        return clone(_tip);
    },
    emitChange: function () {
        this.emit('change');
    },
    addChangeListener: function (callback) {
        this.on('change', callback);
    },
    removeChangeListener: function (callback) {
        this.removeListener('change', callback);
    }
});

UserTipsDispatcher.register(function (payload) {
    let returnRes = true;
    switch (payload.actionType) {
        case UserTipsConstants.LOAD_ONE_USER_TIP :
            loadOneUserTip();

            break;
        case UserTipsConstants.STORE_USER_TIP :
            storeUserTip(payload.data);

            break;
        case UserTipsConstants.UPDATE_TIP_OBJ :
            updateTipObj(payload.data);

            break;
        case UserTipsConstants.SHOW_HELP :
            openHelp();

            break;
        case UserTipsConstants.NEVER_SHOW_TIPS_AGAIN :
            neverShowTipsAgain();

            break;
        case UserTipsConstants.RESET_USER_TIP :
            resetUserTip();

            break;
        case UserTipsConstants.STORE_AND_SHOW_USER_TIP :
            storeAndShowUserTip(payload.data);

            break;
        case UserTipsConstants.SHOW_SPECIFIC_HELP_ITEM :
            showSpecificHelpItem(payload.data);

            break;
        case UserTipsConstants.CLOSE_USER_TIP :
            closeUserTip(payload.data);

            break;
        default :
            break;
    }

    UserTipsStore.emitChange();

    return returnRes;
});

export default UserTipsStore;
