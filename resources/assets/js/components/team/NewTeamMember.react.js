import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import FilterActions from '../../actions/FilterActions';
		
function getCurrentState( props ) {

	return {

		open 			: 	props.open,
		newTeamMember 	: 	props.newTeamMember
	}
}

var NewTeamMember = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.open !== nextProps.open ) {

			return true;
		}

		return false;
	},

	render : function() {

		if( !this.state.open ) {

			return ( <div></div> );
		}

		const newTeamMemberActions = [

			<FlatButton
				label="Close"
				primary={true}
				onTouchTap={this._closeNewTeamMember}
			/>,

		];

		return (

			<div>

				<Dialog
					title="New Team Member"
					actions={newTeamMemberActions}
					modal={false}
					open={this.state.open}
					onRequestClose={this._closeNewTeamMember}
				>
					<p>Thank you, a new user has been created for {this.state.newTeamMember.user.forename} {this.state.newTeamMember.user.surname}</p>
					<p>Their password is <strong>{this.state.newTeamMember.newSystemUser.password}</strong>. Their password has
						been sent to the email address you provided.</p>
					<p>The password will not be displayed again.</p>
				</Dialog>

			</div>

		)
	},

	_closeNewTeamMember : function() {

		FilterActions.closeLightbox();
	}

});

export default  NewTeamMember;