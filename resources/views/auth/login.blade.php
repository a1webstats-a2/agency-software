@extends( 'templates.signup' )

<?php 
$configData = getAgentConfigData();
$loginLogo  = '/api/proxy/api/v1/images/agent-logo/' . $configData['agentID'];
?>

@section( 'title', $configData['name'] )

@section( 'content' )

    @if ( session( 'status' ) )
        <div class="alert alert-danger">
            <ul>
                <li>{{ session('status') }}</li>
            </ul>
        </div>
    @endif

    <div class="row">
        <div  class="col-md-6 col-md-offset-3">
            <div id="loginForm">
                <div className="clr"></div><br /><br /><br />

                <div id="loginLogo" class="blueHeader">
                    <img src="{{ $loginLogo }}" />
                </div>
                <br /><br />
                <div>
                    <form method="POST" action="/auth/login">
                        <div class="form-group">
                            <label for="username">Email Address</label>
                            <input type="text" name="email" class="form-control" id="email" placeholder="Email Address" value="{{ old('email') }}">
                        </div>
                        <br />
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <br />
                        <input type="hidden" name="redirectArea" value="{{ isset( $redirectLocation ) ? $redirectLocation : '' }}">
                        <input type="hidden" name="reportID" value="{{ isset( $reportID ) ? $reportID : '' }}">
                        <br />
                        <button type="submit" alt="login" class="btn btn-primary">Login</button>
                    </form>

                    <br />
                    <br />
                    <p><a href="/forgotten-password">Forgotten Password</a></p>

                </div>

            </div>
        </div>
    </div>
@endsection
