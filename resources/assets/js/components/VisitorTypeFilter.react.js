import React from 'react';
import SocialIcon from 'material-ui/svg-icons/social/group';

function getCurrentState(props) {
    return {
        data: props.data
    }
}

const VisitorTypeFilter = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return false;
    },
    render: function () {
        return (
            <div className="dateFromBox form-group">
                <div className="row">
                    <div className="col-md-2">
                        <SocialIcon/>
                    </div>
                    <div className="col-md-8 overflowHidden">
                        <label className="control-label">Visitor Type:</label>
                        <p>{this.state.data.storedValue.textValue}</p>
                    </div>
                </div>
            </div>
        )
    }
});

export default VisitorTypeFilter;
