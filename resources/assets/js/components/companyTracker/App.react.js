import React from 'react';
import NavBar from '../navigation/NavBar.react';
import TrackedCompaniesStore from '../../stores/TrackedCompaniesStore';
import FiltersStore from '../../stores/TrackedCompaniesStore';
import TrackedCompaniesActions from '../../actions/TrackedCompaniesActions';
import Tracked from './Tracked.react';
import NotificationsStore from '../../stores/NotificationsStore';
import PreviousVisits from '../sessionResult/PreviousVisits.react';
import CompanyData from '../companyData/CompanyData.react';
import CreateNote from '../sessionResult/CreateNote.react';
import AssignToTeamMember from '../companyTracker/AssignToTeamMember.react';
import EditCompanyRecord from '../companyData/EditCompanyRecord.react';
import SendOrganisationViaEmail from '../organisationsDashboard/SendOrganisationViaEmail.react';
import AdditionalCompanyData from '../companyData/AdditionalCompanyData.react';
import ClientTypesLightbox from '../prospecting/ClientTypesLightbox.react';
import UserTipsStore from '../../stores/UserTipsStore';

function getCurrentState() {

 	var settings 				= 	FiltersStore.getAllSettings();
	var windowBoxOpen 			= 	FiltersStore.getWindowBoxOpen();
	var	newFilters 				= 	FiltersStore.haveNewFiltersBeenApplied();
	var paginationData  		= 	FiltersStore.getPaginationTotals();
	var filters 				=	FiltersStore.getFilters();
	var isApplicationResting 	=	FiltersStore.isApplicationResting();
	var teamMembers 			= 	FiltersStore.getTeamMembers();

	return {	

		editCompanyData 		: 	{

			activeRecord 	: FiltersStore.getActiveRecord(),
    		open 			: FiltersStore.checkLightboxOpen( 'editCompanyRecord', 1 )
		},
		
		clientTypesObj 				: 	FiltersStore.getClientTypesLightboxObj(),
		additionalCompanyData 		: 	FiltersStore.getAdditionalCompanyData(),
		additionalCompanyDataOpen 	: 	FiltersStore.checkLightboxOpen( 'additionalCompanyData', -1 ),
		clientTypesLightboxOpen 	: 	FiltersStore.checkLightboxOpen( 'clientTypes', 1 ),


		sendOrganisationViaEmailOpen 	: 	FiltersStore.checkLightboxOpen( 'sendOrganisationViaEmailOpen', 1 ),
		
		notes 			: 	{

			notes : FiltersStore.getNotes()

		},
		clientTags 				: 	FiltersStore.getClientTypes(),

		assignTeamMember 	: 	{

			currentTrackingObj 	: 	FiltersStore.getCurrentTrackingObj(),
			open 				: 	FiltersStore.checkLightboxOpen( 'assignToTeamMember', 1 ),
			teamMembers 		: 	teamMembers
		},

		isSearchingForPossibleOptions 	: 	FiltersStore.isSearchingForActiveRecordOptions(),
		checkedForPossibleOptions 		: 	FiltersStore.checkedForActiveRecordPossibleOptions(),
		possibleOptions 				: 	FiltersStore.getActiveRecordSuggestions(),
		chosenPossibleOption 			: 	FiltersStore.getActiveRecordChosenOption(),


		teamMembers 			: 	teamMembers,
		assignedTo 				: 	TrackedCompaniesStore.getAssignedTo(),
		createNoteOpen 			: 	FiltersStore.checkLightboxOpen( 'createNote', 1 ),
		companyData 			: 	{

				companyData 	: FiltersStore.getOrganisationData(),
            	dueDilData      : false,
            	open            : FiltersStore.checkLightboxOpen( 'companyData', 0 )
		},
		tracked 					: 	TrackedCompaniesStore.fetchTrackedOrganisations(),
		relationships 				: 	TrackedCompaniesStore.getRelationships(),
		clientTags 					: 	FiltersStore.getClientTypes(),
		settings 					: 	FiltersStore.getAllSettings(),
	
		sendEmailToTeamMembers 		: 	FiltersStore.getSendEmailToTeamMembers(),

		isLoadingPreviousHistory 	: 	FiltersStore.isLoadingPreviousHistory(),
		notificationsData 			: 	{
			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),
			accountOK 				: 	FiltersStore.accountIsOK(),
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()
		},
		previousVisitsObj 	: 	FiltersStore.getPreviousVisits(),
		previousHistory 	: 	{

			open 		: FiltersStore.checkLightboxOpen( 'previousVisits', 1 ), 
			prevResults : FiltersStore.getPreviousHistory(),
			orgName 	: FiltersStore.getPreviousHistoryOrganisationName()
		}	
	}
}

var App = React.createClass({

	getInitialState : function() {

		return getCurrentState();
	},

	componentDidMount : function() {
		
		UserTipsStore.addChangeListener( this._onChange );

		TrackedCompaniesStore.addChangeListener( this._onChange );

	},

	componentWillMount : function() {


		TrackedCompaniesActions.setTrackedOrganisations()

	},


	componentWillUnmount: function() {
		
		UserTipsStore.removeChangeListener( this._onChange );

		TrackedCompaniesStore.removeChangeListener( this._onChange );
	},

	componentWillReceiveProps : function() {

		
	},

	render : function() {

		return (

			<div className="reportBuilder">

				<NavBar data={this.state.notificationsData} />

				<div className="clr"></div>

				<div className="container mainContainer">

					<div className="row" id="results">

						<div className="col-md-12">

							<Tracked 
								settings={this.state.settings.user}
								tracked={this.state.tracked} 
								relationships={this.state.relationships} 
								tags={this.state.clientTags}
								assignedTo={this.state.assignedTo}
							/>
						</div>
					</div>
				</div>

				<PreviousVisits previousVisitsObj={this.state.previousVisitsObj} settings={this.state.settings} isLoading={this.state.isLoadingPreviousHistory} previousVisits={this.state.previousHistory} />

                <CompanyData companyData={this.state.companyData} />

                <EditCompanyRecord 
                	isSearchingForPossibleOptions={this.state.isSearchingForPossibleOptions}
                	chosenPossibleOption={this.state.chosenPossibleOption}
					checkedForPossibleOptions={this.state.checkedForPossibleOptions}
                	possibleOptions={this.state.possibleOptions}
                	editCompanyRecordData={this.state.editCompanyData} 
                	storeType="tracker" />

                <CreateNote notes={this.state.notes.notes} open={this.state.createNoteOpen} />

                <AssignToTeamMember assignedData={this.state.assignTeamMember} />
                
                <AdditionalCompanyData open={this.state.additionalCompanyDataOpen} additionalCompanyData={this.state.additionalCompanyData} />
  				<ClientTypesLightbox 
                	open={this.state.clientTypesLightboxOpen} 
                	relationships={this.state.relationships} 
                	tags={this.state.clientTags} 
                	storeType="tracker"
                	organisationID={this.state.clientTypesObj.organisationID} />
                <SendOrganisationViaEmail sendEmailToTeamMembers={this.state.sendEmailToTeamMembers} teamMembers={this.state.teamMembers} open={this.state.sendOrganisationViaEmailOpen} settings={this.state.settings} />

			</div>
		)
	},

	_onChange: function() {

		this.setState( getCurrentState() );
	}

});

export default  App;