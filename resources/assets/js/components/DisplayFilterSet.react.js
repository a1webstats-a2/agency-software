import React from 'react';
import Lang from '../classes/Lang';

function getCurrentState(props) {
    return {
        set: props.filters
    }
}

String.prototype.rtrim = function (s) {
    if (s == undefined)
        s = '\\s';
    return this.replace(new RegExp("[" + s + "]*$"), '');
};

const DisplayFilterSet = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        const displayFilterSetTypeNames = {
            'counties': 'Counties',
            'campaignname': 'Campaign Title',
            'campaignsource': 'Campaign Source',
            'campaignmedium': 'Campaign Medium',
            'devicetype': 'Device Type',
            'events': 'Events',
            'geolocation': 'Geolocation',
            'visitortypefilter': 'Visitor Type',
            'visitortypefilters': 'Visitor Types',
            'referrer': 'Referrers',
            'firstpagevisited': 'First Page Visited',
            'pagevisited': 'Page Visited',
            'lastpagevisited': 'Last Page Visited',
            'organisation': 'Organisations',
            'keyword': 'Keywords',
            'ip': 'IP Search',
            'tags': 'Tags',
            'town': 'Town / City',
            'countries': 'Countries',
            'source': 'Source'
        }

        var typesString = "";
        let filterType = false;

        for (var filterKey in this.state.set.filters) {
            if (!filterType) {
                filterType = this.state.set.filters[filterKey].type;
            }

            var filter = this.state.set.filters[filterKey];
            typesString += filter.storedValue.textValue + ", ";
        }

        typesString = typesString.rtrim(", ");

        var includeText = "";
        var andOrText   = (this.state.set.andOr === 0) ? this.state.set.andOr + ' where' : 'Where';

        if (this.state.set.include) {
            const displayType = Lang.getWordUCFirst(displayFilterSetTypeNames[filterType.toLowerCase()]);

            includeText = (
                <p className="includeText">{andOrText} one or more of the options for filter type {displayType} below
                    are found</p>
            )
        } else {
            includeText = (
                <p className="includeText">{andOrText} none of the options below are found</p>
            )
        }

        return (
            <div>
                <div className="displayFilterDiv">
                    {includeText}
                    <p className="advancedFilterSelectedValues">{typesString}</p>

                    <div className="clr"></div>
                </div>
            </div>
        )
    }
});

export default DisplayFilterSet;
