import React 			from 'react';
import TagFilter 		from './TagFilter.react';
import FiltersStore 	from '../../stores/FiltersStore';

function getCurrentState( props ) {

	return {

		tags 	: 	FiltersStore.getClientTypes()

	};
}

var ExcludeTagFilters = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {


		var tags = this.state.tags.map( function( tag, i ) {

			return <TagFilter type="exclude" tag={tag} key={i} />
		})

		return (

			<div>
				<div className="row">
					<div className="col-md-12">
						<h3>Exclude Tag Filters</h3><br />
					</div>
				</div>

				<div className="row">
					{tags}

				</div>
			</div>

		)
	}
});

export default  ExcludeTagFilters;