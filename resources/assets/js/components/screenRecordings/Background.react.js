import React 					from 'react';
import ScreenRecorderActions 	from '../../actions/ScreenRecorderActions';
import FirstScreenshot 			from './FirstScreenshot.react';

function getCurrentState( props ) {

	return {

		scrollTo 			: 	props.scrollTo,
		play 				: 	props.play,
		background 			: 	props.background,
		firstScreenshot 	: 	props.firstScreenshot,
		aspectRatio 		: 	props.aspectRatio
	}
}

class Background extends React.Component {

	constructor( props ) {

		super( props );
		this.state 						= getCurrentState( props );
		this.state.background 			= false;

	}

	shouldComponentUpdate( nextProps, nextState ) {

		if( this.state.play !== nextState.play ) {

			return true;
		}

		if( this.state.firstScreenshot !== nextState.firstScreenshot ) {

			return true;
		}

		if( this.state.scrollTo !== nextState.scrollTo ) {

			return true;
		}

		if( this.state.background !== nextState.background ) {

			return true;
		}

		return false;
	}

	componentWillReceiveProps( nextProps ) {

		this.setState( getCurrentState( nextProps ) );
	}

	render() {

		let img = "";

		if( this.state.background ) {

			const marginTop 	= 	-Math.abs( this.state.scrollTo );
			const imgSrc 		= 	'/api/proxy/api/v1/screenshot/image/' + this.state.background;

			img = (

				<div style={{ zIndex : 1, marginTop }}>
					<img src={imgSrc}  />
				</div>
			)
		
		} else if( this.state.firstScreenshot ) {

			let firstImage 		=	new Image();
			firstImage.src 		= 	'/api/proxy/api/v1/screenshot/image/' + this.state.firstScreenshot;

			firstImage.onload 	= 	() => this._setImageLoadedHeight( firstImage.height );
			firstImage.width 	= 	this.state.aspectRatio.displayImageWidth;

			const marginTop 	= 	-Math.abs( this.state.scrollTo );

			img = (

				<div style={{ zIndex : 1, marginTop }}>
					<FirstScreenshot 
						imgSrc={firstImage.src} 
					/>
				
				</div>
			)
		}

		return (

			<div>	
				{img}
			</div>
		)
	}

	_setImageLoadedHeight( height) {

		if( height ) {

			ScreenRecorderActions.setNaturalHeight( height );
			
		}
	}
}

export default Background;