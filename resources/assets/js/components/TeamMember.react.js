import React from 'react';
import FilterActions from '../actions/FilterActions';
import Avatar from 'material-ui/Avatar';
import {List,ListItem} from 'material-ui/List';
import TestIcon from 'material-ui/svg-icons/action/alarm-off';

const styles = {
 
  	checkbox: {
    	marginBottom 	: 16,
    	float 			: 'left',
    	width 			: '50%'
  	}
};

import Checkbox from 'material-ui/Checkbox';


var TeamMember = React.createClass({

	getInitialState : function() {
		
		var include = this.props.include;
		
		return {
			
			disabled 			: this.props.disabled,
			includeTeamMembers 	: include,
			member 				: this.props.member,
			checked 			: this.props.selected,
			sendToAll 			: this.props.sendToAll
		}
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( JSON.stringify( this.props.includeTeamMembers ) !== JSON.stringify( nextProps.includeTeamMembers ) ) {

			return true;
		}

		if( this.props.member !== nextProps.member ) {

			return true;
		}

		if( this.props.selected !== nextProps.selected ) {

			return true;
		}

		if( this.props.sendToAll !== nextProps.sendToAll ) {

			return true;
		}

		if( this.props.disabled !== nextProps.disabled ) {

			return true;
		}

		return false;
	},

	componentWillReceiveProps : function( newProps ) {

		var checked = newProps.include.indexOf( this.state.member.id ) > -1;

		this.setState({

			includeTeamMembers 	: newProps.include,
			checked 			: newProps.selected,
			sendToAll 			: newProps.sendToAll,
			disabled 			: newProps.disabled
		})
	},

	render : function() {

		if( typeof this.state.member !== "undefined" ) {

			var forename 	= this.state.member.forename;
			var surname 	= this.state.member.surname;
			var id 			= 'teamMember' + this.state.member.id;
			var name 		= forename + ' ' + surname;
		}
		
		var avatarSrc = "/api/proxy/api/v1/user/avatar/" + this.state.member.id;

		return(
			
			<ListItem
				disabled={this.state.disabled}
				rightAvatar={<Avatar src={avatarSrc} />}
				primaryText={name}
		        insetChildren={true}
				leftCheckbox={<Checkbox checked={this.state.checked} disabled={this.state.disabled} value={id} onCheck={this._addTeamMember} />} />

		);
	},

	_addTeamMember : function() {

		var memberKey 	= this.state.includeTeamMembers.indexOf( parseInt( this.state.member.id ) );

		if( memberKey > -1 ) {

			this.setState({

				checked : false
			})

			FilterActions.removeTeamMemberFromExportCriteria( this.state.member.id );
		
		} else {

			this.setState({

				checked : true
			})

			FilterActions.addTeamMemberToExportCriteria( this.state.member.id );
		}
	}
});

export default  TeamMember;