import React from 'react';
import A2GoogleMap from './A2GoogleMap.react';
import ClientTypes from './prospecting/ClientTypes.react';
import Paper from 'material-ui/Paper';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

var Result = React.createClass({
    getInitialState: function () {
        return {
            relationships: this.props.relationships,
            resultKey: this.props.myKey,
            displayedColumns: this.props.displayedColumns,
            tags: this.props.tags
        }
    },
    componentWillReceiveProps: function () {
        this.setState({
            relationships: this.props.relationships,
            tags: this.props.tags
        })
    },
    handleSelect: function () {
    },
    render: function () {
        let longitude, latitude;

        if (this.props.data.geolocation_longitude !== '' && this.props.data.geolocation_longitude) {
            longitude = this.props.data.geolocation_longitude;
            latitude = this.props.data.geolocation_latitude;
        } else {
            longitude = this.props.data.neustar_longitude;
            latitude = this.props.data.neustar_latitude;
        }

        var displayedColumns = this.state.displayedColumns;
        var displayCheckbox = false;
        var tdStyle = {padding: 0, height: '30px'};
        var trStyle = {height: '30px'};
        var thStyle = {height: '10px', padding: '0'};
        var thRow = {height: '20px', padding: '0'};
        var theader = {height: '20px', padding: '0'};
        var striped = true;
        var adjustForCheckbox = false;
        var tableBodyStyle = {};

        return (
            <Paper zDepth={5}>
                <div className="resultTabs ">
                    <Tabs
                        onSelect={this.handleSelect}
                        selectedIndex={this.state.selectedIndex}
                    >
                        <TabList>
                            <Tab>Results</Tab>
                            <Tab>Location Information</Tab>
                            <Tab>Client Tags</Tab>
                            <Tab>Device Information</Tab>
                        </TabList>

                        <TabPanel>
                            <div className="row resultRow table-responsive">
                                <div className="col-md-12">
                                    <Table>
                                        <TableHeader style={theader} adjustForCheckbox={adjustForCheckbox}
                                                     displaySelectAll={displayCheckbox}>
                                            <TableRow style={thRow}>
                                                <TableHeaderColumn style={thStyle}>Data Type</TableHeaderColumn>
                                                <TableHeaderColumn style={thStyle}>Data Value</TableHeaderColumn>
                                            </TableRow>
                                        </TableHeader>

                                        <TableBody style={tableBodyStyle} stripedRows={striped}
                                                   displayRowCheckbox={displayCheckbox}>

                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.url) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">URL</TableRowColumn>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">{this.props.data.url}</TableRowColumn>
                                            </TableRow>
                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.hostname) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">Hostname</TableRowColumn>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">{this.props.data.hostname}</TableRowColumn>
                                            </TableRow>
                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.pageVisitDate) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle} className="resultTd">Page Visit
                                                    Date</TableRowColumn>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">{this.props.data.start_time}</TableRowColumn>
                                            </TableRow>
                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.duration) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle} className="resultTd">Duration
                                                    (seconds)</TableRowColumn>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">{this.props.data.duration_seconds}</TableRowColumn>
                                            </TableRow>
                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.queryString) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle} className="resultTd">Query
                                                    String</TableRowColumn>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">{this.props.data.query_string}</TableRowColumn>
                                            </TableRow>
                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.ipAddress) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle} className="resultTd">IP
                                                    Address</TableRowColumn>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">{this.props.data.string_ip}</TableRowColumn>
                                            </TableRow>
                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.location) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">Location</TableRowColumn>
                                                <TableRowColumn style={tdStyle}
                                                                className="resultTd">{latitude}, {longitude}</TableRowColumn>
                                            </TableRow>

                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.totalVisitors) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle} className="resultTd">Total Visits to
                                                    URL</TableRowColumn>
                                                <TableRowColumn
                                                    style={tdStyle}
                                                    className="resultTd">{this.props.data.totalvisitors}</TableRowColumn>
                                            </TableRow>
                                            <TableRow style={trStyle}
                                                      className={(displayedColumns.organisation_name) ? 'trDisplay' : 'trHidden'}>
                                                <TableRowColumn style={tdStyle} className="resultTd">Organisation
                                                    Name</TableRowColumn>
                                                <TableRowColumn style={tdStyle} className="resultTd">
                                                    <p className="left">{this.props.data.name} </p>
                                                </TableRowColumn>
                                            </TableRow>

                                        </TableBody>
                                    </Table>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="row resultRow">
                                <div className="col-md-12">
                                    <A2GoogleMap
                                        organisation={this.props.data.name}
                                        longitude={longitude}
                                        latitude={latitude}/>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="row resultRow">
                                <div className="col-md-12">
                                    <ClientTypes
                                        tags={this.state.tags}
                                        organisationId={this.props.data.organisationid}
                                        relationships={this.state.relationships}
                                        organisationid={this.props.data.organisationid}/>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="row resultRow">
                                <div className="col-md-12">
                                </div>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            </Paper>
        )
    },
});

export default Result;
