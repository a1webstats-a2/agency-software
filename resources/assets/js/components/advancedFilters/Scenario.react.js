import React from 'react';
import AdvancedFilterActions from '../../actions/AdvancedFilterActions';
import CreateNewFilter from './CreateNewFilter.react';
import InlineEdit from 'react-edit-inline';
import ScenarioFilters from './ScenarioFilters.react';

function getCurrentState(props) {
    return {
        savedScenarios: props.savedScenarios,
        scenarioCount: props.count,
        setFilters: props.setFilters,
        scenario: props.scenario,
        index: props.index
    }
}

var Scenario = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        var scenario = this.state.scenario;
        let include  = "";

        if (this.state.scenario.currentIncludeType !== -1) {
            include = (this.state.scenario.currentIncludeType) ? "include" : "exclude";
        }

        var filterCount = 0;

        for (var key in scenario.filters) {
            filterCount++;
        }

        var createFilter = <CreateNewFilter
            filterCount={filterCount}
            include={include}
            scenarioName={scenario.name}
            scenarioNameEdited={this.state.scenario.scenarioNameEdited}
            loadScenarioID={this.state.scenario.loadScenarioID}
            savedScenarios={this.state.savedScenarios}
            anyThatMatch={this.state.scenario.currentAnyThatMatch}
            choices={this.state.scenario.currentChoices}
            scenarioID={this.state.scenario.id}
            filterChoiceType={this.state.scenario.choiceType}
            andOr={this.state.scenario.currentAndOr}
        />

        var filters = <ScenarioFilters
            filters={scenario.filters}
            scenarioID={scenario.id}
        />

        var andOrDiv = '';

        if (this.state.index !== this.state.scenarioCount) {
            andOrDiv = (
                <div className="scenarioAndOrDiv">
                    <select value={scenario.scenarioCurrentAndOr} onChange={this._setScenarioCurrentAndOr}
                            className="form-control">
                        <option>AND</option>
                        <option>OR</option>
                    </select>
                    <div className="clr"></div>
                </div>
            )
        } else {
            andOrDiv = (<div className="scenarioAndOrDiv">&nbsp;</div>);
        }

        var spacerDiv = "";

        if (this.state.index % 3 === 0) {
            spacerDiv = (
                <div className="clr"></div>
            )
        }

        return (
            <div>
                <div className="scenarioBox">
                    <div className="row">
                        <div className="col-md-3">
                            <div className="scenarioHeader">
                                <div className="row">
                                    <div className="col-md-10">
                                        <InlineEdit
                                            text={scenario.name}
                                            paramName="message"
                                            change={this._changeScenarioName}
                                            style={{
                                                width: 150,
                                                wordWrap: 'break-word',
                                                display: 'inline-block',
                                                margin: 0,
                                                padding: 0,
                                                fontSize: 20,
                                                outline: 0,
                                                border: 0
                                            }}
                                        />
                                    </div>
                                    <div className="col-md-2">
                                        <span onClick={this._deleteScenario}
                                              className="closeIcon glyphicon glyphicon-remove"
                                              aria-hidden="true"></span>
                                    </div>
                                </div>
                                <br/>
                                <div>
                                    {createFilter}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-9">
                            <div className="displayFiltersContainer">
                                {filters}
                            </div>
                        </div>
                    </div>
                </div>
                {andOrDiv}
                {spacerDiv}
            </div>
        )
    },
    _setScenarioCurrentAndOr: function (event) {
        AdvancedFilterActions.setScenarioCurrentAndOr({
            scenarioID: this.state.scenario.id,
            andOr: event.target.value
        });
    },
    _setAndOr: function (event) {
        AdvancedFilterActions.setAndOrForScenario({
            scenarioID: this.state.scenario.id,
            andOr: event.target.value
        });
    },
    _deleteScenario: function () {
        AdvancedFilterActions.deleteScenario(this.state.scenario.id);
    },
    _changeScenarioName: function (data) {
        AdvancedFilterActions.setScenarioName({
            id: this.state.scenario.id,
            name: data.message,
            editing: false
        });
    }
});

export default Scenario;
