import React from 'react';
import Today from 'material-ui/svg-icons/action/today';
import DatePicker from 'material-ui/DatePicker';
import FilterActions from '../actions/FilterActions';

var DateFromBox  =  React.createClass({

    getInitialState :   function() {

        return {

            myKey           :   this.props.myKey,
            storedValue     :   this.props.storedValue || '2016-01-01'
        };

    },

    componentWillReceiveProps : function( newProps ) {

        this.setState({

            storedValue     :   newProps.storedValue
        })
    },

    shouldComponentUpdate : function( nextProps, nextState ) {

        if( this.props.storedValue !== nextProps.storedValue ) {

            return true;
        }

        return false;
    },

    returnDateFrom :    function( e, date ) {

        this.setState({

            storedValue  :   date
        })
    },

    formatDate  :   function( d ) {

        return d.getFullYear() + '-' + ( "0" + ( d.getMonth() + 1 ) ).slice( -2 ) + '-' + ("0" + d.getDate()).slice(-2)
    },

    getInitialDate : function() {

        return new Date( this.state.storedValue );
    },

    render  :   function() {

        var initialDate     = this.getInitialDate();

        return (

            <div className="dateFromBox form-group">
                <div className="row">

                    <div className="col-md-2">
                        <Today />
                    </div>
                    <div className="col-md-8 overflowHidden">
                        <label className="control-label">Date From:</label>
                        <DatePicker 
                            id="dateFrom"
                            name="dateFrom"
                            autoOk={true}
                            formatDate={this.formatDate} 
                            value={initialDate}
                            onChange={this._updateDate}  />


                    </div>
                    
                </div>

            </div>

        )
    },

    _removeFilter   :   function( e ) {

        FilterActions.removeFilter( this.state.myKey );
    },

    _updateDate     :   function( e, date ) {

        this.setState({

            storedValue : this.formatDate( date )
        })

        FilterActions.editFilter({

            id              :   this.state.myKey,
            storedValue     :   this.formatDate( date ),
            type            :   'dateFrom'
        });
    }  
})

export default  DateFromBox;