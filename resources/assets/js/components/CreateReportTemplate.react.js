import React from 'react';
import FilterActions from '../actions/FilterActions';
import FiltersStore from '../stores/FiltersStore';
import NavBar from './navigation/NavBar.react';
import SnackbarA1 from './Snackbar.react';
import NotificationsStore from '../stores/NotificationsStore';
import Footer from './Footer.react';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import UserTipHelpLink from './UserTipHelpLink.react';
import UserTips from './UserTips.react';

function getCurrentState(props) {

    return {

        scenariosFilterObj: FiltersStore.getScenarioFiltersObj(),

        filters: FiltersStore.getFilters(),
        isApplicationResting: FiltersStore.isApplicationResting(),
        templates: FiltersStore.getExistingTemplates(),
        templateName: '',
        createTemplateStatus: FiltersStore.getCreateTemplateStatus(),
        settings: FiltersStore.getAllSettings(),
        displayFancyFilters: FiltersStore.showFancyFilters(),
        exportOptions: {

            exportSelection: FiltersStore.getExportSelection(),
            teamMembers: FiltersStore.getTeamMembers(),
            include: FiltersStore.getExportCriteria(),
            selectionType: FiltersStore.getExportSelectionType(),
            autoReportOpen: FiltersStore.checkLightboxOpen('newAutomatedReport', 1),
            templates: FiltersStore.getExistingTemplates(),
            automationSettings: FiltersStore.getAutomationSettings(),
            existingAutomatedReport: FiltersStore.getExistingReportData(),
            existingAutomatedReportOpen: FiltersStore.checkLightboxOpen('viewEditExistingReport', 1),
            firstLoadComplete: FiltersStore.isFirstLoadComplete('reportWizard'),
            quickOpen: FiltersStore.getQuickOpenObj(),
            createTemplateStatus: FiltersStore.getCreateTemplateStatus(),
            filters: FiltersStore.getFilters()
        },
        latestAddedFilters: FiltersStore.getLatestAddedFilters(),
        snackbarSettings: FiltersStore.getSnackbarSettings(),
        notificationsData: {
            dateRangeOpen: FiltersStore.checkLightboxOpen('dateRange', -1),
            accountOK: FiltersStore.accountIsOK(),
            snackbarSettings: FiltersStore.getSnackbarSettings(),
            isNewNotifications: NotificationsStore.isNewNotifications(),
            notificationsOpen: NotificationsStore.isNotificationsOpen(),
            numberOfNew: NotificationsStore.getNumberOfNewNotifications(),
            user: FiltersStore.getUser(),
            settings: FiltersStore.getAllSettings(),
            notifications: NotificationsStore.returnNotifications(),
            notificationsBoxOpen: NotificationsStore.isNotificationsOpen(),
            isOpen: NotificationsStore.isNotificationsOpen(),
            newFilters: FiltersStore.haveNewFiltersBeenApplied(),
            paginationData: FiltersStore.getPaginationTotals(),
            allFilters: FiltersStore.getFilters(),
            isApplicationResting: FiltersStore.isApplicationResting(),
            ppcChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 1),
            organicChecked: FiltersStore.checkIfFilterTypeAndIndexExists('trafficType', 2),
            display: NotificationsStore.getDisplay(),
            viewNotificationID: NotificationsStore.viewNotificationID()
        },
        looseFilterSetDisplay: FiltersStore.getLooseFilterSetDisplay()

    }
}

const style = {

    refresh: {

        display: 'inline-block',
        position: 'relative',
    }
}

var CreateReportTemplate = React.createClass({

    getInitialState: function () {

        return getCurrentState(this.props);
    },

    componentDidMount: function () {

        FiltersStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {

        FiltersStore.removeChangeListener(this._onChange);
    },

    shouldComponentUpdate: function (nextProps, nextState) {

        if (this.state.looseFilterSetDisplay !== nextState.looseFilterSetDisplay) {

            return true;
        }

        if (this.state.templateName !== nextState.templateName) {

            return true;
        }

        if (this.state.settings !== nextState.settings) {

            return true;
        }

        if (this.state.exportOptions !== nextState.exportOptions) {

            return true;
        }

        if (this.state.templates !== nextState.templates) {

            return true;
        }

        return false;
    },

    componentWillReceiveProps: function (newProps) {

        this.setState(getCurrentState(newProps));
    },

    setTemplateName: function (data) {

        this.setState({

            templateName: data.target.value
        })
    },

    render: function () {

        var disableButton = false;

        if (this.state.templateName.length < 1) {

            disableButton = true;
        }

        return (

            <div className="reportBuilder">

                <NavBar data={this.state.notificationsData}/>

                <div className="clr"></div>

                <div className="container mainContainer">

                    <div className="row">

                        <div className="col-md-12" id="results">

                            <div className="row">

                                <div className="col-md-4">

                                    <h3>Create New Report Template</h3>

                                </div>

                                <div className="col-md-8">

                                    <div style={{marginTop: 30}}>
                                        <UserTipHelpLink/>
                                    </div>

                                </div>
                            </div>

                            <div className="clr"></div>

                            <div className="form-group">

                                <label>Template Name</label>
                                <input type="text" value={this.state.templateName} onChange={this.setTemplateName}
                                       className="form-control"/>
                            </div>

                            <div className="clr"></div>
                            <br/>


                            <div className="form-group">
                                <RaisedButton label="Create Report Template" disabled={disableButton} primary={true}
                                              onClick={this._saveTemplate}/>
                            </div>

                            <div className="clr"></div>

                            <br/><br/>

                            <RefreshIndicator
                                size={40}
                                left={10}
                                top={0}
                                status={this.state.createTemplateStatus}
                                style={style.refresh}
                            />
                        </div>
                    </div>
                </div>
                <SnackbarA1 snackbarSettings={this.state.snackbarSettings}/>

                <Footer
                    looseFilterSetDisplay={this.state.looseFilterSetDisplay}
                    scenariosFilters={this.state.scenariosFilterObj}
                    isApplicationResting={this.state.isApplicationResting}
                    settings={this.state.settings}
                    filters={this.state.filters}
                    display={this.state.displayFancyFilters}
                    latestAddedFilters={this.state.latestAddedFilters}
                />
            </div>

        )
    },
    _saveTemplate: function () {
        FilterActions.saveReportTemplate({
            templateName: this.state.templateName
        });
    },
    _onChange: function () {
        this.setState(getCurrentState());
    }

});

export default CreateReportTemplate;
