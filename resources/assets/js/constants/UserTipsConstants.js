var keyMirror = require( 'keyMirror' );

module.exports = keyMirror({

	LOAD_ONE_USER_TIP 		: 	null,
	STORE_USER_TIP 			: 	null,
	UPDATE_TIP_OBJ 			: 	null,
	CLOSE_USER_TIP 			: 	null,
	NEVER_SHOW_TIPS_AGAIN 	: 	null,
	SHOW_HELP 				: 	null,
	RESET_USER_TIP 			: 	null,
	SHOW_SPECIFIC_HELP_ITEM	: 	null,
	STORE_AND_SHOW_USER_TIP : 	null
});