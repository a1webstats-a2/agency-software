import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import FilterActions from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {

		exclusionID 	: 	props.exclusionID,
		organisation 	: 	props.organisation

	}
}

var Exclusion = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		return (

			<TableRow key={this.state.e} selectable={false}>
				<TableRowColumn>{this.state.organisation}</TableRowColumn>
				<TableRowColumn><DeleteIcon style={{ color : 'red' }} onClick={this._removeTag} /></TableRowColumn>
			</TableRow>

		)
	},

	_removeTag : function() {

		if( confirm( "Are you sure you want to remove the exclusion for " + this.state.organisation ) ) {

			FilterActions.removeExclusion( this.state.exclusionID );
		}
	}
});

export default Exclusion;