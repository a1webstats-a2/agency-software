<?php

namespace App\Http\Controllers\EndUsers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;


class EndUser extends \App\Http\Controllers\Controller
{

    public function __construct()
    {

    }

    public function index(Request $request)
    {

        echo json_encode($request->session()->get('user'));
    }


    public function getCurrentOauthToken(Request $request)
    {

        echo json_encode($request->session()->get('full_access_token'));
    }

    public function migrationSuccess(Request $request)
    {

        return view('UI.endUsers.migrationSuccess');
    }

    public function storeMigrationNewPassword(Request $request)
    {

        $this->validate($request, [

            'password' => 'same:repeat_password|min:12'
        ]);

        $client = new Client();

        try {
            $client->request('POST', env('API_SOURCE') . '/migrate', [
                    'form_params' => [
                        'key'      => $request->input('key'),
                        'email'    => $request->input('email'),
                        'password' => $request->input('password')
                    ]
                ]
            );

            return redirect('migrate/success');
        } catch (\Exception $e) {
            return redirect('migrate/password?email=' . $request->input('email') . '&key=' . $request->input('key'))
                ->withErrors(['msg' => 'Sorry, there was a problem setting your password. Please try again. If the problem persists, please contact us.']);
        }
    }

    public function migrationNewPassword(Request $request)
    {
        return view('UI.endUsers.migrationNewPassword', [

            'agentData' => getAgentConfigData()
        ]);
    }

    public function cancellationSuccess(Request $request)
    {
        return view('UI.endUsers.cancellationSuccess', [
            'agentData' => getAgentConfigData()
        ]);
    }


    public function cancellationFailure(Request $request)
    {
        return view('UI.endUsers.cancellationFailure', [
            'agentData' => getAgentConfigData()
        ]);
    }

    public function cancelSubscription(Request $request)
    {
        return view('auth.cancel_subscription', [
            'agentData' => getAgentConfigData()
        ]);
    }

    public function postCancelSubscription(Request $request)
    {
        $this->validate($request, [
            'reasonForCancellation' => 'required'
        ]);

        $client = new Client();
        $send   = [
            'client_id'    => session('clientId'),
            'clientId'     => session('clientId'),
            'access_token' => session('token'),
            'user_id'      => session('userId'),
            'password'     => $request->input('password'),
            'reason'       => $request->input('reasonForCancellation')
        ];

        $response = $client->request('POST', env('API_SOURCE') . '/cancel-subscription', [
                'form_params' => $send
            ]
        );

        $contents = $response->getBody()->getContents();

        $result = (trim($contents) === "true") ? true : false;

        if ($result) {
            return redirect("cancellation-success");
        }

        return redirect("cancellation-failure");
    }

    public function loginLegacy(Request $request)
    {
        return view('auth.login-stage', [
            'agentData' => getAgentConfigData()
        ]);
    }

    public function loginWithRedirect(Request $request, $redirectLocation)
    {
        if ($request->session()->has('userId')) {

            if ($request->session('agentData') &&
                session('agentData')->id === 12) {

                return redirect($redirectLocation);
            }
        }

        return view('auth.login', [
            'redirectLocation' => $redirectLocation,
            'agentData'        => getAgentConfigData()
        ]);
    }

    public function loginWithRedirectAndReportID(Request $request, $redirectLocation, $reportID)
    {
        if ($request->session()->has('userId')) {

            if ($request->session('agentData') &&
                session('agentData')->id === 12) {

                $redirectURL = $redirectLocation . '/' . $reportID;

                return redirect($redirectURL);
            }
        }

        return view('auth.login', [
            'redirectLocation' => $redirectLocation,
            'reportID'         => $reportID,
            'agentData'        => getAgentConfigData()
        ]);
    }

    public function postLoginLegacy(Request $request)
    {
        $client = new Client();

        $formParams = $request->input();

        $response = $client->request('POST', env('API_SOURCE') . '/free-access/does-user-exist', [
                'form_params' => $formParams
            ]
        );

        $userID = $response->getBody()->getContents();

        if ($userID) {
            return redirect('auth/login-with-email/' . $userID);
        } else {
            die();
        }
    }

    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'forename'             => 'required|max:100',
            'surname'              => 'required|max:100',
            'tel'                  => 'required|max:50|Regex:/[0-9]{3}/',
            'email'                => 'a2UniqueEmail|email|required|max:100',
            'username'             => 'required|max:100',
            'password'             => 'required|min:12|same:repeatPassword',
            'website'              => 'required|max:200',
            'timezone'             => 'required|max:100',
            'hearAboutA1'          => 'required|max:100',
            'termsAndConditions'   => 'required|max:10',
            'g-recaptcha-response' => 'recaptcha'
        ]);

        $client = new Client();

        $formParams               = $request->input();
        $formParams['ip_address'] = $_SERVER['REMOTE_ADDR'];

        $response = $client->request('POST', env('API_SOURCE') . '/signup', [
                'form_params' => $formParams
            ]
        );

        $response = $response->getBody()->getContents();

        $res = json_decode($response);

        if (isset($res->res)) {
            $loginObj = new Login;
            $loginObj->login($request);

            return redirect('signup-stage-2');

        }
    }

    public function activateLink($link, $email)
    {
        $client = new Client();

        $response = $client->request('POST', env('API_SOURCE') . '/activate-account', [
            'form_params' => [
                'emailAdd' => $email,
                'link'     => $link
            ]
        ]);

        $response = json_decode($response->getBody()->getContents());

        return view('UI.endUsers.activate-link',
            [
                'success'   => $response->success,
                'agentData' => getAgentConfigData()
            ]
        );
    }

    public function signupComplete()
    {
        $client = new Client();

        $response = $client->request('POST', env('API_SOURCE') . '/get-tracker-code', [
            'form_params' => [
                'client_id'    => session('clientId'),
                'clientId'     => session('clientId'),
                'access_token' => session('token'),
                'user_id'      => session('userId')
            ]
        ]);

        $response = $response->getBody()->getContents();

        $trackerCode = json_decode($response);

        return view('UI.endUsers.signup-complete', [
            'trackerCode' => $trackerCode,
            'clientID'    => session('clientId'),
            'agentData'   => getAgentConfigData()
        ]);
    }

    public function postSignupStage2(Request $request)
    {
        $answers = [];

        foreach ($request->input() as $key => $val) {
            if (preg_match('/answer/', $key)) {
                $answerKey = intval(str_replace("answer", "", $key));
            } else {
                $answerKey = 1000;
            }

            $answers[$answerKey] = $val;
        }

        $client = new Client();

        $send = [
            'client_id'    => session('clientId'),
            'clientId'     => session('clientId'),
            'access_token' => session('token'),
            'user_id'      => session('userId'),
            'answers'      => $answers
        ];

        $client->request('POST', env('API_SOURCE') . '/signup-complete', [
                'form_params' => $send
            ]
        );

        return redirect('signup-complete');
    }

    public function signupStage2(Request $request)
    {
        $client = new Client();

        $response = $client->request('GET', env('API_SOURCE') . '/signup-questions', []);
        $response = $response->getBody()->getContents();

        $questions = json_decode($response);

        return view('UI.endUsers.signup-stage-2', ['questions' => $questions, 'agentData' => getAgentConfigData()]);
    }

    public function signup()
    {
        return view('UI.endUsers.signup', array(
                'timezones' => \DateTimeZone::listIdentifiers(\DateTimeZone::ALL),
                'agentData' => getAgentConfigData()
            )
        );
    }
}
