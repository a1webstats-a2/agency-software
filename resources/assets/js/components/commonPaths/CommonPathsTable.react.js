import React from 'react';
import FilterActions from '../../actions/FilterActions';
import {Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn}
  from 'material-ui/Table';

function getCurrentState( props ) {

	return {

		paths  		: props.paths,
		rowSelected : props.rowSelected
	}
}

var CommonPathsTable = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		var rows = this.state.paths.map( function( row, i ) {

			var urls = JSON.parse( row.path );

			var multiple 	= 1;

			var lastURL 	= "";

			var displayURLs = [];

			urls.forEach( function( url, u ) {

				if( lastURL !== url ) {

					displayURLs.push({

						url 	: 	url,
						count 	: 	multiple,
						row 	: 	i
					})
				
				} else {

					displayURLs[(displayURLs.length - 1)].count++;
				}
			
				lastURL = url;
			})

			var urlsDisplay = displayURLs.map( function( url, u ) {

				var multipleText = "";

				if( url.count > 1 ) {

					multipleText = " x " + url.count;
				}

				var urlText = url.url;

				return (

					<p key={u}>{urlText} {multipleText}</p>
				);
			})

			var selected = ( this.state.rowSelected !== false && this.state.rowSelected === i ) ? true : false;

			return (

				<TableRow selected={selected} key={i}>
					<TableRowColumn style={{ width : '60%' }}>{urlsDisplay}</TableRowColumn>
					<TableRowColumn>{row.num_path_repeated}</TableRowColumn>
					<TableRowColumn>{parseFloat( row.percentage ).toFixed( 2 )}</TableRowColumn>
				</TableRow>
			)

		}.bind( this ));


		return (

			<Table
				enableSelectAll={false}
				multiSelectable={false}
	          	fixedHeader={true}
	          	fixedFooter={false}
	          	selectable={true}
	          	onRowSelection={this._setCommonPathSessionIDs}
	        >
				<TableHeader>
					<TableRow>
						<TableHeaderColumn style={{ width : '60%' }}>Visitor URLs</TableHeaderColumn>
						<TableHeaderColumn>Number of Visitors</TableHeaderColumn>
						<TableHeaderColumn>% of Total</TableHeaderColumn>
					</TableRow>
				</TableHeader>
				<TableBody>
					{rows}
				</TableBody>
			</Table>


		);
	},

	_setCommonPathSessionIDs : function( rowI ) {


		var selected = this.state.paths[rowI];

		if( typeof selected === "undefined" ) {

			return false;
		}

		FilterActions.setCommonPathsSelection({

			sessionIDs 	: 	JSON.parse( selected.session_ids ),
			rowSelected : 	rowI 
		});
	}

})

export default  CommonPathsTable;