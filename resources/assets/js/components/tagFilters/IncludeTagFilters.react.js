import React from 'react';
import TagFilter from './TagFilter.react';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import FilterActions from '../../actions/FilterActions';

function getCurrentState(props) {
    const tagsData = props.tagsData;
    return {
        tags: tagsData.tags,
        type: tagsData.type,
        filterChange: tagsData.filterChange
    };
}

const styles = {
    radioButton: {
        marginTop: 10,
        marginBottom: 10
    }
};

const IncludeTagFilters = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (
            this.state.type === nextState.type &&
            this.state.tags === nextState.tags &&
            this.state.filterChange === nextState.filterChange
        ) {
            return false;
        }
        return true;
    },
    render: function () {
        let tags = this.state.tags.map(function (tag, i) {
            return <TagFilter type={this.state.type} tag={tag} key={i}/>
        }.bind(this))

        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <br/><h3>Tag Filters</h3>
                    </div>
                </div>

                <div className="clr"></div>
                <div className="row">
                    <div className="col-md-6">
                        <RadioButtonGroup onChange={this._saveIncludeExcludeOption} name="includeExclude"
                                          defaultSelected="include">
                            <RadioButton
                                value="include"
                                label="Include"
                                style={styles.radioButton}
                            />
                            <RadioButton
                                value="exclude"
                                label="Exclude"
                                style={styles.radioButton}
                            />

                        </RadioButtonGroup>
                    </div>
                </div>

                <div className="clr"></div>
                <br/><br/>

                <div className="row">
                    {tags}

                </div>
            </div>
        )
    },
    _saveIncludeExcludeOption: function (event) {
        FilterActions.setIncludeCustomFilterType(event.target.value);
    }
});
export default IncludeTagFilters;
