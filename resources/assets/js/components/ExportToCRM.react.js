import React from 'react';
import Dialog from 'material-ui/Dialog';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton';
import {Link} from 'react-router'
import FilterActions from '../actions/FilterActions';
import Updating from './Updating.react';

function getCurrentState(props) {
    return {
        allSettings: props.settings,
        updating: props.updatingBox,
        settings: props.settings.team,
        open: props.open,
        selection: props.selection
    }
}

const ExportToCRM = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this._cancel}
            />,
            <FlatButton
                label="Export"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._export}
            />,
        ];

        let salesforceDisabled = true;

        if (this.state.settings.salesforce_organisation_id &&
            this.state.settings.salesforce_organisation_id !== '') {

            salesforceDisabled = false;
        }

        let zapierListItem = ''

        if (12 === this.state.allSettings.agentConfig.id) {
            zapierListItem = (
                <List style={{width: 300}}>
                    <ListItem
                        key={-1}
                        rightAvatar={<Avatar src="/images/crms/zapier.png"/>}
                        primaryText="Zapier"
                        insetChildren={true}
                        leftCheckbox={<Checkbox disabled={false} checked={this.state.selection.zapier}
                                                onCheck={this._setZapierExport} value="zapier"/>}/>
                </List>
            )
        }

        return (
            <div>
                <Dialog
                    title="Export to CRM"
                    modal={false}
                    actions={actions}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    <p>To use Salesforce or Dynamics export, please check out our
                        &nbsp;<Link to="/developer">Developer</Link> page -> Third Party Apps section</p>
                    {zapierListItem}

                    <List style={{width: 300}}>
                        <ListItem
                            key={-1}
                            rightAvatar={<Avatar src="/images/crms/salesforce-small.png"/>}
                            primaryText="Salesforce"
                            insetChildren={true}
                            leftCheckbox={<Checkbox disabled={salesforceDisabled} value="salesforce"
                                                    onCheck={this._setSalesforceExport}/>}/>
                    </List>
                    <Updating updating={this.state.updating}/>
                </Dialog>
            </div>

        )
    },
    _export: function () {
        FilterActions.exportToCRMChoices();
    },
    _cancel: function () {
        FilterActions.closeLightbox();
    },
    _setSalesforceExport: function (event, isChecked) {
        let exportSelection = this.state.selection;
        exportSelection.salesforce = isChecked;

        FilterActions.setCRMExportSelection(exportSelection);
    },
    _setZapierExport: function (event, isChecked) {
        let exportSelection = this.state.selection;
        exportSelection.zapier = isChecked;

        FilterActions.setCRMExportSelection(exportSelection);
    }
});

export default ExportToCRM;
