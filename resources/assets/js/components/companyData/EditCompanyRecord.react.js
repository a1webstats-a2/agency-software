import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Breakdown from './Breakdown.react';
import TrackerActions from '../../actions/TrackedCompaniesActions';
import FilterActions from '../../actions/FilterActions';

function getCurrentState(props) {
    return {
        isSearchingForPossibleOptions: props.isSearchingForPossibleOptions,
        chosenPossibleOption: props.chosenPossibleOption,
        possibleOptions: props.possibleOptions,
        checkedForPossibleOptions: props.checkedForPossibleOptions,
        activeRecord: props.editCompanyRecordData.activeRecord,
        open: props.editCompanyRecordData.open,
        storeType: props.storeType
    }
}

const EditCompanyRecord = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    closeEditRecord: function () {
        FilterActions.closeLightbox();
    },
    render: function () {
        let saveLabel = "Save";

        if (this.state.checkedForPossibleOptions) {
            saveLabel = "Confirm";
        }

        const actions = [
            <FlatButton
                label={saveLabel}
                onClick={this._saveRecord}
                primary={true}/>,
            <FlatButton
                label="Close"
                secondary={true}
                onTouchTap={this.closeEditRecord}/>
        ];

        if (!this.state.activeRecord) {
            return (<div></div>);
        }

        return (
            <div>
                <Dialog
                    title="Edit Organisation Record"
                    modal={false}
                    actions={actions}
                    open={this.state.open}
                    onRequestClose={this.closeEditRecord}
                    autoScrollBodyContent={true}
                >
                    <br/><br/>
                    <Breakdown
                        isSearchingForPossibleOptions={this.state.isSearchingForPossibleOptions}
                        chosenPossibleOption={this.state.chosenPossibleOption}
                        possibleOptions={this.state.possibleOptions}
                        tel={this.state.activeRecord.data.use_organisation_tel}
                        name={this.state.activeRecord.data.use_organisation_name}
                        website={this.state.activeRecord.data.use_organisation_website}
                        companyId={this.state.activeRecord.data.organisationid}
                        activeRecord={this.state.activeRecord}
                        storeType={this.state.storeType}
                    />
                </Dialog>
            </div>
        )
    },
    _saveRecord: function (event) {
        event.preventDefault();

        if (!this.state.checkedForPossibleOptions) {
            FilterActions.findPossibleActiveRecordSuggestions();
        } else {
            switch (this.state.storeType) {
                case "reportBuilder" :
                    FilterActions.saveEndUserCompanyCompanyRecord();

                    break;

                case "organisationDashboard" :
                    FilterActions.updateOrganisationData();

                    break;

                case "tracker" :
                    TrackerActions.updateEndUserCompanyRecord();

                    break;
            }
        }
    }
});

export default EditCompanyRecord;
