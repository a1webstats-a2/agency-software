import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import EmailAlerts from './EmailAlerts.react';
import FilterActions from '../../actions/FilterActions';
import AutoTagging from './AutoTagging.react';
import AutoExportToCMSNew from './AutoExportToCMSNew.react';

function getCurrentState(props) {
    return {
        triggerActionsIsLoadingEmailSendTo: props.triggerActionsIsLoadingEmailSendTo,
        triggerActionSendToList: props.triggerActionSendToList,
        templateID: props.templateID,
        open: props.open,
        teamMembers: props.teamMembers,
        zapierIsSetup: props.zapierIsSetup,
        autoExports: props.autoExports,
    }
}

class EditTriggerActions extends React.Component {
    constructor(props) {
        super(props);

        this.state = getCurrentState(props);
    }

    componentWillReceiveProps(props) {
        this.setState(getCurrentState(props));
    }

    render() {
        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onClick={this._handleClose}
            />
        ];

        return (
            <Dialog
                title="Edit Trigger Actions / Automation"
                actions={actions}
                modal={false}
                autoScrollBodyContent={true}
                open={this.state.open}
                onRequestClose={() => this._handleClose}
            >
                <Tabs>
                    <Tab label="Email Alert">
                        <EmailAlerts
                            isLoading={this.state.triggerActionsIsLoadingEmailSendTo}
                            templateID={this.state.templateID}
                            triggerActionSendToList={this.state.triggerActionSendToList}
                            teamMembers={this.state.teamMembers}/>
                    </Tab>
                    <Tab label="Tagging">
                        <AutoTagging />
                    </Tab>
                    <Tab label="Third Party">
                        <AutoExportToCMSNew
                            autoExports={this.state.autoExports}
                            templateId={this.state.templateID}
                            zapierIsSetup={this.state.zapierIsSetup}
                        />
                    </Tab>
                </Tabs>
            </Dialog>
        )
    }
    _handleClose() {
        FilterActions.closeLightbox();
    }
}

export default EditTriggerActions;
