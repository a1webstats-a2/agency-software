import React from 'react';
import {List, ListItem} from 'material-ui/List';
import FilterActions from '../actions/FilterActions';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Checked from 'material-ui/svg-icons/toggle/radio-button-checked';
import Unchecked from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import Favourite from 'material-ui/svg-icons/action/favorite';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

function getCurrentState( props ) {

	return {

		ppcChecked  	: 	props.ppcChecked,
		organicChecked 	: 	props.organicChecked
	}
}

function twoDigits(d) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlDateOnly = function() {

	return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate());

}

const styles = {

  radioButton: {

    marginBottom 	: 10
  },

  checkbox: {
    marginBottom: 16,
  },
};

function createAllVisitorsFilter() {

	var filterNames = [

		'Organisations',
		'Education',
		'ISPs',
		'Crawl Bots',
		'Public',
		'Unknown'

	]

	var newFilters = [];

	for( var i = 1; i < 7; i++ ) {

		if( i === 4 ) {

			continue;
		}

		newFilters.push({

			type          	:   "visitorTypeFilter", 
			storedValue 	:   {

				id 			: 	i,
				name 		: 	i,
				value 		: 	i,
				textValue 	: 	filterNames[(i-1)]
			},

			id 	: 	i
		})
	}	


	FilterActions.quickLink({

		createFilters 	 		: newFilters,
		removeAllFiltersByType 	: 'visitorTypeFilter'
	});
}


function createOrganicFilterRaw() {

	return {

		type          	:   "visitorTypeFilter", 
		storedValue 	:   {

			id 			: 	1,
			name 		: 	1,
			value 		: 	1,
			textValue 	: 	"Organisations"
		},

		id 	: 	1
	}
}

function createJustOrganisationsFilter() {

	var newFilters = [];

	newFilters.push( createOrganicFilterRaw() );

	FilterActions.quickLink({

		createFilters 	 		: newFilters,
		removeAllFiltersByType 	: 'visitorTypeFilter'
	});

}

function createPPCFilterRaw() {

	return  {

		type          	:   "trafficType", 
		storedValue 	:   {

			id 			: 	1,
			name 		: 	1,
			value 		: 	1,
			textValue 	: 	"PPC"
		},

		id 	: 	1
	}
}

function createTodayDateFiltersRaw() {

	var date 	= new Date();
	var date2 	= new Date();

	date2.setDate( date2.getDate() + 1 );

	var dateFromFilter = {

		storedValue 	: 	date.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	date2.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createTodayFilter() {

	var dateFilters = createTodayDateFiltersRaw();

	FilterActions.quickLink({

		editedFilters : [ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});
}

function createYesterdayFiltersRaw() {

	var date 	= new Date();

	date.setDate( date.getDate() - 1 );

	var date2 	= new Date();

	date2.setDate( date2.getDate() - 1 );

	var dateFromFilter = {

		storedValue 	: 	date.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	date2.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createYesterdayFilter() {

	var dateFilters = createYesterdayFiltersRaw();

	FilterActions.quickLink({

		editedFilters : [ dateFilters.dateFromFilter, dateFilters.dateToFilter ]

	});

}

function createLastWeekFiltersRaw() {

	var date		= new Date();
	date.setDate( date.getDate() - 7 );

	var day 	= date.getDay(),
		diff 	= date.getDate() - day + ( day == 0 ? -6:1 ); 

	var dateMondayLastWeek = new Date( date.setDate( diff ) );
	var dateSundayLastWeek = new Date( date.setDate( date.getDate() + 6 ) );

	var dateFromFilter = {

		storedValue 	: 	dateMondayLastWeek.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	dateSundayLastWeek.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createLastWeekFilter() {

	var dateFilters = createLastWeekFiltersRaw();
	
	FilterActions.quickLink({

		editedFilters : 	[ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});

}

function createLast7DaysFilter() {


	var date 		= new Date();
	var date2 		= new Date();

	date.setDate( date.getDate() - 7 );

	var dateFromFilter = {

		storedValue 	: 	date.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	date2.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	FilterActions.quickLink({

		editedFilters 	: 	[ dateFromFilter, dateToFilter ]
	});

}

function createLastMonthFilters() {

	var lastMonthStartDate = new Date();
	lastMonthStartDate.setDate( 1 );
	lastMonthStartDate.setMonth( lastMonthStartDate.getMonth() -1 );

	var endLastMonthDate = new Date(); 
	endLastMonthDate.setDate( 1 ); 
	endLastMonthDate.setHours( -1 ); 

	var dateFromFilter = {

		storedValue 	: 	lastMonthStartDate.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	endLastMonthDate.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createLastMonthFilter() {

	var dateFilters = createLastMonthFilters();

	FilterActions.quickLink({

		editedFilters 	: 	[ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});

}

function createThisMonthToDateFilters() {

	var monthStartDate = new Date();
	monthStartDate.setDate( 1 );

	var monthEndDate = new Date(); 

	var dateFromFilter = {

		storedValue 	: 	monthStartDate.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	monthEndDate.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createCurrentMonthFilter() {

	var dateFilters = createThisMonthToDateFilters();

	FilterActions.quickLink({

		editedFilters : [ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});
}

function getCreateFilters( value ) {

	var createFilters = [];

	switch( value ) {

		case 4 :

			createFilters.push( createPPCFilterRaw() );

			break

		case 5 :

			createFilters.push( createOrganicFilterRaw() );

			break;


	}

	return createFilters;

}

var QuickLinks = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.ppcChecked !== nextProps.ppcChecked ) {

			return true;
		}

		if( this.props.organicChecked !== nextProps.organicChecked ) {

			return true;
		}

		return false;
	},

	render : function() {

		return (

			<div>	

				<div className="col-md-12">

					Quick Links

					<SelectField floatingLabelStyle={{ fontSize : 12, fontWeight : 'normal', color : '#fff' }} floatingLabelText="Date Range" value={this.state.value} onChange={this._setDateRange}>
						<MenuItem value={1} primaryText="Today" />
						<MenuItem value={2} primaryText="Yesterday" />
						<MenuItem value={3} primaryText="Last Week" />
						<MenuItem value={4} primaryText="This Month to Date" />
						<MenuItem value={5} primaryText="Last Month" />
					</SelectField>

					<SelectField floatingLabelStyle={{ fontSize : 12, fontWeight : 'normal', color : '#fff' }} floatingLabelText="Quick Links" value={this.state.value} onChange={this._setQuickLinkType}>
						<MenuItem value={0} primaryText="Companies" />
						<MenuItem value={1} primaryText="Entry Pages" />
						<MenuItem value={2} primaryText="Visited Pages" />
						<MenuItem value={3} primaryText="Common Visitor Paths" />
						<MenuItem value={4} primaryText="Google Adwords visitors" />
						<MenuItem value={5} primaryText="Google Organic visitors" />
						<MenuItem value={6} primaryText="Keywords" />
						<MenuItem value={7} primaryText="Referrers" />
						<MenuItem value={8} primaryText="Countries" />

					</SelectField>


				</div>
			</div>

		)
	},

	_setDateRange : function( event, index, value ) {

		switch( value ) {

			case 2 :

				var dateFilters = createYesterdayFiltersRaw();

				break;

			case 3 :

				var dateFilters = createLastWeekFiltersRaw();

				break;

			case 4 :

				var dateFilters = createThisMonthToDateFilters();

				break;

			case 5 :

				var dateFilters = createLastMonthFilters();

				break;
		}

		var editFilters = [];

		if( typeof dateFilters !== "undefined" ) {

			editFilters = [ dateFilters.dateFromFilter, dateFilters.dateToFilter ];
		}

		FilterActions.editFilters( editFilters );

	},

	_setQuickLinkType : function( event, index, value ) {

		var createFilters 	= getCreateFilters( value );

		FilterActions.quickLink({

			createFilters 	: 	createFilters 
		});
	}
	
});

export default  QuickLinks;