import React 				from 'react';
import FilterActions 		from '../../actions/FilterActions.js';
import NotificationActions 	from '../../actions/NotificationActions.js';
import NotificationsStore 	from '../../stores/NotificationsStore';
import Notification 		from './Notification.react';
import FiltersStore 		from '../../stores/FiltersStore';
import NotificationsList 	from './NotificationsList.react';
import Badge 				from 'material-ui/Badge';
import Notifications 		from 'material-ui/svg-icons/social/notifications';
import NotificationsActive 	from 'material-ui/svg-icons/social/notifications-active';


function getCurrentState( props ) {

	return {

		snackbarSettings 		: 	props.snackbarSettings,
		notificationsBoxOpen 	: 	props.isOpen,
		numberOfNew 			: 	props.numberOfNew,
		settings 				: 	props.settings,
		isNewNotifications		: 	props.isNew,
		notifications 			: 	props.notifications,
		display 				: 	props.display,
		viewNotificationID 		: 	props.viewNotificationID
	}
}



var NotificationsManager = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props )

	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		var rerender = false;

		if( this.props.isOpen !== nextProps.isOpen ) {

			rerender = true;
		}

		if( this.props.numberOfNew !== nextProps.numberOfNew ) {

			rerender = true;
		}

		if( this.props.display !== nextProps.display ) {

			rerender = true;
		}

		if( JSON.stringify( this.props.settings ) !== JSON.stringify( nextProps.settings ) ) {

			rerender = true;
		}

		if( JSON.stringify( this.props.notifications ) !== JSON.stringify( nextProps.notifications ) ) {

			rerender = true;
		}

		return rerender;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},


    openNotifications : function() {

        NotificationActions.openNotifications();
    },

	render : function() {

		if( this.state.isNewNotifications )  {

            var notificationsIcon = <Badge
            						  onClick={this.openNotifications}	
                                      badgeContent={this.state.numberOfNew}
                                      secondary={true}
                                    >
                                        <NotificationsActive style={{ marginTop : -10, padding: 0, color : '#fff' }} onClick={this.openNotifications}  />
                                    </Badge>
        } else {

            var notificationsIcon = <Badge
                                      badgeContent={this.state.numberOfNew}
                                      secondary={true}
                                    >
                                        <Notifications style={{ marginTop : -10, padding: 0, color : '#fff' }} onClick={this.openNotifications}  />
                                    </Badge>
        }
			
		return(

			<div className="notificationsManager">
				{notificationsIcon}
				<NotificationsList
					snackbarSettings={this.state.snackbarSettings}
					notificationsBoxOpen={this.state.notificationsBoxOpen}
					numberOfNew={this.state.numberOfNew}
					settings={this.state.settings}
					isNewNotifications={this.state.isNewNotifications}
					notifications={this.state.notifications}
					display={this.state.display}
					viewNotificationID={this.state.viewNotificationID}
				/>
			</div>
		)
	}



})

export default  NotificationsManager;