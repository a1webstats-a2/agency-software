import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import EditIcon from 'material-ui/svg-icons/image/edit';
import FilterActions from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {

		ID 			: 	props.tagID,
		description : 	props.tagDescription
	}
}

var Tag = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		return (

			<TableRow selectable={false}>
				<TableRowColumn>{this.state.description}</TableRowColumn>
				<TableRowColumn>
					<EditIcon onClick={this._editTag} style={{ marginRight : 20 }} />
					<DeleteIcon style={{ color : 'red' }} onClick={this._removeTag} />
				</TableRowColumn>
			</TableRow>

		)
	},

	_editTag 	: 	function() {

		FilterActions.editTagType( this.state );
	},

	_removeTag : function() {

		if( confirm( "Are you sure you want to remove the exclusion for " + this.state.description ) ) {

			FilterActions.deleteTag( this.state.ID );
		}
	}
});

export default Tag;