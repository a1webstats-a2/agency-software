import React from 'react';
import Paper from 'material-ui/Paper';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import FilterActions from '../../actions/FilterActions';
const styles = {

	paper 	: 	{

		padding 	: 	20,
		height  	: 	400
	},

	raisedButton : 	{

		marginBottom 	: 	10,
		height 			: 	65
	}
}

function getCurrentState( props ) {

	return {

		dateRange 	: 	props.dateRange,
		trafficType : 	props.trafficType,
		dataType 	: 	props.dataType
					
	}
}

function twoDigits(d) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlDateOnly = function() {

	return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate());

}


function createAllVisitorsFilter() {

	var filterNames = [

		'Organisations',
		'Education',
		'ISPs',
		'Crawl Bots',
		'Public',
		'Unknown'

	]

	var newFilters = [];

	for( var i = 1; i < 7; i++ ) {

		if( i === 4 ) {

			continue;
		}

		newFilters.push({

			type          	:   "visitorTypeFilter", 
			storedValue 	:   {

				id 			: 	i,
				name 		: 	i,
				value 		: 	i,
				textValue 	: 	filterNames[(i-1)]
			},

			id 	: 	i
		})
	}	


	FilterActions.quickLink({

		createFilters 	 		: [], //newFilters,
		removeAllFiltersByType 	: null //'visitorTypeFilter'
	});
}


function createOrganicFilterRaw() {

	return {

		type          	:   "visitorTypeFilter", 
		storedValue 	:   {

			id 			: 	1,
			name 		: 	1,
			value 		: 	1,
			textValue 	: 	"Organisations"
		},

		id 	: 	1
	}
}

function createJustOrganisationsFilter() {

	var newFilters = [];

	newFilters.push( createOrganicFilterRaw() );

	FilterActions.quickLink({

		createFilters 	 		: newFilters,
		removeAllFiltersByType 	: 'visitorTypeFilter'
	});

}

function createPPCFilterRaw() {

	return  {

		type          	:   "trafficType", 
		storedValue 	:   {

			id 			: 	1,
			name 		: 	1,
			value 		: 	1,
			textValue 	: 	"PPC"
		},

		id 	: 	1
	}
}

function createTodayDateFiltersRaw() {

	var date 	= new Date();
	var date2 	= new Date();

	var dateFromFilter = {

		storedValue 	: 	date.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	date2.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createTodayFilter() {

	var dateFilters = createTodayDateFiltersRaw();

	FilterActions.quickLink({

		editedFilters : [ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});
}

function createYesterdayFiltersRaw() {

	var date 	= new Date();

	date.setDate( date.getDate() - 1 );

	var date2 	= new Date();

	date2.setDate( date2.getDate() - 1 );

	var dateFromFilter = {

		storedValue 	: 	date.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	date2.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createYesterdayFilter() {

	var dateFilters = createYesterdayFiltersRaw();

	FilterActions.quickLink({

		editedFilters : [ dateFilters.dateFromFilter, dateFilters.dateToFilter ]

	});

}

function createLastWeekFiltersRaw() {

	var date		= new Date();
	date.setDate( date.getDate() - 7 );

	var day 	= date.getDay(),
		diff 	= date.getDate() - day + ( day == 0 ? -6:1 ); 

	var dateMondayLastWeek = new Date( date.setDate( diff ) );
	var dateSundayLastWeek = new Date( date.setDate( date.getDate() + 6 ) );

	var dateFromFilter = {

		storedValue 	: 	dateMondayLastWeek.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	dateSundayLastWeek.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createLastWeekFilter() {

	var dateFilters = createLastWeekFiltersRaw();
	
	FilterActions.quickLink({

		editedFilters : 	[ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});

}

function createLast7DaysFilter() {


	var date 		= new Date();
	var date2 		= new Date();

	date.setDate( date.getDate() - 7 );

	var dateFromFilter = {

		storedValue 	: 	date.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	date2.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}

}

function createLastMonthFilters() {

	var lastMonthStartDate = new Date();
	lastMonthStartDate.setDate( 1 );
	lastMonthStartDate.setMonth( lastMonthStartDate.getMonth() -1 );

	var endLastMonthDate = new Date(); 
	endLastMonthDate.setDate( 1 ); 
	endLastMonthDate.setHours( -1 ); 

	var dateFromFilter = {

		storedValue 	: 	lastMonthStartDate.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	endLastMonthDate.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createLastMonthFilter() {

	var dateFilters = createLastMonthFilters();

	FilterActions.quickLink({

		editedFilters 	: 	[ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});

}

function createThisMonthToDateFilters() {

	var monthStartDate = new Date();
	monthStartDate.setDate( 1 );

	var monthEndDate = new Date(); 

	var dateFromFilter = {

		storedValue 	: 	monthStartDate.toMysqlDateOnly(),
		type 			: 	'dateFrom',
		id 				: 	0
	}

	var dateToFilter = {

		storedValue 	: 	monthEndDate.toMysqlDateOnly(),
		type 			: 	'dateTo',
		id 				: 	1
	}

	return {

		dateFromFilter 	: dateFromFilter,
		dateToFilter 	: dateToFilter
	}
}

function createCurrentMonthFilter() {

	var dateFilters = createThisMonthToDateFilters();

	FilterActions.quickLink({

		editedFilters : [ dateFilters.dateFromFilter, dateFilters.dateToFilter ] 
	});
}



function getCreateFilters( value ) {

	var createFilters = [];

	switch( value ) {

		case 4 :

			createFilters.push( createPPCFilterRaw() );

			break

		case 5 :

			createFilters.push( createOrganicFilterRaw() );

			break;


	}

	return createFilters;

}

var QuickReportBuilder = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.dateRange !== nextProps.dateRange ) {

			return true;
		}

		if( this.props.trafficType !== nextProps.trafficType ) {

			return true;
		}

		if( this.props.dataType !== nextProps.dataType ) {

			return true;
		}

		return false;
	},


	render : function() {

		return (

			<div className="quickReportBuilder">

				<Paper style={styles.paper} zDepth={4}>

					<div className="col-md-12">

						<div className="blueHeader">

							<h4>Quick Report Builder</h4>
						</div>

						<div className="row">

							<SelectField fullWidth={true} onChange={this._setDateRange} value={this.state.dateRange} floatingLabelStyle={{ fontSize : 12, fontWeight : 'normal', color : '#fff' }} floatingLabelText="Date Range">
								<MenuItem value={1} primaryText="Today" />
								<MenuItem value={2} primaryText="Yesterday" />
								<MenuItem value={3} primaryText="Last Week" />
								<MenuItem value={4} primaryText="Last 7 Days" />
								<MenuItem value={5} primaryText="This Month to Date" />
								<MenuItem value={6} primaryText="Last Month" />
								<MenuItem value={7} primaryText="Custom" />
							</SelectField>

						</div>

						<div className="clr"></div><br />

						<div className="row">

							<SelectField fullWidth={true} onChange={this._setDataType} value={this.state.dataType} floatingLabelStyle={{ fontSize : 12, fontWeight : 'normal', color : '#fff' }} floatingLabelText="Traffic Type">
								<MenuItem value={5} primaryText="All Visitors" />
								<MenuItem value={11} primaryText="All Visitors via Google PPC" />
								<MenuItem value={14} primaryText="All Visitors via Google Organic" />
								<MenuItem value={12} primaryText="All Visitors via Bing PPC" />
								<MenuItem value={15} primaryText="All Visitors via Bing Organic" />
								<MenuItem value={6} primaryText="Organisations" />
								<MenuItem value={8} primaryText="Referrers" />
								<MenuItem value={7} primaryText="Keywords" />
								<MenuItem value={9} primaryText="All Pages" />
								<MenuItem value={10} primaryText="Entry Pages" />
								
							</SelectField>

						</div>

						<div className="clr"></div><br /><br />

						<div className="row">

						    <RaisedButton secondary={true} label="Go" fullWidth={true} onClick={this._createQuickReport} className="customPrimaryButton" />

						</div>
					</div>
				</Paper>
			</div>
		)
	},

	_setDataType : function( event, key, val ) {

		FilterActions.setQuickReportDataType( val );
	},

	_setTrafficType : function( event, key, val ) {

		FilterActions.setQuickReportTrafficType( val );
	},

	_setDateRange : function( event, key, val ) {

		FilterActions.setQuickReportDateRange( val );
	},

	_createQuickReport : function( event ) {

		event.preventDefault();
		
		var dateFilters 	= [];

		var createFilters 	= [];

		switch( this.state.dateRange ) {

			case 1 :

				dateFilters = createTodayDateFiltersRaw();
	
				break;

			case 2 :

				dateFilters = createYesterdayFiltersRaw();

				break;

			case 3 :

				dateFilters = createLastWeekFiltersRaw();

				break;

			case 4 :

				dateFilters = createLast7DaysFilter();

				break;

			case 5 :

				dateFilters = createThisMonthToDateFilters();

				break;

			case 6 :

				dateFilters = createLastMonthFilters();

				break;

		}

		switch( this.state.dataType ) {

			case 11 :

				createFilters.push({

		   			type          :   'ppc', 
		   			storedValue   :   {

		   				criteria 	: 	'include',
		   				id 			: 	'googlePPC',
		   				value 		: 	"Google", 
		   				textValue 	: 	"Google"
		   			},
		   			id 				: 	'googlePPC',
					
		   		});

				break;

			case 12 :

				createFilters.push({

		   			type          :   'ppc', 
		   			storedValue   :   {

		   				criteria 	: 	'include',
		   				id 			: 	'bingPPC',
		   				value 		: 	"Bing", 
		   				textValue 	: 	"Bing"
		   			},
		   			id 				: 	'bingPPC',
					
		   		});

				break;

			case 13 :

				createFilters.push({

		   			type          :   'ppc', 
		   			storedValue   :   {

		   				criteria 	: 	'include',
		   				id 			: 	'linkedinPPC',
		   				value 		: 	"LinkedIn", 
		   				textValue 	: 	"LinkedIn"
		   			},
		   			id 				: 	'linkedinPPC',
					
		   		});

				break;

			case 14 :

				createFilters.push({

		   			type          :   'organic', 
		   			storedValue   :   {

		   				criteria 	: 	'include',
		   				id 			: 	'googleOrganic',
		   				value 		: 	"Google", 
		   				textValue 	: 	"Google"
		   			},
		   			id 				: 	'googleOrganic',
					
		   		});

				break;

			case 15 :

				createFilters.push({

		   			type          :   'organic', 
		   			storedValue   :   {

		   				criteria 	: 	'include',
		   				id 			: 	'bingOrganic',
		   				value 		: 	"Bing", 
		   				textValue 	: 	"Bing"
		   			},
		   			id 				: 	'bingOrganic',
					
		   		});

				break;

			case 16 :

				createFilters.push({

		   			type          :   'organic', 
		   			storedValue   :   {

		   				criteria 	: 	'include',
		   				id 			: 	'linkedinOrganic',
		   				value 		: 	"LinkedIn", 
		   				textValue 	: 	"LinkedIn"
		   			},
		   			id 				: 	'linkedinOrganic',
					
		   		});

				break;
		}

		var sendDateFilters = false;

		if( this.state.dateRange != 7 ) {

			sendDateFilters = [ dateFilters.dateFromFilter, dateFilters.dateToFilter ];		
		}

		FilterActions.quickLink({

			editedFilters 	: 	sendDateFilters,
			createFilters 	: 	createFilters,
			viewType 		: 	this.state.dataType
		})
	}
});

export default  QuickReportBuilder;