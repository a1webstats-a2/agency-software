import React from 'react';

import TrackedCompaniesStore 	from '../../stores/TrackedCompaniesStore';
import TrackedCompaniesActions 	from '../../actions/TrackedCompaniesActions';
import FiltersStore 			from '../../stores/FiltersStore';
import FilterActions 			from '../../actions/FilterActions';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';


function getCurrentState( props ) {

	var trackedOptions 	= TrackedCompaniesStore.getTrackedOptions();

	var defaultValue 	= "Track - Unassigned";

	if( trackedOptions ) {


		if( trackedOptions.assigned_user ) {

			defaultValue = trackedOptions.assigned_user;

			if( trackedOptions.assigned_user != "Track - Unassigned" &&
				trackedOptions.assigned_user != "Don't track" ) {

				defaultValue = 'teamMember' + trackedOptions.assigned_user;
			}
		}
	}

	return {
			
		defaultValue 		: 	defaultValue,
		currentTrackingObj 	: 	TrackedCompaniesStore.getTrackedOptions(),
		open 				: 	FiltersStore.checkLightboxOpen( 'assignToTeamMemberInCompanyTracker', 1 ),
		teamMembers 		: 	FiltersStore.getTeamMembers()
	}
} 

const styles = {

  
  	radioButton : {

  		marginTop 		: 10,
  		marginBottom	: 10
  	}
};

var AssignToTeamMember = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.state === nextState ) {

			return false;
		}

		return true;
	},

	handleClose : function() {

		FilterActions.closeLightbox();
	},

	render : function() {

		var actions = [

			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this.handleClose} />,
		
			<FlatButton
				label="Confirm"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this._trackedAssignToTeamMember} />

		];

		var teamMemberRadioButtons = this.state.teamMembers.map( function( teamMember, i ) {

			var val 	= 'teamMember' + teamMember.id;

			var label 	= (

				<div>
					{teamMember.forename} {teamMember.surname}
				</div>
			)

			return (

				<RadioButton 
					style={styles.radioButton}
					key={i}
					value={val}
					label={label} />

			)
		});



		return (

			<div>

				<Dialog
					title="Assign to Team Member"
					actions={actions}
					modal={false}
					open={this.state.open}
					onRequestClose={this.handleClose} >
					<RadioButtonGroup name="assignedTo" valueSelected={this.state.defaultValue} onChange={this._setTeamMember} >

						<RadioButton style={styles.radioButton} key={-2} value="Don't track" label="Don't Track" />

						<RadioButton style={styles.radioButton} key={-1} value="Track - Unassigned" label="Track - Unassigned" />

						{teamMemberRadioButtons}

					</RadioButtonGroup>

				</Dialog>

			</div>

		)
	},

	_setTeamMember : function( event, value ) {

		TrackedCompaniesActions.setCurrentTrackedObjAssignedToTeamMember( value.replace( 'teamMember', '' ) );
	},

	_trackedAssignToTeamMember : function() {

		TrackedCompaniesActions.trackCompaniesSaveAssignedTo();
	},

	_handleRequestClose : function() {

		FilterActions.closeLightbox();
	}
});

export default  AssignToTeamMember;