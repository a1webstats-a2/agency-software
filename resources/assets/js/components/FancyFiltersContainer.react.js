import React from 'react';
import A1Chip from './A1Chip.react';
import A1ScenarioChip from './A1ScenarioChip.react';
import FilterTypeSet from './FilterTypeSet.react';

function getCurrentState(props) {
    return {
        noResultsStatus: props.noResultsStatus,
        latestAddedFilters: props.latestAddedFilters,
        filters: props.filters
    }
}

var FancyFiltersContainer = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.filters !== nextProps.filters) {
            return true;
        }

        if (this.props.noResultsStatus !== nextProps.noResultsStatus) {
            return true;
        }

        return false;
    },
    render: function () {
        if (!this.state.filters || this.state.filters.length === 0) {
            return (<div></div>);
        }

        var filters = this.state.filters;
        var containsScenarioFilter = false;

        for (var key in filters) {
            var filter = filters[key];

            if (typeof filter !== "undefined") {
                if (filter.type === "scenarioFilter") {
                    containsScenarioFilter = true;
                }
            }
        }

        let displayFilters = [];
        let pagesFilterSet = [];
        let countriesSet = [];
        let entryPageSet = [];
        let keywordsSet = [];
        let organisationsSet = [];
        let referrersSet = [];
        let looseFilters = [];

        for (var key in filters) {
            if (!filters[key]) {
                continue;
            }

            var filter = filters[key];

            switch (filters[key].type) {
                case "ppc" :
                    var text = filters[key].storedValue.textValue;

                    looseFilters.push(
                        <A1Chip noResultsStatus={this.state.noResultsStatus}
                                latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='ppc' type='PPC'
                                text={text} filter={filters[key]}/>
                    );

                    break;
                case "organic" :
                    var text = filters[key].storedValue.textValue;

                    looseFilters.push(
                        <A1Chip noResultsStatus={this.state.noResultsStatus}
                                latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='organic'
                                type='Organic' text={text} filter={filters[key]}/>
                    );

                    break;
                case "setSessionIDs" :
                    var text = filters[key].storedValue.textValue;

                    displayFilters.push(
                        <A1Chip
                            latestAddedFilters={this.state.latestAddedFilters}
                            key={key}
                            avatar='previousHistory'
                            type='Visit History'
                            text={text}
                            filter={filters[key]}/>
                    );

                    break;
                case "trafficType" :
                    looseFilters.push(
                        <A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='traffic'
                                type='Traffic Type' text={filters[key].storedValue.textValue} filter={filters[key]}/>
                    );

                    break;
                case "banked" :
                    displayFilters.push(
                        <A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='bank'
                                type='Banked Filter' text='' filter={filters[key]}/>
                    );

                    break;
                case "tagFilter" :
                    looseFilters.push(
                        <A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='tag'
                                type='Include Tag' text={filters[key].storedValue.textValue} filter={filters[key]}/>
                    );

                    break;
                case "CustomFilterKeyword" :
                    keywordsSet.push(filter);

                    break;
                case "CustomFilterURL" :
                    pagesFilterSet.push(filter);

                    break;
                case "customFilterTotalVisits" :
                    looseFilters.push(
                        <A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='numbered'
                                type='Total Visits' text={filters[key].storedValue.value} filter={filters[key]}/>
                    );

                    break;
                case "CustomFilterEntryPage" :
                    entryPageSet.push(filter);

                    break;
                case "CustomFilterLastPage" :
                    looseFilters.push(
                        <A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='pageView'
                                type='Final Page' text={filters[key].storedValue.textValue} filter={filters[key]}/>
                    );

                    break;
                case "customFilterReferrerCountry" :
                    countriesSet.push(filter);

                    break;
                case "customFilterReferrer" :
                    referrersSet.push(filter);

                    break;
                case "customFilterTimeOnSite" :
                    looseFilters.push(
                        <A1Chip latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='timeline'
                                type='Time On URL' text={filters[key].storedValue.value} filter={filters[key]}/>
                    );

                    break;
                case "organisationFilter" :
                    if (typeof filters[key].storedValue !== "undefined" &&
                        typeof filters[key].storedValue.name !== "undefined" &&
                        typeof filters[key].storedValue.name) {

                        var stringArray = filters[key].storedValue.name.split(" ");

                        var returnString = stringArray.map(function (word, i) {

                            if (word.indexOf("Ftip") === -1) {
                                return word;
                            } else {
                                return "";
                            }
                        })

                        filter.storedValue.textValue = returnString.join(" ").trim();
                        organisationsSet.push(filter);
                    }

                    break;

                case "scenarioFilter" :
                    looseFilters.push(
                        <A1ScenarioChip noResultsStatus={this.state.noResultsStatus}
                                        latestAddedFilters={this.state.latestAddedFilters} key={key} avatar='business'
                                        type='Scenario' text={filters[key].storedValue.name} filter={filters[key]}/>
                    )

                    break;
            }
        }

        var organisationVisitorTypeObj = {
            storedValue: {
                id: 1,
                name: 1,
                value: 1,
                textValue: 'Organisations'
            },
            type: 'visitorTypeFilter',
            round: 0
        };

        var educationVisitorTypeObj = {
            storedValue: {
                id: 2,
                name: 2,
                value: 2,
                textValue: 'Education'
            },
            type: 'visitorTypeFilter',
            round: 0

        };

        var ISPvisitorTypeObj = {
            storedValue: {
                id: 3,
                name: 3,
                value: 3,
                textValue: 'ISPs'
            },
            type: 'visitorTypeFilter',
            round: 0
        };
        var unknownVisitorTypeObj = {
            storedValue: {
                id: 6,
                name: 6,
                value: 6,
                textValue: 'Unknown'
            },
            type: 'visitorTypeFilter',
            round: 0
        }

        var visitorTypeFilters = [];
        visitorTypeFilters.push(organisationVisitorTypeObj);
        visitorTypeFilters.push(educationVisitorTypeObj);
        visitorTypeFilters.push(ISPvisitorTypeObj);
        visitorTypeFilters.push(unknownVisitorTypeObj);

        if (!containsScenarioFilter) {
            displayFilters.push(<FilterTypeSet noResultsStatus={this.state.noResultsStatus} key={1} avatar="SocialIcon"
                                               typeDisplay="Visitor Types" filters={visitorTypeFilters}/>);
        }

        if (pagesFilterSet.length > 0) {
            key++;

            displayFilters.push(
                <FilterTypeSet noResultsStatus={this.state.noResultsStatus} key={key} avatar="PageView"
                               type="customFilterURL" typeDisplay="Pages" filters={pagesFilterSet}/>
            )
        }

        if (countriesSet.length > 0) {
            key++;

            displayFilters.push(
                <FilterTypeSet noResultsStatus={this.state.noResultsStatus} key={key} avatar="Location" type="Country"
                               typeDisplay="Countries" filters={countriesSet}/>
            )
        }

        if (entryPageSet.length > 0) {
            key++;

            displayFilters.push(
                <FilterTypeSet noResultsStatus={this.state.noResultsStatus} key={key} avatar="PageView"
                               type="Entry Page" typeDisplay="Entry Pages" filters={entryPageSet}/>
            )
        }

        if (keywordsSet.length > 0) {
            key++;

            displayFilters.push(
                <FilterTypeSet noResultsStatus={this.state.noResultsStatus} key={key} avatar="Key" type="Keywords"
                               typeDisplay="Keywords" filters={keywordsSet}/>
            )
        }

        if (organisationsSet.length > 0) {
            key++;

            displayFilters.push(
                <FilterTypeSet noResultsStatus={this.state.noResultsStatus} key={key} avatar="Business"
                               type="Organisations" typeDisplay="Organisations" filters={organisationsSet}/>
            )
        }

        if (referrersSet.length > 0) {
            key++;

            displayFilters.push(
                <FilterTypeSet noResultsStatus={this.state.noResultsStatus} key={key} avatar="ReferrerIcon"
                               type="Referrers" typeDisplay="Referrers" filters={referrersSet}/>
            )
        }

        displayFilters.reverse();

        return (
            <div style={{width: '100%'}}>
                {looseFilters}
                {displayFilters}
            </div>
        )
    }
});

export default FancyFiltersContainer;
