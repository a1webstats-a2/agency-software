import React from 'react';
import Template from './Template.react';
import AutomatedReport from './export/AutomatedReport.react';
import ExistingReports from './export/ExistingReports.react';

function getCurrentState(props) {
    return {
        existingReportsData: props.existingReportsData,
        settings: props.settings,
        exportOptions: props.exportOptions,
        autoReportOpen: props.autoReportOpen,
        existingReportOpen: props.existingReportOpen
    }
}

const ExistingTemplates = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.settings !== nextProps.settings) {
            return true;
        }
        if (JSON.stringify(this.props.templates) !== JSON.stringify(nextProps.templates)) {
            return true;
        }
        if (this.props.existingReportsData !== nextProps.existingReportsData) {
            return true;
        }
        if (this.props.autoReportOpen !== nextProps.autoReportOpen) {
            return true;
        }
        if (this.props.exportOptions !== nextProps.exportOptions) {
            return true;
        }

        return false;
    },
    render: function () {
        let hasDefaultTemplate = false;

        this.state.exportOptions.templates.forEach(function (template) {
            if (template.load_at_login) {
                hasDefaultTemplate = true;
            }
        })

        const templates = this.state.exportOptions.templates.map(function (template, i) {
            return <Template
                key={i}
                myKey={i}
                hasDefaultTemplate={hasDefaultTemplate}
                settings={this.state.settings}
                exportOptions={this.state.exportOptions}
                data={template}/>

        }.bind(this));

        return (
            <div className="existingTemplates">
                <table className="table">
                    <thead>
                    <tr>
                        <th>Report Name</th>
                        <th>Actions</th>
                        <th>Automation</th>
                        <th>Display in Dashboard</th>
                        <th>Set as Default Filters</th>
                        <th>Trigger Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {templates}
                    </tbody>
                </table>

                <AutomatedReport open={this.state.autoReportOpen} settings={this.state.settings}
                                 exportOptions={this.state.exportOptions}/>
                <ExistingReports
                    existingReportsData={this.state.existingReportsData}
                    open={this.state.existingReportOpen}
                    settings={this.state.settings}
                    exportOptions={this.state.exportOptions}/>
            </div>
        )
    }
});

export default ExistingTemplates;
