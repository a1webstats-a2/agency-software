import ReportBuilderDispatcher from '../dispatcher/ReportBuilderDispatcher';
import FilterConstants from '../constants/FilterConstants';

const FilterActions = {
    loadNewFormatOrganisations: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_NEW_FORMAT_ORGANISATIONS
        })
    },
    loadServiceStatus: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_SERVICE_STATUS
        })
    },
    saveAutoTags: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_AUTO_TAGS
        })
    },
    storeAutoTags: function (autoTags) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_AUTO_TAGS,
            data: autoTags
        })
    },
    updateUserPermissions: function (permissions) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_USER_PERMISSIONS,
            data: permissions
        })
    },
    storeUserCommunicationPreferences: function (preferences) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_USER_COMMUNICATION_PREFERENCES,
            data: preferences
        })
    },
    loadCommunicationPreferences: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_COMMUNICATION_PREFERENCES
        })
    },
    closeLoginAndUpdateResults: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CLOSE_LOGIN_AND_UPDATE_RESULTS
        })
    },
    setRenewTokenDidSucceed: function (didSucceed) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_RENEW_TOKEN_DID_SUCCEED,
            data: didSucceed
        })
    },
    attemptLogin: function (loginCredentials) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ATTEMPT_LOGIN,
            data: loginCredentials
        })
    },
    storeWebSocketMessage: function (msg) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_WEBSOCKET_MSG,
            data: msg
        })
    },
    storeDefaultTriggerActionsEmailSendToList: function (sendTo) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_DEFAULT_TRIGGER_ACTIONS_EMAIL_SEND_TO_LIST,
            data: sendTo
        })
    },
    updateTriggerActionEmailSendToList: function (sendToData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_TRIGGER_ACTION_EMAIL_SEND_TO_LIST,
            data: sendToData
        })
    },
    saveChangesToTriggerActionEmailAlerts: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_CHANGES_TO_TRIGGER_ACTION_EMAIL_ALERTS
        })
    },
    showOrganisationInLightboxAndSetIndex: function (rowData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_ORGANISATION_IN_LIGHTBOX_AND_SET_INDEX,
            data: rowData
        })
    },
    openTriggerActions: function (templateID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_TRIGGER_ACTIONS,
            data: templateID
        })
    },
    showFiltersInConsole: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_FILTERS_IN_CONSOLE
        })
    },
    setNoResultsReportMsg: function (newText) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_NO_RESULTS_REPORT_MSG,
            data: newText
        })
    },
    logActivity: function (activityMsg) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOG_ACTIVITY,
            data: activityMsg
        })
    },
    reportNoResults: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REPORT_NO_RESULTS
        })
    },
    showReportNoResultsBox: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_REPORT_NO_RESULTS_BOX
        })
    },
    storeTrackerCode: function (trackerCode) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_TRACKER_CODE,
            data: trackerCode
        })
    },
    setDynamicsCallbackURL: function (callbackURL) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_DYNAMICS_CALLBACK_URL,
            data: callbackURL
        })
    },
    loadTrackerCode: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_TRACKER_CODE
        })
    },
    setAlertsForTemplate: function (alertObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ALERTS_FOR_TEMPLATE,
            data: alertObj
        })
    },
    openHelpWindow: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_HELP_WINDOW
        })
    },
    setSalesforceOrganisationID: function (salesforceID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_SALESFORCE_ID,
            data: salesforceID
        })
    },
    setOrgHasNoImage: function (orgID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ORG_HAS_NO_IMAGE,
            data: orgID
        })
    },
    updateReportData: function (reportData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_REPORT_DATA,
            data: reportData
        })
    },
    submitReportRecord: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SUBMIT_REPORT_RECORD
        })
    },
    reportRecord: function (orgID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REPORT_RECORD,
            data: orgID
        })
    },
    sendTestWebhookData: function (hookURLs) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SEND_TEST_WEBHOOK_DATA,
            data: hookURLs
        })
    },
    setFlatPageVisits: function (pageVisits) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_FLAT_PAGE_VISITS,
            data: pageVisits
        })
    },
    setConcatenatePageVisits: function (set) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CONCATENATE_PAGE_VISITS,
            data: set
        })
    },
    showOrganisationInLightbox: function (organisation) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_ORGANISATION_IN_LIGHTBOX,
            data: organisation
        })
    },
    storeNewFormatOrganisations: function (organisations) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_NEW_FORMAT_ORGANISATIONS,
            data: organisations
        })
    },
    setSendOrganisationByEmailAttachSpreadsheet: function (isChecked) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_SEND_ORGANISATION_BY_EMAIL_ATTACH_SPREADSHEET,
            data: isChecked
        })
    },
    startBulkCRMExport: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.START_BULK_CRM_EXPORT
        })
    },
    openChooseExportType: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_CHOOSE_EXPORT_TYPE
        })
    },
    setBulkOrganisationExportType: function (option) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_BULK_ORGANISATION_EXPORT_TYPE,
            data: option
        })
    },
    deleteTag: function (tagID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DELETE_TAG,
            data: tagID
        })
    },
    setOrganisationByEmailText: function (text) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ORGANISATION_BY_EMAIL_TEXT,
            data: text
        })
    },
    showClientSecret: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_CLIENT_SECRET
        })
    },
    updateTagDescription: function (newDescription) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_TAG_DESCRIPTION,
            data: newDescription
        })
    },
    storeNewTagDescription: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_NEW_TAG_DESCRIPTION
        })
    },
    editTagType: function (tag) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.EDIT_TAG,
            data: tag
        })
    },
    removeExclusion: function (exclusionID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_EXCLUSION,
            data: exclusionID
        })
    },
    storeExclusions: function (exclusions) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_EXCLUSIONS,
            data: exclusions
        })
    },
    updateVATNumber: function (vatNumber) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_VAT_NUMBER,
            data: vatNumber
        })
    },
    loadExclusions: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_EXCLUSIONS
        })
    },
    updateLoadOnLoginForTemplate: function (updateObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_LOAD_ON_LOGIN_FOR_TEMPLATE,
            data: updateObj
        })
    },
    showFiltersInLightbox: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_FILTERS_IN_LIGHTBOX,
            data: filters
        })
    },
    advancedFiltersStraightToExport: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ADVANCED_FILTERS_STRAIGHT_TO_EXPORT,
            data: filters
        })
    },
    storePricing: function (pricing) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_PRICING,
            data: pricing
        })
    },
    showScenarioFilters: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_SCENARIO_FILTERS,
            data: filters
        })
    },
    editTeamSettingsInStoreOnly: function (settings) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.EDIT_TEAM_SETTINGS_IN_STORE_ONLY,
            data: settings
        })
    },
    saveTeamSettings: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_TEAM_SETTINGS,
        })
    },
    updateInvoiceTo: function (invoiceTo) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_INVOICE_TO,
            data: invoiceTo
        })
    },
    storeInvoices: function (invoices) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_INVOICES,
            data: invoices
        })
    },
    loadInvoices: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_INVOICES
        })
    },
    selectAllOptionsIfSelectionEmpty: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SELECT_ALL_OPTIONS_IF_SELECTION_EMPTY,
        })
    },
    createScenarioFilters: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_SCENARIO_FILTERS,
            data: filters
        })
    },
    removeAllButDateFilters: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_ALL_BUT_DATE_FILTERS
        })
    },
    setPossibleActiveRecordSuggestions: function (suggestions) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_POSSIBLE_ACTIVE_RECORD_SUGGESTIONS,
            data: suggestions
        })
    },
    setPossibleOption: function (organisationID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_POSSIBLE_OPTION,
            data: organisationID
        })
    },
    exportToCRMChoices: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.EXPORT_TO_CRM_CHOICES
        })
    },
    openExportToCRM: function (organisationID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_EXPORT_TO_CRM,
            data: organisationID
        })
    },
    setCRMExportSelection: function (selection) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CRM_EXPORT_SELECTION,
            data: selection
        })
    },
    setFiltersAndLoad: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_FILTERS_AND_LOAD,
            data: filters
        })
    },
    loadFiltersAndInitialData: function (reportID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_FILTERS_AND_INITIAL_DATA,
            data: reportID
        })
    },
    loadFilters: function (reportID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_REPORT_FILTERS,
            data: reportID
        })
    },
    setQuickReportDateRange: function (dateRange) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_REPORT_DATE_RANGE,
            data: dateRange
        })
    },
    startQuickExportNewOrganisationsFormat: function (selection) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_EXPORT_EXPORT_NEW_ORGANISATIONS_FORMAT,
            data: selection
        })
    },
    quickExportShowDataNewOrganisations: function (selection) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_EXPORT_SHOW_DATA_NEW_ORGANISATIONS_FORMAT,
            data: selection
        })
    },
    loadPrices: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_PRICES
        })
    },
    saveBillingDetails: function (billingDetails) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_BILLING_DETAILS,
            data: billingDetails
        })
    },
    setQuickExportSearchResults: function (searchText) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_EXPORT_SEARCH_RESULTS,
            data: searchText
        })
    },
    processStripePayment: function (token) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.PROCESS_STRIPE_PAYMENT,
            data: token
        })
    },
    setCommonPathsSelection: function (sessionIDs) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_COMMON_PATHS_SESSION_IDS,
            data: sessionIDs
        })
    },
    showSelectedCommonPath: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_SELECTED_COMMON_PATH
        })
    },
    setLandingPage: function (nextOrPrevious) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_LANDING_PAGE_NUMBER,
            data: nextOrPrevious
        })
    },
    setDisplayPerPageForLandingPages: function (val) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_DISPLAY_PER_PAGE_FOR_LANDING_PAGES,
            data: val
        })
    },
    makePayment: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.MAKE_PAYMENT
        })
    },
    startGoCardlessProcess: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.START_GOCARDLESS_PROCESS,
        })
    },
    setPaymentType: function (paymentType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_PAYMENT_TYPE,
            data: paymentType
        })
    },
    loadLink: function (typeAndID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_LINK,
            data: typeAndID
        })
    },
    setTemplateDisplayedInDashboard: function (templateID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_TEMPLATE_DISPLAYED_IN_DASHBOARD,
            data: templateID
        })
    },
    setAutomatedReportBlocked: function (templateID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_AUTOMATED_REPORT_BLOCKED,
            data: templateID
        })
    },
    setExportCriteriaSendToAll: function (sendToAllBool) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_EXPORT_CRITERIA_SEND_TO_ALL,
            data: sendToAllBool
        })
    },
    storeNewTemplates: function (newTemplates) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_NEW_TEMPLATES,
            data: newTemplates
        })
    },
    setCurrentMap: function (mapObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CURRENT_MAP_OBJ,
            data: mapObj
        })
    },
    openClientTypesLightbox: function (clientTypesObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_CLIENT_TYPES_LIGHTBOX,
            data: clientTypesObj
        })
    },
    addToExcludeSelection: function (organisationID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ADD_TO_EXCLUDE_SELECTION,
            data: organisationID
        })
    },
    setQuickReportDataType: function (dataType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_REPORT_DATA_TYPE,
            data: dataType
        })
    },
    setQuickReportTrafficType: function (trafficType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_REPORT_TRAFFIC_TYPE,
            data: trafficType
        })
    },
    quickDashboardLink: function (filter) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_DASHBOARD_LINK,
            data: filter
        })
    },
    setHelpWindow: function (helpObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_HELP_WINDOW,
            data: helpObj
        })
    },
    updateIncludeFields: function (items) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_INCLUDE_FIELDS,
            data: items
        })
    },
    openExport: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_EXPORT
        })
    },
    setIndividualFieldExportIncludeType: function (exportFieldObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_INDIVIDUAL_FIELD_EXPORT_TYPE,
            data: exportFieldObj
        })
    },
    blockOrganisation: function (organisationID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.BLOCK_ORGANISATION,
            data: organisationID
        })
    },
    updateStripeDetails: function (tokenData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_STRIPE_DETAILS,
            data: tokenData
        })
    },
    storeAdditionalCompanyData: function (companyData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_ADDITIONAL_COMPANY_DATA,
            data: companyData
        })
    },
    showAdditionalCompanyData: function (organisationID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_ADDITIONAL_COMPANY_DATA,
            data: organisationID
        })
    },
    loadMorePreviousVisits: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_MORE_PREVIOUS_VISITS
        })
    },
    setPreviousVisitsSelection: function (selection) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_PREVIOUS_VISITS_SELECTION,
            data: selection
        })
    },
    changeRoute: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CHANGE_ROUTE
        })
    },
    setAddOrRemoveQuickExportSelection: function (selectedKeywordIDs) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ADD_OR_REMOVE_QUICK_EXPORT_SELECTION,
            data: selectedKeywordIDs
        })
    },
    setQuickExportSearchString: function (searchString) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_EXPORT_SEARCH_STRING,
            data: searchString
        })
    },
    quickExportShowData: function (filterType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_EXPORT_SHOW_DATA,
            data: filterType
        })
    },
    openNotes: function (organisationID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_NOTES,
            data: organisationID
        })
    },
    fireOffOrganisationEmail: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.FIRE_OFF_ORGANISATION_EMAIL
        })
    },
    saveAssignedToForOrganisationsDashboard: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_ASSIGNED_TO_FOR_ORGANISATIONS_DASHBOARD
        })
    },
    setTrackedOrganisationData: function (organisationData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_TRACKED_ORGANISATION_DATA,
            data: organisationData
        })
    },
    displayFancyFilters: function (displayFancyFiltersBool) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DISPLAY_FANCY_FILTERS,
            data: displayFancyFiltersBool
        })
    },
    setMenuWindowboxOpen: function (open) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_WINDOW_BOX_OPEN,
            data: open
        })
    },
    displayPreviousVisits: function (sessionIDs) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DISPLAY_PREVIOUS_VISITS,
            data: sessionIDs
        })
    },
    setOrderBy: function (orderByKey) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ORDER_BY,
            data: orderByKey
        })
    },
    quickLink: function (quickLinkObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_LINK,
            data: quickLinkObj
        })
    },
    setQuickOpen: function (quickOpenObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_OPEN,
            data: quickOpenObj
        })
    },
    createFilters: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_FILTERS,
            data: filters
        })
    },
    removeFiltersByType: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_FILTERS_BY_TYPE,
            data: filters
        })
    },
    editFilters: function (editedFilters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.EDIT_FILTERS,
            data: editedFilters
        })
    },
    quickExportSelectAll: function (filterType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_EXPORT_SELECT_ALL,
            data: filterType
        })
    },
    quickExportDeselectAll: function (filterType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_EXPORT_DESELECT_ALL,
            data: filterType
        })
    },
    setCountryStatsForDialog: function (countryStats) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_COUNTRY_VISITOR_STATS,
            data: countryStats
        })
    },
    setPaginationReturnAmount: function (amount) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_PAGINATION_RETURN_AMOUNT,
            data: amount
        })
    },
    generateNewClientSecret: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.GENERATE_NEW_CLIENT_SECRET
        })
    },
    setFirstLoadComplete: function (section) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_FIRST_LOAD_COMPLETE,
            data: section
        })
    },
    storeNewClientSecret: function (newSecret) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_NEW_CLIENT_SECRET,
            data: newSecret
        })
    },
    updateUserSettings: function (newSettings) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_USER_SETTINGS,
            data: newSettings
        })
    },
    setGeolocation: function (useGeolocationBool) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_USE_GEOLOCATION,
            data: useGeolocationBool
        })
    },
    setDisplayGCLID: function (displayGCLID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_DISPLAY_GCLID,
            data: displayGCLID
        })
    },
    updateHooks: function (hooks) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_HOOKS,
            data: hooks
        })
    },
    bankResults: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.BANK_RESULTS
        })
    },
    setActiveRecord: function (activeRecord) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ACTIVE_RECORD,
            data: activeRecord
        })
    },
    addTeamMemberToSendEmailArray: function (teamMemberID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ADD_TEAM_MEMBER_TO_SEND_EMAIL_ARRAY,
            data: teamMemberID
        })
    },
    sendOrganisationViaEmail: function (payload) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SEND_ORGANISATION_VIA_EMAIL,
            data: payload
        })
    },
    setCurrentRoute: function (route) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CURRENT_ROUTE
        })
    },
    saveAssignedTo: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_ASSIGNED_TO
        })
    },
    setCurrentTrackedObjAssignedToTeamMember: function (teamMemberId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ASSIGNED_TO_TEAM_MEMBER,
            data: teamMemberId
        })
    },
    removeTrackedOrganisation: function (resultKey) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_TRACKED_ORGANISATION,
            data: resultKey
        })
    },
    closeSessionResultPopover: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CLOSE_SESSION_RESULT_POPOVER
        })
    },
    setSessionResultPopover: function (popoverObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_SESSION_RESULT_POPOVER,
            data: popoverObj
        })
    },
    setNewClientTypes: function (tags) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.NEW_SET_CLIENT_TYPES,
            data: tags
        })
    },
    incrementFilterChange: function (resultsUpdated) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.INCREMENT_FILTER_CHANGE,
            data: resultsUpdated
        })
    },
    setFiltersBeforeAdvancedFiltering: function (filters) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_FILTERS_BEFORE_ADVANCED_FILTERING,
            data: filters
        })
    },
    resetFiltersOnlyAndNoAction: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.RESET_FILTERS_ONLY_AND_NO_ACTION
        })
    },
    resetFilters: function (doNotReloadAnalytics) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.RESET_FILTERS,
            data: doNotReloadAnalytics
        })
    },
    openPopover: function (popoverSettings) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_POPOVER,
            data: popoverSettings
        })
    },
    setIncludeCustomFilterType: function (shouldIncludeOrExclude) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CUSTOM_FILTER_INCLUDE_TYPE,
            data: shouldIncludeOrExclude
        })
    },
    setCrypt: function (paymentType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CRYPT,
            data: paymentType
        })
    },
    hideDrawers: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.HIDE_DRAWERS
        })
    },
    deleteNote: function (note) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DELETE_NOTE,
            data: note
        })
    },
    openDrawer: function (drawerIndex) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_DRAWER,
            data: drawerIndex
        })
    },
    setActiveTab: function (activeTab) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ACTIVE_TAB,
            data: activeTab
        })
    },
    setAvatar: function (message) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_AVATAR,
            data: message
        })
    },
    uploadAvatar: function (file) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPLOAD_AVATAR,
            data: file
        })
    },
    storeNotes: function (notes) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_NOTES,
            data: notes
        })
    },
    fetchNotes: function (orgId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.FETCH_NOTES,
            data: orgId
        })
    },
    saveNote: function (note) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_NOTE,
            data: note
        })
    },
    startQuickExport: function (type) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.START_QUICK_EXPORT,
            data: type
        })
    },
    addOrRemoveQuickExportSelection: function (objectID) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ADD_OR_REMOVE_QUICK_EXPORT_SELECTION,
            data: objectID
        })
    },
    setQuickExportSelectionType: function (selectionType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_EXPORT_SELECTION_TYPE,
            data: selectionType
        })
    },
    setQuickExportFileType: function (exportObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_QUICK_EXPORT_FILE_TYPE,
            data: exportObj
        })
    },
    updateFilterNumbers: function (numbers) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_FILTER_NUMBERS,
            data: numbers
        })
    },
    setCreateNoteOrganisationId: function (orgId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_NOTE_ORGANISATIONID,
            data: orgId
        })
    },
    storeUser: function (user) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_USER,
            data: user
        })
    },
    setUser: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_USER
        })
    },
    storeAnalyticsData: function (analytics) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_ANALYTICS_DATA,
            data: analytics
        })
    },
    setAnalyticsData: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ANALYTICS_DATA
        })
    },
    setAllVisitorTypesSelected: function (selected) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ALL_VISITOR_TYPES_SELECTED,
            data: selected
        })
    },
    setAllVisitorTypesUnselected: function (selected) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ALL_VISITOR_TYPES_UNSELECTED,
            data: selected
        })
    },
    updateTeamMember: function (newDetails) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_TEAM_MEMBER,
            data: newDetails
        })
    },
    setTeamMemberToEdit: function (teamMember) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_TEAM_MEMBER_TO_EDIT,
            data: teamMember
        })
    },
    editTeamMember: function (memberId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.EDIT_TEAM_MEMBER,
            data: memberId
        })
    },
    showNewTeamMemberDetails: function (userDetails) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_NEW_TEAM_MEMBER,
            data: userDetails
        })
    },
    deleteTeamMember: function (memberId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DELETE_TEAM_MEMBER,
            data: memberId
        })
    },
    createNewTeamMember: function (newTeamMemberData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_NEW_TEAM_MEMBER,
            data: newTeamMemberData
        })
    },
    storeCommonPaths: function (commonPathsData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_COMMON_PATHS,
            data: commonPathsData
        })
    },
    loadCommonPaths: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_COMMON_PATHS
        })
    },
    setCustomFilterType: function (type) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CUSTOM_FILTER_TYPE,
            data: type
        })
    },
    updateNarrowedSearchOptions: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_NARROWED_SEARCH_OPTIONS
        })
    },
    deleteReportTemplate: function (id) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DELETE_REPORT_TEMPLATE,
            data: id
        })
    },
    createNewTrackedOrganisation: function (id) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_NEW_TRACKED_ORGANISATION,
            data: id
        })
    },
    setExportSelectionType: function (type) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_EXPORT_SELECTION_TYPE,
            data: type
        })
    },
    removeExportSelection: function (id) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_EXPORT_SELECTION,
            data: id
        })
    },
    setInitialFiltersByTypeAndValue: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_INITIAL_FILTERS_BY_TYPE_AND_VALUE
        })
    },
    hideSearchByLoadingStatus: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.HIDE_SEARCH_BY_LOADING_STATUS
        })
    },
    resetSearchString: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.RESET_SEARCH_STRING
        })
    },
    loadInitialData: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_INITIAL_DATA
        })
    },
    updateInitialLoadedData: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_INITIAL_LOADED_DATA
        })
    },
    setSearchByLoadingStatus: function (searchString) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_SEARCH_BY_LOADING_STATUS,
            data: searchString
        })
    },
    setSearchBySelectAll: function (type) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_SEARCH_BY_SELECT_ALL,
            data: type
        })
    },
    storePreviousHistory: function (previousHistoryObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_PREVIOUS_HISTORY,
            data: previousHistoryObj
        })
    },
    setDateRange: function (dateRange) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_DATE_RANGE,
            data: dateRange
        })
    },
    setPreviousHistory: function (previousHistoryObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_PREVIOUS_HISTORY,
            data: previousHistoryObj
        })
    },
    closeLightbox: function (lightboxObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CLOSE_LIGHTBOX,
            data: lightboxObj
        })
    },
    setLightbox: function (lightboxObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_LIGHTBOX,
            data: lightboxObj
        })
    },
    removeExistingReport: function (reportId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_EXISTING_REPORT,
            data: reportId
        })
    },
    closeExistingAutomatedReport: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CLOSE_EXISTING_AUTOMATED_REPORT
        })
    },
    updateExistingAutomatedReportInStoreOnly: function (automatedReport) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_EXISTING_AUTOMATED_REPORT_IN_STORE_ONLY,
            data: automatedReport
        })
    },
    setStateUpdated: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_STATE_UPDATED
        })
    },
    openViewEditAutomatedReport: function (automatedReportId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_VIEW_EDIT_AUTOMATED_REPORT,
            data: automatedReportId
        })
    },
    setExistingAutomatedReportData: function (reportData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_EXISTING_REPORT_DATA,
            data: reportData
        })
    },
    viewExistingAutomatedReports: function (template) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.VIEW_EXISTING_AUTOMATED_REPORTS,
            data: template
        })
    },
    findPossibleActiveRecordSuggestions: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.FIND_POSSIBLE_ACTIVE_RECORD_SUGGESTIONS
        })
    },
    updateExportOptions: function (exportOptions) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_EXPORT_OPTIONS,
            data: exportOptions
        })
    },
    updateActiveRecord: function (updatedActiveRecord) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_ACTIVE_RECORD,
            data: updatedActiveRecord
        })
    },
    updateExportCriteria: function (sortedData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_EXPORTED_CRITERIA,
            data: sortedData
        })
    },
    updateAutomatedDelivery: function (automationSettings) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_AUTOMATED_DELIVERY,
            data: automationSettings
        })
    },
    createAutomatedDelivery: function (automationSettings) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_AUTOMATED_DELIVERY,
            data: automationSettings
        })
    },
    resetSelectedTeamMembers: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.RESET_SELECTED_TEAM_MEMBERS
        })
    },
    closeSetAutomation: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CLOSE_SET_AUTOMATION
        })
    },
    openSetAutomation: function (template) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.OPEN_SET_AUTOMATION,
            data: template
        })
    },
    removeFilterByType: function (filterObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_FILTER_BY_TYPE,
            data: filterObj
        })
    },
    resetNarrowedOptions: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.RESET_NARROWED_OPTIONS
        })
    },
    setCountryInclusionType: function (includeType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_COUNTRY_INCLUSION_TYPE,
            data: includeType
        })
    },
    setSnackbarClosed: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CLOSE_SNACKBAR
        })
    },
    storeAllBrowsers: function (browsers) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ALL_BROWSERS,
            data: browsers
        })
    },
    setBrowsers: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.GET_ALL_BROWSERS
        })
    },
    resetUpdatingBox: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.RESET_UPDATING_BOX
        })
    },
    saveThirdPartyAppSettings: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_THIRD_PARTY_APP_SETTINGS
        })
    },
    setZohoKey: function (newKey) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ZOHO_KEY,
            data: newKey
        })
    },
    startExport: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.START_EXPORT
        })
    },
    addRemoveExportType: function (newTypeObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ADD_REMOVE_EXPORT_TYPE,
            data: newTypeObj
        })
    },
    removeTeamMemberFromExportCriteria: function (memberId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_TEAM_MEMBER_FROM_EXPORT_CRITERIA,
            memberId: memberId
        })
    },
    addTeamMemberToExportCriteria: function (memberId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ADD_TEAM_MEMBER_TO_EXPORT_CRITERIA,
            memberId: memberId
        })
    },
    setExportTypes: function (exportTypes) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_EXPORT_TYPES,
            types: exportTypes
        })
    },
    setSnackbar: function (dataObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_SNACKBAR,
            message: dataObj
        });
    },
    broadcastInitialLoadComplete: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.BROADCAST_INITIAL_LOAD
        });
    },
    asyncAddClientTag: function (newTag) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ASYNC_NEW_CLIENT_TYPE,
            newTag: newTag
        });
    },
    setClientTypes: function (clientTypes) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CLIENT_TYPES,
            clientTypes: clientTypes
        });
    },
    setDisplayByValue: function (displayBy) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_DISPLAY_BY_FILTER,
            displayBy: displayBy
        });
    },
    updateClientTypes: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_CLIENT_TYPES
        });
    },
    loadResultsIfNoPreviousLoads: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_RESULTS_IF_NO_PREVIOUS_LOADS
        });
    },
    changeRelationshipStatus: function (relationshipData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CHANGE_RELATIONSHIP_STATUS,
            relationship: relationshipData
        });
    },
    showNewResults: function (newResults) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SHOW_NEW_RESULTS,
            newResults: newResults
        });
    },
    loadNewReportFilters: function (data) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.LOAD_NEW_REPORT_FILTERS,
            filters: data
        });
    },
    setNewTemplateName: function (newName) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_NEW_TEMPLATE_NAME,
            data: newName
        });
    },
    updateClientRelationship: function (data) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_CLIENT_RELATIONSHIP,
            filters: data
        });
    },
    setTemplateFilters: function (templateObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_TEMPLATE_FILTERS,
            templateId: templateObj
        });
    },
    setOrganisationInclusionType: function (includeExcludeType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ORGANISATION_INCLUSION_TYPE,
            data: includeExcludeType
        })
    },
    setClientPages: function (pages) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_CLIENT_PAGES,
            data: pages
        })
    },
    setCountries: function (countries) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_COUNTRIES,
            data: countries
        })
    },
    applicationNotResting: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.APPLICATION_NOT_RESTING
        })
    },
    setReferrers: function (referrers) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_REFERRERS,
            data: referrers
        })
    },
    setLoadedTemplates: function (loadedTemplates) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_LOADED_TEMPLATES,
            data: loadedTemplates
        })
    },
    updateReportTemplates: function (type) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_REPORT_TEMPLATES,
            data: type
        });
    },
    saveReportTemplate: function (templateObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_REPORT_TEMPLATE,
            data: templateObj
        });
    },
    removeFlashMessages: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.REMOVE_FLASH_MESSAGES
        });
    },
    setFlashMessage: function (payload) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_FLASH_MESSAGE,
            data: payload
        });
    },
    downloadCSV: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DOWNLOAD_CSV
        });
    },
    paginationNext: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.PAGINATION_NEXT,
            data: null
        });
    },
    paginationPrev: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.PAGINATION_PREV,
            data: null
        });
    },
    updateDisplayedStatus: function (displayValues) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_DISPLAYED_COLUMNS,
            data: displayValues
        })
    },
    loadFilter: function (data) {
        ReportBuilderDispatcher.handleAction({
            actionType: FilterConstants.LOAD_FILTERS,
            data: data
        })
    },
    createFilter: function (filterObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_FILTER,
            filterObj: filterObj
        });
    },
    editFilter: function (filterObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.EDIT_FILTER,
            filterObj: filterObj
        });
    },
    changeLoadingStatus: function (message) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CHANGE_LOADING_STATUS,
            data: message
        });
    },
    quickCreateReport: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.QUICK_CREATE_REPORT
        });
    },
    updateResults: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_RESULTS
        });
    },
    removeFilter: function (filterObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.DELETE_FILTER,
            id: filterObj
        });
    },
    createNewTag: function (tagData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.CREATE_NEW_TAG,
            tag: tagData
        });
    },
    setOrganisationData: function (resultKey) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ORGANISATION_DATA,
            data: resultKey
        });
    },
    storeOrganisationData: function (organisationData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_ORGANISATION_DATA,
            data: organisationData
        });
    },
    updateOrganisationData: function (organisationData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.UPDATE_ANALYTICS_ORGANISATION_DATA,
            data: organisationData
        });
    },
    saveEndUserCompanyCompanyRecord: function (companyData) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SAVE_END_USER_COMPANY_COMPANY_DATA,
            data: companyData
        });
    },
    setCompanyDataOpen: function (organisationId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_COMPANY_DATA_OPEN,
            data: organisationId
        });
    },
    setCompanyDataClose: function (organisationId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_COMPANY_DATA_CLOSE,
            data: organisationId
        });
    },
    setAllOperatingSystems: function () {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ALL_OPERATING_SYSTEMS
        });
    },
    storeAllOperatingSystems: function (operatingSystems) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_ALL_OPERATING_SYSTEMS,
            data: operatingSystems
        });
    },
    narrowSearchOptions: function (searchCriteria) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.NARROW_SEARCH_OPTIONS,
            data: searchCriteria
        });
    },
    setNarrowedOptions: function (narrowedOptions) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_NARROWED_OPTIONS,
            data: narrowedOptions
        });
    },
    setAllSelected: function (selectedObj) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_ALL_SELECTED,
            data: selectedObj
        });
    },
    addToExportSelection: function (sessionId) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.ADD_TO_EXPORT_SELECTION,
            data: sessionId
        });
    },
    turnAutoExportToCRMOnForReportTemplate: function (crmAndReportTemplate) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.TURN_ON_AUTO_EXPORT_FOR_CRM,
            data: crmAndReportTemplate
        });
    },
    turnAutoExportToCRMOffForReportTemplate: (crmAndReportTemplate) => {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.TURN_OFF_AUTO_EXPORT_FOR_CRM,
            data: crmAndReportTemplate
        });
    },
    startStripeSessionAndRedirect: function (paymentType) {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.START_STRIPE_SESSION_AND_REDIRECT,
            data: paymentType
        })
    },
    storeZapierIsSetup: (zapierIsSetup) => {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_ZAPIER_IS_SETUP,
            data: zapierIsSetup,
        })
    },
    storeAutoExports: (data) => {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.STORE_AUTO_EXPORTS,
            data,
        })
    },
    setAutoExport: (data) => {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_AUTO_EXPORT,
            data,
        })
    },
    setMicrosoftClarityProjectId: (data) => {
        ReportBuilderDispatcher.dispatch({
            actionType: FilterConstants.SET_MICROSOFT_CLARITY_PROJECT_ID,
            data,
        })
    }
};
export default FilterActions;
