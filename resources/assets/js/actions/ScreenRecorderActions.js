import ScreenRecorderDispatcher 	from '../dispatcher/ScreenRecorderDispatcher';
import ScreenRecorderConstants 		from '../constants/ScreenRecorderConstants';

var ScreenRecorderActions = {

	loadVideo : function( videoID ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.LOAD_VIDEO,
			data 		: 	videoID
		})
	},

	setDisplayOffsets : function( offsets ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.SET_DISPLAY_OFFSETS,
			data 		: 	offsets
		})
	},

	pauseVideo : function() {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.PAUSE_VIDEO
		})
	},

	setDisplayType : function( displayType ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.SET_DISPLAY_TYPE,
			data 		: 	displayType
		})
	},

	storeHeatmap : function( heatmapData ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.STORE_HEATMAP,
			data 		: 	heatmapData
		})
	},

	loadHeatmap : function( pageID ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.LOAD_HEATMAP,
			data 		: 	pageID
		})
	},

	setNaturalHeight : function( height ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.SET_NATURAL_HEIGHT,
			data 		: 	height
		})
	},

	setVideoI : function( seek ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.SET_VIDEO_I,
			data 		: 	seek
		})
	},

	updateState : function() {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.UPDATE_STATE
		})
	},

	startPlayVideo : function() {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.START_PLAY_VIDEO
		})
	},

	saveRecording : function( recording ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.SAVE_RECORDING,
			data 		: 	recording
		})
	},

	storeVideo : function( video ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.STORE_VIDEO,
			data 		: 	video
		})
	},

	setRenderedImageHeight : function( height ) {

		ScreenRecorderDispatcher.dispatch({

			actionType 	: 	ScreenRecorderConstants.SET_RENDERED_IMAGE_HEIGHT_2,
			data 		: 	height
		})
	}
}

export default ScreenRecorderActions;