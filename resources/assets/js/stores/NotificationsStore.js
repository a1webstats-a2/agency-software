import NotificationsDispatcher from '../dispatcher/NotificationsDispatcher';
import NotificationConstants   from '../constants/NotificationConstants';
import FiltersStore 			from './FiltersStore';
import FilterActions 			from '../actions/FilterActions';
import EventEmitterObj            from 'events';
var EventEmitter = EventEmitterObj.EventEmitter;
import merge                   from 'merge';
import assign                  from 'object-assign';
import NotificationActions 	from '../actions/NotificationActions';

var _notifications 			= { 'new' : [], old : [], viewType : 'current', selection : [] };
var _newNotifications 		= false;
var _numNotifications 		= 0;
var _isNotificationsOpen 	= false;
var _viewNotification 		= 0;
var _display 				= "all";

function loadNotifications() {

	return clone( _notifications );
}

function addNotification( notification ) {

	_notifications.push( notification.data );
	_newNotifications = true;

}

function storeNotifications( notifications ) {

	_notifications.new = notifications.new;
	_notifications.old = notifications.old;

	if( _notifications.new.length > 0 ) {

		_newNotifications = true;
	}

	_numNotifications = _notifications.new.length;
}

function isNewNotifications() {

	return clone( _newNotifications );
}

function setInitialNotifications() {

	var request = new XMLHttpRequest();

	request.open( "GET", "api/proxy/api/v1/user/notifications" );
	request.setRequestHeader( 'Content-Type', 'application/json' );

	request.onreadystatechange 	=  function() {

		if( request.readyState === 4 && request.status === 200 ) {

			let data = false;

			try {

				data = JSON.parse( request.response );
			
			} catch( e ) {

				
			}

			if( data ) {

				NotificationActions.storeNotifications( JSON.parse( request.response ) );
			}
		}	
	};

	request.send();
}

function deleteNotification( notification ) {

	delete _notifications.old[notification.key];

	_numNotifications-= 1;

	var request = new XMLHttpRequest();

	request.open( "DELETE", "api/proxy/api/v1/user/notifications/" + notification.notification_id );
	request.setRequestHeader( 'Content-Type', 'application/json' );

	request.onreadystatechange 	=  function() {

		FilterActions.setSnackbar({

			message 	: 	'Thank you, the notification has been deleted'
		})	
	};

	request.send();
}

function clone( obj ) {

    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function archiveNotification( notification ) {

	delete _notifications.new[notification.key];

	_notifications.old.push( notification );

	var request = new XMLHttpRequest();

	request.open( "GET", "api/proxy/api/v1/user/notifications/" + notification.notification_id + "/edit" );
	request.setRequestHeader( 'Content-Type', 'application/json' );

	request.onreadystatechange 	=  function() {

		FilterActions.setSnackbar({

			message 	: 	'Thank you, the notification has been archived'
		})	
	};

	request.send();
}

function deleteSelection() {

	var notificationsClone 		= clone( _notifications.new );
	var oldNotificactionsClone 	= clone( _notifications.old );
	var newNotifications 		= [];
	var oldNotifications 		= [];

	notificationsClone.map( function( notification, i ) {

		var indexOf = _notifications.selection.indexOf( notification.notification_id );

		if( indexOf > -1 ) {

			delete notificationsClone[indexOf];
		
		} else {

			newNotifications.push( notification );
		}
	})

	oldNotificactionsClone.map( function( notification, i ) {

		var indexOf = _notifications.selection.indexOf( notification.notification_id );

		if( indexOf > -1 ) {

			delete oldNotificactionsClone[indexOf];
		
		} else {

			oldNotifications.push( notification );
		}
	})


	_notifications.new = newNotifications;
	_notifications.old = oldNotifications;


	var sendObj = {

		selection : _notifications.selection
	}
	
	var request = new XMLHttpRequest();

	request.open( "POST", "api/proxy/api/v1/user/notifications/delete" );
	request.setRequestHeader( 'Content-Type', 'application/json' );

	request.onreadystatechange 	=  function() {

		FilterActions.setSnackbar({

			message 	: 	'Selected notifications have been deleted'
		})	
	};

	request.send( JSON.stringify( sendObj ) );
}

function archiveSelection() {

	var notificationsClone 	= clone( _notifications.new );
	var newNotifications 	= [];

	notificationsClone.map( function( notification, i ) {

		var indexOf = _notifications.selection.indexOf( notification.notification_id );

		if( indexOf > -1 ) {

			delete notificationsClone[indexOf];

			_notifications.old.push( notification );
		
		} else {

			newNotifications.push( notification );
		}
	})

	_notifications.new = newNotifications;


	var sendObj = {

		selection : _notifications.selection
	}
	
	var request = new XMLHttpRequest();

	request.open( "POST", "api/proxy/api/v1/user/notifications/archive" );
	request.setRequestHeader( 'Content-Type', 'application/json' );

	request.onreadystatechange 	=  function() {

		FilterActions.setSnackbar({

			message 	: 	'Selected notifications have been archived'
		})	
	};

	request.send( JSON.stringify( sendObj ) );
}

function deleteIndividual() {

	var notificationsClone 		= clone( _notifications.new );
	var oldNotificactionsClone 	= clone( _notifications.old );
	var newNotifications 		= [];
	var oldNotifications 		= [];

	notificationsClone.map( function( notification, i ) {

		if( notification.notification_id !== _viewNotification ) {

			newNotifications.push( notification );
		}
	})

	oldNotificactionsClone.map( function( notification, i ) {

		if( notification.notification_id !== _viewNotification ) {

			oldNotifications.push( notification );
		}
	})

	_notifications.new = newNotifications;
	_notifications.old = oldNotifications;

	_display = "all";

	var request = new XMLHttpRequest();

	request.open( "DELETE", "api/proxy/api/v1/user/notifications/" + _viewNotification );
	request.setRequestHeader( 'Content-Type', 'application/json' );

	request.onreadystatechange 	=  function() {

	};

	request.send();
}

function backToAll() {

	_display = "all";
}

function openNotifications() {

	_isNotificationsOpen = true;
}

function closeNotifications() {

	_isNotificationsOpen = false;
}

function setNotificationViewType( type ) {

	if( type !== _notifications.viewType && typeof type === "string" ) {

		_notifications.selection 	= [];
	}

	_notifications.viewType 	= type;
}

function viewNotification( notificationID ) {

	_display 			= "individual";
    _viewNotification 	= notificationID;

    var updatedNewNotifications = [];

     _notifications.new.forEach( function( notification,  n ) {

     	var read = 0;

     	if( notification.read === 1 ) {

     		read = 1;
     	
     	} else {

	     	if( parseInt( notification.notification_id ) === parseInt( notificationID ) ) {

	     		read = 1;
	     	}
	    }

     	notification.read = read;

     	updatedNewNotifications.push( notification );
     })

     _notifications.new = updatedNewNotifications;

    var request = new XMLHttpRequest();

	request.open( "PUT", "api/proxy/api/v1/user/notifications/" + notificationID );
	request.setRequestHeader( 'Content-Type', 'application/json' );

	request.onreadystatechange 	=  function() {

		
	};

	request.send();
}

function setNotificationSelection( selectionObj ) {

	var selection = selectionObj.selected;

	if( typeof selection === "string" ) {

		switch( selectionObj.type ) {

			case "new" :

				selection = _notifications.new.map( function( notification, i ) {

					return notification.notification_id;
				
				});

				break;

			case "archive" :

				selection = _notifications.old.map( function( notification, i ) {

					return notification.notification_id;
				
				});

				break;
		}
	}

	_notifications.selection = selection;

}

function updateNotifications() {

	
}

function discardAllNotifications() {
	
}

var NotificationsStore = merge(EventEmitter.prototype, {

	emitChange: function() {

		this.emit( 'notificationschange' );
	},

	storeNotifications : function( notifications ) {

		storeNotifications( notifications );
	},

	addChangeListener: function( callback )  {

		this.on( 'notificationschange' , callback);
	},
	
	getNotificationSelection : function() {

		return clone( _notificationSelection );
	},

	isNotificationsOpen : function() {

		return clone( _isNotificationsOpen );
	},

	viewNotificationID : function() {

		return clone( _viewNotification );
	},

	getDisplay : function() {

		return clone( _display );
	},

	removeChangeListener: function( callback ) {

		this.removeListener( 'notificationschange', callback);
	},

	isNewNotifications : function() {

		var count = 0;

		_notifications.new.forEach( function( notification, n ) {

			if( notification.read === 0 ) {

				count++;
			}
		})

		if( count > 0 ) {

			return true;
		}

		return false;
	},

	getNumberOfNewNotifications : function() {

		return clone( _numNotifications );

	},

	returnNotifications : function() {

		return clone( _notifications );
	}

});

NotificationsDispatcher.register( function( payload ) {

	switch( payload.actionType ) {

		case NotificationConstants.LOAD_NOTIFICATIONS :

			loadNotifications();

			break;

		case NotificationConstants.ADD_NOTIFICATION :

			addNotification( payload );

			break;

		case NotificationConstants.DISCARD_NOTIFICATIONS :

			discardAllNotifications();

			break;

		case NotificationConstants.LOAD_INITIAL_NOTIFICATIONS :

			setInitialNotifications();

			break;

		case NotificationConstants.STORE_NOTIFICATIONS :

			storeNotifications( payload.data );

			break;

		case NotificationConstants.ARCHIVE_NOTIFICATION :

			archiveNotification( payload.data );

			break;

		case NotificationConstants.DELETE_NOTIFICATION :

			deleteNotification( payload.data );

			break;

		case NotificationConstants.UPDATE_NOTIFICATIONS :

			updateNotifications();

			break;

		case NotificationConstants.OPEN_NOTIFICATIONS :

			openNotifications();

			break;

		case NotificationConstants.CLOSE_NOTIFICATIONS :

			closeNotifications();

			break;

		case NotificationConstants.SET_NOTIFICATION_SELECTION :

			setNotificationSelection( payload.data );

			break;

		case NotificationConstants.ARCHIVE_SELECTION :

			archiveSelection();

			break;

		case NotificationConstants.SET_NOTIFICATION_VIEW_TYPE :

			setNotificationViewType( payload.data );

			break;

		case NotificationConstants.DELETE_SELECTION :

			deleteSelection();

			break;

		case NotificationConstants.VIEW_NOTIFICATION :

			viewNotification( payload.data );

			break;

		case NotificationConstants.DELETE_INDIVIDUAL :

			deleteIndividual();

			break;

		case NotificationConstants.BACK_TO_ALL :

			backToAll();

			break;

		default:


			break;
	}


  	NotificationsStore.emitChange();

});

export default NotificationsStore;