import React         from 'react';
import FilterActions from '../actions/FilterActions';

var KeywordOption = React.createClass({

	getInitialState : function() {

		return {

			keywordData : this.props.keywordData,
			key 		: this.props.myKey
		};
	},

	render : function() {

		return (

			<option value={this.state.key}>{this.state.keywordData.keyword}</option>
		);
	}
});

export default  KeywordOption;