import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FilterActions from '../../actions/FilterActions';
import AdvancedFilterActions from '../../actions/AdvancedFilterActions';

function getCurrentState(props) {
    return {
        open: props.open,
        savedScenarios: props.savedScenarios
    }
}

var ManageSavedScenarios = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.savedScenarios !== nextProps.savedScenarios) {
            return true;
        }

        if (this.props.open !== nextProps.open) {
            return true;
        }

        return false;
    },
    render: function () {
        const actions = [
            <FlatButton
                onTouchTap={this._close}
                label="Close"
                primary={true}
            />
        ];

        var scenarios = [];

        for (var key in this.state.savedScenarios) {
            let scenario = this.state.savedScenarios[key];
            let tr = (
                <tr key={key}>
                    <td>{scenario.name}</td>
                    <td>
						<span className="link warningText" onClick={() => this._deleteSavedScenario(key)}>
							delete
						</span>
                    </td>
                </tr>
            )

            scenarios.push(tr);
        }

        return (
            <div>
                <Dialog
                    title="Manage Saved Scenarios"
                    modal={true}
                    actions={actions}
                    autoScrollBodyContent={true}
                    open={this.state.open}
                >
                    <table className="table table-striped">
                        <tbody>
                        {scenarios}
                        </tbody>
                    </table>
                </Dialog>
            </div>
        )
    },
    _deleteSavedScenario: function (id) {
        if (!confirm("Are you sure you want to delete this scenario?")) {
            return false;
        }

        AdvancedFilterActions.deleteSavedScenario(id);
    },
    _close: function () {
        FilterActions.closeLightbox();
    }
});

export default ManageSavedScenarios;