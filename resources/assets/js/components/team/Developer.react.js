import React from 'react';
import TextField from 'material-ui/TextField';
import HookIcon from 'material-ui/svg-icons/action/swap-horiz';
import RaisedButton from 'material-ui/RaisedButton';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';

function getCurrentState( props ) {
	return {
		newOrgHook 		: 	props.settings.newOrgHook,
		newVisitHook 	: 	props.settings.newVisitHook,
		secret 			: 	FiltersStore.getClientSecret(),
		clientId 		: 	FiltersStore.getClientId()
	}
}

const styles = {
	raisedButton : 	{
		width 	: 	300,
		margin 	: 	12
	}
}

const Developer = React.createClass({
	getInitialState : function() {
		return getCurrentState( this.props );
	},
	componentWillReceiveProps : function( newProps ) {
		this.setState( getCurrentState( newProps ) );
	},
	shouldComponentUpdate : function() {
		return true;
	},
	setNewOrganisationHook : function( event ) {
		this.setState({
			newOrgHook 	: event.target.value
		})
	},
	setNewVisitHook : function( event ) {
		this.setState({
			newVisitHook : event.target.value
		})
	},
	render : function() {
		return (
			<div>
				<div className="row">
					<div className="col-md-12">
						<h3>Hooks</h3><br />
					</div>
				</div>

				<div className="row" id="developerSettings">
					<div className="col-md-12">
						<HookIcon />
						<h4>Hooks</h4>
						<div className="clr"></div><br />
						<p>We currently offer the following event hooks:</p>

						<TextField
					    	hintText="http://yourdomain.com/catch-event"
					    	onChange={this.setNewOrganisationHook}
					    	floatingLabelText="New Organisation"
					    	value={this.state.newOrgHook}
					    	fullWidth={true}
					    /><br />

					    <TextField
					    	hintText="http://yourdomain.com/catch-event"
					    	onChange={this.setNewVisitHook}
					    	floatingLabelText="New Visit"
					    	value={this.state.newVisitHook}
					    	fullWidth={true}
					    /><br /><br />

					    <RaisedButton label="Update Settings" onClick={this._updateSettings} primary={true} style={styles.RaisedButton} />
					    <RaisedButton label="Send Test Data" onClick={this._sendTestData} secondary={true} style={{ marginLeft : 20 }} />

					    <br />
					</div>
				</div>
			</div>
		)
	},
	_generateNewClientSecret : function() {
		FilterActions.generateNewClientSecret();
	},
	_sendTestData : function() {
		FilterActions.sendTestWebhookData( {
			newOrgHook 		: 	this.state.newOrgHook,
			newVisitHook 	: 	this.state.newVisitHook
		} );
	},
	_updateSettings : function() {
		FilterActions.updateHooks({
			newOrgHook 		: 	this.state.newOrgHook,
			newVisitHook 	: 	this.state.newVisitHook
		});
	}
});

export default  Developer;
