import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState(props) {
    return {}
}

var Billing = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },

    shouldComponentUpdate: function (nextProps, nextState) {
        return true;
    },

    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <h3>Billing</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label>Billing Forename</label>
                            <input className="form-control" type="text" name="BillingFirstnames"/>
                        </div>

                        <div className="form-group">

                            <label>Billing Surname</label>
                            <input className="form-control" type="text" name="BillingSurname"/>

                        </div>

                        <div className="form-group">

                            <label>Billing Address 1</label>
                            <input className="form-control" type="text" name="BillingAddress1"/>

                        </div>

                        <div className="form-group">

                            <label>Billing Address 2</label>
                            <input className="form-control" type="text" name="BillingAddress2"/>

                        </div>

                        <div className="form-group">

                            <label>Billing City</label>
                            <input className="form-control" type="text" name="BillingCity"/>

                        </div>

                        <div className="form-group">

                            <label>Billing Postcode</label>
                            <input className="form-control" type="text" name="BillingPostCode"/>

                        </div>
                        <div className="form-group">
                            <label>Billing Country</label>
                            <input className="form-control" type="text" name="BillingCountry"/>
                        </div>
                        <div className="form-group">
                            <label>Billing Postcode</label>
                            <input className="form-control" type="text" name="BillingPostCode"/>
                        </div>
                        <div className="clr"></div>
                        <br/>
                        <RaisedButton label="Update Billing Info" secondary={true}/>
                    </div>
                </div>
            </div>
        )
    }
});

export default Billing;
