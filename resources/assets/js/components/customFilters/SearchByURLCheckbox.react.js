import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import Checkbox from 'material-ui/Checkbox';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import Person from 'material-ui/svg-icons/social/person';

const styles = {
 
  	checkbox: {
   	 	
   	 	marginBottom: 16,
  	}
};



function getCurrentState( props ) {

	var searchByData = props.searchByData;

	return {

		isChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'CustomFilterURL', props.data.id ), 
		type 				: 	props.type,
		option 				: 	props.data,
		rowCount 			: 	props.data.row_count,
		nameField 			: 	props.nameField,
		textValue 			: 	props.data[props.nameField],
		value 				: 	props.data.id,
		selectAll 			: 	props.selectAll,
		include 			: 	props.include,
		filterChange 		: 	searchByData.filterChange,
		inclusionType 		: 	searchByData.inclusionType,
		filtersBeingEdited 	: 	props.filtersBeingEdited


	}
}

var SearchByURLCheckbox = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.data !== nextProps.data ) {

			return true;
		}

		if( this.state.inclusionType !== nextState.inclusionType ) {

			return true;
		}

		if( this.props.filtersBeingEdited !== nextProps.filtersBeingEdited ) {

			return true;
		}
			
		if( this.state.include !== nextProps.include ) {

			return true;
		}
			
		if( this.state.isChecked !== nextState.isChecked ) {

			return true;
		}
			
		if( this.state.filterChange !== nextState.filterChange ) {

			return true;
		}

		return false;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render 	: function() {

		var countField 		=	"include_count";

		switch( this.state.inclusionType.toLowerCase() ) {

			case "include" :

				countField = "include_count";

				break;

			case "nonexplicitinclude" :

				countField = "include_count";

				break;

			case "exclude" :

				countField = "exclude_count";

				break;

			case "firstpagevisited" :

				countField = "first_page_visited_count";

				break;

			case "notfirstpagevisited" :

				countField = "not_first_page_visited_count";

				break;

			case "lastpagevisited" :

				countField = "last_page_visited_count";

				break;

			case "notlastpagevisited" :

				countField = "not_last_page_visited_count";

				break;

		}

		var resCountClass 	= 	( this.state.filtersBeingEdited ) ? 'greyedOutResultCount' : 'resultCount';


		var label 			= 	(

			<div>

		    	{this.state.option[this.state.nameField]}

		    	<span className={resCountClass}>{this.state.option[countField]} visitor(s)</span>
			</div>
		)



		return (

			<div className="row">
				<div className="col-md-12">
					<Checkbox
				    	label={label}
				    	checked={this.state.isChecked}
				    	style={styles.checkbox}
				    	onCheck={this._createFilter}
				    />
				</div>		
			</div>		
		);
	},

	_createFilter : function() {

		var newState  = {

   			type          :   'CustomFilterURL', 
   			storedValue   :   {

   				criteria 	: 	this.state.include,
   				value 		: 	this.state.value, 
   				textValue 	: 	this.state.textValue,
   				searchBox 	: 	true,
   				id 			: 	this.state.option.id

   			},
   			id 				: 	this.state.option.id,
			
   		}

   		var checked = this.state.isChecked;

   		if( this.state.isChecked ) {

   			checked = false;
   		
   		} else {

   			checked = true;
   		}

   		this.setState({

   			isChecked : checked
   		})

   		if( checked ) {

   			FilterActions.createFilter( newState );
   		
   		} else {

   			FilterActions.removeFilterByType({

   				type 	: 	'CustomFilterURL',
   				id 		: 	this.state.option.id
   			});
   		}
	}
});

export default  SearchByURLCheckbox;