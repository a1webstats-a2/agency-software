import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import FilterActions from '../../actions/FilterActions';
import TextField from 'material-ui/TextField';
import FiltersStore from '../../stores/FiltersStore';
import {Tabs, Tab} from 'material-ui/Tabs';
import ExistingNotes from './ExistingNotes.react';

function getCurrentState( props ) {

	return {

		open 	 	: 	props.open,
		notes 		: 	props.notes

	}
}

const styles = {
  	
  	headline: {
    	fontSize: 24,
   	 	paddingTop: 16,
    	marginBottom: 12,
   	 	fontWeight: 400,
  	},
};

var CreateNote = React.createClass({

	getInitialState : function() {

		var currentState 		= getCurrentState( this.props );
		currentState.noteText 	= '';
		currentState.subject 	= '';

		return currentState;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( newProps, newState ) {

		if( newProps.notes !== this.props.notes  ) {

			return true;
		}

		if( this.props.open !== newProps.open ) {

			return true;
		}

		return false;
	},

	changeOpenState : function() {


	},

	closeCreateNote : function() {

		FilterActions.closeLightbox();

	},

	changeNoteText : function( newText ) {


		this.setState({

			noteText : 	newText.target.value 
		});
	},

	changeSubject : function( event ) {

		this.setState({

			subject : event.target.value
		})
	},

	render : function() {

		const actions = [
        	
        	<FlatButton
            	label="Close"
            	secondary={true}
            	onTouchTap={this.closeCreateNote}
            />,

        	<FlatButton
                label="Save"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._saveNote}
            />

        ];

		return (

			<div className="createNote">

				<Dialog
		          title="Notes"
		          modal={false}
		          actions={actions}
		          changeOpenState={this.changeOpenState}
		          open={this.state.open}
		          onRequestClose={this.closeCreateNote}
                  autoScrollBodyContent={true}
		        >

		        	<Tabs>
		        		<Tab label="Notes">
				        	<ExistingNotes notes={this.state.notes} />
						</Tab>

						<Tab label="Create Note">

							<div className="row">

								<div className="col-md-12">
									<TextField
										name="subject"
										id="subject"
								    	hintText="Subject"
								    	fullWidth={true}
								    	onChange={this.changeSubject}
								    /><br />

						        	<TextField
						        		name="noteText"
						        		id="noteText"
						        		onChange={this.changeNoteText}
								      	hintText="Note"
								      	fullWidth={true}
								      	multiLine={true}
								      	rows={8}
								      	rowsMax={8}
								    />
								</div>
							</div>
						</Tab>

				    </Tabs>

		        </Dialog>

			</div>
		)
	},

	_saveNote : function() {

		FilterActions.saveNote({

			note 	: 	this.state.noteText,
			subject : 	this.state.subject,
			orgId 	: 	FiltersStore.getNoteOrganisationId()

		});
	}
});

export default  CreateNote;