import React from 'react';
import FiltersStore	from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';

import Checkbox from 'material-ui/Checkbox';

const styles = {

  	checkbox: {
    	marginBottom: 16
  	}
};

function getCurrentState( osProps ) {

	return {

		isChecked 	: osProps.checked,
		osData 		: osProps.osData
	}
}

var OperatingSystemFilter = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );

	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.isChecked !== nextProps.checked ) {

			return true;
		}

		return false;
	},

	render : function() {

		var osName = ( typeof this.state.osData !== "undefined" ) ? this.state.osData.name : '';

		return (

			<div className="col-md-4">
			
				<Checkbox
					label={osName}
					style={styles.checkbox}
					checked={this.state.isChecked}
					onCheck={this._saveCheckedState}
				/>
			</div>

		)

	},

	_saveCheckedState : function( event ) {

		var newState  = {

   			type          	:   "osFilter", 
   			id 				: 	this.state.osData.id,
   			storedValue 	:   {

				id 		: 	this.state.osData.id,
				name 	: 	this.state.osData.name,
				value 	: 	this.state.osData.id
			},
			overrideKey 	: "os" + this.state.osData.id, 

   		}

   		var checked = event.target.checked;


   		if( checked ) {

   			FilterActions.createFilter( newState );

   		} else {

   			FilterActions.removeFilterByType({

   				type 	: 	'osFilter',
   				id 		: 	this.state.osData.id
   			});
   		}
	}
})

export default  OperatingSystemFilter;