import React from 'react';
import Checkbox from 'material-ui/Checkbox';
import AdvancedFilterActions from '../../actions/AdvancedFilterActions';
import TimerIcon from 'material-ui/svg-icons/image/timer';

function getCurrentState(props) {
    return {
        checked: props.checked,
        choice: props.choice,
        type: props.type
    }
}

const FilterChoice = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },
    render: function () {
        var timeSpent = '';

        if (this.state.type.toLowerCase() === "pagevisited" && this.state.checked) {

            timeSpent = <TimerIcon onClick={this._openTimeSettings}/>
        }

        return (
            <div>
                <div className="row">
                    <div className="col-md-9">
                        <Checkbox
                            onCheck={this._setFilterChoice}
                            checked={this.state.checked}
                            label={this.state.choice[1]}
                            labelStyle={{fontWeight: 'normal'}}
                        />
                    </div>
                    <div className="col-md-3">
                        {timeSpent}
                    </div>
                </div>
            </div>
        )
    },

    _openTimeSettings: function () {
        if (typeof this.state.choice[0] !== "undefined" &&
            this.state.choice[0]) {

            let timeFilter = this.state.choice[0];

            if (typeof timeFilter === "string") {
                timeFilter = timeFilter.trim();
            }

            timeFilter = parseInt(timeFilter);

            AdvancedFilterActions.openTimeSettings(timeFilter);

        }
    },
    _setFilterChoice: function (obj, isChecked) {
        AdvancedFilterActions.setFilterChoice({
            id: parseInt(this.state.choice[0]),
            display: this.state.choice[1],
            addFilter: isChecked
        })
    }
});

export default FilterChoice;
