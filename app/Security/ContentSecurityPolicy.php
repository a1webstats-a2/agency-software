<?php

declare(strict_types=1);

namespace App\Security;

use Spatie\Csp\Directive;
use Spatie\Csp\Keyword;
use Spatie\Csp\Policies\Policy;

final class ContentSecurityPolicy extends Policy
{
    public function configure(): void
    {
        $this
            ->addDirective(Directive::BASE, Keyword::SELF)
            ->addDirective(Directive::CONNECT, Keyword::SELF)
            ->addDirective(Directive::DEFAULT, Keyword::SELF)
            ->addDirective(Directive::FORM_ACTION, Keyword::SELF)
            ->addDirective(Directive::IMG, Keyword::SELF)
            ->addDirective(Directive::MEDIA, Keyword::SELF)
            ->addDirective(Directive::OBJECT, Keyword::NONE)
            ->addDirective(Directive::SCRIPT, Keyword::SELF)
            ->addDirective(Directive::STYLE, Keyword::SELF)
            ->addDirective(Directive::STYLE, '*.cloudflare.com')
            ->addDirective(Directive::FRAME, '*.stripe.com')
            ->addDirective(Directive::SCRIPT, '*.stripe.com')
            ->addDirective(Directive::SCRIPT, '*.polyfill.io')
            ->addDirective(Directive::SCRIPT, '*.ravenjs.com')
            ->addDirective(Directive::FONT, '*.gstatic.com')
            ->addDirective(Directive::SCRIPT, '*.gstatic.com')
            ->addDirective(Directive::IMG, '*.gstatic.com')
            ->addDirective(Directive::IMG, '*.websuccess-data.com')
            ->addDirective(Directive::IMG, '*.amazonaws.com')
            ->addDirective(Directive::IMG, Keyword::SELF)
            ->addDirective(Directive::CONNECT, 'w3.org')
            ->addDirective(Directive::FONT, Keyword::SELF)
            ->addDirective(Directive::SCRIPT, '*.pusher.com')
            ->addDirective(Directive::CONNECT, 'ws://ws-eu.pusher.com')
            ->addDirective(Directive::CONNECT, '*.googleapis.com')
            ->addDirective(Directive::DEFAULT, '*.googleapis.com')
            ->addDirective(Directive::IMG, '*.googleapis.com')
            ->addDirective(Directive::STYLE, '*.googleapis.com')
            ->addDirective(Directive::SCRIPT, '*.googleapis.com')
            ->addDirective(Directive::STYLE, '*.jsdelivr.net')
            ->addDirective(Directive::SCRIPT, '*.google.com')
            ->addDirective(Directive::STYLE, Keyword::UNSAFE_INLINE)
            ->addDirective(Directive::IMG, 'data:');
    }
}
