<?php
$configData = getAgentConfigData();
$loginLogo = $configData['logo'] ?? '';

if (!isset($agentConfig)) {
    $agentConfig = $configData;
}

if ($agentConfig['agentID'] === 12) {
    $favicon = env('API_SOURCE') . '/favicon.ico';
} else {
    $favicon = 's3://hostimages1/favicons/' . $agentConfig['agentID'] . '.ico';
}
?>

        <!doctype html>
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en"> <!--<![endif]-->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<head>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">

    <link rel='shortcut icon' href='{{ $favicon }}' type='image/x-icon' />
    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <meta name="robots" content="noindex,nofollow"/>
    <title>{{ $configData['name'] }}</title>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style>
        .a1HeaderLogo {
            margin-left: 40px;
        }

        p {
            margin-left: 0 !important;
        }

        .bugImg {
            float: right;
        }
    </style>
</head>
<body id="signupContent">

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div id="signupForm" class="signupForm">
            <img src="/api/proxy/api/v1/images/agent-logo" />
            @yield( 'content' );
        </div>
    </div>
</div>

<div class="clr"></div>
<footer>
</footer>

<?php if(!empty($scripts)): ?>
    <?php foreach( $scripts as $scr ) : ?>
        <script src="<?= $scr ?>"></script>
    <?php endforeach ?>
<?php endif ?>

</body>
</html>
