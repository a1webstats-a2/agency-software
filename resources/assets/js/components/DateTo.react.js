import React                   from 'react';
import FilterActions           from '../actions/FilterActions';
import Today from 'material-ui/svg-icons/action/today';


import DatePicker from 'material-ui/DatePicker';



var DateToBox  =  React.createClass({

    getInitialState :   function() {

        return {

            myKey           :   this.props.myKey,
            storedValue     :   this.props.storedValue || '2016-01-01'
        };
    },

    returnDateFrom :    function( e, date ) {

        this.setState({

            storedValue  :   date
        })
    },

    componentWillReceiveProps : function( nextProps ) {

        this.setState({

            storedValue : nextProps.storedValue
        })
    },

    shouldComponentUpdate : function( nextProps, nextState ) {

        if( this.state.storedValue !== nextState.storedValue ) {

            return true;
        }

        if( this.props.storedValue !== nextProps.storedValue ) {

            return true;
        }

        return false;
    },

    formatDate  :   function( d ) {

        return d.getFullYear() + '-' + ( "0" + ( d.getMonth() + 1 ) ).slice( -2 ) + '-' + ("0" + d.getDate()).slice(-2)
    },

    getInitialDate : function() {

        return new Date( this.state.storedValue );
    },

    render  :   function() {

        var initialDate = this.getInitialDate();

        return (

            <div className="dateFromBox form-group">
                <div className="row">
                    <div className="col-md-2">
                        <Today />
                    </div>
                    <div className="col-md-8 overflowHidden">
                        <label className="control-label">Date To:</label>

                        <DatePicker 
                            id="dateTo"
                            name="dateTo"
                            autoOk={true}
                            formatDate={this.formatDate} 
                            value={initialDate}
                            onChange={this._updateDate}  />


                    </div>
                  
                </div>

            </div>

        )
    },

    _removeFilter   :   function( e ) {

        FilterActions.removeFilter( this.state.myKey );
    },

    _updateDate     :   function( e, date ) {

        this.setState({

            storedValue : this.formatDate( date )
        })

        FilterActions.editFilter({

            id              :   this.state.myKey,
            storedValue     :   this.formatDate( date ),
            type            :   'dateTo'
        });

    } 
})

export default  DateToBox;