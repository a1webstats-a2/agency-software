@extends( 'templates.signup' )
@section( 'content' )
    
    <div class="row">
        <div  class="col-md-6 col-md-offset-3">

            <div id="loginForm">

                <div id="a1strap">
                    <h1>{{ $agentData['name'] }}</h1>
                </div>

                <br /><br />

                <p>We’re sorry that you wish to cancel your {{ $agentData['name'] }} subscription, 
                    but understand that everyone has their reasons.</p>
             
                <p>We are dedicated to constantly improve the {{ $agentData['name'] }} system
                     and so would really appreciate it if you could please supply 
                     us with a few words detailing your reasons for cancellation
                      in the box below?</p>
             
                <p>Typical reasons for cancellation are:</p>

                <ul>
                    <li>Lack of resources to follow up on identified companies.</li>
                    <li>Limited usage of the {{ $agentData['name'] }} system (so not getting full
                         value from the functionality on offer).</li>
                    <li>Moved to a competitor product.</li>
                </ul>
                <div class="clr"></div><br /><br />

                <div>

                    <form method="POST">

                     

                        <textarea name="reasonForCancellation"></textarea>
                        <div class="clr"></div><br /><br />

                        <div class="form-group">
                            <label for="password">Please enter your password to confirm cancellation</label>

                            <input type="password" name="password" class="form-control" id="password" >
                        </div>

                        <div class="clr"></div><br /><br />
                        <button class="btn btn-danger" type="submit" alt="login">Cancel Subscription</button>
                    </form>


                </div>

            </div>
        </div>
    </div>
@endsection