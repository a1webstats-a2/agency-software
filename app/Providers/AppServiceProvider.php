<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Validator::extend( 'a2UniqueEmail', function( $attribute, $value, $parameters, $validator ) {

            $client         = new \GuzzleHttp\Client();

            $response       = $client->request( 'POST', env( 'API_SOURCE' ) . '/validation/unique-email', [

                'form_params' => [ 

                    'emailAddress'     =>  $value,
                
                ]
            ]);

            $response       =   json_decode( $response->getBody()->getContents() );

            return !$response->alreadyRegistered;
        });

        Validator::extend( 'agentCodeCorrect', function( $attribute, $value, $parameters, $validator ) {

            $client         = new \GuzzleHttp\Client();

            $response       = $client->request( 'POST', env( 'API_SOURCE' ) . '/validation/agent-code', [

                'form_params' => [ 

                    'agentCode'     =>  $value
                ]
            ]);

            $response       =   json_decode( $response->getBody()->getContents() );

            return filter_var( $response, FILTER_VALIDATE_BOOLEAN );
        });

        Validator::extend( 'recaptcha', function( $attribute, $value, $parameters, $validator ) {

            $client     = new \GuzzleHttp\Client();

            $googleResponse = $client->request( 'POST', 'https://www.google.com/recaptcha/api/siteverify', [

                    'form_params'   =>  [

                        'secret'    =>  '6Lf_Yw4UAAAAAJxqh3iFnXxhuHURUjcv7iw_A_Ud',
                        'response'  =>  $value,
                        'remoteip'  =>  $_SERVER['REMOTE_ADDR']

                    ]
                ]
            );

            $response   = json_decode( $googleResponse->getBody()->getContents() );

            return $response->success;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
