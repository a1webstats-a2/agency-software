import React from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import FiltersStore from '../../stores/FiltersStore';

function getCurrentState( props ) {

	return {

		loading 	: 	props.isLoading
	}
}

const style = {
	container: {
    	position: 'relative',
  	},
  	refresh: {
   	 	display: 'inline-block',
    	position: 'relative',
  	}	
};


var SearchByRefreshIndicator = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.isLoading !== nextProps.isLoading ) {

			return true;
		}

		return false;
	},

	render : function() {

		return (

			<div>
				<RefreshIndicator
			    	size={40}
			    	left={0}
			    	top={0}
			      	status={this.state.loading}
			      	style={style.refresh}
			    />

			</div>

		)
	}
});

export default  SearchByRefreshIndicator;