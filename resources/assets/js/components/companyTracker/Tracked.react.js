import React from 'react';
import TrackedCompany from './TrackedCompany.react';
import SessionResult from '../sessionResult/SessionResult.react';
import FiltersStore from '../../stores/FiltersStore';
import TrackedCompaniesStore from '../../stores/TrackedCompaniesStore';
import TrackedCompaniesActions from '../../actions/TrackedCompaniesActions';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import UserTipHelpLink from '../UserTipHelpLink.react';
import UserTips from '../UserTips.react';

function getCurrentState( props ) {

	return {

		tracked 				: 	props.tracked,
		clientTags 				: 	props.tags,
		relationships 			: 	props.relationships,
		assignedTo 				: 	props.assignedTo,
		settings 				: 	props.settings
	}
}

var Tracked = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.tracked !== nextProps.tracked ) {

			return true;
		}

		if( this.props.clientTags !== nextProps.clientTags ) {

			return true;
		}


		if( this.props.assignedTo !== nextProps.assignedTo ) {

			return true;
		}

		if( this.props.relationships !== nextProps.relationships ) {

			return true;
		}

		return false;
	},

	render : function() {

		var companyRecords = this.state.tracked.map( function( result, i ) {

			if( result ) {

				return <TrackedCompany settings={this.state.settings} myKey={i} relationships={this.state.relationships} tags={this.state.clientTags} data={result} key={i} /> 
			}

			return false;
			
		}.bind( this ) );

		return (

			<div className="reportBuilder">

				<div className="row">

					<div className="col-md-3">

						<h3>Assigned to:</h3>

						<SelectField value={this.state.assignedTo} onChange={this._changeAssignedTo}>
				          	<MenuItem value={1} primaryText="All" />
				          	<MenuItem value={2} primaryText="Me" />
				        </SelectField>	

					</div>

					<div className="col-md-9">

						<div className="row">
							<div className="col-md-3">

								<h3>Company Tracker</h3>
							</div>
							<div className="col-md-9">
								<div style={{ marginTop : 25 }}>
									<UserTipHelpLink />
								</div>
							</div>
						</div>

						<div className="clr"></div><br /><br />

						{companyRecords}

					</div>

				</div>

				<UserTips />
			</div>
		)
	},

	_changeAssignedTo : function( event, key, payload ) {

		TrackedCompaniesActions.setAssignedToOption( payload );
	}
});

export default  Tracked;