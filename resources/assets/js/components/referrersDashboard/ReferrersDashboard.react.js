import React from 'react';

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import QuickExport 		from '../export/QuickExport.react';
import FilterActions 		from '../../actions/FilterActions';
import FiltersStore  		from '../../stores/FiltersStore'; 
import NotificationsStore 	from '../../stores/NotificationsStore';
import NavBar 				from '../navigation/NavBar.react';
import SnackbarA1 			from '../Snackbar.react';
import Footer 				from '../Footer.react';
import QuickExportCheckbox 	from '../export/QuickExportCheckbox.react';
import UserTipHelpLink 		from '../UserTipHelpLink.react';

function getCurrentState( props ) {

	return {

		looseFilterSetDisplay 	: 	FiltersStore.getLooseFilterSetDisplay(),
		scenariosFilterObj 		: 	FiltersStore.getScenarioFiltersObj(),
		displayPerPage 			: 	FiltersStore.getDisplayPerPageOnLandingPages(),
		landingPageNo 			: 	FiltersStore.getCurrentLandingPageNo(),
		quickExportResults 		: 	FiltersStore.getQuickExportSearchResults(),
		quickExportSearchString : 	FiltersStore.getQuickExportSearchString(),
		settings 				: 	FiltersStore.getAllSettings(),
		open 					: 	FiltersStore.checkIfDrawerOpen( 8 ),
		data 					: 	FiltersStore.getAnalyticsData().referrers,
		quickExportSettings 	: 	FiltersStore.getQuickExportSettings(),
		isApplicationResting 	: 	FiltersStore.isApplicationResting(),
		snackbarSettings  		:	FiltersStore.getSnackbarSettings(),
		notificationsData 		: 	{

			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),
			accountOK 				: 	FiltersStore.accountIsOK(),
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()

		},

		latestAddedFilters 		: 	FiltersStore.getLatestAddedFilters(),
		displayFancyFilters 	: 	FiltersStore.showFancyFilters(),
		filters 				: 	FiltersStore.getFilters(),

	}
}

function getPaginatedResults( organisations, landingPageNo, perPage = 10 ) {

	var startPage 	= landingPageNo * perPage;
	return organisations.splice( startPage, perPage );
}

var ReferrersDashboard = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	componentDidMount: function() {

		FiltersStore.addChangeListener( this._onChange );

		FilterActions.selectAllOptionsIfSelectionEmpty();

	},

	componentWillUnmount: function() {
		
		FiltersStore.removeChangeListener( this._onChange );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.state.quickExportSearchString !== nextState.quickExportSearchString ) {

			return true;
		}

		if( this.state.looseFilterSetDisplay !== nextState.looseFilterSetDisplay ) {

			return true;
		}

		if( this.state.quickExportSettings !== nextState.quickExportSettings ) {

			return true;
		}

		if( this.state.quickExportResults !== nextState.quickExportResults ) {

			return true;
		}

		if( this.state.notificationsData !== nextState.notificationsData ) {

			return true;
		}

		if( this.state.snackbarSettings !== nextState.snackbarSettings ) {

			return true;
		}

		if( this.state.data !== nextState.data ) {

			return true;
		}

		if( this.state.quickExportSettings.selection  !== 
			nextState.quickExportSettings.selection ) {

			return true;
		}

		return false;
	},

	render : function() {

		var resultsCount 	= ( typeof this.state.quickExportResults !== "undefined" ) ? this.state.quickExportResults.length : 0;
		var perPage 		= this.state.displayPerPage;
		var currentPage 	= ( this.state.landingPageNo + 1 );
		var disableNext 	= ( ( ( this.state.landingPageNo * perPage ) + perPage ) >= resultsCount ) ? true : false;
		var totalPages 		= ( typeof this.state.quickExportResults !== "undefined" ) ? Math.ceil( resultsCount / perPage ) : 0;
		var disablePrev 	= ( this.state.landingPageNo === 1 ) ? true : false;

		var paginationStats = (

			<div>

				<p><strong>{this.state.quickExportResults.length}</strong> results | Page <strong>{currentPage}</strong> of <strong>{totalPages}</strong></p><br />

				<div className="clr"></div><br />

			</div>
		)

		var display = "";

		if( !this.state.isApplicationResting ) {

			var trs = [];
		
		} else {

			var showResults = getPaginatedResults( this.state.quickExportResults, this.state.landingPageNo, this.state.displayPerPage );

			var trs = showResults.map( function( referrer, i ) {

				var isSelected = false;

				if( typeof referrer[0] === 'string' || referrer[0] instanceof String ) {

					referrer[0] = referrer[0].trim();
				}

				if( typeof this.state.quickExportSettings.selection[parseInt( referrer[0] )] !== "undefined" ) {

					isSelected = true;
				}

				return (

					<TableRow key={i} selectable={false} >
	        			<TableRowColumn style={{ width : '10%', backgroundColor : '#fff' }}>
	        				<QuickExportCheckbox 
	        					isChecked={isSelected} 
	        					quickExportResults={this.state.quickExportSettings.selection}
	        					data={referrer} />
	        			</TableRowColumn>
						<TableRowColumn style={{ backgroundColor : '#fff' }}>{referrer[1]}</TableRowColumn>
	        			<TableRowColumn style={{ backgroundColor : '#fff' }}>{referrer[2]}</TableRowColumn>
					</TableRow>
				)
				
			}.bind( this ) );

			if( showResults.length > 0 ) {

				display = (

					<Table 
					 	onRowSelection={this._addToExportSelection}
					 	multiSelectable={true}
					 >
						<TableHeader
							adjustForCheckbox={false}
							enableSelectAll={false}
							displaySelectAll={false}
						>
							<TableRow>
								<TableHeaderColumn style={{ width : '10%' }}>Select</TableHeaderColumn>
								<TableHeaderColumn>Referrer</TableHeaderColumn>
								<TableHeaderColumn>Total Referrals</TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody 
							displayRowCheckbox={false}
							deselectOnClickaway={false}>
							{trs}
						</TableBody>
					</Table>
				)

			} else {

				display = (

					<div className="alert alert-danger">

						No results found

					</div>

				)
			}
		}


		return (

			<div className="reportBuilder">

				<NavBar data={this.state.notificationsData} />

				<div className="clr"></div>

				<div className="container mainContainer">
					
					<div className="row">

						<div className="col-md-3">

							<div className="displayOptions">

								<QuickExport 
									type="referrers"
									landingPageNo={this.state.landingPageNo}
									disableNext={disableNext}
									displayPerPage={this.state.displayPerPage}
									filterType="customFilterReferrer"
									searchString={this.state.quickExportSearchString}
									applicationResting={this.state.isApplicationResting}
									fileTypes={this.state.quickExportSettings.fileTypes} 
									selectionType={this.state.quickExportSettings.selectionType}
									selection={this.state.quickExportSettings.selection} />

							</div>
						</div>

						<div className="col-md-9" id="results">

							<div className="row">
								<div className="col-md-2">

									<h3>Referrers</h3>
								</div>
								<div className="col-md-10">
									<div style={{ marginTop : 25 }}>
										<UserTipHelpLink />
									</div>
								</div>
							</div>

							<div className="clr"></div>

							{paginationStats}

							<div className="clr"></div><br />

							<div className="row">

								<div className="col-md-12">

									{display}
									
									<div className="clr"></div><br /><br />

									{paginationStats}
								</div>
								<div className="clr"></div><br />
							</div>
						</div>
					</div>
				</div>

				<Footer 
					looseFilterSetDisplay={this.state.looseFilterSetDisplay}				
					scenariosFilters={this.state.scenariosFilterObj}
					isApplicationResting={this.state.isApplicationResting}
					settings={this.state.settings}
					filters={this.state.filters}
					display={this.state.displayFancyFilters}
					latestAddedFilters={this.state.latestAddedFilters}
				/>

				<SnackbarA1 snackbarSettings={this.state.snackbarSettings} />

			</div>
		)
	},

	_addToExportSelection : function( selectedRows ) {

		var returnArray 	= [];

		var newResults 		= [];

		newResults 			= this.state.data;

		var mappingObj 		= newResults;

		if( selectedRows === "all" ) {

			newResults.map( function( selected, i ) {

				var newSelected = newResults[selected];

				returnArray[parseInt( newSelected[0] )] = newSelected[1];
			})

		} else {

			selectedRows.map( function( selected, i ) {

				var newSelected = this.state.quickExportResults[selected];

				returnArray[parseInt( newSelected[0] )] = newSelected[1];
			
			}.bind( this ) );
		}

		
		FilterActions.setAddOrRemoveQuickExportSelection( returnArray );
		
	},

	_onChange : function() {

		this.setState( getCurrentState() );
	}
});

export default  ReferrersDashboard;