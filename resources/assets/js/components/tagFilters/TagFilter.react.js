import React 			from 'react';
import FilterActions 	from '../../actions/FilterActions';
import FiltersStore 	from '../../stores/FiltersStore';
import Checkbox from 'material-ui/Checkbox';

function getCurrentState( props ) {

	return {

		tag 	: props.tag,
		checked : FiltersStore.checkIfFilterTypeAndIndexExists( props.type + "TagFilter", props.tag.id ),
		type 	: props.type

	};
}

const styles = {
 
	checkbox 	: 	{
		marginBottom 	: 16,
	}
};


var TabFilter = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {
		/*
		if( this.state === nextState ) {

			return false;
		}*/
		
		return true;
	},

	render : function() {

		return (

			<div className="col-md-3">
				<Checkbox checked={this.state.checked} onClick={this._createTagFilter} label={this.state.tag.type} style={styles.checkbox} />
			</div>

		)
	},

	_createTagFilter : function( event, isChecked ) {

		var checked = ( this.state.checked ) ? false : true;

   		if( checked ) {

   			var filterObj  = {

	   			type          	:   this.state.type + 'TagFilter', 
	   			storedValue 	:   {

	   				criteria 	: 	"include",
					id 			: 	this.state.tag.id,
					name 		: 	this.state.tag.type,
					value 		: 	this.state.tag.id,
					textValue 	: 	this.state.tag.type
				},
				id 				: 	this.state.tag.id
	   		}

			FilterActions.createFilter( filterObj );

		} else {

			FilterActions.removeFilterByType({

				type 	: 	this.state.type + 'TagFilter',
				id 		: 	this.state.tag.id
			})
		}
	}
});

export default  TabFilter;