@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>

@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm" class="signupForm">

				<div id="loginLogo">
                    <img src="/images/{{ $loginLogo }}" />
                </div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<div class="col-md-6">

						<ul class="progress-indicator">
				            <li>
				                <span class="bubble"></span>
				                Step 1
				            </li>
				            <li>
				                <span class="bubble"></span>
				                Step 2
				            </li>
				        	
				        </ul>

				    </div>
				</div>

				<h1>Signup Complete!</h1>
			</div>
		</div>
	</div>
@endsection