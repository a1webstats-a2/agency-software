import React 			from 'react';
import TeamMember 		from '../team/TeamMember.react';
import {List, ListItem} from 'material-ui/List';
import TeamIcon 		from 'material-ui/svg-icons/action/accessibility';
import Checkbox 		from 'material-ui/Checkbox';
import {Table,
	TableBody,
	TableRow, 
	TableRowColumn} 	from 'material-ui/Table';
import Avatar 			from 'material-ui/Avatar';
import RaisedButton 	from 'material-ui/RaisedButton';
import FilterActions 	from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {

		isLoading 				: props.isLoading,
		triggerActionSendToList : props.triggerActionSendToList,
		teamMembers 			: props.teamMembers,
		templateID 				: props.templateID
	}
}

const styles = {

	avatar 	: 	{

		marginTop		: 	10,
		marginBottom 	: 	10,
		marginRight 	: 	20
	}
}

class EmailAlerts extends React.Component {

	constructor( props ) {

		super( props );

		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( props ) {

		this.setState( getCurrentState( props ) );
	}

	render() {

		const teamMemberCheckboxes = this.state.teamMembers.map( function( teamMember, t ) {

			const avatarURL = "/api/proxy/api/v1/user/avatar/" + teamMember.id;

			const defaultChecked = ( typeof this.state.triggerActionSendToList[teamMember.id] !== "undefined" &&
				this.state.triggerActionSendToList[teamMember.id] ) ? true : false;

			return (

				<TableRow key={t} selectable={false}>
					<TableRowColumn>
						<Avatar src={avatarURL} style={styles.avatar} />
						{teamMember.forename} {teamMember.surname}
					</TableRowColumn>
					<TableRowColumn>
						<Checkbox 
							style={{ marginTop : 10 }} 
							checked={defaultChecked}
							onCheck={ ( event, isChecked ) => this._setTeamMemberAlertBool( teamMember.id, isChecked ) } />
					</TableRowColumn>
				</TableRow>
			)
		
		}.bind( this ) );

		let display = "";

		if( this.state.isLoading ) {

			display = (

				<div style={{ width : 200, height : 200 }}>
					<img src="/images/loading_large.gif" />
				</div>
			)
		
		} else {

			display = (

				<Table selectable={false}>
					<TableBody displayRowCheckbox={false}>
						{teamMemberCheckboxes}
					</TableBody>
				</Table>
			)
		}

		return (

			<div>
				<div className="clr"></div><br />
				<h2 style={{ marginLeft : 25 }}>Who would you like to receive email alerts when template criteria is met?</h2>
				<div className="clr"></div><br />
				{display}
				<div className="clr"></div><br />
				<RaisedButton 
					style={{ marginLeft : 25, marginTop : 20 }} 
					secondary={true} 
					onTouchTap={ () => this._saveChanges() }
					label="Save Changes" />
			</div>
		)
	}

	_saveChanges() {

		FilterActions.saveChangesToTriggerActionEmailAlerts();
	}

	_setTeamMemberAlertBool( teamMemberID, isChecked  ) {

		FilterActions.updateTriggerActionEmailSendToList({

			templateID 	: 	this.state.templateID,
			teamMemberID,
			isChecked
		})
	}
}

export default EmailAlerts;