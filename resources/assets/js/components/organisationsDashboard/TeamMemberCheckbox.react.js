import React from 'react';
import {List, ListItem} from 'material-ui/List';
import Checkbox from 'material-ui/Checkbox';
import Avatar from 'material-ui/Avatar';

import FilterActions from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {

		avatarSrc  				: 	props.avatarSrc,
		name 					: 	props.name,
		data 					: 	props.data,
		sendEmailToTeamMembers 	: 	props.sendEmailToTeamMembers

	}
}

var TeamMemberCheckbox = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.sendEmailToTeamMembers !== nextProps.sendEmailToTeamMembers ) {

			return true;
		}

		if( this.props.avatarSrc !== nextProps.avatarSrc ) {

			return true;
		}

		return false;
	},

	render : function() {

		var isChecked = ( this.state.sendEmailToTeamMembers.indexOf( this.state.data.id ) > -1 ) ? true :  false;

		return (

			<div>

				<ListItem
        			rightAvatar={<Avatar src={this.state.avatarSrc} />}
          			leftCheckbox={<Checkbox checked={isChecked} onClick={this._setIncludeExcludeTeamMember} />}
        			primaryText={this.state.name}
          	
    			/>
			</div>

		)
	},

	_setIncludeExcludeTeamMember : function( event, isChecked ) {

		FilterActions.addTeamMemberToSendEmailArray( this.state.data.id );
	}
});

export default  TeamMemberCheckbox;


