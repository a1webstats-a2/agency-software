import FiltersStore from '../stores/FiltersStore';

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

class Lang {

	static getWordUCFirst( word ) {

		if( typeof word === "undefined" ) {

			return "";
		}

		const wordLowerCase = word.toLowerCase();

		const lang = FiltersStore.getLang();

		if( typeof lang[wordLowerCase] !== "undefined" ) {

			return lang[wordLowerCase].capitalize();
		} 

		return wordLowerCase.capitalize();

	}

	static getWordLowerCase( word ) {

		if( typeof word === "undefined" ) {

			return "";
		}
		
		const wordLowerCase = word.toLowerCase();

		if( typeof lang[wordLowerCase] !== "undefined" ) {

			return lang[wordLowerCase];
		}

		return wordLowerCase;
	}
}

export default Lang;