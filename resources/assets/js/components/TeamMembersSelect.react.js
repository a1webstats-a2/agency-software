import React 			from 'react';
import TeamMember 		from './TeamMember.react';
import FiltersStore 	from '../stores/FiltersStore';
import FilterActions 	from '../actions/FilterActions';
import {List, ListItem} from 'material-ui/List';
import Subheader 		from 'material-ui/Subheader';
import Avatar 			from 'material-ui/Avatar';
import TeamIcon 		from 'material-ui/svg-icons/action/accessibility';
import Checkbox 		from 'material-ui/Checkbox';

function getCurrentState( props ) {


	if( props.existingReport ) {

	}

	return {

		sendToAll 		: 	props.sendToAll,
		existingReport 	: 	props.existingReport,
		teamMembers 	: 	props.teamMembers,
		include 		: 	props.includeTeamMembers
	}
}

const styles = {
	root	: {
		display		: 	'flex',
		flexWrap	: 	'wrap',

	},

	teamMemberSelect : {

		width : '100%'
	}
};


var TeamMembersSelect = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
		
	},

	componentDidMount : function() {

		if( !this.state.existingReport ) {

			FilterActions.resetSelectedTeamMembers();

		}
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.sendToAll !== nextProps.sendToAll ) {

			return true;
		}

		if( this.props.existingReport !== nextProps.existingReport ) {

			return true;
		}
		
		if( this.props.teamMembers !== nextProps.teamMembers ) {

			return true;
		}

		if( this.props.includeTeamMembers !== nextProps.includeTeamMembers ) {

			return true;
		}

		return false;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		
		var teamMemberCheckboxes 	= [];

		teamMemberCheckboxes.push( 

			<ListItem
				key={-1}
				rightAvatar={<Avatar color="#8b0000" icon={<TeamIcon  />} />}
				primaryText="All Team Members"
		        insetChildren={true}
				leftCheckbox={<Checkbox checked={this.state.sendToAll} value="allTeam" onCheck={this._setWholeTeam} />} />


		);

		this.state.teamMembers.map( function( member, i ) {
	
			var teamMemberSelected = false;

			if( this.state.include.indexOf( member.id ) > -1 ) {

				teamMemberSelected = true;
			}

			teamMemberCheckboxes.push( <TeamMember selected={teamMemberSelected} disabled={this.state.sendToAll} include={this.state.include} key={i} member={member} /> );

		}.bind( this ));

		return (

			<div className="teamMembers">

				<div style={styles.root}>
					<List style={styles.teamMemberSelect}>
						{teamMemberCheckboxes}
					</List>
				</div>
			</div>
		)
	},

	_setWholeTeam : function() {

		var sendToAll = ( this.state.sendToAll ) ? false : true;

		FilterActions.setExportCriteriaSendToAll( sendToAll );
	}
});

export default  TeamMembersSelect;