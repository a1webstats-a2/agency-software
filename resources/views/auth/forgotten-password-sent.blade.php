@extends( 'templates.signup' )
@section( 'content' )
    
    <div class="row">
        <div  class="col-md-6 col-md-offset-3">

            <div id="signupForm" class="signupForm">

                <div id="a1strap">
                    <h1>{{ $agentData['name'] }}</h1>
                </div>

                <br /><br />

                <p>If we have an account matching the email address {{ $email }},
                    a reset password link has been sent to that address.</p>
                <p>Please follow the instructions in this email.</p>
                <p>Thank you</p>
            
            </div>
        </div>
    </div>
@endsection