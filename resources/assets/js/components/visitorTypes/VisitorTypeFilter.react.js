import React 			from 'react';
import FiltersStore		from '../../stores/FiltersStore';
import FilterActions 	from '../../actions/FilterActions';
import Checkbox 		from 'material-ui/Checkbox';

const styles = {

  	checkbox: {
    	marginBottom: 16
  	}
};

var VisitorTypeFilter = React.createClass({

	getInitialState : function() {

		return {

			isChecked 	: FiltersStore.checkIfFilterTypeAndIndexExists( "visitorTypeFilter", this.props.myKey ),
			visitorType : this.props.visitorType,
			myKey 		: parseInt( this.props.myKey )
		}
	},

	componentWillReceiveProps : function() {

		this.setState({

			isChecked : FiltersStore.checkIfFilterTypeAndIndexExists( "visitorTypeFilter", this.state.myKey )
		})

	},

	render : function() {

		return(

			<div className="col-md-4">
				
				<Checkbox
					label={this.state.visitorType}
					style={styles.checkbox}
					checked={this.state.isChecked}
					onCheck={this._saveCheckedState}
				/>
			</div>

		);
	},

	_saveCheckedState : function( event ) {

		var filterNames = [

			'Organisations',
			'Education',
			'ISPs',
			'Crawl Bots',
			'Public',
			'Unknown'

		]

		var newState  = {

			isChecked 	: FiltersStore.checkIfFilterTypeAndIndexExists( "visitorTypeFilter", this.props.myKey ),
   			type          	:   "visitorTypeFilter", 
   			storedValue 	:   {

				id 			: 	this.state.myKey,
				name 		: 	this.state.myKey,
				value 		: 	this.state.myKey,
				textValue 	: 	filterNames[ ( this.state.myKey -1 ) ]
			},

			id 	: 	this.state.myKey
   		}

   		var checked = event.target.checked;

   		this.setState({

   			isChecked : checked
   		})

   		if( checked ) {

   			FilterActions.createFilter( newState );


   		} else {

   			FilterActions.removeFilterByType({

   				type 	: 	"visitorTypeFilter",
   				id 		: 	this.state.myKey
   			});
   		}
	}


});

export default  VisitorTypeFilter;