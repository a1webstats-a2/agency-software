import React 				from 'react';
import domtoimage 			from 'dom-to-image';
import ScreenRecorder 		from '../classes/ScreenRecorder';
import ScreenRecorderStore 	from '../stores/ScreenRecorderStore';

class ScreenshotTest extends React.Component {

	constructor( props ) {

		super( props );
		const recordingObj 	= new ScreenRecorder;
		this.state = {

			playObj 		: '',
			recordingObj
		}
	}	

	componentDidMount() {


	}

	componentDidUpdate() {


	}

	render() {

		return (

			<div>

				{this.state.playObj}

				<h2>Screenshot Test</h2>

				<div className="clr"></div><br /><br /><br />

				<button onClick={ () => this._stopRecording() }>STOP recording</button>

				<div className="clr"></div><br /><br /><br />

				<button className="track">track me</button>	

				<form>

					<label>Take new screenshots when I type here please!</label>
					<input type="text" />

				</form>

				<div>
					<img src="/images/whale.jpg" onLoad={() => this._startRecording()} height="1200" />
				</div>
			</div>
		)
	}

	_startRecording() {

		//this.state.recordingObj.startRecording()
	}

	_playRecording() {

		this.state.playObj = this.state.recordingObj.play();
	}

	_stopRecording() {

		this.state.recordingObj.stopRecording();

		alert( "Recording stopped" );
	}
}

export default ScreenshotTest;