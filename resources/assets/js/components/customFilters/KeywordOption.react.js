import React from 'react';


var KeywordOption = React.createClass({

	getInitialState : function() {

		return {

			keyword : 	this.props.keyword,
			key 	: 	this.props.myKey,
			id 		: 	this.props.myKey
		}
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.keyword !== nextProps.keyword ) {

			return true;
		}

		if( this.props.myKey !== nextProps.myKey ) {

			return true;
		}

		return false;
	},

	render : 	function() {


		return (	

			<option value={this.state.id}>{this.state.keyword}</option>

		); 
	}

});

export default  KeywordOption;