import React from 'react';


function setState( props ) {

	return {

		id 		: 	props.myKey,
		name 	: 	props.pageName
	};
}

var PageOption = React.createClass({

	getInitialState 	: 	function() {

		return setState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( setState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.pageName !== nextProps.pageName ) {

			return true;
		}

		return false;

	},

	render 	: 	function() {

		return(

			<option value={this.state.id}>{this.state.name}</option>

		);
	}
});

export default  PageOption;