![Picture](https://api1.websuccess-data.com/images/agents/12.png)

## About A1WebStats Agency Software

This software is for organisations who wish to white label services
provided by A1WebStats. The software uses the A1WebStats API to retrieve 
client data and cannot be used in conjunction with any other alternative
API.

### Installing

1. Ensure that you have the correct server requirements. A LAMP ( although no DB is required for this software ) style platform is required. Please also ensure that you meet the system criteria  stated within the [Lumen documentation](https://lumen.laravel.com/docs/5.4)
2. Clone the repo
3. Run command 'sh install.sh'
4. Run 'npm install'


### Configuration

* We recommend configuring the software using a localhost. Using PHP this can be done by running 'php -S localhost:8000' via the terminal. Please ensure you are in the public folder when running this command
* If all has gone correctly, you should now be presented with an installation page:

![Picture](https://api1.websuccess-data.com/images/agent-installation.png)

## License

A1WebStats Agency Software ( built upon Laravel framework ) is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

This software is solely for the use of authorised white labellers using the A1 API to source and display data within.
