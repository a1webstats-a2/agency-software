<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;

final class Dashboard extends Controller
{
    public function termsAndConditions()
    {
        return view('UI.endUsers.terms', []);
    }

    public function index()
    {
        return view('UI.dashboard', [
            'agentConfig' => getAgentConfigData()
        ]);
    }
}
