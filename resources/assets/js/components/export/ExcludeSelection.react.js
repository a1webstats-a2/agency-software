import React from 'react';

var getCurrentState = function (props) {
    return {
        excludeSelection: props.selection,
        byName: props.selectionByName
    }
}

var ExcludeSelection = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.selection !== nextProps.selection) {
            return true;
        }

        return false;
    },
    render: function () {
        const trs = this.state.excludeSelection.map(function (organisation, i) {
            return (
                <tr key={i}>
                    <td>{this.state.byName[organisation]}</td>
                </tr>
            )
        }.bind(this));

        let title = '';

        if (this.state.excludeSelection.length > 0) {
            title = (
                <h4>Exclude Selection</h4>
            )
        }

        return (
            <div className="exportSelection">
                {title}
                <table className="table">
                    <tbody>
                    {trs}
                    </tbody>

                </table>
            </div>
        )
    }
});

export default ExcludeSelection;
