import React 	from 'react';
import Pusher   from 'pusher-js';

class FilterLog extends React.Component {

	constructor( props ) {

		super( props );

		this.state = {

			logs 	: 	[]
		}
	}

    componentDidMount() {

        var pusher = new Pusher( 'd8ef5a0ae7a920f0b203', {
            
            cluster         : 'eu',
            encrypted       : false
        });

        var channel = pusher.subscribe( 'a1' );


        let thisObj = this;

        channel.bind( 'filter-log' , function( data ) {
        
        	let logs 	= thisObj.state.logs;
            logs.unshift( data );
        	thisObj.setState({ logs : logs });

        });

    }

	render() {

		let displayLogs = this.state.logs.map( function( log, l ) {

			let logHTML = (

				<div dangerouslySetInnerHTML={{ __html : log }} />
			)

			return (

				<div className="row" key={l}>	

					<div className="col-md-12">

						{logHTML}

						<button onClick={() => this._logError( log )} className="btn btn-danger">Log Error</button>

						<div className="clr"></div><br /><br />

					</div>

				</div>
			);
			
		}.bind( this ));

		return (

			<div className="reportBuilder" style={{ background : '#000', color : '#fff' }}>

				<div className="clr"></div>

				<div className="container mainContainer" style={{ marginTop : 0 }}>
					
					<div className="row">

						<div className="col-md-12" id="results">
							
							<h2>Filter Log</h2>

							<div className="clr"></div><br /><br />

							{displayLogs}

						</div>
					</div>
				</div>
			</div>
		)
	}

	_logError( log ) {

		const xhr 		= new XMLHttpRequest();
	  	xhr.open( "POST", "/api/proxy/api/v1/report/no-results-via-filter-log" );
		xhr.setRequestHeader( 'Content-Type', 'application/json' );
	  	xhr.send( JSON.stringify( { log } ) ); 
	}
}

export default FilterLog;