import React from 'react';
import FilterActions from '../../actions/FilterActions';
import ResultTabs from './ResultTabs.react';
import SessionResultData from './SessionResultData.react';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Comment from 'material-ui/svg-icons/communication/comment';
import {List, ListItem} from 'material-ui/List';
import IntelligenceIcon from 'material-ui/svg-icons/action/visibility';
import FlagIcon from 'material-ui/svg-icons/content/flag';
import Paper from 'material-ui/Paper';
import EditRecordIcon from 'material-ui/svg-icons/content/create';
import HistoryIcon from 'material-ui/svg-icons/action/history';
import CRMIcon from 'material-ui/svg-icons/communication/import-contacts';
import normalizeUrl from 'normalize-url';
import DisplayTag from './DisplayTag.react';
import ReactImageFallback from "react-image-fallback";

const styles = {
    popover: {
        padding: 20
    },
    checkbox: {},
    headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },
    paper: {
        marginBottom: 50,
        width: '100%'
    },
    standardTable: {
        td: {
            width: '40%'
        }
    },
    buttons: {
        secondary: {
            margin: 12
        },
        primary: {
            margin: 12
        }
    },
    svg: {
        color: '#7D5AFF'
    }
};

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}

function hasWhiteSpace(s) {
    return s.indexOf(' ') >= 0;
}

var SessionResult = React.createClass({
    getInitialState: function () {
        var pageVisits = JSON.parse(this.props.data.page_visits);
        var pageIds = [];
        var fullReferrerURL = false;
        var sessionDuration = 0;
        var numImpressions = 0;
        var newPageVisits = [];
        if (pageVisits && pageVisits.length > 0) {
            numImpressions = pageVisits.length;
            pageVisits.reverse();
            if (typeof pageVisits[0].duration_seconds !== "undefined") {
                newPageVisits = pageVisits.map(function (page, p) {
                    let queryString = "";
                    if (!hasWhiteSpace(page.query_string)) {
                        queryString = page.query_string;
                    }

                    pageIds.push(page.page_id);

                    let newPage = [];
                    newPage[0] = page.url;
                    newPage[1] = page.duration_seconds;
                    newPage[2] = page.timezone;
                    newPage[3] = page.name;
                    newPage[5] = page.meta_title;
                    newPage[6] = queryString;
                    newPage[10] = page.page_visit_id;
                    newPage[11] = page.page_id;
                    newPage[12] = page.https;
                    return newPage;
                });

            } else {
                newPageVisits = pageVisits.map(function (page, p) {
                    var newArray = page.split("|");
                    pageIds.push(newArray[3]);
                    sessionDuration += parseInt(newArray[1]);

                    if (p === 0) {
                        var fullReferrer = newArray[4];

                        if (fullReferrer.toLowerCase() !== "unknown") {

                            fullReferrerURL = fullReferrer;
                        }
                    }

                    return newArray;
                })
            }
        }

        if (sessionDuration === 0 && numImpressions === 1) {
            sessionDuration = "one page visit";
        } else if (sessionDuration > 60) {
            var newSessionDuration = sessionDuration / 60;
            var sessionMinutes = Math.floor(newSessionDuration);
            var subtract = sessionMinutes * 60;
            var sessionSeconds = sessionDuration - subtract;

            sessionDuration = (
                <div>
                    <bold>{sessionMinutes}</bold> minutes <bold>{sessionSeconds}</bold> seconds
                </div>
            )
        } else {
            sessionDuration = (
                <div>
                    <bold>{sessionDuration}</bold>
                    seconds
                </div>
            )
        }

        const isTracked = this.props.data.is_tracked > 0;
        const showMore = (typeof this.props.settings.user.display_page_visits_by_default === "undefined") ? true : this.props.settings.user.display_page_visits_by_default;

        return {
            heatmapData: this.props.heatmapData,
            myKey: this.props.myKey,
            isTracked: isTracked,
            settings: this.props.settings,
            pageIdsString: pageIds.join(),
            popoverOpenID: this.props.popoverOpenID,
            map: null,
            sessionDuration: sessionDuration,
            fullReferrerURL: fullReferrerURL,
            assignedTo: this.props.assignedTo,
            excludeMe: this.props.excludeMe,
            relationships: this.props.relationships,
            resultKey: this.props.myKey,
            exportMe: this.props.exportMe,
            clientTypesByIndex: this.props.clientTypesByIndex,
            popoverObj: this.props.popoverObj,
            popoverOpen: this.props.popoverOpenID === this.props.data.id,
            tags: this.props.tags,
            pageVisits: newPageVisits,
            longitude: this.props.data.longitude,
            latitude: this.props.data.latitude,
            data: this.props.data,
            showMore: showMore,
            showHistory: false,
            selectedForExport: this.props.exportMe,
            blockedOrganisations: this.props.blockedOrganisations,
        }
    },
    componentWillReceiveProps: function (newProps) {
        var pageVisits = JSON.parse(newProps.data.page_visits);
        var pageIds = [];
        var fullReferrerURL = false;
        var sessionDuration = 0;
        var numImpressions = (pageVisits) ? pageVisits.length : 0;
        var newPageVisits = [];

        if (pageVisits && pageVisits.length > 0) {
            let eventID = 0;
            let eventDescription = "";

            if (typeof pageVisits[0].duration_seconds !== 'undefined') {
                let firstPage = pageVisits[0];

                if (firstPage.name !== 'unknown') {
                    fullReferrerURL = firstPage.name;
                }

                newPageVisits = pageVisits.map(function (page) {
                    let queryString = '';

                    if (!hasWhiteSpace(page.query_string)) {
                        queryString = page.query_string;
                    }

                    pageIds.push(page.page_id);

                    if (typeof page.id !== "undefined") {
                        eventID = page.id;
                    }

                    if (typeof page.event_description !== "undefined") {
                        eventDescription = page.event_description;
                    }

                    let newPage = [];
                    newPage[0] = page.url;
                    newPage[1] = page.duration_seconds;
                    newPage[2] = page.timezone;
                    newPage[3] = page.name;
                    newPage[5] = page.meta_title;
                    newPage[6] = queryString;
                    newPage[7] = eventID;
                    newPage[8] = eventDescription;
                    newPage[10] = page.page_visit_id;
                    newPage[11] = page.page_id;
                    newPage[12] = page.https;
                    newPage[13] = page.hasOwnProperty('job_id') ? page.job_id : '';

                    sessionDuration += parseInt(page.duration_seconds);

                    return newPage;
                });
            } else {
                let firstPage = pageVisits[0].split("|");

                if (firstPage[4] !== "unknown") {
                    fullReferrerURL = firstPage[4];
                }

                newPageVisits = pageVisits.map(function (page, p) {
                    var newArray = page.split("|");

                    pageIds.push(newArray[3]);

                    newArray[7] = eventID;
                    newArray[8] = eventDescription;

                    sessionDuration += parseInt(newArray[1]);

                    return newArray;
                })
            }
        }

        if (sessionDuration === 0 && numImpressions === 1) {
            sessionDuration = "one page visit";
        } else if (sessionDuration > 60) {
            var newSessionDuration = sessionDuration / 60;
            var sessionMinutes = Math.floor(newSessionDuration);
            var subtract = sessionMinutes * 60;
            var sessionSeconds = sessionDuration - subtract;

            sessionDuration = (
                <div>
                    <bold>{sessionMinutes}</bold> minutes <bold>{sessionSeconds}</bold> seconds
                </div>
            )
        } else {
            sessionDuration = (
                <div>
                    <bold>{sessionDuration}</bold> seconds
                </div>
            )
        }

        let isTracked = (newProps.data.is_tracked > 0);

        this.setState({
            heatmapData: newProps.heatmapData,
            myKey: newProps.myKey,
            isTracked: isTracked,
            settings: newProps.settings,
            sessionDuration: sessionDuration,
            assignedTo: newProps.assignedTo,
            popoverObj: newProps.popoverObj,
            fullReferrerURL: fullReferrerURL,
            popoverOpen: (newProps.popoverOpenID === newProps.data.id),
            pageIdsString: pageIds.join(),
            relationships: newProps.relationships,
            tags: newProps.tags,
            exportMe: newProps.exportMe,
            popoverOpenID: newProps.popoverOpenID,
            excludeMe: newProps.excludeMe,
            pageVisits: newPageVisits,
            data: newProps.data,
            selectedForExport: newProps.exportMe,
            clientTypesByIndex: newProps.clientTypesByIndex,
            longitude: newProps.data.longitude,
            latitude: newProps.data.latitude,
            blockedOrganisations: newProps.blockedOrganisations,
        })
    },

    openPopover: function (event) {
        var open = (!this.state.popoverOpen);

        FilterActions.setSessionResultPopover({
            open: open,
            anchorEl: event.currentTarget,
            id: this.state.data.id
        })
    },
    handleSelect: function () {},
    openEditCompany: function () {
        FilterActions.setOrganisationData(this.state.myKey);
    },
    showHideMoreInfo: function () {
        var displayed = (!this.state.showMore);

        this.setState({
            showMore: displayed
        })
    },
    handleRequestClose: function () {
        FilterActions.closeSessionResultPopover();
    },
    openEditCompanyRecord: function () {
        FilterActions.setActiveRecord({
            data: this.state.data,
            myKey: this.state.myKey
        });
    },
    render: function () {
        if (typeof this.state.blockedOrganisations[this.state.data.organisationid] !== "undefined") {
            return (<div></div>);
        }
        let displayMoreInfo = {};
        let showHideText;

        if (this.state.showMore) {
            displayMoreInfo.display = 'inline';
            showHideText = 'Hide';
        } else {
            displayMoreInfo.display = 'none';
            showHideText = 'More';
        }

        let historyLink;
        let intelligenceLink;

        if (this.state.data.last_visit) {
            historyLink =
                <FlatButton onClick={this._showHistory} label="History" icon={<HistoryIcon color={styles.svg.color}/>}/>
        }

        if (this.state.data.contractor_sanitized) {
            intelligenceLink = <FlatButton onClick={this._showIntelligence} label="Intel"
                                           icon={<IntelligenceIcon color={styles.svg.color}/>}/>
        }

        let isTracked = (parseInt(this.state.data.assigned_user) > 0 || parseInt(this.state.data.assigned_user) === -2);
        let stringArray = this.state.data.use_organisation_name.split(" ");

        var returnString = stringArray.map(function (word, i) {
            if (word.indexOf("Ftip") === -1) {
                return word;
            } else {
                return '';
            }
        })

        let organisationName = returnString.join(" ").trim();
        let websiteURL = '';
        let website = '';

        if (this.state.data.use_organisation_website) {
            websiteURL = normalizeUrl(this.state.data.use_organisation_website);
            website = (
                <p>
                    <a href={websiteURL} onClick={this._openLink} target="_blank">{websiteURL}</a>
                </p>
            );
        }

        let subtitleText = (
            <div className="subtitleText">
                {website}
                <p>{this.state.data.use_organisation_tel}</p>
            </div>
        )

        let linkedInLink = "";

        if (this.state.data.organisation_type_id === 1) {
            const linkedInHref = "https://www.linkedin.com/search/results/companies/?keywords=" + organisationName + "&origin=SWITCH_SEARCH_VERTICAL";

            linkedInLink = (
                <a target="_blank" href={linkedInHref}>
                    <img src="/images/linkedin.png"/>
                </a>
            )
        }

        var adwordsImg = "";

        if (this.state.data.ppc && this.state.data.sanitised_referrer_name.includes("google")) {
            adwordsImg = (
                <img className="adwordsImg"
                     src="https://stock-images-api.s3-eu-west-1.amazonaws.com/ads-logo-horizontal.png"/>
            );
        } else if (this.state.data.ppc && this.state.data.sanitised_referrer_name.includes("bing")) {
            adwordsImg = (
                <img className="adwordsImg" src="/images/bingAdwords.png"/>
            );
        }

        let locationString;

        if (this.state.data.geolocation_formatted_address) {
            locationString = (
                <div>
                    {this.state.data.geolocation_formatted_address}
                    &nbsp;<a className="viewMapLink" onClick={this._viewLocation} href="#">view</a>
                </div>
            )
        } else {
            var registeredAddress = '';

            if (this.state.data.registered_address) {
                registeredAddress = (
                    <p>Registered Address: {this.state.data.registered_address}</p>
                )
            }

            locationString = (
                <div>
                    <p>Location (from
                        IP): {this.state.data.city_name}, {this.state.data.county_name}, {this.state.data.country}
                        &nbsp;<a className="viewMapLink" onClick={this._viewLocation} href="#">view</a>
                    </p>
                    {registeredAddress}
                </div>
            )
        }

        let displayTags = [];

        if (typeof this.state.relationships[this.state.data.organisationid] !== "undefined") {
            const relationships = this.state.relationships[this.state.data.organisationid];

            displayTags = relationships.map(function (relationship, i) {
                if (relationship) {
                    if (typeof this.state.clientTypesByIndex[relationship] !== "undefined") {
                        return <DisplayTag key={i} relationship={this.state.clientTypesByIndex[relationship]}/>;
                    }
                }
            }.bind(this));
        }

        let logo = "";

        if (inArray(this.state.data.organisation_type_id, [1, 2, 5, 7]) &&
            parseInt(this.state.data.has_image) !== 0 &&
            this.state.data.use_organisation_website) {

            let website = this.state.data.use_organisation_website;
            website = website.replace("http://", "");
            website = website.replace("https://", "");
            website = website.replace("www.", "");

            const logoURL = "//logo.clearbit.com/" + website + "?size=50";
            const altText = "Corporate logo for " + this.state.data.use_organisation_name;

            logo = (
                <div>
                    <ReactImageFallback
                        src={logoURL}
                        onError={(failedURL) => this._noCorporateImage(failedURL, this.state.data.organisationid)}
                        fallbackImage="/images/noOrganisationImage.png"
                        alt={altText}
                    />
                    <div className="clr"></div>
                    <br/>
                </div>
            )
        }

        return (
            <div className="sessionResult">
                <Paper style={styles.paper} zDepth={5}>
                    <div className="resultTabs ">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="customSessionResultContainer">
                                    <div className="customSessionResultContainerHeader">
                                        <div className="row">
                                            <div className="col-md-7">
                                                <p className="organisationName">{organisationName}</p>
                                                {subtitleText}
                                                {locationString}
                                            </div>
                                            <div className="col-md-3">
                                                {displayTags}
                                            </div>
                                            <div className="col-md-2">
                                                {linkedInLink}
                                                <div className="clr"></div>
                                                <br/>
                                                {logo}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="customSessionResultContainerInner">
                                        <div className="row">
                                            <div className="col-md-9">
                                                <SessionResultData
                                                    parentID={this.state.data.id}
                                                    referrer={this.state.fullReferrerURL}
                                                    settings={this.state.settings}
                                                    sessionDuration={this.state.sessionDuration}
                                                    data={this.state.data}
                                                    numberOfPages={this.state.pageVisits.length}/>
                                            </div>
                                            <div className="col-md-3">
                                                {adwordsImg}
                                                <List>
                                                    <ListItem primaryText="Track" onClick={this._trackCompany}
                                                              leftIcon={<Checkbox style={styles.checkbox}
                                                                                  checked={isTracked}
                                                                                  checkedIcon={<ActionFavorite/>}
                                                                                  uncheckedIcon={
                                                                                      <ActionFavoriteBorder/>}/>}/>
                                                    <ListItem primaryText="Export" onClick={this._addToExportSelection}
                                                              leftIcon={<Checkbox style={styles.checkbox}
                                                                                  disabled={this.state.excludeMe}
                                                                                  checked={this.state.selectedForExport}/>}/>
                                                </List>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="moreInfo" style={displayMoreInfo}>
                                                    <ResultTabs
                                                        settings={this.state.settings}
                                                        heatmapData={this.state.heatmapData}
                                                        organisationName={organisationName}
                                                        resultKey={this.state.resultKey}
                                                        data={this.state.data}
                                                        pageVisits={this.state.pageVisits}
                                                        locationString={locationString}
                                                        longitude={this.state.longitude}
                                                        latitude={this.state.latitude}
                                                        clientID={this.state.settings.team.end_user_company_id}
                                                        tags={this.state.tags}
                                                        relationships={this.state.relationships}
                                                        myKey={this.state.myKey}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="customSessionResultContainerActions">
                                            <FlatButton
                                                onClick={this.openEditCompanyRecord}
                                                label="Edit"
                                                icon={<EditRecordIcon color={styles.svg.color} />}
                                            />
                                            <FlatButton
                                                onClick={this.showHideMoreInfo}
                                                label={showHideText}
                                                icon={<Assessment color={styles.svg.color}/>}
                                            />
                                            <FlatButton
                                                onClick={this._createNote}
                                                label="Notes"
                                                icon={<Comment color={styles.svg.color}/>}
                                            />
                                            {historyLink}
                                            {intelligenceLink}
                                            <FlatButton
                                                onClick={this._sendViaEmail}
                                                icon={<EmailIcon color={styles.svg.color}/>}
                                                label="Email"
                                            />
                                            <FlatButton
                                                onClick={this._exportToCRM}
                                                icon={<CRMIcon color={styles.svg.color}/>}
                                                label="CRM"
                                            />
                                            <FlatButton
                                                onClick={this._reportRecord}
                                                icon={<FlagIcon color={styles.svg.color}/>}
                                                label=""
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Paper>
            </div>
        );
    },
    _noCorporateImage: function (failedURL, orgID) {
        if (parseInt(this.state.data.has_image) !== 0) {
            FilterActions.setOrgHasNoImage(orgID);
        }
    },
    _exportToCRM: function () {
        FilterActions.openExportToCRM(this.state.data.organisationid);
    },
    _reportRecord: function () {
        FilterActions.reportRecord(this.state.data);
    },
    _viewLocation: function (event) {
        event.preventDefault();

        const locationString = `${this.state.data.city_name}, ${this.state.data.county_name}, ${this.state.data.country}`;

        FilterActions.setCurrentMap({
            organisationName: this.state.data.use_organisation_name,
            isGeoLocated: this.state.data.geolocated,
            locationString,
            longitude: this.state.data.longitude,
            latitude: this.state.data.latitude
        });
    },
    _blockOrganisation: function () {
        if (!confirm(`Are you sure you want to block ${this.state.data.use_organisation_name}`)) {
            return false;
        }

        FilterActions.blockOrganisation({
            name: this.state.data.use_organisation_name,
            id: this.state.data.organisationid
        });
    },
    _trackCompany: function () {
        FilterActions.createNewTrackedOrganisation(this.state.myKey);
    },
    _addToExcludeSelection: function () {
        FilterActions.addToExcludeSelection({
            id: this.state.data.organisationid,
            name: this.state.data.use_organisation_name
        });
    },
    _sendViaEmail: function () {
        FilterActions.sendOrganisationViaEmail(this.state.data.id);
    },
    _createNote: function () {
        FilterActions.openNotes(this.state.data.organisationid);
    },
    _showHistory: function () {
        FilterActions.setPreviousHistory({
            currentSession: this.state.data.id,
            ipAddress: this.state.data.binary_ip_address,
            organisationId: this.state.data.organisationid,
            organisationName: this.state.data.use_organisation_name
        });
    },
    _showIntelligence: function () {
        FilterActions.showAdditionalCompanyData(this.state.data.organisationid);
    },
    _addToExportSelection: function () {
        if (this.state.selectedForExport) {
            FilterActions.removeExportSelection(this.state.data.id);
        } else {
            FilterActions.addToExportSelection({
                sessionId: this.state.data.id,
                organisationName: this.state.data.use_organisation_name,
                sessionDateTime: this.state.data.session_start_date_time
            });
        }
    }
});

export default SessionResult;
