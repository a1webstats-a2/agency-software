<?php

namespace App\Http\Controllers\EndUsers;

use Illuminate\Http\Request;

class Feedback extends \App\Http\Controllers\Controller {

	public function index( Request $request ) {
		
		$questions = [];

		foreach( $request->input() as $key => $value ) {

			if( preg_match( '/question/', $key ) ) {

				$questions[str_replace( "question", "", $key )] = $value;
			}
		}

		$client 	= new \GuzzleHttp\Client();

		$response 	= $client->request( 'POST', env( 'API_SOURCE' ) . '/end-of-trial/feedback', [

				'form_params' => [

					'otherComments'	=>	$request->input( 'other' ),
					'questions'		=>	$questions,
					'clientID'		=>	$request->input( 'subscriberID' ),
				]
			]
		);

		$contents 	= $response->getBody()->getContents();

		return view( 'UI.endUsers.feedbackSuccess' );

		//print_r( $request->input() ); exit();
	}
}