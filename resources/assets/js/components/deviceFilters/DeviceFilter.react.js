import React from 'react';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

import Checkbox from 'material-ui/Checkbox';

const styles = {

  	checkbox: {
    	marginBottom: 16
  	}
};


function getCurrentState( deviceProps ) {

	return {

		isChecked 	: 	deviceProps.checked,
		device 		: 	deviceProps.device,
		id 			: 	deviceProps.myId
	}
}

var DeviceFilter 	= React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.isChecked !== nextProps.isChecked ) {

			return false;
		}

		return true;
	},

	render : function() {

		var deviceName = ( typeof this.state.device !== "undefined" ) ? this.state.device.capitalizeFirstLetter() : '';

		return(
			<div className="col-md-4">
				

				<Checkbox
					label={deviceName}
					style={styles.checkbox}
					checked={this.state.isChecked}
					onCheck={this._saveCheckedState}
				/>
			</div>
		);
	},

	_saveCheckedState : function( event ) {

		var newState  = {

   			type          	:   "deviceFilter", 
   			id 				: 	this.state.id,
   			storedValue 	:   {

				id 		: 	this.state.device,
				name 	: 	this.state.device,
				value 	: 	this.state.id
			},
			overrideKey 	: "device" + this.state.device, 

   		}

   		var checked = event.target.checked;

   		if( checked ) {

   			FilterActions.createFilter( newState );

   		} else {

   			FilterActions.removeFilterByType({

   				type 	: 	'deviceFilter',
   				id 		: 	this.state.id
   			});
   		}
	}
})

export default  DeviceFilter;