<?php 

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoCardless extends Controller {

	public function show(Request $request, $redirectID ) {
		return view(
			'UI.payment.gocardless.new-mandate', []
		);
	}
}