import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FilterActions from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {

		settings 	: 	props.settings
	}
}

var BillingSettings = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		var sendInvoicesTo  = ( this.state.settings.send_invoices_to ) ? this.state.settings.send_invoices_to : '';
		var vatNumber  		= ( this.state.settings.vat_number ) ? this.state.settings.vat_number : '';


		return (

			<div>

				<div className="row">
						
					<div className="col-md-12">
						
						<h3>General Settings</h3><br />
					</div>

				</div>

				<div className="row">

					<div className="col-md-8">
						<TextField
							fullWidth={true}
							value={sendInvoicesTo}
							hintText="Send Invoices To (accounts@yourcompany.com)"
							onChange={this._updateInvoiceTo}
						/><br />

						<TextField
							fullWidth={true}
							hintText="Billing Address 1"
							value={this.state.settings.addr1}
							onChange={ ( event, newVal ) => this._editTeamSettings( newVal, "addr1" ) }
						/><br />

						<TextField
							fullWidth={true}
							hintText="Billing Address 2"
							value={this.state.settings.addr2}
							onChange={ ( event, newVal ) => this._editTeamSettings( newVal, "addr2" ) }

						/><br />

						<TextField
							fullWidth={true}
							hintText="Billing City"
							value={this.state.settings.town_or_city}
							onChange={ ( event, newVal ) => this._editTeamSettings( newVal, "town_or_city" ) }

						/><br />

						<TextField
							fullWidth={true}
							hintText="Billing County"
							value={this.state.settings.county}
							onChange={ ( event, newVal ) => this._editTeamSettings( newVal, "county" ) }

						/><br />

						<TextField
							fullWidth={true}
							hintText="Billing Postcode"
							value={this.state.settings.postcode}
							onChange={ ( event, newVal ) => this._editTeamSettings( newVal, "postcode" ) }

						/><br />

						<TextField
							fullWidth={true}
							value={vatNumber}
							hintText="VAT Number"
							onChange={this._updateVATNumber}

						/><br />

						<div className="clr"></div><br />

					    <RaisedButton onClick={this._saveTeamSettings} label="Update" primary={true} style={{ marginTop : 20 }} />

					</div>
				</div>
			</div>

		)
	},

	_editTeamSettings : function( newVal, field ) {

		let settings 	= this.state.settings;
		settings[field] = newVal;
		FilterActions.editTeamSettingsInStoreOnly( settings );
	},

	_saveTeamSettings : function() {

		FilterActions.saveTeamSettings();
	},

	_updateVATNumber : function( event, newVal ) {

		FilterActions.updateVATNumber( newVal );
	},

	_updateInvoiceTo : function( event, newVal ) {

		FilterActions.updateInvoiceTo( newVal );
	}
});

export default BillingSettings;