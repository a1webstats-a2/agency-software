import React 				from 'react';
import OrganisationFilters 	from './OrganisationFilters.react';
import SearchBy 			from './customFilters/SearchBy.react';
import FiltersStore 		from '../stores/FiltersStore';
import FilterActions 		from '../actions/FilterActions';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

import Toggle from 'material-ui/Toggle';
const styles = {

  	block: {
    	maxWidth: 250,
  	},
  	toggle: {
    	marginBottom: 16,
  	},
  	radioButton : {

  		marginTop  		: 10,
  		marginBottom 	: 10
  	}
};

function getCurrentState( props ) {

	return {

		organisationsData 	: 	props.organisationsData,
		searchByData 		: 	props.searchByData
	}
}

var OrganisationFiltersContainer = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		var radioIncludeValue = "include";

		return (

			<div className="organisationFiltersContainer">
				
				<div className="row">
					<div className="col-md-12">
						<br /><h3>Organisation Filters</h3>
					</div>
				</div>

				<div className="clr"></div>

				<div className="row">

					<div className="col-md-6">
						

					    <RadioButtonGroup onChange={this._saveIncludeExcludeOption} name="includeExclude" defaultSelected={radioIncludeValue}>
							<RadioButton
								value="include"
								label="Include"
								style={styles.radioButton}
							/>
							<RadioButton
								value="exclude"
								label="Exclude"
								style={styles.radioButton}
							/>
								
						</RadioButtonGroup>
					</div>
				</div>

				<div className="clr"></div>

				<SearchBy searchByData={this.state.searchByData} type="organisationFilter" />

				<div className="clr"></div>

				

				<OrganisationFilters searchByData={this.state.searchByData} organisationsData={this.state.organisationsData} />


			</div>
		);
	
	},

	_saveIncludeExcludeOption : 	function( event, value ) {

		FilterActions.setOrganisationInclusionType( value );
	}
});

export default  OrganisationFiltersContainer;