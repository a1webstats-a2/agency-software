@extends( 'templates.signup' )
@section( 'content' )
    
    <div class="row">
        <div  class="col-md-6 col-md-offset-3">

            <div id="loginForm">

                <div id="a1strap">
                    <h1>{{ $agentData['name'] }}</h1>
                </div>

                <br /><br />

                <div>

                    <form method="POST">

                        <input type="hidden" name="userID" value="{{ $id }}" />

                        <input type="hidden" name="validationString" value="{{ $validationString }}" />
                        <div class="form-group">
                            <label for="newPassword">New Password (minimum 12 characters) <span>*</span></label>
                            <input type="password" name="newPassword" class="form-control" id="newPassword" placeholder="New Password" />
                        </div>

                        <br />
                        <div class="form-group">
                            <label for="newPasswordRepeat">Repeat New Password <span>*</span></label>
                            <input type="password" name="newPasswordRepeat" class="form-control" id="newPasswordRepeat" placeholder="Repeat New Password">
                        </div>

                        <br />
                        
                        <button type="submit" alt="login" id="submitButton"></button>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection