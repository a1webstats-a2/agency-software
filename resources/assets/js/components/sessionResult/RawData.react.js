import React from 'react';
function getCurrentState( props ) {

	return {

		data 	: 	props.data
	}
}

const styles = {

    standardTable   :   {

        td  :   {

            width   :   '40%'
        }
    }

};

var RawData = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.data.string_ip !== nextProps.data.string_ip ) {

			return true;
		}

		if( this.props.data.hostname !== nextProps.data.hostname ) {

			return true;
		}

		return false;
	},

	render : function() {

		const editedByClient = ( this.state.data.client_edited === 1 ) ? "Yes" : "No";

		return (

			<div>	
				<table className="table">
					<tbody>
						<tr>
							<td style={styles.standardTable.td}>
								<strong>Visit ID</strong>
							</td>
							<td>
								{this.state.data.id}
							</td>
						</tr>
						<tr>
							<td style={styles.standardTable.td}>
								<strong>Organisation ID</strong>
							</td>
							<td>
								{this.state.data.organisationid}
							</td>
						</tr>
						<tr>
		                    <td style={styles.standardTable.td}>
		                        <strong>IP Address:</strong>
		                    </td>

		                    <td>{this.state.data.string_ip}</td>
		                </tr>
		                <tr>
		                    <td style={styles.standardTable.td}>
		                        <strong>Hostname:</strong>
		                    </td>

		                    <td>{this.state.data.hostname}</td>
		                </tr>
		                <tr>
		                	<td style={styles.standardTable.td}>
		                        <strong>Edited by Client:</strong>
		                    </td>
		                	<td>{editedByClient}</td>
		                </tr>
		                <tr>
		                	<td style={styles.standardTable.td}>
		                		<strong>Type</strong>
		                	</td>
		                	<td>
		                		{this.state.data.lo}-{this.state.data.organisation_type_id}
		                	</td>

		                </tr>
		            </tbody>
		        </table>
			</div>
		)
	}
});

export default  RawData;