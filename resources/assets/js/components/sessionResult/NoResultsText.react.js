import React 						from 'react';
import RaisedButton 				from 'material-ui/RaisedButton';
import TextField 					from 'material-ui/TextField';
import FilterActions 				from '../../actions/FilterActions';

function getCurrentState( props ) {

	return props;
}

class NoResultsText extends React.Component {

	constructor( props ) {

		super( props );

		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( newProps ) {

		this.setState( getCurrentState( newProps ) );
	}

	render() {

		let reportText = "";

		if( this.state.resultsReportHasBeenSent ) {

			reportText = (

				<p><strong>Thank you, we will investigate what went wrong with this report.</strong></p>
			)

		} else if( this.state.reportNoResultsBoxIsOpen ) {

			reportText = (

				<div>
					<TextField onChange={ ( event, newText ) => this._setNoResultsReportMsg( newText ) } fullWidth={true} hintText="Add a message" /> 

					<div className="clr"></div><br />

					<RaisedButton onTouchTap={() => this._sendNoResultsReport() } primary={true} label="Please investigate" />
				</div>
			)
		}

		return (

			<div className="row">
				<div className="alert alert-danger" id="resultsUpdatingMessage">
					<p>Sorry, there are <strong>no results</strong> matching your search criteria.&nbsp; 
						<a href="#" onClick={( event ) => this._showReportNoResultsBox( event ) }>Expected to see results?</a></p>

					<div>
						<div className="clr"></div><br />

						{reportText}
					</div>
				</div>
			</div>

		)
	}

	_setNoResultsReportMsg( newText ) {

		FilterActions.setNoResultsReportMsg( newText );
	}

	_sendNoResultsReport() {

		FilterActions.reportNoResults();
	}

	_showReportNoResultsBox( event ) {

		event.preventDefault();
		FilterActions.showReportNoResultsBox();
	}
}

export default NoResultsText;