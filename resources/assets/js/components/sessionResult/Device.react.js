import React         from 'react';


import ChromePNG     from '../png/ChromePNG.react';
import FirefoxPNG    from '../png/FirefoxPNG.react';
import OperaPNG      from '../png/OperaPNG.react';
import SafariPNG     from '../png/SafariPNG.react';

import DesktopPNG    from '../png/DesktopPNG.react';
import TabletPNG     from '../png/TabletPNG.react';
import MobilePNG     from '../png/MobilePNG.react';
import MacPNG        from '../png/MacPNG.react';

import WindowsPNG    from '../png/WindowsPNG.react';
import LinuxPNG      from '../png/LinuxPNG.react';
import AndroidPNG    from '../png/AndroidPNG.react';
import IOSPNG        from '../png/IOSPNG.react';
import MacosPNG      from '../png/MacOSPNG.react';

function findReadableOperatingSystem( fullOperatingSystemJargon ) {

    var returnName = "Unknown";

    if( !fullOperatingSystemJargon ) {

        return returnName;
    }

    var knownVals = {

        'Win'                   :  'windows',
        'Win16'                 :  'windows',
        'Windows 95'            :  'windows',
        'Windows 98'            :  'windows',
        'Windows NT 5.0'        :  'windows',
        'Windows NT 5.1'        :  'windows',
        'Windows NT 5.2'        :  'windows',
        'Windows NT 6.0'        :  'windows',
        'Windows NT 6.1'        :  'windows',
        'Windows NT 6.2'        :  'windows',
        'Windows NT 4.0'        :  'windows',
        'Windows ME'            :  'windows',
        'OpenBSD'               :  'open bsd',
        'SunOS'                 :  'sun os',
        'Linux'                 :  'linux',
        'Mac_PowerPC'           :  'windows',
        'QNX'                   :  'qnx',
        'BeOS'                  :  'be os',
        'OS/2'                  :  'os 2', 
        '5.0 (Windows NT 10.0'  :  'windows',
        'iPhone OS'             :  'ios',
        'Mac OS'                :  'mac os',
        'Android'               :  'android'
    }

    var knownIndexes = [

        'Android',
        'Win',
        'Win16',
        'Windows 95',
        'Windows 98',
        'Windows NT 5.0',
        'Windows NT 5.1',
        'Windows NT 5.2',
        'Windows NT 6.0',
        'Windows NT 6.1',
        'Windows NT 6.2',
        'Windows NT 4.0',
        'Windows ME',
        'OpenBSD',
        'SunOS',
        'Linux',
        'Mac_PowerPC',
        'QNX',
        'BeOS',
        'OS/2',
        '5.0 (Windows NT 10.0',
        'iPhone OS',
        'Mac OS'
    ]

    knownIndexes.forEach( function( name, key ) {

        if( fullOperatingSystemJargon.includes( name ) ) {

            if( typeof knownVals[name] !== "undefined" ) {
            
                returnName = knownVals[name];
            }
        }
    });

    return returnName;
}
 
var Device =   React.createClass({

    shouldComponentUpdate : function( nextProps, nextState ) {

        if( this.props.data.browser_parent_name !== nextProps.data.browser_parent_name ) {

            return true;
        }

        if( this.props.data.device_parent_name !== nextProps.data.device_parent_name ) {

            return true;
        }

        if( this.props.data.operating_system_parent_name !== nextProps.data.operating_system_name ) {

            return true;
        }

        return false;
    },

    render  :   function() {

        var browserImg          =   "";

        var deviceParentType    =   "desktop";

        var osImg               =   "";

        if( this.props.data.browser_parent_name ) {

            switch( this.props.data.browser_parent_name.toLowerCase() ) {

                case "chrome" :

                    browserImg  =   <ChromePNG />;

                    break;

                case "firefox" :

                    browserImg  =   <FirefoxPNG />;

                    break;

                case "safari" :

                    browserImg  =   <SafariPNG />;

                    break;

                case "opera" :

                    browserImg  =   <OperaPNG />;

                    break;
            }
        }

        if( this.props.data.device_parent_name ) {

            switch( this.props.data.device_parent_name.toLowerCase() ) {

                case "desktop" :

                    deviceParentType =  <DesktopPNG />;

                    break;

                case "mac" :

                    deviceParentType =  <MacPNG />;

                    break;


                case "tablet" :

                    deviceParentType =  <TabletPNG />;

                    break;

                case "mobile" :

                    deviceParentType =  <MobilePNG />;

                    break;
            }
        }

        var operatingSystem = findReadableOperatingSystem( this.props.data.operating_system_name );

        if( this.props.data.operating_system_name ) {

            switch( operatingSystem.toLowerCase() ) {

                case "mac os" :
               
                    osImg = <MacosPNG />

                    break;

                case "ios" :

                    osImg = <IOSPNG />

                    break;

                case "windows" :

                    osImg = <WindowsPNG />

                    break;

                case "linux" :

                    osImg = <LinuxPNG />

                    break;
            }

        } 

        return (

            <table className="table">
                <tbody>

                    <tr>
                        <td><strong>Monitor Resolution</strong></td>
                        <td>{this.props.data.monitor_resolution}</td>
                    </tr>

                    <tr>
                        <td><strong>Browser</strong></td>
                        <td>{this.props.data.browser_parent_name}</td>
                    </tr>

                    <tr>
                        <td><strong>Operating System</strong></td>
                        <td>{operatingSystem}</td>
                    </tr>

                    <tr>
                        <td><strong>Device Information</strong></td>
                        <td className="svgImages">{deviceParentType} {osImg} {browserImg} </td>
                    </tr>
                </tbody>
            </table>
        )
    }

});

export default  Device;