import React from 'react';
import DataList from '../DataTables/DataList.react';
import Paper from 'material-ui/Paper';
import DateCell from './DateCell.react';
import moment from 'moment';
import CustomTextCell from './CustomTextCell.react';
import VisitDuration from './VisitDuration.react';
import OrganisationHoverCell from './OrganisationHoverCell.react';
import ActionsCell from './ActionsCell.react';
import Device from './Device.react';
import SelectCell from './SelectCell.react';
import FiltersStore from '../../stores/FiltersStore';
import FilterActions from '../../actions/FilterActions';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import PhoneIcon from 'material-ui/svg-icons/communication/phone';
import WebsiteIcon from 'material-ui/svg-icons/hardware/computer';
import UserTipsStore from '../../stores/UserTipsStore';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import HistoryIcon from 'material-ui/svg-icons/action/history';
import IntelligenceIcon from 'material-ui/svg-icons/action/visibility';
import TagIcon from 'material-ui/svg-icons/action/label-outline';
import InfoIcon from 'material-ui/svg-icons/action/info-outline';
import Comment from 'material-ui/svg-icons/communication/comment';
import EditRecordIcon from 'material-ui/svg-icons/content/create';
import ReactImageFallback from "react-image-fallback";
import IconButton from 'material-ui/IconButton';
import UserTipActions from '../../actions/UserTipActions';
import A1Flag from './A1Flag.react';
import NextIcon from 'material-ui/svg-icons/AV/skip-next';
import PrevIcon from 'material-ui/svg-icons/AV/skip-previous';
import Lang from '../../classes/Lang';
import normalizeUrl from 'normalize-url';

"use strict";

import FixedDataTable from 'fixed-data-table';

const {Table, Column, Cell} = FixedDataTable;

class DataListWrapper {
    constructor(indexMap, data) {
        this._indexMap = indexMap;
        this._data = data;
        this.data = data.data;
    }
    getSize() {
        return this._indexMap.length;
    }
    getCollection() {
        return this._data;
    }
    getObjectAt(index) {
        return this._data.getObjectAt(
            this._indexMap[index],
        );
    }
}

const SortTypes = {
    ASC: 'ASC',
    DESC: 'DESC',
};

function reverseSortDirection(sortDir) {
    return sortDir === SortTypes.DESC ? SortTypes.ASC : SortTypes.DESC;
}

const iconStyle = {
    height: 15, width: 15, color: '#170550'
}

class SortHeaderCell extends React.Component {
    constructor(props) {
        super(props);

        this._onSortChange = this._onSortChange.bind(this);
    }

    render() {
        var {onSortChange, sortDir, children, ...props} = this.props;

        return (
            <Cell {...props}>
                <a onClick={this._onSortChange}>
                    {children} {sortDir ? (sortDir === SortTypes.DESC ? '↓' : '↑') : ''}
                </a>
            </Cell>
        );
    }
    _onSortChange(e) {
        e.preventDefault();

        if (this.props.onSortChange) {

            let newSortDir = (this.props.sortDir === "DESC") ? "ASC" : "DESC";

            this.props.onSortChange(
                this.props.columnKey,
                newSortDir
            );
        }
    }
}

const styles = {
    paper: {
        padding: 20,
        height: 650
    },
    button: {
        fontSize: '10px'
    },
    svg: {
        color: '#170550'
    }
}

function getSearchResults(searchString, dataList) {
    var filterBy = searchString.toLowerCase();
    var size = dataList.getSize();
    var filteredIndexes = [];

    for (var index = 0; index < size; index++) {

        let company = dataList.getObjectAt(index);

        if (
            company.name.toLowerCase().includes(filterBy.toLowerCase())

        ) {
            filteredIndexes.push(index);
        }
    }

    return new DataListWrapper(filteredIndexes, dataList)
}

class OrganisationsDatatable extends React.Component {
    constructor(props) {
        super(props);

        this._dataList = new DataList(props.companies);

        this._defaultSortIndexes = [];
        let size = this._dataList.getSize();

        for (var index = 0; index < size; index++) {

            this._defaultSortIndexes.push(index);
        }

        let width = props.width;

        let quickExportSettings = FiltersStore.getQuickExportSettings();

        this.state = {
            colSortDirs: {},
            tags: props.tags,
            allowNextOrganisation: false,
            allowPrevOrganisation: false,
            nextOrganisationRowIndex: 0,
            prevOrganisationRowIndex: 0,
            currentRowIndex: FiltersStore.getCurrentOrganisationRowIndex(),
            selection: quickExportSettings.selection,
            width: width,
            exclude: FiltersStore.getExcludeSelection(),
            filteredDataList: this._dataList,
            sortedDataList: this._dataList,
            searchString: "",
            tracked: FiltersStore.getTrackedOrganisations(),
            showOrganisationInLightboxOpen: FiltersStore.checkLightboxOpen('showOrganisationInLightbox', -1),
            organisationForLightbox: FiltersStore.getOrganisationToShowInLightbox(),
            flatPageVisits: props.flatPageVisits,
            type: props.type,
            relationships: props.relationships,
            tip: UserTipsStore.getTip()
        };

        this._onSortChange = this._onSortChange.bind(this);
        this._onFilterChange = this._onFilterChange.bind(this);
    }

    componentWillReceiveProps(newProps) {

        this._dataList = new DataList(newProps.companies);
        this._defaultSortIndexes = [];

        let size = this._dataList.getSize();

        for (var index = 0; index < size; index++) {

            this._defaultSortIndexes.push(index);
        }

        let sortIndexes = this._defaultSortIndexes.slice();
        let quickExportSettings = FiltersStore.getQuickExportSettings();

        let sortedDataList = new DataListWrapper(sortIndexes, this._dataList);

        if (this.state.searchString !== "") {
            sortedDataList = getSearchResults(this.state.searchString, this._dataList);
        } else {
            sortedDataList = new DataListWrapper(sortIndexes, this._dataList);
        }

        const currentRowIndex = FiltersStore.getCurrentOrganisationRowIndex();

        let allowNextOrganisation = false;
        let allowPrevOrganisation = false;


        if (typeof sortedDataList.data[(currentRowIndex + 1)] !== "undefined") {
            allowNextOrganisation = true;
        }

        if (typeof sortedDataList.data[(currentRowIndex - 1)] !== "undefined") {
            allowPrevOrganisation = true;
        }

        this.setState({
            tags: newProps.tags,
            currentRowIndex: currentRowIndex,
            allowNextOrganisation: allowNextOrganisation,
            allowPrevOrganisation: allowPrevOrganisation,
            nextOrganisationRowIndex: (currentRowIndex + 1),
            prevOrganisationRowIndex: (currentRowIndex - 1),
            type: newProps.type,
            exclude: FiltersStore.getExcludeSelection(),
            selection: quickExportSettings.selection,
            width: newProps.width,
            filteredDataList: sortedDataList,
            sortedDataList: sortedDataList,
            flatPageVisits: newProps.flatPageVisits,
            tracked: FiltersStore.getTrackedOrganisations(),
            showOrganisationInLightboxOpen: FiltersStore.checkLightboxOpen('showOrganisationInLightbox', -1),
            organisationForLightbox: FiltersStore.getOrganisationToShowInLightbox(),
            relationships: newProps.relationships,
            tip: UserTipsStore.getTip()
        });
    }

    componentDidMount() {
        if (document.getElementById("recentCompaniesDiv")) {
            this.setState({
                width: (document.getElementById("recentCompaniesDiv").offsetWidth - 70)
            });
        }
    }
    _onSortChange(columnKey, sortDir) {
        var sortIndexes = this._defaultSortIndexes.slice();

        let sortVal = 0;

        if (columnKey === "created_at") {
            sortIndexes.sort((indexA, indexB) => {
                let valueA = this._dataList.getObjectAt(indexA)[columnKey];
                let valueB = this._dataList.getObjectAt(indexB)[columnKey];

                const datea = moment(valueA);
                const dateb = moment(valueB);

                if (datea.isAfter(dateb)) {
                    sortVal = 1;
                }

                if (datea.isBefore(dateb)) {
                    sortVal = -1;
                }

                if (sortVal !== 0 && sortDir === SortTypes.ASC) {
                    sortVal = sortVal * -1;
                }

                return sortVal;
            });
        } else {
            sortIndexes.sort((indexA, indexB) => {
                var valueA = this._dataList.getObjectAt(indexA)[columnKey];
                var valueB = this._dataList.getObjectAt(indexB)[columnKey];

                if (valueA > valueB) {
                    sortVal = 1;
                }
                if (valueA < valueB) {
                    sortVal = -1;
                }
                if (sortVal !== 0 && sortDir === SortTypes.ASC) {
                    sortVal = sortVal * -1;
                }

                return sortVal;
            });
        }

        let sortedDataList = new DataListWrapper(sortIndexes, this._dataList);

        this.setState({
            sortedDataList: sortedDataList,
            colSortDirs: {
                [columnKey]: sortDir,
            },
        });
    }

    _onFilterChange(e) {
        if (!e.target.value) {
            this.setState({
                sortedDataList: this._dataList
            });
        }

        let searchResults = getSearchResults(e.target.value, this._dataList);

        this.setState({
            searchString: e.target.value,
            sortedDataList: searchResults,
        });
    }

    render() {
        var {sortedDataList, colSortDirs} = this.state;
        let table = (
            <Table
                rowHeight={30}
                rowsCount={sortedDataList.getSize()}
                headerHeight={30}
                width={1100}
                height={400}
                {...this.props}>

                <Column
                    columnKey="created_at"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.created_at}>
                            Visit Date
                        </SortHeaderCell>
                    }
                    cell={<DateCell data={sortedDataList} col="created_at"/>}
                    width={100}
                />
                <Column
                    columnKey="name"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.name}>
                            {Lang.getWordUCFirst("organisation")}
                        </SortHeaderCell>
                    }
                    cell={<OrganisationHoverCell data={sortedDataList} col="name"/>}
                    width={200}
                />

                <Column
                    columnKey="country_flat"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.country_flat}>

                        </SortHeaderCell>
                    }
                    cell={<A1Flag data={sortedDataList} col="country_flat"/>}
                    width={40}
                />

                <Column
                    columnKey="keywords_flat"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.id}>
                            Keywords
                        </SortHeaderCell>
                    }
                    cell={<CustomTextCell data={sortedDataList} col="keywords_flat"/>}
                    width={100}
                />

                <Column
                    columnKey="number_of_visits_in_date_range"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.number_of_visits_in_date_range}>
                            Visits
                        </SortHeaderCell>
                    }
                    cell={<CustomTextCell data={sortedDataList} col="number_of_visits_in_date_range"/>}
                    width={50}
                />
                <Column
                    columnKey="number_of_page_visits"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.number_of_page_visits}>
                            Pages
                        </SortHeaderCell>
                    }
                    cell={<CustomTextCell data={sortedDataList} col="number_of_page_visits"/>}
                    width={50}
                />
                <Column
                    columnKey="complete_session_duration"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.complete_session_duration}>
                            Duration
                        </SortHeaderCell>
                    }
                    cell={<VisitDuration data={sortedDataList} col="complete_session_duration"/>}
                    width={100}
                />
                <Column
                    columnKey="browser_flat"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.browser_flat}>
                            Device
                        </SortHeaderCell>
                    }
                    cell={<Device data={sortedDataList} col="browser_flat"/>}
                    width={80}
                />
                <Column
                    columnKey="last_visit_by_organisation"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.last_visit_by_organisation}>
                            Prev. Visit
                        </SortHeaderCell>
                    }
                    cell={<DateCell data={sortedDataList} col="last_visit_by_organisation"/>}
                    width={80}
                />
                <Column
                    columnKey="id"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.id}>
                            Actions
                        </SortHeaderCell>
                    }
                    cell={<ActionsCell data={sortedDataList} tags={this.state.tags}
                                       relationships={this.state.relationships} col="id"/>}
                    width={140}
                />
                <Column
                    columnKey="id"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.id}>
                            Select
                        </SortHeaderCell>
                    }
                    cell={<SelectCell type="select" selection={this.state.selection} data={sortedDataList} col="id"/>}
                    width={50}
                />
                <Column
                    columnKey="id"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.id}>
                            Exclude
                        </SortHeaderCell>
                    }
                    cell={<SelectCell type="exclude" selection={this.state.exclude} data={sortedDataList} col="id"/>}
                    width={50}
                />
                <Column
                    columnKey="id"
                    header={
                        <SortHeaderCell
                            onSortChange={this._onSortChange}
                            sortDir={colSortDirs.id}>
                            Track
                        </SortHeaderCell>
                    }
                    cell={<SelectCell type="track" selection={this.state.tracked} data={sortedDataList} col="id"/>}
                    width={70}
                />
            </Table>
        );

        let lightboxOrganisationTitle, lightboxOrganisationContent, showTitle = '';
        let historyButton = false;

        if (this.state.organisationForLightbox) {
            if (this.state.organisationForLightbox.multi_visitor === 1) {
                historyButton = (
                    <FlatButton onClick={() => this._showHistory(this.state.organisationForLightbox.organisation_id,
                        this.state.organisationForLightbox.name)} label="History"
                                icon={<HistoryIcon color={styles.svg.color}/>}/>
                );
            }

            lightboxOrganisationTitle = this.state.organisationForLightbox.name;

            let website = this.state.organisationForLightbox.website
                ? normalizeUrl(this.state.organisationForLightbox.website)
                :'';

            const logoURL = "//logo.clearbit.com/" + website + "?size=200";
            let linkedInLink;
            const linkedInHref = "https://www.linkedin.com/search/results/companies/?keywords=" + lightboxOrganisationTitle + "&origin=SWITCH_SEARCH_VERTICAL";

            linkedInLink = (
                <a target="_blank" href={linkedInHref}>
                    <img className="mt-2 ml-10" src="/images/linkedin.png"/>
                </a>
            )

            let prevButton = '';
            let nextButton = '';

            if (this.state.allowPrevOrganisation) {
                prevButton = (
                    <FlatButton
                        onTouchTap={() => this._showOrganisationAtRow(this.state.prevOrganisationRowIndex)}
                        label="Prev"
                        icon={<PrevIcon/>}
                        color={styles.svg.color}/>
                )
            }

            if (this.state.allowNextOrganisation) {
                nextButton = (
                    <FlatButton
                        onTouchTap={() => this._showOrganisationAtRow(this.state.nextOrganisationRowIndex)}
                        label="Next"
                        icon={<NextIcon/>}
                        color={styles.svg.color}
                    />
                )
            }

            showTitle = (
                <div className="flex justify-between">
                    <div className="mt-2 flex">
                        {lightboxOrganisationTitle}
                        {linkedInLink}
                    </div>
                    <div>
                        {prevButton}
                        {nextButton}
                    </div>
                </div>
            )

            let displayPageVisits = "";

            if (this.state.flatPageVisits) {
                let pagesVisitedJSON = this.state.flatPageVisits;
                let pageVisitRows = pagesVisitedJSON.map((pageVisit, p) => {
                    let pageVisitDuration = moment("2015-01-01").startOf('day')
                        .seconds(pageVisit.duration_seconds)
                        .format('H:mm:ss');

                    return (
                        <tr key={p}>
                            <td>
                                <p>{pageVisit.url}</p>
                                <p>{pageVisit.query_string}</p>
                            </td>
                            <td style={{textAlign: 'right'}}>
                                <p>{pageVisitDuration}</p>
                            </td>
                        </tr>
                    )
                });

                displayPageVisits = (
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>URL</th>
                            <th style={{textAlign: 'right'}}>Duration</th>
                        </tr>
                        </thead>
                        <tbody>
                        {pageVisitRows}
                        </tbody>
                    </table>
                )
            }

            let data = this.state.organisationForLightbox;

            let browserIcon = "";

            let osIcon = "";

            let deviceIcon = "";

            if (data.browser_flat !== "undefined" &&
                data.browser_flat) {

                switch (data.browser_flat.toLowerCase()) {
                    case "firefox" :
                        browserIcon = (
                            <img src="/images/mozilla-md.png"/>
                        )

                        break;
                    case "chrome" :
                        browserIcon = (
                            <img src="/images/chrome-md.png"/>
                        )

                        break;
                    case "internet explorer" :
                        browserIcon = (
                            <img src="/images/internet-explorer-md.png"/>
                        )

                        break;
                    case "safari" :
                        browserIcon = (
                            <img src="/images/safari-md.png"/>
                        )

                        break;
                }
            }

            if (data.os_flat !== "undefined" &&
                data.os_flat) {

                switch (data.os_flat.toLowerCase()) {
                    case "win" :
                        osIcon = (
                            <img src="/images/windows-md.png"/>
                        )

                        break;
                    case "wow" :
                        osIcon = (
                            <img src="/images/windows-md.png"/>
                        )

                        break;
                    case "linux" :
                        osIcon = (
                            <img src="/images/android-md.png"/>
                        )
                        break;
                    case "macintosh" :
                        osIcon = (
                            <img src="/images/macos-md.png"/>
                        )

                        break;
                }
            }

            if (data.device_flat !== "undefined" &&
                data.device_flat) {

                switch (data.device_flat.toLowerCase()) {
                    case "mac" :
                        deviceIcon = (
                            <img src="/images/imac-md.png"/>
                        )

                        break;

                    case "desktop" :
                        deviceIcon = (
                            <img src="/images/desktop-md.png"/>
                        )

                        break;
                    case "mobile" :
                        deviceIcon = (
                            <img src="/images/smartphone-md.png"/>
                        )

                        break;

                    case "tablet" :
                        deviceIcon = (
                            <img src="/images/tablet-md.png"/>
                        )

                        break;
                }
            }

            const visitDate = moment(this.state.organisationForLightbox.created_at);

            let lastVisit = "";

            if (this.state.organisationForLightbox.last_visit_by_organisation) {
                lastVisit = moment(this.state.organisationForLightbox.last_visit_by_organisation).format("Do MMMM YYYY");
            }

            let duration = "One Page Visit";

            if (this.state.organisationForLightbox.duration_seconds > 0) {
                duration = moment("2015-01-01").startOf('day')
                    .seconds(this.state.organisationForLightbox.duration_seconds)
                    .format('H:mm:ss');
            }

            let logoAlt = "Organisation logo for " + this.state.organisationForLightbox.name;

            lightboxOrganisationContent = (
                <div className="organisationLightbox">
                    <div className="row">
                        <div className="col-md-3">
                            <div className="clr"></div>
                            <br/>
                            <ReactImageFallback
                                src={logoURL}
                                fallbackImage="/images/no-image.png"
                                initialImage="/images/loading_large.gif"
                                alt={logoAlt}
                                className="my-image"/>

                            <div className="clr"></div>
                            <br/>
                        </div>

                        <div className="col-md-8 col-md-offset-1">
                            <div className="clr"></div>
                            <br/>

                            <div className="row">
                                <div style={{marginLeft: 15, float: 'left'}}>{deviceIcon}</div>
                                <div style={{marginLeft: 15, float: 'left'}}>{osIcon}</div>
                                <div style={{marginLeft: 15, float: 'left'}}>{browserIcon}</div>
                            </div>

                            <div className="clr"></div>
                            <br/>

                            <div className="row">
                                <div className="col-md-1">
                                    <PhoneIcon/>
                                </div>
                                <div className="col-md-10 col-md-offset-1">
                                    {this.state.organisationForLightbox.tel}
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-1">
                                    <WebsiteIcon/>
                                </div>
                                <div className="col-md-10 col-md-offset-1">
                                    <a href={website} target="_blank">
                                        {website}
                                    </a>
                                </div>
                            </div>

                            <div className="clr"></div>
                            <br/>

                            <table className="table" style={{marginLeft: -8}}>
                                <tbody>
                                <tr>
                                    <td style={{width: 85}}><strong>Visit Date</strong></td>
                                    <td>{visitDate.format("Do MMMM YYYY")}</td>
                                    <td><strong>Previous Visit</strong></td>
                                    <td>{lastVisit}</td>
                                </tr>
                                <tr>
                                    <td><strong>Referrer</strong></td>
                                    <td>{this.state.organisationForLightbox.referrer_flat}</td>
                                    <td><strong>Country</strong></td>
                                    <td>{this.state.organisationForLightbox.country_flat}</td>
                                </tr>
                                <tr>
                                    <td><strong>Keyword</strong></td>
                                    <td>{this.state.organisationForLightbox.keywords_flat}</td>
                                    <td><strong>Duration</strong></td>
                                    <td>{duration}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="clr"></div>
                    <br/>
                    <div className="col-md-12">
                        {displayPageVisits}
                    </div>
                </div>
            )
        }

        let actions = [
            <FlatButton
                onClick={() => this._openEditCompanyRecord(this.state.organisationForLightbox)}
                label="Edit"
                icon={<EditRecordIcon color="#170550"/>}/>,
            <FlatButton
                label="Notes"
                onClick={() => this._createNote(this.state.organisationForLightbox.organisation_id)}
                icon={<Comment color={styles.svg.color}/>}/>,
            <FlatButton
                onClick={() => this._showIntelligence(this.state.organisationForLightbox.organisation_id)}
                label="Intel"
                icon={<IntelligenceIcon color={styles.svg.color}/>}/>,
            <FlatButton
                onClick={() => this._sendViaEmail(this.state.organisationForLightbox.organisation_id)}
                icon={<EmailIcon color={styles.svg.color}/>}
                label="Email"/>,
            <FlatButton
                onClick={() => this._showTags(this.state.organisationForLightbox.organisation_id)}
                label="Tags"
                icon={<TagIcon color={styles.svg.color}/>}/>
        ];

        if (historyButton) {
            actions.push(historyButton);
        }

        actions.push(
            <FlatButton
                label="Close"
                primary={true}
                onTouchTap={this._handleClose}
            />
        )

        let helpLink = "";

        if (this.state.tip.id !== -1 && !this.state.tip.displayOnPageLoad) {
            helpLink = (
                <a href="#" onClick={(event) => this._showHelp(event)} style={{marginLeft: 20}}>
                    <img src="/images/help.png" alt="Help symbol" style={{marginRight: 10}}/>
                    Help
                </a>
            )
        }

        return (
            <Paper style={styles.paper} zDepth={4}>
                <div className="blueHeader" style={{height: 45}}>
                    <h4>{Lang.getWordUCFirst("organisations")}</h4>
                </div>

                <div className="row">
                    <div className="col-md-3">
                        <RaisedButton fullWidth={true} labelStyle={styles.button} labelPosition="before"
                                      labelColor="#fff" backgroundColor='#FFB347' style={styles.raisedButton}
                                      onClick={this._selectAll} label="Select All"/>
                    </div>
                    <div className="col-md-3">
                        <RaisedButton fullWidth={true}
                                      labelStyle={styles.button}
                                      labelPosition="before"
                                      labelColor="#fff"
                                      backgroundColor='#FFB347'
                                      style={styles.raisedButton}
                                      onClick={this._deselectAll}
                                      label="Deselect All"/>
                    </div>


                    <div className="col-md-3">
                        <RaisedButton fullWidth={true} labelStyle={styles.button} labelPosition="before"
                                      labelColor="#fff" backgroundColor='#53b68b' style={styles.raisedButton}
                                      onClick={() => this._startExport(this.state.sortedDataList)} label="Export"/>
                    </div>
                    <div className="col-md-3">
                        <RaisedButton fullWidth={true} labelStyle={styles.button} backgroundColor='#9cb8d9'
                                      labelColor="#fff" label="Show" style={styles.raisedButton}
                                      onClick={() => this._showData(this.state.sortedDataList)}/>
                    </div>

                </div>

                <div className="clr"></div>
                <br/>

                <table>
                    <tbody>
                    <tr>
                        <td>
                            <input onChange={this._onFilterChange} placeholder="Search" />
                        </td>
                        <td>
                            <IconButton
                                tooltip="Edit Record"
                                iconStyle={iconStyle}>
                                <EditRecordIcon/>
                            </IconButton>
                        </td>
                        <td>
                            Edit Record
                        </td>
                        <td>
                            <IconButton
                                tooltip="Send via Email"
                                iconStyle={iconStyle}>
                                <EmailIcon/>
                            </IconButton>
                        </td>
                        <td> Send via Email</td>
                        <td>
                            <IconButton
                                tooltip="Tag"
                                iconStyle={iconStyle}>
                                <TagIcon/>
                            </IconButton>
                        </td>
                        <td>
                            Tag
                        </td>
                        <td>
                            <IconButton
                                tooltip="View / Add Notes"
                                iconStyle={iconStyle}>
                                <Comment/>
                            </IconButton>
                        </td>
                        <td>View / Add Notes</td>
                        <td>
                            <IconButton
                                tooltip="View Additional Information"
                                iconStyle={iconStyle}>
                                <InfoIcon/>
                            </IconButton>
                        </td>
                        <td>
                            View Additional Information
                        </td>
                        <td>
                            {helpLink}
                        </td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                {table}
                <div className="clr"></div>
                <br/><br/><br/>

                <Dialog
                    title={showTitle}
                    actions={actions}
                    modal={false}
                    autoScrollBodyContent={true}
                    open={this.state.showOrganisationInLightboxOpen}
                    onRequestClose={this._handleClose}
                    contentStyle={{width: '70%'}}
                >
                    {lightboxOrganisationContent}
                </Dialog>
            </Paper>
        );
    }
    _showOrganisationAtRow(row) {
        FilterActions.showOrganisationInLightboxAndSetIndex({
            organisation: this.state.sortedDataList.getObjectAt(row),
            row: row
        });
    }
    _showHelp(event) {
        event.preventDefault();
        UserTipActions.showHelp();
    }
    _selectAll() {
        FilterActions.quickExportSelectAll("organisationFilter");
    }
    _deselectAll() {
        FilterActions.quickExportDeselectAll("organisationFilter");
    }
    _showData(selection) {
        let sendSelection = false;

        if (typeof selection !== "undefined" &&
            typeof selection._indexMap !== "undefined" &&
            typeof selection.data !== "undefined" &&
            selection._indexMap.length !== selection.data.length) {

            sendSelection = selection._indexMap;
        }

        FilterActions.quickExportShowDataNewOrganisations(sendSelection);
    }
    _startExport(selection) {
        let sendSelection = false;

        if (typeof selection !== "undefined" &&
            typeof selection._indexMap !== "undefined" &&
            typeof selection.data !== "undefined" &&
            selection._indexMap.length !== selection.data.length) {

            sendSelection = selection._indexMap;
        }

        FilterActions.startQuickExportNewOrganisationsFormat(sendSelection);
    }
    _showIntelligence(orgID) {
        FilterActions.showAdditionalCompanyData(parseInt(orgID));
    }
    _openEditCompanyRecord(org) {
        FilterActions.setActiveRecord({
            data: {
                use_organisation_linked_in_address: '',
                use_organisation_tel: org.tel,
                use_organisation_website: org.website,
                organisationid: parseInt(org.organisation_id),
                use_organisation_name: org.name
            },
            myKey: -1
        });
    }
    _openIntelligence() {
        FilterActions.showAdditionalCompanyData(orgID);
    }
    _createNote(orgID) {
        FilterActions.openNotes(orgID);
    }
    _showHistory(orgID, orgName) {
        FilterActions.setPreviousHistory({
            currentSession: 1,
            organisationId: orgID,
            organisationName: orgName
        });
    }
    _showTags(orgID) {
        let relationships = [];

        if (
            typeof this.state.relationships !== "undefined" &&
            typeof this.state.relationships[orgID] !== "undefined"
        ) {
            relationships = JSON.stringify(this.state.relationships[orgID]);
        }

        FilterActions.openClientTypesLightbox({
            organisation_id: orgID,
            relationships_json: relationships
        });
    }
    _sendViaEmail(orgID) {
        FilterActions.sendOrganisationViaEmail(orgID);
    }
    _handleClose() {
        FilterActions.closeLightbox();
    }
}

export default OrganisationsDatatable;
