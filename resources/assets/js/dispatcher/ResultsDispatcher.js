var React 		        		=   require( 'react' );
var ReactDOM 	        		=   require( 'react-dom' );
var Dispatcher 					=	require( './Dispatcher' );
var assign 						=	require( 'object-assign' );

var ResultsDispatcher 	=	assign( new Dispatcher(), {

	handleViewAction 	: 	function( action ) {

		this.dispatch({

			source 	: 	'VIEW_ACTION',
			action 	: 	action

		})
	}
});

module.exports 	= 	ResultsDispatcher; 
