import React from 'react';
import FilterActions from '../../actions/FilterActions';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import ViewExistingIcon from 'material-ui/svg-icons/action/build';

function getCurrentState( props ) {

	return {

		report 			: props.data,
		settings 		: props.settings,
		exportOptions 	: props.exportOptions
	}
}

const styles = {

	svgIcon 	: 	{

		marginRight : 20
	}
}

function parseDate( inputDate ) {

	if( typeof inputDate == "undefined" || !inputDate ) {

		return false;
	}

    var parts            =  inputDate.match( /(\d+)/g );

    var formattedDate    =  new Date( parts[0], parts[1]-1, parts[2], parts[3], parts[4], parts[5] );

    return new Date( formattedDate );
}

function ordinal_suffix_of( i ) {
    
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function twoDigits( d ) {

    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

var ExistingReportRow = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props !== nextProps ) {

			return true;
		}

		return false;
	},

	render : function() {

		var displayDate = "Unknown";

		if( typeof this.state.report.lastFired !== "undefined" &&
			this.state.report.lastFired &&
			typeof this.state.report.lastFired.created_at !== "undefined" 
		) {
			var monthNames      =   [  "January", "February", "March", "April", "May", "June",
        
	          "July", "August", "September", "October", "November", "December"
	        ];

	        displayDate     =   moment( this.state.report.lastFired.created_at  ).format( "Do MMMM YYYY" ); 

	        switch( this.state.settings.user.preferred_date_format ) {

	            case "dd/mm/yyyy" :

	                displayDate = moment( this.state.report.lastFired.created_at ).format( "DD/MM/YYYY" );

	                break;

	            case "mm/dd/yyyy" :

	                displayDate = moment( this.state.report.lastFired.created_at ).format( "MM/DD/YYYY" );

	                break;

	            case "yyyy-mm-dd" :

	                displayDate = moment( this.state.report.lastFired.created_at ).format( "YYYY-MM-DD" );
	                
	                break;
	        }
		}

		return (

			<tr>
				<td>{this.state.report.reportData.name}</td>
				<td>
					<ViewExistingIcon style={styles.svgIcon} color="#170550" onClick={this._viewEditReport} />
					<DeleteIcon style={styles.svgIcon} color="#170550" onClick={this._deleteReport} />
				</td>
				<td>
					{displayDate}
				</td>

			</tr>
		)
	},

	_deleteReport : function() {

		if( !confirm( "Are you sure you want to delete the template?" ) ) {

			return false;
		}

		FilterActions.removeExistingReport({

			reportId : this.state.report.reportData.id
		})

	},

	_viewEditReport : function() {
		
		FilterActions.setLightbox({

			type 	: 	'viewEditExistingReport',
			id 		: 	1
		});

		FilterActions.openViewEditAutomatedReport({

			reportId 	: this.state.report.reportData.id,
			report 		: this.state.report.reportData,
			users 		: this.state.report.users,
			types 		: this.state.report.types
		});
	}
});

export default  ExistingReportRow;
