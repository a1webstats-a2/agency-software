/**
 Copyright 2021, A1WebStats, All rights reserved.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.react';
import LargeCheckbox from './components/LargeCheckbox.react';

if (document.getElementById('content')) {
    ReactDOM.render(<App/>, document.getElementById('content'));
}
if (document.getElementById('termsAndConditionsCheckbox')) {
	if (document.getElementById('timezoneField').value === '--- select ---') {
		document.getElementById('timezoneField').value = Intl.DateTimeFormat().resolvedOptions().timeZone;
	}

	ReactDOM.render(
		<LargeCheckbox label='' name='termsAndConditions' />,
		document.getElementById('termsAndConditionsCheckbox')
	)
}
