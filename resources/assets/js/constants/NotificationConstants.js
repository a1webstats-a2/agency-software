var keyMirror = require( 'keyMirror' );

module.exports = keyMirror({

	OPEN_NOTIFICATIONS 								: 	null,
	CLOSE_NOTIFICATIONS 							: 	null,
	LOAD_NOTIFICATIONS			                    : 	null,
	ADD_NOTIFICATION 								: 	null,
	DISCARD_NOTIFICATIONS 							: 	null,
	LOAD_INITIAL_NOTIFICATIONS 						: 	null,
	STORE_NOTIFICATIONS 							: 	null,
	ARCHIVE_NOTIFICATION 							: 	null, 
	SET_NOTIFICATION_SELECTION 						: 	null,
	SET_NOTIFICATION_VIEW_TYPE 						: 	null,
	ARCHIVE_SELECTION 								: 	null,
	DELETE_SELECTION 								: 	null,
	VIEW_NOTIFICATION 								: 	null,
	DELETE_INDIVIDUAL 								: 	null,
	BACK_TO_ALL 									: 	null,
	DELETE_NOTIFICATION 							: 	null,
	UPDATE_NOTIFICATIONS 							: 	null
});