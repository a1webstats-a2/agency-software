import React 							from 	'react';
import Dialog 							from 	'material-ui/Dialog';
import FlatButton 						from 	'material-ui/FlatButton';
import RaisedButton 					from 	'material-ui/RaisedButton';
import FilterActions 					from 	'../../actions/FilterActions';
import {RadioButton, RadioButtonGroup} 	from 	'material-ui/RadioButton';
import {List, ListItem} 				from 	'material-ui/List';
import Avatar 							from 	'material-ui/Avatar';
import Checkbox 						from 	'material-ui/Checkbox';


function getCurrentState( props ) {

	return 	{ 
		
		selection 					: 	props.selection,
		settings 					: 	props.settings.team,
		bulkOrganisationExportType 	: 	props.bulkOrganisationExportType,
		open 						: 	props.open
	}
}

class ChooseExportType extends React.Component {
	
	constructor( props ) {
		
		super( props );

		this.state = getCurrentState( props );

	}

	componentWillReceiveProps( newProps ) {

		this.setState( getCurrentState( newProps ) );

	}

	render() {


		const actions = [
			
			<FlatButton
				label="Cancel"
				secondary={true}
				onTouchTap={this._close}
			/>,
			<FlatButton
				label="Export"
				primary={true}
				keyboardFocused={true}
				onTouchTap={() => this._export( this.state.bulkOrganisationExportType ) }
			/>
		];

		const styles = {
			
			block: {
				maxWidth: 250,
			},

			radioButton: {
				marginBottom: 16,
			}
		};

		let crmOptions = "";

		if( this.state.settings.zoho_auth_token !== "" ) {

			let zohoDisabled 	= false;
			let zohoLabel 		= "Zoho";
		
		} else {

			let zohoDisabled 	= true;
			let zohoLabel 		= "Zoho - no auth token set";
		}

		if( this.state.bulkOrganisationExportType === "crm" ) {

			crmOptions = (

				<div>

					<List style={{ width : 300 }}>
						<ListItem
							key={1}
							rightAvatar={<Avatar src="/images/crms/zoho-small.png" />}
							primaryText="Zoho"
					        insetChildren={true}
							leftCheckbox={<Checkbox checked={this.state.selection.zoho} onCheck={( event, isChecked ) => this._setZohoExport( this.state.selection, isChecked ) } value="zoho"  />} />

						<ListItem
							key={2}
							rightAvatar={<Avatar src="/images/crms/salesforce-small.png" />}
							primaryText="SalesForce (coming soon)"
					        insetChildren={true}
							leftCheckbox={<Checkbox disabled={true} value="zoho"  />} />

						<ListItem
							key={3}
							rightAvatar={<Avatar src="/images/crms/dynamics-small.png" />}
							primaryText="Dynamics (coming soon)"
							disabled={true}
					        insetChildren={true}
							leftCheckbox={<Checkbox disabled={true} value="zoho"  />} />
					</List>
				</div>

			)
		}

		return (

			<div>
				<Dialog
					title="Export to CRM"
					actions={actions}
					modal={false}
					open={this.state.open}
					onRequestClose={this._close}
				>

					<div className="clr"></div><br />

					<RadioButtonGroup 
						valueSelected={this.state.bulkOrganisationExportType}
						name="bulkOrganisationExportType" 
						onChange={this._setBulkOrganisationExportType}
					>
						
						<RadioButton
							value="csv"
							label="Excel ( CSV )"
							style={styles.radioButton}
						/>
						<RadioButton
							value="crm"
							label="CRM"
							style={styles.radioButton}
						/>
					</RadioButtonGroup>

					{crmOptions}

				</Dialog>

			</div>
		)
	}

	_setBulkOrganisationExportType( event, payload ) {

		FilterActions.setBulkOrganisationExportType( payload );
	}

	_close() {

		FilterActions.closeLightbox();
	}

	_export( exportType ) {

		if( exportType === "csv" ) {
		
			FilterActions.startQuickExport( "organisationFilter" );

		} else {

			FilterActions.startBulkCRMExport();
		}
	}

	_setZohoExport( selection, isChecked ) {

		var exportSelection 	= selection;
		exportSelection.zoho 	= isChecked;

		FilterActions.setCRMExportSelection( exportSelection );
	}
}

export default ChooseExportType;