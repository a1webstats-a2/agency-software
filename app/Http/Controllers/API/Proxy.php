<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Http\Response as LaravelResponse;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;

final class Proxy
{
    public function index(Request $request, $id = null): LaravelResponse
    {
        return $this->run($request, $id);
    }

    public function create(Request $request): LaravelResponse
    {
        return $this->run($request);
    }

    public function store(Request $request): LaravelResponse
    {
        return $this->run($request);
    }

    public function show(Request $request, $id = null): LaravelResponse
    {
        return $this->run($request, $id);
    }

    public function edit(Request $request): LaravelResponse
    {
        return $this->run($request);
    }

    public function update(Request $request, $id = null): LaravelResponse
    {
        return $this->run($request, $id);
    }

    public function destroy(Request $request, $id = null): LaravelResponse
    {
        return $this->run($request, $id);
    }

    public function run(Request $request, $id = null): LaravelResponse
    {
        $agentData = \getAgentConfigData();

        $clientArgs = [
            'base_uri' => env('API_SOURCE') . '/',
            'verify'   => false,
        ];

        if ($request->session()->get('regenerateCount') < 10) {
            session([
                'regenerateCount' => ($request->session()->get('regenerateCount') + 1),
            ]);
        } else {
            session(['regenerateCount' => 0]);

            $request->session()->regenerate();
        }

        $formParams = [];
        $query      = [
            'client_id'    => session('clientId'),
            'clientId'     => session('clientId'),
            'access_token' => session('token'),
            'user_id'      => session('userId'),
            'userEmail'    => session('user')['email'] ?? null,
            'ip_address'   => $_SERVER['REMOTE_ADDR'],
            'agentID'      => $agentData['agentID'],
            'agent'        => $agentData['name'],
            'admin_user'   => session('user')['admin_user'] ?? null,
        ];

        if ($id) {
            $query['object_id'] = $id;
        }

        switch ($request->method()) {
            case 'GET':

                break;
            case 'POST':
                $formParams = array_merge(
                    $query,
                    $request->input()
                );

                if ($request->hasFile('file')) {
                    $formParams = null;

                    $clientArgs['multipart'] = [
                        [
                            'name'     => 'file',
                            'contents' => fopen($request->file('file')->getPathname(), 'rb'),
                        ],
                        [
                            'name'     => 'user_id',
                            'contents' => $query['user_id'],
                        ],
                        [
                            'name'     => 'access_token',
                            'contents' => $query['access_token'],
                        ],
                        [
                            'name'     => 'client_id',
                            'contents' => $query['client_id'],
                        ],
                    ];
                }

                break;
            case 'DELETE':
            case 'PUT':
                $formParams = array_merge(
                    $query,
                    $request->input()
                );

                break;
            default :
                $query = array_merge($query, $request->input());

                break;
        }

        $clientArgs['query']  = $query;

        if (!empty($formParams)) {
            $clientArgs['form_params'] = $formParams;
        }

        return self::makeGuzzleRequest($request, $clientArgs);
    }

    private static function makeGuzzleRequest(Request $request, $clientArgs): LaravelResponse
    {
        $client = new Client($clientArgs);

        try {
            $headers = ['Connection' => 'close'];

            $rawResponse = $client->request(
                $request->method(),
                str_replace('api/proxy', '', $request->path()),
                $headers
            );

            $contentType     = $rawResponse->getHeaders()['Content-Type'][0] ?? 'text/html; charset=UTF-8';
            $laravelResponse = new LaravelResponse($rawResponse->getBody(), 200);
        } catch (ClientException|GuzzleException $e) {
            $contentType = 'text/html; charset=UTF-8';

            $laravelResponse = new LaravelResponse(
                $e->getResponse()->getBody(),
                $e->getResponse()->getStatusCode()
            );
        } catch (\ErrorException $e) {
            $contentType = 'text/html; charset=UTF-8';

            $laravelResponse = new LaravelResponse(
                $e->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        } catch (\Exception $e) {
            $contentType = 'text/html; charset=UTF-8';

            if ($e->getResponse()) {
                $laravelResponse = new LaravelResponse(
                    $e->getResponse()->getBody(),
                    $e->getResponse()->getStatusCode()
                );
            } else {
                return response('no response given', 500);
            }
        }

        $laravelResponse->header('Content-Type', $contentType);

        return $laravelResponse;
    }
}
