@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>


@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

				<div id="signupLogo">

                    <img src="/images/{{ $loginLogo }}" />

				</div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<div class="col-md-6">

						<ul class="progress-indicator">
				            <li class="completed">
				                <span class="bubble"></span>
				                Step 1
				            </li>
				            <li class="completed">
				                <span class="bubble"></span>
				                Step 2
				            </li>
				            <li class="completed">
				            	<span class="bubble"></span>
				            	Finished!
				            </li>
				        	
				        </ul>

				    </div>
				</div>

				<h1>Signup Complete!</h1>

				<p>Thank you for signing up! Before you can start, you'll need to activate your account. Please click the link that we've sent to your email address.</p>

				<p><strong>You can then login via <a target="_blank" href="{{ $configData['website'] }}">{{ $configData['website'] }}</a>. Please bookmark this URL.</strong></p>

				<p>If you can't see our email, it's possible that it's gone in to your "junk" folder. Throughout your subscription we will send emails to you with important information, to ensure that you
				 get the best level of service from {{ $configData['name'] }}, please ensure that you have us whitelisted.</p>

				<p>For a comprehensive guide on whitelisting email addresses, please visit <a href="http://www.whatcounts.com/how-to-whitelist-emails/#Mobileme" target="_blank">http://www.whatcounts.com/how-to-whitelist-emails/#Mobileme</a></p>

				<p> If you still can't find it, please contact us <a href="{{ $configData['website'] }}/contact" target="_blank">contact us</a></p>


				<div class="row">
					<div class="col-md-9">
						<p>You'll also need to insert the following javascript snippet at the bottom of your site, preferably just before the closing of the body tag. Again, if you're not sure how to do this, please don't hestitate to contact us!</p>

						<p>Alternatively, you can <a href="{{ env( 'API_SOURCE' ) }}/download-wordpress-plugin/{{ $clientID }}">download our Wordpress plugin</a> if you have a Wordpress website.</p>
					</div>
					<div class="col-md-3">
						<a href="{{ env( 'API_SOURCE' ) }}/download-wordpress-plugin/{{ $clientID }}">
							<img src="/images/wordpress.png" />
						</a>
					</div>
				</div>

				<div class="clr"></div><br /><br />

				<p>Your javascript snippet:</p>

				<pre>
					{{ $trackerCode->sampleInScriptTags }}
				</pre>

				<br /><br />

				<div>
					<p>An example, showing where your javascript code should be placed:</p>

					<pre>
					
						{{ $trackerCode->sampleInBodyTags }}
					</pre>

				</div>

				<div class="clr"></div><br /><br /><br /><br />

				<div class="row">

					<div class="col-md-4">

						<img src="/images/bandit.png" style="margin-top: 80px" />
							<div class="clr"></div><br /><br /><br />

						<div style="margin-left: 15px">
							<script src="https://d35xd5ovpwtfyi.cloudfront.net/loader/loader.min.js" async="" defer=""></script><img src="https://d35xd5ovpwtfyi.cloudfront.net/loader/buttons/008DBD.png" data-appointlet-organization="a1webstats" data-appointlet-service="43047">						
						</div>
					</div>

					<div class="col-md-8">

						<h2>What's Next?</h2> 

						<p>You <strong>WON’T</strong> get cowboy pushy sales calls 
							from us.  No-one here will ever try to push you 
							into buying after your trial.</p><br />

						<p>You <strong>WILL</strong> get the offer of a call with 
							us that’s focused on you getting maximum benefits 
							from your A1WebStats trial.</p><br />

						<p>Here’s a trialler comment that typically 
							summarises how beneficial the calls are …</p><br />
 
						<p style="margin-left: 30px"><em>"Thank you for focusing so much on the success
						 of our website. From our call today I can see how the 
						 system goes way beyond the 'simple stuff' (as you called it) 
						 of identifying companies that visited our website,
						  and how I can use the system to gain a lot more
						   from our website visitors."</em></p><br />
 
						<p>All you need to do is click on the 'Book Now' button 
							(on the left) to pick a date/time convenient for you, 
							plus your choice of how you’d like us to contact you.</p>
 						<br />
						<p>We suggest booking your call for a date/time when 
							you expect to have (roughly) 300 website visitors, 
							so that there’s plenty of data to analyse
							 within the call.</p><br />
 
						<p>If you need to modify or cancel your booked call, 
							you can do that within the call confirmation email 
							that’s emailed to you.</p>

					</div>
				</div>

				<div class="clr"></div><br />

				<div class="row">

					<div class="col-md-4" style="text-align: center">
						<a href="https://twitter.com/a1webstats?lang=en" target="_blank">
							<img src="/images/twitter-signup.png" style="margin-top: 80px;" />
						</a>
						<div class="clr"></div>
					</div>

					<div class="col-md-8">
						<br /><br /><br /><br /><br /><br />
						<p>
							<a href="https://twitter.com/a1webstats?lang=en" target="_blank">
								Follow us on Twitter
							</a> for exclusive information regarding A1WebStats
						</p>
					</div>
				</div>

				<div class="clr"></div><br /><br /><br />

			</div>
		</div>
	</div>
@endsection