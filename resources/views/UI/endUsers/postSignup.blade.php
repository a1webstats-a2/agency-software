@extends( 'templates.signup' )

@section( 'content' )

	<h1>Signup Complete</h1>

	<p>Thank you, you have successfully signed up!</p>

	<p>You will need to place this javascript snippet at the bottom of your page.</p>

	<p>{{ $response }}</p>

	<p><a href="/">Login</a></p>

@endsection