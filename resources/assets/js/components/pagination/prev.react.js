import React         	from 'react';
import FilterActions 	from '../../actions/FilterActions';
import Previous 		from 'material-ui/svg-icons/image/navigate-before';
import RaisedButton 	from 'material-ui/RaisedButton';

function getCurrentState( props ) {

	return {

		paginationSettings 	: 	props.paginationSettings,
		isLoadingResults 	: 	props.isLoadingResults
	}
}

var PaginationPrev = React.createClass({ 

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps ) {

		if( this.props === nextProps ) {

			return false;
		}

		return true;
	},

	render 	: 	function() {

		var disabled = ( this.state.isLoadingResults ) ? true : false;

		if( this.state.paginationSettings.offset > 0 ) {

			return (

				<RaisedButton fullWidth={true} disabled={disabled} style={ { marginBottom : 20 } } label="Previous" primary={true} icon={<Previous />} onClick={this._actionPrev} />

			);
		}

		return (
			<div className="paginationPrev">
			</div>

		);
	},

	_actionPrev 	: 	function() {

		FilterActions.paginationPrev();

	},

});

export default  PaginationPrev;