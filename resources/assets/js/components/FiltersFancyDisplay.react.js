import React from 'react' ;
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';
import ResetIcon from 'material-ui/svg-icons/content/remove-circle';
import SaveIcon from 'material-ui/svg-icons/content/save';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import {Link} from 'react-router'
import ExportIcon from 'material-ui/svg-icons/file/file-download';
import IconButton from 'material-ui/IconButton';
import ListIcon from 'material-ui/svg-icons/action/list';
import FilterActions from '../actions/FilterActions' ;
import FancyFiltersContainer from './FancyFiltersContainer.react';

function getCurrentState(props) {
    return {
        noResultsStatus: props.noResultsStatus,
        isApplicationResting: props.isApplicationResting,
        filters: props.filters,
        settings: props.settings,
        display: props.display,
        latestAddedFilters: props.latestAddedFilters
    }
}

const styles = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
    }
};

const FiltersFancyDisplay = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function () {
        return true;
    },

    render: function () {
        let fancyFiltersContainerStyle;
        if (this.state.display) {
            fancyFiltersContainerStyle = {
                flexWrap: 'wrap',
                display: 'flex',
                maxHeight: 150,
                overflow: 'scroll'

            }
        } else {
            fancyFiltersContainerStyle = {
                height: 50,
                overflow: 'hidden'
            }
        }

        let refreshIcon, resetIcon, saveIcon, exportIcon, listIcon = '';
        var loadingStatus = 'ready'

        if (!this.state.isApplicationResting) {
            loadingStatus = 'loading'
        } else {
            listIcon = (
                <Link to="/results">
                    <IconButton tooltip="Show Results" tooltipPosition="top-center">
                        <ListIcon color="#170550"/>
                    </IconButton>
                </Link>

            )

            if (window.location.pathname === "/results") {
                exportIcon = (
                    <IconButton
                        onClick={this._export}
                        tooltip="Export"
                        tooltipPosition="top-center"
                        tooltipStyles={{zIndex: 9999999999}}>
                        <ExportIcon color="#170550"/>
                    </IconButton>
                )
            }

            saveIcon = (
                <IconButton onClick={this._save} tooltip="Create Template" tooltipPosition="top-center">
                    <SaveIcon color="#170550"/>
                </IconButton>
            )

            refreshIcon = (
                <IconButton onClick={this._refresh} tooltip="Load / Reload Results" tooltipPosition="top-center">
                    <RefreshIcon color="#170550"/>
                </IconButton>
            )

            resetIcon = (
                <IconButton onClick={this._resetFilters} tooltip="Reset Filters" tooltipPosition="top-center">
                    <ResetIcon color="#170550"/>
                </IconButton>
            )
        }

        return (
            <div>
                <div className="row">
                    <div className="col-md-5">
                        <div style={fancyFiltersContainerStyle}>
                            <div style={{flexWrap: 'wrap', width: '100%', display: 'flex', float: 'left'}}>
                                <FancyFiltersContainer
                                    noResultsStatus={this.state.noResultsStatus}
                                    latestAddedFilters={this.state.latestAddedFilters}
                                    filters={this.state.filters}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="fancyFiltersToolbar" id="showHideFancyFiltersToggle">
                            <div className="quickToolbarOption">
                                {listIcon}
                            </div>
                            <div className="quickToolbarOption">
                                {exportIcon}
                            </div>
                            <div className="quickToolbarOption">
                                {refreshIcon}
                            </div>
                            <div className="quickToolbarOption">
                                {resetIcon}
                            </div>
                            <div className="quickToolbarOption">
                                {saveIcon}
                            </div>
                            <div id="bottomLoadingStatus">
                                <RefreshIndicator
                                    size={40}
                                    left={10}
                                    top={0}
                                    status={loadingStatus}
                                    style={styles.refresh}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    _export: function () {
        FilterActions.openExport();
    },
    _save: function () {
        FilterActions.quickCreateReport();
    },
    _resetFilters: function () {
        if (!confirm('Reset filters?')) {
            return false;
        }

        FilterActions.resetFilters();
    },
    _refresh: function (event) {
        event.preventDefault();

        FilterActions.updateResults();
    },
    _showHideFancyFilters: function (event) {
        event.preventDefault();

        let displayFancyFilters = (!this.state.display);

        FilterActions.displayFancyFilters(displayFancyFilters)
    },
    _handleRequestDelete: function (event) {
    }
});

export default FiltersFancyDisplay;
