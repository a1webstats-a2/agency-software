import {browserHistory} from 'react-router'
import ReportBuilderDispatcher from '../dispatcher/ReportBuilderDispatcher';
import FilterConstants from '../constants/FilterConstants';
import EventEmitterObj from 'events';
import UserTipActions from '../actions/UserTipActions';
import moment from 'moment';
import Raven from 'raven-js';
import PrettyPrintScenarioFilter from '../classes/PrettyPrintScenarioFilter';
import PrettyPrintStandardFilter from '../classes/PrettyPrintStandardFilter';

let _ravenInstalled = false;
let ravenDSN = false;
if (typeof window.location.hostname !== "undefined" &&
    window.location.hostname !== "localhost") {
    ravenDSN = 'https://ba63685621304ec6881296fff1f91196@sentry.io/103582';
    _ravenInstalled = true;
}
Raven.config(ravenDSN)
    .install();
let EventEmitter = EventEmitterObj.EventEmitter;

function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
}

Date.prototype.toMysqlTime = function () {
    return twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
}
Date.prototype.toMysqlDateOnly = function () {
    const momentDate = moment(this);
    const returnDate = momentDate.format('YYYY-MM-DD');
    return returnDate;
}
Date.prototype.toMysqlFormat = function () {
    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
};
import merge from 'merge';
import assign from 'object-assign';
import FilterActions from '../actions/FilterActions';
import NotificationsStore from './NotificationsStore';
import AdavancedFilterActions from '../actions/AdvancedFilterActions';

let _storeTest = 0;
let date = new Date();
let _patternMatchI = 0;
let defaultTimeFrom = new Date();
let defaultTimeTo = new Date();
let _prices = {
    monthly: false,
    annually: false
}
let _renewTokenDidSucceed = -1;
let _renewTokenMessage = "";
let _currentRowOrganisationIndex = 0;
let _noResultsReportSent = false;
let _loadingAdditionalCompanyData = true;
let _reportRecord = {
    sendFeedbackWhenResolved: false,
    reasonForReporting: false,
    visitData: null,
    organisation: '',
    userID: -1
}
let _lang = false;
let _invoices = [];
let _crmExportSelection = {
    zapier: false,
    dynamics: false,
    salesforce: false
}
let _flatPageVisits = false;
let _websocketMessages = [];
defaultTimeFrom.setHours(0);
defaultTimeFrom.setMinutes(0);
defaultTimeFrom.setSeconds(0);
defaultTimeTo.setHours(23);
defaultTimeTo.setMinutes(59);
defaultTimeTo.setSeconds(59);
let defaultDateFrom = new Date();
defaultDateFrom.setDate(date.getDate());
let defaultDateTo = new Date();
defaultDateTo.setDate(defaultDateTo.getDate());
let _autoTags = {};
let _showReportNoResultsBox = false;
let _trackerCode = {
    plain: '',
    sampleInBodyTags: '',
    sampleInScriptTags: ''
}
let getAJAXRequests = (function () {
    let oldSend = XMLHttpRequest.prototype.send,
        currentRequests = [];
    XMLHttpRequest.prototype.send = function () {
        currentRequests.push(this);
        oldSend.apply(this, arguments);
        this.addEventListener('readystatechange', function () {
            let idx;
            if (this.readyState === XMLHttpRequest.DONE) {
                idx = currentRequests.indexOf(this);
                if (idx > -1) {
                    currentRequests.splice(idx, 1);
                }
            }
        }, false);
    };
    return function () {
        let numRequests = 0;
        for (let i in currentRequests) {
            numRequests++;
        }
        return numRequests;
    }
}());
let _userCommunicationPreferences = {
    allow_phone: -1,
    allow_transactional: -1,
    allow_offers: -1,
    allow_services: -1,
    allow_blog: -1
}
let _editTag = {
    ID: -1,
    description: ''
}
let _noResultsMsg = "";
let _newFormattedOrganisations = [];
let _exclusions = {};
let _exportOrganisationIDToCRM = 0;
let _accountOK = true;
let _accountStatus = 0;
let _updatedActiveRecordPossibleOptions = [];
let _checkedForActiveRecordPossibleOptions = true;
let _updatedActiveRecordChosenOption = -1;
let _updatedActiveRecordSearchingForOptions = false;
let _updatingAnalytics = false;
let _showScenarioFilters = null;
let _clientTypesByIndex = {};
let _sendOrganisationByEmailText = "";
let _organisationsDatatableWidth = 0;
let _bulkOrganisationExportType = "csv";
let _looseFiltersToDisplayInLightbox = {
    filters: null,
    type: ''
};
let _loadingTriggerActionsEmailSendTo = true;
let _editTriggerActionsForTemplateID = 0;
let _triggerActionEmailAlertTeamMembers = {};
let _trackedOrganisations = {}
let _sendOrganisationByEmailAttachSpreadsheet = false;
let _allSettings = {
    agentConfig: {
        contact: null,
        logo: false,
        name: false,
        id: -1
    },
    hooks: {
        newOrgHook: 'http://',
        newVisitHook: 'http://'
    },
    user: {
        display_page_visits_by_default: true,
        preferred_date_format: 'fulltext',
        enable_desktop_notifications: false,
        disable_tips: false,
        display_full_urls: false
    },
    team: {
        display_page_visits_concatenated: 0,
        use_geolocation: 0,
        display_gclid: 0,
        uses_zoho: 0,
        zoho_auth_token: '',
        dynamics_callback_url: '',
        end_user_company_id: 0,
        subscribed_until: '2016-01-01 00:00:00',
        using_gocardless: false,
        addr1: '',
        addr2: '',
        country: '',
        county: '',
        postcode: '',
        town_or_city: '',
        salesforce_organisation_id: '',
        send_invoices_to: '',
        vat_number: '',
        clarity: '',
    }
};
let _help = {msg: ""};
let _currentMapDetails = {
    organisationName: '',
    isGeoLocated: false,
    locationString: '',
    longitude: false,
    latitude: false
}
let _newTeamMember = {
    newSystemUser: {
        id: 0,
        password: ''
    },
    user: {
        forename: '',
        user: '',
        email: '',
        id: 0,
        consentedToTransactionalEmails: false
    }
};
let _updatingBox = {
    updating: false,
    message: '',
    component: ''
}
let _quickExportSearchResults = false;
let _commonPathsSessionIDs = [];
let _commonPathsSessionIDsSet = false;
let _commonPathsRowSelected = false;
let _agentLogo = false;
let _agentName = false;
let _agentID = 0;
let _zohoKey = "";
let _filterRound = 0;
let _exportOrganisations = [];
let _lastSetOfFilters = [];
let _landingPageNumber = 0;
let _clientTypesLightboxObj = {
    organisationID: -1,
    relationships: []
}
let _numberOfResultLoads = 0;
let _blockedOrganisations = [];
let _quickExportSelectionType = "all";
let _quickExportFileTypes = {
    csv: true,
    pdf: false
}
let _additionalCompanyData = {
    data: [],
    organisationID: 0
}
let _displayPerPageOnLandingPages = 100;
let _updatedActiveRecord = {
    data: {
        'use_organisation_name': "",
        'use_organisation_tel': "",
        'use_organisation_website': "",
        'use_organisation_linked_in_address': "",
        'organisationid': ""
    }
};
let _currentRoute = window.location.pathname.replace("/", "");
let _quickExportSelection = [];
let _quickExportSearchString = "";
let _loadingNotes = false;
let _sendCompanyID = 0;
let _latestAddedFilters = [];
let _showFancyFilters = false;
let _analyticsOutOfDate = false;
let _previousHistoryOrganisation = "";
let _previousVisits = {
    selection: [],
    loading: false,
    offset: 0,
    allFound: [],
    organisationID: 0
}
let _excludeSelection = [];
let _excludeSelectionByName = {};
let _loadingDateChange = false;
let _loadingPreviousHistory = false;
let _applicationIsResting = false;
let _createTemplateStatus = "hide";
let _countryVisitorStats = false;
let _uploadingAvatar = false;
let _errorMsg = false;
let _bankedFilterID = 0;
let _activeRecord = false;
let _sessionResultPopoverObj = {open: false};
let _customFilterIncludeOrExclude = "include";
let _filtersByTypeAsIndex = {};
let _setAutomationSettings = {open: false, template: null};
let _allSelected = {};
let _selectAllSearchByType = '';
let _user = {id: 0, forename: '', surname: '', user_type_id: 1};
let _teamMemberToEdit = null;
let _editingFilters = false;
let _noteOrganisationId = 0;
let _clientSecret = "";
let _avatarSet = false;
let _activeTab = 'dashboard';
let _openDrawer = 4;
let _keyboardPressLapse = null;
let _newFiltersApplied = true;
let _crypt = '';
let _filterChange = 0;
let _currentTrackingObj = null;
let _resultsUpdated = false;
let _popoverSettings = {anchorEl: null, open: false, element: ''};
let _zapierIsSetup = false;
let _analyticsData = {
    referrers: [],
    visitorStats: {},
    organisations: [],
    keywords: [],
    lastPages: [],
    pages: [],
    entryPages: [],
    visitorLocations: [],
    tags: [],
    mailCampaigns: [],
    counties: [],
    campaignSources: [],
    campaignMediums: [],
    heatmapData: {},
    events: [],
};
let _paymentType = 2;
let _filters = [
    {
        storedValue: defaultDateFrom.toMysqlDateOnly(),
        type: 'dateFrom',
        round: 0
    },
    {
        storedValue: defaultDateTo.toMysqlDateOnly(),
        type: 'dateTo',
        round: 0
    },
    {
        storedValue: defaultTimeFrom,
        type: 'timeFrom',
        round: 0
    },
    {
        storedValue: defaultTimeTo,
        type: 'timeTo',
        round: 0
    },
    {
        storedValue: 2,
        type: 'displayBy',
        round: 0
    },
    {
        storedValue: {
            id: 1,
            name: 1,
            value: 1,
            textValue: 'Organisations'
        },
        type: 'visitorTypeFilter',
        round: 0
    },
    {
        storedValue: {
            id: 2,
            name: 2,
            value: 2,
            textValue: 'Education'
        },
        type: 'visitorTypeFilter',
        round: 0
    },
    {
        storedValue: {
            id: 6,
            name: 6,
            value: 6,
            textValue: 'Unknown'
        },
        hidden: true,
        type: 'visitorTypeFilter',
        round: 0
    },
    {
        id: 0,
        storedValue: {
            id: 0,
            name: 0,
            value: 1,
            textValue: '1'
        },
        type: 'minNumberOfPages',
        round: 0
    },
    {
        id: 0,
        storedValue: {
            id: 0,
            name: 0,
            value: 0,
            textValue: '0'
        },
        type: 'minSessionDuration',
        round: 0
    },
    {
        id: 0,
        storedValue: {
            id: 0,
            name: 0,
            value: 0,
            textValue: 'None'
        },
        type: 'maxNumberOfPages',
        round: 0
    },
    {
        id: 0,
        storedValue: {
            id: 0,
            name: 0,
            value: 1,
            textValue: 1
        },
        type: 'firstLoad',
        round: 0
    },
    {
        id: 0,
        storedValue: {
            id: 0,
            name: 0,
            value: false,
            textValue: "No"
        },
        type: 'newVisitorsOnly',
        round: 0
    },
    {
        id: 0,
        storedValue: {
            id: 0,
            name: 0,
            value: false,
            textValue: "No"
        },
        type: 'hasVideoOnly',
        round: 0
    },
    {
        id: 0,
        storedValue: {
            id: 0,
            name: 0,
            value: false,
            textValue: "No"
        },
        type: 'returnVisitorsOnly',
        round: 0
    },
];
let _originalFilters = clone(_filters);
let _templates = [];
let _results = {
    totals: {},
    visitorStats: {},
    relationships: [],
    results: []
};
let _resultsOriginalState = clone(_results);
let _sendEmailToTeamMembers = [];
let _windowBoxOpen = true;
let _notes = [];
let _customFilterType = 'customfilterurl';
let _exportSelection = [];
let _exportSelectionType = "all";
let _searchByLoadingStatus = "ready";
let _previousHistory = [];
let _storedOrganisationData = {set: false};
let _initialLoadComplete = false;
let _filterId = _filters.length + 1;
let _loadingStatus = {loadingStatus: "hide", loadingMsg: ""};
let _clientId = 0;
let _allOrganisations = [];
let _organisationFilters = {};
let _paginationReturn = 10;
let _paginationOffset = 0;
let _paginationDisplayOnPage = 10;
let _paginationNumberOfPages = 1;
let _referrers = [];
let _flashMessages = [];
let _countries = [];
let _keywords = [];
let _keywordValues = [];
let _clientPages = [];
let _snackbarOpen = false;
let _snackbarMsg = '';
let _snackbarComponent = false;
let _snackbarOtherData = {};
let _inclusionType = "include";
let _countryInclusionType = true;
let _companyDataOpen = [];
let _allBrowsers = [];
let _operatingSystems = [];
let _narrowedSearchOptions = [];
let _currentSearchCriteria = {searchString: '', type: 'url', fullType: 'customFilterURL', noSearchResults: false};
let _narrowedSearchOptionsType = "";
let _commonPaths = [];
let _allVisitorTypesSelected = false;
let _lightboxSettings = {type: null, id: null};
let _firstLoadComplete = {filters: false, reportWizard: false, analytics: false};
let _quickReportDateRange = 1;
let _quickReportTrafficType = 1;
let _quickReportDataType = 6;
let _orderBy = 1;
let _exisitingAutomatedReport = {
    open: false,
    data: false
};
let _existingAutomatedReportsData = {
    open: false,
    reports: []
};
let _displayedColumns = {
    url: true,
    hostname: true,
    pageVisitDate: true,
    duration: true,
    queryString: true,
    ipAddress: true,
    location: true,
    organisation_name: true,
    totalVisitors: true
}
let _quickOpen = {
    type: '',
    tab: 0
}
let _teamMembers = [];
let _relationships = [];
let _clientTypes = [];
let _displayBy = 2; // session view
let _exportCriteria = {
    showFullURLOnEntryPage: false,
    sendToAll: false,
    newTemplateName: "",
    includeTeamMembers: [],
    exportTypes: {
        PDF: false,
        CSV: true,
        Excel: false
    },
    include: [
        {id: 0, name: 'Organisation Name', include: true},
        {id: 1, name: 'Telephone Number', include: true},
        {id: 2, name: 'Website', include: true},
        {id: 3, name: 'LinkedIn', include: true},
        {id: 8, name: 'Hostname', include: true},
        {id: 9, name: 'IP Address', include: true},
        {id: 10, name: 'Session Start Date/Time', include: true},
        {id: 11, name: 'Monitor Resolution', include: true},
        {id: 12, name: 'Operating System', include: true},
        {id: 14, name: 'Browser', include: true},
        {id: 15, name: 'Device Type', include: true},
        {id: 18, name: 'Longitude', include: false},
        {id: 19, name: 'Latitude', include: false},
        {id: 20, name: 'Country', include: true},
        {id: 25, name: 'Town / City', include: true},
        {id: 26, name: 'County / State', include: true},
        {id: 27, name: 'Registered Address', include: true},
        {id: 28, name: 'Entry Page', include: true},
        {id: 29, name: 'Keywords', include: true},
        {id: 30, name: 'Sources', include: true},
        {id: 31, name: 'PPC', include: true},
        {id: 32, name: 'Organic', include: true},
        {id: 33, name: 'Pages Visited', include: true},
        {id: 34, name: 'Geolocated', include: true},
        {id: 35, name: 'Geolocated Address', include: false},
        {id: 36, name: 'Visit Duration', include: true},
        {id: 37, name: 'UTM Parameters', include: false},
        {id: 38, name: 'Tags', include: false}
    ]
};
let _showOrganisationInLightbox = null;
let _autoExports = null;

function setQuickOpen(quickOpenObj) {
    _quickOpen = quickOpenObj;
    switch (quickOpenObj.type) {
        case "reportWizard" :
            openDrawer(3)
            break;
    }
}

function addTeamMemberToSendEmailArray(teamMemberID) {
    if (_sendEmailToTeamMembers.indexOf(teamMemberID) === -1) {
        _sendEmailToTeamMembers.push(teamMemberID);
    } else {
        let index = _sendEmailToTeamMembers.indexOf(teamMemberID);
        delete _sendEmailToTeamMembers[index];
    }
}

function incrementFilterChange(newFiltersApplied) {
    _filterChange++;
    if (newFiltersApplied) {
        _applicationIsResting = true;
    }
}

function addToExcludeSelection(organisation) {
    let indexOf = _excludeSelection.indexOf(organisation.id);
    _excludeSelectionByName[organisation.id] = organisation.name;
    if (indexOf === -1) {
        _excludeSelection.push(organisation.id);
    } else {
        delete _excludeSelection[indexOf];
    }
}

function setQuickExportFileType(exportObj) {
    _quickExportFileTypes[exportObj.type] = exportObj.apply;
}

function setInitialFiltersByTypeAndValue() {
    for (let key in _filters) {
        let filter = _filters[key];
        if (typeof filter !== "undefined") {
            if (typeof filter.storedValue === "object") {
                let newVal = filter.storedValue.value;
                if (!isNaN(newVal)) {
                    newVal = parseInt(newVal);
                }
                addFilterTypeAndID(filter.type, newVal);
            } else {
                let newVal = filter.storedValue;
                if (!isNaN(newVal)) {
                    newVal = parseInt(newVal);
                }
                addFilterTypeAndID(filter.type, newVal);
            }
        }
    }
}

function updateExportOptions(exportOptions) {
    _exportCriteria = exportOptions;
}

function blockOrganisation(organisation) {
    _blockedOrganisations[organisation.id] = organisation.name;
    let request = new XMLHttpRequest();
    setSnackbarInternal({
        message: organisation.name + " blocked"
    })
    request.open("GET", "/api/proxy/api/v1/organisation/block/" + organisation.id + "/edit");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send();
}

function removeExclusion(exclusionID) {
    if (typeof _exclusions[exclusionID] !== "undefined") {
        delete _exclusions[exclusionID];
    }
    setSnackbarInternal({
        message: 'Exclusion removed'
    })
    let request = new XMLHttpRequest();
    request.open("DELETE", "/api/proxy/api/v1/tags/exclusions/" + exclusionID);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send();
}

function setActiveRecord(activeRecord) {
    setLightbox({
        type: 'editCompanyRecord',
        id: 1
    })
    _activeRecord = clone(activeRecord);
    _updatedActiveRecord = clone(activeRecord);
}

function addTeamMemberToExportCriteria(memberId) {
    _exportCriteria.includeTeamMembers.push(parseInt(memberId.memberId));
}

function removeTeamMemberFromExportCriteria(memberId) {
    let key = _exportCriteria.includeTeamMembers.indexOf(parseInt(memberId.memberId));
    if (key > -1) {
        delete _exportCriteria.includeTeamMembers[key];
    }
}

function saveBillingDetails(billingDetails) {
    _allSettings.team = billingDetails;
    let request = new XMLHttpRequest();
    setSnackbarInternal({
        message: "Saving Billing Details"
    })
    request.open("POST", "/api/proxy/api/v1/end-user-company/billing-details");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(billingDetails));
}

function storeAutoTags(autoTags) {
    _autoTags = autoTags;
}

function broadcastInitialLoad() {
    _initialLoadComplete = true;
}

function loadFilters(data) {
    _filters = data.filters;
}

function loadNewReportFilters(newFilters) {
    let saveNewFilters = [];
    let ignoreFilterTypes = [
        'timefrom',
        'timeto'
    ];
    let highestKey = 0;
    for (let i in newFilters) {
        let newFilter = newFilters[i];
        if (typeof newFilter.type !== "undefined") {
            if (ignoreFilterTypes.indexOf(newFilter.type.toLowerCase()) === -1) {
                saveNewFilters[i] = newFilter;
            }
        }
        highestKey = i;
    }
    highestKey++;
    saveNewFilters[0] = _filters[0];
    saveNewFilters[1] = _filters[1];
    _filters = saveNewFilters;
    _filterId = highestKey;
    _filtersByTypeAsIndex = {};
    setInitialFiltersByTypeAndValue();
    loadResults();
    browserHistory.push('/results');
}

function startQuickExport(type) {
    let selection = {};
    if (Array.isArray(_quickExportSelection)) {
        _quickExportSelection.forEach(function (item, i) {
            if (item) {
                selection[i] = item;
            }
        });
    } else {
        selection = _quickExportSelection;
    }
    let sendObj = {
        exportCriteria: _exportCriteria,
        selectionType: _quickExportSelectionType,
        fileTypes: _quickExportFileTypes,
        selection: selection,
        excludeSelection: {
            organisations: _excludeSelection
        },
        type: type,
        filters: sortFilters(_filters)
    }
    let request = new XMLHttpRequest();
    setSnackbarInternal({
        message: "Starting Export"
    })
    request.open("POST", "/api/proxy/internal/quick-export");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    let cache = [];
    const jsonToSend = JSON.stringify(sendObj, function (key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                return;
            }
            cache.push(value);
        }
        return value;
    });
    cache = null;
    request.send(jsonToSend);
}

function storePricing(pricing) {
    _prices = pricing;
}

function updateUserPermissions(permissions) {
    _userCommunicationPreferences = permissions;
    const request = new XMLHttpRequest();
    request.open("PUT", "/api/proxy/api/v1/user/communication-preferences");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    break;
            }
        }
    };
    request.send(JSON.stringify({permissions}));
}

function loadCommunicationPreferences() {
    const request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/user/communication-preferences");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    FilterActions.storeUserCommunicationPreferences(JSON.parse(request.responseText));
                    break;
            }
        }
    };
    request.send();
}

function setTemplateFilters(templateFiltersObj) {
    let templateId = templateFiltersObj.templateId.templateId;
    let request = new XMLHttpRequest();
    _applicationIsResting = false;
    setSnackbarInternal({
        message: "Loading Template"
    })
    request.open("GET", "/api/proxy/api/v1/report-templates/" + templateId);
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            _applicationIsResting = true;
            switch (request.status) {
                case 200 :
                    FilterActions.loadNewReportFilters(JSON.parse(request.responseText));
                    break;
            }
        }
    };
    request.send();
}

function storeExclusions(exclusions) {
    _exclusions = exclusions;
}

function addFilterTypeAndID(type, id) {
    if (typeof type === "undefined") {
        return false;
    }
    if (typeof _filtersByTypeAsIndex === 'undefined') {
        _filtersByTypeAsIndex = {};
    }
    if (typeof _filtersByTypeAsIndex[type.toLowerCase()] === "undefined") {
        _filtersByTypeAsIndex[type.toLowerCase()] = [];
    }
    if (typeof id !== 'undefined') {
        if (_filtersByTypeAsIndex[type.toLowerCase()].indexOf(id) === -1) {
            switch (type.toLowerCase()) {
                case 'datefrom':
                    _filtersByTypeAsIndex[type.toLowerCase()][0] = id;
                    break;
                case 'dateto':
                    _filtersByTypeAsIndex[type.toLowerCase()][0] = id;
                    break;
                default :
                    _filtersByTypeAsIndex[type.toLowerCase()].push(id);
                    break;
            }
        }
    }
}

function showReportNoResultsBox() {
    _showReportNoResultsBox = true;
}

function setQuickExportSelectionType(exportSelectionType) {
    _quickExportSelectionType = exportSelectionType;
    let newSelection = []
    if (exportSelectionType === "all") {
        _quickExportSearchResults.forEach(function (item, i) {
            return newSelection[parseInt(item[0])] = item[1];
        });
        _quickExportSelection = newSelection;
    } else if (exportSelectionType === "patternMatch") {
        _quickExportSearchResults.forEach(function (item, i) {
            return newSelection[parseInt(item[0])] = item[1];
        });
        _quickExportSelection = newSelection;
    } else if (exportSelectionType === "selection") {
        _quickExportSelection = newSelection;
    }
}

function acknowledgeDateChange() {
    _applicationIsResting = false;
    _updatingAnalytics = true;
    _quickReportDateRange = 7;
    _analyticsOutOfDate = true;
    _newFiltersApplied = true;
    _paginationOffset = 0;
    let filterParams = sortFilters(_filters);
    let request = new XMLHttpRequest();
    FilterActions.setSnackbar({
        message: 'Updating filters',
        open: true
    });
    request.open("POST", "/api/proxy/api/v1/events/date-change");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            _loadingDateChange = false;
            switch (request.status) {
                case 200 :
                    let returnData = false;
                    try {
                        returnData = JSON.parse(request.responseText);
                    } catch (e) {
                        returnData = false;
                    }
                    if (!returnData) {
                        return false;
                    }
                    closeSnackbar();
                    _applicationIsResting = true;
                    _updatingAnalytics = false;
                    storeAnalyticsData(JSON.parse(request.responseText).analytics);
                    _editingFilters = false;
                    if (typeof returnData.results !== "undefined" && returnData.results) {
                        _results = returnData.results;
                    }
                    if (typeof returnData.commonPaths !== "undefined" && returnData.commonPaths) {
                        _commonPaths = JSON.parse(returnData.commonPaths);
                    }
                    if (_currentRoute.includes("organisations")) {
                        FilterActions.loadNewFormatOrganisations();
                    } else {
                        FilterActions.incrementFilterChange();
                    }
                    break;
                case 401 :
                    FilterActions.setLightbox({
                        type: 'attemptNewToken',
                        id: -1
                    });
                    break;
            }
        }
    };
    let sendObj = clone(filterParams);
    sendObj.type = _currentSearchCriteria.type;
    sendObj.searchString = _currentSearchCriteria.searchString;
    sendObj.fullType = _currentSearchCriteria.fullType;
    sendObj.routeType = _currentRoute;
    request.send(JSON.stringify(sendObj));
}

function sendOrganisationViaEmail(organisationID) {
    setLightbox({
        type: 'sendOrganisationViaEmailOpen',
        id: 1
    })
    _sendCompanyID = parseInt(organisationID);
}

function createFilters(filterObjects) {
    removeFilterByType({
        type: 'setSessionIDs',
        id: 1
    })
    _paginationOffset = 0;
    setNewFiltersAdded(filterObjects);
    filterObjects.forEach(filterObj => {
        addFilterTypeAndID(filterObj.type.toLowerCase(), filterObj.id);
        const thisFilterId = _filterId;
        _filterId++;
        _filters[thisFilterId] = {
            id: thisFilterId,
            type: filterObj.type,
            storedValue: filterObj.storedValue
        }
    })
}

function showScenarioFilters(filters) {
    _showScenarioFilters = filters;
    setLightbox({
        type: 'showScenarioFilters',
        id: -1
    })
}

function createRawFilters(filters) {
    filters.forEach(filter => {
        createRawFilter(filter);
    })
}

function setNewFiltersAdded(filters) {
    filters.forEach((filter) => {
        if (typeof filter !== "undefined") {
            if (typeof _latestAddedFilters[filter.type] === "undefined") {
                _latestAddedFilters[filter.type] = [];
            }
            _latestAddedFilters[filter.type].push(filter.storedValue.id);
        }
    })
}

function createRawFilter(filterObj) {
    if (typeof filterObj === 'undefined') {
        return false;
    }
    setNewFiltersAdded([filterObj]);
    switch (filterObj.type.toLowerCase()) {
        case "minnumberofpages" :
            addFilterTypeAndID(filterObj.type.toLowerCase(), filterObj.storedValue.value);
            break;
        case "maxnumberofpages" :
            addFilterTypeAndID(filterObj.type.toLowerCase(), filterObj.storedValue.value);
            break;
        default :
            addFilterTypeAndID(filterObj.type.toLowerCase(), filterObj.id);
            break;
    }
    let thisFilterId = _filterId;
    _filterId++;
    _paginationOffset = 0;
    _filterRound++;
    _filters[thisFilterId] = {
        id: thisFilterId,
        type: filterObj.type,
        storedValue: filterObj.storedValue,
        round: _filterRound
    };
}

function createFilter(filterObj) {
    removeFilterByType({
        type: 'setSessionIDs',
        id: 1
    })
    deleteFilterByType(filterObj);
    createRawFilter(filterObj);
}

function deleteFilter(id) {
    const filter = _filters[id];
    delete _filters[id];
    removeFilterByType({
        type: filter.type,
        id: filter.storedValue.id
    });
}

function changeLoadingStatusInternal(messageObj) {
    let loadingStatus = (messageObj.loading) ? "loading" : "hide";
    _loadingStatus = {
        loadingStatus: loadingStatus,
        loadingMsg: messageObj.message
    };
}

function changeLoadingStatus(messageObj) {
    messageObj = messageObj.data;
    let loadingStatus = (messageObj.loading) ? "loading" : "hide";
    _loadingStatus = {
        loadingStatus: loadingStatus,
        loadingMsg: messageObj.message
    };
}

function getClientId() {
    return _clientId;
}

function setCustomFilterIncludeType(includeOrExclude) {
    _customFilterIncludeOrExclude = includeOrExclude;
}

function editFilters(editedFilters) {
    _paginationOffset = 0;
    _editingFilters = true;
    editedFilters.map(function (filterObj, i) {
        _filters[filterObj.id] = assign({}, _filters[filterObj.id], filterObj);
    })
}

function setEndCompanyOrganisationRelationships(relationships) {
    _relationships = relationships;
}

function exportToCRMChoices() {
    _updatingBox.updating = true;
    _updatingBox.component = 'exportToCRM',
        _updatingBox.message = 'Exporting to CRM choices'
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/proxy/api/v1/export/crm', true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(JSON.stringify({
            crmChoices: _crmExportSelection,
            organisationID: _exportOrganisationIDToCRM
        })
    );
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                setSnackbarInternal({
                    message: 'Export complete'
                })
            } else {
                setSnackbarInternal({
                    message: 'Export failed, please renew token'
                })
            }
            closeLightbox();
            _bulkOrganisationExportType = 'csv';
            FilterActions.resetUpdatingBox();
        }
    };
}

function updateStripeDetails(token) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "api/proxy/api/v1/stripe/update-customer");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({
            token: token
        })
    );
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    alert("Thank you, your card details have been updated");
                    break;
                default :
                    alert("Sorry, there was a problem updating your card details: " + xhr.responseText);
                    break;
            }
        }
    };
}

function processStripePayment(token) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/stripe/new-customer", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({
            paymentType: _paymentType,
            token: token
        })
    );
    _applicationIsResting = false;
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            let success = (xhr.responseText === "true");
            if (success) {
                window.location = "/stripe/success";
            } else {
                alert("Sorry, there was a problem creating your subscription. Reason given by Stripe: " + xhr.responseText);
            }
        }
    };
}

function showNewResults(newResults) {
    _openDrawer = 5;
    setEndCompanyOrganisationRelationships(newResults.relationships);
    _paginationNumberOfPages = Math.ceil(newResults.totals.total / _paginationDisplayOnPage);
    _results = newResults;
    _loadingStatus = "hide";
}

function sortFilters(filters) {
    let sortedFilters = {};
    sortedFilters.selectionType = _exportSelectionType;
    sortedFilters.exportSelection = _exportSelection;
    for (let key in filters) {
        if (!filters[key]) {
            continue;
        }
        let filter = filters[key];
        if (filter.storedValue === '' || !filter.storedValue) {
            continue;
        }
        if (typeof filter.storedValue.anyThatMatch !== "undefined" &&
            filter.storedValue.anyThatMatch.apply) {
            if (typeof sortedFilters.patternMatches === "undefined") {
                sortedFilters.patternMatches = [];
            }
            sortedFilters.patternMatches.push(filter);
        } else {
            switch (filter.type) {
                case "firstLoad" :
                    sortedFilters.firstLoad = filter.storedValue.value;
                    break;
                case "hasVideoOnly" :
                    sortedFilters.hasVideoOnly = filter.storedValue.value;
                    break;
                case "newVisitorsOnly" :
                    sortedFilters.newVisitorsOnly = filter.storedValue.value;
                    break;
                case 'returnVisitorsOnly' :
                    sortedFilters.returnVisitorsOnly = filter.storedValue.value;
                    break;
                case "scenarioFilter" :
                    if (typeof sortedFilters.scenarios === "undefined") {
                        sortedFilters.scenarios = [];
                    }
                    sortedFilters.scenarios.push(filter);
                    break;
                case "minSessionDuration" :
                    sortedFilters.minSessionDuration = filter.storedValue.value;
                    break;
                case "maxNumberOfPages" :
                    sortedFilters.maxNumberOfPages = filter.storedValue.value;
                    break;
                case "minNumberOfPages" :
                    sortedFilters.minNumberOfPages = filter.storedValue.value;
                    break;
                case "ppc" :
                    if (typeof sortedFilters.ppc === "undefined") {
                        sortedFilters.ppc = [];
                    }
                    sortedFilters.ppc.push(filter.storedValue.value);
                    break;
                case "organic" :
                    if (typeof sortedFilters.organic === "undefined") {
                        sortedFilters.organic = [];
                    }
                    sortedFilters.organic.push(filter.storedValue.value);
                    break;
                case "trafficType" :
                    if (typeof sortedFilters.trafficTypes === "undefined") {
                        sortedFilters.trafficTypes = [];
                    }
                    sortedFilters.trafficTypes.push(filter.storedValue.value);
                    break;
                case "banked" :
                    if (typeof sortedFilters.banked === "undefined") {
                        sortedFilters.banked = [];
                    }
                    sortedFilters.banked.push(filter.storedValue.value);
                    break;
                case "tagFilter":
                    if (typeof sortedFilters.includeTagFilters === "undefined") {
                        sortedFilters.includeTagFilters = [];
                    }
                    if (typeof sortedFilters.excludeTagFilters === "undefined") {
                        sortedFilters.excludeTagFilters = [];
                    }
                    if (filter.storedValue.criteria === "include") {
                        sortedFilters.includeTagFilters.push(filter.storedValue.value);
                    } else {
                        sortedFilters.excludeTagFilters.push(filter.storedValue.value);
                    }
                    break;
                case "setSessionIDs":
                    sortedFilters.setSessionIDs = filter.storedValue.sessionIDs;
                    break;
                case "CustomFilterKeyword" :
                    if (typeof sortedFilters.keywordValues === "undefined") {
                        sortedFilters.keywordValues = [];
                    }
                    sortedFilters.keywordValues.push(filter.storedValue.value);
                    break;
                case "browserFilter" :
                    if (typeof sortedFilters.browsers === "undefined") {
                        sortedFilters.browsers = [];
                    }
                    sortedFilters.browsers.push(filter.storedValue.id);
                    break;
                case "deviceFilter" :
                    if (typeof sortedFilters.devices === "undefined") {
                        sortedFilters.devices = [];
                    }
                    sortedFilters.devices.push(filter.storedValue.id);
                    break;
                case "osFilter" :
                    if (typeof sortedFilters.operatingSystems === "undefined") {
                        sortedFilters.operatingSystems = [];
                    }
                    sortedFilters.operatingSystems.push(filter.storedValue.id);
                    break;
                case "visitorTypeFilter" :
                    if (typeof sortedFilters.visitorTypes === "undefined") {
                        sortedFilters.visitorTypes = [];
                    }
                    if (filter.storedValue.value !== false) {
                        sortedFilters.visitorTypes.push(filter.storedValue.name);
                    }
                    break;
                case "displayBy" :
                    sortedFilters.displayBy = filter.storedValue;
                    break;
                case "timeFrom" :
                    let timeFrom = new Date(filter.storedValue);
                    sortedFilters.timeFrom = timeFrom.toMysqlTime();
                    break;
                case "timeTo" :
                    let timeTo = new Date(filter.storedValue);
                    sortedFilters.timeTo = timeTo.toMysqlTime();
                    break;
                case "keyword" :
                    if (typeof sortedFilters.keywords === "undefined") {
                        sortedFilters.keywords = [];
                    }
                    sortedFilters.keywords.push({
                        keyword: filter.storedValue.keywordId,
                        value: filter.storedValue.keywordValue
                    });
                    break;
                case "customFilterTimeOnSite" :
                    if (typeof sortedFilters.timeSpent === "undefined") {
                        sortedFilters.timeSpent = {};
                    }
                    sortedFilters.timeSpent[filter.storedValue.criteria] = parseInt(filter.storedValue.value);
                    break;
                case "customFilterTotalVisits" :
                    if (typeof sortedFilters.totalVisits === "undefined") {
                        sortedFilters.totalVisits = {};
                    }
                    sortedFilters.totalVisits[filter.storedValue.criteria] = parseInt(filter.storedValue.value);
                    break;
                case "customFilterReferrerCountry" :
                    if (typeof sortedFilters.includeCountries === "undefined") {
                        sortedFilters.includeCountries = [];
                    }
                    if (typeof sortedFilters.excludeCountries === "undefined") {
                        sortedFilters.excludeCountries = [];
                    }
                    if (filter.storedValue.criteria === "include") {
                        sortedFilters.includeCountries.push(parseInt(filter.storedValue.value));
                    } else {
                        sortedFilters.excludeCountries.push(parseInt(filter.storedValue.value));
                    }
                    break;
                case "customFilterReferrer" :
                    switch (filter.storedValue.criteria.toLowerCase()) {
                        case "include" :
                            if (typeof sortedFilters.includeReferrers === "undefined") {
                                sortedFilters.includeReferrers = [];
                            }
                            sortedFilters.includeReferrers.push(filter.storedValue.value);
                            break;
                        case "exclude" :
                            if (typeof sortedFilters.excludeReferrers === "undefined") {
                                sortedFilters.excludeReferrers = [];
                            }
                            sortedFilters.excludeReferrers.push(filter.storedValue.value);
                            break;
                    }
                    break;
                case "specificIP" :
                    if (typeof sortedFilters.specificIPFilters === "undefined") {
                        sortedFilters.specificIPFilters = [];
                    }
                    sortedFilters.specificIPFilters.push(filter.storedValue)
                    break;
                case "dateFrom" :
                    sortedFilters.dateFrom = filter.storedValue;
                    break;
                case "dateTo" :
                    sortedFilters.dateTo = filter.storedValue;
                    break;
                case "organisationFilter" :
                    if (typeof sortedFilters.includeOrganisations === "undefined") {
                        sortedFilters.includeOrganisations = [];
                    }
                    if (typeof sortedFilters.excludeOrganisations === "undefined") {
                        sortedFilters.excludeOrganisations = [];
                    }
                    if (typeof filter.storedValue !== "undefined") {
                        if (filter.storedValue.id !== "NaN") {
                            if (filter.storedValue.include === "include") {
                                sortedFilters.includeOrganisations.push(filter.storedValue.id);
                            } else {
                                sortedFilters.excludeOrganisations.push(filter.storedValue.id);
                            }
                        }
                    }
                    break;
                case "CustomFilterEntryPage" :
                    if (typeof sortedFilters.entryPages === "undefined") {
                        sortedFilters.entryPages = [];
                    }
                    sortedFilters.entryPages.push({
                        criteria: filter.storedValue.criteria,
                        value: parseInt(filter.storedValue.id)
                    })
                    break;
                case "CustomFilterLastPage" :
                    if (typeof sortedFilters.lastPages === "undefined") {
                        sortedFilters.lastPages = [];
                    }
                    sortedFilters.lastPages.push({
                        criteria: filter.storedValue.criteria,
                        value: parseInt(filter.storedValue.id)
                    })
                    break;
                case "CustomFilterURL" :
                    if (typeof sortedFilters.urls === "undefined") {
                        sortedFilters.urls = [];
                    }
                    sortedFilters.urls.push({
                            'criteria': filter.storedValue.criteria,
                            'value': parseInt(filter.storedValue.value)
                        }
                    );
                    break;
                case "IPRange" :
                    if (typeof sortedFilters.ipRanges === "undefined") {
                        sortedFilters.ipRanges = [];
                    }
                    sortedFilters.ipRanges.push(filter.storedValue);
                    break;
            }
        }
    }
    sortedFilters.clientId = getClientId();
    sortedFilters.orderBy = _orderBy;
    sortedFilters.paginationReturn = getPaginationReturn();
    sortedFilters.paginationOffset = getPaginationOffset();
    sortedFilters.displayNumResultsOnPage = _paginationDisplayOnPage;
    sortedFilters.displayBy = getDisplayBy();
    return sortedFilters;
}

function getPaginationReturn() {
    return _paginationReturn;
}

function getPaginationOffset() {
    return _paginationOffset;
}

function setOrganisationData(resultKey) {
    setLightbox({
        type: 'companyData',
        id: 0
    });
    let result = _results.results[resultKey];
    let orgData = {
        orgData: result.organisation_data,
        orgId: result.organisationid
    };
    _storedOrganisationData = orgData;
    _storedOrganisationData.set = true;
}

function storeNewFormatOrganisations(organisations) {
    _newFormattedOrganisations = organisations;
    selectAllOptionsIfSelectionEmpty();
    let trackedOrganisations = {};
    _newFormattedOrganisations.forEach(function (org, i) {
        if (org.assigned_user) {
            trackedOrganisations[org.organisation_id] = org.assigned_user;
        }
    })
    _trackedOrganisations = trackedOrganisations;
}

function downloadCSV() {
    FilterActions.setSnackbar({
        message: 'Thank you, your report is being generated, we will alert you when it ' +
            'is ready to download. You can continue using this application in the meantime'
    })
    let filterParams = sortFilters(_filters);
    filterParams.clientId = getClientId();
    filterParams.paginationReturn = 1000000;
    filterParams.paginationOffset = 0;
    filterParams.displayBy = getDisplayBy();
    filterParams.actionType = 'Generating CSV';
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/internal/export/csv");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            let data = JSON.parse(request.responseText);
            FilterActions.setSnackbar({
                msg: 'Thank you, the report has been generated',
                file: '/internal/download/csv/contents/' + data.file
            })
        }
    };
    request.send(JSON.stringify(filterParams));
}

function setAlertsForTemplate(alertObj) {
    let request = new XMLHttpRequest();
    request.open("PUT", "/api/proxy/api/v1/report-templates/" + alertObj.templateID);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.setSnackbar({
                message: 'Template updated'
            });
        }
    };
    request.send(JSON.stringify(alertObj));
}

function uploadLoadOnLoginForTemplate(updateObj) {
    const newTemplates = _templates.map(function (template, i) {
        if (template.id === updateObj.templateID) {
            template.load_at_login = updateObj.apply;
        }
        return template;
    });
    _templates = newTemplates;
    FilterActions.setSnackbar({
        message: 'Settings updated'
    })
    let request = new XMLHttpRequest();
    request.open("PUT", "/api/proxy/api/v1/templates/load-at-login/" + updateObj.templateID);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updateObj));
}

function updateIncludeFields(items) {
    _exportCriteria.include = items;
}

function updateExistingAutomatedReportInStoreOnly(existingReport) {
    _exisitingAutomatedReport.data.report = existingReport;
}

function updateInvoiceTo(invoiceTo) {
    _allSettings.team.send_invoices_to = invoiceTo;
}

function setDisplayedColumns(displayObj) {
    _displayedColumns[displayObj.data.checkboxName] = displayObj.data.displayed;
}

function setDisplayPerPageForLandingPages(displayPerPageOnLandingPages) {
    _displayPerPageOnLandingPages = displayPerPageOnLandingPages;
}

function setFlashMessage(data) {
    _flashMessages.push(data.data);
    setTimeout(function () {
        FilterActions.removeFlashMessages();
    }, 3000);
}

function setPaginationOffset(type) {
    let msg = "";
    _applicationIsResting = false;
    if (type === "next") {
        msg = "Loading next results";
        _paginationOffset += _paginationReturn;
    } else {
        msg = "Loading previous results";
        _paginationOffset -= _paginationReturn;
    }
    FilterActions.setSnackbar({
        message: msg,
        open: true
    })
    loadResults();
}

function saveReportTemplate(templateObj) {
    _createTemplateStatus = "loading";
    setSnackbarInternal({
        message: "Creating Template"
    })
    _applicationIsResting = false;
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/internal/templates");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        _applicationIsResting = true;
        if (request.readyState === 4 && request.status === 200) {
            _createTemplateStatus = "hide";
            FilterActions.storeNewTemplates(JSON.parse(request.responseText).templates);
        }
    };
    request.send(JSON.stringify({
        templateName: templateObj.data.templateName,
        filters: _filters,
        sortedFilters: sortFilters(_filters)
    }));
}

function setClientId(clientId) {
    _clientId = clientId;
}

function getDisplayBy() {
    return _displayBy;
}

function instantUpdate() {
    _resultsUpdated = true;
    let filterParams = sortFilters(_filters);
    FilterActions.openDrawer(5);
    loadResults();
}

function resetResults() {
    instantUpdate();
}

function quickExportShowData(filterType) {
    setSnackbarInternal({
        message: 'Loading Data'
    })
    _applicationIsResting = false;
    let includeType
    if (filterType.toLowerCase() === 'customFilterReferrer') {
        includeType = 'includereferrers';
    } else if (filterType.toLowerCase() === 'customfilterurl') {
        includeType = 'nonexplicitInclude';
    } else if (filterType.toLowerCase() === 'customfilterentrypage') {
        includeType = 'firstpagevisited';
    } else {
        includeType = 'include';
    }
    resetToOriginalFilters();
    if (_quickExportSelectionType === 'patternMatch') {
        _patternMatchI++;
        createRawFilter({
            type: filterType,
            id: _patternMatchI,
            storedValue: {
                id: _patternMatchI,
                value: _patternMatchI,
                textValue: _quickExportSearchString,
                name: _quickExportSearchString,
                include: includeType,
                criteria: includeType,
                anyThatMatch: {
                    apply: true,
                    pattern: _quickExportSearchString
                }
            }
        })
    } else if (_quickExportSelection.length > 0) {
        if (filterType.toLowerCase() === "organisationfilter") {
            removeAllButOrganisationVisitorTypes();
        }
        _quickExportSelection.map(function (filter, i) {
            createRawFilter({
                type: filterType,
                id: i,
                storedValue: {
                    id: i,
                    value: i,
                    textValue: filter,
                    name: filter,
                    include: includeType,
                    criteria: includeType
                }
            })
        })
    } else if (filterType.toLowerCase() === 'organisationfilter') {
        removeAllButOrganisationVisitorTypes();
        for (let key in _quickExportSelection) {
            let filter = _quickExportSelection[key];
            createRawFilter({
                type: filterType,
                id: key,
                storedValue: {
                    id: key,
                    value: key,
                    textValue: filter,
                    name: filter,
                    include: includeType,
                    criteria: includeType
                }
            })
        }
    }
    browserHistory.push('/results');
    loadResults();
}

function setAutoExport(autoExport) {
    if (autoExport.checked) {
        _autoExports.push({
            report_template_id: autoExport.templateId,
        })
    } else {
        _autoExports = clone(_autoExports).filter(thisAutoExport => thisAutoExport.report_template_id !== autoExport.templateId)
    }
}

function removeAllButOrganisationVisitorTypes() {
    removeFilterByType({
        type: 'visitorTypeFilter',
        id: 3
    })
    removeFilterByType({
        type: 'visitorTypeFilter',
        id: 4
    })
    removeFilterByType({
        type: 'visitorTypeFilter',
        id: 5
    })
    removeFilterByType({
        type: 'visitorTypeFilter',
        id: 6
    })
}

function editTeamSettingsInStoreOnly(settings) {
    _allSettings.team = settings;
}

function reportRecord(visitData) {
    setLightbox({
        type: 'reportRecord',
        id: -1
    })
    _reportRecord.organisation = visitData.use_organisation_name;
    _reportRecord.visitData = visitData;
}

function startGocardlessProcess() {
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/gocardless/new-customer");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    let returnObj = JSON.parse(request.responseText);
                    window.location = returnObj.url;
                    break;
                default :
                    alert('Sorry, there was a problem creating your subscription');
                    break;
            }
        }
    }
    request.send(JSON.stringify({paymentType: _paymentType}));
}

function handleError(statusCode) {
    _accountOK = false;
    _accountStatus = statusCode;
}

function superUserIsAdmin() {
    if (1 === parseInt(_user.super_user_is_admin)) {
        return true;
    }
    return false;
}

function loadResults() {
    resetNoResultsReportData();
    _numberOfResultLoads++;
    _applicationIsResting = false;
    _blockedOrganisations = [];
    let request = new XMLHttpRequest();
    if (_currentRoute !== "results") {
        scroll(0, 0);
    }
    let sortedFilters = sortFilters(_filters);
    request.open("POST", "/api/proxy/api/v1/page/visits");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    let parsedResults = false;
                    try {
                        parsedResults = JSON.parse(request.responseText);
                    } catch (e) {
                        parsedResults = false;
                    }
                    if (!parsedResults) {
                        return false;
                    }
                    _results = parsedResults;
                    _paginationNumberOfPages = Math.ceil(_results.totals.total / _paginationDisplayOnPage);
                    _newFiltersApplied = false;
                    closeSnackbar();
                    _latestAddedFilters = [];
                    showFiltersInConsole();
                    break;
                case 403 :
                    handleError(403);
                    break;
                case 401 :
                    FilterActions.setLightbox({
                        type: 'attemptNewToken',
                        id: -1
                    });
                    break;
            }
            FilterActions.incrementFilterChange(true);
        }
    }
    request.send(JSON.stringify(sortedFilters));
}

function showFiltersInConsole() {
    if (superUserIsAdmin()) {
        let filtersLog = "--- START " + moment().format('Do MMMM YYYY, h:mm:ss a') + " ---<br />";
        filtersLog += "New Filters<br />";
        filtersLog += printPrettyFilters(_filters);
        filtersLog += "--- END ---<br /><br />";
        const request = new XMLHttpRequest();
        request.open("POST", "/api/proxy/api/v1/admin/log/filters");
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(JSON.stringify({log: filtersLog}));
    }
}

function printPrettyFilters(filters) {
    let returnString = "";
    for (let i in filters) {
        let filterObj = filters[i];
        if (filterObj) {
            if (typeof filterObj.type === "undefined") {
                returnString += "no type property on object<br />";
            } else {
                switch (filterObj.type.toLowerCase()) {
                    case "scenariofilter" :
                        returnString += "Scenario, " + filterObj.storedValue.textValue + "<br />";
                        returnString += printPrettyFilters(filterObj.storedValue.scenario.filters) + "<br />";
                        break;
                    default :
                        returnString += printPrettyFilter(filterObj) + "<br />";
                        break;
                }
            }
        }
    }
    ;
    return returnString;
}

function printPrettyFilter(filterObj, i) {
    let returnString = "";
    if (typeof filterObj.setID !== "undefined") {
        returnString += printPrettyScenarioFilter(filterObj);
    } else {
        returnString += printPrettyStandardFilter(filterObj);
    }
    return returnString;
}

function printPrettyScenarioFilter(filterObj) {
    let prettyPrintScenarioFilter = new PrettyPrintScenarioFilter;
    prettyPrintScenarioFilter.setFilter(filterObj);
    return prettyPrintScenarioFilter.printPrettyString();
}

function printPrettyStandardFilter(filterObj) {
    let prettyPrintStandardFilter = new PrettyPrintStandardFilter;
    prettyPrintStandardFilter.setFilter(filterObj);
    return prettyPrintStandardFilter.printPrettyString();
}

function setDynamicsCallbackURL(callbackURL) {
    _allSettings.team.dynamics_callback_url = callbackURL;
}

function setMicrosoftClarityProjectId(projectId) {
    _allSettings.team.clarity = projectId
}

function storeNewTagDescription() {
    closeLightbox();
    const request = new XMLHttpRequest();
    const newClientTypes = [];
    _clientTypes.forEach(function (tag, t) {
        if (tag.id === _editTag.ID) {
            tag.type = _editTag.description;
        }
        newClientTypes.push(tag);
    })
    _clientTypes = newClientTypes;
    if (_currentRoute !== 'results') {
        scroll(0, 0);
    }
    request.open('PUT', `/api/proxy/api/v1/tags/${_editTag.ID}`);
    request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    FilterActions.setSnackbar({
                        message: 'Tag updated'
                    })
                    break;
                case 500 :
                    FilterActions.setSnackbar({
                        message: 'Error updating tag'
                    })
            }
        }
    }
    request.send(JSON.stringify({
            description: _editTag.description
        })
    );
}

function updateResults() {
    _renewTokenDidSucceed = -1;
    _renewTokenMessage = "";
    closeLightbox();
    _applicationIsResting = false;
    setSnackbarInternal({
        message: 'Fetching Results ...'
    })
    loadResults();
    if (_currentRoute !== "results") {
        browserHistory.push('/results');
    }
}

function setNextResults() {
    let newFilterParams = sortFilters(_filters);
    newFilterParams.paginationOffset += _paginationReturn;
    ResultActions.setNextResults(newFilterParams);
}

function updateTagDescription(newDescription) {
    _editTag.description = newDescription;
}

function dateChangeUpdate() {
}

function openClientTypesLightbox(organisation) {
    setLightbox({
        type: 'clientTypes',
        id: 1
    })
    let relationships = [];
    if (organisation.relationships_json) {
        try {
            relationships[organisation.organisation_id] = JSON.parse(organisation.relationships_json);
        } catch (e) {
            relationships[organisation.organisation_id] = [];
        }
    }
    _clientTypesLightboxObj = {
        organisationID: organisation.organisation_id,
        relationships: relationships
    };
}

function showFiltersInLightbox(filters) {
    setLightbox({
        type: 'looseFiltersInLightbox',
        id: -1
    });
    let type = "";
    if (typeof filters[0] !== "undefined") {
        type = filters[0].type;
    }
    _looseFiltersToDisplayInLightbox = {
        filters: filters,
        type: type
    }
}

function editFilter(filterObj) {
    _paginationOffset = 0;
    _editingFilters = true;
    if (filterObj.type === "dateFrom" || filterObj.type === "dateTo") {
        _filters[filterObj.id] = assign({}, _filters[filterObj.id], filterObj);
        acknowledgeDateChange();
    } else {
        switch (filterObj.type) {
            case "timeFrom" :
                _filters[2] = {
                    round: 0,
                    storedValue: filterObj.storedValue,
                    type: 'timeFrom'
                }
                break;
            case "timeTo" :
                _filters[3] = {
                    round: 0,
                    storedValue: filterObj.storedValue,
                    type: 'timeTo'
                }
                break;
            case "minNumberOfPages" :
                if (typeof _filtersByTypeAsIndex.minnumberofpages !== "undefined" &&
                    typeof _filtersByTypeAsIndex.minnumberofpages[0] !== "undefined") {
                    _filtersByTypeAsIndex.minnumberofpages[0] = filterObj.storedValue.value;
                    replaceBasicFilter(filterObj);
                } else {
                }
                updateResults();
                break;
            case "maxNumberOfPages" :
                replaceBasicFilter(filterObj);
                if (typeof _filtersByTypeAsIndex.maxnumberofpages !== "undefined" &&
                    typeof _filtersByTypeAsIndex.maxnumberofpages[0] !== "undefined") {
                    _filtersByTypeAsIndex.maxnumberofpages[0] = filterObj.storedValue.value;
                    updateResults();
                }
                break;
            case "minSessionDuration" :
                if (typeof _filtersByTypeAsIndex.minsessionduration !== "undefined") {
                    _filtersByTypeAsIndex.minsessionduration[0] = filterObj.storedValue.value;
                    replaceBasicFilter(filterObj);
                    updateResults();
                }
                break;
            case "visitorTypeFilter" :
                replaceBasicFilter(filterObj);
                if (typeof _filtersByTypeAsIndex.visitortypefilter !== "undefined") {
                    if (Boolean(filterObj.storedValue.value)) {
                        if (_filtersByTypeAsIndex.visitortypefilter.indexOf(filterObj.id) === -1) {
                            _filtersByTypeAsIndex.visitortypefilter.push(filterObj.id);
                        }
                    } else {
                        let indexOfVisitorType = _filtersByTypeAsIndex.visitortypefilter.indexOf(filterObj.id);
                        if (indexOfVisitorType > -1) {
                            delete _filtersByTypeAsIndex.visitortypefilter[indexOfVisitorType];
                        }
                    }
                } else {
                }
                break;
            default :
                replaceBasicFilter(filterObj);
                break;
        }
    }
}

function replaceBasicFilter(newFilter) {
    let newFilters = [];
    let filtersClone = clone(_filters);
    let foundFilter = false;
    let filterIndex = 0;
    try {
        filtersClone.forEach(function (filter, i) {
            if (filter) {
                if (typeof filter.type !== "undefined" &&
                    filter.type.toLowerCase() === newFilter.type.toLowerCase() &&
                    parseInt(filter.storedValue.id) === parseInt(newFilter.storedValue.id)) {
                    foundFilter = true;
                    filter.storedValue.value = newFilter.storedValue.value;
                    filter.storedValue.textValue = newFilter.storedValue.textValue;
                }
                newFilters[i] = filter;
            }
        })
    } catch (e) {
        for (let filterI in filtersClone) {
            let filter = filtersClone[filterI];
            filterIndex++;
            if (filter) {
                if (typeof filter.type !== "undefined" &&
                    filter.type.toLowerCase() === newFilter.type.toLowerCase() &&
                    parseInt(filter.storedValue.id) === parseInt(newFilter.storedValue.id)) {
                    foundFilter = true;
                    filter.storedValue.value = newFilter.storedValue.value;
                    filter.storedValue.textValue = newFilter.storedValue.textValue;
                }
                newFilters[filterIndex] = filter;
            }
        }
    }
    if (!foundFilter) {
        let nextIndex = 1;
        for (let filterKey in newFilters) {
            nextIndex = filterKey;
        }
        newFilters[(nextIndex + 1)] = newFilter;
    }
    _filters = newFilters;
}

function setDateRange(dateRange) {
    editFilters(dateRange);
    closeLightbox();
    acknowledgeDateChange();
}

function setPaymentType(paymentType) {
    _paymentType = paymentType;
}

function storeLoadedTemplates(templates) {
    _templates = clone(templates);
    browserHistory.push('/saved-templates');
}

function setLoadedTemplates(templates) {
    _templates = templates;
}

function setCountries(countries) {
    _countries = countries;
}

function setReferrers(referrers) {
    _referrers = referrers;
}

function setClientPages(pages) {
    _clientPages = pages;
}

function setOrganisationInclusionType(inclusionType) {
    _inclusionType = inclusionType;
}

function setOrganisations(organisations) {
    _allOrganisations = organisations;
}

function addTagToMasterAnalyticsData(tagObj) {
    let tagTypeAlreadyExistsInAnalyticsObj = false;
    _analyticsData.tags.forEach(function (tag) {
        tagTypeAlreadyExistsInAnalyticsObj = tag[0] === tagObj.clientType;
    });
    if (!tagTypeAlreadyExistsInAnalyticsObj) {
        _analyticsData.tags.push([tagObj.clientType, tagObj.tagName]);
    }
}

function updateClientRelationship(data) {
    addTagToMasterAnalyticsData(data.filters);
    changeRelationshipStatus(data);
    const request = new XMLHttpRequest();
    request.open('POST', '/api/proxy/api/v1/euco-relationships');
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify({
            clientId: getClientId(),
            type: data.filters.clientType,
            organisationId: data.filters.organisationId,
            action: data.filters.action
        })
    );
}

function showOrganisationInLightbox(organisation) {
    if (!organisation) {
        return false;
    }
    _flatPageVisits = false;
    _showOrganisationInLightbox = organisation;
    const request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/page-visits/" + organisation.id);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            let newJSON;
            try {
                newJSON = JSON.parse(request.responseText);
            } catch (e) {
                newJSON = false;
            }
            if (newJSON) {
                FilterActions.setFlatPageVisits(JSON.parse(request.responseText));
            }
        }
    };
    request.send();
    setLightbox({
        type: 'showOrganisationInLightbox',
        id: -1
    })
}

function updateOrganisationAnalyticsData(relationship) {
    let relationships = clone(_analyticsData.organisationRelationships);
    let create = false;
    if (typeof relationships[parseInt(relationship.organisationId)] !== "undefined" &&
        relationships[parseInt(relationship.organisationId)]) {
        let indexOf = relationships[parseInt(relationship.organisationId)].indexOf(parseInt(relationship.clientType));
        if (indexOf > -1) {
            delete relationships[parseInt(relationship.organisationId)][indexOf];
        } else {
            create = true;
        }
    } else {
        create = true;
        relationships[parseInt(relationship.organisationId)] = [];
    }
    if (create) {
        relationships[parseInt(relationship.organisationId)].push(parseInt(relationship.clientType));
    }
    _analyticsData.organisationRelationships = relationships;
    _clientTypesLightboxObj.relationships[parseInt(relationship.organisationId)] = relationships[parseInt(relationship.organisationId)];
}

function quickExportExportNewOrganisationsFormat(selection) {
    setSelectionNewOrganisationsFormat(selection);
    startQuickExport("organisationFilter");
}

function setSelectionNewOrganisationsFormat(selection) {
    let newSelection = {};
    if (selection) {
        selection.forEach(function (orgIndex, i) {
            let org = _newFormattedOrganisations[orgIndex];
            newSelection[org.organisation_id] = org.name;
        });
        _quickExportSelection = newSelection;
    }
}

function quickExportShowDataNewOrganisationsFormat(selection) {
    setSelectionNewOrganisationsFormat(selection);
    quickExportShowData('organisationFilter');
}

function changeRelationshipStatus(relationship) {
    relationship = relationship.filters;

    if (typeof _results.relationships[parseInt(relationship.organisationId)] !== 'undefined') {
        let indexOf = _results.relationships[parseInt(relationship.organisationId)].indexOf(parseInt(relationship.clientType));
        if (relationship.action === 'delete') {
            if (indexOf >= 0) {
                delete _results.relationships[parseInt(relationship.organisationId)][indexOf];
            }
        } else {
            _results.relationships[parseInt(relationship.organisationId)].push(parseInt(relationship.clientType));
        }
    } else {
        _results.relationships[parseInt(relationship.organisationId)] = [parseInt(relationship.clientType)]
    }

    updateOrganisationAnalyticsData(relationship);
}

function storeInvoices(invoices) {
    _invoices = invoices;
}

function createNewTag(tagData) {
    if (_currentRoute === "organisations") {
        setLightbox({
            type: 'clientTypes',
            id: 1
        });
    } else {
        closeLightbox();
    }
    _applicationIsResting = false;
    let newTag = {
        id: 'a1optimistic' + tagData.tag.tag,
        type: tagData.tag.tag,
        name: tagData.tag.tag
    };
    _clientTypes.push(newTag);
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/tags");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _applicationIsResting = true;
            let response = JSON.parse(request.responseText);
            setSnackbarInternal({
                message: "Tag successfully created"
            });
            FilterActions.setNewClientTypes(response.relationships);
        }
    };
    request.send(JSON.stringify({tag: tagData.tag.tag, clientId: _clientId}));
}

function saveAutoTags() {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/trigger-actions/autotags");
    xhr.setRequestHeader('Content-Type', 'application/json');
    const triggerActionAutotags = {
        reportID: _editTriggerActionsForTemplateID,
        tags: _autoTags
    }
    xhr.send(JSON.stringify(triggerActionAutotags));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    FilterActions.setSnackbar({
                        message: "Changes successfully saved"
                    });
                    break;
                default :
                    alert("An error occurred, please try again");
                    break;
            }
        }
    }
}

function saveChangesToTriggerActionEmailAlerts() {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/proxy/api/v1/trigger-actions/emails');
    xhr.setRequestHeader('Content-Type', 'application/json');

    const triggerActionEmailAlertsObj = {
        reportID: _editTriggerActionsForTemplateID,
        teamMembers: _triggerActionEmailAlertTeamMembers,
    }
    xhr.send(JSON.stringify(triggerActionEmailAlertsObj));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    FilterActions.setSnackbar({
                        message: 'Changes successfully saved'
                    });
                    break;
                default :
                    alert('An error occurred, please try again');

                    break;
            }
        }
    }
}

function setClientTypes(clientTypes) {
    const clientTypesByIndex = {};
    clientTypes.forEach(function (clientType, ct) {
        clientTypesByIndex[clientType.id] = clientType;
    })
    _clientTypesByIndex = clientTypesByIndex;
    _clientTypes = clientTypes;
}

function setTeamMembers(members) {
    _teamMembers = members;
}

function setSnackbarInternal(messageObj) {
    let msg = (typeof messageObj.message !== "undefined") ? messageObj.message : '';
    _snackbarMsg = msg;
    _snackbarComponent = (typeof messageObj.component !== "undefined") ? messageObj.component : false;
    _snackbarOtherData = msg;
    _snackbarOpen = true;
}

function setSnackbar(messageObj) {
    _snackbarMsg = messageObj.message.message;
    _snackbarComponent = (typeof messageObj.message.component !== "undefined") ? messageObj.message.component : false;
    _snackbarOtherData = messageObj.message;
    _snackbarOpen = true;
}

function addNewTagAsync(tagData) {
    updateClientTypes();
}

function setDisplayByFilter(displayByFilter) {
    _displayBy = parseInt(displayByFilter.displayBy.val);
}

function loadReportFilters(reportID) {
    _applicationIsResting = false;
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/internal/load-report/" + reportID);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.setFiltersAndLoad(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function setDisplayGCLID(displayGCLID) {
    _allSettings.team.display_gclid = (displayGCLID) ? 1 : 0;
    updateTeamSettings();
}

function startExport() {
    setSnackbarInternal({
        message: 'Generating Report ...'
    });
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/internal/export");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
    };
    let filters = sortFilters(clone(_filters));
    filters.actionType = 'Exporting';
    let exportCriteria = clone(_exportCriteria);
    let cache = [];
    let sendObj = {
        exclude: clone(_excludeSelection),
        exportCriteria: clone(exportCriteria),
        filters: clone(filters),
        rawFilters: clone(_filters)
    }
    let jsonToSend = JSON.stringify(sendObj, function (key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                return;
            }
            cache.push(value);
        }
        return value;
    });
    request.send(jsonToSend);
    cache = null;
}

function openExport() {
    browserHistory.push('/export');
}

function setFiltersAndLoad(filters) {
    _applicationIsResting = false;
    if (!filters) {
        return false;
    }
    let newFilters = [];
    for (let key in filters) {
        newFilters.push(filters[key]);
    }
    _filters = newFilters;
    setInitialPayloadOnReportLoad();
    setInitialFiltersByTypeAndValue();
    updateAnalytics(true);
    loadResults();
}

function saveTeamSettings() {
    updateTeamSettings();
}

function makePayment() {
    setSnackbarInternal({
        message: 'Registering Payment ...'
    });
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/gocardless/make-payment");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
    };
    request.send(JSON.stringify({
            paymentType: _paymentType
        })
    );
}

function setQuickReportDateRange(dateRange) {
    _quickReportDateRange = dateRange;
}

function setQuickReportTrafficType(trafficType) {
    _quickReportTrafficType = trafficType;
}

function setQuickReportDataType(dataType) {
    _quickReportDataType = dataType;
}

function setPreviousVisitsSelection(selection) {
    _previousVisits.selection = selection;
}

function updateAnalyticsOrganisationDataNewFormat(activeRecord) {
    closeLightbox();
    ajaxUpdateEndUserCompanyOrganisationData();
    loadNewFormatOrganisations();
}

function updateAnalyticsOrganisationData() {
    let activeRecord = clone(_updatedActiveRecord);
    if (typeof activeRecord.newFormat !== "undefined") {
        updateAnalyticsOrganisationDataNewFormat(activeRecord);
        return false;
    }
    if (typeof _analyticsData.organisations[activeRecord.myKey] !== "undefined") {
        let recordOK = true;
        try {
            _analyticsData.organisations[activeRecord.myKey].name = activeRecord.data.use_organisation_name;
        } catch (e) {
            recordOK = false;
            if (_ravenInstalled) {
                Raven.captureMessage('Edit organisation record errro', {tags: _analyticsData});
            }
        }
        if (recordOK) {
            _analyticsData.organisations[activeRecord.myKey].tel = activeRecord.data.use_organisation_tel;
            _analyticsData.organisations[activeRecord.myKey].website = activeRecord.data.use_organisation_website;
        }
    }
    ajaxUpdateEndUserCompanyOrganisationData();
    closeLightbox();
    FilterActions.setSnackbar({
        message: "Organisation data updated"
    })
}

function saveEndUserCompanyCompanyData() {
    FilterActions.setSnackbar({
        message: 'Thank you, your copy of the company data has been saved'
    });
    _results.results[_updatedActiveRecord.myKey] = _updatedActiveRecord.data;
    ajaxUpdateEndUserCompanyOrganisationData();
}

function logActivity(activity) {
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/user-activity");
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify({
            actionType: activity
        })
    );
}

function ajaxUpdateEndUserCompanyOrganisationData() {
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/end-user-company/organisation");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _updatedActiveRecordChosenOption = -1;
            _updatedActiveRecordPossibleOptions = [];
            _checkedForActiveRecordPossibleOptions = false;
        }
    };
    request.send(JSON.stringify({
            sessionID: _updatedActiveRecord.data.main_session_id,
            companyId: _updatedActiveRecord.data.organisationid,
            name: _updatedActiveRecord.data.use_organisation_name,
            tel: _updatedActiveRecord.data.use_organisation_tel,
            website: _updatedActiveRecord.data.use_organisation_website,
            linkedIn: "",
            newOrganisationID: _updatedActiveRecordChosenOption
        })
    );
}

function removeFlashMessages() {
    _flashMessages = [];
}

function setCompanyDataOpen(organisationData) {
    let organisationId = organisationData.data;
    _companyDataOpen[organisationId] = true;
}

function setCompanyDataClose(organisationData) {
    let organisationId = organisationData.data;
    _companyDataOpen[organisationId] = false;
}

function getAllBrowsers() {
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/browsers");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeAllBrowsers(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function setAllBrowsers(browsers) {
    _allBrowsers = browsers;
}

function setAllOperatingSystems() {
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/operating-systems");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeAllOperatingSystems(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function storeAllOperatingSystems(operatingSystems) {
    _operatingSystems = operatingSystems;
}

function closeSnackbar() {
    _snackbarOpen = false;
}

function setCountryInclusionType(inclusionType) {
    _countryInclusionType = inclusionType.data.exclude;
}

function mergeOptions(obj1, obj2) {
    let obj3 = {};
    for (let attrname in obj1) {
        obj3[attrname] = obj1[attrname];
    }
    for (let attrname in obj2) {
        obj3[attrname] = obj2[attrname];
    }
    return obj3;
}

function loadPrices() {
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/end-user-company/pricing");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _applicationIsResting = true;
            FilterActions.storePricing(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function deleteTag(tagID) {
    const newTags = [];
    _clientTypes.forEach(function (tag, t) {
        if (tag.id !== tagID) {
            newTags.push(tag);
        }
    })
    _clientTypes = newTags;
    const request = new XMLHttpRequest();
    request.open("DELETE", "/api/proxy/api/v1/tags/" + tagID);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.setSnackbar({
                message: "Tag Removed"
            })
        }
    };
    request.send();
}

function resetFiltersOnlyAndNoAction() {
    resetToOriginalFilters();
}

function findMatchingOptions(searchCriteria) {
    _currentSearchCriteria = searchCriteria;
    _currentSearchCriteria.noSearchResults = false;
    let request = new XMLHttpRequest();
    searchCriteria.includeType = _customFilterIncludeOrExclude;
    let newSearchCriteria = mergeOptions(searchCriteria, sortFilters(_filters));
    request.open("POST", "/api/proxy/api/v1/search-hinting");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            setSearchByLoadingStatus(false, searchCriteria.searchString);
            let results = JSON.parse(request.responseText);
            setNarrowedOptions(results);
            changeLoadingStatusInternal({
                message: '',
                loading: false
            })
            FilterActions.incrementFilterChange();
        }
    };
    request.send(JSON.stringify(newSearchCriteria));
}

function storeWebsocketMsg(msg) {
    _websocketMessages.push(msg);
}

function quickCreateReport() {
    browserHistory.push('/create-template');
}

function setSearchByLoadingStatus(setBoolean, searchString) {
    if (setBoolean) {
        _searchByLoadingStatus = "loading";
    } else {
        _searchByLoadingStatus = "ready";
    }
    FilterActions.incrementFilterChange();
}

function narrowSearchOptions(searchCriteria) {
    searchCriteria = searchCriteria.data;
    _currentSearchCriteria.type = searchCriteria.type;
    setSearchByLoadingStatus(true, searchCriteria.searchString);
    if (searchCriteria.searchString.length > 2) {
        findMatchingOptions(searchCriteria);
    }
}

function removeFiltersByType(filters) {
    filters.map(function (filter, i) {
        deleteFilterByType(filter);
    })
    return;
    let filterType = filters[0].type;
    let countFilterType = getFilterCountByType(filterType);
    if (countFilterType === 0) {
        FilterActions.setSearchBySelectAll('');
        FilterActions.setAllSelected({
            type: filterType,
            allSelected: false
        })
    }
}

function deleteFilterByType(filter) {
    if (typeof filter.type === "undefined") {
    } else {
        if (typeof _filtersByTypeAsIndex === "undefined") {
        } else if (typeof _filtersByTypeAsIndex[filter.type.toLowerCase()] !== "undefined") {
            let indexOf = _filtersByTypeAsIndex[filter.type.toLowerCase()].indexOf(filter.id);
            if (indexOf > -1) {
                delete _filtersByTypeAsIndex[filter.type.toLowerCase()][indexOf];
            } else {
                if (typeof filter.storedValue !== "undefined") {
                    indexOf = _filtersByTypeAsIndex[filter.type.toLowerCase()].indexOf(filter.storedValue.id);
                    delete _filtersByTypeAsIndex[filter.type.toLowerCase()][indexOf];
                }
            }
        }
    }
    const newFilters = clone(_filters);
    if (Array.isArray(newFilters)) {
        newFilters.forEach(function (savedFilter, i) {
            if (typeof savedFilter !== "undefined") {
                if (savedFilter.type.toLowerCase() === filter.type.toLowerCase()) {
                    if (
                        (parseInt(savedFilter.storedValue.value) === parseInt(filter.id) ||
                            savedFilter.storedValue.id === filter.id) ||
                        (typeof filter.storedValue !== "undefined" && savedFilter.storedValue.id ===
                            filter.storedValue.id)
                    ) {
                        delete newFilters[i];
                    }
                }
                if (savedFilter.type.toLowerCase() === "setsessionids" && filter.type.toLowerCase() === "setsessionids") {
                    delete newFilters[i];
                }
            }
        });
    }
    _filters = newFilters;
}

function removeFilterByType(filter) {
    const filterType = filter.type;
    if (_looseFiltersToDisplayInLightbox) {
        if (_looseFiltersToDisplayInLightbox.type.toLowerCase() === filter.type.toLowerCase()) {
            let newLooseFiltersToDisplay = [];
            _looseFiltersToDisplayInLightbox.filters.forEach(function (looseFilter, i) {
                if (looseFilter.storedValue.id !== filter.id) {
                    newLooseFiltersToDisplay.push(looseFilter);
                }
            })
            _looseFiltersToDisplayInLightbox.filters = newLooseFiltersToDisplay;
        }
    }
    deleteFilterByType(filter);
    if (filterType.toLowerCase() === "setsessionids") {
        _filters = _lastSetOfFilters;
        setInitialFiltersByTypeAndValue();
        updateResults();
    }
}

function reportNoResults() {
    let sendObj = {
        filters: clone(_filters),
        sortedFilters: sortFilters(clone(_filters)),
        msg: _noResultsMsg
    }
    _noResultsReportSent = true;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/report/no-results");
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(sendObj));
}

function getFilterCountByType(filterType) {
    let count = 0;
    _filters.forEach(filter => {
        if (typeof filter.storedValue.searchBox !== "undefined" &&
            filter.storedValue.searchBox) {
            count++;
        } else if (filter.type.toLowerCase() === filterType.toLowerCase()) {
            count++;
        }
    })
    return count;
}

function resetNoResultsReportData() {
    _noResultsReportSent = false;
    _noResultsMsg = "";
    _showReportNoResultsBox = false;
}

function resetNarrowedOptions() {
    _narrowedSearchOptions = [];
}

function setNarrowedOptions(options) {
    if (typeof options === "undefined" || !options) {
        return false;
    }
    if (options.length === 0) {
        _currentSearchCriteria.noSearchResults = true;
    }
    _narrowedSearchOptions = options;
}

function openSetAutomation(template) {
    template = template.data;
    setLightbox({
        type: 'newAutomatedReport',
        id: 1
    });
    _setAutomationSettings = {
        template: template
    }
}

function closeSetAutomation() {
    _setAutomationSettings = {
        open: false,
        template: null
    }
}

function testJestStore() {
    _storeTest = 23;
}

function resetSelectedTeamMembers() {
    _exportCriteria.includeTeamMembers = [];
}

function orgHasNoImage(orgID) {
    let newResults = clone(_results.results);
    newResults.forEach(function (result) {
        if (orgID === result.organisationid) {
            result.has_image = 0;
        }
    })
    _results.results = newResults;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/organisations/no-image");
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
            orgID: orgID
        })
    );
}

function updateAutomatedDelivery(automationSettings) {
    closeLightbox();
    setSnackbarInternal({
        message: "Thank you, the automated report has been updated"
    })
    automationSettings = automationSettings.data;
    _exisitingAutomatedReport.data.report.fireDate = automationSettings.fireDate;
    _exisitingAutomatedReport.data.report.fireTime = automationSettings.fireTime;
    _exisitingAutomatedReport.data.report.repeat = automationSettings.repeat;
    _exisitingAutomatedReport.data.report.name = automationSettings.title;
    let date = automationSettings.fireDate.toMysqlFormat();
    let time = automationSettings.fireTime.toMysqlTime();
    let newAutomationSettings = {
        date: date,
        time: time,
        pdf: automationSettings.exportPDF,
        csv: automationSettings.exportCSV,
        repeat: automationSettings.repeat,
        teamMembers: _exportCriteria.includeTeamMembers,
        includeFields: _exportCriteria.include,
        title: automationSettings.title,
        templateId: automationSettings.templateId,
        reportId: automationSettings.reportId,
        showFullURLs: _exisitingAutomatedReport.data.report.show_full_urls_on_entry_pages
    }
    closeExistingAutomatedReport();
    let request = new XMLHttpRequest();
    request.open("PUT", "/api/proxy/api/v1/automated-reports/" + automationSettings.reportId);
    request.setRequestHeader('Content-Type', 'application/json');
    let cache = [];
    request.send(JSON.stringify(newAutomationSettings, function (key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    return;
                }
                cache.push(value);
            }
            return value;
        })
    );
}

function removeAllFiltersByType(type) {
    _filters.forEach(function (filter, i) {
        if (filter.type.toLowerCase() === type.toLowerCase()) {
            delete _filters[i];
        }
    });
}

function quickLink(quickLinkObj) {
    let needUpdate = false;
    _numberOfResultLoads = 1;
    switch (quickLinkObj.viewType) {
        case 5:
            browserHistory.push('/results');
            break;
        case 11:
            browserHistory.push('/results');
            break;
        case 12:
            browserHistory.push('/results');
            break;
        case 13:
            browserHistory.push('/results');
            break;
        case 14:
            browserHistory.push('/results');
            break;
        case 15:
            browserHistory.push('/results');
            break;
        case 6:
            browserHistory.push('/organisations');
            break;
        case 7:
            browserHistory.push('/keywords');
            break;
        case 8:
            browserHistory.push('/referrers');
            break;
        case 9:
            browserHistory.push('/all-pages');
            break;
        case 10:
            browserHistory.push('/entry-pages');
            break;
    }
    if (quickLinkObj.viewType === 5) {
        needUpdate = true;
    }
    resetAllButDateFilters();
    _applicationIsResting = false;
    _results = clone(_resultsOriginalState);
    if (typeof quickLinkObj.removeFilterByTypeAndID !== "undefined") {
        removeFilterByType(quickLinkObj.removeFilterByTypeAndID);
    }
    if (typeof quickLinkObj.editedFilters !== "undefined" && quickLinkObj.editedFilters && quickLinkObj.editedFilters.length > 0) {
        if (quickLinkObj.editedFilters[0].storedValue !== _filters[0].storedValue) {
            needUpdate = true;
        }
        if (quickLinkObj.editedFilters[1].storedValue !== _filters[1].storedValue) {
            needUpdate = true;
        }
        editFilters(quickLinkObj.editedFilters);
    }
    if (typeof quickLinkObj.createFilters !== "undefined" && quickLinkObj.createFilters.length > 0) {
        if (quickLinkObj.createFilters.length > -1) {
            needUpdate = true;
        }
        createRawFilters(quickLinkObj.createFilters, true);
    }
    if (needUpdate) {
        setSnackbarInternal({
            message: "Loading Results ..."
        })
        clearTimeout(_keyboardPressLapse);
        _keyboardPressLapse = setTimeout(function () {
            _paginationOffset = 0;
            let filterParams = sortFilters(_filters);
            showFiltersInConsole();
            let request = new XMLHttpRequest();
            request.open("POST", "/api/proxy/api/v1/events/quick-link");
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    switch (request.status) {
                        case 200 :
                            _loadingDateChange = false;
                            let returnData = false;
                            try {
                                returnData = JSON.parse(request.responseText);
                            } catch (e) {
                                returnData = false;
                            }
                            if (returnData &&
                                (typeof returnData.error === "undefined" || !returnData.error) &&
                                typeof returnData.results !== undefined) {
                                closeSnackbar();
                                scroll(0, 0);
                                _applicationIsResting = true;
                                _results = returnData.results;
                                _paginationNumberOfPages = Math.ceil(_results.totals.total / _paginationDisplayOnPage);
                                storeAnalyticsData(returnData.analytics);
                                _newFiltersApplied = false;
                                _editingFilters = false;
                                _latestAddedFilters = [];
                                FilterActions.incrementFilterChange();
                            }
                            break;
                        case 401 :
                            FilterActions.setLightbox({
                                type: 'attemptNewToken',
                                id: -1
                            });
                            break;
                        case 402 :
                            handleError(402);
                            let errMessage = "Sorry, there is an issue with your account, " +
                                "please check that " +
                                "your subscription is up to date";
                            _applicationIsResting = true;
                            if (window.location.pathname !== "/billing") {
                                alert(errMessage);
                            }
                            break;
                    }
                }
            };
            let sendObj = clone(filterParams);
            request.send(JSON.stringify(sendObj));
        }, 0);
    } else {
        _applicationIsResting = true;
    }
}

function setUseGeolocation(useGeolocation) {
    _allSettings.team.use_geolocation = (useGeolocation) ? 1 : 0;
    updateTeamSettings();
}

function updateTeamSettings() {
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/settings/team");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.setSnackbar({
                message: 'Settings updated'
            })
        }
    };
    request.send(JSON.stringify(_allSettings.team));
}

function createAutomatedDelivery(payload) {
    FilterActions.closeLightbox();
    FilterActions.setSnackbar({
        message: "Thank you, the automated report has been successfully created"
    })
    let automationSettings = payload.data;
    let date = automationSettings.fireDate.toMysqlFormat();
    let time = automationSettings.fireTime.toMysqlTime();
    let newAutomationSettings = {
        actionType: 'Creating Automated Delivery',
        date: date,
        time: time,
        pdf: automationSettings.exportPDF,
        csv: automationSettings.exportCSV,
        repeat: automationSettings.repeat,
        sendToAll: automationSettings.sendToAll,
        teamMembers: _exportCriteria.includeTeamMembers,
        includeFields: _exportCriteria.include,
        title: automationSettings.title,
        templateId: _setAutomationSettings.template.template.id,
        showFullURLs: _exportCriteria.showFullURLOnEntryPage
    }
    let request = new XMLHttpRequest();
    request.open('POST', '/api/proxy/api/v1/automated-reports');
    request.setRequestHeader('Content-Type', 'application/json');
    FilterActions.setFlashMessage({
        type: 'success',
        msg: 'Thank you, the automated report has been created successfully'
    })
    const cache = [];
    const safeJSON = JSON.stringify(newAutomationSettings, function (key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                return;
            }
            cache.push(value);
        }
        return value;
    });
    request.send(safeJSON);
}

function storeUserCommunicationPreferences(preferences) {
    _userCommunicationPreferences = preferences;
}

function updateReportData(reportData) {
    _reportRecord = reportData
}

function updateActiveRecordNewFormat(updatedRecord) {
    _updatedActiveRecord = clone(updatedRecord);
}

function updateActiveRecord(updatedRecord) {
    if (typeof updatedRecord.newFormat !== "undefined") {
        updateActiveRecordNewFormat(updatedRecord);
        return false;
    }
    let currentName = '';
    if (typeof _results.results !== "undefined" &&
        typeof _results.results[_updatedActiveRecord.myKey] !== "undefined" &&
        typeof _results.results[_updatedActiveRecord.myKey].use_organisation_name !== "undefined") {
        currentName = _results.results[_updatedActiveRecord.myKey].use_organisation_name;
    } else {
        _checkedForActiveRecordPossibleOptions = false;
    }
    let newName = updatedRecord.data.use_organisation_name;
    if (currentName !== newName) {
        _checkedForActiveRecordPossibleOptions = false;
    }
    _updatedActiveRecord = clone(updatedRecord);
}

function storeTrackerCode(trackerCode) {
    _trackerCode = trackerCode;
}

function setTrackedOrganisationData(organisation) {
    if (typeof organisation.organisation_id !== "undefined") {
        organisation.organisationid = organisation.organisation_id;
    }
    _currentTrackingObj = organisation;
    setLightbox({
        type: 'assignToTeamMember',
        id: 1
    })
}

function loadTrackerCode() {
    const request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/get-tracker-code");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            let trackerCode = false;
            try {
                trackerCode = JSON.parse(request.responseText);
            } catch (e) {
                trackerCode = false;
            }
            if (trackerCode) {
                FilterActions.storeTrackerCode(JSON.parse(request.responseText));
            }
        }
    };
    request.send();
}

function loadExclusions() {
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/tags/exclusions");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeExclusions(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function createNewTrackedOrganisation(payload) {
    setLightbox({
        type: 'assignToTeamMember',
        id: 1
    })
    let resultKey = payload.data;
    let result = _results.results[resultKey];
    _results.results[resultKey].is_tracked = 1;
    result.myKey = resultKey;
    _currentTrackingObj = result;
}

function removeTrackedOrganisation(key) {
    let res = _results.results[key];
    _results.results[key].is_tracked = 0;
    let request = new XMLHttpRequest();
    request.open("DELETE", "/api/proxy/api/v1/track-organisation/" + res.organisationid);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.setSnackbar({
                message: "Tracked organisation removed"
            })
        }
    };
    request.send();
}

function updateExportCriteria(sortedList) {
    _exportCriteria.include = sortedList.data;
}

function viewExistingAutomatedReports(templateObj) {
    setLightbox({
        type: 'viewReportsForTemplate',
        id: 1
    });
    let templateData = templateObj.data;
    _existingAutomatedReportsData.open = true;
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/automated-reports/" + templateData.template.id);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.setExistingAutomatedReportData(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function setExistingReportData(reportData) {
    _existingAutomatedReportsData.reports = reportData.data;
}

function closeExistingAutomatedReports() {
    _existingAutomatedReportsData.open = false;
}

function updateTriggerActionEmailSendToList(sendToData) {
    _triggerActionEmailAlertTeamMembers[sendToData.teamMemberID] = sendToData.isChecked;
}

function openViewEditAutomatedReport(reportData) {
    reportData = reportData.data;
    _exisitingAutomatedReport.data = reportData;
    _exportCriteria.includeTeamMembers = reportData.users;
    if (reportData.types.indexOf(1) > -1) {
        _exportCriteria.exportTypes.CSV = true;
    }
    if (reportData.types.indexOf(2) > -1) {
        _exportCriteria.exportTypes.PDF = true;
    }
}

function closeExistingAutomatedReport() {
    _exisitingAutomatedReport.open = false;
}

function setAllSelected(allSelected) {
    let selectedObj = allSelected.data;
    _allSelected[selectedObj.type.toLowerCase()] = selectedObj.allSelected;
}

function setCRMExportSelection(selection) {
    _crmExportSelection = selection;
}

function removeItemFromStore(automatedReportID) {
    let automatedReports = clone(_existingAutomatedReportsData.reports);
    let newAutomatedReports = [];
    automatedReports.forEach(function (automatedReport, i) {
        if (automatedReport.reportData.id !== automatedReportID) {
            newAutomatedReports.push(automatedReport);
        }
    });
    _existingAutomatedReportsData.reports = newAutomatedReports;
}

function removeExistingReport(reportData) {
    _applicationIsResting = false;
    let reportId = reportData.data.reportId;
    removeItemFromStore(reportData.data.reportId)
    let request = new XMLHttpRequest();
    request.open("DELETE", "/api/proxy/api/v1/automated-reports/" + reportId);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    _applicationIsResting = true;
                    FilterActions.setStateUpdated();
                    break;
            }
        }
    };
    request.send();
    closeExistingAutomatedReports();
}

function closeLightbox() {
    _lightboxSettings = {type: null, id: null};
    _loadingAdditionalCompanyData = true;
    _existingAutomatedReportsData = {
        open: false,
        reports: []
    }
}

function setLightbox(lightboxObj) {
    let lightboxSettings;
    if (typeof lightboxObj.data !== "undefined") {
        lightboxSettings = lightboxObj.data;
    } else {
        lightboxSettings = lightboxObj;
    }
    _lightboxSettings.type = lightboxSettings.type;
    _lightboxSettings.id = lightboxSettings.id;
}

function loadMorePreviousVisits() {
    _loadingPreviousHistory = true;
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/organisation-session-history");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _loadingPreviousHistory = false;
            FilterActions.storePreviousHistory(JSON.parse(request.responseText));
        }
    };
    request.send(JSON.stringify({
            organisationID: _previousVisits.organisationID,
            offset: _previousVisits.offset
        })
    );
}

function loadResultsIfNoPreviousLoads() {
    _applicationIsResting = false;
    if (_numberOfResultLoads === 0) {
        loadResults();
    }
}

function setSalesforceID(salesforceID) {
    _allSettings.team.salesforce_organisation_id = salesforceID;
}

function setZohoKey(zohoKey) {
    _allSettings.team.zoho_auth_token = zohoKey;
}

function setPreviousHistory(prevHistoryObj) {
    setLightbox({
        type: 'previousVisits',
        id: 1
    })
    _lastSetOfFilters = clone(_filters);
    _loadingPreviousHistory = true;
    prevHistoryObj = prevHistoryObj.data;
    _previousHistoryOrganisation = prevHistoryObj.organisationName;
    _previousVisits = {
        selection: [],
        loading: false,
        offset: 0,
        allFound: [],
        organisationID: prevHistoryObj.organisationId
    }
    let request = new XMLHttpRequest();
    request.open('POST', '/api/proxy/api/v1/organisation-session-history');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _loadingPreviousHistory = false;
            let history;
            try {
                history = JSON.parse(request.responseText);
            } catch (e) {
                history = false;
            }
            if (history) {
                FilterActions.storePreviousHistory(history);
            }
        }
    };
    request.send(JSON.stringify({
            organisationID: prevHistoryObj.organisationId,
            ipAddress: prevHistoryObj.ipAddress,
            offset: _previousVisits.offset
        })
    );
}

function showOrganisationInLightboxAndSetIndex(rowData) {
    showOrganisationInLightbox(rowData.organisation);
    _currentRowOrganisationIndex = rowData.row;
}

function storePreviousHistory(prevHistoryObj) {
    _previousHistory = prevHistoryObj.data;
    _previousVisits.offset += 100;
    let existingArray = clone(_previousVisits.allFound);
    prevHistoryObj.data.map(function (newArray, i) {
        existingArray = existingArray.concat(newArray);
    });
    _previousVisits.allFound = existingArray;
}

function setSearchBySelectAll(selectAllObj) {
    _selectAllSearchByType = selectAllObj.data;
}

function storeNewClientSecret(newSecret) {
    _clientSecret = newSecret;
}

function setUser(user) {
    _user = user;
}

function saveThirdPartyAppSettings() {
    _updatingBox.updating = true;
    _updatingBox.component = 'thirdPartyKeys';
    _updatingBox.message = 'Updating Keys';
    let request = new XMLHttpRequest();
    request.open('POST', '/api/proxy/api/v1/settings/third-party-apps');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            setSnackbarInternal({
                message: 'Credentials updated',
            })
            FilterActions.resetUpdatingBox();
        }
    };
    request.send(JSON.stringify({
            key: _allSettings.team.zoho_auth_token,
            salesforce: _allSettings.team.salesforce_organisation_id,
            clarity: _allSettings.team.clarity,
        })
    );
}

function setAllSettings(settings) {
    if (settings.team) {
        _allSettings.team = settings.team;
    }
    if (settings.user) {
        _allSettings.user = settings.user;
    }
    if (settings.hooks) {
        _allSettings.hooks = settings.hooks;
    }
    _allSettings.agentConfig = {
        logo: _agentLogo,
        name: _agentName,
        id: _agentID
    }
}

function setDefaultFilters(defaultFilters) {
    if (defaultFilters.length === 0) {
        return false;
    }
    setSnackbarInternal({
        message: 'Applying default filters'
    })
    const newFilters = [];
    const keep = [
        'datefrom',
        'dateto',
        'timefrom',
        'timeto'
    ];
    for (let i in _filters) {
        if (keep.indexOf(_filters[i].type.toLowerCase()) > -1) {
            newFilters.push(_filters[i]);
        }
    }
    for (let i in defaultFilters) {
        newFilters.push(defaultFilters[i]);
    }
    _filters = newFilters;
    setInitialFiltersByTypeAndValue();
    if (_currentRoute === '') {
        updateResults();
    }
}

function submitReportRecord() {
    closeLightbox();
    _reportRecord.userID = _user.id;
    const request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/organisations/report-record");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    _reportRecord = {
                        sendFeedbackWhenResolved: false,
                        reasonForReporting: false,
                        visitData: null,
                        organisation: '',
                        userID: -1
                    }
                    FilterActions.setSnackbar({
                        message: "Record reported"
                    })
                    break;
            }
        }
    }
    request.send(JSON.stringify({reportData: _reportRecord}));
}

function updateVATNumber(VATNumber) {
    _allSettings.team.vat_number = VATNumber;
}

function applicationNotResting() {
    _applicationIsResting = false;
}

function setOrgsDatatableWidth() {
    if (document.getElementById("recentCompaniesDiv")) {
        _organisationsDatatableWidth = (document.getElementById("recentCompaniesDiv").offsetWidth - 70);
    }
}

function setInitialPayloadOnReportLoad() {
    setOrgsDatatableWidth();
    const request = new XMLHttpRequest();
    request.open('POST', '/api/proxy/internal/application/load');
    request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    request.onreadystatechange = () => {
        if (4 === request.readyState) {
            editFilter({
                id: 0,
                storedValue: {
                    id: 0,
                    name: 0,
                    value: 0,
                    textValue: 0
                },
                type: 'firstLoad',
                round: 0
            });
            let applicationData, errMessage
            switch (request.status) {
                case 200 :
                    applicationData = JSON.parse(request.responseText);
                    _agentLogo = applicationData.agentData.logo_filename;
                    _agentName = applicationData.agentData.name;
                    _agentID = applicationData.agentData.id;
                    if (typeof applicationData.agentData.lang !== "undefined" &&
                        applicationData.agentData.lang) {
                        _lang = JSON.parse(applicationData.agentData.lang);
                    }
                    _allSettings.agentConfig.id = applicationData.agentData.id;
                    _allSettings.agentConfig.contact = applicationData.agentData.contact_email;
                    setLoadedTemplates(applicationData.templates);
                    setClientTypes(applicationData.tags);
                    setTeamMembers(applicationData.teamMembers);
                    setUser(applicationData.user);
                    setClientId(applicationData.clientId);
                    setAllBrowsers(applicationData.browsers);
                    storeAllOperatingSystems(applicationData.operatingSystems);
                    if (typeof applicationData.defaultFilters !== 'undefined') {
                        setDefaultFilters(applicationData.defaultFilters);
                    }
                    setAllSettings(applicationData.settings);
                    NotificationsStore.storeNotifications(applicationData.notifications);
                    break;
                case 401 :
                    applicationData = JSON.parse(request.responseText);
                    _allSettings.agentConfig.name = applicationData.company;
                    _allSettings.agentConfig.contact = applicationData.contact_email;
                    handleError(401);
                    errMessage = "Sorry, there is an issue with your account, " +
                        "please check that " +
                        "your subscription is up to date";
                    _applicationIsResting = true;
                    if (window.location.pathname !== "/billing") {
                        alert(errMessage);
                    }
                    FilterActions.incrementFilterChange();
                    break;
                case 402 :
                    handleError(402);
                    applicationData = JSON.parse(request.responseText);
                    _agentLogo = applicationData.agentData.logo_filename;
                    _agentName = applicationData.agentData.name;
                    setUser(applicationData.user);
                    setAllSettings(applicationData.settings);
                    errMessage = "Sorry, there is an issue with your account, " +
                        "please check that " +
                        "your subscription is up to date";
                    _applicationIsResting = true;
                    if (window.location.pathname !== "/billing") {
                        alert(errMessage);
                    }
                    FilterActions.incrementFilterChange();
                    break;
                case 403 :
                    handleError(403);
                    errMessage = "Your account has not been validated, please validate your account by clicking the link " +
                        " given in the email that was sent to you following sign up";
                    _applicationIsResting = true;
                    if (window.location.pathname !== "/billing") {
                        alert(errMessage);
                    }
                    FilterActions.incrementFilterChange();
                    break;
            }
        }
    };
    let sortedFilters = JSON.stringify(sortFilters(_filters));
    request.send(sortedFilters);
}

function setInitialPayload() {
    setOrgsDatatableWidth();
    _applicationIsResting = false;
    setInitialFiltersByTypeAndValue();
    const request = new XMLHttpRequest();
    request.open('POST', '/api/proxy/internal/application/load');
    request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            editFilter({
                id: 0,
                storedValue: {
                    id: 0,
                    name: 0,
                    value: 0,
                    textValue: 0
                },
                type: 'firstLoad',
                round: 0
            });
            let applicationData, errMessage;
            switch (request.status) {
                case 200 :
                    try {
                        applicationData = JSON.parse(request.responseText);
                    } catch (e) {
                        applicationData = false;
                    }
                    if (!applicationData) {
                        return false;
                    }
                    _agentLogo = applicationData.agentData.logo_filename;
                    _agentName = applicationData.agentData.name;
                    _agentID = applicationData.agentData.id;
                    _allSettings.agentConfig.id = applicationData.agentData.id;
                    _allSettings.agentConfig.contact = applicationData.agentData.contact_email;
                    if (typeof applicationData.agentData.lang !== 'undefined' && applicationData.agentData.lang) {
                        _lang = JSON.parse(applicationData.agentData.lang);
                    }
                    let numberOfDaysToDisplay = 1;
                    if (applicationData.agentData.number_of_days_displayed_by_default) {
                        numberOfDaysToDisplay = applicationData.agentData.number_of_days_displayed_by_default;
                    }
                    let newDateFrom = moment().format('YYYY-MM-DD');
                    switch (numberOfDaysToDisplay) {
                        case 7 :
                            newDateFrom = moment().subtract(7, 'days').format('YYYY-MM-DD');
                            _filters[0].storedValue = newDateFrom;
                            break;
                        case 30 :
                            newDateFrom = moment().subtract(30, 'days').format('YYYY-MM-DD');
                            _filters[0].storedValue = newDateFrom;
                            break;
                    }
                    setLoadedTemplates(applicationData.templates);
                    setClientTypes(applicationData.tags);
                    setTeamMembers(applicationData.teamMembers);
                    setUser(applicationData.user);
                    setClientId(applicationData.clientId);
                    setAllBrowsers(applicationData.browsers);
                    storeAllOperatingSystems(applicationData.operatingSystems);
                    if (typeof applicationData.defaultFilters !== "undefined") {
                        setDefaultFilters(applicationData.defaultFilters);
                    }
                    storeAnalyticsData(applicationData.analytics);
                    _applicationIsResting = true;
                    setAllSettings(applicationData.settings);
                    NotificationsStore.storeNotifications(applicationData.notifications);
                    _applicationIsResting = true;
                    UserTipActions.loadOneUserTip();
                    if (window.location.pathname === "/results") {
                        loadResults();
                    } else {
                        FilterActions.incrementFilterChange();
                    }
                    break;
                case 401 :
                    applicationData = JSON.parse(request.responseText);
                    _allSettings.agentConfig.name = applicationData.company;
                    _allSettings.agentConfig.contact = applicationData.contact_email;
                    handleError(401);
                    errMessage = "Sorry, there is an issue with your account, " +
                        "please check that " +
                        "your subscription is up to date";
                    _applicationIsResting = true;
                    if (window.location.pathname !== "/billing") {
                        alert(errMessage);
                    }
                    FilterActions.incrementFilterChange();
                    break;
                case 402 :
                    handleError(402);
                    applicationData = JSON.parse(request.responseText);
                    _agentLogo = applicationData.agentData.logo_filename;
                    _agentName = applicationData.agentData.name;
                    setUser(applicationData.user);
                    setAllSettings(applicationData.settings);
                    errMessage = "Sorry, there is an issue with your account, " +
                        "please check that " +
                        "your subscription is up to date";
                    _applicationIsResting = true;
                    if (window.location.pathname !== "/billing") {
                        alert(errMessage);
                    }
                    FilterActions.incrementFilterChange();
                    break;
                case 403 :
                    handleError(403);
                    errMessage = "Your account has not been validated, please validate your account by clicking the link " +
                        " given in the email that was sent to you following sign up";
                    _applicationIsResting = true;
                    if (window.location.pathname !== "/billing") {
                        alert(errMessage);
                    }
                    FilterActions.incrementFilterChange();
                    break;
            }
        }
    };
    let sortedFilters = JSON.stringify(sortFilters(_filters));
    request.send(sortedFilters);
}

function bankResults() {
    let banked = _results.results.map(function (result, i) {
        return result.id;
    })
    FilterActions.createFilter({
        type: 'banked',
        storedValue: {
            include: 'include',
            id: _bankedFilterID,
            name: 'Banked Filter ' + _bankedFilterID,
            value: banked
        },
        id: _bankedFilterID
    })
    _bankedFilterID++;
    _filters = _originalFilters;
}

function setOrderBy(orderBy) {
    _orderBy = orderBy;
    _paginationOffset = 0;
    _applicationIsResting = false;
    setSnackbarInternal({
        message: 'Updating Results',
    })
    loadResults();
}

function setExportSelectionType(payload) {
    _exportSelectionType = payload.data;
}

function addToExportSelection(session) {
    _exportSelection.push(session);
    _exportSelectionType = "selection";
    setSnackbarInternal({
        message: 'Export list updated'
    })
}

function removeExportSelection(payload) {
    let sessionId = payload.data;
    let newExportSelection = [];
    _exportSelection.forEach(function (session, i) {
        if (session.sessionId !== sessionId) {
            newExportSelection.push(session);
        }
    });
    _exportSelection = newExportSelection;
    if (_exportSelection.length === 0) {
        _exportSelectionType = "all";
    }
}

function closeSessionResultPopover() {
    _sessionResultPopoverObj = false;
}

function setSessionResultPopover(popoverObj) {
    _sessionResultPopoverObj = popoverObj;
}

function optimisticDeleteReportTemplate(id) {
    let newTemplates = [];
    _templates.forEach(function (template, i) {
        if (template) {
            if (template.id !== id) {
                newTemplates.push(template);
            }
        }
    })
    _templates = newTemplates;
    setSnackbarInternal({
        message: 'Template deleted',
    })
}

function resetUpdatingBox() {
    _updatingBox = {
        updating: false,
        message: '',
        component: ''
    }
}

function deleteReportTemplate(payload) {
    let id = payload.data;
    let request = new XMLHttpRequest();
    setSnackbarInternal({
        message: 'Template deleted',
    })
    optimisticDeleteReportTemplate(id);
    request.open("DELETE", "/api/proxy/internal/templates/" + id);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
    };
    request.send();
}

function setExportCriteriaSendToAll(sendToAllBool) {
    _exportCriteria.sendToAll = sendToAllBool;
}

function loadNewFormatOrganisations() {
    setSnackbarInternal({
        message: 'Updating organisations',
    })
    _applicationIsResting = false;
    const request = new XMLHttpRequest();
    request.open('POST', '/api/proxy/api/v1/organisations/new-format');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _applicationIsResting = true;
            closeSnackbar();
            let jsonData;
            try {
                jsonData = JSON.parse(request.responseText);
            } catch (e) {
                jsonData = false;
            }
            if (jsonData) {
                FilterActions.storeNewFormatOrganisations(jsonData);
            }
        }
    };
    request.send(JSON.stringify(sortFilters(_filters)));
}

function updateReportTemplates(type) {
    const request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/end-company-report-templates/" + getClientId());
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        _createTemplateStatus = "hide";
        if (request.readyState === 4 && request.status === 200) {
            if (type === "new") {
                setSnackbarInternal({
                    component: 'viewSavedReportsLink'
                })
            }
            FilterActions.setLoadedTemplates(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function generateNewClientSecret() {
    const request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/settings/oauth");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeNewClientSecret(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function updateNarrowedSearchOptions() {
    FilterActions.changeLoadingStatus({
        loading: true,
        message: 'Applying updated filters'
    });
    findMatchingOptions(_currentSearchCriteria);
}

function setCustomFilterType(typeObj) {
    resetNarrowedOptions();
    _customFilterType = typeObj.data;
}

function setConcatenatePageVisits(set) {
    _allSettings.team.display_page_visits_concatenated = set;
    updateTeamSettings();
}

function loadInvoices() {
    const request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/invoices");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeInvoices(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function storeDefaultTriggerActionEmailSendToList(sendToList) {
    sendToList.forEach(function (sendToList, i) {
        _triggerActionEmailAlertTeamMembers[sendToList] = true;
    })
}

function storeCommonPaths(commonPathsObj) {
    _commonPaths = commonPathsObj.data;
}

function setCommonPaths() {
    const request = new XMLHttpRequest();
    _applicationIsResting = false;
    request.open("POST", "/api/proxy/api/v1/common-paths");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _applicationIsResting = true;
            let parsedJSON;
            try {
                parsedJSON = JSON.parse(request.responseText);
            } catch (e) {
                parsedJSON = false;
            }
            if (parsedJSON) {
                FilterActions.storeCommonPaths(parsedJSON);
            }
        }
    };
    request.send(JSON.stringify(sortFilters(_filters)));
}

function createNewTeamMember(teamMemberObj) {
    let teamMember = teamMemberObj.data;
    setSnackbarInternal({
        message: 'Creating User'
    })
    const request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/end-user-teams/member");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200) {
                let newUser;
                try {
                    newUser = JSON.parse(request.responseText);
                } catch (e) {
                    newUser = false;
                }
                if (!newUser) {
                    return false;
                }
                if (typeof newUser.message !== "undefined") {
                    alert(newUser.message);
                    return false;
                }
                let newTeamMembers = clone(_teamMembers);
                teamMember.id = newUser.id;
                newTeamMembers.push(teamMember);
                _teamMembers = newTeamMembers;
                FilterActions.showNewTeamMemberDetails({
                    newSystemUser: newUser,
                    user: teamMember,
                });
            } else if (request.status === 422) {
                let errors = JSON.parse(request.responseText);
                let errorMsg = "";
                for (let errorKey in errors) {
                    let error = errors[errorKey];
                    errorMsg += error + "\n";
                }
                alert(errorMsg);
            }
        }
    };
    request.send(JSON.stringify(teamMember));
}

function setFlatPageVisits(pageVisits) {
    _flatPageVisits = pageVisits;
}

function editTeamMember(memberId) {
    setSnackbarInternal({
        message: 'Team member updated'
    })
    let request = new XMLHttpRequest();
    request.open("PUT", "/api/proxy/api/v1/end-user-teams/member/" + memberId);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case "200" :
                    break;
                default :
                    alert("Sorry, an error occured attempting to update the user");
                    break;
            }
        }
    };
    request.send();
}

function setCurrentMapObj(mapObj) {
    _currentMapDetails = mapObj;
    setLightbox({
        type: 'currentMapDetails',
        id: 1
    })
}

function deleteTeamMember(memberId) {
    let request = new XMLHttpRequest();
    setSnackbarInternal({
        message: 'The user has been removed'
    })
    let newTeamMembers = [];
    _teamMembers.forEach(function (member, i) {
        if (parseInt(member.id) !== parseInt(memberId)) {
            newTeamMembers.push(member);
        }
    })
    _teamMembers = newTeamMembers;
    request.open("DELETE", "/api/proxy/api/v1/end-user-teams/member/" + memberId);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
        }
    };
    request.send();
}

function openTriggerActions(templateID) {
    _editTriggerActionsForTemplateID = templateID;
    _lightboxSettings = {
        type: 'editTriggerActions',
        id: -1
    }
    _loadingTriggerActionsEmailSendTo = true;
    const request = new XMLHttpRequest();
    request.open('GET', `/api/proxy/api/v1/trigger-actions/emails/${templateID}`);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    _loadingTriggerActionsEmailSendTo = false;
                    try {
                        JSON.parse(request.responseText);
                    } catch (e) {
                        return false;
                    }
                    const parsedResponse = JSON.parse(request.responseText);
                    FilterActions.storeDefaultTriggerActionsEmailSendToList(parsedResponse.sendEmailsTo);
                    FilterActions.storeAutoTags(parsedResponse.autoTags);
                    FilterActions.storeZapierIsSetup(parsedResponse.zapierIsSetup);
                    FilterActions.storeAutoExports(parsedResponse.autoExports);

                    break;
            }
        }
    };
    request.send();
}

function setPossibleActiveRecordSuggestions(suggestions) {
    _updatedActiveRecordSearchingForOptions = false;
    _updatedActiveRecordPossibleOptions = suggestions;
}

function findPossibleActiveRecordSuggestions() {
    let original = "";
    if (typeof _results.results !== "undefined" &&
        typeof _results.results[_updatedActiveRecord.myKey] !== "undefined") {
        original = _results.results[_updatedActiveRecord.myKey].use_organisation_name;
    }
    let newRecord = _updatedActiveRecord.data.use_organisation_name;
    _checkedForActiveRecordPossibleOptions = true;
    _updatedActiveRecordSearchingForOptions = true;
    if (original !== newRecord) {
        let request = new XMLHttpRequest();
        request.open("POST", "/api/proxy/api/v1/organisations/suggestions");
        request.setRequestHeader('Content-Type', 'application/json');
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                FilterActions.setPossibleActiveRecordSuggestions(JSON.parse(request.responseText));
            }
        };
        request.send(JSON.stringify({
            name: newRecord
        }));
    }
}

function setInitialUser() {
    let request = new XMLHttpRequest();
    request.open("GET", "/current-session-user");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeUser(JSON.parse(request.response));
        }
    };
    request.send();
}

function setRenewTokenDidSucceed(didSucceed) {
    if (didSucceed) {
        _renewTokenDidSucceed = 1;
        _renewTokenMessage = "Thank you, you have logged back in";
        _accountOK = true;
        _accountStatus = 0;
    } else {
        _renewTokenDidSucceed = 0;
        _renewTokenMessage = "Sorry, authentication failed";
    }
}

function updateTeamMember(newTeamMemberDetails) {
    let request = new XMLHttpRequest();
    request.open("PUT", "/api/proxy/api/v1/end-user-teams/member/" + newTeamMemberDetails.id);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    FilterActions.setSnackbar({
                        message: 'Team member updated'
                    })
                    FilterActions.updateInitialLoadedData();
                    break;
                case 409 :
                    alert("Sorry, a user is already registered with the new email address");
                    break;
            }
        }
    };
    request.send(JSON.stringify(newTeamMemberDetails));
}

function showSelectedCommonPath() {
    let newFilter = {
        type: 'setSessionIDs',
        id: 1,
        storedValue: {
            sessionIDs: _commonPathsSessionIDs,
            value: 1,
            id: 1,
            textValue: "Common Path"
        }
    }
    createRawFilter(newFilter);
    setSnackbarInternal({
        message: "Loading Common Path"
    })
    loadResults();
    browserHistory.push('/results');
}

function setIndividualFieldExportType(exportObj) {
    let newIncludeList = exportObj.allItems.map(function (type, i) {
        let newType = type;
        if (parseInt(type.id) === parseInt(exportObj.id)) {
            newType.include = exportObj.checked
        }
        return newType;
    })
    if (exportObj.editing) {
        _exisitingAutomatedReport.data.report.include_fields = newIncludeList;
    }
    let exportCriteria = clone(_exportCriteria);
    exportCriteria.include = newIncludeList;
    _exportCriteria = exportCriteria;
}

function setTeamMemberToEdit(teamMember) {
    FilterActions.setLightbox({
        type: 'editTeamMember',
        id: 1
    });
    _teamMemberToEdit = teamMember;
}

function setAllVisitorTypesSelected(booleanVal) {
    _allVisitorTypesSelected = booleanVal;
}

function setCommonPathsSessionIDs(sessionsObj) {
    _commonPathsSessionIDs = sessionsObj.sessionIDs;
    _commonPathsRowSelected = sessionsObj.rowSelected[0];
    _commonPathsSessionIDsSet = true;
}

function setAnalyticsData() {
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/analytics/all");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeAnalyticsData(JSON.parse(request.response));
        }
    };
    request.send(JSON.stringify(sortFilters(_filters)));
}

function setOrganisationByEmailText(emailText) {
    _sendOrganisationByEmailText = emailText;
}

function setCrypt(paymentType) {
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/payments/crypt");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
        }
    };
    request.send(JSON.stringify(paymentType));
}

function storeAnalyticsData(analytics) {
    if (!analytics) {
        return false;
    }
    let sessions = analytics.sessions;
    let analyticsObj = clone(analytics);
    let relationshipsRaw = analyticsObj.relationships;
    if (typeof relationshipsRaw === undefined ||
        typeof relationshipsRaw == "undefined" ||
        !relationshipsRaw) {
        return false;
    }
    let relationships = [];
    relationshipsRaw.forEach(function (relationship, i) {
        relationship = relationship;
        if (relationship[0] !== "") {
            if (typeof relationships[parseInt(relationship[0])] === "undefined") {
                relationships[parseInt(relationship[0])] = [];
            }
            relationships[parseInt(relationship[0])].push(parseInt(relationship[1]));
        }
    });
    _analyticsData = {
        referrers: (analyticsObj.referrers) ? analyticsObj.referrers : [],
        visitorStats: {
            'total_visitors': analyticsObj.total_visitors,
            'unique_visitors': analyticsObj.unique_visitors,
            'average_pages_pervisits': analyticsObj.average_pages_per_visit,
            'total_page_count': analyticsObj.page_count,
            'percentage_new_visits': analyticsObj.percentage_new_visits,
            'return_visitors': analyticsObj.return_visitors
        },
        organisations: (analyticsObj.organisations) ? analyticsObj.organisations : [],
        keywords: (analyticsObj.keywords) ? analyticsObj.keywords : [],
        mailCampaigns: (analyticsObj.mail_campaigns) ? analyticsObj.mail_campaigns : [],
        campaignSources: (analyticsObj.campaignSources) ? analyticsObj.campaignSources : [],
        campaignMediums: (analyticsObj.campaignMediums) ? analyticsObj.campaignMediums : [],
        lastPages: (analyticsObj.last_pages) ? analyticsObj.last_pages : [],
        pages: (analyticsObj.all_pages) ? analyticsObj.all_pages : [],
        entryPages: (analyticsObj.entry_pages) ? analyticsObj.entry_pages : [],
        visitorLocations: (analyticsObj.countries) ? analyticsObj.countries : [],
        cities: (analyticsObj.cities) ? analyticsObj.cities : [],
        counties: (analyticsObj.counties) ? analyticsObj.counties : [],
        tags: (analyticsObj.tags) ? analyticsObj.tags : [],
        uniqueSessions: sessions,
        indexes: analyticsObj.indexes,
        organisationRelationships: relationships,
        heatmapData: (analyticsObj.heatmap_data) ? analyticsObj.heatmap_data : [],
        events: (analyticsObj.events) ? analyticsObj.events : []
    };
    if (typeof analyticsObj.total_visits !== "undefined") {
        _analyticsData.visitorStats.total_visits = analyticsObj.total_visits;
    }
    setQuickExportSearchResults('');
}

function setNoteOrganisationId(orgId) {
    _noteOrganisationId = orgId;
}

function setNoResultsReportMsg(msg) {
    _noResultsMsg = msg;
}

function setHelpWindow(helpObj) {
    setLightbox({
        type: 'helpWindow',
        id: -1
    })
    _help = helpObj;
}

function setPossibleOption(organisationID) {
    _updatedActiveRecordChosenOption = organisationID;
}

function saveNote(note) {
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/notes");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 422) {
                alert("Please make sure the subject, and note fields are complete");
            }
            if (request.status === 200) {
                FilterActions.closeLightbox();
            }
        }
    };
    request.send(JSON.stringify(note));
}

function setFiltersBeforeAdvancedFiltering(filters) {
    _filters = filters;
    setInitialFiltersByTypeAndValue();
}

function openNotes(organisationID) {
    _loadingNotes = true;
    _noteOrganisationId = organisationID;
    setLightbox({
        type: 'createNote',
        id: 1
    })
    fetchNotes(organisationID);
}

function changeRoute() {
    resetNoResultsReportData();
    _quickExportSearchString = "";
    _quickExportSelection = [];
    _narrowedSearchOptions = [];
    _landingPageNumber = 0;
    setOrgsDatatableWidth();
    if (_currentRoute !== "organisations") {
        UserTipActions.loadOneUserTip();
    }
}

function storeNotes(notes) {
    _notes = notes;
}

function setAssignedToTeamMember(teamMemberObj) {
    let teamMemberId = teamMemberObj.teamMemberID;
    if (_currentTrackingObj) {
        _currentTrackingObj.assigned_to = parseInt(teamMemberId);
        _currentTrackingObj.assigned_user = parseInt(teamMemberId);
        if (teamMemberObj.storeType === "reportBuilder") {
            _results.results[_currentTrackingObj.myKey].assigned_user = teamMemberId;
        }
    }
}

function saveAssignedToForOrganisationsDashboard() {
    let isTracked = false;
    if (_currentTrackingObj.assigned_user > 0) {
        isTracked = true;
    }
    if (_currentTrackingObj.key !== -1) {
        let currentOrg = _analyticsData.organisations[_currentTrackingObj.key];
        currentOrg[6] = _currentTrackingObj.assigned_user;
        _analyticsData.organisations[_currentTrackingObj.key] = currentOrg;
    } else {
        if (_currentTrackingObj.assigned_user === -1) {
            delete _trackedOrganisations[_currentTrackingObj.organisationid];
        } else {
            _trackedOrganisations[_currentTrackingObj.organisationid] = _currentTrackingObj.assigned_user;
        }
    }
    saveAssignedToTeamMember();
}

function removeAllButDateFilters() {
    let basicFilters = clone(_filters);
    let newBasicFilters = [];
    newBasicFilters[0] = basicFilters[0];
    newBasicFilters[1] = basicFilters[1];
    _filters = newBasicFilters;
    _filtersByTypeAsIndex = {};
    setInitialFiltersByTypeAndValue();
}

function saveAssignedToTeamMember() {
    FilterActions.closeLightbox();
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/assign-team-member");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
        }
    };
    request.send(JSON.stringify(_currentTrackingObj));
    FilterActions.setSnackbar({
        message: 'Team member successfully assigned'
    })
}

function addRemoveExportType(typeObj) {
    _exportCriteria.exportTypes[typeObj.type] = typeObj.value;
}

function resetAllButDateFilters() {
    let currentFilters = clone(_filters);
    _filters = clone(_originalFilters);
    let keepTypes = [
        'visitortypefilter',
        'datefrom',
        'dateto',
        'timefrom',
        'timeto',
        'minsessionduration',
        'minnumberofpages',
        'maxnumberofpages'
    ];
    let newFilters = [];
    for (let key in currentFilters) {
        if (typeof currentFilters[key] !== "undefined") {
            let filter = currentFilters[key];
            if (filter) {
                if (keepTypes.indexOf(filter.type.toLowerCase()) > -1) {
                    newFilters.push(filter);
                }
            }
        }
    }
    _filters = newFilters;
    _filtersByTypeAsIndex = {};
    setInitialFiltersByTypeAndValue();
    _paginationOffset = 0;
    _results = clone(_resultsOriginalState);
}

function setSendOrganisationByEmailAttachSpreadsheet(isChecked) {
    _sendOrganisationByEmailAttachSpreadsheet = isChecked;
}

function showNewTeamMember(newTeamMember) {
    _newTeamMember = newTeamMember;
    setLightbox({
        type: 'newTeamMember',
        id: 1
    })
}

function resetToOriginalFilters() {
    const originalFilters = clone(_originalFilters);
    delete originalFilters[0];
    delete originalFilters[1];
    delete originalFilters[2];
    delete originalFilters[3];
    originalFilters[0] = _filters[0];
    originalFilters[1] = _filters[1];
    _showScenarioFilters = null;
    _filters = originalFilters;
    _paginationOffset = 0;
    _filtersByTypeAsIndex = {};
    setInitialFiltersByTypeAndValue();
}

function resetToOriginalState() {
    resetToOriginalFilters();
    setInitialFiltersByTypeAndValue();
    updateResults();
}

function resetFilters(doNotReloadAnalytics = false) {
    setSnackbarInternal({
        message: 'Clearing all filters ...'
    })
    _results = {
        totals: {},
        visitorStats: {},
        relationships: [],
        results: []
    };
    _applicationIsResting = false;
    _openDrawer = 5;
    resetToOriginalFilters();
    setInitialFiltersByTypeAndValue();
    AdavancedFilterActions.resetFiltersNoAction();
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/events/reset-filters");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            switch (request.status) {
                case 200 :
                    let returnData = true;
                    try {
                        returnData = JSON.parse(request.responseText);
                    } catch (e) {
                        returnData = false;
                    }
                    if (!returnData) {
                        return false;
                    }
                    if (!doNotReloadAnalytics) {
                        storeAnalyticsData(returnData.analytics);
                    }
                    _results = returnData.results;
                    _paginationNumberOfPages = Math.ceil(_results.totals.total / _paginationDisplayOnPage);
                    break;
                default :
                    _paginationNumberOfPages = 0;
                    break;
            }
            closeSnackbar();
            _newFiltersApplied = false;
            _applicationIsResting = true;
            FilterActions.incrementFilterChange();
        }
    };
    let sendFilters = sortFilters(_filters);
    sendFilters.doNotReloadAnalytics = doNotReloadAnalytics;
    request.send(JSON.stringify(sendFilters));
}

function addslashes(str) {
    let replaceStr = (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    return encodeURI(replaceStr);
}

function setQuickExportSearchResults(searchText) {
    _landingPageNumber = 0;
    let dataSet = false;
    _quickExportSearchResults = [];
    _quickExportSearchString = searchText;
    searchText = searchText.replace("?", "");
    let regexp = new RegExp(addslashes(searchText), 'gi');
    let multipleFieldSearch = false;
    switch (_currentRoute) {
        case "referrers" :
            dataSet = _analyticsData.referrers;
            break;
        case "organisations" :
            dataSet = _analyticsData.organisations;
            break;
        case "countries" :
            dataSet = _analyticsData.visitorLocations;
            break;
        case "entry-pages" :
            dataSet = _analyticsData.entryPages;
            multipleFieldSearch = true;
            break;
        case "all-pages" :
            dataSet = _analyticsData.pages;
            multipleFieldSearch = true;
            break;
        case "last-pages" :
            dataSet = _analyticsData.lastPages;
            multipleFieldSearch = true;
            break;
        case "keywords" :
            dataSet = _analyticsData.keywords;
            break;
        default :
            dataSet = _analyticsData.organisations;
            break;
    }
    if ((searchText !== "" && searchText) || typeof searchText === "undefined") {
        if (dataSet) {
            let alreadyContains = [];
            let newSelection2 = [];
            dataSet.forEach(function (referrer, i) {
                if (typeof referrer[1] !== "undefined" && referrer[1]) {
                    if (referrer[1].match(regexp)) {
                        if (alreadyContains.indexOf(parseInt(referrer[0])) === -1) {
                            alreadyContains.push(parseInt(referrer[0]));
                            newSelection2.push(referrer);
                        }
                    }
                    if (multipleFieldSearch) {
                        if (typeof referrer[3] !== "undefined") {
                            if (referrer[3].match(regexp)) {
                                if (alreadyContains.indexOf(parseInt(referrer[0])) === -1) {
                                    alreadyContains.push(parseInt(referrer[0]));
                                    newSelection2.push(referrer);
                                }
                            }
                        }
                    }
                }
            });
            _quickExportSearchResults = newSelection2;
            if (_quickExportSelectionType === "all") {
                let newSelection = [];
                _quickExportSearchResults.forEach(function (item, i) {
                    newSelection[parseInt(item[0])] = item[1];
                })
                _quickExportSelection = newSelection;
            }
        }
    } else {
        _quickExportSearchResults = dataSet;
        if (_quickExportSelectionType === "all") {
            if (_quickExportSearchResults) {
                let newSelection = [];
                _quickExportSearchResults.forEach(function (item, i) {
                    newSelection[parseInt(item[0])] = item[1];
                })
                _quickExportSelection = newSelection;
            }
        }
    }
    _applicationIsResting = true;
}

function setQuickExportSearchString(searchString) {
    _landingPageNumber = 0;
    _quickExportSearchString = searchString;
}

function setBulkOrganisationExportType(option) {
    _bulkOrganisationExportType = option;
}

function setAutoExports(autoExports) {
    _autoExports = autoExports;
}

function showClientSecret() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/proxy/api/v1/user/client-secret", true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    _clientSecret = xhr.responseText;
                    FilterActions.incrementFilterChange();
                    break;
                case 500 :
                    alert("Sorry, no secret was found");
                    break;
            }
        }
    };
}

function uploadAvatar(file) {
    _uploadingAvatar = true;
    let formData = new FormData();
    formData.append('avatar', file[0], 'avatar.jpg');
    let xhr = new XMLHttpRequest();
    xhr.open("post", "/api/proxy/api/v1/user/avatar", true);
    xhr.send(formData);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            FilterActions.setAvatar(JSON.parse(xhr.responseText));
        }
    };
}

function advancedFiltersStraightToExport(scenarios) {
    processAdvancedFilters(scenarios);
    browserHistory.push('/export');
}

function processAdvancedFilters(scenarios) {
    removeAllButDateFilters();
    let scenariosArray = scenarios.scenarios;
    for (let key in scenariosArray) {
        let scenario = scenariosArray[key];
        createRawFilter({
            type: 'scenarioFilter',
            id: scenario.id,
            storedValue: {
                id: scenario.id,
                value: scenario.id,
                textValue: scenario.name,
                name: scenario.name,
                scenario,
                include: 'include',
                criteria: 'include'
            }
        })
    }
    for (let key in scenarios.looseFilters) {
        let filter = scenarios.looseFilters[key];
        if (filter) {
            switch (filter.type.toLowerCase()) {
                case "minnumberofpages"    :
                    filter.id = 0;
                    filter.storedValue.id = 0;
                    break;
                case "maxnumberofpages"    :
                    filter.id = 0;
                    filter.storedValue.id = 0;
                    break;
            }
            createRawFilter(filter);
        }
    }
}

function createScenarioFilters(scenarios) {
    processAdvancedFilters(scenarios);
}

function updateAnalytics(dontUpdateApplicationIsResting) {
    let request = new XMLHttpRequest();
    FilterActions.setSnackbar({
        message: "Updating Analytics",
        open: true
    });
    _applicationIsResting = false;
    request.open("POST", "/api/proxy/api/v1/analytics/all");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            let returnData = JSON.parse(request.responseText);
            if (typeof returnData.error === "undefined" || !returnData.error) {
                closeSnackbar();
                if (!dontUpdateApplicationIsResting) {
                    _applicationIsResting = true;
                }
                _analyticsOutOfDate = false;
                storeAnalyticsData(returnData);
                FilterActions.incrementFilterChange();
            }
        }
    };
    request.send(JSON.stringify(sortFilters(_filters)));
}

function displayFancyFilters(displayBoolean) {
    _showFancyFilters = displayBoolean;
}

function openDrawer(index) {
    _openDrawer = index;
    resetNarrowedOptions();
}

function deleteNote(note) {
    let newNotes = [];
    _notes.forEach(function (note, i) {
        if (note.id !== note.id) {
            newNotes.push(note);
        }
    })
    _notes = newNotes;
    let request = new XMLHttpRequest();
    request.open("DELETE", "/api/proxy/api/v1/notes/" + note.id);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.setSnackbar({
                message: 'Thank you, note successfully deleted'
            })
        }
    };
    request.send();
}

function loadFiltersAndInitialData(reportID) {
    setSnackbarInternal({
        message: "Updating Analytics",
        open: true
    });
    setInitialFiltersByTypeAndValue();
    _applicationIsResting = false;
    const request3 = new XMLHttpRequest();
    request3.open("POST", "/api/proxy/internal/application/load");
    request3.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request3.onreadystatechange = function () {
        if (request3.readyState === 4 && request3.status === 200) {
            let applicationData;
            try {
                applicationData = JSON.parse(request3.responseText);
            } catch (e) {
                applicationData = false;
            }
            if (!applicationData) {
                return false;
            }
            _agentLogo = applicationData.agentData.logo_filename;
            _agentName = applicationData.agentData.name;
            _allSettings.agentConfig.contact = applicationData.agentData.contact_email;
            setLoadedTemplates(applicationData.templates);
            setClientTypes(applicationData.tags);
            setTeamMembers(applicationData.teamMembers);
            setUser(applicationData.user);
            setClientId(applicationData.clientId);
            setAllBrowsers(applicationData.browsers);
            storeAllOperatingSystems(applicationData.operatingSystems);
            if (typeof applicationData.defaultFilters !== "undefined") {
                setDefaultFilters(applicationData.defaultFilters);
            }
            setAllSettings(applicationData.settings);
            NotificationsStore.storeNotifications(applicationData.notifications);
            const request1 = new XMLHttpRequest();
            request1.open("GET", "/api/proxy/internal/load-report/" + reportID);
            request1.setRequestHeader('Content-Type', 'application/json');
            request1.onreadystatechange = function () {
                if (request1.readyState === 4 && request1.status === 200) {
                    let newFilters = JSON.parse(request1.responseText);
                    _filters = newFilters;
                    const request2 = new XMLHttpRequest();
                    request2.open("POST", "/api/proxy/api/v1/analytics/all");
                    request2.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    request2.onreadystatechange = function () {
                        if (request2.readyState === 4) {
                            if (request2.status === 200) {
                                let returnData = JSON.parse(request2.responseText);
                                if (typeof returnData.error === "undefined" || !returnData.error) {
                                    storeAnalyticsData(returnData);
                                }
                                FilterActions.loadNewFormatOrganisations();
                                FilterActions.incrementFilterChange();
                            }
                        }
                    };
                    request2.send(JSON.stringify(sortFilters(_filters)));
                }
            };
            request1.send();
        }
    }
    let filtersObj = sortFilters(_filters);
    filtersObj.loadAnalytics = false;
    let sendObj = JSON.stringify(filtersObj);
    request3.send(sendObj);
}

function updateUserSettings(settings) {
    _allSettings.user = settings;
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/settings/user");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
    };
    request.send(JSON.stringify(settings));
}

function setFirstLoadComplete(section) {
    _firstLoadComplete[section] = true;
}

function setNewTemplateName(newName) {
    _exportCriteria.newTemplateName = newName;
}

function openPopover(popoverSettings) {
    _popoverSettings = popoverSettings;
}

function resetSearchString() {
    _currentSearchCriteria.searchString = "";
}

function fetchNotes(orgId) {
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/notes/" + orgId);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            FilterActions.storeNotes(JSON.parse(request.response));
        }
    };
    request.send();
}

function setTemplateDisplayedInDashboard(templateKey) {
    let template = _templates[templateKey];
    if (template.display_in_dashboard) {
        template.display_in_dashboard = false;
    } else {
        template.display_in_dashboard = true;
    }
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/templates/dashboard");
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify({
            templateID: template.id,
            display: template.display_in_dashboard
        })
    );
    _templates[templateKey] = template;
}

function editTag(tag) {
    _editTag = tag;
    setLightbox({
        type: 'editTag',
        id: -1
    });
}

function setAutomatedReportBlocked(templateID) {
    let template = _templates[templateID];
    if (template.blocked) {
        template.blocked = false;
    } else {
        template.blocked = true;
    }
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/automated-reports/block");
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify({
            automatedReportID: template.automated_report_id,
            block: template.blocked
        })
    );
    _templates[templateID] = template;
}

function openExportToCRM(organisationID) {
    setLightbox({
        type: 'exportToCRM',
        id: -1
    })
    _exportOrganisationIDToCRM = organisationID;
}

function displayPreviousVisits(prevVisitsObj) {
    closeLightbox();
    setSnackbarInternal({
        open: true,
        message: "Loading previous visits"
    })
    resetToOriginalFilters();
    let selection = _previousVisits.selection;
    if (_previousVisits.selection.length === 0) {
        selection = _previousHistory.map(function (visit, i) {
            return visit.session_id;
        });
    }
    let newFilter = {
        type: 'setSessionIDs',
        id: 1,
        storedValue: {
            sessionIDs: selection,
            value: 1,
            textValue: prevVisitsObj.orgName
        }
    }
    _previousVisits.selection = [];
    _previousVisits.offset = 0;
    createRawFilter(newFilter);
    updateResults();
}

function fireOffOrganisationEmail() {
    setSnackbarInternal({
        message: 'Thank you, an email has been sent'
    })
    closeLightbox();
    let sortedFilters = sortFilters(clone(_filters));
    sortedFilters.includeOrganisations = [_sendCompanyID];
    sortedFilters.paginationReturn = 10;
    sortedFilters.exportCriteria = _exportCriteria;
    sortedFilters.emailText = _sendOrganisationByEmailText;
    sortedFilters.attachSpreadsheet = _sendOrganisationByEmailAttachSpreadsheet;
    sortedFilters.sendEmailOptions = {
        teamMembers: _sendEmailToTeamMembers,
        companyID: _sendCompanyID
    }
    _sendOrganisationByEmailText = '';
    _sendOrganisationByEmailAttachSpreadsheet = false;
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/emails/single-organisation");
    request.setRequestHeader('Content-Type', 'application/json');
    let cache = [];
    request.send(JSON.stringify(sortedFilters, function (key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    return;
                }
                cache.push(value);
            }
            return value;
        })
    );
    cache = null;
}

function startStripeSessionAndRedirect(paymentType) {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/stripe/start-session");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({
            planType: paymentType
        })
    );
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let stripe = Stripe('pk_live_dT41jwq0wBBQvk24YIMdHHUW');
                if (window.location.hostname === 'localhost') {
                    stripe = Stripe('pk_test_ZNK2cPSbc954G7d6LIZLmkFO')
                }
                stripe.redirectToCheckout({
                    sessionId: xhr.responseText
                }).then(function (result) {
                });
            } else {
                alert('Sorry, an error occurred, please try again later');
            }
        }
    }
}

function updateHooks(hooks) {
    _allSettings.hooks.newOrgHook = hooks.newOrgHook;
    _allSettings.hooks.newVisitHook = hooks.newVisitHook;
    let request = new XMLHttpRequest();
    request.open("POST", "/api/proxy/api/v1/hooks");
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
        }
    };
    request.send(JSON.stringify(_allSettings.hooks));
    FilterActions.setSnackbar({
        message: "Thank you, hooks have been updated"
    })
}

function setAddOrRemoveQuickExportSelection(selection) {
    if (selection.length > 0) {
        _quickExportSelectionType = "selection";
    }
    _quickExportSelection = selection;
}

function showAdditionalCompanyData(organisationID) {
    _additionalCompanyData.data = [];
    setLightbox({
        type: 'additionalCompanyData',
        id: -1
    })
    let request = new XMLHttpRequest();
    request.open("GET", "/api/proxy/api/v1/organisation/additional-data/" + organisationID);
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            _loadingAdditionalCompanyData = false;
            FilterActions.storeAdditionalCompanyData(JSON.parse(request.responseText));
        }
    };
    request.send();
}

function startBulkCRMExport() {
    closeLightbox();
    setSnackbarInternal({
        message: 'Exporting organisations...'
    });
    let selection = {};
    _quickExportSelection.forEach(function (item, i) {
        if (item) {
            selection[i] = item;
        }
    });
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/export/crm-bulk", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({
            crmChoices: _crmExportSelection,
            selection: selection
        })
    );
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200 :
                    _bulkOrganisationExportType = "csv";
                    FilterActions.setSnackbar({
                        message: 'Organisations successfully exported'
                    })
                    break;
            }
        }
    };
}

function storeAdditionalCompanyData(companyData) {
    _additionalCompanyData.data = companyData;
}

function addOrRemoveQuickExportSelection(orgObj) {
    if (typeof _quickExportSelection[orgObj.id] !== "undefined") {
        delete _quickExportSelection[orgObj.id];
    } else {
        _quickExportSelection[orgObj.id] = orgObj.display;
    }
    if (_quickExportSelection.length > 0) {
        _quickExportSelectionType = "selection";
    } else {
        _quickExportSelectionType = "all";
    }
}

function setPaginationReturnAmount(amount) {
    _results = clone(_resultsOriginalState);
    _paginationReturn = amount;
    _paginationOffset = 0;
    _paginationDisplayOnPage = amount;
    updateResults();
}

function setCountryVisitorStats(visitorStats) {
    let countryStats = visitorStats;
    _countryVisitorStats = countryStats;
    FilterActions.setLightbox({
        type: 'countryVisitorStats',
        id: 1
    })
}

function quickDashboardLink(newFilter) {
    resetAllButDateFilters();
    createRawFilter(newFilter);
    setSnackbarInternal({
        message: 'Loading data'
    })
    updateResults();
}

function setAvatar(message) {
    _uploadingAvatar = false;
    _errorMsg = message.error;
    _avatarSet = true;
}

function quickExportSelectAll(filterType) {
    let selection = [];
    _newFormattedOrganisations.forEach(function (filter, i) {
        selection[filter.organisation_id] = filter.name;
    });
    setAddOrRemoveQuickExportSelection(selection);
}

function quickExportDeselectAll(filterType) {
    _quickExportSelection = [];
}

function setActiveTab(tab) {
    _currentSearchCriteria.noSearchResults = false;
    _activeTab = tab;
    _inclusionType = "include";
    _customFilterIncludeOrExclude = "include";
    resetSearchString();
    setSearchByLoadingStatus(false);
    resetNarrowedOptions();
    FilterActions.incrementFilterChange();
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    let copy = obj.constructor();
    for (let attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function hideDrawers() {
    _openDrawer = 0;
}

function loadLink(typeAndID) {
    switch (typeAndID.type) {
        case "organisation" :
            let id = parseInt(typeAndID.id);
            let fromDate = new moment().subtract(6, 'months');
            _filters[0] = {
                round: 0,
                storedValue: fromDate.format('YYYY-MM-DD'),
                type: 'dateFrom'
            }
            let newFilter = {
                id: id,
                type: "organisationFilter",
                storedValue: {
                    criteria: "include",
                    id: id,
                    include: "include",
                    value: id,
                    textValue: "via email link",
                    name: "via email link"
                }
            }
            createRawFilter(newFilter);
            loadResults();
            break;
    }
}

function selectAllOptionsIfSelectionEmpty() {
    _quickExportSelectionType = "all";
    let dataSet = getDataSet();
    let newQuickExportSelection = [];
    dataSet.forEach(function (item, i) {
        if (typeof item.name !== "undefined") {
            newQuickExportSelection[item.organisation_id] = item.name;
        } else if (typeof item[0] === "string") {
            newQuickExportSelection[parseInt(item[0].trim())] = item[1];
        } else {
            newQuickExportSelection[item[0]] = item[1];
        }
    });
    _quickExportSelection = newQuickExportSelection;
}

function setWindowBoxOpen(open) {
    _windowBoxOpen = open;
}

function setLandingPageNumber(nextOrPrevious) {
    if (nextOrPrevious === "next") {
        _landingPageNumber++;
    } else {
        _landingPageNumber--;
    }
}

function getFiltersByTypeWithParam(type) {
    let filters = [];
    if (typeof _filtersByTypeAsIndex === "undefined") {
        return [];
    }
    if (typeof _filtersByTypeAsIndex[type.toLowerCase()] !== "undefined") {
        filters = _filtersByTypeAsIndex[type.toLowerCase()];
    }
    return filters;
}

function getDataSet() {
    let dataSet = [];
    switch (_currentRoute) {
        case "referrers" :
            dataSet = _analyticsData.referrers;
            break;
        case "organisations" :
            dataSet = _newFormattedOrganisations;
            break;
        case "countries" :
            dataSet = _analyticsData.visitorLocations;
            break;
        case "entry-pages" :
            dataSet = _analyticsData.entryPages;
            break;
        case "all-pages" :
            dataSet = _analyticsData.pages;
            break;
        case "last-pages" :
            dataSet = _analyticsData.lastPages;
            break;
        case "keywords" :
            dataSet = _analyticsData.keywords;
            break;
        default :
            dataSet = _analyticsData.organisations;
            break;
    }
    return dataSet;
}

function openChooseExportType() {
    setLightbox({
        type: 'chooseExportTypeCSVOrCRM',
        id: -1
    })
}

function checkLightboxOpenInternal(type, id) {
    if (_lightboxSettings.type === type && parseInt(_lightboxSettings.id) === id) {
        return true;
    }
    return false;
}

function turnOffAutoExportForCRM(exportData) {
    const xhr = new XMLHttpRequest();
    xhr.open('DELETE', `/api/proxy/api/v1/trigger-actions/autoexport/${exportData.templateId}`);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200:
                    FilterActions.setAutoExport({
                        templateId: exportData.templateId,
                        checked: false,
                    });

                    break;
            }
        }
    };
}

function turnOnAutoExportForCRM(exportData) {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '/api/proxy/api/v1/trigger-actions/autoexport');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(JSON.stringify(exportData));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200:
                    FilterActions.setAutoExport({
                        templateId: exportData.templateId,
                        checked: true,
                    });

                    break;
            }
        }
    };
}

function attemptLogin(loginCredentials) {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/renew-token");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(loginCredentials));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200:
                    FilterActions.setRenewTokenDidSucceed(true);
                    break;
                case 500 :
                    FilterActions.setRenewTokenDidSucceed(false);
                    break;
            }
        }
    };
}

function storeZapierIsSetup(zapierIsSetup) {
    _zapierIsSetup = zapierIsSetup
}

function sendTestWebhookData(webhookURLs) {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/proxy/api/v1/webhooks/test", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(webhookURLs));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            switch (xhr.status) {
                case 200:
                    FilterActions.setSnackbar({
                        message: "Test data sent"
                    })
                    break;
            }
        }
    };
}

browserHistory.listen(location => {
    _currentRoute = location.pathname.replace("/", "");
    changeRoute();
    if (typeof a1ChangeRouteListener !== "undefined") {
        a1ChangeRouteListener();
    }
});

let FiltersStore = merge(EventEmitter.prototype, {
    getCountryVisitorStats: function () {
        return _countryVisitorStats;
    },
    getActiveRecordSuggestions: function () {
        return clone(_updatedActiveRecordPossibleOptions);
    },
    filtersGetHeatmapData: function () {
        return clone(_analyticsData.heatmapData);
    },
    getAllSettings: function () {
        return clone(_allSettings);
    },
    getResultsFound: function () {
        let found = -1;
        if (_resultsUpdated) {
            if (typeof _results.results !== "undefined") {
                found = _results.results.length;
            }
        }
        return found;
    },
    parse: function (val) {
        return !!+val;
    },
    isSearchingForActiveRecordOptions: function () {
        return _updatedActiveRecordSearchingForOptions;
    },
    accountIsOK: function () {
        return clone({
            accountOK: _accountOK,
            accountStatus: _accountStatus
        })
    },
    getInvoices: function () {
        return clone(_invoices);
    },
    getQuickExportSettings: function () {
        let returnObj = {
            selectionType: clone(_quickExportSelectionType),
            fileTypes: clone(_quickExportFileTypes),
            selection: clone(_quickExportSelection)
        };
        return returnObj;
    },
    isLoadingAdditionalCompanyData: function () {
        return _loadingAdditionalCompanyData;
    },
    getUpdateBox: function (component) {
        if (component.toLowerCase() === _updatingBox.component.toLowerCase()) {
            if (_updatingBox.updating) {
                return clone(_updatingBox);
            }
        }
        return {
            updating: false
        };
    },
    isZapierSetup: () => {
        return _zapierIsSetup;
    },
    getClientTypesByIndex: function () {
        return clone(_clientTypesByIndex);
    },
    getScenarioFiltersObj: function () {
        return {
            filters: _showScenarioFilters,
            open: checkLightboxOpenInternal("showScenarioFilters", -1)
        }
    },
    clone: function (obj) {
        if (null == obj || "object" != typeof obj) return obj;
        let copy = obj.constructor();
        for (let attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    },
    getCurrentTrackingObj: function () {
        return clone(_currentTrackingObj);
    },
    getSessionResultPopoverObj: function () {
        return clone(_sessionResultPopoverObj);
    },
    getCRMExportSelection: function () {
        return clone(_crmExportSelection);
    },
    getQuickExportSearchResults: function () {
        if (typeof _quickExportSearchString === "undefined" || _quickExportSearchString === "") {
            _quickExportSearchResults = getDataSet();
        }
        let returnResults = clone(_quickExportSearchResults);
        returnResults = returnResults.sort(function (b, a) {
            if (parseInt(a[2]) < parseInt(b[2])) return -1;
            if (parseInt(a[2]) > parseInt(b[2])) return 1;
            return 0;
        });
        return returnResults;
    },
    isApplicationResting: function () {
        return _applicationIsResting;
    },
    removeFiltersByType: function (filters) {
        removeFiltersByType(filters);
    },
    getCurrentLandingPageNo: function () {
        return _landingPageNumber;
    },
    getPricing: function () {
        return clone(_prices);
    },
    getQuickExportSearchString: function () {
        return clone(_quickExportSearchString);
    },
    resetToOriginalState: function () {
        resetToOriginalState();
    },
    getPaymentType: function () {
        return _paymentType;
    },
    createFilter: function (filterObj) {
        createRawFilter(filterObj);
    },
    getPopoverSettingsNew: function () {
        return _sessionResultPopoverObj;
    },
    getPopoverID: function () {
        if (!_sessionResultPopoverObj) {
            return -1;
        }
        return _sessionResultPopoverObj.id;
    },
    checkSessionResultPopoverObjOpen: function (id) {
    },
    getWindowBoxOpen: function () {
        return _windowBoxOpen;
    },
    areFiltersCurrentlyBeingEdited: function () {
        return _editingFilters;
    },
    getCurrentMapDetails: function () {
        return clone(_currentMapDetails);
    },
    noResultsFound: function () {
        if (_applicationIsResting && _results && _results.results.length === 0 && _currentRoute === "results") {
            return true;
        }
        return false;
    },
    getClientSecret: function () {
        return _clientSecret;
    },
    getDisplayPerPageOnLandingPages: function () {
        return _displayPerPageOnLandingPages;
    },
    externalSortFilters: function () {
        return sortFilters(_filters);
    },
    getQuickReportDateRange: function () {
        return _quickReportDateRange;
    },
    getQuickReportTrafficType: function () {
        return _quickReportTrafficType;
    },
    getQuickReportDataType: function () {
        return _quickReportDataType;
    },
    getTrackerCode: function () {
        return _trackerCode;
    },
    removeFilterByType: function (filter) {
        removeFilterByType(filter);
    },
    getLatestAddedFilters: function () {
        return _latestAddedFilters;
    },
    areResultsSet: function () {
        return typeof _results.results !== "undefined";
    },
    getCreateTemplateStatus: function () {
        return _createTemplateStatus;
    },
    getSendEmailToTeamMembers: function () {
        return clone(_sendEmailToTeamMembers);
    },
    getCurrentSearchCriteria: function () {
        return _currentSearchCriteria;
    },
    getPopoverSettings: function () {
        return _popoverSettings;
    },
    getUserCommunicationPreferences: function () {
        return clone(_userCommunicationPreferences);
    },
    getBlockedList: function () {
        return _blockedOrganisations;
    },
    getPreviousHistoryOrganisationName: function () {
        return _previousHistoryOrganisation;
    },
    getEditTriggerActionsTemplateID: function () {
        return _editTriggerActionsForTemplateID;
    },
    getCurrentOrganisationRowIndex: function () {
        return _currentRowOrganisationIndex;
    },
    getSalesforceID: function () {
        return _allSettings.team.salesforce_organisation_id;
    },
    getHelp: function () {
        return clone(_help);
    },
    getReportRecordData: function () {
        return clone(_reportRecord);
    },
    reportNoResultsBoxIsOpen: function () {
        return _showReportNoResultsBox;
    },
    getOrganisationExportSelection: function () {
        return _exportOrganisations;
    },
    getClientTypesLightboxObj: function () {
        return clone(_clientTypesLightboxObj);
    },
    getStoreTest: function () {
        return _storeTest;
    },
    getOrganisationsDatatableWidth: function () {
        return _organisationsDatatableWidth;
    },
    getLang: function () {
        return _lang;
    },
    commonPathsSessionIDsHaveBeenSet: function () {
        return _commonPathsSessionIDsSet;
    },
    getNewTeamMember: function () {
        return clone(_newTeamMember);
    },
    getPreviousVisits: function () {
        return clone(_previousVisits);
    },
    getAuthenticationMsg: function () {
        return {
            type: _renewTokenDidSucceed,
            msg: _renewTokenMessage
        }
    },
    getUploadAvatarStatus: function () {
        return {
            uploading: _uploadingAvatar,
            error: _errorMsg
        };
    },
    isLoadingTriggerActionsEmailSendTo: function () {
        return _loadingTriggerActionsEmailSendTo;
    },
    getCrypt: function () {
        return _crypt;
    },
    getAdditionalCompanyData: function () {
        return clone(_additionalCompanyData);
    },
    getLooseFilterSetDisplay: function () {
        const looseFilters = clone(_looseFiltersToDisplayInLightbox);
        return {
            lightboxOpen: checkLightboxOpenInternal('looseFiltersInLightbox', -1),
            filters: looseFilters.filters,
            type: looseFilters.type
        };
    },
    getExcludeSelectionByName: function () {
        return clone(_excludeSelectionByName);
    },
    getExcludeSelection: function () {
        return clone(_excludeSelection);
    },
    haveNewFiltersBeenApplied: function () {
        return _newFiltersApplied;
    },
    checkedForActiveRecordPossibleOptions: function () {
        return _checkedForActiveRecordPossibleOptions;
    },
    shouldCustomFilterIncludeOrExclude: function () {
        return _customFilterIncludeOrExclude;
    },
    getQuickOpenObj: function () {
        return _quickOpen;
    },
    getOrderBy: function () {
        return _orderBy;
    },
    checkIfDrawerOpen: function (index) {
        if (_openDrawer === index) {
            return true;
        }
        return false;
    },
    getOpenDrawerIndex: function () {
        return _openDrawer;
    },
    getBulkOrganisationExportType: function () {
        return _bulkOrganisationExportType;
    },
    getActiveRecordChosenOption: function () {
        return _updatedActiveRecordChosenOption;
    },
    getActiveTab: function () {
        return _activeTab;
    },
    isAvatarSet: function () {
        return _avatarSet;
    },
    getNotes: function () {
        return clone(_notes);
    },
    getExportSelectionType: function () {
        return clone(_exportSelectionType);
    },
    getNoteOrganisationId: function () {
        return _noteOrganisationId;
    },
    getWebsocketConn: function () {
        return _websocketConn;
    },
    getAutoTags: function () {
        return clone(_autoTags);
    },
    getAnalyticsData: function () {
        return clone(_analyticsData);
    },
    getAllVisitorTypesSelected: function () {
        return _allVisitorTypesSelected;
    },
    getTeamMemberToEdit: function () {
        return _teamMemberToEdit;
    },
    getTrackedOrganisations: function () {
        return clone(_trackedOrganisations);
    },
    getUpdatingAnalytics: function () {
        return _updatingAnalytics;
    },
    getUser: function () {
        return _user;
    },
    getCommonPaths: function () {
        return clone(_commonPaths);
    },
    getCustomFilterType: function () {
        return _customFilterType;
    },
    checkSelectedForExport: function (sessionId) {
        let found = false;
        _exportSelection.forEach(function (session, i) {
            if (session.sessionId === sessionId) {
                found = true;
            }
        })
        return found;
    },
    getAutoExports: () => {
        return _autoExports;
    },
    getCommonPathRowSelected: function () {
        return _commonPathsRowSelected;
    },
    getFilterChange: function () {
        return _filterChange;
    },
    getExportSelection: function () {
        return clone(_exportSelection);
    },
    getKeywordValues: function () {
        return _keywordValues;
    },
    getSearchByLoadingStatus: function () {
        return _searchByLoadingStatus;
    },
    getOrganisationToShowInLightbox: function () {
        return _showOrganisationInLightbox;
    },
    getSelectAllSearchByType: function () {
        return _selectAllSearchByType;
    },
    getPreviousHistory: function () {
        return _previousHistory;
    },
    isFirstLoadComplete: function (section) {
        return _firstLoadComplete[section];
    },
    checkLightboxOpen: function (type, id = -1) {
        if (_lightboxSettings.type === type && parseInt(_lightboxSettings.id) === id) {
            return true;
        }
        return false;
    },
    getExistingReportData: function () {
        return clone(_exisitingAutomatedReport);
    },
    checkAllSelected: function (checkSelected) {
        if (typeof _allSelected[checkSelected.type.toLowerCase()] !== "undefined") {
            return _allSelected[checkSelected.type.toLowerCase()];
        }
        return false;
    },
    getExistingReportsData: function () {
        return clone(_existingAutomatedReportsData);
    },
    getAutomationSettings: function () {
        return _setAutomationSettings;
    },
    getNarrowedOptionsType: function () {
        return _narrowedSearchOptionsType;
    },
    getNarrowedOptions: function () {
        return _narrowedSearchOptions;
    },
    checkIfFilterTypeAndIndexExists: function (type, id) {
        if (typeof _filtersByTypeAsIndex === "undefined") {
            return false;
        }
        if (typeof _filtersByTypeAsIndex[type.toLowerCase()] != "undefined") {
            if (_filtersByTypeAsIndex[type.toLowerCase()].indexOf(id) > -1) {
                return true;
            }
            if (_filtersByTypeAsIndex[type.toLowerCase()].indexOf(parseInt(id)) > -1) {
                return true;
            }
        }
        return false;
    },
    getFiltersByTypeWithParam: function (type) {
        let filters = [];
        if (typeof _filtersByTypeAsIndex[type.toLowerCase()] !== "undefined") {
            filters = _filtersByTypeAsIndex[type.toLowerCase()];
        }
        return filters;
    },
    getFilterByTypeWithParam: function (type) {
        let filters = getFiltersByTypeWithParam(type);
        if (typeof filters[0] === "undefined") {
            return 0;
        }
        return filters[0];
    },
    resultsReportHasBeenSent: function () {
        return _noResultsReportSent;
    },
    getFiltersByType: function () {
        return clone(_filtersByTypeAsIndex);
    },
    getSortedFilters: function () {
        return sortFilters(clone(_filters));
    },
    getCountryInclusionType: function () {
        return _countryInclusionType;
    },
    checkIfCheckboxChecked: function (filterId) {
        if (typeof _filters[filterId] != "undefined") {
            return true;
        }
        return false;
    },
    getAllOperatingSystems: function () {
        return _operatingSystems;
    },
    getAllBrowsers: function () {
        return _allBrowsers;
    },
    internalRemoveAllFiltersByType: function (type) {
        removeAllFiltersByType(type);
    },
    showFancyFilters: function () {
        return _showFancyFilters;
    },
    checkCompanyDataOpen: function (organisationId) {
        if (typeof _companyDataOpen[organisationId] === "undefined") {
            return false;
        }
        return _companyDataOpen[organisationId];
    },
    getOrganisationData: function (organisationId) {
        if (typeof _storedOrganisationData === 'undefined') {
            return false;
        }
        return _storedOrganisationData;
    },
    getExportCriteria: function () {
        return clone(_exportCriteria);
    },
    getTeamMembers: function () {
        return clone(_teamMembers);
    },
    getSnackbarSettings: function () {
        return {
            component: clone(_snackbarComponent),
            open: clone(_snackbarOpen),
            msg: clone(_snackbarMsg),
            data: clone(_snackbarOtherData)
        }
    },
    getKeywords: function () {
        return _keywords;
    },
    getDisplayBy: function () {
        return _displayBy;
    },
    getClientTypes: function () {
        return clone(_clientTypes);
    },
    getWebsocketMessages: function () {
        return clone(_websocketMessages);
    },
    loadInitialPayload: function () {
        setInitialPayload();
    },
    getNumberOfResultLoads: function () {
        return _numberOfResultLoads;
    },
    getExclusions: function () {
        return clone(_exclusions);
    },
    getEditTag: function () {
        return clone(_editTag);
    },
    getTriggerAlertEmailSendToList: function () {
        return _triggerActionEmailAlertTeamMembers;
    },
    getCurrentRoute: function () {
        return _currentRoute;
    },
    getNewFormattedOrganisations: function () {
        return clone(_newFormattedOrganisations);
    },
    getFlatPageVisits: function () {
        return clone(_flatPageVisits);
    },
    getPaginationTotals: function () {
        if (!_results) {
            return false;
        }
        let total = (typeof _results.totals.total !== "undefined") ? _results.totals.total : 0;
        _paginationNumberOfPages = Math.ceil(_results.totals.total / _paginationDisplayOnPage);
        return {
            total: total,
            numPages: _paginationNumberOfPages,
            returnAmount: _paginationReturn,
            offset: _paginationOffset
        };
    },
    getInclusionType: function () {
        return _inclusionType;
    },
    objectEntries: function (obj) {
        let index = 0;
        let propKeys = Object.getOwnPropertyNames(obj).concat(Object.getOwnPropertySymbols(obj));
        return {
            [Symbol.iterator]() {
                return this;
            },
            next() {
                if (index < propKeys.length) {
                    let key = propKeys[index];
                    index++;
                    return {value: [key, obj[key]]};
                } else {
                    return {done: true};
                }
            }
        };
    },
    isLoadingPreviousHistory: function () {
        return _loadingPreviousHistory;
    },
    loadClientPages: function () {
        let request = new XMLHttpRequest();
        request.open("GET", "/api/proxy/api/v1/page");
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                let pages = JSON.parse(request.responseText);
                FilterActions.setClientPages(pages);
            }
        }
    },
    loadCountries: function () {
        let request = new XMLHttpRequest();
        request.open("GET", "/api/proxy/api/v1/countries");
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                let countries = JSON.parse(request.responseText);
                FilterActions.setCountries(countries);
            }
        }
    },
    loadReferrers: function () {
        let request = new XMLHttpRequest();
        request.open("GET", "/api/proxy/api/v1/referrers");
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                let referrers = JSON.parse(request.responseText);
                FilterActions.setReferrers(referrers);
            }
        }
    },
    getZohoKey: function () {
        return _zohoKey;
    },
    loadExistingTemplates: function () {
        FilterActions.setSnackbar({
            message: "Loading template filters"
        })
        let request = new XMLHttpRequest();
        request.open("GET", "/api/proxy/internal/templates");
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                let templates = JSON.parse(request.responseText);
                FilterActions.setLoadedTemplates(templates);
            }
        }
    },
    getClientPages: function () {
        return _clientPages;
    },
    getExistingTemplates: function () {
        return clone(_templates);
    },
    getFlashMessages: function () {
        return _flashMessages;
    },
    getCountries: function () {
        return _countries;
    },
    getReferrers: function () {
        return _referrers;
    },
    getPaginationReturn: function () {
        return _paginationReturn;
    },
    getPaginationOffset: function () {
        return _paginationOffset;
    },
    getActiveRecord: function () {
        return clone(_updatedActiveRecord);
    },
    getDisplayedColumns: function () {
        return _displayedColumns;
    },
    getClientId: function () {
        return _clientId;
    },
    loadClientId: function () {
    },
    getLastFilterId: function () {
        return (_filterId - 1);
    },
    getLoadingStatus: function () {
        return _loadingStatus;
    },
    getOrganisationFilters: function () {
        return _organisationFilters;
    },
    getAllOrganisations: function () {
        return _allOrganisations;
    },
    getFilters: function () {
        return clone(_filters);
    },
    getResults: function () {
        if (typeof _results === 'undefined') {
            _results = {
                totals: {},
                visitorStats: {},
                relationships: [],
                results: []
            };
        }
        if (typeof _results.totals === 'undefined') {
            _results.totals = {};
        }
        if (typeof _results.visitorStats === 'undefined') {
            _results.visitorStats = {};
        }
        if (typeof _results.relationships === 'undefined') {
            _results.relationships = [];
        }
        if (typeof _results.results === "undefined") {
            _results.results = [];
        }
        return clone(_results);
    },
    getClientRelationships: function () {
        return clone(_results.relationships);
    },
    emitChange: function () {
        this.emit('change');
    },
    addChangeListener: function (callback) {
        this.on('change', callback);
    },
    removeChangeListener: function (callback) {
        this.removeListener('change', callback);
    }
});
ReportBuilderDispatcher.register(function (payload) {
    let returnRes = true;
    switch (payload.actionType) {
        case FilterConstants.FETCH_NOTES :
            fetchNotes(payload.data);
            break;
        case FilterConstants.SET_CURRENT_ROUTE :
            _currentRoute = window.location.pathname.replace("/", "");
            break;
        case FilterConstants.UPLOAD_AVATAR :
            uploadAvatar(payload.data);
            break;
        case FilterConstants.LOAD_INITIAL_DATA :
            setInitialPayload();
            break;
        case FilterConstants.CREATE_RAW_FILTER :
            createRawFilter(filter);
            break;
        case FilterConstants.ATTEMPT_LOGIN :
            attemptLogin(payload.data);
            break;
        case FilterConstants.STORE_USER :
            setUser(payload.data);
            setClientId(payload.data.end_user_company_id);
            break;
        case FilterConstants.SET_USER :
            setInitialUser();
            break;
        case FilterConstants.SAVE_REPORT_TEMPLATE :
            saveReportTemplate(payload);
            break;
        case FilterConstants.DOWNLOAD_CSV :
            downloadCSV();
            break;
        case FilterConstants.PAGINATION_NEXT :
            setPaginationOffset('next');
            break;
        case FilterConstants.PAGINATION_PREV :
            setPaginationOffset('prev');
            break;
        case FilterConstants.SET_SALESFORCE_ID :
            setSalesforceID(payload.data);
            break;
        case FilterConstants.LOAD_FILTERS:
            loadFilters(action.data);
            break;
        case FilterConstants.CREATE_FILTER :
            createFilter(payload.filterObj);
            break;
        case FilterConstants.EDIT_FILTER :
            editFilter(payload.filterObj);
            break;
        case FilterConstants.SET_PAGINATION_RETURN_AMOUNT :
            setPaginationReturnAmount(payload.data);
            break;
        case FilterConstants.UPDATE_EXPORT_OPTIONS :
            updateExportOptions(payload.data);
            break;
        case FilterConstants.UPDATE_RESULTS :
            updateResults();
            break;
        case FilterConstants.DELETE_FILTER :
            deleteFilter(payload.id);
            break;
        case FilterConstants.CHANGE_LOADING_STATUS :
            changeLoadingStatus(payload);
            break;
        case FilterConstants.SHOW_NEW_RESULTS :
            showNewResults(payload.newResults);
            break;
        case FilterConstants.SEND_TEST_WEBHOOK_DATA :
            sendTestWebhookData(payload.data);
            break;
        case FilterConstants.DISPLAY_PREVIOUS_VISITS :
            displayPreviousVisits(payload.data);
            break;
        case FilterConstants.TEST_JEST :
            testJestStore();
            break;
        case FilterConstants.SET_DISPLAYED_COLUMNS :
            setDisplayedColumns(payload);
            break;
        case FilterConstants.CREATE_FLASH_MESSAGE :
            setFlashMessage(payload);
            break;
        case FilterConstants.SET_LOADED_TEMPLATES :
            setLoadedTemplates(payload.data);
            break;
        case FilterConstants.SET_LANDING_PAGE_NUMBER :
            setLandingPageNumber(payload.data);
            break;
        case FilterConstants.QUICK_EXPORT_SHOW_DATA_NEW_ORGANISATIONS_FORMAT :
            quickExportShowDataNewOrganisationsFormat(payload.data);
            break;
        case FilterConstants.LOAD_TRACKER_CODE :
            loadTrackerCode();
            break;
        case FilterConstants.SET_NEW_TEMPLATE_NAME :
            setNewTemplateName(payload.data);
            break;
        case FilterConstants.OPEN_TRIGGER_ACTIONS :
            openTriggerActions(payload.data);
            break;
        case FilterConstants.SAVE_AUTO_TAGS :
            saveAutoTags();
            break;
        case FilterConstants.SET_REFERRERS :
            setReferrers(payload);
            break;
        case FilterConstants.SET_COUNTRIES :
            setCountries(payload);
            break;
        case FilterConstants.RESET_UPDATING_BOX :
            resetUpdatingBox();
            break;
        case FilterConstants.SET_CLIENT_PAGES :
            setClientPages(payload);
            break;
        case FilterConstants.SET_ORGANISATION_INCLUSION_TYPE :
            setOrganisationInclusionType(payload.data);
            break;
        case FilterConstants.SET_TEMPLATE_FILTERS :
            setTemplateFilters(payload);
            break;
        case FilterConstants.SHOW_FILTERS_IN_CONSOLE :
            showFiltersInConsole();
            break;
        case FilterConstants.SET_FILTERS_AND_LOAD :
            setFiltersAndLoad(payload.data);
            break;
        case FilterConstants.LOAD_NEW_REPORT_FILTERS :
            loadNewReportFilters(JSON.parse(payload.filters));
            break;
        case FilterConstants.UPDATE_CLIENT_RELATIONSHIP :
            updateClientRelationship(payload);
            break;
        case FilterConstants.SET_ALERTS_FOR_TEMPLATE :
            setAlertsForTemplate(payload.data);
            break;
        case FilterConstants.CHANGE_RELATIONSHIP_STATUS :
            changeRelationshipStatus(payload);
            break;
        case FilterConstants.CREATE_NEW_TAG :
            createNewTag(payload);
            break;
        case FilterConstants.UPDATE_CLIENT_TYPES :
            updateClientTypes();
            break;
        case FilterConstants.SET_NOTE_ORGANISATIONID :
            setNoteOrganisationId(payload.data);
            break;
        case FilterConstants.SET_DISPLAY_BY_FILTER :
            setDisplayByFilter(payload);
            break;
        case FilterConstants.ASYNC_NEW_CLIENT_TYPE :
            addNewTagAsync(payload);
            break;
        case FilterConstants.BROADCAST_INITIAL_LOAD :
            broadcastInitialLoad();
            break;
        case FilterConstants.SAVE_BILLING_DETAILS :
            saveBillingDetails(payload.data);
            break;
        case FilterConstants.SET_SNACKBAR :
            setSnackbar(payload);
            break;
        case FilterConstants.SET_EXPORT_TYPES :
            setExportTypes(payload);
            break;
        case FilterConstants.ADD_TEAM_MEMBER_TO_EXPORT_CRITERIA :
            addTeamMemberToExportCriteria(payload);
            break;
        case FilterConstants.REMOVE_TEAM_MEMBER_FROM_EXPORT_CRITERIA :
            removeTeamMemberFromExportCriteria(payload);
            break;
        case FilterConstants.START_EXPORT :
            startExport();
            break;
        case FilterConstants.SET_ORGANISATION_DATA :
            setOrganisationData(payload.data);
            break;
        case FilterConstants.STORE_ORGANISATION_DATA :
            storeOrganisationData(payload);
            break;
        case FilterConstants.SHOW_ORGANISATION_IN_LIGHTBOX_AND_SET_INDEX :
            showOrganisationInLightboxAndSetIndex(payload.data);
            break;
        case FilterConstants.RESET_SEARCH_STRING :
            resetSearchString();
            break;
        case FilterConstants.SAVE_END_USER_COMPANY_COMPANY_DATA :
            saveEndUserCompanyCompanyData(payload);
            break;
        case FilterConstants.EDIT_TEAM_SETTINGS_IN_STORE_ONLY :
            editTeamSettingsInStoreOnly(payload.data);
            break;
        case FilterConstants.REMOVE_FLASH_MESSAGES :
            removeFlashMessages();
            break;
        case FilterConstants.LOG_ACTIVITY :
            logActivity(payload.data);
            break;
        case FilterConstants.UPDATE_LOAD_ON_LOGIN_FOR_TEMPLATE :
            uploadLoadOnLoginForTemplate(payload.data);
            break;
        case FilterConstants.SET_COMPANY_DATA_OPEN :
            setCompanyDataOpen(payload);
            break;
        case FilterConstants.REPORT_NO_RESULTS :
            reportNoResults();
            break;
        case FilterConstants.SET_COMPANY_DATA_CLOSE :
            setCompanyDataClose(payload);
            break;
        case FilterConstants.GET_ALL_BROWSERS :
            getAllBrowsers();
            break;
        case FilterConstants.SET_ALL_BROWSERS :
            setAllBrowsers(payload.data);
            break;
        case FilterConstants.SET_ALL_OPERATING_SYSTEMS :
            setAllOperatingSystems();
            break;
        case FilterConstants.SET_FLAT_PAGE_VISITS :
            setFlatPageVisits(payload.data);
            break;
        case FilterConstants.STORE_ALL_OPERATING_SYSTEMS :
            storeAllOperatingSystems(payload.data);
            break;
        case FilterConstants.CLOSE_SNACKBAR :
            closeSnackbar();
            break;
        case FilterConstants.STORE_USER_COMMUNICATION_PREFERENCES :
            storeUserCommunicationPreferences(payload.data);
            break;
        case FilterConstants.SET_COUNTRY_INCLUSION_TYPE :
            setCountryInclusionType(payload);
            break;
        case FilterConstants.NARROW_SEARCH_OPTIONS :
            narrowSearchOptions(payload);
            break;
        case FilterConstants.SET_NARROWED_OPTIONS :
            setNarrowedOptions(payload.data);
            break;
        case FilterConstants.SET_POSSIBLE_OPTION :
            setPossibleOption(payload.data);
            break;
        case FilterConstants.REMOVE_FILTER_BY_TYPE :
            removeFilterByType(payload.data);
            break;
        case FilterConstants.RESET_NARROWED_OPTIONS :
            resetNarrowedOptions();
            break;
        case FilterConstants.UPDATE_EXISTING_AUTOMATED_REPORT_IN_STORE_ONLY :
            updateExistingAutomatedReportInStoreOnly(payload.data);
            break;
        case FilterConstants.OPEN_SET_AUTOMATION :
            openSetAutomation(payload);
            break;
        case FilterConstants.CLOSE_SET_AUTOMATION :
            closeSetAutomation();
            break;
        case FilterConstants.QUICK_EXPORT_EXPORT_NEW_ORGANISATIONS_FORMAT :
            quickExportExportNewOrganisationsFormat(payload.data);
            break;
        case FilterConstants.RESET_SELECTED_TEAM_MEMBERS :
            resetSelectedTeamMembers();
            break;
        case FilterConstants.CREATE_AUTOMATED_DELIVERY :
            createAutomatedDelivery(payload);
            break;
        case FilterConstants.UPDATE_EXPORTED_CRITERIA :
            console.log(payload);
            updateExportCriteria(payload);
            break;
        case FilterConstants.VIEW_EXISTING_AUTOMATED_REPORTS :
            viewExistingAutomatedReports(payload);
            break;
        case FilterConstants.SHOW_ORGANISATION_IN_LIGHTBOX :
            showOrganisationInLightbox(payload.data);
            break;
        case FilterConstants.SET_EXISTING_REPORT_DATA :
            setExistingReportData(payload);
            break;
        case FilterConstants.OPEN_VIEW_EDIT_AUTOMATED_REPORT :
            openViewEditAutomatedReport(payload);
            break;
        case FilterConstants.CLOSE_EXISTING_AUTOMATED_REPORT :
            closeExistingAutomatedReport();
            break;
        case FilterConstants.UPDATE_AUTOMATED_DELIVERY :
            updateAutomatedDelivery(payload);
            break;
        case FilterConstants.SET_ALL_SELECTED :
            setAllSelected(payload);
            break;
        case FilterConstants.REMOVE_EXISTING_REPORT :
            removeExistingReport(payload);
            break;
        case FilterConstants.SET_LIGHTBOX :
            setLightbox(payload);
            break;
        case FilterConstants.SET_DYNAMICS_CALLBACK_URL :
            setDynamicsCallbackURL(payload.data);
            break;
        case FilterConstants.CLOSE_LIGHTBOX :
            closeLightbox(payload);
            break;
        case FilterConstants.RESET_FILTERS :
            resetFilters(payload.data);
            break;
        case FilterConstants.SET_ORGANISATION_BY_EMAIL_TEXT :
            setOrganisationByEmailText(payload.data);
            break;
        case FilterConstants.SET_PREVIOUS_HISTORY :
            setPreviousHistory(payload);
            break;
        case FilterConstants.SUBMIT_REPORT_RECORD :
            submitReportRecord();
            break;
        case FilterConstants.STORE_PREVIOUS_HISTORY :
            storePreviousHistory(payload);
            break;
        case FilterConstants.OPEN_CHOOSE_EXPORT_TYPE :
            openChooseExportType();
            break;
        case FilterConstants.SET_SEARCH_BY_SELECT_ALL :
            setSearchBySelectAll(payload);
            break;
        case FilterConstants.SET_QUICK_EXPORT_SEARCH_RESULTS :
            setQuickExportSearchResults(payload.data);
            break;
        case FilterConstants.SET_CONCATENATE_PAGE_VISITS :
            setConcatenatePageVisits(payload.data);
            break;
        case FilterConstants.UPDATE_INITIAL_LOADED_DATA :
            break;
        case FilterConstants.HIDE_SEARCH_BY_LOADING_STATUS :
            setSearchByLoadingStatus(false);
            break;
        case FilterConstants.SET_INITIAL_FILTERS_BY_TYPE_AND_VALUE :
            setInitialFiltersByTypeAndValue();
            break;
        case FilterConstants.ADD_TO_EXPORT_SELECTION :
            addToExportSelection(payload.data);
            break;
        case FilterConstants.REMOVE_EXPORT_SELECTION :
            removeExportSelection(payload);
            break;
        case FilterConstants.SET_EXPORT_SELECTION_TYPE :
            setExportSelectionType(payload);
            break;
        case FilterConstants.CREATE_NEW_TRACKED_ORGANISATION :
            createNewTrackedOrganisation(payload);
            break;
        case FilterConstants.LOAD_PRICES :
            loadPrices();
            break;
        case FilterConstants.STORE_NEW_FORMAT_ORGANISATIONS :
            storeNewFormatOrganisations(payload.data);
            break;
        case FilterConstants.DELETE_REPORT_TEMPLATE :
            deleteReportTemplate(payload);
            break;
        case FilterConstants.UPDATE_REPORT_TEMPLATES :
            updateReportTemplates(payload.data);
            break;
        case FilterConstants.UPDATE_NARROWED_SEARCH_OPTIONS :
            updateNarrowedSearchOptions();
            break;
        case FilterConstants.SET_CUSTOM_FILTER_TYPE :
            setCustomFilterType(payload);
            break;
        case FilterConstants.SAVE_CHANGES_TO_TRIGGER_ACTION_EMAIL_ALERTS :
            saveChangesToTriggerActionEmailAlerts();
            break;
        case FilterConstants.SET_SEND_ORGANISATION_BY_EMAIL_ATTACH_SPREADSHEET :
            setSendOrganisationByEmailAttachSpreadsheet(payload.data);
            break;
        case FilterConstants.SET_COMMON_PATHS :
            setCommonPaths();
            break;
        case FilterConstants.STORE_COMMON_PATHS :
            storeCommonPaths(payload);
            break;
        case FilterConstants.CREATE_NEW_TEAM_MEMBER :
            createNewTeamMember(payload);
            break;
        case FilterConstants.EDIT_TEAM_MEMBER :
            editTeamMember(payload.data);
            break;
        case FilterConstants.SET_WINDOW_BOX_OPEN :
            setWindowBoxOpen(payload.data);
            break;
        case FilterConstants.DELETE_TEAM_MEMBER :
            deleteTeamMember(payload.data);
            break;
        case FilterConstants.UPDATE_TRIGGER_ACTION_EMAIL_SEND_TO_LIST :
            updateTriggerActionEmailSendToList(payload.data);
            break;
        case FilterConstants.SET_TEAM_MEMBER_TO_EDIT :
            setTeamMemberToEdit(payload.data);
            break;
        case FilterConstants.REMOVE_ALL_BUT_DATE_FILTERS :
            removeAllButDateFilters();
            break;
        case FilterConstants.UPDATE_TEAM_MEMBER :
            updateTeamMember(payload.data);
            break;
        case FilterConstants.SET_ALL_VISITOR_TYPES_SELECTED :
            setAllVisitorTypesSelected(true);
            break;
        case FilterConstants.LOAD_EXCLUSIONS :
            loadExclusions();
            break;
        case FilterConstants.SET_ALL_VISITOR_TYPES_UNSELECTED :
            setAllVisitorTypesSelected(false);
            break;
        case FilterConstants.SET_ANALYTICS_DATA :
            setAnalyticsData();
            break;
        case FilterConstants.STORE_ANALYTICS_DATA :
            storeAnalyticsData(payload.data);
            break;
        case FilterConstants.SET_RENEW_TOKEN_DID_SUCCEED :
            setRenewTokenDidSucceed(payload.data);
            break;
        case FilterConstants.UPDATE_FILTER_NUMBERS :
            storeUpdatedFilterNumbers();
            break;
        case FilterConstants.SAVE_NOTE :
            saveNote(payload.data);
            break;
        case FilterConstants.STORE_NOTES :
            storeNotes(payload.data);
            break;
        case FilterConstants.SET_BULK_ORGANISATION_EXPORT_TYPE :
            setBulkOrganisationExportType(payload.data);
            break;
        case FilterConstants.SET_AVATAR :
            setAvatar(payload.data);
            break;
        case FilterConstants.SET_ACTIVE_TAB :
            setActiveTab(payload.data);
            break;
        case FilterConstants.OPEN_DRAWER :
            openDrawer(payload.data);
            break;
        case FilterConstants.SHOW_REPORT_NO_RESULTS_BOX :
            showReportNoResultsBox();
            break;
        case FilterConstants.STORE_EXCLUSIONS :
            storeExclusions(payload.data);
            break;
        case FilterConstants.SAVE_THIRD_PARTY_APP_SETTINGS :
            saveThirdPartyAppSettings();
            break;
        case FilterConstants.SET_SEARCH_BY_LOADING_STATUS :
            setSearchByLoadingStatus(true, payload.data);
            break;
        case FilterConstants.HIDE_DRAWERS :
            hideDrawers();
            break;
        case FilterConstants.SHOW_SCENARIO_FILTERS :
            showScenarioFilters(payload.data);
            break;
        case FilterConstants.STORE_PRICING :
            storePricing(payload.data);
            break;
        case FilterConstants.SET_CUSTOM_FILTER_INCLUDE_TYPE :
            setCustomFilterIncludeType(payload.data);
            break;
        case FilterConstants.SET_CRYPT :
            setCrypt(payload.data);
            break;
        case FilterConstants.UPDATE_USER_PERMISSIONS :
            updateUserPermissions(payload.data);
            break;
        case FilterConstants.DELETE_NOTE :
            deleteNote(payload.data);
            break;
        case FilterConstants.LOAD_SERVICE_STATUS :
            loadServiceStatus();
            break;
        case FilterConstants.INCREMENT_FILTER_CHANGE :
            incrementFilterChange(payload.data);
            break;
        case FilterConstants.NEW_SET_CLIENT_TYPES :
            setClientTypes(payload.data);
            break;
        case FilterConstants.SET_SESSION_RESULT_POPOVER :
            setSessionResultPopover(payload.data);
            break;
        case FilterConstants.CLOSE_SESSION_RESULT_POPOVER :
            closeSessionResultPopover();
            break;
        case FilterConstants.OPEN_POPOVER :
            openPopover(payload.data);
            break;
        case FilterConstants.SET_ASSIGNED_TO_TEAM_MEMBER :
            setAssignedToTeamMember(payload.data);
            break;
        case FilterConstants.LOAD_FILTERS_AND_INITIAL_DATA :
            loadFiltersAndInitialData(payload.data);
            break;
        case FilterConstants.REMOVE_TRACKED_ORGANISATION :
            removeTrackedOrganisation(payload.data);
            break;
        case FilterConstants.SAVE_ASSIGNED_TO :
            saveAssignedToTeamMember();
            break;
        case FilterConstants.SET_ZOHO_KEY :
            setZohoKey(payload.data);
            break;
        case FilterConstants.BANK_RESULTS :
            bankResults();
            break;
        case FilterConstants.UPDATE_USER_SETTINGS :
            updateUserSettings(payload.data);
            break;
        case FilterConstants.UPDATE_HOOKS :
            updateHooks(payload.data);
            break;
        case FilterConstants.SET_USE_GEOLOCATION :
            setUseGeolocation(payload.data);
            break;
        case FilterConstants.STORE_NEW_CLIENT_SECRET :
            storeNewClientSecret(payload.data);
            break;
        case FilterConstants.SHOW_FILTERS_IN_LIGHTBOX :
            showFiltersInLightbox(payload.data);
            break;
        case FilterConstants.SET_ACTIVE_RECORD :
            setActiveRecord(payload.data);
            break;
        case FilterConstants.ADD_REMOVE_EXPORT_TYPE :
            addRemoveExportType(payload.data);
            break;
        case FilterConstants.SET_NO_RESULTS_REPORT_MSG :
            setNoResultsReportMsg(payload.data);
            break;
        case FilterConstants.SET_FIRST_LOAD_COMPLETE :
            setFirstLoadComplete(payload.data);
            break;
        case FilterConstants.SHOW_CLIENT_SECRET :
            showClientSecret();
            break;
        case FilterConstants.SET_INDIVIDUAL_FIELD_EXPORT_TYPE :
            setIndividualFieldExportType(payload.data);
            break;
        case FilterConstants.SET_COUNTRY_VISITOR_STATS :
            setCountryVisitorStats(payload.data);
            break;
        case FilterConstants.UPDATE_INCLUDE_FIELDS :
            updateIncludeFields(payload.data);
            break;
        case FilterConstants.REPORT_RECORD :
            reportRecord(payload.data);
            break;
        case FilterConstants.EDIT_FILTERS :
            editFilters(payload.data);
            break;
        case FilterConstants.SAVE_TEAM_SETTINGS :
            saveTeamSettings();
            break;
        case FilterConstants.CREATE_FILTERS :
            createFilters(payload.data);
            break;
        case FilterConstants.SET_FILTERS_BEFORE_ADVANCED_FILTERING :
            setFiltersBeforeAdvancedFiltering(payload.data);
            break;
        case FilterConstants.REMOVE_FILTERS_BY_TYPE :
            removeFiltersByType(payload.data);
            break;
        case FilterConstants.QUICK_CREATE_REPORT :
            quickCreateReport();
            break;
        case FilterConstants.SET_QUICK_OPEN :
            setQuickOpen(payload.data);
            break;
        case FilterConstants.GENERATE_NEW_CLIENT_SECRET :
            generateNewClientSecret();
            break;
        case FilterConstants.STORE_WEBSOCKET_MSG :
            storeWebsocketMsg(payload.data);
            break;
        case FilterConstants.QUICK_LINK :
            quickLink(payload.data);
            break;
        case FilterConstants.STORE_DEFAULT_TRIGGER_ACTIONS_EMAIL_SEND_TO_LIST :
            storeDefaultTriggerActionEmailSendToList(payload.data);
            break;
        case FilterConstants.DISPLAY_FANCY_FILTERS :
            displayFancyFilters(payload.data);
            break;
        case FilterConstants.UPDATE_ANALYTICS_ORGANISATION_DATA :
            updateAnalyticsOrganisationData(payload.data);
            break;
        case FilterConstants.STORE_TRACKER_CODE :
            storeTrackerCode(payload.data);
            break;
        case FilterConstants.SAVE_ASSIGNED_TO_FOR_ORGANISATIONS_DASHBOARD :
            saveAssignedToForOrganisationsDashboard();
            break;
        case FilterConstants.SEND_ORGANISATION_VIA_EMAIL :
            sendOrganisationViaEmail(payload.data);
            break;
        case FilterConstants.ADD_TEAM_MEMBER_TO_SEND_EMAIL_ARRAY :
            addTeamMemberToSendEmailArray(payload.data);
            break;
        case FilterConstants.SET_TRACKED_ORGANISATION_DATA :
            setTrackedOrganisationData(payload.data);
            break;
        case FilterConstants.FIRE_OFF_ORGANISATION_EMAIL:
            fireOffOrganisationEmail();
            break;
        case FilterConstants.RESET_FILTERS_ONLY_AND_NO_ACTION :
            resetFiltersOnlyAndNoAction();
            break;
        case FilterConstants.EXPORT_TO_CRM_CHOICES :
            exportToCRMChoices();
            break;
        case FilterConstants.SET_QUICK_EXPORT_SELECTION_TYPE :
            setQuickExportSelectionType(payload.data);
            break;
        case FilterConstants.REMOVE_EXCLUSION :
            removeExclusion(payload.data);
            break;
        case FilterConstants.OPEN_NOTES :
            openNotes(payload.data);
            break;
        case FilterConstants.SET_QUICK_EXPORT_FILE_TYPE :
            setQuickExportFileType(payload.data);
            break;
        case FilterConstants.START_QUICK_EXPORT :
            startQuickExport(payload.data);
            break;
        case FilterConstants.SHOW_ADDITIONAL_COMPANY_DATA :
            showAdditionalCompanyData(payload.data);
            break;
        case FilterConstants.ADD_OR_REMOVE_QUICK_EXPORT_SELECTION :
            addOrRemoveQuickExportSelection(payload.data);
            break;
        case FilterConstants.SET_ADD_OR_REMOVE_QUICK_EXPORT_SELECTION :
            setAddOrRemoveQuickExportSelection(payload.data);
            break;
        case FilterConstants.SET_QUICK_EXPORT_SEARCH_STRING :
            setQuickExportSearchString(payload.data);
            break;
        case FilterConstants.STORE_ADDITIONAL_COMPANY_DATA :
            storeAdditionalCompanyData(payload.data);
            break;
        case FilterConstants.SET_EXPORT_CRITERIA_SEND_TO_ALL :
            setExportCriteriaSendToAll(payload.data);
            break;
        case FilterConstants.QUICK_EXPORT_SHOW_DATA :
            quickExportShowData(payload.data);
            break;
        case FilterConstants.CHANGE_ROUTE :
            changeRoute();
            break;
        case FilterConstants.APPLICATION_NOT_RESTING :
            applicationNotResting();
            break;
        case FilterConstants.SET_PREVIOUS_VISITS_SELECTION :
            setPreviousVisitsSelection(payload.data);
            break;
        case FilterConstants.LOAD_MORE_PREVIOUS_VISITS :
            loadMorePreviousVisits(payload.data);
            break;
        case FilterConstants.BLOCK_ORGANISATION :
            blockOrganisation(payload.data);
            break;
        case FilterConstants.OPEN_EXPORT :
            openExport();
            break;
        case FilterConstants.DELETE_TAG :
            deleteTag(payload.data);
            break;
        case FilterConstants.QUICK_DASHBOARD_LINK :
            quickDashboardLink(payload.data);
            break;
        case FilterConstants.SET_HELP_WINDOW :
            setHelpWindow(payload.data);
            break;
        case FilterConstants.SET_QUICK_REPORT_DATE_RANGE :
            setQuickReportDateRange(payload.data);
            break;
        case FilterConstants.STORE_NEW_TEMPLATES :
            storeLoadedTemplates(payload.data);
            break;
        case FilterConstants.SET_QUICK_REPORT_DATA_TYPE :
            setQuickReportDataType(payload.data);
            break;
        case FilterConstants.SET_ORDER_BY :
            setOrderBy(payload.data);
            break;
        case FilterConstants.LOAD_COMMUNICATION_PREFERENCES :
            loadCommunicationPreferences();
            break;
        case FilterConstants.MAKE_PAYMENT :
            makePayment();
            break;
        case FilterConstants.START_BULK_CRM_EXPORT :
            startBulkCRMExport();
            break;
        case FilterConstants.UPDATE_ACTIVE_RECORD :
            updateActiveRecord(payload.data);
            break;
        case FilterConstants.SELECT_ALL_OPTIONS_IF_SELECTION_EMPTY :
            selectAllOptionsIfSelectionEmpty();
            break;
        case FilterConstants.QUICK_EXPORT_SELECT_ALL :
            quickExportSelectAll(payload.data);
            break;
        case FilterConstants.SET_DISPLAY_GCLID :
            setDisplayGCLID(payload.data);
            break;
        case FilterConstants.QUICK_EXPORT_DESELECT_ALL :
            quickExportDeselectAll(payload.data);
            break;
        case FilterConstants.SET_QUICK_REPORT_TRAFFIC_TYPE :
            setQuickReportTrafficType(payload.data);
            break;
        case FilterConstants.ADD_TO_EXCLUDE_SELECTION :
            addToExcludeSelection(payload.data);
            break;
        case FilterConstants.SET_CURRENT_MAP_OBJ :
            setCurrentMapObj(payload.data);
            break;
        case FilterConstants.SET_AUTOMATED_REPORT_BLOCKED :
            setAutomatedReportBlocked(payload.data);
            break;
        case FilterConstants.SET_TEMPLATE_DISPLAYED_IN_DASHBOARD :
            setTemplateDisplayedInDashboard(payload.data);
            break;
        case FilterConstants.OPEN_CLIENT_TYPES_LIGHTBOX :
            openClientTypesLightbox(payload.data);
            break;
        case FilterConstants.LOAD_NEW_FORMAT_ORGANISATIONS :
            loadNewFormatOrganisations();
            break;
        case FilterConstants.START_GOCARDLESS_PROCESS :
            startGocardlessProcess();
            break;
        case FilterConstants.SET_PAYMENT_TYPE :
            setPaymentType(payload.data);
            break;
        case FilterConstants.LOAD_LINK :
            loadLink(payload.data);
            break;
        case FilterConstants.LOAD_RESULTS_IF_NO_PREVIOUS_LOADS :
            loadResultsIfNoPreviousLoads();
            break;
        case FilterConstants.SHOW_SELECTED_COMMON_PATH :
            showSelectedCommonPath();
            break;
        case FilterConstants.SET_COMMON_PATHS_SESSION_IDS :
            setCommonPathsSessionIDs(payload.data);
            break;
        case FilterConstants.SHOW_NEW_TEAM_MEMBER :
            showNewTeamMember(payload.data);
            break;
        case FilterConstants.PROCESS_STRIPE_PAYMENT :
            processStripePayment(payload.data);
            break;
        case FilterConstants.LOAD_REPORT_FILTERS :
            loadReportFilters(payload.data);
            break;
        case FilterConstants.SET_DATE_RANGE :
            setDateRange(payload.data);
            break;
        case FilterConstants.OPEN_EXPORT_TO_CRM :
            openExportToCRM(payload.data);
            break;
        case FilterConstants.SET_CRM_EXPORT_SELECTION :
            setCRMExportSelection(payload.data);
            break;
        case FilterConstants.SET_DISPLAY_PER_PAGE_FOR_LANDING_PAGES :
            setDisplayPerPageForLandingPages(payload.data);
            break;
        case FilterConstants.SET_POSSIBLE_ACTIVE_RECORD_SUGGESTIONS :
            setPossibleActiveRecordSuggestions(payload.data);
            break;
        case FilterConstants.CREATE_SCENARIO_FILTERS :
            createScenarioFilters(payload.data);
            break;
        case FilterConstants.ADVANCED_FILTERS_STRAIGHT_TO_EXPORT :
            advancedFiltersStraightToExport(payload.data);
            break;
        case FilterConstants.FIND_POSSIBLE_ACTIVE_RECORD_SUGGESTIONS :
            findPossibleActiveRecordSuggestions();
            break;
        case FilterConstants.LOAD_INVOICES :
            loadInvoices();
            break;
        case FilterConstants.UPDATE_INVOICE_TO :
            updateInvoiceTo(payload.data);
            break;
        case FilterConstants.STORE_INVOICES :
            storeInvoices(payload.data);
            break;
        case FilterConstants.EDIT_TAG :
            editTag(payload.data);
            break;
        case FilterConstants.UPDATE_VAT_NUMBER :
            updateVATNumber(payload.data);
            break;
        case FilterConstants.STORE_NEW_TAG_DESCRIPTION :
            storeNewTagDescription();
            break;
        case FilterConstants.UPDATE_TAG_DESCRIPTION :
            updateTagDescription(payload.data);
            break;
        case FilterConstants.ORG_HAS_NO_IMAGE :
            orgHasNoImage(payload.data);
            break;
        case FilterConstants.UPDATE_REPORT_DATA :
            updateReportData(payload.data);
            break;
        case FilterConstants.UPDATE_STRIPE_DETAILS :
            updateStripeDetails(payload.data);
            break;
        case FilterConstants.STORE_AUTO_TAGS :
            storeAutoTags(payload.data);
            break;
        case FilterConstants.TURN_ON_AUTO_EXPORT_FOR_CRM :
            turnOnAutoExportForCRM(payload.data);

            break;
        case FilterConstants.START_STRIPE_SESSION_AND_REDIRECT:
            startStripeSessionAndRedirect(payload.data);
            break;
        case FilterConstants.STORE_ZAPIER_IS_SETUP:
            storeZapierIsSetup(payload.data);

            break;
        case FilterConstants.TURN_OFF_AUTO_EXPORT_FOR_CRM:
            turnOffAutoExportForCRM(payload.data);

            break;
        case FilterConstants.STORE_AUTO_EXPORTS:
            setAutoExports(payload.data);

            break;
        case FilterConstants.SET_AUTO_EXPORT:
            setAutoExport(payload.data);

            break;
        case FilterConstants.SET_MICROSOFT_CLARITY_PROJECT_ID:
            setMicrosoftClarityProjectId(payload.data)

            break;
        default :
            break;
    }

    FiltersStore.emitChange();

    return returnRes;
});
export default FiltersStore;
