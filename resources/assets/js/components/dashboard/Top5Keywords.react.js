import React from 'react';
import KeywordLink from './links/KeywordLink.react';
import Paper from 'material-ui/Paper';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const styles = {
  paper: {
    padding: 20,
    height: 600
  },
  refreshIndicator: {
    display: 'inline-block',
    position: 'relative',
  }
}

function getCurrentState(props) {
  return {
    keywords: props.keywords,
    isApplicationResting: props.isApplicationResting
  };
}

var Top5Keywords = React.createClass({
  getInitialState: function () {
    return getCurrentState(this.props);
  },
  shouldComponentUpdate: function (nextProps) {
    var update = false;

    if (nextProps.keywords.length > 0) {
      if (this.props.keywords.length === 0) {
        update = true;
      } else if (this.props.keywords[0][1] !== nextProps.keywords[0][1]) {
        update = true;
      }
    }

    if (this.props.isApplicationResting !== nextProps.isApplicationResting) {
      update = true;
    }

    return update;
  },
  componentWillReceiveProps: function (newProps) {
    this.setState(getCurrentState(newProps));
  },
  render: function () {
    if (!this.state.isApplicationResting) {
      return (
          <div className="visitorDetails">
            <Paper style={styles.paper} zDepth={4}>
              <div className="blueHeader">
                <h4>Paid Ads Keywords</h4>
              </div>

              <RefreshIndicator
                  size={70}
                  left={0}
                  top={20}
                  status="loading"
                  style={styles.refreshIndicator}
              />
              <br/><br/>
            </Paper>
          </div>
      )
    }

    var keywordsMain = this.state.keywords.slice(0, 10);

    var keywords = keywordsMain.map(function (keyword, i) {
      var keyword = keyword;
      let displayKeyword = keyword[1];
      const spaceCount = (displayKeyword.split(" ").length - 1);

      if (displayKeyword.length > 20 && spaceCount === 0) {
        displayKeyword = displayKeyword.substring(0, 15) + " ... ";
      } else if (displayKeyword.length > 40) {
        displayKeyword = displayKeyword.substring(0, 36) + " ... ";
      }

      return (
          <tr key={i}>
            <td style={{width: '70%'}}><KeywordLink key={i} keywordId={keyword[0]} keyword={displayKeyword}/></td>
            <td style={{textAlign: 'right'}}>{keyword[2]}</td>
          </tr>

      )
    });

    return (
        <div className="visitorDetails">
          <Paper style={styles.paper} zDepth={4}>
            <div className="blueHeader">
              <h4>Paid Ads Keywords</h4>
            </div>
            <br/>
            <table style={{overflow: 'hidden'}} className="table">
              <tbody>
              {keywords}
              </tbody>
            </table>
          </Paper>
        </div>
    )
  }
});

export default Top5Keywords;
