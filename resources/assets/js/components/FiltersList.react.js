import React         		from 'react';
import DateFrom      		from './DateFrom.react';
import DateTo        		from './DateTo.react';
import IPFilterItem  		from './ipFilters/IPFilterItem.react';
import SpecificIPFilter 	from './ipFilters/SpecificIPFilter.react';
import KeywordFilter 		from './KeywordFilter.react';
import CustomFilter 		from './CustomFilter.react';
import OrganisationFilter 	from './OrganisationFilter.react';
import TimeFrom 			from './TimeFrom.react';
import TimeTo 				from './TimeTo.react';
import KeywordValue 		from './KeywordValue.react';
import HideDrawers 			from './HideDrawers.react';
import DrawerToolbar 		from './DrawerToolbar.react';
import IncludeTagFilter 	from './IncludeTagFilter.react';
import ExcludeTagFilter 	from './ExcludeTagFilter.react';
import BankedFilter 		from './BankedFilter.react';
import VisitorTypeFilter 	from './VisitorTypeFilter.react';
import DeviceFilter 		from './DeviceFilter.react';
import BrowserFilter 		from './BrowserFilter.react';
import OSFilter 			from './OSFilter.react';
import PreviousVisits 		from './PrevVisitsFilter.react';

const style = {

	button 	: 	{
  
  		margin 	: 12
  	}
};

function getCurrentState( newProps ) {

	return {

		showToolbar 	: newProps.showToolbar,
		filters 		: newProps.filters,
		filtersChange 	: newProps.filtersChange
	}
}

var FiltersList = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props === nextProps && 
			this.props.filters === nextProps.filters &&
			this.props.filtersChange === nextProps.filtersChange ) {

			return false;
		}

		return true;
	},

	render: function() {

		var filters     		= this.state.filters;

		var bankedFilters 		= [];

		var dateFilterItems 	= [];

		var ipFilterItems 		= [];

		var keywordItems 		= [];

		var keywordValues 		= [];

		var customItems 		= [];

		var organisationFilters = [];

		var timeFilterItems 	= [];

		var includeTagFilters 	= [];

		var excludeTagFilters 	= [];

		var visitorTypes 		= [];

		var deviceFilters 		= [];

		var browserFilters 		= [];

		var osFilters 			= [];

		var previousHistory 	= [];


		for (var key in filters) {

			if( !filters[key] ) {

				continue;
			}

			switch( filters[key].type ) {

				case "setSessionIDs" :

					previousHistory.push( <PreviousVisits key={key} myKey={key} data={filters[key]} storedValue={filters[key].storedValue} value={filters[key].storedValue.textValue} /> );

					break;

				case "banked" :

					bankedFilters.push( <BankedFilter key={key} myKey={key} storedValue={filters[key].storedValue} value={filters[key].id} /> );

					break;

				case "includeTagFilter" :

					includeTagFilters.push( <IncludeTagFilter key={key} myKey={key} storedValue={filters[key].storedValue} value={filters[key].id} textValue={filters[key].storedValue.textValue} /> );

					break;

				case "excludeTagFilter" :

					excludeTagFilters.push( <ExcludeTagFilter key={key} myKey={key} storedValue={filters[key].storedValue} value={filters[key].id} textValue={filters[key].storedValue.textValue} /> );

					break;

				case "CustomFilterKeyword" :

					keywordValues.push( <KeywordValue key={key} myKey={key} storedValue={filters[key].storedValue} textValue={filters[key].storedValue.textValue} /> );

					break;
				
				case "dateFrom" :

					dateFilterItems.push( <DateFrom key={key} myKey={key} storedValue={filters[key].storedValue} /> );

					break;
				
				case "dateTo" :

					dateFilterItems.push( <DateTo key={key} myKey={key} storedValue={filters[key].storedValue} /> );

					break;
				

				case "IPRange" :

					ipFilterItems.push( <IPFilterItem key={key} myKey={key} storedValue={filters[key].storedValue} /> );

				break;

				case "specificIP" :

					ipFilterItems.push( <SpecificIPFilter key={key} myKey={key} storedValue={filters[key].storedValue} /> );

				break;

				case "keyword" :

					keywordItems.push( <KeywordFilter key={key} storedValue={filters[key].storedValue.keywordId} myKey={key} textValue={filters[key].storedValue.keywordText} /> );

				break;

				case "CustomFilterURL" :

					customItems.push( <CustomFilter key={key} myKey={key} type="URL" criteria={filters[key].storedValue.criteria} textValue={filters[key].storedValue.textValue} value={filters[key].storedValue.value}  /> );

				break;

				case "customFilterTotalVisits" :

					customItems.push( <CustomFilter key={key} myKey={key} type="Total Visits" criteria={filters[key].storedValue.criteria} value={filters[key].storedValue.value}  /> );

				break;

				case "customFilterReferrerCountry" :

					customItems.push( <CustomFilter key={key} myKey={key} type="Referrer Country" criteria={filters[key].storedValue.criteria} textValue={filters[key].storedValue.textValue} value={filters[key].storedValue.value}  /> );


				break;

				case "customFilterReferrer" :

					customItems.push( <CustomFilter key={key} myKey={key} type="Referrer" criteria={filters[key].storedValue.criteria} textValue={filters[key].storedValue.textValue} value={filters[key].storedValue.value}  /> );

				break;


				case "customFilterTimeOnSite" :

					customItems.push( <CustomFilter key={key} myKey={key} type="Time on Site" criteria={filters[key].storedValue.criteria} value={filters[key].storedValue.value}  /> );

				break;

				case "organisationFilter" :

					organisationFilters.push( <OrganisationFilter key={key} myKey={key} type="Organisation Filter" criteria={filters[key].storedValue.include} data={filters[key].storedValue} value={filters[key].storedValue.name} /> )

				break;

				case "timeFrom" :

					timeFilterItems.push( <TimeFrom key={key} myKey={key} /> );

				break;


				case "timeTo" :

					timeFilterItems.push( <TimeTo key={key} myKey={key} /> );

				break;

				case "visitorTypeFilter" :

					visitorTypes.push( <VisitorTypeFilter key={key} myKey={key} data={filters[key]} /> );

				break;

				case "deviceFilter" :

					deviceFilters.push( <DeviceFilter key={key} myKey={key} data={filters[key]} /> );

				break;

				case "browserFilter" :

					browserFilters.push( <BrowserFilter key={key} myKey={key} data={filters[key]} /> );

				break;	

				case "osFilter" :

					osFilters.push( <OSFilter key={key} myKey={key} data={filters[key]} /> );

				break;
			}
		}

		var drawerToolbar = "";

		if( this.state.showToolbar ) {

			drawerToolbar = <DrawerToolbar />;
		}

		var displayFilters = (

			<div>
				{dateFilterItems}
				{timeFilterItems}
				{bankedFilters}
				{ipFilterItems}
				{keywordItems}
				{customItems}
				{organisationFilters}
				{keywordValues}
				{includeTagFilters}
				{excludeTagFilters}
				{visitorTypes}
				{deviceFilters}
				{browserFilters} 	
				{osFilters}
				{previousHistory}
			</div>
		);

		if( previousHistory.length > 0 ) {

			displayFilters = (

				<div>
					{previousHistory}
				</div>
			)
		}

		return (
			<section id="main">

				{drawerToolbar}

			
				<h3>Applied Filters</h3>
				<br />

				<div id="filters-list">
					{displayFilters}

				</div>
			
			</section>
		);
	}
});

export default  FiltersList;