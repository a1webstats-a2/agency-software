<?php
declare(strict_types=1);

namespace App\Http\Controllers\System\SSL;

use App\Http\Controllers\Controller;

final class Install extends Controller
{
    public function index()
    {
        $url = $_SERVER['HTTP_HOST'];

        $execCommand = sprintf(
            'certbot run -n --redirect -d %s',
            $url
        );

        exec($execCommand);
    }
}