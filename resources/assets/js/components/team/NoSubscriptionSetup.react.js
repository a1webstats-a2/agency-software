import React from 'react';
import Step1 from '../payment/Step1.react';
import Step2 from '../payment/Step2.react';
import PreviousInvoices from "../payment/PreviousInvoices.react";
import {Tab, Tabs} from "material-ui/Tabs";

function getCurrentState( props ) {

	return {

		settings 	: 	props.settings
	}
}

var NoSubscriptionSetup = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {
		var display = "";

		if (!this.state.settings.addr1) {
			display = <Step1 settings={this.state.settings} />
		} else {
			display = <Step2 settings={this.state.settings} pricing={this.state.pricing} />
		}

		return (
			<div>
                <Tabs>
                    <Tab label="Create new subscription">
						<div className="clr"></div><br />
						<h2>You currently have no subscription setup</h2>

						<div className="clr"></div><br />

						<p className="standOutText"><strong>We have no minimum contract! You can cancel at any time!</strong></p>

						<div className="clr"></div><br />

						{display}
					</Tab>
					<Tab label="Previous invoices">
                        <div className="clr"></div><br />
						<PreviousInvoices />
					</Tab>
				</Tabs>
			</div>
		)
	}
});

export default  NoSubscriptionSetup;