@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>

@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

				<div id="loginLogo">
                    <img src="/images/{{ $loginLogo }}" />
                </div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<div class="col-md-6">

						<ul class="progress-indicator">
				            <li class="completed">
				                <span class="bubble"></span>
				                Step 1
				            </li>
				            <li class="completed">
				                <span class="bubble"></span>
				                Step 2
				            </li>
				            <li>
				            	<span class="bubble"></span>
				            	Finished!
				            </li>
				        	
				        </ul>

				    </div>
				</div>

				<h1>Signup - Step 2 - Nearly there!</h1>

				<p>{{ $configData['name'] }} offers so much more than purely identifying companies that visit your website.</p>

				<p>Please indicate your areas of interest below and our system will deliver information that’s closely linked to your interests, where 10 represents <strong>high interest</strong> and 0 represents <strong>no interest</strong></p>


				<div class="clr"></div><br /><br /><br />


				<form method="POST">

					@foreach( $questions as $question )

						<div class="row">
							<div class="col-md-10">
								<label>{{ $question->question }}</label>
							</div>
							<div class="col-md-2">
								<select class="form-control" name="answer{{ $question->id }}">

									<?php 

									for( $i = 10; $i >= 0; $i-- ) {

										$selected = ( $i === 0 ) ? ' selected="selected" ' : '';
									?>

										<option {{ $selected }} value="{{ $i }}">{{ $i }}</option>
								
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="clr"></div><br /><br />

					@endforeach

					<div class="clr"></div><br /><br />

					<p>If you have any other specific information about your website visitors that you’d be interested in, please enter that below.</p>

					<textarea id="otherSpecificInformationTextarea" name="otherSpecificInformation"></textarea>

					<div class="clr"></div><br /><br />

					<div class="row">

						<div class="form-group">

							<button type="submit" class="signupButton">Complete Sign Up</button>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection