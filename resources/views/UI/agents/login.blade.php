@extends( 'templates.agents' )

@section( 'content' )

	<div class="row">

		<div class="col-md-12">

			<h3>Login</h3><br />

			<form>

				<div class="form-group">

					<label for="email">Email Address</label>
					<input type="email" class="form-control" placeholder="your@emailaddress.com">

				</div>

				<div class="form-group">

					<label for="password">Password</label>
					<input type="password" class="form-control" placeholder="********">

				</div>

				<div class="form-group">

					<button class="btn btn-primary" type="submit">Login</button>
				</div>

			</form>

		</div>
	</div>
	

@endsection