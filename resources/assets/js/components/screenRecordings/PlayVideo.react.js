import MouseCursor 				from './MouseCursor.react';
import Background 				from './Background.react';
import React 					from 'react';
import VideoControls 			from './VideoControls.react';
import ReactDOM 				from 'react-dom';
import ScreenRecorderActions 	from '../../actions/ScreenRecorderActions';

function getCurrentState( props ) {

	return {

		scrollTo 			: 	props.scrollTo,
		displayOffsetLeft 	: 	props.displayOffsetLeft,
		displayOffsetTop 	: 	props.displayOffsetTop,
		firstScreenshot		: 	props.firstScreenshot,
		videoIsLoaded		: 	props.videoIsLoaded,
		playStatus			: 	props.playStatus,
		mouseX				: 	props.mouseX,
		mouseY				: 	props.mouseY,
		background			: 	props.background,
		aspectRatio 		: 	props.aspectRatio

	}
}

class PlayVideo extends React.Component {

	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );

	}

	componentDidMount() {

	
		this._updateDisplayOffsets();
		window.addEventListener( "resize", this._updateDisplayOffsets.bind( this ) );

	}

	componentWillUnmount() {

		window.removeEventListener( "resize", this._updateDisplayOffsets.bind( this ) );
	}

	componentWillReceiveProps( nextProps ) {

		this.setState( getCurrentState( nextProps ) );
	}

	render() {

		let play = false;

		let that = this;

		if( this.state.videoIsLoaded && this.state.playStatus === 1 ) {

			play 	= true;

		}

		return (

			<div ref="videoPlayer" className="videoPlayer">
				<Background 
					background={this.state.background} 
					firstScreenshot={this.state.firstScreenshot}
					aspectRatio={this.state.aspectRatio} 
					scrollTo={this.state.scrollTo}
					play={play}
				/> 
				<MouseCursor 
					scrollTo={this.state.scrollTo}
					displayOffsetLeft={this.state.displayOffsetLeft}
					displayOffsetTop={this.state.displayOffsetTop}
					x={this.state.mouseX} 
					y={this.state.mouseY}
					aspectRatio={this.state.aspectRatio} 

				/>

			</div>
		)
	}

	_updateDisplayOffsets() {

		let element 		= ReactDOM.findDOMNode( this );
		let elementOffsets 	= element.getBoundingClientRect();
		ScreenRecorderActions.setDisplayOffsets( elementOffsets );
	}
}

export default PlayVideo;