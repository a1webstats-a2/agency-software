import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import QuickExport from '../export/QuickExport.react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import NotificationsStore from '../../stores/NotificationsStore';
import NavBar from '../navigation/NavBar.react';
import SnackbarA1 from '../Snackbar.react';
import Footer from '../Footer.react';
import QuickExportCheckbox 	from '../export/QuickExportCheckbox.react';
import UserTipHelpLink from '../UserTipHelpLink.react';

function getCurrentState( props ) {

	const analyticsData = FiltersStore.getAnalyticsData();

	return {

		scenariosFilterObj 		: 	FiltersStore.getScenarioFiltersObj(),
		displayPerPage 			: 	FiltersStore.getDisplayPerPageOnLandingPages(),
		landingPageNo 			: 	FiltersStore.getCurrentLandingPageNo(),
		looseFilterSetDisplay 	: 	FiltersStore.getLooseFilterSetDisplay(),
		quickExportSearchString : 	FiltersStore.getQuickExportSearchString(),
		quickExportResults 		: 	FiltersStore.getQuickExportSearchResults(),
		latestAddedFilters 		: 	FiltersStore.getLatestAddedFilters(),
		displayFancyFilters 	: 	FiltersStore.showFancyFilters(),
		filters 				: 	FiltersStore.getFilters(),
 		settings 				: 	FiltersStore.getAllSettings(),
 		heatmapData 			: 	analyticsData.heatmapData,
		data 					: 	analyticsData.entryPages,
		quickExportSettings 	: 	FiltersStore.getQuickExportSettings(),
		isApplicationResting 	: 	FiltersStore.isApplicationResting(),
		snackbarSettings  		:	FiltersStore.getSnackbarSettings(),
		notificationsData 		: 	{
			
			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),
			accountOK 				: 	FiltersStore.accountIsOK(),
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()


		}
	}
}

function getPaginatedResults( organisations, landingPageNo, perPage = 10 ) {

	var startPage 	= landingPageNo * perPage;
	return organisations.splice( startPage, perPage );
}



var EntryPagesDashboard = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	componentDidMount: function() {

		FiltersStore.addChangeListener( this._onChange );

		FilterActions.selectAllOptionsIfSelectionEmpty();

	},

	componentWillUnmount: function() {

		FiltersStore.removeChangeListener( this._onChange );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.state.looseFilterSetDisplay !== nextState.looseFilterSetDisplay ) {

			return true;
		}
		
		if( this.state.isApplicationResting !== nextState.isApplicationResting ) {

			return true;
		}

		if( this.state.quickExportResults !== nextState.quickExportResults ) {

			return true;
		}

		if( this.state.quickExportSearchString !== nextState.quickExportSearchString ) {

			return true;
		}

		if( this.state.data !== nextState.data ) {

			return true;
		}

		if( this.state.quickExportSettings !== nextState.quickExportSettings ) {

			return true;
		}

		if( JSON.stringify( this.state.snackbarSettings ) !== JSON.stringify( nextState.snackbarSettings ) ) {

			return true;
		}

		if( this.state.notificationsData !== nextState.notificationsData ) {

			return true;
		}

		return false;
	},

	render : function() {

		var resultsCount 	= ( typeof this.state.quickExportResults !== "undefined" ) ? this.state.quickExportResults.length : 0;
		var perPage 		= this.state.displayPerPage;
		var currentPage 	= ( this.state.landingPageNo + 1 );
		var disableNext 	= ( ( ( this.state.landingPageNo * perPage ) + perPage ) >= resultsCount ) ? true : false;
		var totalPages 		= ( typeof this.state.quickExportResults !== "undefined" ) ? Math.ceil( resultsCount / perPage ) : 0;
		var disablePrev 	= ( this.state.landingPageNo === 1 ) ? true : false;

		var paginationStats = (

			<div>

				<p><strong>{this.state.quickExportResults.length}</strong> results | Page <strong>{currentPage}</strong> of <strong>{totalPages}</strong></p><br />

				<div className="clr"></div>

			</div>
		)

		var display = '';

		if( !this.state.isApplicationResting ) {

			var trs = [];
		
		} else {

			var showResults = getPaginatedResults( this.state.quickExportResults, this.state.landingPageNo, this.state.displayPerPage );

			var trs = showResults.map( function( page, i ) {

				var selected 	= false;

				if( typeof this.state.quickExportSettings.selection[parseInt( page[0] )] !== "undefined" ) {

					selected = true;
				}

				let heatmapDisplay = "";

				if( typeof this.state.heatmapData[parseInt( page[0] )] !== "undefined" &&
					this.state.settings.team.end_user_company_id === 1653 ) {

					const linkURL = "/heatmap/" + parseInt( page[0] );

					heatmapDisplay = (

						<div>
							<a href={linkURL} target="_blank">
								<img src="/images/thermometer.png" />
							</a>
						</div>
					)
				}

				return (

					<TableRow  selectable={false} key={i}>
						<TableRowColumn style={{ width : '10%', background : '#fff' }}>

								<QuickExportCheckbox 
		        					isChecked={selected} 
		        					quickExportResults={this.state.quickExportSettings.selection}
		        					data={page} />
		        			</TableRowColumn>
						<TableRowColumn style={{ width : '70%', background : '#fff' }}>{page[1]}<br /><span style={{ color : '#ccc' }}>{page[3]}</span></TableRowColumn>
	        			<TableRowColumn style={{ width : '8%' }}>
							{heatmapDisplay}
						</TableRowColumn>
	        			<TableRowColumn style={{ background : '#fff', textAlign : 'right' }}>{page[2]}</TableRowColumn>
					</TableRow>
				)
			}.bind( this ) );

			if( showResults.length > 0 ) {

				display = (

					<Table 
					 	onRowSelection={this._addToExportSelection}
					 	multiSelectable={true}
					 >
						<TableHeader
							adjustForCheckbox={false}
							enableSelectAll={false}
							displaySelectAll={false}
						>
							<TableRow>
								<TableHeaderColumn style={{ width : '10%' }}>Select</TableHeaderColumn>
								<TableHeaderColumn style={{ width : '80%' }}>URL</TableHeaderColumn>
								<TableHeaderColumn>Total Visits</TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody 
							deselectOnClickaway={false}
							displayRowCheckbox={false}
						>
							{trs}
						</TableBody>
					</Table>
				)
			
			} else {

				display = (

					<div className="alert alert-danger">

						No results found
					</div>
				)
			}
		}

		return (

			<div className="reportBuilder">

				<NavBar data={this.state.notificationsData} />
			
				<div className="clr"></div>

				<div className="container mainContainer">
					
					<div className="row">

						<div className="col-md-3">

							<div className="displayOptions">

								<QuickExport 
									type="entryPages"
									filterType="CustomFilterEntryPage"
									landingPageNo={this.state.landingPageNo}
									disableNext={disableNext}
									displayPerPage={this.state.displayPerPage}
									applicationResting={this.state.isApplicationResting}
									fileTypes={this.state.quickExportSettings.fileTypes} 
									selectionType={this.state.quickExportSettings.selectionType}
									selection={this.state.quickExportSettings.selection} />

							</div>
						</div>

						<div className="col-md-9" id="results">

							<div className="row">
								<div className="col-md-2">

									<h3>Entry Pages</h3>
								</div>
								<div className="col-md-10">
									<div style={{ marginTop : 25 }}>
										<UserTipHelpLink />
									</div>
								</div>
							</div>

							<div className="clr"></div>
							
							{paginationStats}

							<div className="clr"></div>

							{display}
							

							<div className="clr"></div><br />
							

							<br />{paginationStats}
							<div className="clr"></div><br />

						</div>
					</div>
						
				</div>

				<div className="clr"></div><br /><br /><br />

				<Footer 
					looseFilterSetDisplay={this.state.looseFilterSetDisplay}
					scenariosFilters={this.state.scenariosFilterObj}
					isApplicationResting={this.state.isApplicationResting}
					settings={this.state.settings}
					filters={this.state.filters}
					display={this.state.displayFancyFilters}
					latestAddedFilters={this.state.latestAddedFilters}
				/>

				<SnackbarA1 snackbarSettings={this.state.snackbarSettings} />

			</div>

		)
	},

	_addToExportSelection : function( selectedRows ) {

		var returnArray 	= [];

		var newResults 		= [];

		if( this.state.quickExportSearchString !== "" ) {

			newResults = this.state.data.filter( function( page, i ) {


				return page[1].toLowerCase().includes( this.state.quickExportSearchString.toLowerCase() );

			}.bind( this ) );
		
		} else {

			newResults = this.state.data;
		}

		var mappingObj = newResults;

		if( selectedRows === "all" ) {

			newResults.map( function( selected, i ) {


				returnArray[parseInt( selected[0] )] = selected[1];
			})

		} else {

			selectedRows.map( function( selected, i ) {

				var page = newResults[selected];

				returnArray[parseInt( page[0] )] = page[1];
			})
		}

		
		FilterActions.setAddOrRemoveQuickExportSelection( returnArray );

	},

	_onChange : function() {

		this.setState( getCurrentState() );
	}
});

export default  EntryPagesDashboard;