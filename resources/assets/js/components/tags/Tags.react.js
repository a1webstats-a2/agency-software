import React 				from 'react';
import FiltersStore 		from '../../stores/FiltersStore';
import NavBar 				from '../navigation/NavBar.react';
import NotificationsStore 	from '../../stores/NotificationsStore';
import {Tab,Tabs} 			from 'material-ui/Tabs';
import FilterActions 		from '../../actions/FilterActions';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import Exclusion 			from './Exclusion.react';
import Tag 					from './Tag.react';
import Dialog 				from 'material-ui/Dialog';
import FlatButton 			from 'material-ui/FlatButton';
import RaisedButton 		from 'material-ui/RaisedButton';
import TextField 			from 'material-ui/TextField';
import SnackbarA1 			from '../Snackbar.react';
import UserTipHelpLink 		from '../UserTipHelpLink.react';
import UserTips 			from '../UserTips.react';

function getCurrentState( props ) {

	return {
		
		snackbarSettings  		:	FiltersStore.getSnackbarSettings(),
		editTag 				: 	FiltersStore.getEditTag(),
		editTagOpen 			: 	FiltersStore.checkLightboxOpen( 'editTag', -1 ),
		tags 					: 	FiltersStore.getClientTypes(),
		exclusions 				: 	FiltersStore.getExclusions(),
		notificationsData 		: 	{
	
			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),		
			accountOK 				: 	FiltersStore.accountIsOK(),
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()
		}
	}
}

var Tags = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	componentDidMount : function() {

		FilterActions.loadExclusions();
		FiltersStore.addChangeListener( this._onChange );
	},

	componentWillUnmount: function() {

		FiltersStore.removeChangeListener( this._onChange );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		const tableRows 	= 	[];

		const tags 			= 	[];

		const masterTags 	=	[  4, 12, 2,  ];

		for( let t in this.state.tags ) {

			if( this.state.tags[t].id > 12 ) {

				tags.push( 
				
					<Tag 
						key={t} 
						tagID={this.state.tags[t].id} 
						tagDescription={this.state.tags[t].type}
					/>
				);
			}
		}

		for( let i in this.state.exclusions ) {

			let exclusion = this.state.exclusions[i];

			tableRows.push(

				<Exclusion key={i} exclusionID={i} organisation={exclusion} />
			)
		}

		const actions = [

			<FlatButton 
				label="Update"
				primary={true}
				onTouchTap={this._updateTag}
			/>,
			
			<FlatButton
				label="Close"
				secondary={true}
				onTouchTap={this._handleClose}
			/>
		];

		return (

			<div className="reportBuilder">

				<NavBar data={this.state.notificationsData} />
			
				<div className="clr"></div>

				<div className="container mainContainer">
					
					<div className="row">

						<div className="col-md-12" id="results">

							<div className="row">

								<div className="col-md-1">

									<h3>Tags</h3>
								</div>

								<div className="col-md-11">

									<div style={{ marginTop : 27 }}>
										<UserTipHelpLink />
									</div>
								</div>
							</div>

							<Tabs>
						    	<Tab label="Your Tags">
						    		<Table>
										<TableHeader
											adjustForCheckbox={false}
											displaySelectAll={false}
										>
											<TableRow>
												<TableHeaderColumn>Tag</TableHeaderColumn>
												<TableHeaderColumn>Actions</TableHeaderColumn>
											</TableRow>
										</TableHeader>
										<TableBody>
											{tags}
										</TableBody>
									</Table>
								</Tab>
								<Tab label="Exclusions">
									<Table>
										<TableHeader
											adjustForCheckbox={false}
											displaySelectAll={false}
										>
											<TableRow>
												<TableHeaderColumn>Organisation</TableHeaderColumn>
												<TableHeaderColumn>Actions</TableHeaderColumn>
											</TableRow>
										</TableHeader>
										<TableBody>
											{tableRows}
										</TableBody>
									</Table>
								</Tab>
							</Tabs>
						</div>
					</div>
				</div>

				<Dialog
		        	title="Edit Tag"
		        	actions={actions}
		        	modal={false}
		        	open={this.state.editTagOpen}
		        	onRequestClose={this._handleClose}
		        >
		        	<TextField
		        		id="tagName"
		        		onChange={this._updateTagDescription}
						defaultValue={this.state.editTag.description}
					/><br />
		        </Dialog>

				<SnackbarA1 snackbarSettings={this.state.snackbarSettings} />
				<UserTips />

			</div>
		)
	},

	_updateTagDescription : function( event, newText ) {

		FilterActions.updateTagDescription( newText );
	},

	_updateTag : function() {

		FilterActions.storeNewTagDescription();
	},

	_handleClose : function() {

		FilterActions.closeLightbox();
	},

	_onChange: function() {

	    this.setState( getCurrentState() );
	}
});

export default Tags;