import React from 'react';
import FilterStore from '../../stores/FiltersStore';
import CountryCheckbox from './CountryCheckbox.react';
import FilterActions from '../../actions/FilterActions';
import SearchBy from '../customFilters/SearchBy.react';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';

import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';

const styles = {

  	toggle: {
    	marginBottom: 16,
  	},

  	raisedButton : {

  		marginRight 	: 	12,
  		marginTop 		: 	0,
  		marginBottom 	: 	40
  	},

  	checkbox: {
   	 	
   	 	marginBottom: 16,
  	},

  	radioButton: {
    	marginBottom: 10,
    	marginTop: 10
  	}
};



function getInitialState( props ) {

	var countriesData = props.countriesData;

	return {

		filtersBeingEdited 	: 	countriesData.filtersBeingEdited,
		searchCriteria 		: 	countriesData.searchCriteria,
		allCountryFilters 	: 	countriesData.allCountryFilters,
		allCountries 		: 	countriesData.allCountries,
		exclude 			: 	countriesData.exclude,
		active  			: 	props.active,
		selectAll 			: 	countriesData.selectAll,
		searchByData 		: 	props.searchByData
	}
}

function sortInclude( b, a ) {

	if ( a.include_count < b.include_count )

		return -1;

	if ( a.include_count > b.include_count )

		return 1;

	return 0;
}

function sortExclude( b, a ) {

	if ( a.exclude_count < b.exclude_count )

		return -1;

	if ( a.exclude_count > b.exclude_count )

		return 1;

	return 0;
}

var CountryFilters = React.createClass({

	getInitialState : function() {

		return getInitialState( this.props );
			
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.searchByData.countryIncludeType !== nextProps.searchByData.countryIncludeType ) {

			return true;
		}

		if( this.props.countriesData.filtersBeingEdited !== nextProps.countriesData.filtersBeingEdited ) {

			return true;
		}

		if( nextProps.searchByData.searchString !== this.props.searchByData.searchString ) {

			return true;
		}

		if( this.props.countriesData.searchCriteria !== nextProps.countriesData.searchCriteria ) {

			return true;
		}
			
		if( this.props.countriesData.allCountryFilters !== nextProps.countriesData.allCountryFilters ) {

			return true;
		}
			
		if( this.props.countriesData.activeTab !== nextProps.countriesData.activeTab ) {

			return true;
		}
			
		if( this.props.countriesData.allCountries !== nextProps.countriesData.allCountries ) {

			return true;
		}
			
		if( this.props.countriesData.exclude !== nextProps.countriesData.exclude ) {

			return true;
		}
			
		if( this.props.countriesData.active !== nextProps.countriesData.active ) {

			return true;
		}
			
		if( this.props.countriesData.selectAll !== nextProps.countriesData.selectAll ) {

			return true;
		}
		
		if( this.props.countriesData.narrowedOptions !== nextProps.countriesData.narrowedOptions ) {

			return true;
		}
		
		if( this.props.countriesData.filterChange !== nextProps.countriesData.filterChange ) {

			return true;
		}

		return false;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getInitialState( newProps ) );
	},


	

	render : function() {

		var checkboxesLeft 	= [];

		var checkboxesRight = [];

		var halfNumCheckboxes 	= this.state.allCountries.length / 2;

		switch( this.state.exclude ) {

			case true :

				this.state.allCountries.sort( sortInclude );

				break;

			case false :

				this.state.allCountries.sort( sortExclude );

				break;
		}

		this.state.allCountries.map( function( country, i ) {

			var countryCheckbox = <CountryCheckbox searchByData={this.state.searchByData} filtersBeingEdited={this.state.filtersBeingEdited} searchByCheckbox={false} checked={this.state.selectAll} key={i} myKey={i} country={country} />

			if( i > halfNumCheckboxes ) {

				checkboxesLeft.push( countryCheckbox );
			
			} else {

				checkboxesRight.push( countryCheckbox );
			}


		}.bind( this ));

		var radioIncludeValue = ( this.state.exclude ) ? 'include' : 'exclude';

		return (

			<div className="countriesContainer">

				<div className="row">
					<div className="col-md-12">
						<br /><h3>Country Filters</h3>
					</div>
				</div>

				<div className="clr"></div>


				<div className="row">

					<div className="col-md-6">
					

					    <RadioButtonGroup onChange={this._saveIncludeExcludeOption} name="includeExclude" defaultSelected={radioIncludeValue}>
							<RadioButton
								value="include"
								label="Include"
								style={styles.radioButton}
							/>
							<RadioButton
								value="exclude"
								label="Exclude"
								style={styles.radioButton}
							/>
								
						</RadioButtonGroup>
					</div>
				</div>

				<div className="clr"></div>

				<SearchBy searchByData={this.state.searchByData} type="customFilterReferrerCountry" />

				<div className="clr"></div><br />

				<div className="row">

					<div className="col-md-12">

						<RaisedButton label="Select All" onClick={this._selectAll} style={styles.raisedButton} primary={true} />

						<RaisedButton label="Deselect All" onClick={this._deselectAll} style={styles.raisedButton} secondary={true} />

						
					</div>
				</div>

				<div className="clr"></div>

				<div className="row">

					<div className="col-md-6">
						{checkboxesRight}
					</div>

					<div className="col-md-6">
						{checkboxesLeft}
					</div>
				</div>
			</div>
		)
	},

	_deselectAll : function() {

		FilterActions.setAllSelected({

			type 			: 	'customFilterReferrerCountry',
			allSelected 	: 	false
		});

		this.setState({

			selectAll : false
		})

		var removeFilters = this.state.allCountries.map( function( country, i ) {

	   		return {

	   			type 	: 	'customFilterReferrerCountry',
	   			id 		: 	country.id
	   		}
		});	

		FilterActions.removeFiltersByType( removeFilters );

	},

	_selectAll : function() {

		FilterActions.setAllSelected({

			type 			: 	'customFilterReferrerCountry',
			allSelected 	: 	true
		});

		this.setState({

			selectAll : true
		})

		var criteria = ( this.state.exclude ) ? 'include' : 'exclude';

		var newFilters 	=	this.state.allCountries.map( function( country, i ) {

			return {

	   			type          :   'customFilterReferrerCountry', 
	   			storedValue   :   {

	   				criteria 	: 	criteria,
	   				value 		: 	country.id, 
	   				textValue 	: 	country.name,
	   				searchBox 	: 	false,
	   				id 			: 	country.id
	   			},
	            id           :   country.id   
        	}

   		}.bind( this ) );

   		FilterActions.createFilters( newFilters );
	},

	_saveIncludeExcludeOption : function( event, value ) {

		var exclude = ( value.trim() === "exclude" ) ? false : true;

		FilterActions.setCountryInclusionType({ exclude : exclude });
	}
});

export default  CountryFilters;