import React from 'react';

function getCurrentState( props ) {

	return {
		
		scrollTo 			: 	props.scrollTo,
		displayOffsetTop 	: 	props.displayOffsetTop,
		displayOffsetLeft 	: 	props.displayOffsetLeft,
		aspectRatio 		: 	props.aspectRatio,
		mouseX 				: 	props.x,
		mouseY 				: 	props.y
	}
}

class MouseCursor extends React.Component {

	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );

	}

	shouldComponentUpdate( nextProps, nextState ) {

		return true;
	}

	componentWillReceiveProps( nextProps ) {

		this.setState( getCurrentState( nextProps ) );

	}

	render() {

		const top 	= this.state.mouseX - this.state.scrollTo;
		const left 	= this.state.mouseY + 36;

		let display = "";

		if( this.state.mouseX === 0 ) {

			display = (

				<div></div>

			)
		
		} else {
	
			display = (

				<div style={{ position : 'absolute', top : top, left : left }}>
					<img src="/images/cursor.png" />
				</div>

			)
		}

		return (

			<div>
				{display}
			</div>
		)
	}
}

export default MouseCursor;