@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>


@section( 'content' )
	
	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

				<div id="signupLogo">


				</div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<div class="col-md-8">
						<h2>{{ $company->company_name }} Feedback</h2>

						<p>On a scale of 1-10, where 1 is strongly disagree, and 10 is strongly agree …</p>

						<div class="clr"></div><br />

						<form action="{{ $agentDetails->system_website }}/end-of-trial/feedback">
							<fieldset>
								<input type="hidden" name="subscriberID" value="{{ $company->id }}" />
							
								@foreach( $questions as $question )
									<div class="form-group">
										<label>{{ str_replace( '[[[companyName]]]', $agentDetails->company, $question->question ) }}</label>

										<select style="float: right" name="question{{ $question->id }}" selected="5">
											@for( $i = 1; $i <= 10; $i++ ) 
												<option>{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="clr"></div><br />
								@endforeach

								<p>Any other comments:</p>

								<textarea name="other" cols="75" rows="10"></textarea>

								<div class="clr"></div><br /><br />

								<button class="btn btn-primary">Send</button>
							</fieldset>
						</form>	
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
