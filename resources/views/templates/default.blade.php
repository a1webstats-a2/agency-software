<?php 
if ($agentConfig['agentID'] === 12) {
    $favicon = env( 'API_SOURCE' ) . '/favicon.ico';
} else {
    $favicon = 's3://hostimages1/favicons/' . $agentConfig['agentID'] . '.ico';
}
?>

<!doctype html>
    <!--[if IE 7 ]>    <html class="ie ie7" lang="en"> <![endif]-->
    <!--[if IE 8 ]>    <html class="ie ie8" lang="en"> <![endif]-->
    <!--[if IE 9 ]>    <html class="ie ie9" lang="en"> <![endif]-->
    <!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]--> 
    <head>
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
        <link rel='shortcut icon' href='{{ $favicon }}' type='image/x-icon'>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fixed-data-table/0.6.3/fixed-data-table-base.css" />
        <meta name="robots" content="noindex,nofollow" />
        <title>{{ $agentConfig['name'] }}</title>

        <style>
            <?= $agentConfig['css'] ?>
        </style>

    </head>
    <body>
        <div id="content"></div>
                


           
        <div class="clr"></div>
        <footer>
        </footer>  

      
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDge_r-YgKf2RMIiTXftkBwyQ21SBBuF4I"></script>
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
        <script src="{{ mix('js/app.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/polyfill/v3/polyfill.min.js?version=4.8.0&features=default"></script>
        <script src="https://cdn.ravenjs.com/3.15.0/raven.min.js"
            crossorigin="anonymous"></script>

        <?php if( env( 'APP_ENV' ) !== "local" && $agentConfig['agentID'] === 12 ) :  ?>
            <script>
                Raven.config('https://ba63685621304ec6881296fff1f91196@sentry.io/103582').install()
          
                var cid = 1653;

                (function() {
                    window.a1WebStatsObj = 'a1w';
                    window.a1w = window.a1w || function(){
                        (window.a1w.q = window.ga.q || []).push(arguments)
                    },
                    window.a1w.l = 1 * new Date();
                    var a = document.createElement('script');
                    var m = document.getElementsByTagName('script')[0];
                    a.async = 1;
                    a.src = "https://api1.websuccess-data.com/js/a1TrackerCode.js";
                    m.parentNode.insertBefore(a,m)
                })();

            </script> 
        <?php endif ?>
        <?php if( isset( $scripts ) && !empty( $scripts ) ) : ?>
            <?php foreach( $scripts as $scr ) : ?>
                <script src="<?= $scr ?>"></script>
            <?php endforeach ?>
        <?php endif ?>

        <?php if( $agentConfig['agentID'] === 12 ) : ?>
            <script type="text/javascript">
            window.__lc = window.__lc || {};
            window.__lc.license = 6534351;
            (function() {
              var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
              lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            })();
            </script>
        <?php endif ?>

    </body>
</html>
