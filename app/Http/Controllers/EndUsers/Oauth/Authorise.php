<?php
namespace App\Http\Controllers\EndUsers\Oauth;
use Illuminate\Http\Request;

class Authorise extends \App\Http\Controllers\Controller {

	public function index( Request $request ) {

		return view( 'oauth.authorise' );
	}

	public function store( Request $request ) {

		$client 	= new \GuzzleHttp\Client();

		$response 	= $client->request( 'POST', env( 'API_SOURCE' ) . '/oauth/access-token', [

				'form_params' => [

					'grant_type'	=>	'authorization_code',
					'client_id'		=>	$request->input( 'client_id' ),
					'client_secret'	=>	$request->input( 'secret' )
				]
			]
		);

		$contents 	= $response->getBody()->getContents();

		session( [ 'full_access_token' => json_decode( $contents ) ] );

		header( "Location: " . $request->input( 'redirect_uri' ) );
		die();
	}
}