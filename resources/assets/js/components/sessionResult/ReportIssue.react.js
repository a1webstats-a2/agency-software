import React 					from 'react';
import FilterActions 			from '../../actions/FilterActions';
import FlatButton 				from 'material-ui/FlatButton';
import RaisedButton 			from 'material-ui/RaisedButton';
import Dialog 					from 'material-ui/Dialog';
import Checkbox 				from 'material-ui/Checkbox';
import TextField 				from 'material-ui/TextField';

class ReportIssue extends React.Component {
	
	constructor( props ) {

		super( props );

		this.state = {

			open 	: 	props.open,
			user 	: 	props.user,
			data 	: 	props.data
		}
	}

	componentWillReceiveProps( newProps ) {

		this.setState({

			open 	: 	newProps.open,
			user 	: 	newProps.user,
			data 	: 	newProps.data
		})
	}

	componentDidMount() {


	}

	render() {

		const actions = [
		
			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this._handleClose}
			/>,
			<FlatButton
				label="Submit"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this._submitReport}
			/>
		];

		return (

			<div>

				<Dialog
					title="Report Record"
					actions={actions}
					modal={false}
					open={this.state.open}
					onRequestClose={this._handleClose}
				>

					<div className="clr"></div><br />
					
					<p>Reported by: {this.state.user.forename} {this.state.user.surname} at&nbsp; 
						{this.state.user.company_name}</p>

					<div className="clr"></div><br />

					<Checkbox
						label="Would you like us to report back to you when resolved?"
						labelPosition="left"
						onCheck={( event, isChecked ) => this._updateReportData( 'sendFeedbackWhenResolved', isChecked )}
						labelStyle={{ fontWeight : 'normal', color : 'inherit' }}
					/>

					<div className="clr"></div><br />

					<TextField
						hintText="What would you like to report about this record?"
						onChange={( event, newValue ) => this._updateReportData( 'reasonForReporting', newValue )}
						fullWidth={true}
					/>
				</Dialog>
			</div>

		)
	}

	_submitReport() {

		FilterActions.submitReportRecord();
	}

	_updateReportData( field, val ) {

		let reportData 		= this.state.data;
		reportData[field] 	= val;

		FilterActions.updateReportData( reportData );
	}

	_handleClose() {

		FilterActions.closeLightbox();
	}

}

export default ReportIssue;