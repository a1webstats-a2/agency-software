import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FilterActions from '../actions/FilterActions';

function getCurrentState( props ) {

	return {

		helpData 	: 	props.helpdata,
		open 		: 	props.open
	}
}

var HelpWindow = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.open !== nextProps.open ) {

			return true;
		}

		return false;
	},

	render : function() {

		var actions =  [

			<FlatButton
		        label="Close"
		        primary={true}
		        onTouchTap={this._handleClose}
		    />
      	];

		return (

			<Dialog
	          	title="Help"
	         	actions={actions}
	          	modal={false}
	          	open={this.state.open}
	          	onRequestClose={this._handleClose}
	        >
	        	{this.state.helpData.msg}
	        </Dialog>
		)
	},

	_handleClose : function() {

		FilterActions.closeLightbox();
	}
});

export default  HelpWindow;