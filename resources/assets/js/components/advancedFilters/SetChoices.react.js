import React from 'react';

function getCurrentState( props ) {

	return {

		anyThatMatch 	: 	props.anyThatMatch,
		choices  		: 	props.choices
	}
}

var SetChoices = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.anyThatMatch !== nextProps.anyThatMatch ) {

			return true;
		}

		if( this.props.choices !== nextProps.choices ) {

			return true;
		}

		return false;
	},

	render : function() {


		if( this.state.anyThatMatch && this.state.anyThatMatch.apply ) {

			var set = "Any values that match \"" + this.state.anyThatMatch.pattern + "\"";
		
		} else {

			var set = [];

			for( var key in this.state.choices ) {

				var item  		= this.state.choices[key];

				var itemDisplay = (

					<div key={key}>
						<p>{item} <span className="redX">x</span></p>
					</div>
				)

				set.push( itemDisplay );
			}
		}


		return (

			<div style={{ marginTop : 10, marginBottom : 20 }}>
				{set}
			</div>

		)
	}
});

export default SetChoices;