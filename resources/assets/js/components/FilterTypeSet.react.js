import React from 'react';
import Chip from 'material-ui/Chip';
import SocialIcon from 'material-ui/svg-icons/social/group';
import Avatar from 'material-ui/Avatar';
import PageView from 'material-ui/svg-icons/action/pageview';
import Search from 'material-ui/svg-icons/action/search';
import FilterActions from '../actions/FilterActions';
import Location from 'material-ui/svg-icons/communication/location-on';
import Key from 'material-ui/svg-icons/communication/vpn-key';
import Business from 'material-ui/svg-icons/communication/business';
import ReferrerIcon from 'material-ui/svg-icons/navigation/chevron-left';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import Lang from '../classes/Lang';

function getCurrentState(props) {
    return {
        noResultsStatus: props.noResultsStatus,
        avatar: props.avatar,
        typeDisplay: props.typeDisplay,
        filters: props.filters
    }
}

const styles = {
    chip: {
        float: 'left',
        margin: 8
    }
}

var FilterTypeSet = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps) {
        if (this.props.noResultsStatus !== nextProps.noResultsStatus) {
            return true;
        }

        if (this.props.filters !== nextProps.filters) {
            return true;
        }

        return false;
    },
    render: function () {
        var icon = "";

        switch (this.state.avatar) {
            case "PageView" :
                icon = <PageView/>

                break;
            case "SocialIcon" :
                icon = <SocialIcon/>

                break;
            case "Location" :
                icon = <Location/>

                break;
            case "Key" :
                icon = <Key/>

                break;
            case "Business" :
                icon = <Business/>

                break;
            case "ReferrerIcon" :
                icon = <ReferrerIcon/>

                break;
        }

        var allowDelete = true;

        switch (this.state.typeDisplay.toLowerCase()) {
            case "visitor types" :
                allowDelete = false;

                break;
        }

        var deleteIcon = "";

        if (allowDelete) {
            deleteIcon = (
                <DeleteIcon
                    onClick={this._removeAllFiltersByType}
                    style={{color: '#fff', float: 'right', marginTop: 5, marginLeft: 15}}
                />
            )
        }

        var backgroundColor = "#2d1467";

        if (this.state.noResultsStatus) {
            backgroundColor = "#FFBF00";
        }

        var chipStyle = styles.chip;
        chipStyle.backgroundColor = backgroundColor;

        return (
            <div>
                <Chip style={styles.chip}>
                    <Avatar backgroundColor={backgroundColor} icon={icon}/>

                    <strong>{Lang.getWordUCFirst(this.state.typeDisplay)}</strong>

                    {deleteIcon}

                    <Search
                        style={{color: '#fff', float: 'right', marginTop: 5, marginLeft: 15}}
                        onClick={this._showFiltersInLightbox}
                    />

                </Chip>
            </div>

        )
    },

    _removeAllFiltersByType: function () {

        if (this.state.filters[0] !== "undefined") {

            FilterActions.removeFiltersByType(this.state.filters);
        }
    },

    _showFiltersInLightbox: function () {

        FilterActions.showFiltersInLightbox(this.state.filters);
    }
});

export default FilterTypeSet;
