import React from 'react';

function getCurrentState( props ) {

	return {

		url 	: 	props.url,
		display : 	props.display
	}
}

class PageLink extends React.Component {

	constructor( props ) {

		super( props );
		this.state = getCurrentState( props );
	}

	componentWillReceiveProps( newProps ) {

		this.setState( getCurrentState( newProps ) );
	}

	render() {

		return (
			
			<a href={this.state.url} target="_blank">
	            {this.state.display} 
	        </a> 

        )
	}
}

export default PageLink;