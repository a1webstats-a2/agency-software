import React 				from 'react';
import NavBar 				from '../navigation/NavBar.react';
import FilterActions 		from '../../actions/FilterActions';
import FiltersStore 		from '../../stores/FiltersStore';
import Developer 			from '../team/Developer.react';
import NotificationsStore 	from '../../stores/NotificationsStore';
import {Tabs, Tab} 			from 'material-ui/Tabs';
import DeveloperIcon 		from 'material-ui/svg-icons/action/code';
import ThirdPartyApps 		from './ThirdPartyApps.react';
import API 					from './API.react';
import APIIcon 				from 'material-ui/svg-icons/action/http';
import SnackbarA1 			from '../Snackbar.react';
import EventNote 			from 'material-ui/svg-icons/notification/event-note';
import UserTipHelpLink 		from '../UserTipHelpLink.react';
import UserTips 			from '../UserTips.react';
import EventTracking 		from './EventTracking.react';

function getCurrentSettings() {
	var settings = FiltersStore.getAllSettings();

	return {
		thirdPartyKeysUpdating	: 	FiltersStore.getUpdateBox( 'thirdPartyKeys' ),
		salesforceID 			: 	FiltersStore.getSalesforceID(),
		settings 				: 	settings,
		snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
	    user                	:   FiltersStore.getUser(),
		notificationsData 		: 	{
			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),
			accountOK 				: 	FiltersStore.accountIsOK(),
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()
		}
	};
}

const DeveloperDashboard = React.createClass({
	getInitialState : function() {
		return getCurrentSettings();
	},
	componentWillReceieveProps : function() {
		this.setState(getCurrentSettings());
	},
	componentDidMount: function() {
		FiltersStore.addChangeListener( this._onChange );
	},
	componentWillUnmount: function() {
		FiltersStore.removeChangeListener( this._onChange );
	},
	render : function() {
		let tabs = [
			(
				<Tab key={1} label="API" icon={<APIIcon />}>
                	<br /><br />
                	<API settings={this.state.settings.hooks} />
            	</Tab>
			),
			(
				<Tab key={2} label="Hooks" icon={<DeveloperIcon />}>
					<br /><br />
					<Developer settings={this.state.settings.hooks} />
				</Tab>
			),
			(
				<Tab key={3} label="Third Party Apps" icon={<DeveloperIcon />}>
					<br /><br />
					<ThirdPartyApps
						salesforceID={this.state.salesforceID}
						updatingProp={this.state.thirdPartyKeysUpdating}
						agentId={this.state.settings.agentConfig.id}
						settings={this.state.settings.team} />
				</Tab>
			)
		];

        if (this.state.settings.agentConfig.id === 12) {
            tabs.push(
                (
                    <Tab key={4} label="Event Tracking" icon={<EventNote />}>
                        <br /><br />
                        <EventTracking />
                    </Tab>
                )
            )
        }

		return (
			<div className="reportBuilder">

				<NavBar data={this.state.notificationsData} />
			
				<div className="clr"></div>

				<div className="container mainContainer">
					<div className="row">
						<div className="col-md-12" id="results">
							<div className="row">
								<div className="col-md-2">
									<h3>Developer</h3>
								</div>
								<div className="col-md-10">
									<div style={{ marginTop : 30 }}>
										<UserTipHelpLink />
									</div>
								</div>
							</div>
							<div className="clr"></div>
							<Tabs>
								{tabs}
							</Tabs>
						</div>
					</div>
				</div>
				<SnackbarA1 snackbarSettings={this.state.snackbarSettings} />

				<UserTips />
			</div>
		);
	},

	_getCrypt : function (event) {
		FilterActions.setCrypt(event.target.value);
	},

	_onChange : function () {
		this.setState(getCurrentSettings);
	}
});

export default DeveloperDashboard;
