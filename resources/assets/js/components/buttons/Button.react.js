import React from 'react';

var Button = React.createClass({

	shouldComponentUpdate : function( nextProps, nextState ) {

		return false;
	},

	render : function() {

		return(

			<button className="btn btn-primary"></button>
		)
	}
});

export default  Button;