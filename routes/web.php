<?php

Route::get('ssl', 'System\SSL\Install@index');

Route::group(['middleware' => ['web', 'customWeb']], function () {
    Route::resource('lt/organisation', 'UI\InboundLinks\Organisation');
    Route::resource('forgotten-password', 'EndUsers\ForgottenPassword');
});

Route::get('download-wordpress-plugin/{clientID}', function ($clientID) {
    return redirect(env('API_SOURCE') . '/download-wordpress-plugin/' . $clientID);
});

Route::get('end-of-trial/feedback', 'EndUsers\Feedback@index');
Route::get('cancellation-success', 'EndUsers\EndUser@cancellationSuccess');
Route::get('cancellation-failure', 'EndUsers\EndUser@cancellationFailure');

Route::get('cancel-subscription', [
    'middleware' => ['web', 'customWeb'],
    'uses'       => 'EndUsers\EndUser@cancelSubscription'
]);

Route::post('cancel-subscription', [
    'middleware' => ['web', 'customWeb'],
    'uses'       => 'EndUsers\EndUser@postCancelSubscription'
]);

Route::get('reset-password/{userID}/{validationString}', [
    'middleware' => ['web', 'customWeb'],
    'uses'       => 'EndUsers\ResetPassword@index'
]);

Route::post('reset-password/{userID}/{email}', [
    'middleware' => ['web', 'customWeb'],
    'uses'       => 'EndUsers\ResetPassword@store'
]);

Route::get('login-stage', [
    'middleware' => ['web'],
    'uses'       => '\App\Http\Controllers\EndUsers\EndUser@loginLegacy'
]);

Route::post('login-stage', [
    'middleware' => ['web'],
    'uses'       => '\App\Http\Controllers\EndUsers\EndUser@postLoginLegacy'
]);

Route::get('migrate/success', '\App\Http\Controllers\EndUsers\EndUser@migrationSuccess');
Route::get('migrate/password', '\App\Http\Controllers\EndUsers\EndUser@migrationNewPassword');
Route::post('migrate/password', '\App\Http\Controllers\EndUsers\EndUser@storeMigrationNewPassword');

Route::get('login/redirect/{redirectLocation}/{reportID}', [
    'middleware' => ['web'],
    'uses'       => '\App\Http\Controllers\EndUsers\EndUser@loginWithRedirectAndReportID'
]);

Route::get('login/redirect/{redirectLocation}', [
    'middleware' => ['web'],
    'uses'       => '\App\Http\Controllers\EndUsers\EndUser@loginWithRedirect'
]);

Route::get('auth/login/admin/{id}', [
    'middleware' => ['web'],
    'uses'       => '\App\Http\Controllers\Admin\Login@show'
]);

Route::get('auth/login-with-email/{userId}', [
    'middleware' => ['web'],
    'uses'       => '\App\Http\Controllers\Admin\Login@loginWithPresuppliedEmail'
]);

Route::get('current-session-user', [
    'middleware' => ['web'],
    'uses'       => 'EndUsers\EndUser@index'
]);

Route::get('installation', [
    'middleware' => ['web'],
    'uses'       => 'UI\Installation@install'
]);

Route::post('installation', [
    'middleware' => ['web'],
    'uses'       => 'UI\Installation@createJSONConfigFile'
]);

Route::get('internal/download/contents/{assetId}', [
    'middleware' => ['web', 'customWeb', 'agentsignedup'],
    'uses'       => 'Reports\Download@show'
]);

Route::get('advanced-report', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('billing', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);


Route::get('company-tracker', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('create-template', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('saved-templates', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('settings', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('payment/stripe', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('team', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('referrers', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('developer', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('tags', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);


Route::get('countries', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('signup-complete', [
    'uses'       => 'EndUsers\EndUser@signupComplete',
    'middleware' => ['web', 'auth', 'agentsignedup']
]);

Route::get('organisations', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('organisations/{reportID}', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('keywords', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('all-pages', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('common-paths', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('entry-pages', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('saved-reports', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('create-report', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('export', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('results', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('results/{reportID}', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);


Route::get('logout', [
    'uses'       => 'Auth\AuthController@logout',
    'middleware' => ['web']
]);

Route::get('activate-account/{link}/{email}', [
    'uses' => 'EndUsers\EndUser@activateLink'
]);

Route::resource('application/agent-data', 'Agent\Main');

/** load template **/

Route::get('terms-and-conditions', 'UI\Dashboard@termsAndConditions');
Route::resource('api/proxy/api/v1/images/agent-logo', 'API\Proxy');

/* API PROXY */

Route::group(array('prefix' => 'api/proxy', 'middleware' => ['web', 'agentsignedup']), function () {
    Route::resource('api/v1/trigger-actions/autotags', 'API\Proxy');
    Route::resource('api/v1/service/status', 'API\Proxy');
    Route::resource('api/v1/heatmap', 'API\Proxy');
    Route::resource('api/v1/screen-recordings', 'API\Proxy');
    Route::resource('api/v1/trigger-actions/emails', 'API\Proxy');
    Route::resource('api/v1/admin/log/filters', 'API\Proxy');
    Route::resource('api/v1/report/no-results', 'API\Proxy');
    Route::resource('api/v1/screenshot/image', 'API\Proxy');
    Route::resource('api/v1/report/no-results-via-filter-log', 'API\Proxy');
    Route::resource('get-tracker-code', 'API\Proxy');
    Route::resource('api/v1/page-visits', 'API\Proxy');
    Route::resource('api/v1/user-tip', 'API\Proxy');
    Route::resource('api/v1/organisations/report-record', 'API\Proxy');
    Route::resource('api/v1/webhooks/test', 'API\Proxy');
    Route::resource('api/v1/organisations/new-format', 'API\Proxy');
    Route::resource('api/v1/organisations/no-image', 'API\Proxy');
    Route::resource('api/v1/export/crm-bulk', 'API\Proxy');
    Route::resource('api/v1/stripe/new-customer', 'API\Proxy');
    Route::resource('api/v1/stripe/update-customer', 'API\Proxy');
    Route::resource('api/v1/organisations/suggestions', 'API\Proxy');
    Route::resource('internal/load-report', 'API\Proxy');
    Route::resource('api/v1/invoices', 'API\Proxy');
    Route::resource('internal/application/load', 'API\Proxy');
    Route::resource('internal/application/load/update', 'API\Proxy');
    Route::resource('internal/export/csv', 'API\Proxy');
    Route::resource('internal/templates/dashboard', 'API\Proxy', ['as' => 'internal_templates_dashboard']);
    Route::resource('internal/templates', 'API\Proxy');
    Route::resource('internal/export', 'API\Proxy');
    Route::resource('internal/quick-export', 'API\Proxy');
    Route::resource('api/v1/export/crm', 'API\Proxy');
    Route::resource('api/v1/end-user-company/pricing', 'API\Proxy');
    Route::resource('api/v1/templates/dashboard', 'API\Proxy');
    Route::resource('api/v1/templates/load-at-login', 'API\Proxy');
    Route::resource('api/v1/advanced-filters/saved-scenarios', 'API\Proxy');
    Route::resource('api/v1/advanced-filters/scenarios', 'API\Proxy');
    Route::resource('api/v1/advanced-filters', 'API\Proxy');
    Route::resource('api/v1/emails/single-organisation', 'API\Proxy');
    Route::resource('api/v1/hooks', 'API\Proxy');
    Route::resource('api/v1/notes', 'API\Proxy');
    Route::resource('api/v1/organisation-session-history', 'API\Proxy');
    Route::resource('api/v1/page/visit', 'API\Proxy');
    Route::resource('api/v1/page/visits', 'API\Proxy');
    Route::resource('api/v1/assign-team-member', 'API\Proxy');
    Route::resource('api/v1/track-organisation', 'API\Proxy');
    Route::resource('api/v1/page/visit', 'API\Proxy');
    Route::resource('api/v1/organisation/additional-data', 'API\Proxy');
    Route::resource('api/v1/organisation/block', 'API\Proxy');
    Route::resource('api/v1/organisation', 'API\Proxy', ['as' => 'fetch_organisation']);
    Route::resource('api/v1/page', 'API\Proxy');
    Route::resource('api/v1/referrers', 'API\Proxy');
    Route::resource('api/v1/data-feed', 'API\Proxy');
    Route::resource('api/v1/countries', 'API\Proxy');
    Route::resource('api/v1/report-templates', 'API\Proxy');
    Route::resource('api/v1/end-company-report-templates', 'API\Proxy');
    Route::resource('api/v1/end-company-organisations', 'API\Proxy');
    Route::resource('api/v1/euco-relationships', 'API\Proxy');
    Route::resource('api/v1/tags/exclusions', 'API\Proxy');
    Route::resource('api/v1/tags', 'API\Proxy');
    Route::resource('api/v1/end-user-company/organisation', 'API\Proxy', ['as' => 'end_user_company_organisation']);
    Route::resource('api/v1/end-user-company/billing-details', 'API\Proxy');
    Route::resource('api/v1/browsers', 'API\Proxy');
    Route::resource('api/v1/operating-systems', 'API\Proxy');
    Route::resource('api/v1/search-hinting', 'API\Proxy');
    Route::resource('api/v1/automated-reports/block', 'API\Proxy', ['as' => 'automated_reports_block']);
    Route::resource('api/v1/automated-reports', 'API\Proxy');
    Route::resource('api/v1/common-paths', 'API\Proxy');
    Route::resource('api/v1/end-user-teams/member', 'API\Proxy');
    Route::resource('api/v1/analytics/all', 'API\Proxy');
    Route::resource('api/v1/user', 'API\Proxy');
    Route::resource('api/v1/user/avatar', 'API\Proxy');
    Route::resource('api/v1/user/notifications', 'API\Proxy');
    Route::resource('api/v1/user/notifications/archive', 'API\Proxy');
    Route::resource('api/v1/user/notifications/delete', 'API\Proxy');
    Route::resource('api/v1/user/client-secret', 'API\Proxy');
    Route::resource('api/v1/events/filter-change', 'API\Proxy');
    Route::resource('api/v1/events/date-change', 'API\Proxy');
    Route::resource('api/v1/events/reset-filters', 'API\Proxy');
    Route::resource('api/v1/events/quick-link', 'API\Proxy');
    Route::resource('api/v1/payments/crypt', 'API\Proxy');
    Route::resource('api/v1/settings/user', 'API\Proxy', ['as' => 'user_settings']);
    Route::resource('api/v1/settings/third-party-apps', 'API\Proxy');
    Route::resource('api/v1/settings/team', 'API\Proxy');
    Route::resource('api/v1/settings/oauth', 'API\Proxy');
    Route::resource('api/v1/user-activity', 'API\Proxy');
});

Route::get('/', [
        'uses'       => 'UI\Dashboard@index',
        'middleware' => array('web', 'auth', 'agentsignedup')
    ]
);

Route::get('oauth/authorise', 'EndUsers\OAuth\Authorise@index');
Route::post('oauth/authorise', 'EndUsers\Oauth\Authorise@store');

Route::get('oauth/get-current-token', [
        'uses'       => 'EndUsers\EndUser@getCurrentOauthToken',
        'middleware' => ['web', 'auth', 'agentsignedup']
    ]
);

// end user

Route::get('end-user/uses-geolocation/{clientId}', 'EndUsers\EndUser@usesGeolocation');
Route::get('end-user/get-client-id', array(
        'uses'       => 'EndUsers\EndUser@getClientId',
        'middleware' => ['web']
    )
);

// end of end user

Route::get('newPageVisit', 'Dummy\PageVisit@newPage');
Route::get('newPageLink', 'Dummy\PageVisit@newPageLink');

Route::get('auth/login', function () {
    return redirect()->route('login');
});

Route::post('auth/login', array(
        'middleware' => [],
        'uses'       => '\App\Http\Controllers\EndUsers\Login@authenticate'
    )
);

Route::get('auth/logout', [
        'middleware' => ['web'],
        'uses'       => 'Auth\AuthController@logout'
    ]
);

// Registration routes...
Route::get('auth/register', array(
        'middleware' => ['web'],
        'uses'       => 'Auth\AuthController@getRegister'
    )
);

Route::post('auth/register', array(
        'middleware' => ['web'],
        'uses'       => 'Auth\AuthController@postRegister'
    )
);

Auth::routes();

Route::get('/home', 'HomeController@index');
