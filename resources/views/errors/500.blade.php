@extends( 'templates.error_page' )

<?php
$configData = getAgentConfigData();
?>

@section( 'content' )
    <div class="row">
        <div class="col-md-12">
            <div id="signupForm" class="signupForm">
                <br/>
                <div class="row">
                    <div class="col-md-7">
                        <h2>Sorry, there has been a system error.</h2>

                        <p>Please <a href="{{ $configData['website'] }}">contact us</a> to let us know what you
                            were trying to do, so that we can fix it.</p>

                        <br/>
                        <p>Apologies for the inconvenience - we hate errors as much as
                            you do so we will endeavour to find a solution for you as soon
                            as possible.</p>
                        <br/><br/><br/>

                        <p>{{ $configData['name'] }}</p>

                    </div>

                    <div class="col-md-4 col-md-offset-1">
                        <img class="bugImg" src="/images/bug-fixing.png"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
