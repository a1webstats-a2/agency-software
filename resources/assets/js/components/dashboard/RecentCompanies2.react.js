import React from 'react';
import OrganisationFilterLink from './links/OrganisationFilterLink.react';
import Paper from 'material-ui/Paper';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import shallowCompare from 'react-addons-shallow-compare' // ES6

const styles = {

	paper 	: 	{

		padding 	: 	20,
		height  	: 	400
	},

	refreshIndicator : {

		display 	: 'inline-block',
		position 	: 'relative',

	}
}

function getCurrentState( props ) {

	var organisations = props.companies.map( function( organisation, i ) {

		return organisation;
	})


	return {

		organisations  			: 	organisations,
		isApplicationResting 	: 	props.isApplicationResting
	}
}



var RecentCompanies = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		var update = false;

		if( this.state.isApplicationResting !== nextState.isApplicationResting ) {

			update = true;
		}

		if( nextProps.companies.length > 0 ) {

			if( this.props.companies.length === 0 ) {

				update = true;
	
			} else if( this.props.companies[0][1] !== nextProps.companies[0][1] ) {

				update = true;
			}
		}

		return update;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		if( !this.state.isApplicationResting ) {

			return ( 

				<div className="recentCompanies">

					<Paper style={styles.paper} zDepth={4}>
						<div className="blueHeader">
							<h4>Recent Companies</h4>
						</div>

						<RefreshIndicator
							size={70}
							left={0}
							top={20}
							status="loading"
							style={styles.refreshIndicator}
						/>
						<br /><br />
					</Paper>
				</div>
			)
		
		}	

		if( typeof this.state.organisations == "undefined" ) {

			var top10Organisations = [];
		
		} else {

			var top10Organisations = this.state.organisations.slice( 0, 6 );
		}

		var organisations = top10Organisations.map( function( org, i ) {

			return (

				<tr key={i}>
					<td><OrganisationFilterLink key={i} orgName={org[1]} orgId={parseInt( org[0] )} /></td>
				</tr>
			);
		});

		return (

			<div className="recentCompanies">

				<Paper style={styles.paper} zDepth={4}>
					<div className="blueHeader">
						<h4>Recent Companies</h4>
					</div>
					<br />

					<table className="table">

						<tbody>
							{organisations}
						</tbody>

					</table>
				</Paper>
			</div>
		)
	}

});

export default  RecentCompanies;