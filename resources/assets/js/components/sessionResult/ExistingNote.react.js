import React from 'react';
import Delete from 'material-ui/svg-icons/action/delete';
import Note from 'material-ui/svg-icons/av/note';
import Popover from 'material-ui/Popover';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import FilterActions from '../../actions/FilterActions';

function getCurrentState( props ) {

	return {

		myKey 		: 	props.myKey,
		note 		: 	props.note,	
		open 		: 	false
	}
}

const style = {
	height 		: 200,
	width 		: 300,
	padding 	: 15,
	margin 		: 20,
	textAlign 	: 'left',

	svg     :   {

        color :     '#170550'
    }
};



var ExistingNote = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},



	showNote : function( event ) {
		
		this.setState({

			open 	 	: 	true,
			anchorEl 	: 	event.currentTarget
		})
	},

	handleRequestClose : function() {

		this.setState({

			open 	: 	false
		})
	},

	render : function() {

		var avatarSrc = "/api/proxy/api/v1/user/avatar/" + this.state.note.user_id;

		return (

			<tr className="notification">

				<td><Avatar src={avatarSrc} /></td>
				<td>{this.state.note.forename} {this.state.note.surname}</td>
				<td>{this.state.note.created_at}</td>
				<td>{this.state.note.subject}</td>
				<td>
					<Note color={style.svg.color} onTouchTap={this.showNote}  />
					<Delete color={style.svg.color} onTouchTap={this._deleteNote} />

					<Popover
						open={this.state.open}
						anchorEl={this.state.anchorEl}
				        anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
				        targetOrigin={{horizontal: 'right', vertical: 'top'}}
				        onRequestClose={this.handleRequestClose}>
				        <Paper style={style}>
				        	{this.state.note.note}
						</Paper>
					</Popover>
				</td>

			</tr>
		)
		
	},

	_deleteNote : function() {

		if( confirm( "Are you sure you want to delete this note?" ) ) {
		
			FilterActions.deleteNote({

				id 		: 	this.state.note.id, 
				key 	: 	this.state.myKey
			});
		}
	}
});

export default  ExistingNote;
