import React from 'react';

import FiltersStore from '../../stores/FiltersStore';
import DashboardGoogleMap from './DashboardGoogleMap.react';
import CountryVisitorStats from './CountryVisitorStats.react';

function getCurrentState(props) {

    var countryStats = [];

    if (typeof props.analyticsData.visitorLocations !== "undefined") {
        countryStats = props.analyticsData.visitorLocations;
    } else if (typeof props.otherData.analyticsData.visitorLocations !== "undefined") {
        countryStats = props.otherData.analyticsData.visitorLocations;
    }

    return {
        visitorLocations: props.locations,
        isApplicationResting: props.isApplicationResting,
        countryStats: countryStats,
        analyticsData: props.analyticsData,
        otherData: props.otherData
    };
}

var SessionCountries = React.createClass({
    getInitialState: function () {

        return getCurrentState(this.props);
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState(getCurrentState(nextProps));
    },
    shouldComponentUpdate: function (nextProps) {
        var update = false;

        if (FiltersStore.clone(this.props.otherData) !== FiltersStore.clone(nextProps.otherData)) {
            update = true;
        }

        if (nextProps.locations.length > 0) {
            if (this.props.locations.length === 0) {
                update = true;
            } else if (this.props.locations[0][1] !== nextProps.locations[0][1]) {
                update = true;
            }
        }

        return update;

    },
    render: function () {
        return (
            <div className="sessionCountries">
                <DashboardGoogleMap isApplicationResting={this.state.isApplicationResting}
                                    otherData={this.state.otherData} analyticsData={this.state.analyticsData}
                                    countryStats={this.state.visitorLocations}/>
                <CountryVisitorStats otherData={this.state.otherData}/>
            </div>
        )
    }
});

export default SessionCountries;
