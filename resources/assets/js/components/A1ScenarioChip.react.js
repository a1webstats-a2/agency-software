import React from 'react';
import FilterActions from '../actions/FilterActions';
import Chip from 'material-ui/Chip';
import Search from 'material-ui/svg-icons/action/search';
import ScenarioIcon from 'material-ui/svg-icons/action/perm-media';
import Avatar from 'material-ui/Avatar';

const styles = {
    chip: {
        margin: 8
    },
    chipWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        float: 'left'
    },
    toggle: {
        thumbSwitched: {
            backgroundColor: '#53b68b',
        },
        trackSwitched: {
            backgroundColor: '#ccc',
        },
        thumbOff: {
            backgroundColor: '#ccc',
        },
        trackOff: {
            backgroundColor: '#ccc',
        }
    }
}

function getCurrentState(props) {
    var latestAddedFilters = props.latestAddedFilters;

    var isNew = false;

    if (typeof latestAddedFilters[props.filter.type] !== 'undefined') {
        if (latestAddedFilters[props.filter.type].indexOf(props.filter.storedValue.id) > -1) {
            isNew = true;
        }
    }

    return {
        noResultsStatus: props.noResultsStatus,
        latestAddedFilters: props.latestAddedFilters,
        avatar: props.avatar,
        type: props.type,
        filter: props.filter,
        text: props.text,
        isNew: isNew
    }
}

var A1ScenarioChip = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (JSON.stringify(this.props.filter) !== JSON.stringify(nextProps.filter)) {
            return true;
        }

        return false;
    },
    render: function () {
        var avatar = <ScenarioIcon/>;
        var avatarBackgroundColor = "#170550";
        var backgroundColor = "#170550";

        if (this.state.isNew) {
            backgroundColor = "#53b68b";
            avatarBackgroundColor = "#53b68b";
        }

        if (this.state.noResultsStatus) {
            backgroundColor = "#FFBF00";
        }

        return (
            <div style={styles.chipWrapper}>
                <Chip className="chip" backgroundColor={backgroundColor} labelColor="#ffffff" style={styles.chip}>
                    <Avatar backgroundColor={avatarBackgroundColor} icon={avatar}/>
                    <strong>{this.state.type}</strong>
                    <div style={{float: 'right', width: 30, height: 20, marginLeft: 10, color: '#fff', marginTop: 5}}>
                        <Search onClick={this._viewScenarioFilters} style={{color: '#fff'}}/>
                    </div>
                </Chip>
            </div>
        )
    },
    _viewScenarioFilters: function () {
        FilterActions.showScenarioFilters(this.state.filter);
    },
    _handleRequestDelete: function () {
        var remove = {
            type: this.state.filter.type,
            id: this.state.filter.storedValue.id
        }

        FilterActions.removeFilterByType(remove);
    }
});

export default A1ScenarioChip;
