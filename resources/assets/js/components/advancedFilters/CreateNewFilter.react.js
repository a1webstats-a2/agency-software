import React from 'react';
import AdvancedFilterActions from '../../actions/AdvancedFilterActions';
import SetChoices from './SetChoices.react';
import RaisedButton from 'material-ui/RaisedButton';
import SaveIcon from 'material-ui/svg-icons/content/save';
import IconButton from 'material-ui/IconButton';
import Lang from '../../classes/Lang';

function getCurrentState(props) {
    return {
        scenarioName: props.scenarioName,
        scenarioNameEdited: props.scenarioNameEdited,
        savedScenarios: props.savedScenarios,
        anyThatMatch: props.anyThatMatch,
        include: props.include,
        choiceType: props.filterChoiceType,
        choices: props.choices,
        scenarioID: props.scenarioID,
        filterCount: props.filterCount,
        andOr: props.andOr,
        loadScenarioID: props.loadScenarioID
    }
}

function sortByName(a, b) {
    const nameA = a.display.toUpperCase()
    const nameB = b.display.toUpperCase()

    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }

    return 0;
}

const CreateNewFilter = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    render: function () {
        var setChoices = <SetChoices
            anyThatMatch={this.state.anyThatMatch}
            choices={this.state.choices}/>

        const categories = [
            {
                display: 'County',
                key: 'counties',
            },
            {
                display: 'First Page Visited',
                key: 'firstPageVisited',
            },
            {
                display: 'Page Visited',
                key: 'pageVisited',
            },
            {
                display: 'Last Page Visited',
                key: 'lastPageVisited',
            },
            {
                display: 'Campaign Name (utm_campaign)',
                key: 'campaignName',

            },
            {
                display: 'Campaign Medium (utm_medium)',
                key: 'campaignMedium',
            },
            {
                display: 'Campaign Source (utm_source)',
                key: 'campaignSource',
            },
            {
                display: 'Geolocation',
                key: 'geolocation',
            },
            {
                display: 'Referrer',
                key: 'referrer',
            },
            {
                display: Lang.getWordUCFirst("organisation"),
                key: 'organisation',
            },
            {
                display: 'Keyword',
                key: 'keyword',
            },
            {
                display: 'Town / City',
                key: 'town',
            },
            {
                display: 'Country',
                key: 'countries',
            },
            {
                display: 'Source',
                key: 'source',
            },
            {
                display: 'Tags',
                key: 'tags',
            },
            {
                display: 'Visitor Type',
                key: 'visitorTypeFilters',
            },
            {
                display: 'IP',
                key: 'ip',
            },
            {
                display: 'Events',
                key: 'events',
            },
            {
                display: 'Device Type',
                key: 'deviceType',
            }
        ]

        const displayOptions = Object.values(categories).sort(sortByName).map(function (category, c) {
            return <option key={c} value={category.key}>{category.display}</option>;
        });

        var criteria = '';

        if (this.state.filterCount > 0) {
            criteria = (
                <div>
                    <label>Criteria</label>
                    <select value={this.state.andOr} onChange={this._setAndOr} className="form-control">
                        <option>AND</option>
                        <option>OR</option>
                    </select>
                </div>
            )
        }

        var disableAddFilter = false;
        var choicesCount = 0;

        for (var key in this.state.choices) {
            choicesCount++;
        }

        if (choicesCount === 0 || this.state.include === "") {
            disableAddFilter = true;
        }

        var saveIcon = "";

        if (this.state.filterCount > 0) {
            saveIcon = (
                <IconButton
                    onTouchTap={this._saveScenarioData}
                    tooltip="Save Scenario"
                    iconStyle={{marginRight: 10, color: '#fff'}}>
                    <SaveIcon/>
                </IconButton>
            )
        }

        var savedScenarios = [];

        for (var key in this.state.savedScenarios) {
            let scenario = this.state.savedScenarios[key];

            savedScenarios.push(
                <option key={key} value={scenario.id}>{scenario.name}</option>
            )
        }

        return (
            <div>
                <h4>Create Filter</h4><br/>

                {criteria}

                <div className="clr"></div>

                <label>Type</label>
                <select value={this.state.choiceType} className="form-control" onChange={this._openFilterChoices}>
                    <option value="">Select</option>
                    {displayOptions}
                </select>

                <div className="clr"></div>

                <label>Include / Exclude</label>
                <select value={this.state.include} onChange={this._setIncludeExclude} className="form-control">
                    <option value="">Select</option>
                    <option value="include">Include</option>
                    <option value="exclude">Exclude</option>
                </select>

                <div className="clr"></div>
                <br/>

                <div className="row">
                    <div className="col-md-9">
                        <RaisedButton
                            onClick={this._addFilter}
                            label="Add Filter"
                            disabled={disableAddFilter}
                            primary={true}
                            fullWidth={true}/>
                    </div>
                    <div className="col-md-1 ">
                        {saveIcon}
                    </div>
                </div>

                {setChoices}

                <hr/>

                <h4>Load Filter Bundle</h4><br/>

                <select className="form-control" value={this.state.loadScenarioID} onChange={this._setScenarioToLoad}>
                    <option value={-1}>Select</option>
                    {savedScenarios}
                </select>

                <RaisedButton
                    onClick={this._loadScenario}
                    label="Load Scenario"
                    disabled={(this.state.loadScenarioID === -1) ? true : false}
                    primary={true}
                    fullWidth={true}/>

            </div>
        )
    },
    _loadScenario: function () {
        AdvancedFilterActions.loadScenario();
    },
    _setScenarioToLoad: function (event) {
        AdvancedFilterActions.setScenarioToLoad({
            scenarioToLoad: event.target.value,
            scenarioID: this.state.scenarioID
        });
    },
    _setAndOr: function (event) {
        AdvancedFilterActions.setAndOr({
            andOr: event.target.value,
            scenarioID: this.state.scenarioID
        });
    },
    _saveScenarioData: function () {
        if (!this.state.scenarioNameEdited) {
            alert("Please click on \"" + this.state.scenarioName + "\" and edit the name before saving");

            return false;
        }

        AdvancedFilterActions.saveScenarioData(this.state.scenarioID);
    },
    _openFilterChoices: function (event) {
        AdvancedFilterActions.openFilterChoices({
            editing: false,
            filterType: event.target.value,
            scenarioID: this.state.scenarioID
        })
    },
    _addFilter: function () {
        AdvancedFilterActions.addFilter(this.state.scenarioID);
    },
    _setIncludeExclude: function (event) {
        AdvancedFilterActions.setIncludeExclude({
            scenarioID: this.state.scenarioID,
            include: event.target.value === 'include'
        })
    }
});

export default CreateNewFilter;
