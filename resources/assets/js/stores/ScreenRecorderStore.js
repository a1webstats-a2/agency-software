import ScreenRecorderDispatcher 			from '../dispatcher/ScreenRecorderDispatcher';
import ScreenRecorderConstants   			from '../constants/ScreenRecorderConstants';
import ScreenRecorderActions 				from '../actions/ScreenRecorderActions';
import EventEmitterObj            			from 'events';
let EventEmitter 							= EventEmitterObj.EventEmitter;
import merge                   				from 'merge';
import assign                  				from 'object-assign';
import FiltersStore 						from '../stores/FiltersStore';

let _video 	=	{

	frames 		: 	{},
	screenshots : 	{},
	visitData 	: 	{

		meta_title 	: 	'',
		name 		: 	'',
		start_time 	: 	false,
		url 		: 	''
	}
};

let _mouseX 				=	0;
let _mouseY 				=	0;
let _scrollTo 				= 	0;
let _background 			= 	"";
let _firstScreenshot 		= 	"";
let _videoIsLoaded 			= 	false;
let _i 						= 	0;
let _videoLength 			= 	0;
let _videoPlayInterval		=	0;
let _numPlays 				= 	0;
let _backgroundWidth 		= 	0;
let _debug 					= 	true;
let _imageNaturalHeight 	= 	0;
let _displayOffsets 		= 	{

	top 	: 	0,
	right 	: 	0,
	bottom 	: 	0,
	left 	: 	0,
	width 	: 	0,
	height 	: 	0
}
let _heatmapDataTypeToView  = 	'hover';
let _imageRenderedHeight 	= 	0;
let _heatmapData 			= 	{

	heatmapData : 	{

		'hover'		:  	[],
		'scrollTo'	: 	[],
		'clicks'	: 	[]
	},
	visitData 	: 	{

		'count'		:  	0,
		'url'		: 	""
	},
	screenshot 	: 	false
}
let _aspectRatio 			= 	{

	width 	: 	100
}

let _playStatus = 0;

function clone( obj ) {
	
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}


function loadVideo( videoID ) {

	const xhr 		= new XMLHttpRequest();
  	xhr.open( "GET", "/api/proxy/api/v1/screen-recordings/" + videoID );
  	xhr.setRequestHeader( "Content-Type", "application/json;charset=UTF-8" );

  	xhr.send(); 

  	xhr.onreadystatechange 	=  function() {

		if( xhr.readyState === 4 ) {

			switch( xhr.status ) {

				case 200 :

					let video 		= JSON.parse( xhr.responseText );

					video.frames 	= JSON.parse( video.frames );

					ScreenRecorderActions.storeVideo( video );

					break;

			}
		}	
	};
}

function calculateAspectRatios() {

    _aspectRatio = {

    	width 				: 	_video.res.aspectRatioX,
    	height 				: 	_video.res.aspectRatioY,
    	displayImageWidth 	: 	800
    }
}

function storeVideo( video ) {

	_video 	= video;

	setVideoLength();
	preloadAllImages();
	calculateAspectRatios();	
}

function loadImage( img ) {

	let image 			=  	new Image();
	image.src 			= 	'/api/proxy/api/v1/screenshot/image/' + img;
	return image;
}

function preloadAllImages() {

	for( const i in _video.screenshots ) { 

		if( loadImage( _video.screenshots[i] ) ) {

			if( !_firstScreenshot ) {

				_firstScreenshot = _video.screenshots[i];
			}
		}
	}

	_videoIsLoaded = true;
}

function setVideoLength() {

	if( typeof _video.frames === undefined || !_video.frames ) {

		return false;
	}

	const keys 				= Object.keys( _video.frames );

	const lastSecondInVideo = parseInt( keys.pop() );

	_videoLength 			= lastSecondInVideo;

}

function setVideoI( seek ) {

	_playStatus = 0;
	clearInterval( _videoPlayInterval );
	_i = parseInt( seek ); 
	startVideoInterval();
}

function startPlayVideo() {

	preloadAllImages();
	startVideoInterval();
}

function setVideoFinished() {

	_playStatus = 0;
	_i 			= 0;
	_numPlays++;
	clearInterval( _videoPlayInterval );

	ScreenRecorderActions.updateState();
}

function setImageNaturalHeight( height ) {

	_imageNaturalHeight = height;
	calculateAspectRatios();	
}

function setRenderedImageHeightInStore( height ) {

	_imageRenderedHeight = height;
	calculateAspectRatios();
}

function getXUsingRatio( x ) {

 	x = parseInt( x );

 	return Math.round( x * _aspectRatio.width );
}

function getYUsingRatio( y, type ) {

	return Math.round( parseInt( y ) * _aspectRatio.height );
}

function startVideoInterval() {

	_playStatus 		= 1;

	let frames 			= _video.frames;

	let screenshots 	= _video.screenshots;

	_videoPlayInterval 	= setInterval( function() {

		_i++;

		let a = _i;

		if( frames[a] !== undefined && frames[a] ) {

			if( typeof frames[a].click !== "undefined" ) {

				let audio = new Audio( '/sounds/click.mp3' );
				audio.play();
			}

			let mouseX 	= parseInt( frames[a].mouseY );
			let mouseY 	= parseInt( frames[a].mouseX );
			mouseX 		= ( mouseX === 0 ) ? 0 : mouseX;

			_mouseY		= getYUsingRatio( mouseY, "mouse" );
			_mouseX 	= getXUsingRatio( mouseX );
			_scrollTo 	= getYUsingRatio( frames[a].scrollTop, "scrollTop" ); 			
		}

		if( screenshots[a] !== undefined && screenshots[a] ) {	

			_background = screenshots[a];
		}

		ScreenRecorderActions.updateState();

		if( a > _videoLength ) {

			setVideoFinished();
		}

	}, 1 );
}

function loadHeatmap( pageID ) {

	const filters 	= FiltersStore.getFilters();
	const xhr 		= new XMLHttpRequest();
  	xhr.open( "POST", "/api/proxy/api/v1/heatmap" );
  	xhr.setRequestHeader( "Content-Type", "application/json;charset=UTF-8" );
  	xhr.send( JSON.stringify({

  		pageID,
  		dateFrom 	: 	filters[0].storedValue,
  		dateTo 		: 	filters[1].storedValue

  	})); 

  	xhr.onreadystatechange 	=  function() {

		if( xhr.readyState === 4 ) {

			switch( xhr.status ) {

				case 200 :

					ScreenRecorderActions.storeHeatmap( JSON.parse( xhr.responseText ) );

					break;

			}
		}	
	};
}

function pauseVideo() {

	_playStatus = 3;
	clearInterval( _videoPlayInterval );

}

function setDisplayOffsets( offsets ) {

	_displayOffsets = offsets;
}

function storeHeatmapData( heatmapData ) {

	_heatmapData = heatmapData;
}

function setDisplayType( displayType ) {

	_heatmapDataTypeToView = displayType;
}

let ScreenRecorderStore 	= merge( EventEmitter.prototype, {

	getVideo : function() {

		return clone( _video );
	},

	getHeatmapTypeToView : function() {

		return _heatmapDataTypeToView;
	},

	getHeatmapData : function() {

		return _heatmapData;
	},

	getPlayStatus : function() {

		return _playStatus;
	},

	getVideoVisitData : function() {

		return _video.visitData;
	},

	getDisplayOffsets : function() {

		return _displayOffsets;
	},

	getPlayVars : function() {

		return {

			mouseX 			: 	_mouseX,
			mouseY 			: 	_mouseY,
			scrollTo 		: 	_scrollTo,
			background 		: 	_background,
			firstScreenshot : 	_firstScreenshot,
			videoIsLoaded 	: 	_videoIsLoaded,
			playStatus 		: 	_playStatus,
			videoLength 	: 	_videoLength,
			videoI 			: 	_i,
			numPlays 		: 	_numPlays,
			aspectRatio 	: 	_aspectRatio,
			displayOffsets 	: 	_displayOffsets
		}
	},

	emitChange : function() {

		this.emit( 'change' );
	},

	addChangeListener : function( callback )  {

		this.on( 'change' , callback);
	},

	removeChangeListener : function( callback ) {

		this.removeListener( 'change', callback);
	}
});

ScreenRecorderDispatcher.register( function( payload ) {

	let returnRes = true;

	switch( payload.actionType ) {

		case ScreenRecorderConstants.SAVE_RECORDING :

			saveRecording( payload.data );

			break;

		case ScreenRecorderConstants.LOAD_VIDEO :

			loadVideo( payload.data );

			break;

		case ScreenRecorderConstants.STORE_VIDEO :

			storeVideo( payload.data );

			break;

		case ScreenRecorderConstants.START_PLAY_VIDEO :

			startPlayVideo();

			break;

		case ScreenRecorderConstants.SET_DISPLAY_TYPE :

			setDisplayType( payload.data );

			break;

		case ScreenRecorderConstants.SET_RENDERED_IMAGE_HEIGHT_2 :


			setRenderedImageHeightInStore( payload.data );

			break;

		case ScreenRecorderConstants.PAUSE_VIDEO :

			pauseVideo();

			break;

		case ScreenRecorderConstants.SET_DISPLAY_OFFSETS :

			setDisplayOffsets( payload.data );

			break;

		case ScreenRecorderConstants.UPDATE_STATE :

			break;

		case ScreenRecorderConstants.SET_VIDEO_I :

			setVideoI( payload.data );

			break;

		case ScreenRecorderConstants.SET_NATURAL_HEIGHT :

			setImageNaturalHeight( payload.data );

			break;

		case ScreenRecorderConstants.STORE_HEATMAP :

			storeHeatmapData( payload.data );

			break;

		case ScreenRecorderConstants.LOAD_HEATMAP :

			loadHeatmap( payload.data );

			break;

		default :


			break;
	}

	ScreenRecorderStore.emitChange();

  	return returnRes;

});

export default ScreenRecorderStore;


