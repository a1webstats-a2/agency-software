<?php 

if( $agentConfig['agentID'] === 12 ) {

    $favicon = env( 'API_SOURCE' ) . '/favicon.ico';

} else {

    $favicon = env( 'API_SOURCE' ) . '/images/agents/favicons/' . $agentConfig['agentID'] . '.ico';
}
?>

<!doctype html>
    <!--[if IE 7 ]>    <html class="ie ie7" lang="en"> <![endif]-->
    <!--[if IE 8 ]>    <html class="ie ie8" lang="en"> <![endif]-->
    <!--[if IE 9 ]>    <html class="ie ie9" lang="en"> <![endif]-->
    <!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]--> 
    <head>
        <link rel="stylesheet" href="{{ mix('css/app.css' ) }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">  
        <link rel='shortcut icon' href='{{ $favicon }}' type='image/x-icon'/ >
        <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

        <title>@yield( 'title' )</title>
    </head>
    <body id="loginBody">
        <div class="container">

            @if (isset( $errors ) && count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

     

            @yield( 'content' )

            <div class="clr"></div>
        </div>
        <div class="clr"></div>
        <footer>
        </footer>   
 
    </body>
</html>
