import React               from'react';

import FilterActions       from '../actions/FilterActions' ;
import TagIcon from 'material-ui/svg-icons/action/label-outline';

var IncludeTagFilter    =  React.createClass({

    getInitialState :   function() {
        
        var displayValue = this.props.value;

        if( typeof this.props.textValue != "undefined" ) {

            displayValue = this.props.textValue;
        }


        return {

            storedValue     :   this.props.storedValue,
            myKey           :   this.props.myKey,
            value           :   this.props.value,
            displayValue    :   displayValue
        };

    },

    shouldComponentUpdate : function( nextProps, nextState ) {

        return false;
    },

    render  :   function() {

        return (

            <div className="dateFromBox form-group">
                <div className="row">

                    <div className="col-md-2">
                        <TagIcon />
                    </div>
                    <div className="col-md-8 overflowHidden">

                        <label className="control-label">Include Tag:</label>
                        <p>{this.state.displayValue}</p>

                    </div>
                    <div className="col-md-2">
                        <span className="removeFilter" onClick={this._removeFilter}></span>
                    </div>
                </div>

            </div>

        )
    },


    _removeFilter   :   function( e ) {


        FilterActions.removeFilterByType({

            type    :   "includeTagFilter",
            id      :   this.state.storedValue.id
        });

    }

     
})

export default  IncludeTagFilter;