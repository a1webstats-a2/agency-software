import React from 'react';
var ReactPropTypes = React.PropTypes;

import FilterActions from '../actions/FilterActions';

import classNames from 'classnames';

var AddDateFilter   = React.createClass({

	getInitialState   :   function() {

		return  {

			value   :   'dateFrom'
		};
	},


   /**
    * @return {object}
    */
   	render: function() {


   		return (

   			<div className="addDateFilter">
		   		<form className="criteriaBox"  onSubmit={this.handleSubmit}>
			   		<div className="form-group">
				   		<h4>Date Filters</h4>
				   		<select className="form-control" onChange={this._onChange} defaultValue="dateFrom" value={this.state.value} >
				   			<option value="dateFrom">Date (from:)</option>
				   			<option value="dateTo">Date (to:)</option>
				   		</select>
			   		</div>

			   		<button className="btn btn-success" onClick={this._save}>Add Filter</button>

		   		</form>
		   	</div>
   		);
   	},

   	_onChange  :   function( event ) {

   		this.setState( {

   			value   :   event.target.value
   		})
   	},

   	_save   :   function( event ) {

   		event.preventDefault();

   		var newState  = {

   			type          :   this.state.value, 
   			storedDate    :   '2016-01-01',
   			id            :   null

   		}

   		FilterActions.createFilter( newState );
   	}
});

export default  AddDateFilter;