import React 					from  	'react';
import moment 					from 	'moment';

function getCurrentState( props ) {


	return {	

		col 		: 	props.col,
		row 		: 	props.rowIndex,
		data 		: 	props.data
	}
}

var DateCell = React.createClass({

	getInitialState : function() {

		let currentState 		= getCurrentState( this.props );

		return currentState;
	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		return true;
	},

	render : function() {

		let completedDate = moment( this.state.data.getObjectAt( this.state.row )[this.state.col] ).format( 'Do MMM HH:mm' );

		if( ! this.state.data.getObjectAt( this.state.row )[this.state.col]  ) {

			completedDate = "";
		}

		return (

			<div>
				<p style={{ marginTop : 14 }}>{completedDate}</p>
			</div>
		);
	}
});

export default DateCell;