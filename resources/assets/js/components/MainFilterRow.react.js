import React 							from 'react';
import ReactTabs 						from 'react-tabs';
import StandardFilters 					from './StandardFilters.react';
import CustomFilters 					from './CustomFilters.react';
import OrganisationFilters 				from './OrganisationFilters.react';
import OrganisationFiltersContainer 	from './OrganisationFiltersContainer.react';
import DeviceTypeFilters 				from './DeviceFilters.react';
import VisitorTypeFilters 				from './visitorTypes/VisitorTypeFilters.react';
import CountryFilters					from './countryFilters/CountryFilters.react';
import FilterActions 					from '../actions/FilterActions';
import HideDrawers 						from './HideDrawers.react';
import DrawerToolbar 					from './DrawerToolbar.react';
import IncludeTagFilters 				from './tagFilters/IncludeTagFilters.react';
import FiltersFancyDisplay 				from './FiltersFancyDisplay.react';

import Business from 'material-ui/svg-icons/communication/business';
import Label from 'material-ui/svg-icons/action/label-outline';
import Location from 'material-ui/svg-icons/communication/location-on';
import Search from 'material-ui/svg-icons/action/search';
import DeviceIcon from 'material-ui/svg-icons/action/important-devices';
import SocialIcon from 'material-ui/svg-icons/social/group';
import {Tabs, Tab} from 'material-ui/Tabs';

import FiltersStore 			from '../stores/FiltersStore';

function getCurrentState( props ) {

	return {

		deviceFiltersActive : 	false,
		selectedIndex 		: 	0,
		activeTab 			: 	props.activeTab,
		user 				: 	props.user,
		filters 			: 	props.filters,
		customFiltersData 	: 	props.customFiltersData,
		countriesData 		: 	props.countriesData,
		organisationsData 	: 	props.organisationsData,
		tagsData	 		: 	props.tagsData,
		deviceFiltersData 	: 	props.deviceFiltersData,
		searchByData 		: 	props.searchByData,
		settings 			: 	props.settings
	}
}

var MainFilterRow = React.createClass({

	getInitialState : 	function() {

		return getCurrentState( this.props );

	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );

	},

	handleSelect 	: 	function( index, last ) {

		this.setState({

			selectedIndex 	: 	index
		})
	},

	setActiveTab : function( activeTab ) {

		FilterActions.setActiveTab( activeTab.props.value );
	},

	render 	: 	function() {

		return(

			<div className="MainFilterRow">

				<DrawerToolbar />

				<Tabs>	

					<Tab icon={<Search />} onActive={this.setActiveTab} value="filters">
				    	<CustomFilters searchByData={this.state.searchByData} customFiltersData={this.state.customFiltersData} filters={this.state.filters} active={this.state.activeTab} />
					</Tab>
			
					<Tab icon={<Location />} onActive={this.setActiveTab} value="location">
						<CountryFilters searchByData={this.state.searchByData} countriesData={this.state.countriesData} active={this.state.activeTab} />
					</Tab>

					<Tab icon={<Business />} onActive={this.setActiveTab}  value="organisation">
						<OrganisationFiltersContainer searchByData={this.state.searchByData} organisationsData={this.state.organisationsData} active={this.state.activeTab} />
					</Tab>

					<Tab icon={<Label />} onActive={this.setActiveTab} value="tags">
						<IncludeTagFilters tagsData={this.state.tagsData} active={this.state.activeTab} />

					</Tab>

					<Tab icon={<DeviceIcon />} onActive={this.setActiveTab} value="device">
						<DeviceTypeFilters deviceFiltersData={this.state.deviceFiltersData} active={this.state.activeTab} />
					</Tab>

					<Tab icon={<SocialIcon />} onActive={this.setActiveTab} value="audience">
						<VisitorTypeFilters active={this.state.activeTab} />
					</Tab>

				</Tabs>
			</div>
		)
	}
});

export default  MainFilterRow;

	