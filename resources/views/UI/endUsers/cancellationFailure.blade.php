@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>

@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

				<div id="loginLogo">
                    <img src="/images/{{ $loginLogo }}" />
                </div>

				<div class="clr"></div>

				<br /><br />

				<div class="row">

					<h1>Cancellation Error</h1>

					<p>Sorry, there was a problem cancelling your subscription.</p>

					<p>Cancellations can only be performed by super users. If problems persist, please contact us.</p>
				</div>
			</div>
		</div>
	</div>
@endsection