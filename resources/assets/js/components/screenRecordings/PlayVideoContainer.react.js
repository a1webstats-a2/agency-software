import React 					from 'react';
import ScreenRecorderStore 		from '../../stores/ScreenRecorderStore';
import ScreenRecorderActions 	from '../../actions/ScreenRecorderActions';
import PlayVideo 				from './PlayVideo.react';
const keys = { 37: 1, 38: 1, 39: 1, 40: 1 };
import CentreContent 			from './CentreContent.react';
import VideoControls 			from './VideoControls.react';
import FiltersStore  			from '../../stores/FiltersStore';
import NotificationsStore 		from '../../stores/NotificationsStore';
import NavBar 					from '../navigation/NavBar.react';
import moment 					from 'moment';
import DeviceInfo 				from './DeviceInfo.react';

function preventDefault( e ) {
  	
  	e = e || window.event;
  	if ( e.preventDefault )
      	e.preventDefault();
  	e.returnValue = false;  
}

function preventDefaultForScrollKeys( e ) {
    
    if ( keys[e.keyCode] ) {
        preventDefault( e );
        return false;
    }
}

function disableScroll() {
  	
  	if ( window.addEventListener )
    	window.addEventListener( 'DOMMouseScroll', preventDefault, false );
  	window.onwheel = preventDefault; 
  	window.onmousewheel = document.onmousewheel = preventDefault; 
 	window.ontouchmove  = preventDefault;
  	document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    
    if ( window.removeEventListener )
        window.removeEventListener( 'DOMMouseScroll', preventDefault, false );
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel 		= null; 
    window.ontouchmove 	= null;  
    document.onkeydown 	= null;  
}

function loadImage( img ) {

	let image 		= new Image();
	image.src 		= img;
	return image;
}

function getCurrentState() {

	return {

		visitData 				: 	ScreenRecorderStore.getVideoVisitData(),
		playVars 				: 	ScreenRecorderStore.getPlayVars(),
		notificationsData 		: 	{
	
			dateRangeOpen 			: 	FiltersStore.checkLightboxOpen( 'dateRange', -1 ),		
			accountOK 				: 	FiltersStore.accountIsOK(),
			snackbarSettings 		: 	FiltersStore.getSnackbarSettings(),
			isNewNotifications      :   NotificationsStore.isNewNotifications(),        
	        notificationsOpen       :   NotificationsStore.isNotificationsOpen(), 
	        numberOfNew             :   NotificationsStore.getNumberOfNewNotifications(),
	        user                    :   FiltersStore.getUser(),
 			settings 				: 	FiltersStore.getAllSettings(),
	        notifications           :   NotificationsStore.returnNotifications(),
	        notificationsBoxOpen    :   NotificationsStore.isNotificationsOpen(),
	        isOpen                  :   NotificationsStore.isNotificationsOpen(),
	        newFilters 				: 	FiltersStore.haveNewFiltersBeenApplied(),
	        paginationData 			: 	FiltersStore.getPaginationTotals(),
	        allFilters 				: 	FiltersStore.getFilters(),
	        isApplicationResting 	: 	FiltersStore.isApplicationResting(),
	        ppcChecked 				: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 1 ),
	        organicChecked 			: 	FiltersStore.checkIfFilterTypeAndIndexExists( 'trafficType', 2 ),
			display	 				: 	NotificationsStore.getDisplay(),
			viewNotificationID 		: 	NotificationsStore.viewNotificationID()
		}
	}
}


class PlayVideoContainer extends React.Component {

	constructor( props ) {

		super( props );
		this._onChange 	= this._onChange.bind( this );
		this.state 		= getCurrentState();
	}

	componentDidMount() {
		
		ScreenRecorderStore.addChangeListener( this._onChange );
		const queryParams = window.location.pathname.split( '/' );
		ScreenRecorderActions.loadVideo( queryParams[3] );

	}

	componentDidUpdate() {

	}

	componentWillUnmount() {

		ScreenRecorderStore.removeChangeListener( this._onChange );

	}

	render() {

		let style 	= {

			margin 			: 'auto', 
			width 			: 800, 
			marginTop 		: 50, 
			height 			: 500,
			overflow 		: 'hidden',
			border 			: '1px solid #ccc',
			borderRadius 	: 5
		};

		if( this.state.playVars.playStatus === 0 ) {

			style.background 	= '#000'; 
			style.opacity 		= 0.7;
			style.zIndex 		= 0;
			style.margin 		= 'auto'; 
			style.width 		= 800;

	
		}	

		let displayVisitDate = "";

		if( this.state.visitData.start_time ) {

			displayVisitDate = moment( this.state.visitData.start_time ).format( 'Do MMMM YYYY' );
		}

		const tdStyle = { borderTop : 0 };

		return (

			<div>

				<NavBar data={this.state.notificationsData} />

				<div className="clr"></div>


				<div className="container mainContainer">

					<div className="clr"></div><br /><br />


					<div className="row">

						<div className="col-md-3">

							<div style={{ marginTop : 50 }}>

								<h3>{displayVisitDate}</h3>

								<div className="clr"></div><br /><br />
								
								<table>
									<tbody>
										<tr>
											<td style={tdStyle}><strong>URL</strong></td>
										</tr>
										<tr>
											<td style={tdStyle}>{this.state.visitData.url}</td>
										</tr>
										<tr>
											<td colSpan="2">&nbsp;</td>
										</tr>
										<tr>
											<td style={tdStyle}><strong>Meta Title</strong></td>
										</tr>
										<tr>
											<td style={tdStyle}>{this.state.visitData.meta_title}</td>
										</tr>
										<tr>
											<td colSpan="2">&nbsp;</td>
										</tr>
										<tr>	
											<td style={tdStyle}><strong>Visitor</strong></td>
										</tr>
										<tr>
											<td style={tdStyle}>{this.state.visitData.name}</td>
										</tr>
									</tbody>
								</table>

								<div className="clr"></div><br /><br />

								<DeviceInfo
									os={this.state.visitData.os_flat}
									device={this.state.visitData.device_flat}
									browser={this.state.visitData.browser_flat} />

								<div className="clr"></div><br /><br /><br /><br />

								<CentreContent 
									videoIsLoaded={this.state.playVars.videoIsLoaded} 
									playStatus={this.state.playVars.playStatus}
									numPlays={this.state.playVars.numPlays} />

								<VideoControls 
									videoLength={this.state.playVars.videoLength}
									videoI={this.state.playVars.videoI}

								/>
							</div>
						</div>
						<div className="col-md-9">
							<div style={style}>
								<PlayVideo
									scrollTo={this.state.playVars.scrollTo}
									firstScreenshot={this.state.playVars.firstScreenshot}
									videoIsLoaded={this.state.playVarsvideoIsLoaded}
									playStatus={this.state.playVars.playStatus}
									mouseX={this.state.playVars.mouseX}
									mouseY={this.state.playVars.mouseY}
									aspectRatio={this.state.playVars.aspectRatio}
									background={this.state.playVars.background}
									displayOffsetLeft={this.state.playVars.displayOffsets.left}
									displayOffsetTop={this.state.playVars.displayOffsets.top} />
							</div>
						</div>
					</div>
					
				</div>
			</div>
		);

	}

	_onChange() {

		this.setState( getCurrentState() );
	}
}

export default PlayVideoContainer;