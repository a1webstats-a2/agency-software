@extends( 'templates.signup' )

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>

@section( 'content' )
    
    <div class="row">
        <div  class="col-md-6 col-md-offset-3">

            <div id="loginForm">

                <div className="clr"></div><br /><br /><br />

                <div id="loginLogo">
                    <img src="/images/{{ $loginLogo }}" />
                </div>


                <br /><br />


                <div>

                    <form method="POST" action="/auth/login">

                        <div class="form-group">
                            <label for="username">Email Address</label>
                            <input type="text" name="email" class="form-control" id="email" placeholder="Email Address" value="{{ old('email') }}">
                        </div>

                        <br />
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        </div>

                        <input type="hidden" name="filterType" value="{{ $filterType }}" />
                        <input type="hidden" name="id" value="{{ $filterID }}" />

                        <br />
                        
                        <button type="submit" alt="login" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection