@extends( 'templates.signup' )

<style>
label { display : inline!important; }
</style>

<?php 
$configData = getAgentConfigData();
$loginLogo  = ( isset( $configData['logo'] ) ) ? $configData['logo'] : '';
?>


@section( 'content' )

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div id="signupForm2" class="signupForm">

				<div id="signupLogo">

                    <img src="/images/{{ $loginLogo }}" />

				</div>

				<div class="clr"></div>

				<br /><br />

				<h1>Success! You have successfully set your new password</h1>

				<div className="clr"></div><br />

				<p><a href="/">Log In</a></p>

				<div class="clr"></div><br /><br /><br />

			</div>
		</div>
	</div>
@endsection