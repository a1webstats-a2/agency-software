import React from 'react';
import FilterActions from '../actions/FilterActions';
import ReferrerOption from './customFilters/ReferrerOption.react';
import CountryOption from './customFilters/CountryOption.react';
import PageOption from './customFilters/PageOption.react';
import SearchBy from './customFilters/SearchBy.react';
import KeywordOption from './customFilters/KeywordOption.react';
import FiltersStore from '../stores/FiltersStore';
import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

const styles = {
    toggle: {
        marginBottom: 16,
    },
    radioButton: {
        marginTop: 10,
        marginBottom: 10
    }
};

function getInitialState(props) {
    var customFiltersData = props.customFiltersData;

    return {
        searchByData: props.searchByData,
        searchString: customFiltersData.searchString,
        filters: customFiltersData.filters,
        narrowedOptions: customFiltersData.narrowedOptions,
        myKey: props.myKey,
        type: 'CustomFilterURL',
        criteria: 'include',
        value: false,
        filterChange: customFiltersData.filterChange,
        keywords: customFiltersData.keywords,
        referrers: customFiltersData.referrers,
        countries: customFiltersData.countries,
        pages: customFiltersData.pages,
        textValue: '',
        inclusionType: customFiltersData.inclusionType
    }
}

var CustomFilters = React.createClass({
    getInitialState: function () {
        return getInitialState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        var customFiltersData = newProps.customFiltersData;

        var value = 0;
        var textValue = '';

        if (customFiltersData.pages.length > 0) {

            value = -1;
            textValue = "";
        }

        this.setState({

            searchString: customFiltersData.searchString,
            narrowedOptions: customFiltersData.narrowedOptions,
            keywords: customFiltersData.keywords,
            referrers: customFiltersData.referrers,
            countries: customFiltersData.countries,
            pages: customFiltersData.pages,
            myKey: newProps.myKey,
            inclusionType: customFiltersData.inclusionType,
            filterChange: customFiltersData.filterChange,
            filters: customFiltersData.filters,
            searchByData: newProps.searchByData
        })

        if (!this.state.value) {

            this.setState({

                value: value,
                textValue: textValue
            })
        }
    },

    shouldComponentUpdate: function (nextProps, nextState) {

        if (this.props.customFiltersData.inclusionType !== nextProps.customFiltersData.inclusionType) {

            return true;
        }

        if (this.props.customFiltersData.filtersBeingEdited !== nextProps.customFiltersData.filtersBeingEdited) {

            return true;
        }

        if (this.props.customFiltersData.keywords !== nextProps.customFiltersData.keywords) {

            return true;
        }

        if (this.props.customFiltersData.referrers !== nextProps.customFiltersData.referrers) {

            return true;
        }

        if (this.props.customFiltersData.countries !== nextProps.customFiltersData.countries) {

            return true;
        }

        if (this.props.customFiltersData.pages !== nextProps.customFiltersData.pages) {

            return true;
        }

        if (this.props.customFiltersData.filters !== nextProps.customFiltersData.filters) {

            return true;
        }

        if (this.props.customFiltersData.filterChange !== nextProps.customFiltersData.filterChange) {

            return true;
        }

        if (this.state.textValue !== nextState.textValue) {

            return true;
        }

        if (this.props.customFiltersData.searchString !== nextProps.customFiltersData.searchString) {

            return true;
        }

        if (this.state.type !== nextState.type) {

            return true;
        }

        if (this.props.customFiltersData.narrowedOptions !== nextProps.customFiltersData.narrowedOptions) {

            return true;
        }

        return false;
    },

    componentDidMount: function () {

        FilterActions.setFirstLoadComplete('filters');
    },


    changeType: function (data) {

        FilterActions.setCustomFilterType(data.target.value.toLowerCase());

        this.setState({

            type: data.target.value
        });

        var defaultKeywordsID = (typeof this.state.keywords[0] !== "undefined") ? this.state.keywords[0].id : 0;
        var defaultKeywordsValue = (typeof this.state.keywords[0] !== "undefined") ? this.state.keywords[0].value : "";

        var defaultReferrerValue = (typeof this.state.referrers[0] !== "undefined") ? this.state.referrers[0].id : "";

        var defaultCountryValue = (typeof this.state.countries[0] !== "undefined") ? this.state.countries[0].id : "";


        switch (data.target.value.toLowerCase()) {

            case "customfilterkeyword" :

                this.setState({

                    criteria: 'include',
                    value: defaultKeywordsID,
                    textValue: defaultKeywordsValue

                })

                break;

            case "customfilterreferrer" :

                this.setState({

                    criteria: 'include',
                    value: defaultReferrerValue
                })

                break;

            case "customfilterreferrercountry" :

                this.setState({

                    criteria: 'include',
                    value: defaultCountryValue
                })

                break;

            case "customfiltertotalvisits" :

                this.setState({

                    criteria: 'equals'
                })

                break;

            default :

                this.setState({

                    criteria: ''
                })

                break;
        }
    },

    changeCriteria: function (data) {

        this.setState({

            criteria: data.target.value
        });
    },

    changeValue: function (data) {

        var textValue = '';

        if (typeof data.target.options != "undefined") {

            if (typeof data.target.options[data.target.selectedIndex].text !== "undefined") {

                textValue = data.target.options[data.target.selectedIndex].text;
            }

        }

        this.setState({

            value: data.target.value,
            textValue: textValue
        });

    },

    getCriteriaOptions: function () {

        var defaultOptions = (

            <RadioButtonGroup onChange={this._saveIncludeExcludeOption} defaultSelected="include" name="includeExclude">

                <RadioButton
                    label="Explicit Include"
                    value="include"
                    style={styles.radioButton}
                />

                <RadioButton
                    label="Include"
                    value="nonexplicitInclude"
                    style={styles.radioButton}
                />

                <RadioButton
                    label="Exclude"
                    value="exclude"
                    style={styles.radioButton}
                />

                <RadioButton
                    label="First Page Visited"
                    value="firstPageVisited"
                    style={styles.radioButton}
                />

                <RadioButton
                    label="Not First Page Visited"
                    value="notFirstPageVisited"
                    style={styles.radioButton}
                />

                <RadioButton
                    label="Last Page Visited"
                    value="lastPageVisited"
                    style={styles.radioButton}
                />

                <RadioButton
                    label="Not Last Page Visited"
                    value="notLastPageVisited"
                    style={styles.radioButton}
                />
            </RadioButtonGroup>
        );


        var returnOptions = defaultOptions;

        var disabledOptions = [];

        switch (this.state.type.toLowerCase()) {

            case "customfilterurl" :

                returnOptions = defaultOptions;

                break;

            case "customfilterreferrer" :

                returnOptions = (

                    <RadioButtonGroup onChange={this._saveIncludeExcludeOption} defaultSelected="include"
                                      name="includeExclude">

                        <RadioButton
                            label="Include"
                            value="include"
                            style={styles.radioButton}
                        />

                        <RadioButton
                            label="Exclude"
                            value="exclude"
                            style={styles.radioButton}
                        />


                    </RadioButtonGroup>
                )


                break;

            case "customfilterreferrercountry" :

                returnOptions = (

                    <RadioButtonGroup onChange={this._saveIncludeExcludeOption} defaultSelected="include"
                                      name="includeExclude">

                        <RadioButton
                            label="Include"
                            value="include"
                            style={styles.radioButton}
                        />

                        <RadioButton
                            label="Exclude"
                            value="exclude"
                            style={styles.radioButton}
                        />


                    </RadioButtonGroup>
                )


                break;

            case "customfilterkeyword" :

                returnOptions = (

                    <RadioButtonGroup onChange={this._saveIncludeExcludeOption} defaultSelected="include"
                                      name="includeExclude">

                        <RadioButton
                            label="Include"
                            value="include"
                            style={styles.radioButton}
                        />

                        <RadioButton
                            label="Exclude"
                            value="exclude"
                            style={styles.radioButton}
                        />

                    </RadioButtonGroup>
                )

                break;

            case "customfiltertotalvisits" :

                return (

                    <RadioButtonGroup onChange={this._saveIncludeExcludeOption} defaultSelected="include"
                                      name="includeExclude">

                        <RadioButton
                            label="Equals"
                            value="equals"
                            style={styles.radioButton}
                        />

                        <RadioButton
                            label="Greater Than"
                            value="greaterThan"
                            style={styles.radioButton}
                        />

                        <RadioButton
                            label="Less Than"
                            value="lessThan"
                            style={styles.radioButton}
                        />
                    </RadioButtonGroup>
                )
            case "customfiltertimeonsite" :
                return (
                    <RadioButtonGroup onChange={this._saveIncludeExcludeOption} defaultSelected="include"
                                      name="includeExclude">

                        <RadioButton
                            label="Equals"
                            value="equals"
                            style={styles.radioButton}
                        />

                        <RadioButton
                            label="Greater Than"
                            value="greaterThan"
                            style={styles.radioButton}
                        />

                        <RadioButton
                            label="Less Than"
                            value="lessThan"
                            style={styles.radioButton}
                        />


                    </RadioButtonGroup>
                )
                break;
        }

        return returnOptions;
    },
    getAllowInput: function () {
        var referrerOptions = [];

        for (var i = 0; i < this.state.referrers.length; i++) {
            referrerOptions.push(<ReferrerOption key={i} details={this.state.referrers[i]}/>);
        }

        var countryOptions = [];

        for (let [key, value] of FiltersStore.objectEntries(this.state.countries)) {
            if (value.name) {
                countryOptions.push(<CountryOption key={key} myKey={value.id} countryName={value.name}/>);
            }
        }

        var pageOptions = [];

        pageOptions.push(<PageOption key={-1} myKey={-1} pageName="No URL Selected"/>);

        this.state.pages.map(function (page, i) {
            pageOptions.push(<PageOption key={i} myKey={page.id} pageName={page.url}/>);
        })

        var keywordOptions = [];

        this.state.keywords.map(function (keyword, i) {
            keywordOptions.push(<KeywordOption key={i} myKey={keyword.id} keyword={keyword}/>);
        });

        var defaultInput = (
            <div className="allowInput">
                <div className="row">
                    <div className="col-md-12">
                        <h4>Value</h4>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <input type="text" onChange={this.changeValue} className="form-control"/>
                        </div>
                    </div>
                </div>
            </div>

        );

        var returnInput = defaultInput;

        switch (this.state.type.toLowerCase()) {
            case "customfilterreferrer" :
                returnInput =
                    (
                        <div className="criteriaOptions">

                            <div className="row">
                                <div className="col-md-12">
                                    <h4>OR select from dropdown</h4>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <select className="form-control" defaultValue={referrerOptions[0]}
                                                onChange={this.changeValue} value={this.state.value}>


                                            {referrerOptions}

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );

                break;

            case "customfilterreferrercountry" :


                returnInput = (


                    <div className="criteriaOptions">

                        <div className="row">
                            <div className="col-md-12">
                                <h4>OR select from dropdown</h4>
                            </div>
                        </div>

                        <div className="row">

                            <div className="col-md-12">
                                <div className="form-group">
                                    <select className="form-control" defaultValue={countryOptions[0]}
                                            onChange={this.changeValue} value={this.state.value}>

                                        {countryOptions}

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                break;
            case "customfilterurl" :
                returnInput = (
                    <div className="criteriaOptions">

                        <div className="row">
                            <div className="col-md-12">
                                <h4>OR select from dropdown</h4>
                            </div>
                        </div>

                        <div className="row">

                            <div className="col-md-12">
                                <div className="form-group">
                                    <select className="form-control" onChange={this.changeValue}
                                            value={this.state.value}>

                                        {pageOptions}

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                break;
            case "customfilterkeyword" :
                returnInput = (
                    <div className="criteriaOptions">

                        <div className="row">
                            <div className="col-md-12">
                                <h4>OR select from dropdown</h4>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <select className="form-control" onChange={this.changeValue}
                                            value={this.state.value}>

                                        {keywordOptions}

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                break;
        }

        return returnInput;
    },

    render: function () {
        var criteriaOptions = this.getCriteriaOptions();
        var input = this.getAllowInput();

        return (
            <div className="row">

                <div className="col-md-12">

                    <div className="row">
                        <div className="col-md-12">
                            <br/><h3>Filters</h3>
                        </div>
                    </div>

                    <div className="clr"></div>

                    <div className="row">

                        <div className="col-md-12">

                            {criteriaOptions}

                        </div>
                    </div>
                    <div className="clr"></div>
                    <br/>
                    <div className="row">
                        <div className="col-md-6">
                            <h4>Filter Type</h4>
                        </div>
                    </div>
                    <div className="clr"></div>
                    <div className="row">
                        <div className="col-md-12">
                            <select className="form-control" onChange={this.changeType} value={this.state.type}>
                                <option value="CustomFilterURL">URL</option>
                                <option value="CustomFilterKeyword">Keyword</option>
                                <option value="customFilterTotalVisits">Impressions Total</option>
                                <option value="customFilterReferrer">Referrer</option>
                                <option value="customFilterTimeOnSite">Time Spent on Page (seconds)</option>

                            </select>
                        </div>
                    </div>
                    <div className="clr"></div>
                    <br/>
                    <SearchBy searchByData={this.state.searchByData} type={this.state.type}/>
                    <div className="clr"></div>
                    <div className="row customRows">
                        <div className="col-md-12">
                            {input}
                        </div>
                    </div>
                    <div className="clr"></div>
                    <br/>
                    <div className="row">
                        <div className="col-md-12">
                            <RaisedButton label="Add Filter" primary={true} onClick={this._save}/>
                        </div>
                        <div className="clr"></div>
                        <br/><br/>
                    </div>
                </div>
            </div>
        )
    },
    _saveIncludeExcludeOption: function (event) {
        this.setState({
            criteria: event.target.value
        });
        FilterActions.setIncludeCustomFilterType(event.target.value);
    },
    _save: function () {
        if (this.state.value === -1) {
            return false;
        }

        var newState = {
            type: this.state.type,
            storedValue: {
                criteria: this.state.criteria,
                value: this.state.value,
                textValue: this.state.textValue
            },
            id: this.state.value
        }

        FilterActions.createFilter(newState);
    }
});

export default CustomFilters;
