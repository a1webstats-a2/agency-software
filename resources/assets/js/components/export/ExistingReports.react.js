import React from 'react';
import FilterActions from '../../actions/FilterActions';
import FiltersStore from '../../stores/FiltersStore';
import ExistingReportRow from './ExistingReportRow.react';
import ExistingAutomatedReport from './ExistingAutomatedReport.react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

function getCurrentState( props ) {

	return {

		open 			: 	props.open,
		data 			: 	props.existingReportsData,
		reports 		: 	props.existingReportsData.reports,
		settings 		: 	props.settings,
		exportOptions 	: 	props.exportOptions
	}
}

var ExistingReports = React.createClass({

	getInitialState : function() {

		return getCurrentState( this.props );
	},

	handleClose : function() {

		FilterActions.closeLightbox();
	},

	shouldComponentUpdate : function( nextProps, nextState ) {

		if( this.props.open !== nextProps.open ) {

			return true;
		}

		if( this.props.existingReportsData !== nextProps.existingReportsData ) {

			return true;
		}

		if( this.props.settings !== nextProps.settings ) {

			return true;
		}

		if( this.props.exportOptions !== nextProps.exportOptions ) {

			return true;
		}

		return false;

	},

	componentWillReceiveProps : function( newProps ) {

		this.setState( getCurrentState( newProps ) );
	},

	render : function() {

		var existingReportTRs = this.state.reports.map( function( report, i ) {

			return (

				<ExistingReportRow settings={this.state.settings} exportOptions={this.state.exportOptions} key={i} data={report} />
			)

		}.bind( this ) );

		const actions = [

            <FlatButton
                label="Close"
                secondary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Set Automation"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this._setAutomationDetails}
            />
        ];

        var open = ( typeof this.state.open !== "undefined" ) ? this.state.open : false;


		return (

			<div className="existingReports">

				<Dialog
		        	title="Existing Automated Reports"
		        	modal={false}
		        	actions={actions}
		        	autoScrollBodyContent={true}
		        	open={open}
		        	onRequestClose={this.handleClose} 
	                autoScrollBodyContent={true}

		        >	

		        	<table className="table">

		        		<thead>
		        			<tr>
		        				<th>Report Name</th>
		        				<th>Actions</th>
		        				<th>Last Run</th>
		        			</tr>
		        		</thead>

		        		<tbody>
		        			{existingReportTRs}
		        		</tbody>
		        	</table>

		        </Dialog>

		        <ExistingAutomatedReport settings={this.state.settings} exportOptions={this.state.exportOptions} />

			</div>
		);
	}	
});

export default  ExistingReports;