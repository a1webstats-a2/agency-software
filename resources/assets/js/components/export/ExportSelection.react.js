import React from 'react';

const getCurrentState = function (props) {
    return {
        settings: props.settings,
        exportSelection: props.selection
    }
}

function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
}

const ExportSelection = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.selection !== nextProps.selection) {
            return true;
        }
        if (this.state.selection !== nextState.selection) {
            return true;
        }
        if (this.props.settings !== nextProps.settings) {
            return true;
        }
        return false;
    },
    render: function () {
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var trs = this.state.exportSelection.map(function (session, i) {
            var displayDate = "New date";
            var rawDate = new Date(session.sessionDateTime);
            switch (this.state.settings.user.preferred_date_format) {
                case "dd/mm/yyyy" :
                    displayDate = twoDigits(rawDate.getDate()) + '/' + twoDigits(rawDate.getMonth()) + '/' + rawDate.getFullYear();
                    break;
                case "mm/dd/yyyy" :
                    displayDate = twoDigits(rawDate.getMonth()) + '/' + twoDigits(rawDate.getDate()) + '/' + rawDate.getFullYear();
                    break;
                case "yyyy-mm-dd" :
                    displayDate = rawDate.getFullYear() + '-' + twoDigits(rawDate.getMonth()) + '-' + twoDigits(rawDate.getDate());
                    break;
                case "fulltext" :
                    displayDate = ordinal_suffix_of(rawDate.getDate()) + ' ' + monthNames[rawDate.getMonth()] + ' ' + rawDate.getFullYear();
                    break;
            }
            return (
                <tr key={i}>
                    <td>{session.organisationName}</td>
                    <td>{displayDate}</td>
                </tr>
            )
        }.bind(this));
        let title = "";
        if (this.state.exportSelection.length > 0) {
            title = (
                <h4>Export Selection</h4>
            )
        }
        return (
            <div className="exportSelection">
                {title}
                <table className="table">
                    <tbody>
                    {trs}
                    </tbody>
                </table>
            </div>
        )
    }
});
export default ExportSelection;
