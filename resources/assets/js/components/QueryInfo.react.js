import React from 'react';

var QueryInfo = React.createClass({

	render 	: 	function() {


		return(

			<div className="queryInfo">
				<br />
				<h4>Query execution time: {this.props.queryInfo}</h4>

			</div>

		);
	}

});

export default  QueryInfo;