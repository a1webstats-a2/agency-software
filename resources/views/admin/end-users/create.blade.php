@extends( 'templates.default' )

@section( 'content' )

	<h4>Create new End User</h4>

	<form method="POST">

		<div class="form-group">

			<label>Organisation Name</label>

			<input type="text" name="organisationName" />

		</div>

		<div class="form-group">

			<button type="submit" class="btn btn-primary">Create User</button>

		</div>
	</form>

@endsection