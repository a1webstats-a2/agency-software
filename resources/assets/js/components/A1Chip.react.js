import React from 'react';
import FilterActions from '../actions/FilterActions';
import Chip from 'material-ui/Chip';
import Business from 'material-ui/svg-icons/communication/business';
import Location from 'material-ui/svg-icons/communication/location-on';
import DeviceIcon from 'material-ui/svg-icons/action/important-devices';
import SocialIcon from 'material-ui/svg-icons/social/group';
import BankIcon from 'material-ui/svg-icons/editor/attach-money';
import Avatar from 'material-ui/Avatar';
import TagIcon from 'material-ui/svg-icons/action/label-outline';
import PPCIcon from 'material-ui/svg-icons/action/credit-card';
import OrganicIcon from 'material-ui/svg-icons/action/list';
import Key from 'material-ui/svg-icons/communication/vpn-key';
import Today from 'material-ui/svg-icons/action/today';
import PageView from 'material-ui/svg-icons/action/pageview';
import Numbered from 'material-ui/svg-icons/editor/format-list-numbered';
import ReferrerIcon from 'material-ui/svg-icons/navigation/chevron-left';
import Timeline from 'material-ui/svg-icons/action/timeline';
import Schedule from 'material-ui/svg-icons/action/schedule';
import PreviousHistoryIcon from 'material-ui/svg-icons/action/restore';
import TrafficIcon from 'material-ui/svg-icons/maps/traffic';
import Lang from '../classes/Lang';

const styles = {
    chip: {
        margin: 8
    },
    chipWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        float: 'left'
    },
    toggle: {
        thumbSwitched: {
            backgroundColor: '#53b68b',
        },
        trackSwitched: {
            backgroundColor: '#ccc',
        },
        thumbOff: {
            backgroundColor: '#ccc',
        },
        trackOff: {
            backgroundColor: '#ccc',
        }
    }
}

function getHumanFriendlyIncludeTypeString(includeType) {
    let returnString = '';

    switch (includeType.toLowerCase()) {
        case 'nonexplicitinclude' :
            returnString = 'visit may contain';

            break;
        case 'include' :
            returnString = 'visit must include';

            break;
        case 'exclude' :
            returnString = 'visit must exclude';

            break;
        case 'firstpagevisited' :
            returnString = 'include';

            break;
        case 'notfirstpagevisited' :
            returnString = 'exclude';

            break;
        case 'lastpagevisited' :
            returnString = 'last page visited';

            break;
        case 'notlastpagevisited' :
            returnString = 'not last page visited';

            break;
    }

    return returnString;
}

function getCurrentState(props) {

    var latestAddedFilters = props.latestAddedFilters;

    var isNew = false;

    if (typeof latestAddedFilters[props.filter.type] !== 'undefined') {

        if (latestAddedFilters[props.filter.type].indexOf(props.filter.storedValue.id) > -1) {

            isNew = true;
        }
    }

    return {

        noResultsStatus: props.noResultsStatus,
        latestAddedFilters: props.latestAddedFilters,
        avatar: props.avatar,
        type: props.type,
        filter: props.filter,
        text: props.text,
        isNew: isNew
    }
}

var A1Chip = React.createClass({
    getInitialState: function () {
        return getCurrentState(this.props);
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.props.noResultsStatus !== nextProps.noResultsStatus) {
            return true;
        }

        if (this.props.filter !== nextProps.filter) {
            return true;
        }

        return false;
    },
    render: function () {
        var includeType = 'include';
        var valueText = this.state.text;

        if (typeof this.state.filter.storedValue.anyThatMatch !== 'undefined') {
            includeType = 'anything that matches';
            valueText = "\"" + this.state.filter.storedValue.anyThatMatch.pattern + "\"";

        } else if (typeof this.state.filter.storedValue.criteria !== "undefined") {
            includeType = getHumanFriendlyIncludeTypeString(this.state.filter.storedValue.criteria);
        }

        var thisIcon = null;

        switch (this.state.avatar) {
            case "bank" :
                thisIcon = <BankIcon/>;

                break;
            case "tag" :
                thisIcon = <TagIcon/>;

                break;
            case "keyword" :
                thisIcon = <Key/>;

                break;
            case "today"    :
                thisIcon = <Today/>;

                break;
            case "pageView"    :
                thisIcon = <PageView/>;

                break;
            case "numbered" :
                thisIcon = <Numbered/>;

                break;
            case "location" :
                thisIcon = <Location/>;

                break;
            case "ppc" :
                thisIcon = <PPCIcon/>;

                break;
            case "organic" :
                thisIcon = <OrganicIcon/>;

                break;
            case "referrerIcon" :
                thisIcon = <ReferrerIcon/>

                break;
            case "timeline" :
                thisIcon = <Timeline/>

                break;
            case "business" :
                thisIcon = <Business/>

                break;
            case "schedule" :
                thisIcon = <Schedule/>

                break;
            case "socialIcon" :
                thisIcon = <SocialIcon/>;

                break;
            case "deviceIcon" :
                thisIcon = <DeviceIcon/>;

                break;
            case "previousHistory" :
                thisIcon = <PreviousHistoryIcon/>

                break;
            case "traffic" :
                thisIcon = <TrafficIcon/>

                break;
        }

        var avatar = thisIcon;
        var avatarBackgroundColor = "#170550";
        var backgroundColor = "#170550";

        if (this.state.isNew) {
            backgroundColor = "#53b68b";
            avatarBackgroundColor = "#53b68b";
        }

        var showCriteriaTypeFor = [
            'customfilterurl',
            'customfilterentrypage',
            'customfilterreferrer',
            'customfilterreferrercountry',
            'customfilterkeyword',
            'organisationfilter'
        ]

        let includeTypeDisplay;

        if (showCriteriaTypeFor.indexOf(this.state.filter.type.toLowerCase()) === -1) {
            includeTypeDisplay = " ";
        } else {
            includeTypeDisplay = "-> " + includeType + " ->";
        }

        var disallowDeleteTypes = [
            'schedule',
        ];

        var chipStyle = styles.chip;

        if (this.state.noResultsStatus) {
            backgroundColor = "#FFBF00";
            avatarBackgroundColor = "#FFBF00";
        }

        chipStyle.backgroundColor = backgroundColor;

        if (disallowDeleteTypes.indexOf(this.state.avatar) > -1) {
            return (
                <Chip style={chipStyle}>
                    <Avatar backgroundColor={avatarBackgroundColor} icon={avatar}/>
                    <strong>{Lang.getWordUCFirst(this.state.type)}</strong> {includeTypeDisplay} {this.state.text}
                </Chip>
            )
        } else {
            return (
                <div style={styles.chipWrapper}>
                    <Chip className="chip" labelColor="#ffffff" onRequestDelete={this._handleRequestDelete}
                          style={chipStyle}>

                        <Avatar backgroundColor={avatarBackgroundColor} icon={avatar}/>

                        <strong>{Lang.getWordUCFirst(this.state.type)}</strong> {includeTypeDisplay} {valueText}

                    </Chip>
                </div>
            )
        }
    },

    _handleRequestDelete: function () {
        var remove = {
            type: this.state.filter.type,
            id: this.state.filter.storedValue.id
        }

        FilterActions.removeFilterByType(remove);
    }
});

export default A1Chip;
