import React from 'react';
import IncludeField from './IncludeField.react';
import {sortable} from 'react-sortable';

function getCurrentState(props) {
    return {
        editing: props.editing,
        items: props.data,
        draggingIndex: null
    }
}

var ListItem = React.createClass({
    displayName: 'SortableListItem',
    render: function () {
        return (
            <div {...this.props} className="list-item">
                <IncludeField
                    allItems={this.props.children.allItems}
                    editing={this.props.children.editing}
                    displayName={this.props.children.name}
                    include={this.props.children.include}
                    itemID={parseInt(this.props.children.id)}/>
            </div>
        )
    }
})

var SortableListItem = sortable(ListItem);
var SelectIncludeFields = React.createClass({
    getInitialState: function () {
        var currentState = getCurrentState(this.props);

        return currentState;
    },
    componentWillReceiveProps: function (newProps) {
        this.setState(getCurrentState(newProps));
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.state.items[0].displayName !== nextState.items[0].displayName) {
            return true;
        }

        if (this.state.items !== nextState.items) {
            return true;
        }

        if (this.state.draggingIndex !== nextState.draggingIndex) {
            return true;
        }

        if (this.props.data !== nextProps.data) {
            return true;
        }

        if (this.props.editing !== nextProps.editing) {
            return true;
        }

        return false;
    },

    render: function () {
        var listItems = this.state.items.map(function (item, i) {
            if (typeof item.include === "boolean") {
                var checked = item.include;
            } else {
                var checked = parseInt(item.include) === 1;
            }

            item.include  = checked;
            item.editing  = this.state.editing;
            item.allItems = this.state.items;

            var childProps = {className: 'myClass1'};

            return (
                <SortableListItem
                    key={i}
                    updateState={this._updateState}
                    items={this.state.items}
                    draggingIndex={this.state.draggingIndex}
                    sortId={i}
                    outline="list"
                    childProps={childProps}
                >{item}</SortableListItem>
            )
        }.bind(this));

        return (
            <div style={{height: 500, overflowY: 'scroll'}}>
                <div className="list" style={{width: 600}}>{listItems}</div>
            </div>
        )
    },
    _updateState: function (obj) {
        this.setState(obj);
    }
});

export default SelectIncludeFields;